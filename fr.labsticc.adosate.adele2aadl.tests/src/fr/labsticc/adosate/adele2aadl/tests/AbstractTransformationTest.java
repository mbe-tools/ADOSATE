package fr.labsticc.adosate.adele2aadl.tests;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.topcased.adele.common.utils.AdeleCommonUtils;

import de.hpi.sam.mote.TransformationDirection;
import de.hpi.sam.mote.impl.TransformationException;

public class AbstractTransformationTest extends AbstractAdele2AadlTest {

	protected static final String TRANS_DIR = "unit/";
	protected static final String ROOT_TRANS_DIR = ROOT_TEST_FILES_DIR + TRANS_DIR;

	protected AbstractTransformationTest( 	final boolean pb_deleteResultsWhenPassed,
											final String p_testFilesDir )
	throws CoreException {
		super( pb_deleteResultsWhenPassed, p_testFilesDir );
	}

	protected final void testTransformations() {
		testTransformations( Thread.currentThread().getStackTrace()[ 2 ].getMethodName() );
	}
	
	protected void testTransformations( final String p_fileNamePrefix ) {
		testTransformations( p_fileNamePrefix, true, true );
	}

	protected final void testTransformations( final boolean pb_testReverse,
										final boolean pb_testForward ) {
		testTransformations( Thread.currentThread().getStackTrace()[ 2 ].getMethodName(), pb_testReverse, pb_testForward );
	}

	protected void testTransformations( final String p_fileNamePrefix,
										final boolean pb_testReverse,
										final boolean pb_testForward ) {
		final String testFilesDir = getTestFilesDir();
		final URI reverseAadlResUri = URI.createURI( testFilesDir + p_fileNamePrefix + "/" + REVERSE + "." + AADL_EXT );
		final URI reverseAdeleResUri = reverseAadlResUri.trimFileExtension().appendFileExtension( AdeleCommonUtils.ADELE_EXT );
		final URI forwardAdeleResUri = URI.createURI( testFilesDir + p_fileNamePrefix + "/" + FORWARD + "." + AdeleCommonUtils.ADELE_EXT );
		final URI forwardAadlResUri = forwardAdeleResUri.trimFileExtension().appendFileExtension( AADL_EXT );

		try {
			// Ensure there is no existing files for the result (can cause problem with the component mapping function
			// managing external resources).
			if ( pb_testReverse ) {
				System.out.println( "Testing reverse transformation for " + reverseAadlResUri );
				deleteResourceIfExists( reverseAdeleResUri );
				buildTestProject();
				final Resource adeleResult = transformFromFile( reverseAdeleResUri,
																reverseAadlResUri, 
																TransformationDirection.REVERSE );
				final Resource expectedAdeleResult = getResource( forwardAdeleResUri );
				
				check( differences( adeleResult, expectedAdeleResult ) );
				
				if ( isDeletingResultsWhenPasses() ) {
					adeleResult.delete( null );
				}
			}
	
			if ( pb_testForward ) {
				System.out.println( "Testing forward transformation for " + forwardAdeleResUri );
				deleteResourceIfExists( forwardAadlResUri );
				buildTestProject();
				final Resource aadlResult = transformFromFile( 	forwardAdeleResUri,
																forwardAadlResUri, 
																TransformationDirection.FORWARD );
				final Resource expectedAadlResult = getResource( reverseAadlResUri );
		
				check( differences( aadlResult, expectedAadlResult ) );
				
				if ( isDeletingResultsWhenPasses() ) {
					aadlResult.delete( null );
				}
			}
			
			checkConsistency( forwardAdeleResUri, reverseAadlResUri );
		}
		catch( final AssertionError p_err ) {
			throw p_err;
		}
		catch( final Throwable p_th ) {
			p_th.printStackTrace();
			final StringWriter outWriter = new StringWriter();
			final PrintWriter printWriter = new PrintWriter( outWriter );
			p_th.printStackTrace( printWriter );
			
			fail( outWriter.getBuffer().toString() );
		}
	}
	
	private Resource transformFromFile( final URI p_leftFileUri, 
										final URI p_rightFileUri, 
										final TransformationDirection p_transDirection ) 
	throws TransformationException, IOException {
		final ResourceSet resSet = createResourceSet();
		final Resource leftResource;
		final Resource rightResource;
		
		if ( p_transDirection == TransformationDirection.FORWARD ) {
			leftResource = resSet.getResource( p_leftFileUri, true );
			rightResource = resSet.createResource( p_rightFileUri );
		}
		else if ( p_transDirection == TransformationDirection.REVERSE ) {
			leftResource = resSet.createResource( p_leftFileUri );
			rightResource = resSet.getResource( p_rightFileUri, true );
		}
		else {
			throw new IllegalArgumentException();
		}

		getTggEngine().transform( leftResource, rightResource, new BasicEMap<Resource, EList<EObject>>(), p_transDirection, ADELE_2_AADL_RULESET_ID, false );

		if ( p_transDirection == TransformationDirection.FORWARD ) {
			rightResource.save( null );
			
			return getResource( p_rightFileUri );
		}
		
		if ( p_transDirection == TransformationDirection.REVERSE ) {
			leftResource.save( null );
			
			return getResource( p_leftFileUri );
		}
		
		throw new IllegalArgumentException( "Invalid transformation direction." );
	}
}
