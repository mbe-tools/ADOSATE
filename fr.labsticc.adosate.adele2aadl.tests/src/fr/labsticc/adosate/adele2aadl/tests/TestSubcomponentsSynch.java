package fr.labsticc.adosate.adele2aadl.tests;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;

public class TestSubcomponentsSynch extends AbstractSynchronizationTest {
	
	public TestSubcomponentsSynch()
	throws CoreException {
		super( true, "subcomponent/" );
	}
	
	@Override
	public void setUp()
	throws Exception {
		//prepare( "subcomponent_add_data", createResourceSet() );
		
		super.setUp();
	}

	@Test
	public void subcomponent_add_data() {
		testSynchronization();
	}

	@Test
	public void subcomponent_add_subcomponent_type() {
		testSynchronization();
	}

//	@Test
//	public void subcomponent_add_subcomponent_type_external() {
//		testSynchronization();
//	}

	@Test
	public void subcomponent_remove_subcomponent_type() {
		testSynchronization();
	}
}
