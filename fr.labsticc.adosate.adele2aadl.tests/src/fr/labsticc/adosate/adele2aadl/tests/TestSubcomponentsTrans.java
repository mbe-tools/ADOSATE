package fr.labsticc.adosate.adele2aadl.tests;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;

public class TestSubcomponentsTrans extends AbstractTransformationTest {
	
	private static final String TEST_FILES_DIR = ROOT_TRANS_DIR + "subcomponent/";
	
	public TestSubcomponentsTrans()
	throws CoreException {
		super( true, TEST_FILES_DIR );
	}

	@Test
	public void ruleSystem2SystemSubcomponent() {
		testTransformations();
	}

	@Test
	public void ruleSystem2SystemSubcomponentTyped() {
		testTransformations();
	}
	
	@Test
	public void ruleSystem2SystemSubcomponentTypedRefinedTyped() {
		testTransformations();
	}
	
	@Test
	public void ruleSystem2SystemSubcomponentRefinedTyped() {
		testTransformations();
	}
	
	@Test
	public void ruleSystem2SystemSubcomponentRefinedTypedExtRes() {
		testTransformations();
	}
	
	@Test
	public void ruleAbstract2AbstractSubcomponentTypedRefinedTyped() {
		testTransformations();
	}
	
	@Test
	public void rulesTypedAsParent() {
		testTransformations();
	}
}
