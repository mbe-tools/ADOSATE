package fr.labsticc.adosate.adele2aadl.tests;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;

public class TestComponentTypesTrans extends AbstractTransformationTest {
	
	private static final String TEST_FILES_DIR = ROOT_TRANS_DIR;
	
	public TestComponentTypesTrans()
	throws CoreException {
		super( true, TEST_FILES_DIR );
	}

	@Test
	public void component_type() {
		testTransformations( true, true );
	}
	
	@Test
	public void component_type_circ_ref() {
		testTransformations( true, true );
	}
}
