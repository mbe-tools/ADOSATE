package fr.labsticc.adosate.adele2aadl.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses( { 	TestComponentImplTrans.class,
					TestComponentTypesTrans.class,
					TestConnectionsTrans.class,
					TestFeaturesTrans.class,
					TestSubcomponentsTrans.class,
					TestComponentTypesSynch.class,
					TestSubcomponentsSynch.class } )

public class AllTests {
}
