package fr.labsticc.adosate.adele2aadl.tests;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;

public class TestComponentImplTrans extends AbstractTransformationTest {
	
	private static final String TEST_FILES_DIR = ROOT_TRANS_DIR;
	
	public TestComponentImplTrans()
	throws CoreException {
		super( true, TEST_FILES_DIR );
	}

	@Test
	public void component_implementation() {
		testTransformations();
	}
}
