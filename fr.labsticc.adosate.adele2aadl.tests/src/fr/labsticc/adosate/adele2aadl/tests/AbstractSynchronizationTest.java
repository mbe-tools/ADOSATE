package fr.labsticc.adosate.adele2aadl.tests;

import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.topcased.adele.common.utils.AdeleCommonUtils;

import de.hpi.sam.mote.TransformationDirection;
import de.hpi.sam.mote.impl.TransformationException;
import de.hpi.sam.mote.rules.TransformationExecutionResult;
import fr.labsticc.adosate.gmm.osategmm.OsategmmFactory;
import fr.labsticc.gmm.model.megamodel.ComparisonSettings;
import fr.labsticc.gmm.model.megamodel.MegamodelFactory;
import fr.labsticc.gmm.model.megamodel.MetaModel;
import fr.labsticc.gmm.mote.model.gmmmote.GmmmoteFactory;
import fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation;
import fr.openpeople.models.aadl.constants.AadlExtensionHelper;

public class AbstractSynchronizationTest extends AbstractAdele2AadlTest {
	
	private static final String SYNCH_DIR = "synchronization/";
	protected static final String ROOT_SYNCH_DIR = ROOT_TEST_FILES_DIR + SYNCH_DIR;

	protected static final String CHANGES_PROJECT_NAME = "fr.labsticc.adosate.adele2aadl.tests.resources.changes";
	protected static final String ROOT_CHANGES_FILES_DIR = "platform:/resource/" + CHANGES_PROJECT_NAME + "/";

	protected static final String TEMP_PROJECT_NAME = "fr.labsticc.adosate.adele2aadl.tests.resources.temp";
	protected static final String ROOT_TEMP_FILES_DIR = "platform:/resource/" + TEMP_PROJECT_NAME + "/";
	
	private final MoteSynchronizationRelation moteSynchRelation;
	
	protected AbstractSynchronizationTest( 	final boolean pb_deleteResultsWhenPassed,
											final String p_testSubdir ) 
	throws CoreException {
		super( pb_deleteResultsWhenPassed, p_testSubdir );
		
		moteSynchRelation = GmmmoteFactory.eINSTANCE.createMoteSynchronizationRelation();
		moteSynchRelation.setName( "Adele-AADL" );
		moteSynchRelation.setEngine( getTggEngine() );
		moteSynchRelation.setRuleSetId( ADELE_2_AADL_RULESET_ID );
		moteSynchRelation.setOwnedRelationPolicy( OsategmmFactory.eINSTANCE.createAdosateUriRelBinRelPolicy() );
		final MetaModel adeleMm = MegamodelFactory.eINSTANCE.createMetaModel();
		adeleMm.setName( "Adele" );
		adeleMm.getFileExtensions().add( AdeleCommonUtils.ADELE_EXT );
		final ComparisonSettings adeleCompSettings = MegamodelFactory.eINSTANCE.createComparisonSettings();
		adeleCompSettings.setNameSimilarityWeight( nameSimilarityWeight);
		adeleCompSettings.setPositionSimilarityWeight( positionSimilarityWeight );
		adeleMm.setOwnedComparisonSettings( adeleCompSettings );
		moteSynchRelation.setLeftMetaModel( adeleMm );

		final MetaModel aadlMm = MegamodelFactory.eINSTANCE.createMetaModel();
		aadlMm.setName( "AADL" );
		aadlMm.getFileExtensions().add( AadlExtensionHelper.AADL_EXT );
		final ComparisonSettings aadlCompSettings = MegamodelFactory.eINSTANCE.createComparisonSettings();
		aadlCompSettings.setNameSimilarityWeight( nameSimilarityWeight );
		aadlCompSettings.setPositionSimilarityWeight( positionSimilarityWeight );
		aadlMm.setOwnedComparisonSettings( aadlCompSettings );
		moteSynchRelation.setRightMetaModel( aadlMm );
	}

	protected final void testSynchronization() {
		testSynchronization( Thread.currentThread().getStackTrace()[ 2 ].getMethodName(), true, true );
	}
	
	protected final void testSynchronization( 	final boolean pb_testReverse,
												final boolean pb_testForward ) {
		testSynchronization( Thread.currentThread().getStackTrace()[ 2 ].getMethodName(), pb_testReverse, pb_testForward );
	}
	
	protected void copyResource( 	final URI p_sourceResUri,
									final URI p_targetResUri ) 
	throws IOException {
		final ResourceSet resSet = createResourceSet();
		final Resource srcRes = resSet.getResource( p_sourceResUri, true );
		final Resource targetRes = resSet.createResource( p_targetResUri );
		final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		srcRes.save( outputStream, null );
		final ByteArrayInputStream inputStream = new ByteArrayInputStream( outputStream.toByteArray() );
		targetRes.load( inputStream, null );
		targetRes.save( null );
	}

	protected void testSynchronization( final String p_fileName,
										final boolean pb_testReverse,
										final boolean pb_testForward ) {
		final URI changedAdeleResUri = URI.createURI( ROOT_CHANGES_FILES_DIR + getTestFilesDir() + p_fileName + "." + AdeleCommonUtils.ADELE_EXT );
		final URI changedAadlResUri = changedAdeleResUri.trimFileExtension().appendFileExtension( AADL_EXT );
		
		try {

			// Ensure there is no existing files for the result (can cause problem with the component mapping function
			// managing external resources.
			if ( pb_testReverse ) {
				final ResourceSet resSet = createResourceSet();
				
				// Copy the resources in temp directory and check that they are consistent
				final Resource[] tempResources = prepare( p_fileName, resSet );
				final Resource tempAdeleRes = tempResources[ 0 ];
				final Resource tempAadlRes = tempResources[ 1 ];

				// Apply changes to AADL
				final Resource changedAadlRes = getResource( changedAadlResUri, resSet );
				final MetaModel aadlMm = moteSynchRelation.getRightMetaModel();
				getMergeService().merge( 	changedAadlRes, 
											tempAadlRes,
											false,
											aadlMm.useIdForCompare(),
											aadlMm.getNameSimilarityWeight(),
											aadlMm.getPositionSimilarityWeight(), 
											null,
											null,
											null );

				// Synchronize
				moteSynchRelation.synchronize( tempAdeleRes, tempAadlRes, new BasicEMap<Resource, EList<EObject>>(), TransformationDirection.REVERSE );
				tempAadlRes.save( null );
				tempAdeleRes.save( null );
				
				final Resource changedAdeleRes = getResource( changedAdeleResUri );
				check( differences( tempAdeleRes, changedAdeleRes ) );

				if ( isDeletingResultsWhenPasses() ) {
					tempAadlRes.delete( null );
					tempAdeleRes.delete( null );
				}
			}
	
			if ( pb_testForward ) {
				final ResourceSet resSet = createResourceSet();
				final Resource[] tempResources = prepare( p_fileName, resSet );
				final Resource tempAdeleRes = tempResources[ 0 ];
				final Resource tempAadlRes = tempResources[ 1 ];

				// Apply changes to Adele
				final Resource changedAdeleRes = getResource( changedAdeleResUri, resSet );
				final MetaModel adeleMm = moteSynchRelation.getLeftMetaModel();
				getMergeService().merge(	changedAdeleRes,
											tempAdeleRes, 
											true,
											false,
											adeleMm.getNameSimilarityWeight(),
											adeleMm.getPositionSimilarityWeight(),
											null,
											null,
											null );

				// Synchronize
				moteSynchRelation.synchronize( tempAdeleRes, tempAadlRes, new BasicEMap<Resource, EList<EObject>>(), TransformationDirection.FORWARD );
				tempAdeleRes.save( null );
				tempAadlRes.save( null );
				
				final Resource changedAadlRes = getResource( changedAadlResUri );
				
				check( differences( tempAadlRes, changedAadlRes ) );
				
				if ( isDeletingResultsWhenPasses() ) {
					tempAdeleRes.delete( null );
					tempAadlRes.delete( null );
				}
			}
		}
		catch( final AssertionError p_err ) {
			throw p_err;
		}
		catch( final Throwable p_th ) {
			p_th.printStackTrace();
			final StringWriter outWriter = new StringWriter();
			final PrintWriter printWriter = new PrintWriter( outWriter );
			p_th.printStackTrace( printWriter );
			
			fail( outWriter.getBuffer().toString() );
		}
	}
	
	protected Resource[] prepare( 	final String p_fileName,
									final ResourceSet p_resSet ) 
	throws IOException, TransformationException {
		final String testFilesDir = getTestFilesDir();
		final URI initAdeleResUri = URI.createURI( ROOT_SYNCH_DIR + testFilesDir + p_fileName + "." + AdeleCommonUtils.ADELE_EXT );
		final URI tempAdeleResUri = URI.createURI( ROOT_TEMP_FILES_DIR + p_fileName + "." + AdeleCommonUtils.ADELE_EXT );

		final URI initAadlResUri = initAdeleResUri.trimFileExtension().appendFileExtension( AADL_EXT );
		final URI tempAadlResUri = tempAdeleResUri.trimFileExtension().appendFileExtension( AADL_EXT );

		// Copy Adele
		copyResource( initAdeleResUri, tempAdeleResUri );

		// Copy AADL
		copyResource( initAadlResUri, tempAadlResUri );

		final Resource tempAdeleRes = getResource( tempAdeleResUri, p_resSet );
		final Resource tempAadlRes = getResource( tempAadlResUri, p_resSet );


		// Mapping between temp Adele and temp AADL
		final TransformationExecutionResult result = moteSynchRelation.checkConsistency( tempAdeleRes, tempAadlRes );
		checkConsistency( tempAdeleResUri, tempAadlResUri, result );
		
		return new Resource[] { tempAdeleRes, tempAadlRes };
	}
}
