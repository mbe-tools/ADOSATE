package fr.labsticc.adosate.adele2aadl.tests;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;

public class TestFeaturesTrans extends AbstractTransformationTest {
	
	private static final String TEST_FILES_DIR = ROOT_TRANS_DIR + "feature/";
	
	public TestFeaturesTrans()
	throws CoreException {
		super( true, TEST_FILES_DIR );
	}

	@Test
	public void eventPort2EventPort() {
		testTransformations();
	}

	@Test
	public void eventPort2EventPortRefined() {
		testTransformations();
	}

	@Test
	public void dataPort2DataPort() {
		testTransformations();
	}

	@Test
	public void dataPort2DataPortTyped() {
		testTransformations();
	}

	@Test
	public void dataPort2DataPortRefined() {
		testTransformations();
	}

	@Test
	public void dataPort2DataPortTypedRefined() {
		testTransformations();
	}

	@Test
	public void access2Access() {
		testTransformations();
	}

	@Test
	public void access2AccessRefined() {
		testTransformations();
	}

	@Test
	public void access2AccessTyped() {
		testTransformations();
	}

	@Test
	public void access2AccessTypedRefined() {
		testTransformations();
	}
	
	@Test
	public void featureGroup2FeatureGroup() {
		testTransformations();
	}
	
	@Test
	public void parameter2Parameter() {
		testTransformations();
	}
	
	@Test
	public void abstractFeature2AbstractFeature() {
		testTransformations();
	}
}
