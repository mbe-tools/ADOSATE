package fr.labsticc.adosate.adele2aadl.tests;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;

public class TestComponentTypesSynch extends AbstractSynchronizationTest {
	
	public TestComponentTypesSynch()
	throws CoreException {
		super( true, "component_type/" );
	}
	
	@Override
	public void setUp()
	throws Exception {
		prepare( "component_type_add_thread", createResourceSet() );
		
		super.setUp();
	}

	@Test
	public void component_type_add_thread() {
		testSynchronization();
	}

	@Test
	public void component_type_add_extends() {
		testSynchronization();
	}

	@Test
	public void component_type_add_extends_external() {
		testSynchronization();
	}

	@Test
	public void component_type_remove_extends() {
		testSynchronization();
	}

	@Test
	public void component_type_remove_extends_external() {
		testSynchronization();
	}

	@Test
	public void component_type_change_extends() {
		testSynchronization();
	}
}
