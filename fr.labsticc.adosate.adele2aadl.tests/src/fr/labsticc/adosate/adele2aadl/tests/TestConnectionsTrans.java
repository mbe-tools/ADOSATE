package fr.labsticc.adosate.adele2aadl.tests;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;

public class TestConnectionsTrans extends AbstractTransformationTest {
	
	private static final String TEST_FILES_DIR = ROOT_TRANS_DIR + "connection/";
	
	public TestConnectionsTrans()
	throws CoreException {
		super( true, TEST_FILES_DIR );
	}

	@Test
	public void eventDataPortConn2PortConnSrcSubcompoDestParent() {
		testTransformations();
	}
	
	@Test
	public void eventDataPortConn2PortConnSrcParentDestSubcompo() {
		testTransformations();
	}
	
	@Test
	public void eventDataPortConn2PortConnSrcSubcompoDestSubcompo() {
		testTransformations();
	}
	
	@Test
	public void dataAccessConn2AccessConnSrcParentDestSubcompo() {
		testTransformations();
	}
	
	@Test
	public void dataAccessConn2AccessConnSrcSubcompoDestParent() {
		testTransformations();
	}
	
	@Test
	public void dataAccessConn2AccessConnSrcSubcompoDestSubcompo() {
		testTransformations();
	}
	
	@Test
	public void dataAccessConn2AccessConnDataSubcompo() {
		testTransformations( true, true );
	}
		
	@Test
	public void dataPortConn2PortConn() {
		testTransformations( true, true );
	}
	
	@Test
	public void featureGroupConn2FeatureGroupConn() {
		testTransformations( true, true );
	}
}
