package fr.labsticc.adosate.adele2aadl.tests;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.xtext.resource.SynchronizedXtextResourceSet;
import org.junit.After;
import org.junit.Before;
import org.osate.aadl2.PropertySet;
import org.osate.workspace.WorkspacePlugin;
import org.topcased.adele.common.utils.AdeleCommonUtils;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;
import org.topcased.adele.model.KernelSpices.KernelSpicesPackage;

import de.hpi.sam.mote.MoteFactory;
import de.hpi.sam.mote.TGGEngine;
import de.hpi.sam.mote.impl.MoteEngineRelationPolicyImpl;
import de.hpi.sam.mote.impl.TransformationException;
import de.hpi.sam.mote.rules.TransformationExecutionResult;
import fr.labsticc.adosate.gmm.osategmm.OsategmmFactory;
import fr.labsticc.gmm.ide.GmmPlugin;
import fr.labsticc.gmm.merge.service.GmmMergeUtil;
import fr.labsticc.gmm.merge.service.IGmmDifference;
import fr.labsticc.gmm.merge.service.IGmmMergeService;
import fr.labsticc.gmm.model.megamodel.ModelElementsCoveragePolicy;
import fr.openpeople.models.aadl.constants.AadlExtensionHelper;

public class AbstractAdele2AadlTest {
	
	private static final EStructuralFeature[] ORDER_AGNOSTIC_FEATURES = { 
		KernelSpicesPackage.Literals.SK_HIERARCHICAL_OBJECT__CHILDREN,
		KernelSpicesPackage.Literals.SK_COMPONENT__FEATURES,
		KernelSpicesPackage.Literals.SK_RELATION__OBJECTS,
		ADELE_ComponentsPackage.Literals.COMPONENT__ABSTRACT_FEATURE_CONNECTION,
		ADELE_ComponentsPackage.Literals.COMPONENT__BUS_ACCESS_CONNECTION,
		ADELE_ComponentsPackage.Literals.COMPONENT__DATA_ACCESS_CONNECTION,
		ADELE_ComponentsPackage.Literals.COMPONENT__DATA_PORT_CONNECTION,
		ADELE_ComponentsPackage.Literals.COMPONENT__EVENT_DATA_PORT_CONNECTION,
		ADELE_ComponentsPackage.Literals.COMPONENT__EVENT_PORT_CONNECTION,
		ADELE_ComponentsPackage.Literals.COMPONENT__FEATURE_GROUP_CONNECTION,
		ADELE_ComponentsPackage.Literals.COMPONENT__PARAMETER_CONNECTION,
		ADELE_ComponentsPackage.Literals.COMPONENT__PARAMETER_CONNECTION,
		ADELE_ComponentsPackage.Literals.COMPONENT__SUBPROGRAM_ACCESS_CONNECTION,
		ADELE_ComponentsPackage.Literals.COMPONENT__SUBPROGRAM_GROUP_ACCESS_CONNECTION };
	
	protected static final String LINE_SEP = System.getProperty( "line.separator" );
	protected static final String PROJECT_NAME = "fr.labsticc.adosate.adele2aadl.tests.resources";
	protected static final String ROOT_TEST_FILES_DIR = "platform:/resource/" + PROJECT_NAME + "/";

	protected static final String AADL_EXT = WorkspacePlugin.SOURCE_FILE_EXT;
	
	protected static final String ADELE_2_AADL_RULESET_ID = "adele2aadl.ruleSetID";
	
	protected static final String FORWARD =  "forward";
	protected static final String REVERSE =  "reverse";
	
	private final TGGEngine tggEngine;

	private final boolean deletingResultsWhenPasses;

	private final String testFilesDir;

	private final ModelElementsCoveragePolicy coveragePolicy;
	
	private ILabelProvider labelProvider;
	
	private final Collection<EStructuralFeature> orderAgnosticFeatures;

	private final IGmmMergeService mergeService;
	
	protected final double nameSimilarityWeight = 1.0d;

	protected final double positionSimilarityWeight = 0.0d;
	
	private class MoteEngRelationPolicyTest extends MoteEngineRelationPolicyImpl {
		
		@Override
		public URI correspondingUri( final Resource p_resource ) {
			if ( p_resource.getContents().get( 0 ) instanceof PropertySet ) {
				return null;
			}
			
			URI resUri = p_resource.getURI();
			final String resName = resUri.lastSegment();
			
			if ( resName.startsWith( REVERSE ) ) {
				resUri = resUri.trimSegments( 1 ).appendSegment( FORWARD ).appendFileExtension( resUri.fileExtension() );
			}
			
			if ( resName.startsWith( FORWARD ) ) {
				resUri = resUri.trimSegments( 1 ).appendSegment( REVERSE ).appendFileExtension( resUri.fileExtension() );
			}
			
			if ( AadlExtensionHelper.isAadlResource( resUri ) ) {
				return resUri.trimFileExtension().appendFileExtension( AdeleCommonUtils.ADELE_EXT );
			}
			
			if ( AdeleCommonUtils.isAdeleResource( resUri ) ) {
				return resUri.trimFileExtension().appendFileExtension( AADL_EXT );
			}
			
			return null;
		}
	}

	protected AbstractAdele2AadlTest( 	final boolean pb_deletingResultsWhenPassed,
										final String p_testFilesDir ) 
	throws CoreException {
		deletingResultsWhenPasses = pb_deletingResultsWhenPassed;
		testFilesDir = p_testFilesDir;
		tggEngine = MoteFactory.eINSTANCE.createTGGEngine();
		tggEngine.initializeEngine();
		tggEngine.getRelationPolicies().put( ADELE_2_AADL_RULESET_ID, new MoteEngRelationPolicyTest() );
		coveragePolicy = OsategmmFactory.eINSTANCE.createAdeleOsateModelElementsCovPolicy();
		orderAgnosticFeatures = Arrays.asList( ORDER_AGNOSTIC_FEATURES );
		
		mergeService = GmmMergeUtil.getMergeService();
		GmmPlugin.getDefault().getGmmManager().setEnabled( false );
	}

	@Before
	public void setUp()
	throws Exception {
		refreshTestProject();
	}
	
	protected void buildTestProject() 
	throws CoreException {
		final IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject( PROJECT_NAME );
		project.build( IncrementalProjectBuilder.FULL_BUILD , null );
	}

	@After
	public void refresh()
	throws Exception {
		refreshTestProject();
	}
	
	private void refreshTestProject() 
	throws CoreException {
		
		// Refresh the test resources project
		final IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject( PROJECT_NAME );
		project.refreshLocal( IResource.DEPTH_INFINITE, null );
	}

	protected String getTestFilesDir() {
		return testFilesDir;
	}

	protected boolean isDeletingResultsWhenPasses() {
		return deletingResultsWhenPasses;
	}
//
//	protected void setDeletingResultsWhenPasses( final boolean pb_deletingResultsWhenPasses ) {
//		deletingResultsWhenPasses = pb_deletingResultsWhenPasses;
//	}
	
	protected void checkConsistency(	final URI p_leftFileUri, 
										final URI p_rightFileUri ) 
	throws TransformationException {
		System.out.println( "Checking consistency between " + p_leftFileUri + " and " + p_rightFileUri );
		final ResourceSet resourceSet = createResourceSet();

		final Resource leftResource = resourceSet.getResource( p_leftFileUri, true );
		final Resource rightResource = resourceSet.getResource( p_rightFileUri, true );
		final TransformationExecutionResult result = tggEngine.checkConsistency( leftResource, rightResource, new BasicEMap<Resource, EList<EObject>>(), ADELE_2_AADL_RULESET_ID );
		checkConsistency( p_leftFileUri, p_rightFileUri, result );
	}
	
	protected void checkConsistency( 	final URI p_leftFileUri, 
										final URI p_rightFileUri,
										final TransformationExecutionResult p_execTransRes ) { 
		final List<EObject> uncoveredLeftElements = new ArrayList<EObject>();
		final List<EObject> uncoveredRightElements = new ArrayList<EObject>();
		
		for ( final EObject uncovElem : p_execTransRes.getLeftUncoveredElements() ) {
			if ( coveragePolicy.isConsidered( uncovElem ) ) {
				uncoveredLeftElements.add( uncovElem );
			}
		}
		
		for ( final EObject uncovElem : p_execTransRes.getRightUncoveredElements() ) {
			if ( coveragePolicy.isConsidered( uncovElem ) ) {
				uncoveredRightElements.add( uncovElem );
			}
		}

		if ( !uncoveredLeftElements.isEmpty() || !uncoveredRightElements.isEmpty() ) {
			final StringBuilder consistencyMessage = new StringBuilder( "Model " );
			consistencyMessage.append( p_leftFileUri );
			consistencyMessage.append( " is not consistent with model " );
			consistencyMessage.append( p_rightFileUri + "." + LINE_SEP );
			consistencyMessage.append( "Inconsistent elements in model " + p_leftFileUri + ":" );
			
			for ( final EObject eObject : uncoveredLeftElements ) {
				consistencyMessage.append( LINE_SEP + getText( eObject ) );
			}
	
			consistencyMessage.append( LINE_SEP + "Inconsistent elements in model " + p_rightFileUri + ":" );
			
			for ( final EObject eObject : uncoveredRightElements ) {
				consistencyMessage.append( LINE_SEP + getText( eObject ) );
			}
			
			fail( consistencyMessage.toString() );
		}
	}
	
	private String getText( final Object p_object ) {
		if ( labelProvider == null ) {
			final ComposedAdapterFactory composedAdapterFactory = new ComposedAdapterFactory( ComposedAdapterFactory.Descriptor.Registry.INSTANCE );
			labelProvider =  new AdapterFactoryLabelProvider( composedAdapterFactory );
		}
		
		return labelProvider.getText( p_object );
	}

	protected Resource getResource( final URI p_uri ) {
		final ResourceSet resSet = createResourceSet();
		
		if ( resSet.getURIConverter().exists( p_uri, null ) ) {
			return resSet.getResource( p_uri, true );
		}
		
		return null;
	}

	protected Resource getResource( final URI p_uri,
									final ResourceSet p_resSet ) {
		if ( p_resSet.getURIConverter().exists( p_uri, null ) ) {
			return p_resSet.getResource( p_uri, true );
		}
		
		return null;
	}

	protected boolean deleteResourceIfExists( final URI p_uri ) 
	throws IOException {
		final Resource resource = getResource( p_uri );
		
		if ( resource == null ) {
			return false;
		}
			
		resource.delete( null );
		
		return true;
	}
	
	private String toString( final Collection<IGmmDifference> p_differences ) {
		final StringBuilder strBuild = new StringBuilder( "Differences are:" + LINE_SEP );
		
		for ( final IGmmDifference element : p_differences ) {
			strBuild.append( element + LINE_SEP );
		}
		
		return strBuild.toString();
	}
	
	protected void check( final Collection<IGmmDifference> p_differences ) {
		if ( !p_differences.isEmpty() ) {
			fail( toString( p_differences ) );
		}
	}
	
	protected Collection<IGmmDifference> differences( 	final Resource p_result,
														final Resource p_expectedResult ) {
		final Collection<EStructuralFeature> ignoredReferences = new ArrayList<EStructuralFeature>(); 
		final boolean useIds;

		if ( AdeleCommonUtils.isAdeleResource( p_result ) ) {
			ignoredReferences.add( KernelSpicesPackage.Literals.SK_OBJECT__ID );
			useIds = false;
		}
		else {
			useIds = true;
		}
		
		return mergeService.differences( 	p_result,
											p_expectedResult,
											useIds,
											false,
											nameSimilarityWeight,
											positionSimilarityWeight,
											orderAgnosticFeatures,
											ignoredReferences );
	}
	
	protected TGGEngine getTggEngine() {
		return tggEngine;
	}

	protected IGmmMergeService getMergeService() {
		return mergeService;
	}
	
	protected ResourceSet createResourceSet() {
		return new SynchronizedXtextResourceSet();
	}
}
