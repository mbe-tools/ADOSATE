package fr.labsticc.adosate.ide.property;

import java.util.Collection;

import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.PageBook;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.linking.ILinker;
import org.eclipse.xtext.serializer.ISerializer;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.PropertySet;
import org.osate.aadl2.modelsupport.resources.OsateResourceUtil;
import org.osate.xtext.aadl2.parser.antlr.Aadl2Parser;
//import org.osate.xtext.aadl2.ui.propertyview.PropertyViewModel;
//import org.osate.xtext.aadl2.ui.propertyview.associationwizard.PropertyAssociationWizard;
import org.topcased.adele.common.properties.AdelePropertyPanel;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Features.Feature;
import org.topcased.adele.model.ADELE_Relations.Relation;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.modeler.edit.DiagramEditPart;
import org.topcased.modeler.edit.IModelElementEditPart;

import com.google.inject.Inject;

import fr.labsticc.adosate.gmm.osategmm.AdeleOsateSynchronizationRelation;
import fr.labsticc.framework.core.exception.ResourceAccessException;
import fr.labsticc.framework.emf.core.util.EMFUtil;
import fr.labsticc.gmm.ide.GmmManager;
import fr.labsticc.gmm.ide.GmmPlugin;
import fr.labsticc.gmm.model.megamodel.ObligationRelation;

public class AadlPropertiesPanel extends AdelePropertyPanel {

	private static final String SHOW_UNDEFINED_TRUE_TOOL_TIP = "Click to hide undefined properties";
	private static final String SHOW_UNDEFINED_FALSE_TOOL_TIP = "Click to show undefined properties";
	private static final String NO_PROPERTIES_TO_SHOW =	"No properties to show: Please select a single object that is an AADL Property Holder.";
	
	private final GmmManager gmmManager;

	public AadlPropertiesPanel(	final Composite p_parent ) {
		super( p_parent );
		
		gmmManager = GmmPlugin.getDefault().getGmmManager();
	}
	
	/**
	 * Page book for switching between the tree viewer and the "no properties"
	 * message.
	 */
	private PageBook pageBook;
	
	/**
	 * Tree for displaying properties. Underlying model is a
	 * {@link java.util.List} of {@link PropertySet} objects.
	 */
	private TreeViewer treeViewer;
	
	/**
	 * The label for the no results message.
	 */
	private Label noPropertiesLabel;
	
	/**
	 * Action for toggling the display of nonexistent properties
	 */
	private Action showUndefinedAction = null;
	
	/**
	 * Action for creating a new Property Association without using any information
	 * from this view's selection
	 */
	private Action addNewPropertyAssociationToolbarAction = null;
	
	/**
	 * Model
	 */
	//private PropertyViewModel model = null;
	
	/**
	 * The currently viewed model element
	 */
	/*
	 * This variable is used to carry the current selection across reloads
	 */
	protected URI currentSelectionUri = null;
	
	/**
	 * The editing domain for {@link #getCurrentElement()}.
	 */
	private EditingDomain editingDomain = null;
	
	@Inject
	private ISerializer serializer;
	
	@Inject
	private Aadl2Parser aadl2Parser;
	
	@Inject
	private ILinker linker;
	
	public void createControls( final IToolBarManager p_toolBarManager ) {
		pageBook = new PageBook( this, SWT.NULL);
		
		noPropertiesLabel = new Label(pageBook, SWT.LEFT);
		noPropertiesLabel.setText(NO_PROPERTIES_TO_SHOW);
		noPropertiesLabel.setAlignment(SWT.CENTER);
		noPropertiesLabel.setBackground( getDisplay().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
		
//		model = new PropertyViewModel(serializer);
//		model.setShowUndefined(false);
		treeViewer = new TreeViewer(pageBook, SWT.H_SCROLL | SWT.V_SCROLL);
		Tree tree = treeViewer.getTree();
		TableLayout tableLayout = new TableLayout();
		tree.setLayout(tableLayout);
		tree.setLinesVisible(true);
		tree.setHeaderVisible(true);
		new TreeColumn(tree, SWT.LEFT).setText("Property Association");
		tableLayout.addColumnData(new ColumnWeightData(1, true));
		new TreeColumn(tree, SWT.LEFT).setText("Status of the Association");
		tableLayout.addColumnData(new ColumnWeightData(1, true));
//		treeViewer.setLabelProvider(model);
//		treeViewer.setContentProvider(model);
//		treeViewer.setInput(model.getInput());
		
		// Show the "nothing to show" page by default
		pageBook.showPage(noPropertiesLabel);
		createActions();
		fillToolbar( p_toolBarManager );
//		createContextMenu();
	}
	
	private void showTree() {
		pageBook.showPage(treeViewer.getControl());
	}
	
	private void showNoProperties() {
		pageBook.showPage(noPropertiesLabel);
	}
	
	private ImageDescriptor getImageDescriptor( final String p_path ) {
		return AbstractUIPlugin.imageDescriptorFromPlugin( "org.osate.xtext.aadl2.ui", p_path );
	}
	
	private void createActions() {
		showUndefinedAction = new Action() {
			@Override
			public void run() {
			//	model.toggleShowUndefined();
				updateActionStates();
				buildNewModel(getCurrentElement());
			}
		};
		showUndefinedAction.setImageDescriptor(  getImageDescriptor( "icons/propertyview/nonexistent_property.gif" ) );
		
		addNewPropertyAssociationToolbarAction = new Action() {
			@Override
			public void run() {
				
				// WOrkaround: refresh the resource which may have been modified by synchronization layer.
//				final Resource resource = OsateResourceUtil.getResource( currentSelectionUri.trimFragment() );
//				
//				if ( resource != null ) {
//					resource.unload();
//				}
				
//				PropertyAssociationWizard wizard = new PropertyAssociationWizard( null, getCommandStack(), currentSelectionUri, serializer, aadl2Parser, linker);
//				WizardDialog dialog = new WizardDialog( getShell(), wizard );
//				if (dialog.open() == Window.OK)
//					updateView();
			}
		};

		addNewPropertyAssociationToolbarAction.setToolTipText("New Property Association");
		addNewPropertyAssociationToolbarAction.setImageDescriptor( getImageDescriptor("icons/propertyview/new_pa.gif" ) );
		
		updateActionStates();
	}
	
	private void updateActionStates() {
//		final boolean flag = model.getShowUndefined();
//		showUndefinedAction.setChecked(flag);
//		showUndefinedAction.setToolTipText(flag ? SHOW_UNDEFINED_TRUE_TOOL_TIP : SHOW_UNDEFINED_FALSE_TOOL_TIP);
	}
	
	private void fillToolbar( final IToolBarManager p_manager ) {
		p_manager.add(showUndefinedAction);
		p_manager.add(addNewPropertyAssociationToolbarAction);
	}

	@Override
	public boolean setFocus() {
		return super.setFocus() && treeViewer.getControl().setFocus();
	}
	
	private NamedElement getCurrentElement() {
		NamedElement element = null;
		
		if (currentSelectionUri != null) {
			try {
				// This allows to use the resource from the editor if opened
				final Resource resource = gmmManager.getResource( currentSelectionUri.trimFragment() );
				element = (NamedElement) resource.getEObject( currentSelectionUri.fragment() ); 
			}
			catch( final ResourceAccessException p_ex ) {
				p_ex.printStackTrace();
			}
		}
		return element;
	}
	
	/**
	 * Update the view's contents.
	 */
	private void updateView() {
		if (currentSelectionUri != null) {
			buildNewModel(getCurrentElement());
			showTree();
			addNewPropertyAssociationToolbarAction.setEnabled( !EMFUtil.isReadOnly( currentSelectionUri.trimFragment(), ExtensibleURIConverterImpl.INSTANCE ) );
		}
		else {
			showNoProperties();
			addNewPropertyAssociationToolbarAction.setEnabled(false);
		}
	}
	
	private void buildNewModel(NamedElement element) {
							// DB: avoid exceptions when diagram are created automatically from GMM relation.
		if (element != null && !treeViewer.getControl().isDisposed() ) {
//			model.rebuildModel(element);
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					treeViewer.refresh();
					treeViewer.expandAll();
				}
			});
		}
	}
	
	private CommandStack getCommandStack() {
		return (editingDomain == null) ? null : editingDomain.getCommandStack();
	}
	
	@Override
	public void setInput(	final IWorkbenchPart p_part,
							final ISelection p_selection ) {
		updateSelection( p_part, p_selection );
	}
	
	private void updateSelection( 	final IWorkbenchPart part,
									final ISelection selection ) {
		final Collection<IModelElementEditPart> selElements = EMFUtil.selectedObjects( selection, IModelElementEditPart.class );
		final EObject currentSelection;
		
		if ( selElements.size() == 1 ) {
			final EObject semObject = selElements.iterator().next().getEObject();
			currentSelection = getAadlNamedElement( semObject );
		}
		else {
			final Collection<DiagramEditPart> diagramSelElements = EMFUtil.selectedObjects( selection, DiagramEditPart.class );

			if ( diagramSelElements.size() == 1 ) {
				final EObject semObject = diagramSelElements.iterator().next().getEObject();
				currentSelection = getAadlNamedElement( semObject );
			}
			else {
				currentSelection = null;
			}
		}
		
		if ( currentSelection instanceof NamedElement) {
			if (currentSelection.eResource() != null) {
				currentSelectionUri = EcoreUtil.getURI(currentSelection);
				editingDomain = AdapterFactoryEditingDomain.getEditingDomainFor(currentSelection);
			}
			else {
				currentSelectionUri = null;
				editingDomain = null;
			}
		}
		else {
			currentSelectionUri = null;
			editingDomain = null;
		}
		
		// Update the view page
		updateView();
	}
	
	private NamedElement getAadlNamedElement( final EObject p_modelElement ) {
		if ( p_modelElement instanceof SKHierarchicalObject ) {
			SKHierarchicalObject realObject = null;
			
			if ( p_modelElement instanceof Feature ) {
				realObject = ( (Feature) p_modelElement ).getOriginalFeature();
			}
			else if ( p_modelElement instanceof Component ) {
				realObject = ( (Component) p_modelElement ).getOriginalComponent();
			}
			else if ( p_modelElement instanceof Relation ) {
				realObject = ( (Relation) p_modelElement ).getOriginalRelation();
			}
			
			if ( realObject == null ) {
				realObject = (SKHierarchicalObject) p_modelElement;
			}

			for ( final ObligationRelation relation : gmmManager.getMegaModel().getOwnedObligationRelations() ) {
				if ( relation instanceof AdeleOsateSynchronizationRelation ) {
					final AdeleOsateSynchronizationRelation adeleOsateRel = (AdeleOsateSynchronizationRelation) relation;
					
					return (NamedElement) adeleOsateRel.aadlElement( realObject );
				}
			}
		}
		
		return null;
	}

	@Override
	public void refresh() {
		updateView();
	}
}
