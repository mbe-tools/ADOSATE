package fr.labsticc.adosate.ide.property;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.topcased.adele.common.properties.AdelePropertyPanel;
import org.topcased.adele.common.properties.AdelePropertySection;

public class AadlPropertiesPropertySection extends AdelePropertySection {
	
	@Override
	public AdelePropertyPanel createPropertyPanel(	final Composite p_parent,
													final IActionBars p_actionBars ) {
		final AadlPropertiesPanel aadlPropPanel = new AadlPropertiesPanel( p_parent );
		//OsateCorePlugin.getDefault().getInjector( Aadl2Activator.ORG_OSATE_XTEXT_AADL2_AADL2 ).injectMembers( aadlPropPanel );
		aadlPropPanel.createControls( p_actionBars.getToolBarManager() );

		return aadlPropPanel;
	}
}
