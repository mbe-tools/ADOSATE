<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_lxhRwJb6EeK3aPj-rc4BkQ" name="generateOperationalRules">
  <components xsi:type="workflow.components:WorkflowDelegation" xmi:id="_lxhRwZb6EeK3aPj-rc4BkQ" name="workflowDelegation" workflowURI="platform:/plugin/de.hpi.sam.tgg.operationalRulesGenerator/generatorResources/generator.workflow">
    <propertyValues xmi:id="_lxhRwpb6EeK3aPj-rc4BkQ" name="tggFileURI" defaultValue="adele2aadl.tgg"/>
    <propertyValues xmi:id="_lxhRw5b6EeK3aPj-rc4BkQ" name="correspondenceMetamodelURI" defaultValue="adele2aadl.ecore"/>
    <propertyValues xmi:id="_lxhRxJb6EeK3aPj-rc4BkQ" name="projectName" defaultValue="fr.labsticc.adosate.adele2aadl"/>
    <propertyValues xmi:id="_lxhRxZb6EeK3aPj-rc4BkQ" name="javaBasePackage" defaultValue="fr.labsticc.adosate"/>
    <propertyValues xmi:id="_rdGUgJb6EeK3aPj-rc4BkQ" name="generationStrategy" defaultValue="SynchronizationStrategy"/>
  </components>
</workflow:Workflow>
