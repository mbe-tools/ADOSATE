<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:operationalRulesGenerator.workflowComponents="http://operationalRulesGenerator/workflowComponents/1.0" xmlns:workflow="http://mdelab/workflow/1.0" xmlns:workflow.components="http://mdelab/workflow/components/1.0" xmi:id="_kJ5MMOWUEeKkvaFG_cMCxQ" name="Rule Generator Workflow" description="This workflow generates the operational rules plugin from a TGG file.">
  <components xsi:type="workflow.components:DirectoryCleaner" xmi:id="_kKV4IOWUEeKkvaFG_cMCxQ" name="directoryCleaner">
    <directoryURIs>../model/story</directoryURIs>
  </components>
  <components xsi:type="operationalRulesGenerator.workflowComponents:ManifestLoaderComponent" xmi:id="_kOgmcOWUEeKkvaFG_cMCxQ" name="manifestLoaderComponent" modelSlot="manifest" projectName="${projectName}"/>
  <components xsi:type="operationalRulesGenerator.workflowComponents:GenerationStrategyComponent" xmi:id="_kOqXcOWUEeKkvaFG_cMCxQ" name="generationStrategyComponent" tggFileURI="${tggFileURI}" projectName="${projectName}" correspondenceMetamodelURI="${correspondenceMetamodelURI}" javaBasePackage="${javaBasePackage}" generationStrategy="${generationStrategy}"/>
  <properties xmi:id="_tR_ZIOWUEeKkvaFG_cMCxQ" name="tggFileURI" defaultValue="adele2aadl.tgg"/>
  <properties xmi:id="_tR_ZIeWUEeKkvaFG_cMCxQ" name="correspondenceMetamodelURI" defaultValue="adele2aadl.ecore"/>
  <properties xmi:id="_tR_ZIuWUEeKkvaFG_cMCxQ" name="projectName" defaultValue="fr.labsticc.adosate.adele2aadl"/>
  <properties xmi:id="_tR_ZI-WUEeKkvaFG_cMCxQ" name="javaBasePackage" defaultValue="fr.labsticc.adosate"/>
  <properties xmi:id="_tR_ZJOWUEeKkvaFG_cMCxQ" name="generationStrategy" defaultValue="SynchronizationStrategy"/>
</workflow:Workflow>
