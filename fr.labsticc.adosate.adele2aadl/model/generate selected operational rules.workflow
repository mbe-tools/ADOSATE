<?xml version="1.0" encoding="UTF-8"?>
<workflow:Workflow xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:operationalRulesGenerator.workflowComponents="http://operationalRulesGenerator/workflowComponents/1.0" xmlns:workflow="http://mdelab/workflow/1.0" xmi:id="_lxhRwJb6EeK3aPj-rc4BkQ" name="generateOperationalRules">
  <components xsi:type="operationalRulesGenerator.workflowComponents:ManifestLoaderComponent" xmi:id="_1je3sePzEeK_87fyLbPDEg" name="manifestLoaderComponent" modelSlot="manifest" projectName="${projectName}"/>
  <components xsi:type="operationalRulesGenerator.workflowComponents:GenerationStrategyComponent" xmi:id="_1je3suPzEeK_87fyLbPDEg" name="generationStrategyComponent" tggFileURI="${tggFileURI}" projectName="${projectName}" correspondenceMetamodelURI="${correspondenceMetamodelURI}" javaBasePackage="${javaBasePackage}" generationStrategy="${generationStrategy}">
    <rulesToGenerate href="adele2aadl.tgg#_pnQ8_NcqEeKLO_ZZ8518LQ"/>
    <rulesToGenerate href="adele2aadl.tgg#_pnQ9qNcqEeKLO_ZZ8518LQ"/>
  </components>
  <properties xmi:id="_lxhRwpb6EeK3aPj-rc4BkQ" name="tggFileURI" defaultValue="adele2aadl.tgg"/>
  <properties xmi:id="_lxhRw5b6EeK3aPj-rc4BkQ" name="correspondenceMetamodelURI" defaultValue="adele2aadl.ecore"/>
  <properties xmi:id="_lxhRxJb6EeK3aPj-rc4BkQ" name="projectName" defaultValue="fr.labsticc.adosate.adele2aadl"/>
  <properties xmi:id="_lxhRxZb6EeK3aPj-rc4BkQ" name="javaBasePackage" defaultValue="fr.labsticc.adosate"/>
  <properties xmi:id="_rdGUgJb6EeK3aPj-rc4BkQ" name="generationStrategy" defaultValue="SynchronizationStrategy"/>
</workflow:Workflow>
