/**
 */
package fr.labsticc.adosate.adele2aadl;

import de.hpi.sam.mote.TGGNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Subcomponent</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlPackage#getCorrSubcomponent()
 * @model
 * @generated
 */
public interface CorrSubcomponent extends TGGNode {
} // CorrSubcomponent
