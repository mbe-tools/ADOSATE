/**
 */
package fr.labsticc.adosate.adele2aadl;

import de.hpi.sam.mote.MotePackage;

import de.hpi.sam.mote.rules.RulesPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlFactory
 * @model kind="package"
 * @generated
 */
public interface Adele2aadlPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "adele2aadl";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.labsticc.fr/adele2aadl/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "adele2aadl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Adele2aadlPackage eINSTANCE = fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.CorrAxiomImpl <em>Corr Axiom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.CorrAxiomImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getCorrAxiom()
	 * @generated
	 */
	int CORR_AXIOM = 0;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_AXIOM__NEXT = MotePackage.TGG_NODE__NEXT;

	/**
	 * The feature id for the '<em><b>Previous</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_AXIOM__PREVIOUS = MotePackage.TGG_NODE__PREVIOUS;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_AXIOM__RULE_SET = MotePackage.TGG_NODE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Creation Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_AXIOM__CREATION_RULE = MotePackage.TGG_NODE__CREATION_RULE;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_AXIOM__SOURCES = MotePackage.TGG_NODE__SOURCES;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_AXIOM__TARGETS = MotePackage.TGG_NODE__TARGETS;

	/**
	 * The number of structural features of the '<em>Corr Axiom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_AXIOM_FEATURE_COUNT = MotePackage.TGG_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.CorrComponentImpl <em>Corr Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.CorrComponentImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getCorrComponent()
	 * @generated
	 */
	int CORR_COMPONENT = 1;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT__NEXT = MotePackage.TGG_NODE__NEXT;

	/**
	 * The feature id for the '<em><b>Previous</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT__PREVIOUS = MotePackage.TGG_NODE__PREVIOUS;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT__RULE_SET = MotePackage.TGG_NODE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Creation Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT__CREATION_RULE = MotePackage.TGG_NODE__CREATION_RULE;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT__SOURCES = MotePackage.TGG_NODE__SOURCES;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT__TARGETS = MotePackage.TGG_NODE__TARGETS;

	/**
	 * The number of structural features of the '<em>Corr Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT_FEATURE_COUNT = MotePackage.TGG_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.CorrComponentTypeImpl <em>Corr Component Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.CorrComponentTypeImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getCorrComponentType()
	 * @generated
	 */
	int CORR_COMPONENT_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT_TYPE__NEXT = CORR_COMPONENT__NEXT;

	/**
	 * The feature id for the '<em><b>Previous</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT_TYPE__PREVIOUS = CORR_COMPONENT__PREVIOUS;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT_TYPE__RULE_SET = CORR_COMPONENT__RULE_SET;

	/**
	 * The feature id for the '<em><b>Creation Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT_TYPE__CREATION_RULE = CORR_COMPONENT__CREATION_RULE;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT_TYPE__SOURCES = CORR_COMPONENT__SOURCES;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT_TYPE__TARGETS = CORR_COMPONENT__TARGETS;

	/**
	 * The number of structural features of the '<em>Corr Component Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT_TYPE_FEATURE_COUNT = CORR_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.CorrComponentImplementationImpl <em>Corr Component Implementation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.CorrComponentImplementationImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getCorrComponentImplementation()
	 * @generated
	 */
	int CORR_COMPONENT_IMPLEMENTATION = 3;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT_IMPLEMENTATION__NEXT = CORR_COMPONENT__NEXT;

	/**
	 * The feature id for the '<em><b>Previous</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT_IMPLEMENTATION__PREVIOUS = CORR_COMPONENT__PREVIOUS;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT_IMPLEMENTATION__RULE_SET = CORR_COMPONENT__RULE_SET;

	/**
	 * The feature id for the '<em><b>Creation Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT_IMPLEMENTATION__CREATION_RULE = CORR_COMPONENT__CREATION_RULE;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT_IMPLEMENTATION__SOURCES = CORR_COMPONENT__SOURCES;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT_IMPLEMENTATION__TARGETS = CORR_COMPONENT__TARGETS;

	/**
	 * The number of structural features of the '<em>Corr Component Implementation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_COMPONENT_IMPLEMENTATION_FEATURE_COUNT = CORR_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.CorrSubcomponentImpl <em>Corr Subcomponent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.CorrSubcomponentImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getCorrSubcomponent()
	 * @generated
	 */
	int CORR_SUBCOMPONENT = 4;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_SUBCOMPONENT__NEXT = MotePackage.TGG_NODE__NEXT;

	/**
	 * The feature id for the '<em><b>Previous</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_SUBCOMPONENT__PREVIOUS = MotePackage.TGG_NODE__PREVIOUS;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_SUBCOMPONENT__RULE_SET = MotePackage.TGG_NODE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Creation Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_SUBCOMPONENT__CREATION_RULE = MotePackage.TGG_NODE__CREATION_RULE;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_SUBCOMPONENT__SOURCES = MotePackage.TGG_NODE__SOURCES;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_SUBCOMPONENT__TARGETS = MotePackage.TGG_NODE__TARGETS;

	/**
	 * The number of structural features of the '<em>Corr Subcomponent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_SUBCOMPONENT_FEATURE_COUNT = MotePackage.TGG_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.CorrFeatureImpl <em>Corr Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.CorrFeatureImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getCorrFeature()
	 * @generated
	 */
	int CORR_FEATURE = 5;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_FEATURE__NEXT = MotePackage.TGG_NODE__NEXT;

	/**
	 * The feature id for the '<em><b>Previous</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_FEATURE__PREVIOUS = MotePackage.TGG_NODE__PREVIOUS;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_FEATURE__RULE_SET = MotePackage.TGG_NODE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Creation Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_FEATURE__CREATION_RULE = MotePackage.TGG_NODE__CREATION_RULE;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_FEATURE__SOURCES = MotePackage.TGG_NODE__SOURCES;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_FEATURE__TARGETS = MotePackage.TGG_NODE__TARGETS;

	/**
	 * The number of structural features of the '<em>Corr Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_FEATURE_FEATURE_COUNT = MotePackage.TGG_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.CorrDirectedFeatureImpl <em>Corr Directed Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.CorrDirectedFeatureImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getCorrDirectedFeature()
	 * @generated
	 */
	int CORR_DIRECTED_FEATURE = 6;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_DIRECTED_FEATURE__NEXT = CORR_FEATURE__NEXT;

	/**
	 * The feature id for the '<em><b>Previous</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_DIRECTED_FEATURE__PREVIOUS = CORR_FEATURE__PREVIOUS;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_DIRECTED_FEATURE__RULE_SET = CORR_FEATURE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Creation Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_DIRECTED_FEATURE__CREATION_RULE = CORR_FEATURE__CREATION_RULE;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_DIRECTED_FEATURE__SOURCES = CORR_FEATURE__SOURCES;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_DIRECTED_FEATURE__TARGETS = CORR_FEATURE__TARGETS;

	/**
	 * The number of structural features of the '<em>Corr Directed Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_DIRECTED_FEATURE_FEATURE_COUNT = CORR_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.CorrAccessImpl <em>Corr Access</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.CorrAccessImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getCorrAccess()
	 * @generated
	 */
	int CORR_ACCESS = 7;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_ACCESS__NEXT = CORR_FEATURE__NEXT;

	/**
	 * The feature id for the '<em><b>Previous</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_ACCESS__PREVIOUS = CORR_FEATURE__PREVIOUS;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_ACCESS__RULE_SET = CORR_FEATURE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Creation Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_ACCESS__CREATION_RULE = CORR_FEATURE__CREATION_RULE;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_ACCESS__SOURCES = CORR_FEATURE__SOURCES;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_ACCESS__TARGETS = CORR_FEATURE__TARGETS;

	/**
	 * The number of structural features of the '<em>Corr Access</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_ACCESS_FEATURE_COUNT = CORR_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.CorrDirectedFeatureConnectionImpl <em>Corr Directed Feature Connection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.CorrDirectedFeatureConnectionImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getCorrDirectedFeatureConnection()
	 * @generated
	 */
	int CORR_DIRECTED_FEATURE_CONNECTION = 8;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_DIRECTED_FEATURE_CONNECTION__NEXT = MotePackage.TGG_NODE__NEXT;

	/**
	 * The feature id for the '<em><b>Previous</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_DIRECTED_FEATURE_CONNECTION__PREVIOUS = MotePackage.TGG_NODE__PREVIOUS;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_DIRECTED_FEATURE_CONNECTION__RULE_SET = MotePackage.TGG_NODE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Creation Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_DIRECTED_FEATURE_CONNECTION__CREATION_RULE = MotePackage.TGG_NODE__CREATION_RULE;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_DIRECTED_FEATURE_CONNECTION__SOURCES = MotePackage.TGG_NODE__SOURCES;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_DIRECTED_FEATURE_CONNECTION__TARGETS = MotePackage.TGG_NODE__TARGETS;

	/**
	 * The number of structural features of the '<em>Corr Directed Feature Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_DIRECTED_FEATURE_CONNECTION_FEATURE_COUNT = MotePackage.TGG_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.CorrAccessConnectionImpl <em>Corr Access Connection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.CorrAccessConnectionImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getCorrAccessConnection()
	 * @generated
	 */
	int CORR_ACCESS_CONNECTION = 9;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_ACCESS_CONNECTION__NEXT = MotePackage.TGG_NODE__NEXT;

	/**
	 * The feature id for the '<em><b>Previous</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_ACCESS_CONNECTION__PREVIOUS = MotePackage.TGG_NODE__PREVIOUS;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_ACCESS_CONNECTION__RULE_SET = MotePackage.TGG_NODE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Creation Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_ACCESS_CONNECTION__CREATION_RULE = MotePackage.TGG_NODE__CREATION_RULE;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_ACCESS_CONNECTION__SOURCES = MotePackage.TGG_NODE__SOURCES;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_ACCESS_CONNECTION__TARGETS = MotePackage.TGG_NODE__TARGETS;

	/**
	 * The number of structural features of the '<em>Corr Access Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORR_ACCESS_CONNECTION_FEATURE_COUNT = MotePackage.TGG_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.Adele2aadlRuleSetImpl <em>Rule Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlRuleSetImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getAdele2aadlRuleSet()
	 * @generated
	 */
	int ADELE2AADL_RULE_SET = 10;

	/**
	 * The feature id for the '<em><b>Root Corr Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__ROOT_CORR_NODE = RulesPackage.TGG_RULE_SET__ROOT_CORR_NODE;

	/**
	 * The feature id for the '<em><b>Axiom</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__AXIOM = RulesPackage.TGG_RULE_SET__AXIOM;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__RULES = RulesPackage.TGG_RULE_SET__RULES;

	/**
	 * The feature id for the '<em><b>Process Notifications</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__PROCESS_NOTIFICATIONS = RulesPackage.TGG_RULE_SET__PROCESS_NOTIFICATIONS;

	/**
	 * The feature id for the '<em><b>Correspondence Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__CORRESPONDENCE_NODES = RulesPackage.TGG_RULE_SET__CORRESPONDENCE_NODES;

	/**
	 * The feature id for the '<em><b>Source Model Elements</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__SOURCE_MODEL_ELEMENTS = RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Target Model Elements</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__TARGET_MODEL_ELEMENTS = RulesPackage.TGG_RULE_SET__TARGET_MODEL_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Uncovered Source Model Elements</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__UNCOVERED_SOURCE_MODEL_ELEMENTS = RulesPackage.TGG_RULE_SET__UNCOVERED_SOURCE_MODEL_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Uncovered Target Model Elements</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__UNCOVERED_TARGET_MODEL_ELEMENTS = RulesPackage.TGG_RULE_SET__UNCOVERED_TARGET_MODEL_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Source Model Elements Adapter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__SOURCE_MODEL_ELEMENTS_ADAPTER = RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ELEMENTS_ADAPTER;

	/**
	 * The feature id for the '<em><b>Target Model Elements Adapter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__TARGET_MODEL_ELEMENTS_ADAPTER = RulesPackage.TGG_RULE_SET__TARGET_MODEL_ELEMENTS_ADAPTER;

	/**
	 * The feature id for the '<em><b>Transformation Queue</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__TRANSFORMATION_QUEUE = RulesPackage.TGG_RULE_SET__TRANSFORMATION_QUEUE;

	/**
	 * The feature id for the '<em><b>Source Reverse Navigation Store</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__SOURCE_REVERSE_NAVIGATION_STORE = RulesPackage.TGG_RULE_SET__SOURCE_REVERSE_NAVIGATION_STORE;

	/**
	 * The feature id for the '<em><b>Target Reverse Navigation Store</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__TARGET_REVERSE_NAVIGATION_STORE = RulesPackage.TGG_RULE_SET__TARGET_REVERSE_NAVIGATION_STORE;

	/**
	 * The feature id for the '<em><b>Source Model Root Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__SOURCE_MODEL_ROOT_NODE = RulesPackage.TGG_RULE_SET__SOURCE_MODEL_ROOT_NODE;

	/**
	 * The feature id for the '<em><b>Target Model Root Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__TARGET_MODEL_ROOT_NODE = RulesPackage.TGG_RULE_SET__TARGET_MODEL_ROOT_NODE;

	/**
	 * The feature id for the '<em><b>Corr Nodes To Delete</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__CORR_NODES_TO_DELETE = RulesPackage.TGG_RULE_SET__CORR_NODES_TO_DELETE;

	/**
	 * The feature id for the '<em><b>Resource Set</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__RESOURCE_SET = RulesPackage.TGG_RULE_SET__RESOURCE_SET;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__ID = RulesPackage.TGG_RULE_SET__ID;

	/**
	 * The feature id for the '<em><b>External References</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__EXTERNAL_REFERENCES = RulesPackage.TGG_RULE_SET__EXTERNAL_REFERENCES;

	/**
	 * The feature id for the '<em><b>External Ref Patterns</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__EXTERNAL_REF_PATTERNS = RulesPackage.TGG_RULE_SET__EXTERNAL_REF_PATTERNS;

	/**
	 * The feature id for the '<em><b>Sdm Interpreter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET__SDM_INTERPRETER = RulesPackage.TGG_RULE_SET_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Rule Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE2AADL_RULE_SET_FEATURE_COUNT = RulesPackage.TGG_RULE_SET_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RulePackage2AadlPackageImpl <em>Rule Package2 Aadl Package</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RulePackage2AadlPackageImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRulePackage2AadlPackage()
	 * @generated
	 */
	int RULE_PACKAGE2_AADL_PACKAGE = 11;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_PACKAGE2_AADL_PACKAGE__CREATED_CORR_NODES = RulesPackage.TGG_AXIOM__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_PACKAGE2_AADL_PACKAGE__RULE_SET = RulesPackage.TGG_AXIOM__RULE_SET;

	/**
	 * The number of structural features of the '<em>Rule Package2 Aadl Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_PACKAGE2_AADL_PACKAGE_FEATURE_COUNT = RulesPackage.TGG_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleComponent2ComponentTypeImpl <em>Rule Component2 Component Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleComponent2ComponentTypeImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleComponent2ComponentType()
	 * @generated
	 */
	int RULE_COMPONENT2_COMPONENT_TYPE = 12;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_COMPONENT_TYPE__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_COMPONENT_TYPE__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_COMPONENT_TYPE__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Component2 Component Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_COMPONENT_TYPE_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleComponent2ComponentTypeWithExtendsImpl <em>Rule Component2 Component Type With Extends</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleComponent2ComponentTypeWithExtendsImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleComponent2ComponentTypeWithExtends()
	 * @generated
	 */
	int RULE_COMPONENT2_COMPONENT_TYPE_WITH_EXTENDS = 13;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_COMPONENT_TYPE_WITH_EXTENDS__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_COMPONENT_TYPE_WITH_EXTENDS__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_COMPONENT_TYPE_WITH_EXTENDS__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Component2 Component Type With Extends</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_COMPONENT_TYPE_WITH_EXTENDS_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleFeatureGroup2FeatureGroupTypeImpl <em>Rule Feature Group2 Feature Group Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleFeatureGroup2FeatureGroupTypeImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleFeatureGroup2FeatureGroupType()
	 * @generated
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE = 14;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Feature Group2 Feature Group Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleFeatureGroup2FeatureGroupTypeWithInverseImpl <em>Rule Feature Group2 Feature Group Type With Inverse</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleFeatureGroup2FeatureGroupTypeWithInverseImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleFeatureGroup2FeatureGroupTypeWithInverse()
	 * @generated
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_INVERSE = 15;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_INVERSE__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_INVERSE__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_INVERSE__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Feature Group2 Feature Group Type With Inverse</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_INVERSE_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleFeatureGroup2FeatureGroupTypeWithExtendsImpl <em>Rule Feature Group2 Feature Group Type With Extends</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleFeatureGroup2FeatureGroupTypeWithExtendsImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleFeatureGroup2FeatureGroupTypeWithExtends()
	 * @generated
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_EXTENDS = 16;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_EXTENDS__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_EXTENDS__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_EXTENDS__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Feature Group2 Feature Group Type With Extends</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_EXTENDS_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleFeatureGroup2FeatureGroupTypeWithExtendsInverseImpl <em>Rule Feature Group2 Feature Group Type With Extends Inverse</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleFeatureGroup2FeatureGroupTypeWithExtendsInverseImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleFeatureGroup2FeatureGroupTypeWithExtendsInverse()
	 * @generated
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_EXTENDS_INVERSE = 17;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_EXTENDS_INVERSE__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_EXTENDS_INVERSE__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_EXTENDS_INVERSE__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Feature Group2 Feature Group Type With Extends Inverse</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_EXTENDS_INVERSE_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleComponent2ComponentImplementationImpl <em>Rule Component2 Component Implementation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleComponent2ComponentImplementationImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleComponent2ComponentImplementation()
	 * @generated
	 */
	int RULE_COMPONENT2_COMPONENT_IMPLEMENTATION = 18;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_COMPONENT_IMPLEMENTATION__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_COMPONENT_IMPLEMENTATION__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_COMPONENT_IMPLEMENTATION__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Component2 Component Implementation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_COMPONENT_IMPLEMENTATION_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleComponent2ComponentImplementationWithExtendsImpl <em>Rule Component2 Component Implementation With Extends</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleComponent2ComponentImplementationWithExtendsImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleComponent2ComponentImplementationWithExtends()
	 * @generated
	 */
	int RULE_COMPONENT2_COMPONENT_IMPLEMENTATION_WITH_EXTENDS = 19;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_COMPONENT_IMPLEMENTATION_WITH_EXTENDS__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_COMPONENT_IMPLEMENTATION_WITH_EXTENDS__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_COMPONENT_IMPLEMENTATION_WITH_EXTENDS__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Component2 Component Implementation With Extends</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_COMPONENT_IMPLEMENTATION_WITH_EXTENDS_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentImpl <em>Rule Component2 Subcomponent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleComponent2Subcomponent()
	 * @generated
	 */
	int RULE_COMPONENT2_SUBCOMPONENT = 20;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Component2 Subcomponent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentTypedImpl <em>Rule Component2 Subcomponent Typed</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentTypedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleComponent2SubcomponentTyped()
	 * @generated
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED = 21;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Component2 Subcomponent Typed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentTypedAsParentImpl <em>Rule Component2 Subcomponent Typed As Parent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentTypedAsParentImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleComponent2SubcomponentTypedAsParent()
	 * @generated
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT = 22;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Component2 Subcomponent Typed As Parent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentRefinedImpl <em>Rule Component2 Subcomponent Refined</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentRefinedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleComponent2SubcomponentRefined()
	 * @generated
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_REFINED = 23;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_REFINED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_REFINED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_REFINED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Component2 Subcomponent Refined</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_REFINED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentRefinedTypedImpl <em>Rule Component2 Subcomponent Refined Typed</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentRefinedTypedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleComponent2SubcomponentRefinedTyped()
	 * @generated
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_REFINED_TYPED = 24;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_REFINED_TYPED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_REFINED_TYPED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_REFINED_TYPED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Component2 Subcomponent Refined Typed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_REFINED_TYPED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentRefinedTypedAsParentImpl <em>Rule Component2 Subcomponent Refined Typed As Parent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentRefinedTypedAsParentImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleComponent2SubcomponentRefinedTypedAsParent()
	 * @generated
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_REFINED_TYPED_AS_PARENT = 25;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_REFINED_TYPED_AS_PARENT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_REFINED_TYPED_AS_PARENT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_REFINED_TYPED_AS_PARENT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Component2 Subcomponent Refined Typed As Parent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_REFINED_TYPED_AS_PARENT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentTypedRefinedImpl <em>Rule Component2 Subcomponent Typed Refined</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentTypedRefinedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleComponent2SubcomponentTypedRefined()
	 * @generated
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED = 26;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Component2 Subcomponent Typed Refined</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentTypedAsParentRefinedImpl <em>Rule Component2 Subcomponent Typed As Parent Refined</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentTypedAsParentRefinedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleComponent2SubcomponentTypedAsParentRefined()
	 * @generated
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT_REFINED = 27;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT_REFINED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT_REFINED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT_REFINED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Component2 Subcomponent Typed As Parent Refined</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT_REFINED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentTypedRefinedTypedImpl <em>Rule Component2 Subcomponent Typed Refined Typed</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentTypedRefinedTypedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleComponent2SubcomponentTypedRefinedTyped()
	 * @generated
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED_TYPED = 28;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED_TYPED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED_TYPED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED_TYPED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Component2 Subcomponent Typed Refined Typed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED_TYPED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentTypedAsParentRefinedTypedImpl <em>Rule Component2 Subcomponent Typed As Parent Refined Typed</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentTypedAsParentRefinedTypedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleComponent2SubcomponentTypedAsParentRefinedTyped()
	 * @generated
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT_REFINED_TYPED = 29;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT_REFINED_TYPED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT_REFINED_TYPED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT_REFINED_TYPED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Component2 Subcomponent Typed As Parent Refined Typed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT_REFINED_TYPED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentTypedRefinedTypedAsParentImpl <em>Rule Component2 Subcomponent Typed Refined Typed As Parent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleComponent2SubcomponentTypedRefinedTypedAsParentImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleComponent2SubcomponentTypedRefinedTypedAsParent()
	 * @generated
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED_TYPED_AS_PARENT = 30;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED_TYPED_AS_PARENT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED_TYPED_AS_PARENT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED_TYPED_AS_PARENT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Component2 Subcomponent Typed Refined Typed As Parent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED_TYPED_AS_PARENT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleDirectedFeat2DirectedFeatImpl <em>Rule Directed Feat2 Directed Feat</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleDirectedFeat2DirectedFeatImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleDirectedFeat2DirectedFeat()
	 * @generated
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT = 31;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Directed Feat2 Directed Feat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleDirectedFeat2DirectedFeatTypedImpl <em>Rule Directed Feat2 Directed Feat Typed</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleDirectedFeat2DirectedFeatTypedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleDirectedFeat2DirectedFeatTyped()
	 * @generated
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT_TYPED = 32;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT_TYPED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT_TYPED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT_TYPED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Directed Feat2 Directed Feat Typed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT_TYPED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleDirectedFeat2DirectedFeatRefinedImpl <em>Rule Directed Feat2 Directed Feat Refined</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleDirectedFeat2DirectedFeatRefinedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleDirectedFeat2DirectedFeatRefined()
	 * @generated
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT_REFINED = 33;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT_REFINED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT_REFINED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT_REFINED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Directed Feat2 Directed Feat Refined</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT_REFINED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleDirectedFeat2DirectedFeatTypedRefinedImpl <em>Rule Directed Feat2 Directed Feat Typed Refined</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleDirectedFeat2DirectedFeatTypedRefinedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleDirectedFeat2DirectedFeatTypedRefined()
	 * @generated
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT_TYPED_REFINED = 34;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT_TYPED_REFINED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT_TYPED_REFINED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT_TYPED_REFINED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Directed Feat2 Directed Feat Typed Refined</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIRECTED_FEAT2_DIRECTED_FEAT_TYPED_REFINED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleAccess2AccessImpl <em>Rule Access2 Access</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleAccess2AccessImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleAccess2Access()
	 * @generated
	 */
	int RULE_ACCESS2_ACCESS = 35;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Access2 Access</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleAccess2AccessTypedImpl <em>Rule Access2 Access Typed</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleAccess2AccessTypedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleAccess2AccessTyped()
	 * @generated
	 */
	int RULE_ACCESS2_ACCESS_TYPED = 36;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_TYPED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_TYPED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_TYPED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Access2 Access Typed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_TYPED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleAccess2AccessTypedAsParentImpl <em>Rule Access2 Access Typed As Parent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleAccess2AccessTypedAsParentImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleAccess2AccessTypedAsParent()
	 * @generated
	 */
	int RULE_ACCESS2_ACCESS_TYPED_AS_PARENT = 37;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_TYPED_AS_PARENT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_TYPED_AS_PARENT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_TYPED_AS_PARENT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Access2 Access Typed As Parent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_TYPED_AS_PARENT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleAccess2AccessRefinedImpl <em>Rule Access2 Access Refined</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleAccess2AccessRefinedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleAccess2AccessRefined()
	 * @generated
	 */
	int RULE_ACCESS2_ACCESS_REFINED = 38;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_REFINED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_REFINED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_REFINED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Access2 Access Refined</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_REFINED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleAccess2AccessTypedRefinedImpl <em>Rule Access2 Access Typed Refined</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleAccess2AccessTypedRefinedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleAccess2AccessTypedRefined()
	 * @generated
	 */
	int RULE_ACCESS2_ACCESS_TYPED_REFINED = 39;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_TYPED_REFINED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_TYPED_REFINED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_TYPED_REFINED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Access2 Access Typed Refined</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_TYPED_REFINED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleAccess2AccessTypedAsParentRefinedImpl <em>Rule Access2 Access Typed As Parent Refined</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleAccess2AccessTypedAsParentRefinedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleAccess2AccessTypedAsParentRefined()
	 * @generated
	 */
	int RULE_ACCESS2_ACCESS_TYPED_AS_PARENT_REFINED = 40;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_TYPED_AS_PARENT_REFINED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_TYPED_AS_PARENT_REFINED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_TYPED_AS_PARENT_REFINED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Access2 Access Typed As Parent Refined</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS2_ACCESS_TYPED_AS_PARENT_REFINED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupDirFeat2FeatGroupDirFeatImpl <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupDirFeat2FeatGroupDirFeatImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleFeatGroupDirFeat2FeatGroupDirFeat()
	 * @generated
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT = 41;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedImpl <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleFeatGroupDirFeat2FeatGroupDirFeatTyped()
	 * @generated
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED = 42;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentImpl <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent()
	 * @generated
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_AS_PARENT = 43;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_AS_PARENT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_AS_PARENT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_AS_PARENT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_AS_PARENT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupDirFeat2FeatGroupDirFeatRefinedImpl <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Refined</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupDirFeat2FeatGroupDirFeatRefinedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleFeatGroupDirFeat2FeatGroupDirFeatRefined()
	 * @generated
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_REFINED = 44;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_REFINED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_REFINED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_REFINED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Refined</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_REFINED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefinedImpl <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed Refined</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefinedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined()
	 * @generated
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_REFINED = 45;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_REFINED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_REFINED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_REFINED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed Refined</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_REFINED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefinedImpl <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent Refined</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefinedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined()
	 * @generated
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_AS_PARENT_REFINED = 46;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_AS_PARENT_REFINED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_AS_PARENT_REFINED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_AS_PARENT_REFINED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent Refined</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_AS_PARENT_REFINED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupAccess2FeatGroupAccessImpl <em>Rule Feat Group Access2 Feat Group Access</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupAccess2FeatGroupAccessImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleFeatGroupAccess2FeatGroupAccess()
	 * @generated
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS = 47;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Feat Group Access2 Feat Group Access</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupAccess2FeatGroupAccessTypedImpl <em>Rule Feat Group Access2 Feat Group Access Typed</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupAccess2FeatGroupAccessTypedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleFeatGroupAccess2FeatGroupAccessTyped()
	 * @generated
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_TYPED = 48;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_TYPED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_TYPED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_TYPED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Feat Group Access2 Feat Group Access Typed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_TYPED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupAccess2FeatGroupAccessRefinedImpl <em>Rule Feat Group Access2 Feat Group Access Refined</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupAccess2FeatGroupAccessRefinedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleFeatGroupAccess2FeatGroupAccessRefined()
	 * @generated
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_REFINED = 49;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_REFINED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_REFINED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_REFINED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Feat Group Access2 Feat Group Access Refined</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_REFINED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupAccess2FeatGroupAccessTypedRefinedImpl <em>Rule Feat Group Access2 Feat Group Access Typed Refined</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleFeatGroupAccess2FeatGroupAccessTypedRefinedImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleFeatGroupAccess2FeatGroupAccessTypedRefined()
	 * @generated
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_TYPED_REFINED = 50;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_TYPED_REFINED__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_TYPED_REFINED__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_TYPED_REFINED__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Feat Group Access2 Feat Group Access Typed Refined</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_TYPED_REFINED_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoImpl <em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleDirFeatConn2DirFeatConnSrcParentDestSubcompo()
	 * @generated
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_DEST_SUBCOMPO = 51;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_DEST_SUBCOMPO__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_DEST_SUBCOMPO__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_DEST_SUBCOMPO__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_DEST_SUBCOMPO_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePortImpl <em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo Same Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePortImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort()
	 * @generated
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_DEST_SUBCOMPO_SAME_PORT = 52;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_DEST_SUBCOMPO_SAME_PORT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_DEST_SUBCOMPO_SAME_PORT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_DEST_SUBCOMPO_SAME_PORT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo Same Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_DEST_SUBCOMPO_SAME_PORT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentImpl <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleDirFeatConn2DirFeatConnSrcSubcompoDestParent()
	 * @generated
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT = 53;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupImpl <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup()
	 * @generated
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_FEAT_GROUP = 54;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_FEAT_GROUP__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_FEAT_GROUP__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_FEAT_GROUP__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_FEAT_GROUP_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverseImpl <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group Inverse</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverseImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse()
	 * @generated
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_FEAT_GROUP_INVERSE = 55;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_FEAT_GROUP_INVERSE__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_FEAT_GROUP_INVERSE__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_FEAT_GROUP_INVERSE__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group Inverse</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_FEAT_GROUP_INVERSE_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompoImpl <em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Dest Subcompo</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompoImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo()
	 * @generated
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_FEAT_GROUP_DEST_SUBCOMPO = 56;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_FEAT_GROUP_DEST_SUBCOMPO__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_FEAT_GROUP_DEST_SUBCOMPO__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_FEAT_GROUP_DEST_SUBCOMPO__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Dest Subcompo</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_FEAT_GROUP_DEST_SUBCOMPO_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompoImpl <em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Inverse Dest Subcompo</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompoImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo()
	 * @generated
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_FEAT_GROUP_INVERSE_DEST_SUBCOMPO = 57;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_FEAT_GROUP_INVERSE_DEST_SUBCOMPO__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_FEAT_GROUP_INVERSE_DEST_SUBCOMPO__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_FEAT_GROUP_INVERSE_DEST_SUBCOMPO__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Inverse Dest Subcompo</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_FEAT_GROUP_INVERSE_DEST_SUBCOMPO_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePortImpl <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Same Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePortImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort()
	 * @generated
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_SAME_PORT = 58;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_SAME_PORT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_SAME_PORT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_SAME_PORT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Same Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_SAME_PORT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoImpl <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo()
	 * @generated
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO = 59;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePortImpl <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo Same Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePortImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort()
	 * @generated
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO_SAME_PORT = 60;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO_SAME_PORT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO_SAME_PORT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO_SAME_PORT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo Same Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO_SAME_PORT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcParentDestSubcompoFeatImpl <em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcParentDestSubcompoFeatImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleAccessConn2AccessConnSrcParentDestSubcompoFeat()
	 * @generated
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO_FEAT = 61;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO_FEAT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO_FEAT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO_FEAT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO_FEAT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccessImpl <em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat Same Access</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccessImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess()
	 * @generated
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO_FEAT_SAME_ACCESS = 62;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO_FEAT_SAME_ACCESS__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO_FEAT_SAME_ACCESS__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO_FEAT_SAME_ACCESS__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat Same Access</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO_FEAT_SAME_ACCESS_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcSubcompoFeatDestParentImpl <em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcSubcompoFeatDestParentImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleAccessConn2AccessConnSrcSubcompoFeatDestParent()
	 * @generated
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_PARENT = 63;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_PARENT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_PARENT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_PARENT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_PARENT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccessImpl <em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent Same Access</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccessImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess()
	 * @generated
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_PARENT_SAME_ACCESS = 64;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_PARENT_SAME_ACCESS__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_PARENT_SAME_ACCESS__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_PARENT_SAME_ACCESS__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent Same Access</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_PARENT_SAME_ACCESS_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeatImpl <em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo Feat</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeatImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat()
	 * @generated
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_SUBCOMPO_FEAT = 65;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_SUBCOMPO_FEAT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_SUBCOMPO_FEAT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_SUBCOMPO_FEAT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo Feat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_SUBCOMPO_FEAT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcSubcompoDestParentImpl <em>Rule Access Conn2 Access Conn Src Subcompo Dest Parent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcSubcompoDestParentImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleAccessConn2AccessConnSrcSubcompoDestParent()
	 * @generated
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_PARENT = 66;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_PARENT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_PARENT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_PARENT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Parent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_PARENT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeatImpl <em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo Feat</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeatImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat()
	 * @generated
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO_FEAT = 67;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO_FEAT__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO_FEAT__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO_FEAT__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo Feat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO_FEAT_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcSubcompoDestSubcompoImpl <em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcSubcompoDestSubcompoImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleAccessConn2AccessConnSrcSubcompoDestSubcompo()
	 * @generated
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO = 68;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcParentDestSubcompoImpl <em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcParentDestSubcompoImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleAccessConn2AccessConnSrcParentDestSubcompo()
	 * @generated
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO = 69;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoImpl <em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.adele2aadl.impl.RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoImpl
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo()
	 * @generated
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_SUBCOMPO = 70;

	/**
	 * The feature id for the '<em><b>Created Corr Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_SUBCOMPO__CREATED_CORR_NODES = RulesPackage.TGG_RULE__CREATED_CORR_NODES;

	/**
	 * The feature id for the '<em><b>Rule Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_SUBCOMPO__RULE_SET = RulesPackage.TGG_RULE__RULE_SET;

	/**
	 * The feature id for the '<em><b>Input Corr Node Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_SUBCOMPO__INPUT_CORR_NODE_TYPES = RulesPackage.TGG_RULE__INPUT_CORR_NODE_TYPES;

	/**
	 * The number of structural features of the '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_SUBCOMPO_FEATURE_COUNT = RulesPackage.TGG_RULE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '<em>SDM Interpreter</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.mdelab.sdm.interpreter.sde.eclipse.SDEEclipseSDMInterpreter
	 * @see fr.labsticc.adosate.adele2aadl.impl.Adele2aadlPackageImpl#getSDMInterpreter()
	 * @generated
	 */
	int SDM_INTERPRETER = 71;

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.CorrAxiom <em>Corr Axiom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Corr Axiom</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.CorrAxiom
	 * @generated
	 */
	EClass getCorrAxiom();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.CorrComponent <em>Corr Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Corr Component</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.CorrComponent
	 * @generated
	 */
	EClass getCorrComponent();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.CorrComponentType <em>Corr Component Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Corr Component Type</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.CorrComponentType
	 * @generated
	 */
	EClass getCorrComponentType();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.CorrComponentImplementation <em>Corr Component Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Corr Component Implementation</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.CorrComponentImplementation
	 * @generated
	 */
	EClass getCorrComponentImplementation();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.CorrSubcomponent <em>Corr Subcomponent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Corr Subcomponent</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.CorrSubcomponent
	 * @generated
	 */
	EClass getCorrSubcomponent();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.CorrFeature <em>Corr Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Corr Feature</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.CorrFeature
	 * @generated
	 */
	EClass getCorrFeature();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.CorrDirectedFeature <em>Corr Directed Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Corr Directed Feature</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.CorrDirectedFeature
	 * @generated
	 */
	EClass getCorrDirectedFeature();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.CorrAccess <em>Corr Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Corr Access</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.CorrAccess
	 * @generated
	 */
	EClass getCorrAccess();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.CorrDirectedFeatureConnection <em>Corr Directed Feature Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Corr Directed Feature Connection</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.CorrDirectedFeatureConnection
	 * @generated
	 */
	EClass getCorrDirectedFeatureConnection();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.CorrAccessConnection <em>Corr Access Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Corr Access Connection</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.CorrAccessConnection
	 * @generated
	 */
	EClass getCorrAccessConnection();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet <em>Rule Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Set</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet
	 * @generated
	 */
	EClass getAdele2aadlRuleSet();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet#getSdmInterpreter <em>Sdm Interpreter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sdm Interpreter</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet#getSdmInterpreter()
	 * @see #getAdele2aadlRuleSet()
	 * @generated
	 */
	EAttribute getAdele2aadlRuleSet_SdmInterpreter();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RulePackage2AadlPackage <em>Rule Package2 Aadl Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Package2 Aadl Package</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RulePackage2AadlPackage
	 * @generated
	 */
	EClass getRulePackage2AadlPackage();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2ComponentType <em>Rule Component2 Component Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Component2 Component Type</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2ComponentType
	 * @generated
	 */
	EClass getRuleComponent2ComponentType();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2ComponentTypeWithExtends <em>Rule Component2 Component Type With Extends</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Component2 Component Type With Extends</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2ComponentTypeWithExtends
	 * @generated
	 */
	EClass getRuleComponent2ComponentTypeWithExtends();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatureGroup2FeatureGroupType <em>Rule Feature Group2 Feature Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Feature Group2 Feature Group Type</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatureGroup2FeatureGroupType
	 * @generated
	 */
	EClass getRuleFeatureGroup2FeatureGroupType();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatureGroup2FeatureGroupTypeWithInverse <em>Rule Feature Group2 Feature Group Type With Inverse</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Feature Group2 Feature Group Type With Inverse</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatureGroup2FeatureGroupTypeWithInverse
	 * @generated
	 */
	EClass getRuleFeatureGroup2FeatureGroupTypeWithInverse();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatureGroup2FeatureGroupTypeWithExtends <em>Rule Feature Group2 Feature Group Type With Extends</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Feature Group2 Feature Group Type With Extends</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatureGroup2FeatureGroupTypeWithExtends
	 * @generated
	 */
	EClass getRuleFeatureGroup2FeatureGroupTypeWithExtends();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatureGroup2FeatureGroupTypeWithExtendsInverse <em>Rule Feature Group2 Feature Group Type With Extends Inverse</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Feature Group2 Feature Group Type With Extends Inverse</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatureGroup2FeatureGroupTypeWithExtendsInverse
	 * @generated
	 */
	EClass getRuleFeatureGroup2FeatureGroupTypeWithExtendsInverse();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2ComponentImplementation <em>Rule Component2 Component Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Component2 Component Implementation</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2ComponentImplementation
	 * @generated
	 */
	EClass getRuleComponent2ComponentImplementation();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2ComponentImplementationWithExtends <em>Rule Component2 Component Implementation With Extends</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Component2 Component Implementation With Extends</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2ComponentImplementationWithExtends
	 * @generated
	 */
	EClass getRuleComponent2ComponentImplementationWithExtends();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2Subcomponent <em>Rule Component2 Subcomponent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Component2 Subcomponent</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2Subcomponent
	 * @generated
	 */
	EClass getRuleComponent2Subcomponent();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTyped <em>Rule Component2 Subcomponent Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Component2 Subcomponent Typed</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTyped
	 * @generated
	 */
	EClass getRuleComponent2SubcomponentTyped();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedAsParent <em>Rule Component2 Subcomponent Typed As Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Component2 Subcomponent Typed As Parent</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedAsParent
	 * @generated
	 */
	EClass getRuleComponent2SubcomponentTypedAsParent();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentRefined <em>Rule Component2 Subcomponent Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Component2 Subcomponent Refined</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentRefined
	 * @generated
	 */
	EClass getRuleComponent2SubcomponentRefined();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentRefinedTyped <em>Rule Component2 Subcomponent Refined Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Component2 Subcomponent Refined Typed</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentRefinedTyped
	 * @generated
	 */
	EClass getRuleComponent2SubcomponentRefinedTyped();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentRefinedTypedAsParent <em>Rule Component2 Subcomponent Refined Typed As Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Component2 Subcomponent Refined Typed As Parent</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentRefinedTypedAsParent
	 * @generated
	 */
	EClass getRuleComponent2SubcomponentRefinedTypedAsParent();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedRefined <em>Rule Component2 Subcomponent Typed Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Component2 Subcomponent Typed Refined</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedRefined
	 * @generated
	 */
	EClass getRuleComponent2SubcomponentTypedRefined();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedAsParentRefined <em>Rule Component2 Subcomponent Typed As Parent Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Component2 Subcomponent Typed As Parent Refined</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedAsParentRefined
	 * @generated
	 */
	EClass getRuleComponent2SubcomponentTypedAsParentRefined();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedRefinedTyped <em>Rule Component2 Subcomponent Typed Refined Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Component2 Subcomponent Typed Refined Typed</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedRefinedTyped
	 * @generated
	 */
	EClass getRuleComponent2SubcomponentTypedRefinedTyped();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedAsParentRefinedTyped <em>Rule Component2 Subcomponent Typed As Parent Refined Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Component2 Subcomponent Typed As Parent Refined Typed</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedAsParentRefinedTyped
	 * @generated
	 */
	EClass getRuleComponent2SubcomponentTypedAsParentRefinedTyped();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedRefinedTypedAsParent <em>Rule Component2 Subcomponent Typed Refined Typed As Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Component2 Subcomponent Typed Refined Typed As Parent</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedRefinedTypedAsParent
	 * @generated
	 */
	EClass getRuleComponent2SubcomponentTypedRefinedTypedAsParent();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleDirectedFeat2DirectedFeat <em>Rule Directed Feat2 Directed Feat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Directed Feat2 Directed Feat</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirectedFeat2DirectedFeat
	 * @generated
	 */
	EClass getRuleDirectedFeat2DirectedFeat();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleDirectedFeat2DirectedFeatTyped <em>Rule Directed Feat2 Directed Feat Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Directed Feat2 Directed Feat Typed</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirectedFeat2DirectedFeatTyped
	 * @generated
	 */
	EClass getRuleDirectedFeat2DirectedFeatTyped();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleDirectedFeat2DirectedFeatRefined <em>Rule Directed Feat2 Directed Feat Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Directed Feat2 Directed Feat Refined</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirectedFeat2DirectedFeatRefined
	 * @generated
	 */
	EClass getRuleDirectedFeat2DirectedFeatRefined();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleDirectedFeat2DirectedFeatTypedRefined <em>Rule Directed Feat2 Directed Feat Typed Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Directed Feat2 Directed Feat Typed Refined</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirectedFeat2DirectedFeatTypedRefined
	 * @generated
	 */
	EClass getRuleDirectedFeat2DirectedFeatTypedRefined();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleAccess2Access <em>Rule Access2 Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Access2 Access</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccess2Access
	 * @generated
	 */
	EClass getRuleAccess2Access();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleAccess2AccessTyped <em>Rule Access2 Access Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Access2 Access Typed</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccess2AccessTyped
	 * @generated
	 */
	EClass getRuleAccess2AccessTyped();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleAccess2AccessTypedAsParent <em>Rule Access2 Access Typed As Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Access2 Access Typed As Parent</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccess2AccessTypedAsParent
	 * @generated
	 */
	EClass getRuleAccess2AccessTypedAsParent();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleAccess2AccessRefined <em>Rule Access2 Access Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Access2 Access Refined</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccess2AccessRefined
	 * @generated
	 */
	EClass getRuleAccess2AccessRefined();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleAccess2AccessTypedRefined <em>Rule Access2 Access Typed Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Access2 Access Typed Refined</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccess2AccessTypedRefined
	 * @generated
	 */
	EClass getRuleAccess2AccessTypedRefined();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleAccess2AccessTypedAsParentRefined <em>Rule Access2 Access Typed As Parent Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Access2 Access Typed As Parent Refined</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccess2AccessTypedAsParentRefined
	 * @generated
	 */
	EClass getRuleAccess2AccessTypedAsParentRefined();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeat <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeat
	 * @generated
	 */
	EClass getRuleFeatGroupDirFeat2FeatGroupDirFeat();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTyped <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTyped
	 * @generated
	 */
	EClass getRuleFeatGroupDirFeat2FeatGroupDirFeatTyped();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent
	 * @generated
	 */
	EClass getRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatRefined <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Refined</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatRefined
	 * @generated
	 */
	EClass getRuleFeatGroupDirFeat2FeatGroupDirFeatRefined();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed Refined</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined
	 * @generated
	 */
	EClass getRuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent Refined</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined
	 * @generated
	 */
	EClass getRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupAccess2FeatGroupAccess <em>Rule Feat Group Access2 Feat Group Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Feat Group Access2 Feat Group Access</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupAccess2FeatGroupAccess
	 * @generated
	 */
	EClass getRuleFeatGroupAccess2FeatGroupAccess();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupAccess2FeatGroupAccessTyped <em>Rule Feat Group Access2 Feat Group Access Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Feat Group Access2 Feat Group Access Typed</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupAccess2FeatGroupAccessTyped
	 * @generated
	 */
	EClass getRuleFeatGroupAccess2FeatGroupAccessTyped();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupAccess2FeatGroupAccessRefined <em>Rule Feat Group Access2 Feat Group Access Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Feat Group Access2 Feat Group Access Refined</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupAccess2FeatGroupAccessRefined
	 * @generated
	 */
	EClass getRuleFeatGroupAccess2FeatGroupAccessRefined();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupAccess2FeatGroupAccessTypedRefined <em>Rule Feat Group Access2 Feat Group Access Typed Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Feat Group Access2 Feat Group Access Typed Refined</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupAccess2FeatGroupAccessTypedRefined
	 * @generated
	 */
	EClass getRuleFeatGroupAccess2FeatGroupAccessTypedRefined();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcParentDestSubcompo <em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcParentDestSubcompo
	 * @generated
	 */
	EClass getRuleDirFeatConn2DirFeatConnSrcParentDestSubcompo();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort <em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo Same Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo Same Port</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort
	 * @generated
	 */
	EClass getRuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParent <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParent
	 * @generated
	 */
	EClass getRuleDirFeatConn2DirFeatConnSrcSubcompoDestParent();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup
	 * @generated
	 */
	EClass getRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group Inverse</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group Inverse</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse
	 * @generated
	 */
	EClass getRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo <em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Dest Subcompo</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Dest Subcompo</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo
	 * @generated
	 */
	EClass getRuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo <em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Inverse Dest Subcompo</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Inverse Dest Subcompo</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo
	 * @generated
	 */
	EClass getRuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Same Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Same Port</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort
	 * @generated
	 */
	EClass getRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo
	 * @generated
	 */
	EClass getRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo Same Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo Same Port</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort
	 * @generated
	 */
	EClass getRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcParentDestSubcompoFeat <em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcParentDestSubcompoFeat
	 * @generated
	 */
	EClass getRuleAccessConn2AccessConnSrcParentDestSubcompoFeat();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess <em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat Same Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat Same Access</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess
	 * @generated
	 */
	EClass getRuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoFeatDestParent <em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoFeatDestParent
	 * @generated
	 */
	EClass getRuleAccessConn2AccessConnSrcSubcompoFeatDestParent();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess <em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent Same Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent Same Access</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess
	 * @generated
	 */
	EClass getRuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat <em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo Feat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo Feat</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat
	 * @generated
	 */
	EClass getRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoDestParent <em>Rule Access Conn2 Access Conn Src Subcompo Dest Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Parent</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoDestParent
	 * @generated
	 */
	EClass getRuleAccessConn2AccessConnSrcSubcompoDestParent();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat <em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo Feat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo Feat</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat
	 * @generated
	 */
	EClass getRuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoDestSubcompo <em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoDestSubcompo
	 * @generated
	 */
	EClass getRuleAccessConn2AccessConnSrcSubcompoDestSubcompo();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcParentDestSubcompo <em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcParentDestSubcompo
	 * @generated
	 */
	EClass getRuleAccessConn2AccessConnSrcParentDestSubcompo();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo <em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo</em>'.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo
	 * @generated
	 */
	EClass getRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo();

	/**
	 * Returns the meta object for data type '{@link de.mdelab.sdm.interpreter.sde.eclipse.SDEEclipseSDMInterpreter <em>SDM Interpreter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>SDM Interpreter</em>'.
	 * @see de.mdelab.sdm.interpreter.sde.eclipse.SDEEclipseSDMInterpreter
	 * @model instanceClass="de.mdelab.sdm.interpreter.sde.eclipse.SDEEclipseSDMInterpreter"
	 * @generated
	 */
	EDataType getSDMInterpreter();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Adele2aadlFactory getAdele2aadlFactory();

} //Adele2aadlPackage
