/**
 */
package fr.labsticc.adosate.adele2aadl;

import de.hpi.sam.mote.TGGNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Component</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlPackage#getCorrComponent()
 * @model abstract="true"
 * @generated
 */
public interface CorrComponent extends TGGNode {
} // CorrComponent
