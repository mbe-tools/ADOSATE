/**
 */
package fr.labsticc.adosate.adele2aadl;

import de.hpi.sam.mote.TGGNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Directed Feature Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlPackage#getCorrDirectedFeatureConnection()
 * @model
 * @generated
 */
public interface CorrDirectedFeatureConnection extends TGGNode {
} // CorrDirectedFeatureConnection
