/**
 */
package fr.labsticc.adosate.adele2aadl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Directed Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlPackage#getCorrDirectedFeature()
 * @model
 * @generated
 */
public interface CorrDirectedFeature extends CorrFeature {
} // CorrDirectedFeature
