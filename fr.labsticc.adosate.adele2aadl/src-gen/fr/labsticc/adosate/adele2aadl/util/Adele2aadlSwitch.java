/**
 */
package fr.labsticc.adosate.adele2aadl.util;

import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.rules.TGGAxiom;
import de.hpi.sam.mote.rules.TGGMapping;
import de.hpi.sam.mote.rules.TGGRule;
import de.hpi.sam.mote.rules.TGGRuleSet;
import fr.labsticc.adosate.adele2aadl.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlPackage
 * @generated
 */
public class Adele2aadlSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Adele2aadlPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Adele2aadlSwitch() {
		if (modelPackage == null) {
			modelPackage = Adele2aadlPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case Adele2aadlPackage.CORR_AXIOM: {
				CorrAxiom corrAxiom = (CorrAxiom)theEObject;
				T result = caseCorrAxiom(corrAxiom);
				if (result == null) result = caseTGGNode(corrAxiom);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.CORR_COMPONENT: {
				CorrComponent corrComponent = (CorrComponent)theEObject;
				T result = caseCorrComponent(corrComponent);
				if (result == null) result = caseTGGNode(corrComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.CORR_COMPONENT_TYPE: {
				CorrComponentType corrComponentType = (CorrComponentType)theEObject;
				T result = caseCorrComponentType(corrComponentType);
				if (result == null) result = caseCorrComponent(corrComponentType);
				if (result == null) result = caseTGGNode(corrComponentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.CORR_COMPONENT_IMPLEMENTATION: {
				CorrComponentImplementation corrComponentImplementation = (CorrComponentImplementation)theEObject;
				T result = caseCorrComponentImplementation(corrComponentImplementation);
				if (result == null) result = caseCorrComponent(corrComponentImplementation);
				if (result == null) result = caseTGGNode(corrComponentImplementation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.CORR_SUBCOMPONENT: {
				CorrSubcomponent corrSubcomponent = (CorrSubcomponent)theEObject;
				T result = caseCorrSubcomponent(corrSubcomponent);
				if (result == null) result = caseTGGNode(corrSubcomponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.CORR_FEATURE: {
				CorrFeature corrFeature = (CorrFeature)theEObject;
				T result = caseCorrFeature(corrFeature);
				if (result == null) result = caseTGGNode(corrFeature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.CORR_DIRECTED_FEATURE: {
				CorrDirectedFeature corrDirectedFeature = (CorrDirectedFeature)theEObject;
				T result = caseCorrDirectedFeature(corrDirectedFeature);
				if (result == null) result = caseCorrFeature(corrDirectedFeature);
				if (result == null) result = caseTGGNode(corrDirectedFeature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.CORR_ACCESS: {
				CorrAccess corrAccess = (CorrAccess)theEObject;
				T result = caseCorrAccess(corrAccess);
				if (result == null) result = caseCorrFeature(corrAccess);
				if (result == null) result = caseTGGNode(corrAccess);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.CORR_DIRECTED_FEATURE_CONNECTION: {
				CorrDirectedFeatureConnection corrDirectedFeatureConnection = (CorrDirectedFeatureConnection)theEObject;
				T result = caseCorrDirectedFeatureConnection(corrDirectedFeatureConnection);
				if (result == null) result = caseTGGNode(corrDirectedFeatureConnection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.CORR_ACCESS_CONNECTION: {
				CorrAccessConnection corrAccessConnection = (CorrAccessConnection)theEObject;
				T result = caseCorrAccessConnection(corrAccessConnection);
				if (result == null) result = caseTGGNode(corrAccessConnection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.ADELE2AADL_RULE_SET: {
				Adele2aadlRuleSet adele2aadlRuleSet = (Adele2aadlRuleSet)theEObject;
				T result = caseAdele2aadlRuleSet(adele2aadlRuleSet);
				if (result == null) result = caseTGGRuleSet(adele2aadlRuleSet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_PACKAGE2_AADL_PACKAGE: {
				RulePackage2AadlPackage rulePackage2AadlPackage = (RulePackage2AadlPackage)theEObject;
				T result = caseRulePackage2AadlPackage(rulePackage2AadlPackage);
				if (result == null) result = caseTGGAxiom(rulePackage2AadlPackage);
				if (result == null) result = caseTGGMapping(rulePackage2AadlPackage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_COMPONENT2_COMPONENT_TYPE: {
				RuleComponent2ComponentType ruleComponent2ComponentType = (RuleComponent2ComponentType)theEObject;
				T result = caseRuleComponent2ComponentType(ruleComponent2ComponentType);
				if (result == null) result = caseTGGRule(ruleComponent2ComponentType);
				if (result == null) result = caseTGGMapping(ruleComponent2ComponentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_COMPONENT2_COMPONENT_TYPE_WITH_EXTENDS: {
				RuleComponent2ComponentTypeWithExtends ruleComponent2ComponentTypeWithExtends = (RuleComponent2ComponentTypeWithExtends)theEObject;
				T result = caseRuleComponent2ComponentTypeWithExtends(ruleComponent2ComponentTypeWithExtends);
				if (result == null) result = caseTGGRule(ruleComponent2ComponentTypeWithExtends);
				if (result == null) result = caseTGGMapping(ruleComponent2ComponentTypeWithExtends);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE: {
				RuleFeatureGroup2FeatureGroupType ruleFeatureGroup2FeatureGroupType = (RuleFeatureGroup2FeatureGroupType)theEObject;
				T result = caseRuleFeatureGroup2FeatureGroupType(ruleFeatureGroup2FeatureGroupType);
				if (result == null) result = caseTGGRule(ruleFeatureGroup2FeatureGroupType);
				if (result == null) result = caseTGGMapping(ruleFeatureGroup2FeatureGroupType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_INVERSE: {
				RuleFeatureGroup2FeatureGroupTypeWithInverse ruleFeatureGroup2FeatureGroupTypeWithInverse = (RuleFeatureGroup2FeatureGroupTypeWithInverse)theEObject;
				T result = caseRuleFeatureGroup2FeatureGroupTypeWithInverse(ruleFeatureGroup2FeatureGroupTypeWithInverse);
				if (result == null) result = caseTGGRule(ruleFeatureGroup2FeatureGroupTypeWithInverse);
				if (result == null) result = caseTGGMapping(ruleFeatureGroup2FeatureGroupTypeWithInverse);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_EXTENDS: {
				RuleFeatureGroup2FeatureGroupTypeWithExtends ruleFeatureGroup2FeatureGroupTypeWithExtends = (RuleFeatureGroup2FeatureGroupTypeWithExtends)theEObject;
				T result = caseRuleFeatureGroup2FeatureGroupTypeWithExtends(ruleFeatureGroup2FeatureGroupTypeWithExtends);
				if (result == null) result = caseTGGRule(ruleFeatureGroup2FeatureGroupTypeWithExtends);
				if (result == null) result = caseTGGMapping(ruleFeatureGroup2FeatureGroupTypeWithExtends);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_EXTENDS_INVERSE: {
				RuleFeatureGroup2FeatureGroupTypeWithExtendsInverse ruleFeatureGroup2FeatureGroupTypeWithExtendsInverse = (RuleFeatureGroup2FeatureGroupTypeWithExtendsInverse)theEObject;
				T result = caseRuleFeatureGroup2FeatureGroupTypeWithExtendsInverse(ruleFeatureGroup2FeatureGroupTypeWithExtendsInverse);
				if (result == null) result = caseTGGRule(ruleFeatureGroup2FeatureGroupTypeWithExtendsInverse);
				if (result == null) result = caseTGGMapping(ruleFeatureGroup2FeatureGroupTypeWithExtendsInverse);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_COMPONENT2_COMPONENT_IMPLEMENTATION: {
				RuleComponent2ComponentImplementation ruleComponent2ComponentImplementation = (RuleComponent2ComponentImplementation)theEObject;
				T result = caseRuleComponent2ComponentImplementation(ruleComponent2ComponentImplementation);
				if (result == null) result = caseTGGRule(ruleComponent2ComponentImplementation);
				if (result == null) result = caseTGGMapping(ruleComponent2ComponentImplementation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_COMPONENT2_COMPONENT_IMPLEMENTATION_WITH_EXTENDS: {
				RuleComponent2ComponentImplementationWithExtends ruleComponent2ComponentImplementationWithExtends = (RuleComponent2ComponentImplementationWithExtends)theEObject;
				T result = caseRuleComponent2ComponentImplementationWithExtends(ruleComponent2ComponentImplementationWithExtends);
				if (result == null) result = caseTGGRule(ruleComponent2ComponentImplementationWithExtends);
				if (result == null) result = caseTGGMapping(ruleComponent2ComponentImplementationWithExtends);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT: {
				RuleComponent2Subcomponent ruleComponent2Subcomponent = (RuleComponent2Subcomponent)theEObject;
				T result = caseRuleComponent2Subcomponent(ruleComponent2Subcomponent);
				if (result == null) result = caseTGGRule(ruleComponent2Subcomponent);
				if (result == null) result = caseTGGMapping(ruleComponent2Subcomponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_TYPED: {
				RuleComponent2SubcomponentTyped ruleComponent2SubcomponentTyped = (RuleComponent2SubcomponentTyped)theEObject;
				T result = caseRuleComponent2SubcomponentTyped(ruleComponent2SubcomponentTyped);
				if (result == null) result = caseTGGRule(ruleComponent2SubcomponentTyped);
				if (result == null) result = caseTGGMapping(ruleComponent2SubcomponentTyped);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT: {
				RuleComponent2SubcomponentTypedAsParent ruleComponent2SubcomponentTypedAsParent = (RuleComponent2SubcomponentTypedAsParent)theEObject;
				T result = caseRuleComponent2SubcomponentTypedAsParent(ruleComponent2SubcomponentTypedAsParent);
				if (result == null) result = caseTGGRule(ruleComponent2SubcomponentTypedAsParent);
				if (result == null) result = caseTGGMapping(ruleComponent2SubcomponentTypedAsParent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_REFINED: {
				RuleComponent2SubcomponentRefined ruleComponent2SubcomponentRefined = (RuleComponent2SubcomponentRefined)theEObject;
				T result = caseRuleComponent2SubcomponentRefined(ruleComponent2SubcomponentRefined);
				if (result == null) result = caseTGGRule(ruleComponent2SubcomponentRefined);
				if (result == null) result = caseTGGMapping(ruleComponent2SubcomponentRefined);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_REFINED_TYPED: {
				RuleComponent2SubcomponentRefinedTyped ruleComponent2SubcomponentRefinedTyped = (RuleComponent2SubcomponentRefinedTyped)theEObject;
				T result = caseRuleComponent2SubcomponentRefinedTyped(ruleComponent2SubcomponentRefinedTyped);
				if (result == null) result = caseTGGRule(ruleComponent2SubcomponentRefinedTyped);
				if (result == null) result = caseTGGMapping(ruleComponent2SubcomponentRefinedTyped);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_REFINED_TYPED_AS_PARENT: {
				RuleComponent2SubcomponentRefinedTypedAsParent ruleComponent2SubcomponentRefinedTypedAsParent = (RuleComponent2SubcomponentRefinedTypedAsParent)theEObject;
				T result = caseRuleComponent2SubcomponentRefinedTypedAsParent(ruleComponent2SubcomponentRefinedTypedAsParent);
				if (result == null) result = caseTGGRule(ruleComponent2SubcomponentRefinedTypedAsParent);
				if (result == null) result = caseTGGMapping(ruleComponent2SubcomponentRefinedTypedAsParent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED: {
				RuleComponent2SubcomponentTypedRefined ruleComponent2SubcomponentTypedRefined = (RuleComponent2SubcomponentTypedRefined)theEObject;
				T result = caseRuleComponent2SubcomponentTypedRefined(ruleComponent2SubcomponentTypedRefined);
				if (result == null) result = caseTGGRule(ruleComponent2SubcomponentTypedRefined);
				if (result == null) result = caseTGGMapping(ruleComponent2SubcomponentTypedRefined);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT_REFINED: {
				RuleComponent2SubcomponentTypedAsParentRefined ruleComponent2SubcomponentTypedAsParentRefined = (RuleComponent2SubcomponentTypedAsParentRefined)theEObject;
				T result = caseRuleComponent2SubcomponentTypedAsParentRefined(ruleComponent2SubcomponentTypedAsParentRefined);
				if (result == null) result = caseTGGRule(ruleComponent2SubcomponentTypedAsParentRefined);
				if (result == null) result = caseTGGMapping(ruleComponent2SubcomponentTypedAsParentRefined);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED_TYPED: {
				RuleComponent2SubcomponentTypedRefinedTyped ruleComponent2SubcomponentTypedRefinedTyped = (RuleComponent2SubcomponentTypedRefinedTyped)theEObject;
				T result = caseRuleComponent2SubcomponentTypedRefinedTyped(ruleComponent2SubcomponentTypedRefinedTyped);
				if (result == null) result = caseTGGRule(ruleComponent2SubcomponentTypedRefinedTyped);
				if (result == null) result = caseTGGMapping(ruleComponent2SubcomponentTypedRefinedTyped);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT_REFINED_TYPED: {
				RuleComponent2SubcomponentTypedAsParentRefinedTyped ruleComponent2SubcomponentTypedAsParentRefinedTyped = (RuleComponent2SubcomponentTypedAsParentRefinedTyped)theEObject;
				T result = caseRuleComponent2SubcomponentTypedAsParentRefinedTyped(ruleComponent2SubcomponentTypedAsParentRefinedTyped);
				if (result == null) result = caseTGGRule(ruleComponent2SubcomponentTypedAsParentRefinedTyped);
				if (result == null) result = caseTGGMapping(ruleComponent2SubcomponentTypedAsParentRefinedTyped);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED_TYPED_AS_PARENT: {
				RuleComponent2SubcomponentTypedRefinedTypedAsParent ruleComponent2SubcomponentTypedRefinedTypedAsParent = (RuleComponent2SubcomponentTypedRefinedTypedAsParent)theEObject;
				T result = caseRuleComponent2SubcomponentTypedRefinedTypedAsParent(ruleComponent2SubcomponentTypedRefinedTypedAsParent);
				if (result == null) result = caseTGGRule(ruleComponent2SubcomponentTypedRefinedTypedAsParent);
				if (result == null) result = caseTGGMapping(ruleComponent2SubcomponentTypedRefinedTypedAsParent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_DIRECTED_FEAT2_DIRECTED_FEAT: {
				RuleDirectedFeat2DirectedFeat ruleDirectedFeat2DirectedFeat = (RuleDirectedFeat2DirectedFeat)theEObject;
				T result = caseRuleDirectedFeat2DirectedFeat(ruleDirectedFeat2DirectedFeat);
				if (result == null) result = caseTGGRule(ruleDirectedFeat2DirectedFeat);
				if (result == null) result = caseTGGMapping(ruleDirectedFeat2DirectedFeat);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_DIRECTED_FEAT2_DIRECTED_FEAT_TYPED: {
				RuleDirectedFeat2DirectedFeatTyped ruleDirectedFeat2DirectedFeatTyped = (RuleDirectedFeat2DirectedFeatTyped)theEObject;
				T result = caseRuleDirectedFeat2DirectedFeatTyped(ruleDirectedFeat2DirectedFeatTyped);
				if (result == null) result = caseTGGRule(ruleDirectedFeat2DirectedFeatTyped);
				if (result == null) result = caseTGGMapping(ruleDirectedFeat2DirectedFeatTyped);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_DIRECTED_FEAT2_DIRECTED_FEAT_REFINED: {
				RuleDirectedFeat2DirectedFeatRefined ruleDirectedFeat2DirectedFeatRefined = (RuleDirectedFeat2DirectedFeatRefined)theEObject;
				T result = caseRuleDirectedFeat2DirectedFeatRefined(ruleDirectedFeat2DirectedFeatRefined);
				if (result == null) result = caseTGGRule(ruleDirectedFeat2DirectedFeatRefined);
				if (result == null) result = caseTGGMapping(ruleDirectedFeat2DirectedFeatRefined);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_DIRECTED_FEAT2_DIRECTED_FEAT_TYPED_REFINED: {
				RuleDirectedFeat2DirectedFeatTypedRefined ruleDirectedFeat2DirectedFeatTypedRefined = (RuleDirectedFeat2DirectedFeatTypedRefined)theEObject;
				T result = caseRuleDirectedFeat2DirectedFeatTypedRefined(ruleDirectedFeat2DirectedFeatTypedRefined);
				if (result == null) result = caseTGGRule(ruleDirectedFeat2DirectedFeatTypedRefined);
				if (result == null) result = caseTGGMapping(ruleDirectedFeat2DirectedFeatTypedRefined);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_ACCESS2_ACCESS: {
				RuleAccess2Access ruleAccess2Access = (RuleAccess2Access)theEObject;
				T result = caseRuleAccess2Access(ruleAccess2Access);
				if (result == null) result = caseTGGRule(ruleAccess2Access);
				if (result == null) result = caseTGGMapping(ruleAccess2Access);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_ACCESS2_ACCESS_TYPED: {
				RuleAccess2AccessTyped ruleAccess2AccessTyped = (RuleAccess2AccessTyped)theEObject;
				T result = caseRuleAccess2AccessTyped(ruleAccess2AccessTyped);
				if (result == null) result = caseTGGRule(ruleAccess2AccessTyped);
				if (result == null) result = caseTGGMapping(ruleAccess2AccessTyped);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_ACCESS2_ACCESS_TYPED_AS_PARENT: {
				RuleAccess2AccessTypedAsParent ruleAccess2AccessTypedAsParent = (RuleAccess2AccessTypedAsParent)theEObject;
				T result = caseRuleAccess2AccessTypedAsParent(ruleAccess2AccessTypedAsParent);
				if (result == null) result = caseTGGRule(ruleAccess2AccessTypedAsParent);
				if (result == null) result = caseTGGMapping(ruleAccess2AccessTypedAsParent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_ACCESS2_ACCESS_REFINED: {
				RuleAccess2AccessRefined ruleAccess2AccessRefined = (RuleAccess2AccessRefined)theEObject;
				T result = caseRuleAccess2AccessRefined(ruleAccess2AccessRefined);
				if (result == null) result = caseTGGRule(ruleAccess2AccessRefined);
				if (result == null) result = caseTGGMapping(ruleAccess2AccessRefined);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_ACCESS2_ACCESS_TYPED_REFINED: {
				RuleAccess2AccessTypedRefined ruleAccess2AccessTypedRefined = (RuleAccess2AccessTypedRefined)theEObject;
				T result = caseRuleAccess2AccessTypedRefined(ruleAccess2AccessTypedRefined);
				if (result == null) result = caseTGGRule(ruleAccess2AccessTypedRefined);
				if (result == null) result = caseTGGMapping(ruleAccess2AccessTypedRefined);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_ACCESS2_ACCESS_TYPED_AS_PARENT_REFINED: {
				RuleAccess2AccessTypedAsParentRefined ruleAccess2AccessTypedAsParentRefined = (RuleAccess2AccessTypedAsParentRefined)theEObject;
				T result = caseRuleAccess2AccessTypedAsParentRefined(ruleAccess2AccessTypedAsParentRefined);
				if (result == null) result = caseTGGRule(ruleAccess2AccessTypedAsParentRefined);
				if (result == null) result = caseTGGMapping(ruleAccess2AccessTypedAsParentRefined);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT: {
				RuleFeatGroupDirFeat2FeatGroupDirFeat ruleFeatGroupDirFeat2FeatGroupDirFeat = (RuleFeatGroupDirFeat2FeatGroupDirFeat)theEObject;
				T result = caseRuleFeatGroupDirFeat2FeatGroupDirFeat(ruleFeatGroupDirFeat2FeatGroupDirFeat);
				if (result == null) result = caseTGGRule(ruleFeatGroupDirFeat2FeatGroupDirFeat);
				if (result == null) result = caseTGGMapping(ruleFeatGroupDirFeat2FeatGroupDirFeat);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED: {
				RuleFeatGroupDirFeat2FeatGroupDirFeatTyped ruleFeatGroupDirFeat2FeatGroupDirFeatTyped = (RuleFeatGroupDirFeat2FeatGroupDirFeatTyped)theEObject;
				T result = caseRuleFeatGroupDirFeat2FeatGroupDirFeatTyped(ruleFeatGroupDirFeat2FeatGroupDirFeatTyped);
				if (result == null) result = caseTGGRule(ruleFeatGroupDirFeat2FeatGroupDirFeatTyped);
				if (result == null) result = caseTGGMapping(ruleFeatGroupDirFeat2FeatGroupDirFeatTyped);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_AS_PARENT: {
				RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent = (RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent)theEObject;
				T result = caseRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent(ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent);
				if (result == null) result = caseTGGRule(ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent);
				if (result == null) result = caseTGGMapping(ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_REFINED: {
				RuleFeatGroupDirFeat2FeatGroupDirFeatRefined ruleFeatGroupDirFeat2FeatGroupDirFeatRefined = (RuleFeatGroupDirFeat2FeatGroupDirFeatRefined)theEObject;
				T result = caseRuleFeatGroupDirFeat2FeatGroupDirFeatRefined(ruleFeatGroupDirFeat2FeatGroupDirFeatRefined);
				if (result == null) result = caseTGGRule(ruleFeatGroupDirFeat2FeatGroupDirFeatRefined);
				if (result == null) result = caseTGGMapping(ruleFeatGroupDirFeat2FeatGroupDirFeatRefined);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_REFINED: {
				RuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined ruleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined = (RuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined)theEObject;
				T result = caseRuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined(ruleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined);
				if (result == null) result = caseTGGRule(ruleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined);
				if (result == null) result = caseTGGMapping(ruleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_AS_PARENT_REFINED: {
				RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined = (RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined)theEObject;
				T result = caseRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined(ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined);
				if (result == null) result = caseTGGRule(ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined);
				if (result == null) result = caseTGGMapping(ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS: {
				RuleFeatGroupAccess2FeatGroupAccess ruleFeatGroupAccess2FeatGroupAccess = (RuleFeatGroupAccess2FeatGroupAccess)theEObject;
				T result = caseRuleFeatGroupAccess2FeatGroupAccess(ruleFeatGroupAccess2FeatGroupAccess);
				if (result == null) result = caseTGGRule(ruleFeatGroupAccess2FeatGroupAccess);
				if (result == null) result = caseTGGMapping(ruleFeatGroupAccess2FeatGroupAccess);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_TYPED: {
				RuleFeatGroupAccess2FeatGroupAccessTyped ruleFeatGroupAccess2FeatGroupAccessTyped = (RuleFeatGroupAccess2FeatGroupAccessTyped)theEObject;
				T result = caseRuleFeatGroupAccess2FeatGroupAccessTyped(ruleFeatGroupAccess2FeatGroupAccessTyped);
				if (result == null) result = caseTGGRule(ruleFeatGroupAccess2FeatGroupAccessTyped);
				if (result == null) result = caseTGGMapping(ruleFeatGroupAccess2FeatGroupAccessTyped);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_REFINED: {
				RuleFeatGroupAccess2FeatGroupAccessRefined ruleFeatGroupAccess2FeatGroupAccessRefined = (RuleFeatGroupAccess2FeatGroupAccessRefined)theEObject;
				T result = caseRuleFeatGroupAccess2FeatGroupAccessRefined(ruleFeatGroupAccess2FeatGroupAccessRefined);
				if (result == null) result = caseTGGRule(ruleFeatGroupAccess2FeatGroupAccessRefined);
				if (result == null) result = caseTGGMapping(ruleFeatGroupAccess2FeatGroupAccessRefined);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_TYPED_REFINED: {
				RuleFeatGroupAccess2FeatGroupAccessTypedRefined ruleFeatGroupAccess2FeatGroupAccessTypedRefined = (RuleFeatGroupAccess2FeatGroupAccessTypedRefined)theEObject;
				T result = caseRuleFeatGroupAccess2FeatGroupAccessTypedRefined(ruleFeatGroupAccess2FeatGroupAccessTypedRefined);
				if (result == null) result = caseTGGRule(ruleFeatGroupAccess2FeatGroupAccessTypedRefined);
				if (result == null) result = caseTGGMapping(ruleFeatGroupAccess2FeatGroupAccessTypedRefined);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_DEST_SUBCOMPO: {
				RuleDirFeatConn2DirFeatConnSrcParentDestSubcompo ruleDirFeatConn2DirFeatConnSrcParentDestSubcompo = (RuleDirFeatConn2DirFeatConnSrcParentDestSubcompo)theEObject;
				T result = caseRuleDirFeatConn2DirFeatConnSrcParentDestSubcompo(ruleDirFeatConn2DirFeatConnSrcParentDestSubcompo);
				if (result == null) result = caseTGGRule(ruleDirFeatConn2DirFeatConnSrcParentDestSubcompo);
				if (result == null) result = caseTGGMapping(ruleDirFeatConn2DirFeatConnSrcParentDestSubcompo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_DEST_SUBCOMPO_SAME_PORT: {
				RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort ruleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort = (RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort)theEObject;
				T result = caseRuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort(ruleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort);
				if (result == null) result = caseTGGRule(ruleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort);
				if (result == null) result = caseTGGMapping(ruleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT: {
				RuleDirFeatConn2DirFeatConnSrcSubcompoDestParent ruleDirFeatConn2DirFeatConnSrcSubcompoDestParent = (RuleDirFeatConn2DirFeatConnSrcSubcompoDestParent)theEObject;
				T result = caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestParent(ruleDirFeatConn2DirFeatConnSrcSubcompoDestParent);
				if (result == null) result = caseTGGRule(ruleDirFeatConn2DirFeatConnSrcSubcompoDestParent);
				if (result == null) result = caseTGGMapping(ruleDirFeatConn2DirFeatConnSrcSubcompoDestParent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_FEAT_GROUP: {
				RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup = (RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup)theEObject;
				T result = caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup(ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup);
				if (result == null) result = caseTGGRule(ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup);
				if (result == null) result = caseTGGMapping(ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_FEAT_GROUP_INVERSE: {
				RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse = (RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse)theEObject;
				T result = caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse(ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse);
				if (result == null) result = caseTGGRule(ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse);
				if (result == null) result = caseTGGMapping(ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_FEAT_GROUP_DEST_SUBCOMPO: {
				RuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo ruleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo = (RuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo)theEObject;
				T result = caseRuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo(ruleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo);
				if (result == null) result = caseTGGRule(ruleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo);
				if (result == null) result = caseTGGMapping(ruleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_FEAT_GROUP_INVERSE_DEST_SUBCOMPO: {
				RuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo ruleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo = (RuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo)theEObject;
				T result = caseRuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo(ruleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo);
				if (result == null) result = caseTGGRule(ruleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo);
				if (result == null) result = caseTGGMapping(ruleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_SAME_PORT: {
				RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort = (RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort)theEObject;
				T result = caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort(ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort);
				if (result == null) result = caseTGGRule(ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort);
				if (result == null) result = caseTGGMapping(ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO: {
				RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo = (RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo)theEObject;
				T result = caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo(ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo);
				if (result == null) result = caseTGGRule(ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo);
				if (result == null) result = caseTGGMapping(ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO_SAME_PORT: {
				RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort = (RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort)theEObject;
				T result = caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort(ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort);
				if (result == null) result = caseTGGRule(ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort);
				if (result == null) result = caseTGGMapping(ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO_FEAT: {
				RuleAccessConn2AccessConnSrcParentDestSubcompoFeat ruleAccessConn2AccessConnSrcParentDestSubcompoFeat = (RuleAccessConn2AccessConnSrcParentDestSubcompoFeat)theEObject;
				T result = caseRuleAccessConn2AccessConnSrcParentDestSubcompoFeat(ruleAccessConn2AccessConnSrcParentDestSubcompoFeat);
				if (result == null) result = caseTGGRule(ruleAccessConn2AccessConnSrcParentDestSubcompoFeat);
				if (result == null) result = caseTGGMapping(ruleAccessConn2AccessConnSrcParentDestSubcompoFeat);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO_FEAT_SAME_ACCESS: {
				RuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess ruleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess = (RuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess)theEObject;
				T result = caseRuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess(ruleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess);
				if (result == null) result = caseTGGRule(ruleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess);
				if (result == null) result = caseTGGMapping(ruleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_PARENT: {
				RuleAccessConn2AccessConnSrcSubcompoFeatDestParent ruleAccessConn2AccessConnSrcSubcompoFeatDestParent = (RuleAccessConn2AccessConnSrcSubcompoFeatDestParent)theEObject;
				T result = caseRuleAccessConn2AccessConnSrcSubcompoFeatDestParent(ruleAccessConn2AccessConnSrcSubcompoFeatDestParent);
				if (result == null) result = caseTGGRule(ruleAccessConn2AccessConnSrcSubcompoFeatDestParent);
				if (result == null) result = caseTGGMapping(ruleAccessConn2AccessConnSrcSubcompoFeatDestParent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_PARENT_SAME_ACCESS: {
				RuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess ruleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess = (RuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess)theEObject;
				T result = caseRuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess(ruleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess);
				if (result == null) result = caseTGGRule(ruleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess);
				if (result == null) result = caseTGGMapping(ruleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_SUBCOMPO_FEAT: {
				RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat = (RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat)theEObject;
				T result = caseRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat(ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat);
				if (result == null) result = caseTGGRule(ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat);
				if (result == null) result = caseTGGMapping(ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_PARENT: {
				RuleAccessConn2AccessConnSrcSubcompoDestParent ruleAccessConn2AccessConnSrcSubcompoDestParent = (RuleAccessConn2AccessConnSrcSubcompoDestParent)theEObject;
				T result = caseRuleAccessConn2AccessConnSrcSubcompoDestParent(ruleAccessConn2AccessConnSrcSubcompoDestParent);
				if (result == null) result = caseTGGRule(ruleAccessConn2AccessConnSrcSubcompoDestParent);
				if (result == null) result = caseTGGMapping(ruleAccessConn2AccessConnSrcSubcompoDestParent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO_FEAT: {
				RuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat ruleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat = (RuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat)theEObject;
				T result = caseRuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat(ruleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat);
				if (result == null) result = caseTGGRule(ruleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat);
				if (result == null) result = caseTGGMapping(ruleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO: {
				RuleAccessConn2AccessConnSrcSubcompoDestSubcompo ruleAccessConn2AccessConnSrcSubcompoDestSubcompo = (RuleAccessConn2AccessConnSrcSubcompoDestSubcompo)theEObject;
				T result = caseRuleAccessConn2AccessConnSrcSubcompoDestSubcompo(ruleAccessConn2AccessConnSrcSubcompoDestSubcompo);
				if (result == null) result = caseTGGRule(ruleAccessConn2AccessConnSrcSubcompoDestSubcompo);
				if (result == null) result = caseTGGMapping(ruleAccessConn2AccessConnSrcSubcompoDestSubcompo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO: {
				RuleAccessConn2AccessConnSrcParentDestSubcompo ruleAccessConn2AccessConnSrcParentDestSubcompo = (RuleAccessConn2AccessConnSrcParentDestSubcompo)theEObject;
				T result = caseRuleAccessConn2AccessConnSrcParentDestSubcompo(ruleAccessConn2AccessConnSrcParentDestSubcompo);
				if (result == null) result = caseTGGRule(ruleAccessConn2AccessConnSrcParentDestSubcompo);
				if (result == null) result = caseTGGMapping(ruleAccessConn2AccessConnSrcParentDestSubcompo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_SUBCOMPO: {
				RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo = (RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo)theEObject;
				T result = caseRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo(ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo);
				if (result == null) result = caseTGGRule(ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo);
				if (result == null) result = caseTGGMapping(ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Corr Axiom</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Corr Axiom</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrAxiom(CorrAxiom object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Corr Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Corr Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrComponent(CorrComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Corr Component Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Corr Component Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrComponentType(CorrComponentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Corr Component Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Corr Component Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrComponentImplementation(CorrComponentImplementation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Corr Subcomponent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Corr Subcomponent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrSubcomponent(CorrSubcomponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Corr Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Corr Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrFeature(CorrFeature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Corr Directed Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Corr Directed Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrDirectedFeature(CorrDirectedFeature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Corr Access</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Corr Access</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrAccess(CorrAccess object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Corr Directed Feature Connection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Corr Directed Feature Connection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrDirectedFeatureConnection(
			CorrDirectedFeatureConnection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Corr Access Connection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Corr Access Connection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrAccessConnection(CorrAccessConnection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAdele2aadlRuleSet(Adele2aadlRuleSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Package2 Aadl Package</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Package2 Aadl Package</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRulePackage2AadlPackage(RulePackage2AadlPackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Component2 Component Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Component2 Component Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleComponent2ComponentType(RuleComponent2ComponentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Component2 Component Type With Extends</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Component2 Component Type With Extends</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleComponent2ComponentTypeWithExtends(
			RuleComponent2ComponentTypeWithExtends object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Feature Group2 Feature Group Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Feature Group2 Feature Group Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleFeatureGroup2FeatureGroupType(
			RuleFeatureGroup2FeatureGroupType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Feature Group2 Feature Group Type With Inverse</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Feature Group2 Feature Group Type With Inverse</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleFeatureGroup2FeatureGroupTypeWithInverse(
			RuleFeatureGroup2FeatureGroupTypeWithInverse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Feature Group2 Feature Group Type With Extends</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Feature Group2 Feature Group Type With Extends</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleFeatureGroup2FeatureGroupTypeWithExtends(
			RuleFeatureGroup2FeatureGroupTypeWithExtends object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Feature Group2 Feature Group Type With Extends Inverse</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Feature Group2 Feature Group Type With Extends Inverse</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleFeatureGroup2FeatureGroupTypeWithExtendsInverse(
			RuleFeatureGroup2FeatureGroupTypeWithExtendsInverse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Component2 Component Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Component2 Component Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleComponent2ComponentImplementation(
			RuleComponent2ComponentImplementation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Component2 Component Implementation With Extends</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Component2 Component Implementation With Extends</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleComponent2ComponentImplementationWithExtends(
			RuleComponent2ComponentImplementationWithExtends object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleComponent2Subcomponent(RuleComponent2Subcomponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Typed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Typed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleComponent2SubcomponentTyped(
			RuleComponent2SubcomponentTyped object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Typed As Parent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Typed As Parent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleComponent2SubcomponentTypedAsParent(
			RuleComponent2SubcomponentTypedAsParent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Refined</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Refined</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleComponent2SubcomponentRefined(
			RuleComponent2SubcomponentRefined object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Refined Typed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Refined Typed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleComponent2SubcomponentRefinedTyped(
			RuleComponent2SubcomponentRefinedTyped object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Refined Typed As Parent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Refined Typed As Parent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleComponent2SubcomponentRefinedTypedAsParent(
			RuleComponent2SubcomponentRefinedTypedAsParent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Typed Refined</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Typed Refined</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleComponent2SubcomponentTypedRefined(
			RuleComponent2SubcomponentTypedRefined object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Typed As Parent Refined</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Typed As Parent Refined</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleComponent2SubcomponentTypedAsParentRefined(
			RuleComponent2SubcomponentTypedAsParentRefined object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Typed Refined Typed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Typed Refined Typed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleComponent2SubcomponentTypedRefinedTyped(
			RuleComponent2SubcomponentTypedRefinedTyped object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Typed As Parent Refined Typed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Typed As Parent Refined Typed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleComponent2SubcomponentTypedAsParentRefinedTyped(
			RuleComponent2SubcomponentTypedAsParentRefinedTyped object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Typed Refined Typed As Parent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Component2 Subcomponent Typed Refined Typed As Parent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleComponent2SubcomponentTypedRefinedTypedAsParent(
			RuleComponent2SubcomponentTypedRefinedTypedAsParent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Directed Feat2 Directed Feat</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Directed Feat2 Directed Feat</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleDirectedFeat2DirectedFeat(
			RuleDirectedFeat2DirectedFeat object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Directed Feat2 Directed Feat Typed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Directed Feat2 Directed Feat Typed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleDirectedFeat2DirectedFeatTyped(
			RuleDirectedFeat2DirectedFeatTyped object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Directed Feat2 Directed Feat Refined</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Directed Feat2 Directed Feat Refined</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleDirectedFeat2DirectedFeatRefined(
			RuleDirectedFeat2DirectedFeatRefined object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Directed Feat2 Directed Feat Typed Refined</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Directed Feat2 Directed Feat Typed Refined</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleDirectedFeat2DirectedFeatTypedRefined(
			RuleDirectedFeat2DirectedFeatTypedRefined object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Access2 Access</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Access2 Access</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleAccess2Access(RuleAccess2Access object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Access2 Access Typed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Access2 Access Typed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleAccess2AccessTyped(RuleAccess2AccessTyped object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Access2 Access Typed As Parent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Access2 Access Typed As Parent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleAccess2AccessTypedAsParent(
			RuleAccess2AccessTypedAsParent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Access2 Access Refined</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Access2 Access Refined</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleAccess2AccessRefined(RuleAccess2AccessRefined object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Access2 Access Typed Refined</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Access2 Access Typed Refined</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleAccess2AccessTypedRefined(
			RuleAccess2AccessTypedRefined object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Access2 Access Typed As Parent Refined</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Access2 Access Typed As Parent Refined</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleAccess2AccessTypedAsParentRefined(
			RuleAccess2AccessTypedAsParentRefined object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleFeatGroupDirFeat2FeatGroupDirFeat(
			RuleFeatGroupDirFeat2FeatGroupDirFeat object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleFeatGroupDirFeat2FeatGroupDirFeatTyped(
			RuleFeatGroupDirFeat2FeatGroupDirFeatTyped object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent(
			RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Refined</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Refined</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleFeatGroupDirFeat2FeatGroupDirFeatRefined(
			RuleFeatGroupDirFeat2FeatGroupDirFeatRefined object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed Refined</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed Refined</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined(
			RuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent Refined</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent Refined</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined(
			RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Feat Group Access2 Feat Group Access</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Feat Group Access2 Feat Group Access</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleFeatGroupAccess2FeatGroupAccess(
			RuleFeatGroupAccess2FeatGroupAccess object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Feat Group Access2 Feat Group Access Typed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Feat Group Access2 Feat Group Access Typed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleFeatGroupAccess2FeatGroupAccessTyped(
			RuleFeatGroupAccess2FeatGroupAccessTyped object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Feat Group Access2 Feat Group Access Refined</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Feat Group Access2 Feat Group Access Refined</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleFeatGroupAccess2FeatGroupAccessRefined(
			RuleFeatGroupAccess2FeatGroupAccessRefined object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Feat Group Access2 Feat Group Access Typed Refined</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Feat Group Access2 Feat Group Access Typed Refined</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleFeatGroupAccess2FeatGroupAccessTypedRefined(
			RuleFeatGroupAccess2FeatGroupAccessTypedRefined object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleDirFeatConn2DirFeatConnSrcParentDestSubcompo(
			RuleDirFeatConn2DirFeatConnSrcParentDestSubcompo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo Same Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo Same Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort(
			RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestParent(
			RuleDirFeatConn2DirFeatConnSrcSubcompoDestParent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup(
			RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group Inverse</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group Inverse</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse(
			RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Dest Subcompo</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Dest Subcompo</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo(
			RuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Inverse Dest Subcompo</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Inverse Dest Subcompo</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo(
			RuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Same Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Same Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort(
			RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo(
			RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo Same Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo Same Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort(
			RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleAccessConn2AccessConnSrcParentDestSubcompoFeat(
			RuleAccessConn2AccessConnSrcParentDestSubcompoFeat object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat Same Access</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat Same Access</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess(
			RuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleAccessConn2AccessConnSrcSubcompoFeatDestParent(
			RuleAccessConn2AccessConnSrcSubcompoFeatDestParent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent Same Access</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent Same Access</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess(
			RuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo Feat</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo Feat</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat(
			RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Parent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Parent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleAccessConn2AccessConnSrcSubcompoDestParent(
			RuleAccessConn2AccessConnSrcSubcompoDestParent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo Feat</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo Feat</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat(
			RuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleAccessConn2AccessConnSrcSubcompoDestSubcompo(
			RuleAccessConn2AccessConnSrcSubcompoDestSubcompo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleAccessConn2AccessConnSrcParentDestSubcompo(
			RuleAccessConn2AccessConnSrcParentDestSubcompo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo(
			RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TGG Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TGG Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTGGNode(TGGNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TGG Rule Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TGG Rule Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTGGRuleSet(TGGRuleSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TGG Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TGG Mapping</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTGGMapping(TGGMapping object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TGG Axiom</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TGG Axiom</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTGGAxiom(TGGAxiom object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TGG Rule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TGG Rule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTGGRule(TGGRule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //Adele2aadlSwitch
