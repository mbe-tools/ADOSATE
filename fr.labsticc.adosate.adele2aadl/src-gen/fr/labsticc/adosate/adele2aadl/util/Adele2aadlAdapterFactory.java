/**
 */
package fr.labsticc.adosate.adele2aadl.util;

import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.rules.TGGAxiom;
import de.hpi.sam.mote.rules.TGGMapping;
import de.hpi.sam.mote.rules.TGGRule;
import de.hpi.sam.mote.rules.TGGRuleSet;
import fr.labsticc.adosate.adele2aadl.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlPackage
 * @generated
 */
public class Adele2aadlAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Adele2aadlPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Adele2aadlAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = Adele2aadlPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Adele2aadlSwitch<Adapter> modelSwitch = new Adele2aadlSwitch<Adapter>() {
			@Override
			public Adapter caseCorrAxiom(CorrAxiom object) {
				return createCorrAxiomAdapter();
			}
			@Override
			public Adapter caseCorrComponent(CorrComponent object) {
				return createCorrComponentAdapter();
			}
			@Override
			public Adapter caseCorrComponentType(CorrComponentType object) {
				return createCorrComponentTypeAdapter();
			}
			@Override
			public Adapter caseCorrComponentImplementation(CorrComponentImplementation object) {
				return createCorrComponentImplementationAdapter();
			}
			@Override
			public Adapter caseCorrSubcomponent(CorrSubcomponent object) {
				return createCorrSubcomponentAdapter();
			}
			@Override
			public Adapter caseCorrFeature(CorrFeature object) {
				return createCorrFeatureAdapter();
			}
			@Override
			public Adapter caseCorrDirectedFeature(CorrDirectedFeature object) {
				return createCorrDirectedFeatureAdapter();
			}
			@Override
			public Adapter caseCorrAccess(CorrAccess object) {
				return createCorrAccessAdapter();
			}
			@Override
			public Adapter caseCorrDirectedFeatureConnection(CorrDirectedFeatureConnection object) {
				return createCorrDirectedFeatureConnectionAdapter();
			}
			@Override
			public Adapter caseCorrAccessConnection(CorrAccessConnection object) {
				return createCorrAccessConnectionAdapter();
			}
			@Override
			public Adapter caseAdele2aadlRuleSet(Adele2aadlRuleSet object) {
				return createAdele2aadlRuleSetAdapter();
			}
			@Override
			public Adapter caseRulePackage2AadlPackage(RulePackage2AadlPackage object) {
				return createRulePackage2AadlPackageAdapter();
			}
			@Override
			public Adapter caseRuleComponent2ComponentType(RuleComponent2ComponentType object) {
				return createRuleComponent2ComponentTypeAdapter();
			}
			@Override
			public Adapter caseRuleComponent2ComponentTypeWithExtends(RuleComponent2ComponentTypeWithExtends object) {
				return createRuleComponent2ComponentTypeWithExtendsAdapter();
			}
			@Override
			public Adapter caseRuleFeatureGroup2FeatureGroupType(RuleFeatureGroup2FeatureGroupType object) {
				return createRuleFeatureGroup2FeatureGroupTypeAdapter();
			}
			@Override
			public Adapter caseRuleFeatureGroup2FeatureGroupTypeWithInverse(RuleFeatureGroup2FeatureGroupTypeWithInverse object) {
				return createRuleFeatureGroup2FeatureGroupTypeWithInverseAdapter();
			}
			@Override
			public Adapter caseRuleFeatureGroup2FeatureGroupTypeWithExtends(RuleFeatureGroup2FeatureGroupTypeWithExtends object) {
				return createRuleFeatureGroup2FeatureGroupTypeWithExtendsAdapter();
			}
			@Override
			public Adapter caseRuleFeatureGroup2FeatureGroupTypeWithExtendsInverse(RuleFeatureGroup2FeatureGroupTypeWithExtendsInverse object) {
				return createRuleFeatureGroup2FeatureGroupTypeWithExtendsInverseAdapter();
			}
			@Override
			public Adapter caseRuleComponent2ComponentImplementation(RuleComponent2ComponentImplementation object) {
				return createRuleComponent2ComponentImplementationAdapter();
			}
			@Override
			public Adapter caseRuleComponent2ComponentImplementationWithExtends(RuleComponent2ComponentImplementationWithExtends object) {
				return createRuleComponent2ComponentImplementationWithExtendsAdapter();
			}
			@Override
			public Adapter caseRuleComponent2Subcomponent(RuleComponent2Subcomponent object) {
				return createRuleComponent2SubcomponentAdapter();
			}
			@Override
			public Adapter caseRuleComponent2SubcomponentTyped(RuleComponent2SubcomponentTyped object) {
				return createRuleComponent2SubcomponentTypedAdapter();
			}
			@Override
			public Adapter caseRuleComponent2SubcomponentTypedAsParent(RuleComponent2SubcomponentTypedAsParent object) {
				return createRuleComponent2SubcomponentTypedAsParentAdapter();
			}
			@Override
			public Adapter caseRuleComponent2SubcomponentRefined(RuleComponent2SubcomponentRefined object) {
				return createRuleComponent2SubcomponentRefinedAdapter();
			}
			@Override
			public Adapter caseRuleComponent2SubcomponentRefinedTyped(RuleComponent2SubcomponentRefinedTyped object) {
				return createRuleComponent2SubcomponentRefinedTypedAdapter();
			}
			@Override
			public Adapter caseRuleComponent2SubcomponentRefinedTypedAsParent(RuleComponent2SubcomponentRefinedTypedAsParent object) {
				return createRuleComponent2SubcomponentRefinedTypedAsParentAdapter();
			}
			@Override
			public Adapter caseRuleComponent2SubcomponentTypedRefined(RuleComponent2SubcomponentTypedRefined object) {
				return createRuleComponent2SubcomponentTypedRefinedAdapter();
			}
			@Override
			public Adapter caseRuleComponent2SubcomponentTypedAsParentRefined(RuleComponent2SubcomponentTypedAsParentRefined object) {
				return createRuleComponent2SubcomponentTypedAsParentRefinedAdapter();
			}
			@Override
			public Adapter caseRuleComponent2SubcomponentTypedRefinedTyped(RuleComponent2SubcomponentTypedRefinedTyped object) {
				return createRuleComponent2SubcomponentTypedRefinedTypedAdapter();
			}
			@Override
			public Adapter caseRuleComponent2SubcomponentTypedAsParentRefinedTyped(RuleComponent2SubcomponentTypedAsParentRefinedTyped object) {
				return createRuleComponent2SubcomponentTypedAsParentRefinedTypedAdapter();
			}
			@Override
			public Adapter caseRuleComponent2SubcomponentTypedRefinedTypedAsParent(RuleComponent2SubcomponentTypedRefinedTypedAsParent object) {
				return createRuleComponent2SubcomponentTypedRefinedTypedAsParentAdapter();
			}
			@Override
			public Adapter caseRuleDirectedFeat2DirectedFeat(RuleDirectedFeat2DirectedFeat object) {
				return createRuleDirectedFeat2DirectedFeatAdapter();
			}
			@Override
			public Adapter caseRuleDirectedFeat2DirectedFeatTyped(RuleDirectedFeat2DirectedFeatTyped object) {
				return createRuleDirectedFeat2DirectedFeatTypedAdapter();
			}
			@Override
			public Adapter caseRuleDirectedFeat2DirectedFeatRefined(RuleDirectedFeat2DirectedFeatRefined object) {
				return createRuleDirectedFeat2DirectedFeatRefinedAdapter();
			}
			@Override
			public Adapter caseRuleDirectedFeat2DirectedFeatTypedRefined(RuleDirectedFeat2DirectedFeatTypedRefined object) {
				return createRuleDirectedFeat2DirectedFeatTypedRefinedAdapter();
			}
			@Override
			public Adapter caseRuleAccess2Access(RuleAccess2Access object) {
				return createRuleAccess2AccessAdapter();
			}
			@Override
			public Adapter caseRuleAccess2AccessTyped(RuleAccess2AccessTyped object) {
				return createRuleAccess2AccessTypedAdapter();
			}
			@Override
			public Adapter caseRuleAccess2AccessTypedAsParent(RuleAccess2AccessTypedAsParent object) {
				return createRuleAccess2AccessTypedAsParentAdapter();
			}
			@Override
			public Adapter caseRuleAccess2AccessRefined(RuleAccess2AccessRefined object) {
				return createRuleAccess2AccessRefinedAdapter();
			}
			@Override
			public Adapter caseRuleAccess2AccessTypedRefined(RuleAccess2AccessTypedRefined object) {
				return createRuleAccess2AccessTypedRefinedAdapter();
			}
			@Override
			public Adapter caseRuleAccess2AccessTypedAsParentRefined(RuleAccess2AccessTypedAsParentRefined object) {
				return createRuleAccess2AccessTypedAsParentRefinedAdapter();
			}
			@Override
			public Adapter caseRuleFeatGroupDirFeat2FeatGroupDirFeat(RuleFeatGroupDirFeat2FeatGroupDirFeat object) {
				return createRuleFeatGroupDirFeat2FeatGroupDirFeatAdapter();
			}
			@Override
			public Adapter caseRuleFeatGroupDirFeat2FeatGroupDirFeatTyped(RuleFeatGroupDirFeat2FeatGroupDirFeatTyped object) {
				return createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAdapter();
			}
			@Override
			public Adapter caseRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent(RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent object) {
				return createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentAdapter();
			}
			@Override
			public Adapter caseRuleFeatGroupDirFeat2FeatGroupDirFeatRefined(RuleFeatGroupDirFeat2FeatGroupDirFeatRefined object) {
				return createRuleFeatGroupDirFeat2FeatGroupDirFeatRefinedAdapter();
			}
			@Override
			public Adapter caseRuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined(RuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined object) {
				return createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefinedAdapter();
			}
			@Override
			public Adapter caseRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined(RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined object) {
				return createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefinedAdapter();
			}
			@Override
			public Adapter caseRuleFeatGroupAccess2FeatGroupAccess(RuleFeatGroupAccess2FeatGroupAccess object) {
				return createRuleFeatGroupAccess2FeatGroupAccessAdapter();
			}
			@Override
			public Adapter caseRuleFeatGroupAccess2FeatGroupAccessTyped(RuleFeatGroupAccess2FeatGroupAccessTyped object) {
				return createRuleFeatGroupAccess2FeatGroupAccessTypedAdapter();
			}
			@Override
			public Adapter caseRuleFeatGroupAccess2FeatGroupAccessRefined(RuleFeatGroupAccess2FeatGroupAccessRefined object) {
				return createRuleFeatGroupAccess2FeatGroupAccessRefinedAdapter();
			}
			@Override
			public Adapter caseRuleFeatGroupAccess2FeatGroupAccessTypedRefined(RuleFeatGroupAccess2FeatGroupAccessTypedRefined object) {
				return createRuleFeatGroupAccess2FeatGroupAccessTypedRefinedAdapter();
			}
			@Override
			public Adapter caseRuleDirFeatConn2DirFeatConnSrcParentDestSubcompo(RuleDirFeatConn2DirFeatConnSrcParentDestSubcompo object) {
				return createRuleDirFeatConn2DirFeatConnSrcParentDestSubcompoAdapter();
			}
			@Override
			public Adapter caseRuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort(RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort object) {
				return createRuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePortAdapter();
			}
			@Override
			public Adapter caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestParent(RuleDirFeatConn2DirFeatConnSrcSubcompoDestParent object) {
				return createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentAdapter();
			}
			@Override
			public Adapter caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup(RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup object) {
				return createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupAdapter();
			}
			@Override
			public Adapter caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse(RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse object) {
				return createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverseAdapter();
			}
			@Override
			public Adapter caseRuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo(RuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo object) {
				return createRuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompoAdapter();
			}
			@Override
			public Adapter caseRuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo(RuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo object) {
				return createRuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompoAdapter();
			}
			@Override
			public Adapter caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort(RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort object) {
				return createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePortAdapter();
			}
			@Override
			public Adapter caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo(RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo object) {
				return createRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoAdapter();
			}
			@Override
			public Adapter caseRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort(RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort object) {
				return createRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePortAdapter();
			}
			@Override
			public Adapter caseRuleAccessConn2AccessConnSrcParentDestSubcompoFeat(RuleAccessConn2AccessConnSrcParentDestSubcompoFeat object) {
				return createRuleAccessConn2AccessConnSrcParentDestSubcompoFeatAdapter();
			}
			@Override
			public Adapter caseRuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess(RuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess object) {
				return createRuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccessAdapter();
			}
			@Override
			public Adapter caseRuleAccessConn2AccessConnSrcSubcompoFeatDestParent(RuleAccessConn2AccessConnSrcSubcompoFeatDestParent object) {
				return createRuleAccessConn2AccessConnSrcSubcompoFeatDestParentAdapter();
			}
			@Override
			public Adapter caseRuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess(RuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess object) {
				return createRuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccessAdapter();
			}
			@Override
			public Adapter caseRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat(RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat object) {
				return createRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeatAdapter();
			}
			@Override
			public Adapter caseRuleAccessConn2AccessConnSrcSubcompoDestParent(RuleAccessConn2AccessConnSrcSubcompoDestParent object) {
				return createRuleAccessConn2AccessConnSrcSubcompoDestParentAdapter();
			}
			@Override
			public Adapter caseRuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat(RuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat object) {
				return createRuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeatAdapter();
			}
			@Override
			public Adapter caseRuleAccessConn2AccessConnSrcSubcompoDestSubcompo(RuleAccessConn2AccessConnSrcSubcompoDestSubcompo object) {
				return createRuleAccessConn2AccessConnSrcSubcompoDestSubcompoAdapter();
			}
			@Override
			public Adapter caseRuleAccessConn2AccessConnSrcParentDestSubcompo(RuleAccessConn2AccessConnSrcParentDestSubcompo object) {
				return createRuleAccessConn2AccessConnSrcParentDestSubcompoAdapter();
			}
			@Override
			public Adapter caseRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo(RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo object) {
				return createRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoAdapter();
			}
			@Override
			public Adapter caseTGGNode(TGGNode object) {
				return createTGGNodeAdapter();
			}
			@Override
			public Adapter caseTGGRuleSet(TGGRuleSet object) {
				return createTGGRuleSetAdapter();
			}
			@Override
			public Adapter caseTGGMapping(TGGMapping object) {
				return createTGGMappingAdapter();
			}
			@Override
			public Adapter caseTGGAxiom(TGGAxiom object) {
				return createTGGAxiomAdapter();
			}
			@Override
			public Adapter caseTGGRule(TGGRule object) {
				return createTGGRuleAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.CorrAxiom <em>Corr Axiom</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.CorrAxiom
	 * @generated
	 */
	public Adapter createCorrAxiomAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.CorrComponent <em>Corr Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.CorrComponent
	 * @generated
	 */
	public Adapter createCorrComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.CorrComponentType <em>Corr Component Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.CorrComponentType
	 * @generated
	 */
	public Adapter createCorrComponentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.CorrComponentImplementation <em>Corr Component Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.CorrComponentImplementation
	 * @generated
	 */
	public Adapter createCorrComponentImplementationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.CorrSubcomponent <em>Corr Subcomponent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.CorrSubcomponent
	 * @generated
	 */
	public Adapter createCorrSubcomponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.CorrFeature <em>Corr Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.CorrFeature
	 * @generated
	 */
	public Adapter createCorrFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.CorrDirectedFeature <em>Corr Directed Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.CorrDirectedFeature
	 * @generated
	 */
	public Adapter createCorrDirectedFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.CorrAccess <em>Corr Access</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.CorrAccess
	 * @generated
	 */
	public Adapter createCorrAccessAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.CorrDirectedFeatureConnection <em>Corr Directed Feature Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.CorrDirectedFeatureConnection
	 * @generated
	 */
	public Adapter createCorrDirectedFeatureConnectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.CorrAccessConnection <em>Corr Access Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.CorrAccessConnection
	 * @generated
	 */
	public Adapter createCorrAccessConnectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet <em>Rule Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet
	 * @generated
	 */
	public Adapter createAdele2aadlRuleSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RulePackage2AadlPackage <em>Rule Package2 Aadl Package</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RulePackage2AadlPackage
	 * @generated
	 */
	public Adapter createRulePackage2AadlPackageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2ComponentType <em>Rule Component2 Component Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2ComponentType
	 * @generated
	 */
	public Adapter createRuleComponent2ComponentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2ComponentTypeWithExtends <em>Rule Component2 Component Type With Extends</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2ComponentTypeWithExtends
	 * @generated
	 */
	public Adapter createRuleComponent2ComponentTypeWithExtendsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatureGroup2FeatureGroupType <em>Rule Feature Group2 Feature Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatureGroup2FeatureGroupType
	 * @generated
	 */
	public Adapter createRuleFeatureGroup2FeatureGroupTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatureGroup2FeatureGroupTypeWithInverse <em>Rule Feature Group2 Feature Group Type With Inverse</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatureGroup2FeatureGroupTypeWithInverse
	 * @generated
	 */
	public Adapter createRuleFeatureGroup2FeatureGroupTypeWithInverseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatureGroup2FeatureGroupTypeWithExtends <em>Rule Feature Group2 Feature Group Type With Extends</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatureGroup2FeatureGroupTypeWithExtends
	 * @generated
	 */
	public Adapter createRuleFeatureGroup2FeatureGroupTypeWithExtendsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatureGroup2FeatureGroupTypeWithExtendsInverse <em>Rule Feature Group2 Feature Group Type With Extends Inverse</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatureGroup2FeatureGroupTypeWithExtendsInverse
	 * @generated
	 */
	public Adapter createRuleFeatureGroup2FeatureGroupTypeWithExtendsInverseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2ComponentImplementation <em>Rule Component2 Component Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2ComponentImplementation
	 * @generated
	 */
	public Adapter createRuleComponent2ComponentImplementationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2ComponentImplementationWithExtends <em>Rule Component2 Component Implementation With Extends</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2ComponentImplementationWithExtends
	 * @generated
	 */
	public Adapter createRuleComponent2ComponentImplementationWithExtendsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2Subcomponent <em>Rule Component2 Subcomponent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2Subcomponent
	 * @generated
	 */
	public Adapter createRuleComponent2SubcomponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTyped <em>Rule Component2 Subcomponent Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTyped
	 * @generated
	 */
	public Adapter createRuleComponent2SubcomponentTypedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedAsParent <em>Rule Component2 Subcomponent Typed As Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedAsParent
	 * @generated
	 */
	public Adapter createRuleComponent2SubcomponentTypedAsParentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentRefined <em>Rule Component2 Subcomponent Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentRefined
	 * @generated
	 */
	public Adapter createRuleComponent2SubcomponentRefinedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentRefinedTyped <em>Rule Component2 Subcomponent Refined Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentRefinedTyped
	 * @generated
	 */
	public Adapter createRuleComponent2SubcomponentRefinedTypedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentRefinedTypedAsParent <em>Rule Component2 Subcomponent Refined Typed As Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentRefinedTypedAsParent
	 * @generated
	 */
	public Adapter createRuleComponent2SubcomponentRefinedTypedAsParentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedRefined <em>Rule Component2 Subcomponent Typed Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedRefined
	 * @generated
	 */
	public Adapter createRuleComponent2SubcomponentTypedRefinedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedAsParentRefined <em>Rule Component2 Subcomponent Typed As Parent Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedAsParentRefined
	 * @generated
	 */
	public Adapter createRuleComponent2SubcomponentTypedAsParentRefinedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedRefinedTyped <em>Rule Component2 Subcomponent Typed Refined Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedRefinedTyped
	 * @generated
	 */
	public Adapter createRuleComponent2SubcomponentTypedRefinedTypedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedAsParentRefinedTyped <em>Rule Component2 Subcomponent Typed As Parent Refined Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedAsParentRefinedTyped
	 * @generated
	 */
	public Adapter createRuleComponent2SubcomponentTypedAsParentRefinedTypedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedRefinedTypedAsParent <em>Rule Component2 Subcomponent Typed Refined Typed As Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleComponent2SubcomponentTypedRefinedTypedAsParent
	 * @generated
	 */
	public Adapter createRuleComponent2SubcomponentTypedRefinedTypedAsParentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleDirectedFeat2DirectedFeat <em>Rule Directed Feat2 Directed Feat</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirectedFeat2DirectedFeat
	 * @generated
	 */
	public Adapter createRuleDirectedFeat2DirectedFeatAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleDirectedFeat2DirectedFeatTyped <em>Rule Directed Feat2 Directed Feat Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirectedFeat2DirectedFeatTyped
	 * @generated
	 */
	public Adapter createRuleDirectedFeat2DirectedFeatTypedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleDirectedFeat2DirectedFeatRefined <em>Rule Directed Feat2 Directed Feat Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirectedFeat2DirectedFeatRefined
	 * @generated
	 */
	public Adapter createRuleDirectedFeat2DirectedFeatRefinedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleDirectedFeat2DirectedFeatTypedRefined <em>Rule Directed Feat2 Directed Feat Typed Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirectedFeat2DirectedFeatTypedRefined
	 * @generated
	 */
	public Adapter createRuleDirectedFeat2DirectedFeatTypedRefinedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleAccess2Access <em>Rule Access2 Access</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccess2Access
	 * @generated
	 */
	public Adapter createRuleAccess2AccessAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleAccess2AccessTyped <em>Rule Access2 Access Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccess2AccessTyped
	 * @generated
	 */
	public Adapter createRuleAccess2AccessTypedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleAccess2AccessTypedAsParent <em>Rule Access2 Access Typed As Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccess2AccessTypedAsParent
	 * @generated
	 */
	public Adapter createRuleAccess2AccessTypedAsParentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleAccess2AccessRefined <em>Rule Access2 Access Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccess2AccessRefined
	 * @generated
	 */
	public Adapter createRuleAccess2AccessRefinedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleAccess2AccessTypedRefined <em>Rule Access2 Access Typed Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccess2AccessTypedRefined
	 * @generated
	 */
	public Adapter createRuleAccess2AccessTypedRefinedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleAccess2AccessTypedAsParentRefined <em>Rule Access2 Access Typed As Parent Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccess2AccessTypedAsParentRefined
	 * @generated
	 */
	public Adapter createRuleAccess2AccessTypedAsParentRefinedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeat <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeat
	 * @generated
	 */
	public Adapter createRuleFeatGroupDirFeat2FeatGroupDirFeatAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTyped <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTyped
	 * @generated
	 */
	public Adapter createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent
	 * @generated
	 */
	public Adapter createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatRefined <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatRefined
	 * @generated
	 */
	public Adapter createRuleFeatGroupDirFeat2FeatGroupDirFeatRefinedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined
	 * @generated
	 */
	public Adapter createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefinedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined <em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined
	 * @generated
	 */
	public Adapter createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefinedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupAccess2FeatGroupAccess <em>Rule Feat Group Access2 Feat Group Access</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupAccess2FeatGroupAccess
	 * @generated
	 */
	public Adapter createRuleFeatGroupAccess2FeatGroupAccessAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupAccess2FeatGroupAccessTyped <em>Rule Feat Group Access2 Feat Group Access Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupAccess2FeatGroupAccessTyped
	 * @generated
	 */
	public Adapter createRuleFeatGroupAccess2FeatGroupAccessTypedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupAccess2FeatGroupAccessRefined <em>Rule Feat Group Access2 Feat Group Access Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupAccess2FeatGroupAccessRefined
	 * @generated
	 */
	public Adapter createRuleFeatGroupAccess2FeatGroupAccessRefinedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleFeatGroupAccess2FeatGroupAccessTypedRefined <em>Rule Feat Group Access2 Feat Group Access Typed Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleFeatGroupAccess2FeatGroupAccessTypedRefined
	 * @generated
	 */
	public Adapter createRuleFeatGroupAccess2FeatGroupAccessTypedRefinedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcParentDestSubcompo <em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcParentDestSubcompo
	 * @generated
	 */
	public Adapter createRuleDirFeatConn2DirFeatConnSrcParentDestSubcompoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort <em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo Same Port</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort
	 * @generated
	 */
	public Adapter createRuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParent <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParent
	 * @generated
	 */
	public Adapter createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup
	 * @generated
	 */
	public Adapter createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group Inverse</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse
	 * @generated
	 */
	public Adapter createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo <em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Dest Subcompo</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo
	 * @generated
	 */
	public Adapter createRuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo <em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Inverse Dest Subcompo</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo
	 * @generated
	 */
	public Adapter createRuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Same Port</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort
	 * @generated
	 */
	public Adapter createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo
	 * @generated
	 */
	public Adapter createRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort <em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo Same Port</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort
	 * @generated
	 */
	public Adapter createRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcParentDestSubcompoFeat <em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcParentDestSubcompoFeat
	 * @generated
	 */
	public Adapter createRuleAccessConn2AccessConnSrcParentDestSubcompoFeatAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess <em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat Same Access</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess
	 * @generated
	 */
	public Adapter createRuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccessAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoFeatDestParent <em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoFeatDestParent
	 * @generated
	 */
	public Adapter createRuleAccessConn2AccessConnSrcSubcompoFeatDestParentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess <em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent Same Access</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess
	 * @generated
	 */
	public Adapter createRuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccessAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat <em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo Feat</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat
	 * @generated
	 */
	public Adapter createRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeatAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoDestParent <em>Rule Access Conn2 Access Conn Src Subcompo Dest Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoDestParent
	 * @generated
	 */
	public Adapter createRuleAccessConn2AccessConnSrcSubcompoDestParentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat <em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo Feat</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat
	 * @generated
	 */
	public Adapter createRuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeatAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoDestSubcompo <em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoDestSubcompo
	 * @generated
	 */
	public Adapter createRuleAccessConn2AccessConnSrcSubcompoDestSubcompoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcParentDestSubcompo <em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcParentDestSubcompo
	 * @generated
	 */
	public Adapter createRuleAccessConn2AccessConnSrcParentDestSubcompoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo <em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.labsticc.adosate.adele2aadl.RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo
	 * @generated
	 */
	public Adapter createRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.mote.TGGNode <em>TGG Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.mote.TGGNode
	 * @generated
	 */
	public Adapter createTGGNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.mote.rules.TGGRuleSet <em>TGG Rule Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.mote.rules.TGGRuleSet
	 * @generated
	 */
	public Adapter createTGGRuleSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.mote.rules.TGGMapping <em>TGG Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.mote.rules.TGGMapping
	 * @generated
	 */
	public Adapter createTGGMappingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.mote.rules.TGGAxiom <em>TGG Axiom</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.mote.rules.TGGAxiom
	 * @generated
	 */
	public Adapter createTGGAxiomAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hpi.sam.mote.rules.TGGRule <em>TGG Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hpi.sam.mote.rules.TGGRule
	 * @generated
	 */
	public Adapter createTGGRuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //Adele2aadlAdapterFactory
