/**
 */
package fr.labsticc.adosate.adele2aadl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Access</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlPackage#getCorrAccess()
 * @model
 * @generated
 */
public interface CorrAccess extends CorrFeature {
} // CorrAccess
