/**
 */
package fr.labsticc.adosate.adele2aadl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Component Implementation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlPackage#getCorrComponentImplementation()
 * @model
 * @generated
 */
public interface CorrComponentImplementation extends CorrComponent {
} // CorrComponentImplementation
