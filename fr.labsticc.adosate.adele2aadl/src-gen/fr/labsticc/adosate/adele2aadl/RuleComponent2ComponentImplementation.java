/**
 */
package fr.labsticc.adosate.adele2aadl;

import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.impl.TransformationException;
import de.hpi.sam.mote.rules.TGGRule;
import de.hpi.sam.mote.rules.TransformationResult;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rule Component2 Component Implementation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlPackage#getRuleComponent2ComponentImplementation()
 * @model
 * @generated
 */
public interface RuleComponent2ComponentImplementation extends TGGRule {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="de.hpi.sam.mote.helpers.TransformationException"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\t//System.out.println( new java.util.Date() + \": Starting to execute rule \" + getClass().getSimpleName() + \" on parent element \" + parentCorrNode );\r\n\torg.eclipse.emf.ecore.resource.Resource r = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getResourceSet().getResource(\r\n\t\t\t\torg.eclipse.emf.common.util.URI.createPlatformPluginURI(\"/fr.labsticc.adosate.adele2aadl/model/story/RuleComponent2ComponentImplementation_forwardTransformation.story\", true), true);\r\n\t\t\r\n\t\tde.hpi.sam.storyDiagramEcore.ActivityDiagram ad = (de.hpi.sam.storyDiagramEcore.ActivityDiagram) r.getContents().get(0);\r\n\t\t\r\n\t\tde.hpi.sam.storyDiagramEcore.Activity a = ad.getActivities().get(0); \r\n\t\t\r\n\t\tde.mdelab.sdm.interpreter.sde.SDESDMInterpreter sdi = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getSdmInterpreter();\r\n\t\t//final de.mdelab.sdm.interpreter.sde.notifications.SDEOutputStreamNotificationReceiver receiver = new de.mdelab.sdm.interpreter.sde.notifications.SDEOutputStreamNotificationReceiver( null, (de.mdelab.sdm.interpreter.sde.facade.SDEMetamodelFacadeFactory) sdi.getFacadeFactory() );\r\n\t\t//sdi.getNotificationEmitter().addNotificationReceiver( receiver );\r\n\t\t\r\n\t\tjava.util.List<Object> parameters = new java.util.ArrayList<Object>();\r\n\t\t\t\t\r\n\t\t\tparameters.add(parentCorrNode);\r\n\t\t\r\n\t\ttry {\r\n\t\t\tfinal de.hpi.sam.mote.rules.TransformationResult result = (de.hpi.sam.mote.rules.TransformationResult) sdi.executeActivity(a, this, parameters);\r\n\t\t\t//System.out.println( new java.util.Date() + \": Ended executing rule \" + getClass().getSimpleName() + \" with result \" + result.getName() + \".\" );\r\n\t\t\t\r\n\t\t\treturn result;\r\n\t\t}\r\n\t\tcatch (de.mdelab.sdm.interpreter.core.SDMException e) {\r\n\t\t\te.printStackTrace();\r\n\t\t\tthrow new de.hpi.sam.mote.impl.TransformationException(\"Error during execution of rule \'RuleComponent2ComponentImplementation_forwardTransformation\'.\", e);\r\n\t\t}\r\n\t\t//finally {\r\n\t\t\t//sdi.getNotificationEmitter().removeNotificationReceiver( receiver );\r\n\t\t//}\r\n\t\t'"
	 * @generated
	 */
	TransformationResult forwardTransformation(TGGNode parentCorrNode)
			throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="de.hpi.sam.mote.helpers.TransformationException"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\t//System.out.println( new java.util.Date() + \": Starting to execute rule \" + getClass().getSimpleName() + \" on parent element \" + parentCorrNode );\r\n\torg.eclipse.emf.ecore.resource.Resource r = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getResourceSet().getResource(\r\n\t\t\t\torg.eclipse.emf.common.util.URI.createPlatformPluginURI(\"/fr.labsticc.adosate.adele2aadl/model/story/RuleComponent2ComponentImplementation_mappingTransformation.story\", true), true);\r\n\t\t\r\n\t\tde.hpi.sam.storyDiagramEcore.ActivityDiagram ad = (de.hpi.sam.storyDiagramEcore.ActivityDiagram) r.getContents().get(0);\r\n\t\t\r\n\t\tde.hpi.sam.storyDiagramEcore.Activity a = ad.getActivities().get(0); \r\n\t\t\r\n\t\tde.mdelab.sdm.interpreter.sde.SDESDMInterpreter sdi = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getSdmInterpreter();\r\n\t\t//final de.mdelab.sdm.interpreter.sde.notifications.SDEOutputStreamNotificationReceiver receiver = new de.mdelab.sdm.interpreter.sde.notifications.SDEOutputStreamNotificationReceiver( null, (de.mdelab.sdm.interpreter.sde.facade.SDEMetamodelFacadeFactory) sdi.getFacadeFactory() );\r\n\t\t//sdi.getNotificationEmitter().addNotificationReceiver( receiver );\r\n\t\t\r\n\t\tjava.util.List<Object> parameters = new java.util.ArrayList<Object>();\r\n\t\t\t\t\r\n\t\t\tparameters.add(parentCorrNode);\r\n\t\t\r\n\t\ttry {\r\n\t\t\tfinal de.hpi.sam.mote.rules.TransformationResult result = (de.hpi.sam.mote.rules.TransformationResult) sdi.executeActivity(a, this, parameters);\r\n\t\t\t//System.out.println( new java.util.Date() + \": Ended executing rule \" + getClass().getSimpleName() + \" with result \" + result.getName() + \".\" );\r\n\t\t\t\r\n\t\t\treturn result;\r\n\t\t}\r\n\t\tcatch (de.mdelab.sdm.interpreter.core.SDMException e) {\r\n\t\t\te.printStackTrace();\r\n\t\t\tthrow new de.hpi.sam.mote.impl.TransformationException(\"Error during execution of rule \'RuleComponent2ComponentImplementation_mappingTransformation\'.\", e);\r\n\t\t}\r\n\t\t//finally {\r\n\t\t\t//sdi.getNotificationEmitter().removeNotificationReceiver( receiver );\r\n\t\t//}\r\n\t\t'"
	 * @generated
	 */
	TransformationResult mappingTransformation(TGGNode parentCorrNode)
			throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="de.hpi.sam.mote.helpers.TransformationException"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\t//System.out.println( new java.util.Date() + \": Starting to execute rule \" + getClass().getSimpleName() + \" on parent element \" + parentCorrNode );\r\n\torg.eclipse.emf.ecore.resource.Resource r = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getResourceSet().getResource(\r\n\t\t\t\torg.eclipse.emf.common.util.URI.createPlatformPluginURI(\"/fr.labsticc.adosate.adele2aadl/model/story/RuleComponent2ComponentImplementation_reverseTransformation.story\", true), true);\r\n\t\t\r\n\t\tde.hpi.sam.storyDiagramEcore.ActivityDiagram ad = (de.hpi.sam.storyDiagramEcore.ActivityDiagram) r.getContents().get(0);\r\n\t\t\r\n\t\tde.hpi.sam.storyDiagramEcore.Activity a = ad.getActivities().get(0); \r\n\t\t\r\n\t\tde.mdelab.sdm.interpreter.sde.SDESDMInterpreter sdi = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getSdmInterpreter();\r\n\t\t//final de.mdelab.sdm.interpreter.sde.notifications.SDEOutputStreamNotificationReceiver receiver = new de.mdelab.sdm.interpreter.sde.notifications.SDEOutputStreamNotificationReceiver( null, (de.mdelab.sdm.interpreter.sde.facade.SDEMetamodelFacadeFactory) sdi.getFacadeFactory() );\r\n\t\t//sdi.getNotificationEmitter().addNotificationReceiver( receiver );\r\n\t\t\r\n\t\tjava.util.List<Object> parameters = new java.util.ArrayList<Object>();\r\n\t\t\t\t\r\n\t\t\tparameters.add(parentCorrNode);\r\n\t\t\r\n\t\ttry {\r\n\t\t\tfinal de.hpi.sam.mote.rules.TransformationResult result = (de.hpi.sam.mote.rules.TransformationResult) sdi.executeActivity(a, this, parameters);\r\n\t\t\t//System.out.println( new java.util.Date() + \": Ended executing rule \" + getClass().getSimpleName() + \" with result \" + result.getName() + \".\" );\r\n\t\t\t\r\n\t\t\treturn result;\r\n\t\t}\r\n\t\tcatch (de.mdelab.sdm.interpreter.core.SDMException e) {\r\n\t\t\te.printStackTrace();\r\n\t\t\tthrow new de.hpi.sam.mote.impl.TransformationException(\"Error during execution of rule \'RuleComponent2ComponentImplementation_reverseTransformation\'.\", e);\r\n\t\t}\r\n\t\t//finally {\r\n\t\t\t//sdi.getNotificationEmitter().removeNotificationReceiver( receiver );\r\n\t\t//}\r\n\t\t'"
	 * @generated
	 */
	TransformationResult reverseTransformation(TGGNode parentCorrNode)
			throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="de.hpi.sam.mote.helpers.TransformationException"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\r\norg.eclipse.emf.ecore.resource.Resource r = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getResourceSet().getResource(\r\n\t\t\t\torg.eclipse.emf.common.util.URI.createPlatformPluginURI(\"/fr.labsticc.adosate.adele2aadl/model/story/RuleComponent2ComponentImplementation_forwardSynchronization.story\", true), true);\r\n\t\t\r\n\t\tde.hpi.sam.storyDiagramEcore.ActivityDiagram ad = (de.hpi.sam.storyDiagramEcore.ActivityDiagram) r.getContents().get(0);\r\n\t\t\r\n\t\tde.hpi.sam.storyDiagramEcore.Activity a = ad.getActivities().get(0); \r\n\t\t\r\n\t\tde.mdelab.sdm.interpreter.sde.SDESDMInterpreter sdi = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getSdmInterpreter();\r\n\t\t\r\n\t\tjava.util.List<Object> parameters = new java.util.ArrayList<Object>();\r\n\t\t\t\t\r\n\t\t\tparameters.add(corrNode);\r\n\t\t\r\n\t\ttry {\r\n\t\t\treturn (Boolean) sdi.executeActivity(a, this, parameters);\r\n\t\t} catch (de.mdelab.sdm.interpreter.core.SDMException e) {\r\n\t\t\te.printStackTrace();\r\n\t\t\tthrow new de.hpi.sam.mote.impl.TransformationException(\"Error during execution of rule \'RuleComponent2ComponentImplementation_forwardSynchronization\'.\", e);\r\n\t\t}'"
	 * @generated
	 */
	Boolean forwardSynchronization(TGGNode corrNode)
			throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="de.hpi.sam.mote.helpers.TransformationException"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\r\norg.eclipse.emf.ecore.resource.Resource r = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getResourceSet().getResource(\r\n\t\t\t\torg.eclipse.emf.common.util.URI.createPlatformPluginURI(\"/fr.labsticc.adosate.adele2aadl/model/story/RuleComponent2ComponentImplementation_mappingSynchronization.story\", true), true);\r\n\t\t\r\n\t\tde.hpi.sam.storyDiagramEcore.ActivityDiagram ad = (de.hpi.sam.storyDiagramEcore.ActivityDiagram) r.getContents().get(0);\r\n\t\t\r\n\t\tde.hpi.sam.storyDiagramEcore.Activity a = ad.getActivities().get(0); \r\n\t\t\r\n\t\tde.mdelab.sdm.interpreter.sde.SDESDMInterpreter sdi = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getSdmInterpreter();\r\n\t\t\r\n\t\tjava.util.List<Object> parameters = new java.util.ArrayList<Object>();\r\n\t\t\t\t\r\n\t\t\tparameters.add(corrNode);\r\n\t\t\r\n\t\ttry {\r\n\t\t\treturn (Boolean) sdi.executeActivity(a, this, parameters);\r\n\t\t} catch (de.mdelab.sdm.interpreter.core.SDMException e) {\r\n\t\t\te.printStackTrace();\r\n\t\t\tthrow new de.hpi.sam.mote.impl.TransformationException(\"Error during execution of rule \'RuleComponent2ComponentImplementation_mappingSynchronization\'.\", e);\r\n\t\t}'"
	 * @generated
	 */
	Boolean mappingSynchronization(TGGNode corrNode)
			throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model exceptions="de.hpi.sam.mote.helpers.TransformationException"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\r\norg.eclipse.emf.ecore.resource.Resource r = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getResourceSet().getResource(\r\n\t\t\t\torg.eclipse.emf.common.util.URI.createPlatformPluginURI(\"/fr.labsticc.adosate.adele2aadl/model/story/RuleComponent2ComponentImplementation_reverseSynchronization.story\", true), true);\r\n\t\t\r\n\t\tde.hpi.sam.storyDiagramEcore.ActivityDiagram ad = (de.hpi.sam.storyDiagramEcore.ActivityDiagram) r.getContents().get(0);\r\n\t\t\r\n\t\tde.hpi.sam.storyDiagramEcore.Activity a = ad.getActivities().get(0); \r\n\t\t\r\n\t\tde.mdelab.sdm.interpreter.sde.SDESDMInterpreter sdi = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getSdmInterpreter();\r\n\t\t\r\n\t\tjava.util.List<Object> parameters = new java.util.ArrayList<Object>();\r\n\t\t\t\t\r\n\t\t\tparameters.add(corrNode);\r\n\t\t\r\n\t\ttry {\r\n\t\t\treturn (Boolean) sdi.executeActivity(a, this, parameters);\r\n\t\t} catch (de.mdelab.sdm.interpreter.core.SDMException e) {\r\n\t\t\te.printStackTrace();\r\n\t\t\tthrow new de.hpi.sam.mote.impl.TransformationException(\"Error during execution of rule \'RuleComponent2ComponentImplementation_reverseSynchronization\'.\", e);\r\n\t\t}'"
	 * @generated
	 */
	Boolean reverseSynchronization(TGGNode corrNode)
			throws TransformationException;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='\r\ninputCorrNodeTypes.add( fr.labsticc.adosate.adele2aadl.CorrComponentType.class );\r\ninputCorrNodeTypes.add( fr.labsticc.adosate.adele2aadl.CorrAxiom.class );\r\n'"
	 * @generated
	 */
	void initInputCorrNodeTypes();

} // RuleComponent2ComponentImplementation
