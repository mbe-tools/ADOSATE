/**
 */
package fr.labsticc.adosate.adele2aadl;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlPackage
 * @generated
 */
public interface Adele2aadlFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Adele2aadlFactory eINSTANCE = fr.labsticc.adosate.adele2aadl.impl.Adele2aadlFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Corr Axiom</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Corr Axiom</em>'.
	 * @generated
	 */
	CorrAxiom createCorrAxiom();

	/**
	 * Returns a new object of class '<em>Corr Component Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Corr Component Type</em>'.
	 * @generated
	 */
	CorrComponentType createCorrComponentType();

	/**
	 * Returns a new object of class '<em>Corr Component Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Corr Component Implementation</em>'.
	 * @generated
	 */
	CorrComponentImplementation createCorrComponentImplementation();

	/**
	 * Returns a new object of class '<em>Corr Subcomponent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Corr Subcomponent</em>'.
	 * @generated
	 */
	CorrSubcomponent createCorrSubcomponent();

	/**
	 * Returns a new object of class '<em>Corr Directed Feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Corr Directed Feature</em>'.
	 * @generated
	 */
	CorrDirectedFeature createCorrDirectedFeature();

	/**
	 * Returns a new object of class '<em>Corr Access</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Corr Access</em>'.
	 * @generated
	 */
	CorrAccess createCorrAccess();

	/**
	 * Returns a new object of class '<em>Corr Directed Feature Connection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Corr Directed Feature Connection</em>'.
	 * @generated
	 */
	CorrDirectedFeatureConnection createCorrDirectedFeatureConnection();

	/**
	 * Returns a new object of class '<em>Corr Access Connection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Corr Access Connection</em>'.
	 * @generated
	 */
	CorrAccessConnection createCorrAccessConnection();

	/**
	 * Returns a new object of class '<em>Rule Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Set</em>'.
	 * @generated
	 */
	Adele2aadlRuleSet createAdele2aadlRuleSet();

	/**
	 * Returns a new object of class '<em>Rule Package2 Aadl Package</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Package2 Aadl Package</em>'.
	 * @generated
	 */
	RulePackage2AadlPackage createRulePackage2AadlPackage();

	/**
	 * Returns a new object of class '<em>Rule Component2 Component Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Component2 Component Type</em>'.
	 * @generated
	 */
	RuleComponent2ComponentType createRuleComponent2ComponentType();

	/**
	 * Returns a new object of class '<em>Rule Component2 Component Type With Extends</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Component2 Component Type With Extends</em>'.
	 * @generated
	 */
	RuleComponent2ComponentTypeWithExtends createRuleComponent2ComponentTypeWithExtends();

	/**
	 * Returns a new object of class '<em>Rule Feature Group2 Feature Group Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Feature Group2 Feature Group Type</em>'.
	 * @generated
	 */
	RuleFeatureGroup2FeatureGroupType createRuleFeatureGroup2FeatureGroupType();

	/**
	 * Returns a new object of class '<em>Rule Feature Group2 Feature Group Type With Inverse</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Feature Group2 Feature Group Type With Inverse</em>'.
	 * @generated
	 */
	RuleFeatureGroup2FeatureGroupTypeWithInverse createRuleFeatureGroup2FeatureGroupTypeWithInverse();

	/**
	 * Returns a new object of class '<em>Rule Feature Group2 Feature Group Type With Extends</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Feature Group2 Feature Group Type With Extends</em>'.
	 * @generated
	 */
	RuleFeatureGroup2FeatureGroupTypeWithExtends createRuleFeatureGroup2FeatureGroupTypeWithExtends();

	/**
	 * Returns a new object of class '<em>Rule Feature Group2 Feature Group Type With Extends Inverse</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Feature Group2 Feature Group Type With Extends Inverse</em>'.
	 * @generated
	 */
	RuleFeatureGroup2FeatureGroupTypeWithExtendsInverse createRuleFeatureGroup2FeatureGroupTypeWithExtendsInverse();

	/**
	 * Returns a new object of class '<em>Rule Component2 Component Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Component2 Component Implementation</em>'.
	 * @generated
	 */
	RuleComponent2ComponentImplementation createRuleComponent2ComponentImplementation();

	/**
	 * Returns a new object of class '<em>Rule Component2 Component Implementation With Extends</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Component2 Component Implementation With Extends</em>'.
	 * @generated
	 */
	RuleComponent2ComponentImplementationWithExtends createRuleComponent2ComponentImplementationWithExtends();

	/**
	 * Returns a new object of class '<em>Rule Component2 Subcomponent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Component2 Subcomponent</em>'.
	 * @generated
	 */
	RuleComponent2Subcomponent createRuleComponent2Subcomponent();

	/**
	 * Returns a new object of class '<em>Rule Component2 Subcomponent Typed</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Component2 Subcomponent Typed</em>'.
	 * @generated
	 */
	RuleComponent2SubcomponentTyped createRuleComponent2SubcomponentTyped();

	/**
	 * Returns a new object of class '<em>Rule Component2 Subcomponent Typed As Parent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Component2 Subcomponent Typed As Parent</em>'.
	 * @generated
	 */
	RuleComponent2SubcomponentTypedAsParent createRuleComponent2SubcomponentTypedAsParent();

	/**
	 * Returns a new object of class '<em>Rule Component2 Subcomponent Refined</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Component2 Subcomponent Refined</em>'.
	 * @generated
	 */
	RuleComponent2SubcomponentRefined createRuleComponent2SubcomponentRefined();

	/**
	 * Returns a new object of class '<em>Rule Component2 Subcomponent Refined Typed</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Component2 Subcomponent Refined Typed</em>'.
	 * @generated
	 */
	RuleComponent2SubcomponentRefinedTyped createRuleComponent2SubcomponentRefinedTyped();

	/**
	 * Returns a new object of class '<em>Rule Component2 Subcomponent Refined Typed As Parent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Component2 Subcomponent Refined Typed As Parent</em>'.
	 * @generated
	 */
	RuleComponent2SubcomponentRefinedTypedAsParent createRuleComponent2SubcomponentRefinedTypedAsParent();

	/**
	 * Returns a new object of class '<em>Rule Component2 Subcomponent Typed Refined</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Component2 Subcomponent Typed Refined</em>'.
	 * @generated
	 */
	RuleComponent2SubcomponentTypedRefined createRuleComponent2SubcomponentTypedRefined();

	/**
	 * Returns a new object of class '<em>Rule Component2 Subcomponent Typed As Parent Refined</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Component2 Subcomponent Typed As Parent Refined</em>'.
	 * @generated
	 */
	RuleComponent2SubcomponentTypedAsParentRefined createRuleComponent2SubcomponentTypedAsParentRefined();

	/**
	 * Returns a new object of class '<em>Rule Component2 Subcomponent Typed Refined Typed</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Component2 Subcomponent Typed Refined Typed</em>'.
	 * @generated
	 */
	RuleComponent2SubcomponentTypedRefinedTyped createRuleComponent2SubcomponentTypedRefinedTyped();

	/**
	 * Returns a new object of class '<em>Rule Component2 Subcomponent Typed As Parent Refined Typed</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Component2 Subcomponent Typed As Parent Refined Typed</em>'.
	 * @generated
	 */
	RuleComponent2SubcomponentTypedAsParentRefinedTyped createRuleComponent2SubcomponentTypedAsParentRefinedTyped();

	/**
	 * Returns a new object of class '<em>Rule Component2 Subcomponent Typed Refined Typed As Parent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Component2 Subcomponent Typed Refined Typed As Parent</em>'.
	 * @generated
	 */
	RuleComponent2SubcomponentTypedRefinedTypedAsParent createRuleComponent2SubcomponentTypedRefinedTypedAsParent();

	/**
	 * Returns a new object of class '<em>Rule Directed Feat2 Directed Feat</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Directed Feat2 Directed Feat</em>'.
	 * @generated
	 */
	RuleDirectedFeat2DirectedFeat createRuleDirectedFeat2DirectedFeat();

	/**
	 * Returns a new object of class '<em>Rule Directed Feat2 Directed Feat Typed</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Directed Feat2 Directed Feat Typed</em>'.
	 * @generated
	 */
	RuleDirectedFeat2DirectedFeatTyped createRuleDirectedFeat2DirectedFeatTyped();

	/**
	 * Returns a new object of class '<em>Rule Directed Feat2 Directed Feat Refined</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Directed Feat2 Directed Feat Refined</em>'.
	 * @generated
	 */
	RuleDirectedFeat2DirectedFeatRefined createRuleDirectedFeat2DirectedFeatRefined();

	/**
	 * Returns a new object of class '<em>Rule Directed Feat2 Directed Feat Typed Refined</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Directed Feat2 Directed Feat Typed Refined</em>'.
	 * @generated
	 */
	RuleDirectedFeat2DirectedFeatTypedRefined createRuleDirectedFeat2DirectedFeatTypedRefined();

	/**
	 * Returns a new object of class '<em>Rule Access2 Access</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Access2 Access</em>'.
	 * @generated
	 */
	RuleAccess2Access createRuleAccess2Access();

	/**
	 * Returns a new object of class '<em>Rule Access2 Access Typed</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Access2 Access Typed</em>'.
	 * @generated
	 */
	RuleAccess2AccessTyped createRuleAccess2AccessTyped();

	/**
	 * Returns a new object of class '<em>Rule Access2 Access Typed As Parent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Access2 Access Typed As Parent</em>'.
	 * @generated
	 */
	RuleAccess2AccessTypedAsParent createRuleAccess2AccessTypedAsParent();

	/**
	 * Returns a new object of class '<em>Rule Access2 Access Refined</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Access2 Access Refined</em>'.
	 * @generated
	 */
	RuleAccess2AccessRefined createRuleAccess2AccessRefined();

	/**
	 * Returns a new object of class '<em>Rule Access2 Access Typed Refined</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Access2 Access Typed Refined</em>'.
	 * @generated
	 */
	RuleAccess2AccessTypedRefined createRuleAccess2AccessTypedRefined();

	/**
	 * Returns a new object of class '<em>Rule Access2 Access Typed As Parent Refined</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Access2 Access Typed As Parent Refined</em>'.
	 * @generated
	 */
	RuleAccess2AccessTypedAsParentRefined createRuleAccess2AccessTypedAsParentRefined();

	/**
	 * Returns a new object of class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat</em>'.
	 * @generated
	 */
	RuleFeatGroupDirFeat2FeatGroupDirFeat createRuleFeatGroupDirFeat2FeatGroupDirFeat();

	/**
	 * Returns a new object of class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed</em>'.
	 * @generated
	 */
	RuleFeatGroupDirFeat2FeatGroupDirFeatTyped createRuleFeatGroupDirFeat2FeatGroupDirFeatTyped();

	/**
	 * Returns a new object of class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent</em>'.
	 * @generated
	 */
	RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent();

	/**
	 * Returns a new object of class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Refined</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Refined</em>'.
	 * @generated
	 */
	RuleFeatGroupDirFeat2FeatGroupDirFeatRefined createRuleFeatGroupDirFeat2FeatGroupDirFeatRefined();

	/**
	 * Returns a new object of class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed Refined</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed Refined</em>'.
	 * @generated
	 */
	RuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined();

	/**
	 * Returns a new object of class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent Refined</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent Refined</em>'.
	 * @generated
	 */
	RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined();

	/**
	 * Returns a new object of class '<em>Rule Feat Group Access2 Feat Group Access</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Feat Group Access2 Feat Group Access</em>'.
	 * @generated
	 */
	RuleFeatGroupAccess2FeatGroupAccess createRuleFeatGroupAccess2FeatGroupAccess();

	/**
	 * Returns a new object of class '<em>Rule Feat Group Access2 Feat Group Access Typed</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Feat Group Access2 Feat Group Access Typed</em>'.
	 * @generated
	 */
	RuleFeatGroupAccess2FeatGroupAccessTyped createRuleFeatGroupAccess2FeatGroupAccessTyped();

	/**
	 * Returns a new object of class '<em>Rule Feat Group Access2 Feat Group Access Refined</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Feat Group Access2 Feat Group Access Refined</em>'.
	 * @generated
	 */
	RuleFeatGroupAccess2FeatGroupAccessRefined createRuleFeatGroupAccess2FeatGroupAccessRefined();

	/**
	 * Returns a new object of class '<em>Rule Feat Group Access2 Feat Group Access Typed Refined</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Feat Group Access2 Feat Group Access Typed Refined</em>'.
	 * @generated
	 */
	RuleFeatGroupAccess2FeatGroupAccessTypedRefined createRuleFeatGroupAccess2FeatGroupAccessTypedRefined();

	/**
	 * Returns a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo</em>'.
	 * @generated
	 */
	RuleDirFeatConn2DirFeatConnSrcParentDestSubcompo createRuleDirFeatConn2DirFeatConnSrcParentDestSubcompo();

	/**
	 * Returns a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo Same Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Dest Subcompo Same Port</em>'.
	 * @generated
	 */
	RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort createRuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort();

	/**
	 * Returns a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent</em>'.
	 * @generated
	 */
	RuleDirFeatConn2DirFeatConnSrcSubcompoDestParent createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParent();

	/**
	 * Returns a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group</em>'.
	 * @generated
	 */
	RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup();

	/**
	 * Returns a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group Inverse</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Feat Group Inverse</em>'.
	 * @generated
	 */
	RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse();

	/**
	 * Returns a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Dest Subcompo</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Dest Subcompo</em>'.
	 * @generated
	 */
	RuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo createRuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo();

	/**
	 * Returns a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Inverse Dest Subcompo</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Parent Feat Group Inverse Dest Subcompo</em>'.
	 * @generated
	 */
	RuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo createRuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo();

	/**
	 * Returns a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Same Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Parent Same Port</em>'.
	 * @generated
	 */
	RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort();

	/**
	 * Returns a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo</em>'.
	 * @generated
	 */
	RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo createRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo();

	/**
	 * Returns a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo Same Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Dir Feat Conn2 Dir Feat Conn Src Subcompo Dest Subcompo Same Port</em>'.
	 * @generated
	 */
	RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort createRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort();

	/**
	 * Returns a new object of class '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat</em>'.
	 * @generated
	 */
	RuleAccessConn2AccessConnSrcParentDestSubcompoFeat createRuleAccessConn2AccessConnSrcParentDestSubcompoFeat();

	/**
	 * Returns a new object of class '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat Same Access</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo Feat Same Access</em>'.
	 * @generated
	 */
	RuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess createRuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess();

	/**
	 * Returns a new object of class '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent</em>'.
	 * @generated
	 */
	RuleAccessConn2AccessConnSrcSubcompoFeatDestParent createRuleAccessConn2AccessConnSrcSubcompoFeatDestParent();

	/**
	 * Returns a new object of class '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent Same Access</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Parent Same Access</em>'.
	 * @generated
	 */
	RuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess createRuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess();

	/**
	 * Returns a new object of class '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo Feat</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo Feat</em>'.
	 * @generated
	 */
	RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat createRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat();

	/**
	 * Returns a new object of class '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Parent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Parent</em>'.
	 * @generated
	 */
	RuleAccessConn2AccessConnSrcSubcompoDestParent createRuleAccessConn2AccessConnSrcSubcompoDestParent();

	/**
	 * Returns a new object of class '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo Feat</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo Feat</em>'.
	 * @generated
	 */
	RuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat createRuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat();

	/**
	 * Returns a new object of class '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Access Conn2 Access Conn Src Subcompo Dest Subcompo</em>'.
	 * @generated
	 */
	RuleAccessConn2AccessConnSrcSubcompoDestSubcompo createRuleAccessConn2AccessConnSrcSubcompoDestSubcompo();

	/**
	 * Returns a new object of class '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Access Conn2 Access Conn Src Parent Dest Subcompo</em>'.
	 * @generated
	 */
	RuleAccessConn2AccessConnSrcParentDestSubcompo createRuleAccessConn2AccessConnSrcParentDestSubcompo();

	/**
	 * Returns a new object of class '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Access Conn2 Access Conn Src Subcompo Feat Dest Subcompo</em>'.
	 * @generated
	 */
	RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo createRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Adele2aadlPackage getAdele2aadlPackage();

} //Adele2aadlFactory
