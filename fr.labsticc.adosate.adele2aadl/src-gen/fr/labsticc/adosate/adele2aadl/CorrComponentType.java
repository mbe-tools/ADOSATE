/**
 */
package fr.labsticc.adosate.adele2aadl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Component Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlPackage#getCorrComponentType()
 * @model
 * @generated
 */
public interface CorrComponentType extends CorrComponent {
} // CorrComponentType
