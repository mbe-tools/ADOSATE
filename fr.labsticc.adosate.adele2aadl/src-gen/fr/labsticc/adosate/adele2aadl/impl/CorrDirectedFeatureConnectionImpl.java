/**
 */
package fr.labsticc.adosate.adele2aadl.impl;

import de.hpi.sam.mote.impl.TGGNodeImpl;
import fr.labsticc.adosate.adele2aadl.Adele2aadlPackage;
import fr.labsticc.adosate.adele2aadl.CorrDirectedFeatureConnection;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Directed Feature Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CorrDirectedFeatureConnectionImpl extends TGGNodeImpl
		implements CorrDirectedFeatureConnection {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrDirectedFeatureConnectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Adele2aadlPackage.eINSTANCE.getCorrDirectedFeatureConnection();
	}

} //CorrDirectedFeatureConnectionImpl
