/**
 */
package fr.labsticc.adosate.adele2aadl.impl;

import de.hpi.sam.mote.MotePackage;
import fr.labsticc.adosate.adele2aadl.Adele2aadlFactory;
import fr.labsticc.adosate.adele2aadl.Adele2aadlPackage;

import java.io.IOException;
import java.net.URL;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Adele2aadlPackageImpl extends EPackageImpl implements
		Adele2aadlPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected String packageFilename = "adele2aadl.ecore";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrAxiomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrComponentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrComponentImplementationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrSubcomponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrDirectedFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrAccessEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrDirectedFeatureConnectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrAccessConnectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass adele2aadlRuleSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rulePackage2AadlPackageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleComponent2ComponentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleComponent2ComponentTypeWithExtendsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleFeatureGroup2FeatureGroupTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleFeatureGroup2FeatureGroupTypeWithInverseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleFeatureGroup2FeatureGroupTypeWithExtendsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleFeatureGroup2FeatureGroupTypeWithExtendsInverseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleComponent2ComponentImplementationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleComponent2ComponentImplementationWithExtendsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleComponent2SubcomponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleComponent2SubcomponentTypedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleComponent2SubcomponentTypedAsParentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleComponent2SubcomponentRefinedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleComponent2SubcomponentRefinedTypedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleComponent2SubcomponentRefinedTypedAsParentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleComponent2SubcomponentTypedRefinedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleComponent2SubcomponentTypedAsParentRefinedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleComponent2SubcomponentTypedRefinedTypedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleComponent2SubcomponentTypedAsParentRefinedTypedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleComponent2SubcomponentTypedRefinedTypedAsParentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleDirectedFeat2DirectedFeatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleDirectedFeat2DirectedFeatTypedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleDirectedFeat2DirectedFeatRefinedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleDirectedFeat2DirectedFeatTypedRefinedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleAccess2AccessEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleAccess2AccessTypedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleAccess2AccessTypedAsParentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleAccess2AccessRefinedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleAccess2AccessTypedRefinedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleAccess2AccessTypedAsParentRefinedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleFeatGroupDirFeat2FeatGroupDirFeatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleFeatGroupDirFeat2FeatGroupDirFeatTypedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleFeatGroupDirFeat2FeatGroupDirFeatRefinedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleFeatGroupDirFeat2FeatGroupDirFeatTypedRefinedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefinedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleFeatGroupAccess2FeatGroupAccessEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleFeatGroupAccess2FeatGroupAccessTypedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleFeatGroupAccess2FeatGroupAccessRefinedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleFeatGroupAccess2FeatGroupAccessTypedRefinedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleDirFeatConn2DirFeatConnSrcParentDestSubcompoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleAccessConn2AccessConnSrcParentDestSubcompoFeatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccessEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleAccessConn2AccessConnSrcSubcompoFeatDestParentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccessEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleAccessConn2AccessConnSrcSubcompoDestParentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleAccessConn2AccessConnSrcSubcompoDestSubcompoFeatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleAccessConn2AccessConnSrcSubcompoDestSubcompoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleAccessConn2AccessConnSrcParentDestSubcompoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType sdmInterpreterEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Adele2aadlPackageImpl() {
		super(eNS_URI, Adele2aadlFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Adele2aadlPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @generated
	 */
	public static Adele2aadlPackage init() {
		if (isInited) return (Adele2aadlPackage)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI);

		// Obtain or create and register package
		Adele2aadlPackageImpl theAdele2aadlPackage = (Adele2aadlPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Adele2aadlPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Adele2aadlPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		MotePackage.eINSTANCE.eClass();

		// Load packages
		theAdele2aadlPackage.loadPackage();

		// Fix loaded packages
		theAdele2aadlPackage.fixPackageContents();

		// Mark meta-data to indicate it can't be changed
		theAdele2aadlPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Adele2aadlPackage.eNS_URI, theAdele2aadlPackage);
		return theAdele2aadlPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrAxiom() {
		if (corrAxiomEClass == null) {
			corrAxiomEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(0);
		}
		return corrAxiomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrComponent() {
		if (corrComponentEClass == null) {
			corrComponentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(1);
		}
		return corrComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrComponentType() {
		if (corrComponentTypeEClass == null) {
			corrComponentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(2);
		}
		return corrComponentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrComponentImplementation() {
		if (corrComponentImplementationEClass == null) {
			corrComponentImplementationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(3);
		}
		return corrComponentImplementationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrSubcomponent() {
		if (corrSubcomponentEClass == null) {
			corrSubcomponentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(4);
		}
		return corrSubcomponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrFeature() {
		if (corrFeatureEClass == null) {
			corrFeatureEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(5);
		}
		return corrFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrDirectedFeature() {
		if (corrDirectedFeatureEClass == null) {
			corrDirectedFeatureEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(6);
		}
		return corrDirectedFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrAccess() {
		if (corrAccessEClass == null) {
			corrAccessEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(7);
		}
		return corrAccessEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrDirectedFeatureConnection() {
		if (corrDirectedFeatureConnectionEClass == null) {
			corrDirectedFeatureConnectionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(8);
		}
		return corrDirectedFeatureConnectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrAccessConnection() {
		if (corrAccessConnectionEClass == null) {
			corrAccessConnectionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(9);
		}
		return corrAccessConnectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAdele2aadlRuleSet() {
		if (adele2aadlRuleSetEClass == null) {
			adele2aadlRuleSetEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(11);
		}
		return adele2aadlRuleSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAdele2aadlRuleSet_SdmInterpreter() {
        return (EAttribute)getAdele2aadlRuleSet().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRulePackage2AadlPackage() {
		if (rulePackage2AadlPackageEClass == null) {
			rulePackage2AadlPackageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(12);
		}
		return rulePackage2AadlPackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleComponent2ComponentType() {
		if (ruleComponent2ComponentTypeEClass == null) {
			ruleComponent2ComponentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(13);
		}
		return ruleComponent2ComponentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleComponent2ComponentTypeWithExtends() {
		if (ruleComponent2ComponentTypeWithExtendsEClass == null) {
			ruleComponent2ComponentTypeWithExtendsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(14);
		}
		return ruleComponent2ComponentTypeWithExtendsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleFeatureGroup2FeatureGroupType() {
		if (ruleFeatureGroup2FeatureGroupTypeEClass == null) {
			ruleFeatureGroup2FeatureGroupTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(15);
		}
		return ruleFeatureGroup2FeatureGroupTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleFeatureGroup2FeatureGroupTypeWithInverse() {
		if (ruleFeatureGroup2FeatureGroupTypeWithInverseEClass == null) {
			ruleFeatureGroup2FeatureGroupTypeWithInverseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(16);
		}
		return ruleFeatureGroup2FeatureGroupTypeWithInverseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleFeatureGroup2FeatureGroupTypeWithExtends() {
		if (ruleFeatureGroup2FeatureGroupTypeWithExtendsEClass == null) {
			ruleFeatureGroup2FeatureGroupTypeWithExtendsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(17);
		}
		return ruleFeatureGroup2FeatureGroupTypeWithExtendsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleFeatureGroup2FeatureGroupTypeWithExtendsInverse() {
		if (ruleFeatureGroup2FeatureGroupTypeWithExtendsInverseEClass == null) {
			ruleFeatureGroup2FeatureGroupTypeWithExtendsInverseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(18);
		}
		return ruleFeatureGroup2FeatureGroupTypeWithExtendsInverseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleComponent2ComponentImplementation() {
		if (ruleComponent2ComponentImplementationEClass == null) {
			ruleComponent2ComponentImplementationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(19);
		}
		return ruleComponent2ComponentImplementationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleComponent2ComponentImplementationWithExtends() {
		if (ruleComponent2ComponentImplementationWithExtendsEClass == null) {
			ruleComponent2ComponentImplementationWithExtendsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(20);
		}
		return ruleComponent2ComponentImplementationWithExtendsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleComponent2Subcomponent() {
		if (ruleComponent2SubcomponentEClass == null) {
			ruleComponent2SubcomponentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(21);
		}
		return ruleComponent2SubcomponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleComponent2SubcomponentTyped() {
		if (ruleComponent2SubcomponentTypedEClass == null) {
			ruleComponent2SubcomponentTypedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(22);
		}
		return ruleComponent2SubcomponentTypedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleComponent2SubcomponentTypedAsParent() {
		if (ruleComponent2SubcomponentTypedAsParentEClass == null) {
			ruleComponent2SubcomponentTypedAsParentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(23);
		}
		return ruleComponent2SubcomponentTypedAsParentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleComponent2SubcomponentRefined() {
		if (ruleComponent2SubcomponentRefinedEClass == null) {
			ruleComponent2SubcomponentRefinedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(24);
		}
		return ruleComponent2SubcomponentRefinedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleComponent2SubcomponentRefinedTyped() {
		if (ruleComponent2SubcomponentRefinedTypedEClass == null) {
			ruleComponent2SubcomponentRefinedTypedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(25);
		}
		return ruleComponent2SubcomponentRefinedTypedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleComponent2SubcomponentRefinedTypedAsParent() {
		if (ruleComponent2SubcomponentRefinedTypedAsParentEClass == null) {
			ruleComponent2SubcomponentRefinedTypedAsParentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(26);
		}
		return ruleComponent2SubcomponentRefinedTypedAsParentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleComponent2SubcomponentTypedRefined() {
		if (ruleComponent2SubcomponentTypedRefinedEClass == null) {
			ruleComponent2SubcomponentTypedRefinedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(27);
		}
		return ruleComponent2SubcomponentTypedRefinedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleComponent2SubcomponentTypedAsParentRefined() {
		if (ruleComponent2SubcomponentTypedAsParentRefinedEClass == null) {
			ruleComponent2SubcomponentTypedAsParentRefinedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(28);
		}
		return ruleComponent2SubcomponentTypedAsParentRefinedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleComponent2SubcomponentTypedRefinedTyped() {
		if (ruleComponent2SubcomponentTypedRefinedTypedEClass == null) {
			ruleComponent2SubcomponentTypedRefinedTypedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(29);
		}
		return ruleComponent2SubcomponentTypedRefinedTypedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleComponent2SubcomponentTypedAsParentRefinedTyped() {
		if (ruleComponent2SubcomponentTypedAsParentRefinedTypedEClass == null) {
			ruleComponent2SubcomponentTypedAsParentRefinedTypedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(30);
		}
		return ruleComponent2SubcomponentTypedAsParentRefinedTypedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleComponent2SubcomponentTypedRefinedTypedAsParent() {
		if (ruleComponent2SubcomponentTypedRefinedTypedAsParentEClass == null) {
			ruleComponent2SubcomponentTypedRefinedTypedAsParentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(31);
		}
		return ruleComponent2SubcomponentTypedRefinedTypedAsParentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleDirectedFeat2DirectedFeat() {
		if (ruleDirectedFeat2DirectedFeatEClass == null) {
			ruleDirectedFeat2DirectedFeatEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(32);
		}
		return ruleDirectedFeat2DirectedFeatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleDirectedFeat2DirectedFeatTyped() {
		if (ruleDirectedFeat2DirectedFeatTypedEClass == null) {
			ruleDirectedFeat2DirectedFeatTypedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(33);
		}
		return ruleDirectedFeat2DirectedFeatTypedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleDirectedFeat2DirectedFeatRefined() {
		if (ruleDirectedFeat2DirectedFeatRefinedEClass == null) {
			ruleDirectedFeat2DirectedFeatRefinedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(34);
		}
		return ruleDirectedFeat2DirectedFeatRefinedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleDirectedFeat2DirectedFeatTypedRefined() {
		if (ruleDirectedFeat2DirectedFeatTypedRefinedEClass == null) {
			ruleDirectedFeat2DirectedFeatTypedRefinedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(35);
		}
		return ruleDirectedFeat2DirectedFeatTypedRefinedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleAccess2Access() {
		if (ruleAccess2AccessEClass == null) {
			ruleAccess2AccessEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(36);
		}
		return ruleAccess2AccessEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleAccess2AccessTyped() {
		if (ruleAccess2AccessTypedEClass == null) {
			ruleAccess2AccessTypedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(37);
		}
		return ruleAccess2AccessTypedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleAccess2AccessTypedAsParent() {
		if (ruleAccess2AccessTypedAsParentEClass == null) {
			ruleAccess2AccessTypedAsParentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(38);
		}
		return ruleAccess2AccessTypedAsParentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleAccess2AccessRefined() {
		if (ruleAccess2AccessRefinedEClass == null) {
			ruleAccess2AccessRefinedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(39);
		}
		return ruleAccess2AccessRefinedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleAccess2AccessTypedRefined() {
		if (ruleAccess2AccessTypedRefinedEClass == null) {
			ruleAccess2AccessTypedRefinedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(40);
		}
		return ruleAccess2AccessTypedRefinedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleAccess2AccessTypedAsParentRefined() {
		if (ruleAccess2AccessTypedAsParentRefinedEClass == null) {
			ruleAccess2AccessTypedAsParentRefinedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(41);
		}
		return ruleAccess2AccessTypedAsParentRefinedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleFeatGroupDirFeat2FeatGroupDirFeat() {
		if (ruleFeatGroupDirFeat2FeatGroupDirFeatEClass == null) {
			ruleFeatGroupDirFeat2FeatGroupDirFeatEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(42);
		}
		return ruleFeatGroupDirFeat2FeatGroupDirFeatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleFeatGroupDirFeat2FeatGroupDirFeatTyped() {
		if (ruleFeatGroupDirFeat2FeatGroupDirFeatTypedEClass == null) {
			ruleFeatGroupDirFeat2FeatGroupDirFeatTypedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(43);
		}
		return ruleFeatGroupDirFeat2FeatGroupDirFeatTypedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent() {
		if (ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentEClass == null) {
			ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(44);
		}
		return ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleFeatGroupDirFeat2FeatGroupDirFeatRefined() {
		if (ruleFeatGroupDirFeat2FeatGroupDirFeatRefinedEClass == null) {
			ruleFeatGroupDirFeat2FeatGroupDirFeatRefinedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(45);
		}
		return ruleFeatGroupDirFeat2FeatGroupDirFeatRefinedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined() {
		if (ruleFeatGroupDirFeat2FeatGroupDirFeatTypedRefinedEClass == null) {
			ruleFeatGroupDirFeat2FeatGroupDirFeatTypedRefinedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(46);
		}
		return ruleFeatGroupDirFeat2FeatGroupDirFeatTypedRefinedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined() {
		if (ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefinedEClass == null) {
			ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefinedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(47);
		}
		return ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefinedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleFeatGroupAccess2FeatGroupAccess() {
		if (ruleFeatGroupAccess2FeatGroupAccessEClass == null) {
			ruleFeatGroupAccess2FeatGroupAccessEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(48);
		}
		return ruleFeatGroupAccess2FeatGroupAccessEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleFeatGroupAccess2FeatGroupAccessTyped() {
		if (ruleFeatGroupAccess2FeatGroupAccessTypedEClass == null) {
			ruleFeatGroupAccess2FeatGroupAccessTypedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(49);
		}
		return ruleFeatGroupAccess2FeatGroupAccessTypedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleFeatGroupAccess2FeatGroupAccessRefined() {
		if (ruleFeatGroupAccess2FeatGroupAccessRefinedEClass == null) {
			ruleFeatGroupAccess2FeatGroupAccessRefinedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(50);
		}
		return ruleFeatGroupAccess2FeatGroupAccessRefinedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleFeatGroupAccess2FeatGroupAccessTypedRefined() {
		if (ruleFeatGroupAccess2FeatGroupAccessTypedRefinedEClass == null) {
			ruleFeatGroupAccess2FeatGroupAccessTypedRefinedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(51);
		}
		return ruleFeatGroupAccess2FeatGroupAccessTypedRefinedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleDirFeatConn2DirFeatConnSrcParentDestSubcompo() {
		if (ruleDirFeatConn2DirFeatConnSrcParentDestSubcompoEClass == null) {
			ruleDirFeatConn2DirFeatConnSrcParentDestSubcompoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(52);
		}
		return ruleDirFeatConn2DirFeatConnSrcParentDestSubcompoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort() {
		if (ruleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePortEClass == null) {
			ruleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePortEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(53);
		}
		return ruleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleDirFeatConn2DirFeatConnSrcSubcompoDestParent() {
		if (ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentEClass == null) {
			ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(54);
		}
		return ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup() {
		if (ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupEClass == null) {
			ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(55);
		}
		return ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse() {
		if (ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverseEClass == null) {
			ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(56);
		}
		return ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo() {
		if (ruleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompoEClass == null) {
			ruleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(57);
		}
		return ruleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo() {
		if (ruleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompoEClass == null) {
			ruleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(58);
		}
		return ruleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort() {
		if (ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePortEClass == null) {
			ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePortEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(59);
		}
		return ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo() {
		if (ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoEClass == null) {
			ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(60);
		}
		return ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort() {
		if (ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePortEClass == null) {
			ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePortEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(61);
		}
		return ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleAccessConn2AccessConnSrcParentDestSubcompoFeat() {
		if (ruleAccessConn2AccessConnSrcParentDestSubcompoFeatEClass == null) {
			ruleAccessConn2AccessConnSrcParentDestSubcompoFeatEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(62);
		}
		return ruleAccessConn2AccessConnSrcParentDestSubcompoFeatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess() {
		if (ruleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccessEClass == null) {
			ruleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccessEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(63);
		}
		return ruleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccessEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleAccessConn2AccessConnSrcSubcompoFeatDestParent() {
		if (ruleAccessConn2AccessConnSrcSubcompoFeatDestParentEClass == null) {
			ruleAccessConn2AccessConnSrcSubcompoFeatDestParentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(64);
		}
		return ruleAccessConn2AccessConnSrcSubcompoFeatDestParentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess() {
		if (ruleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccessEClass == null) {
			ruleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccessEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(65);
		}
		return ruleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccessEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat() {
		if (ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeatEClass == null) {
			ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeatEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(66);
		}
		return ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleAccessConn2AccessConnSrcSubcompoDestParent() {
		if (ruleAccessConn2AccessConnSrcSubcompoDestParentEClass == null) {
			ruleAccessConn2AccessConnSrcSubcompoDestParentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(67);
		}
		return ruleAccessConn2AccessConnSrcSubcompoDestParentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat() {
		if (ruleAccessConn2AccessConnSrcSubcompoDestSubcompoFeatEClass == null) {
			ruleAccessConn2AccessConnSrcSubcompoDestSubcompoFeatEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(68);
		}
		return ruleAccessConn2AccessConnSrcSubcompoDestSubcompoFeatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleAccessConn2AccessConnSrcSubcompoDestSubcompo() {
		if (ruleAccessConn2AccessConnSrcSubcompoDestSubcompoEClass == null) {
			ruleAccessConn2AccessConnSrcSubcompoDestSubcompoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(69);
		}
		return ruleAccessConn2AccessConnSrcSubcompoDestSubcompoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleAccessConn2AccessConnSrcParentDestSubcompo() {
		if (ruleAccessConn2AccessConnSrcParentDestSubcompoEClass == null) {
			ruleAccessConn2AccessConnSrcParentDestSubcompoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(70);
		}
		return ruleAccessConn2AccessConnSrcParentDestSubcompoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo() {
		if (ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoEClass == null) {
			ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(71);
		}
		return ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getSDMInterpreter() {
		if (sdmInterpreterEDataType == null) {
			sdmInterpreterEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(Adele2aadlPackage.eNS_URI).getEClassifiers().get(10);
		}
		return sdmInterpreterEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Adele2aadlFactory getAdele2aadlFactory() {
		return (Adele2aadlFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isLoaded = false;

	/**
	 * Laods the package and any sub-packages from their serialized form.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void loadPackage() {
		if (isLoaded) return;
		isLoaded = true;

		URL url = getClass().getResource(packageFilename);
		if (url == null) {
			throw new RuntimeException("Missing serialized package: " + packageFilename);
		}
		URI uri = URI.createURI(url.toString());
		Resource resource = new EcoreResourceFactoryImpl().createResource(uri);
		try {
			resource.load(null);
		}
		catch (IOException exception) {
			throw new WrappedException(exception);
		}
		initializeFromLoadedEPackage(this, (EPackage)resource.getContents().get(0));
		createResource(eNS_URI);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isFixed = false;

	/**
	 * Fixes up the loaded package, to make it appear as if it had been programmatically built.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fixPackageContents() {
		if (isFixed) return;
		isFixed = true;
		fixEClassifiers();
	}

	/**
	 * Sets the instance class on the given classifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void fixInstanceClass(EClassifier eClassifier) {
		if (eClassifier.getInstanceClassName() == null) {
			eClassifier.setInstanceClassName("fr.labsticc.adosate.adele2aadl." + eClassifier.getName());
			setGeneratedClassName(eClassifier);
		}
	}

} //Adele2aadlPackageImpl
