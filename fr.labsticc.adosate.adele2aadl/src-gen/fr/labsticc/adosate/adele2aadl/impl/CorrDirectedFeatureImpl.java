/**
 */
package fr.labsticc.adosate.adele2aadl.impl;

import org.eclipse.emf.ecore.EClass;

import fr.labsticc.adosate.adele2aadl.Adele2aadlPackage;
import fr.labsticc.adosate.adele2aadl.CorrDirectedFeature;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Directed Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CorrDirectedFeatureImpl extends CorrFeatureImpl implements
		CorrDirectedFeature {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrDirectedFeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Adele2aadlPackage.eINSTANCE.getCorrDirectedFeature();
	}

} //CorrDirectedFeatureImpl
