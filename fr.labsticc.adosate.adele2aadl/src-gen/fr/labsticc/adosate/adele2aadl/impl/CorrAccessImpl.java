/**
 */
package fr.labsticc.adosate.adele2aadl.impl;

import org.eclipse.emf.ecore.EClass;

import fr.labsticc.adosate.adele2aadl.Adele2aadlPackage;
import fr.labsticc.adosate.adele2aadl.CorrAccess;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Access</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CorrAccessImpl extends CorrFeatureImpl implements CorrAccess {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrAccessImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Adele2aadlPackage.eINSTANCE.getCorrAccess();
	}

} //CorrAccessImpl
