/**
 */
package fr.labsticc.adosate.adele2aadl.impl;

import de.mdelab.sdm.interpreter.sde.eclipse.SDEEclipseSDMInterpreter;
import fr.labsticc.adosate.adele2aadl.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Adele2aadlFactoryImpl extends EFactoryImpl implements
		Adele2aadlFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Adele2aadlFactory init() {
		try {
			Adele2aadlFactory theAdele2aadlFactory = (Adele2aadlFactory)EPackage.Registry.INSTANCE.getEFactory(Adele2aadlPackage.eNS_URI);
			if (theAdele2aadlFactory != null) {
				return theAdele2aadlFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Adele2aadlFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Adele2aadlFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Adele2aadlPackage.CORR_AXIOM: return createCorrAxiom();
			case Adele2aadlPackage.CORR_COMPONENT_TYPE: return createCorrComponentType();
			case Adele2aadlPackage.CORR_COMPONENT_IMPLEMENTATION: return createCorrComponentImplementation();
			case Adele2aadlPackage.CORR_SUBCOMPONENT: return createCorrSubcomponent();
			case Adele2aadlPackage.CORR_DIRECTED_FEATURE: return createCorrDirectedFeature();
			case Adele2aadlPackage.CORR_ACCESS: return createCorrAccess();
			case Adele2aadlPackage.CORR_DIRECTED_FEATURE_CONNECTION: return createCorrDirectedFeatureConnection();
			case Adele2aadlPackage.CORR_ACCESS_CONNECTION: return createCorrAccessConnection();
			case Adele2aadlPackage.ADELE2AADL_RULE_SET: return createAdele2aadlRuleSet();
			case Adele2aadlPackage.RULE_PACKAGE2_AADL_PACKAGE: return createRulePackage2AadlPackage();
			case Adele2aadlPackage.RULE_COMPONENT2_COMPONENT_TYPE: return createRuleComponent2ComponentType();
			case Adele2aadlPackage.RULE_COMPONENT2_COMPONENT_TYPE_WITH_EXTENDS: return createRuleComponent2ComponentTypeWithExtends();
			case Adele2aadlPackage.RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE: return createRuleFeatureGroup2FeatureGroupType();
			case Adele2aadlPackage.RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_INVERSE: return createRuleFeatureGroup2FeatureGroupTypeWithInverse();
			case Adele2aadlPackage.RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_EXTENDS: return createRuleFeatureGroup2FeatureGroupTypeWithExtends();
			case Adele2aadlPackage.RULE_FEATURE_GROUP2_FEATURE_GROUP_TYPE_WITH_EXTENDS_INVERSE: return createRuleFeatureGroup2FeatureGroupTypeWithExtendsInverse();
			case Adele2aadlPackage.RULE_COMPONENT2_COMPONENT_IMPLEMENTATION: return createRuleComponent2ComponentImplementation();
			case Adele2aadlPackage.RULE_COMPONENT2_COMPONENT_IMPLEMENTATION_WITH_EXTENDS: return createRuleComponent2ComponentImplementationWithExtends();
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT: return createRuleComponent2Subcomponent();
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_TYPED: return createRuleComponent2SubcomponentTyped();
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT: return createRuleComponent2SubcomponentTypedAsParent();
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_REFINED: return createRuleComponent2SubcomponentRefined();
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_REFINED_TYPED: return createRuleComponent2SubcomponentRefinedTyped();
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_REFINED_TYPED_AS_PARENT: return createRuleComponent2SubcomponentRefinedTypedAsParent();
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED: return createRuleComponent2SubcomponentTypedRefined();
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT_REFINED: return createRuleComponent2SubcomponentTypedAsParentRefined();
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED_TYPED: return createRuleComponent2SubcomponentTypedRefinedTyped();
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_TYPED_AS_PARENT_REFINED_TYPED: return createRuleComponent2SubcomponentTypedAsParentRefinedTyped();
			case Adele2aadlPackage.RULE_COMPONENT2_SUBCOMPONENT_TYPED_REFINED_TYPED_AS_PARENT: return createRuleComponent2SubcomponentTypedRefinedTypedAsParent();
			case Adele2aadlPackage.RULE_DIRECTED_FEAT2_DIRECTED_FEAT: return createRuleDirectedFeat2DirectedFeat();
			case Adele2aadlPackage.RULE_DIRECTED_FEAT2_DIRECTED_FEAT_TYPED: return createRuleDirectedFeat2DirectedFeatTyped();
			case Adele2aadlPackage.RULE_DIRECTED_FEAT2_DIRECTED_FEAT_REFINED: return createRuleDirectedFeat2DirectedFeatRefined();
			case Adele2aadlPackage.RULE_DIRECTED_FEAT2_DIRECTED_FEAT_TYPED_REFINED: return createRuleDirectedFeat2DirectedFeatTypedRefined();
			case Adele2aadlPackage.RULE_ACCESS2_ACCESS: return createRuleAccess2Access();
			case Adele2aadlPackage.RULE_ACCESS2_ACCESS_TYPED: return createRuleAccess2AccessTyped();
			case Adele2aadlPackage.RULE_ACCESS2_ACCESS_TYPED_AS_PARENT: return createRuleAccess2AccessTypedAsParent();
			case Adele2aadlPackage.RULE_ACCESS2_ACCESS_REFINED: return createRuleAccess2AccessRefined();
			case Adele2aadlPackage.RULE_ACCESS2_ACCESS_TYPED_REFINED: return createRuleAccess2AccessTypedRefined();
			case Adele2aadlPackage.RULE_ACCESS2_ACCESS_TYPED_AS_PARENT_REFINED: return createRuleAccess2AccessTypedAsParentRefined();
			case Adele2aadlPackage.RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT: return createRuleFeatGroupDirFeat2FeatGroupDirFeat();
			case Adele2aadlPackage.RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED: return createRuleFeatGroupDirFeat2FeatGroupDirFeatTyped();
			case Adele2aadlPackage.RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_AS_PARENT: return createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent();
			case Adele2aadlPackage.RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_REFINED: return createRuleFeatGroupDirFeat2FeatGroupDirFeatRefined();
			case Adele2aadlPackage.RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_REFINED: return createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined();
			case Adele2aadlPackage.RULE_FEAT_GROUP_DIR_FEAT2_FEAT_GROUP_DIR_FEAT_TYPED_AS_PARENT_REFINED: return createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined();
			case Adele2aadlPackage.RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS: return createRuleFeatGroupAccess2FeatGroupAccess();
			case Adele2aadlPackage.RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_TYPED: return createRuleFeatGroupAccess2FeatGroupAccessTyped();
			case Adele2aadlPackage.RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_REFINED: return createRuleFeatGroupAccess2FeatGroupAccessRefined();
			case Adele2aadlPackage.RULE_FEAT_GROUP_ACCESS2_FEAT_GROUP_ACCESS_TYPED_REFINED: return createRuleFeatGroupAccess2FeatGroupAccessTypedRefined();
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_DEST_SUBCOMPO: return createRuleDirFeatConn2DirFeatConnSrcParentDestSubcompo();
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_DEST_SUBCOMPO_SAME_PORT: return createRuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort();
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT: return createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParent();
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_FEAT_GROUP: return createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup();
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_FEAT_GROUP_INVERSE: return createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse();
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_FEAT_GROUP_DEST_SUBCOMPO: return createRuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo();
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_PARENT_FEAT_GROUP_INVERSE_DEST_SUBCOMPO: return createRuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo();
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_PARENT_SAME_PORT: return createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort();
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO: return createRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo();
			case Adele2aadlPackage.RULE_DIR_FEAT_CONN2_DIR_FEAT_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO_SAME_PORT: return createRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort();
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO_FEAT: return createRuleAccessConn2AccessConnSrcParentDestSubcompoFeat();
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO_FEAT_SAME_ACCESS: return createRuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess();
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_PARENT: return createRuleAccessConn2AccessConnSrcSubcompoFeatDestParent();
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_PARENT_SAME_ACCESS: return createRuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess();
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_SUBCOMPO_FEAT: return createRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat();
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_PARENT: return createRuleAccessConn2AccessConnSrcSubcompoDestParent();
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO_FEAT: return createRuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat();
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_DEST_SUBCOMPO: return createRuleAccessConn2AccessConnSrcSubcompoDestSubcompo();
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_PARENT_DEST_SUBCOMPO: return createRuleAccessConn2AccessConnSrcParentDestSubcompo();
			case Adele2aadlPackage.RULE_ACCESS_CONN2_ACCESS_CONN_SRC_SUBCOMPO_FEAT_DEST_SUBCOMPO: return createRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case Adele2aadlPackage.SDM_INTERPRETER:
				return createSDMInterpreterFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case Adele2aadlPackage.SDM_INTERPRETER:
				return convertSDMInterpreterToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrAxiom createCorrAxiom() {
		CorrAxiomImpl corrAxiom = new CorrAxiomImpl();
		return corrAxiom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrComponentType createCorrComponentType() {
		CorrComponentTypeImpl corrComponentType = new CorrComponentTypeImpl();
		return corrComponentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrComponentImplementation createCorrComponentImplementation() {
		CorrComponentImplementationImpl corrComponentImplementation = new CorrComponentImplementationImpl();
		return corrComponentImplementation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrSubcomponent createCorrSubcomponent() {
		CorrSubcomponentImpl corrSubcomponent = new CorrSubcomponentImpl();
		return corrSubcomponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrDirectedFeature createCorrDirectedFeature() {
		CorrDirectedFeatureImpl corrDirectedFeature = new CorrDirectedFeatureImpl();
		return corrDirectedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrAccess createCorrAccess() {
		CorrAccessImpl corrAccess = new CorrAccessImpl();
		return corrAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrDirectedFeatureConnection createCorrDirectedFeatureConnection() {
		CorrDirectedFeatureConnectionImpl corrDirectedFeatureConnection = new CorrDirectedFeatureConnectionImpl();
		return corrDirectedFeatureConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrAccessConnection createCorrAccessConnection() {
		CorrAccessConnectionImpl corrAccessConnection = new CorrAccessConnectionImpl();
		return corrAccessConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Adele2aadlRuleSet createAdele2aadlRuleSet() {
		Adele2aadlRuleSetImpl adele2aadlRuleSet = new Adele2aadlRuleSetImpl();
		return adele2aadlRuleSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RulePackage2AadlPackage createRulePackage2AadlPackage() {
		RulePackage2AadlPackageImpl rulePackage2AadlPackage = new RulePackage2AadlPackageImpl();
		return rulePackage2AadlPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleComponent2ComponentType createRuleComponent2ComponentType() {
		RuleComponent2ComponentTypeImpl ruleComponent2ComponentType = new RuleComponent2ComponentTypeImpl();
		return ruleComponent2ComponentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleComponent2ComponentTypeWithExtends createRuleComponent2ComponentTypeWithExtends() {
		RuleComponent2ComponentTypeWithExtendsImpl ruleComponent2ComponentTypeWithExtends = new RuleComponent2ComponentTypeWithExtendsImpl();
		return ruleComponent2ComponentTypeWithExtends;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleFeatureGroup2FeatureGroupType createRuleFeatureGroup2FeatureGroupType() {
		RuleFeatureGroup2FeatureGroupTypeImpl ruleFeatureGroup2FeatureGroupType = new RuleFeatureGroup2FeatureGroupTypeImpl();
		return ruleFeatureGroup2FeatureGroupType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleFeatureGroup2FeatureGroupTypeWithInverse createRuleFeatureGroup2FeatureGroupTypeWithInverse() {
		RuleFeatureGroup2FeatureGroupTypeWithInverseImpl ruleFeatureGroup2FeatureGroupTypeWithInverse = new RuleFeatureGroup2FeatureGroupTypeWithInverseImpl();
		return ruleFeatureGroup2FeatureGroupTypeWithInverse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleFeatureGroup2FeatureGroupTypeWithExtends createRuleFeatureGroup2FeatureGroupTypeWithExtends() {
		RuleFeatureGroup2FeatureGroupTypeWithExtendsImpl ruleFeatureGroup2FeatureGroupTypeWithExtends = new RuleFeatureGroup2FeatureGroupTypeWithExtendsImpl();
		return ruleFeatureGroup2FeatureGroupTypeWithExtends;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleFeatureGroup2FeatureGroupTypeWithExtendsInverse createRuleFeatureGroup2FeatureGroupTypeWithExtendsInverse() {
		RuleFeatureGroup2FeatureGroupTypeWithExtendsInverseImpl ruleFeatureGroup2FeatureGroupTypeWithExtendsInverse = new RuleFeatureGroup2FeatureGroupTypeWithExtendsInverseImpl();
		return ruleFeatureGroup2FeatureGroupTypeWithExtendsInverse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleComponent2ComponentImplementation createRuleComponent2ComponentImplementation() {
		RuleComponent2ComponentImplementationImpl ruleComponent2ComponentImplementation = new RuleComponent2ComponentImplementationImpl();
		return ruleComponent2ComponentImplementation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleComponent2ComponentImplementationWithExtends createRuleComponent2ComponentImplementationWithExtends() {
		RuleComponent2ComponentImplementationWithExtendsImpl ruleComponent2ComponentImplementationWithExtends = new RuleComponent2ComponentImplementationWithExtendsImpl();
		return ruleComponent2ComponentImplementationWithExtends;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleComponent2Subcomponent createRuleComponent2Subcomponent() {
		RuleComponent2SubcomponentImpl ruleComponent2Subcomponent = new RuleComponent2SubcomponentImpl();
		return ruleComponent2Subcomponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleComponent2SubcomponentTyped createRuleComponent2SubcomponentTyped() {
		RuleComponent2SubcomponentTypedImpl ruleComponent2SubcomponentTyped = new RuleComponent2SubcomponentTypedImpl();
		return ruleComponent2SubcomponentTyped;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleComponent2SubcomponentTypedAsParent createRuleComponent2SubcomponentTypedAsParent() {
		RuleComponent2SubcomponentTypedAsParentImpl ruleComponent2SubcomponentTypedAsParent = new RuleComponent2SubcomponentTypedAsParentImpl();
		return ruleComponent2SubcomponentTypedAsParent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleComponent2SubcomponentRefined createRuleComponent2SubcomponentRefined() {
		RuleComponent2SubcomponentRefinedImpl ruleComponent2SubcomponentRefined = new RuleComponent2SubcomponentRefinedImpl();
		return ruleComponent2SubcomponentRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleComponent2SubcomponentRefinedTyped createRuleComponent2SubcomponentRefinedTyped() {
		RuleComponent2SubcomponentRefinedTypedImpl ruleComponent2SubcomponentRefinedTyped = new RuleComponent2SubcomponentRefinedTypedImpl();
		return ruleComponent2SubcomponentRefinedTyped;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleComponent2SubcomponentRefinedTypedAsParent createRuleComponent2SubcomponentRefinedTypedAsParent() {
		RuleComponent2SubcomponentRefinedTypedAsParentImpl ruleComponent2SubcomponentRefinedTypedAsParent = new RuleComponent2SubcomponentRefinedTypedAsParentImpl();
		return ruleComponent2SubcomponentRefinedTypedAsParent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleComponent2SubcomponentTypedRefined createRuleComponent2SubcomponentTypedRefined() {
		RuleComponent2SubcomponentTypedRefinedImpl ruleComponent2SubcomponentTypedRefined = new RuleComponent2SubcomponentTypedRefinedImpl();
		return ruleComponent2SubcomponentTypedRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleComponent2SubcomponentTypedAsParentRefined createRuleComponent2SubcomponentTypedAsParentRefined() {
		RuleComponent2SubcomponentTypedAsParentRefinedImpl ruleComponent2SubcomponentTypedAsParentRefined = new RuleComponent2SubcomponentTypedAsParentRefinedImpl();
		return ruleComponent2SubcomponentTypedAsParentRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleComponent2SubcomponentTypedRefinedTyped createRuleComponent2SubcomponentTypedRefinedTyped() {
		RuleComponent2SubcomponentTypedRefinedTypedImpl ruleComponent2SubcomponentTypedRefinedTyped = new RuleComponent2SubcomponentTypedRefinedTypedImpl();
		return ruleComponent2SubcomponentTypedRefinedTyped;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleComponent2SubcomponentTypedAsParentRefinedTyped createRuleComponent2SubcomponentTypedAsParentRefinedTyped() {
		RuleComponent2SubcomponentTypedAsParentRefinedTypedImpl ruleComponent2SubcomponentTypedAsParentRefinedTyped = new RuleComponent2SubcomponentTypedAsParentRefinedTypedImpl();
		return ruleComponent2SubcomponentTypedAsParentRefinedTyped;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleComponent2SubcomponentTypedRefinedTypedAsParent createRuleComponent2SubcomponentTypedRefinedTypedAsParent() {
		RuleComponent2SubcomponentTypedRefinedTypedAsParentImpl ruleComponent2SubcomponentTypedRefinedTypedAsParent = new RuleComponent2SubcomponentTypedRefinedTypedAsParentImpl();
		return ruleComponent2SubcomponentTypedRefinedTypedAsParent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDirectedFeat2DirectedFeat createRuleDirectedFeat2DirectedFeat() {
		RuleDirectedFeat2DirectedFeatImpl ruleDirectedFeat2DirectedFeat = new RuleDirectedFeat2DirectedFeatImpl();
		return ruleDirectedFeat2DirectedFeat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDirectedFeat2DirectedFeatTyped createRuleDirectedFeat2DirectedFeatTyped() {
		RuleDirectedFeat2DirectedFeatTypedImpl ruleDirectedFeat2DirectedFeatTyped = new RuleDirectedFeat2DirectedFeatTypedImpl();
		return ruleDirectedFeat2DirectedFeatTyped;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDirectedFeat2DirectedFeatRefined createRuleDirectedFeat2DirectedFeatRefined() {
		RuleDirectedFeat2DirectedFeatRefinedImpl ruleDirectedFeat2DirectedFeatRefined = new RuleDirectedFeat2DirectedFeatRefinedImpl();
		return ruleDirectedFeat2DirectedFeatRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDirectedFeat2DirectedFeatTypedRefined createRuleDirectedFeat2DirectedFeatTypedRefined() {
		RuleDirectedFeat2DirectedFeatTypedRefinedImpl ruleDirectedFeat2DirectedFeatTypedRefined = new RuleDirectedFeat2DirectedFeatTypedRefinedImpl();
		return ruleDirectedFeat2DirectedFeatTypedRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleAccess2Access createRuleAccess2Access() {
		RuleAccess2AccessImpl ruleAccess2Access = new RuleAccess2AccessImpl();
		return ruleAccess2Access;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleAccess2AccessTyped createRuleAccess2AccessTyped() {
		RuleAccess2AccessTypedImpl ruleAccess2AccessTyped = new RuleAccess2AccessTypedImpl();
		return ruleAccess2AccessTyped;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleAccess2AccessTypedAsParent createRuleAccess2AccessTypedAsParent() {
		RuleAccess2AccessTypedAsParentImpl ruleAccess2AccessTypedAsParent = new RuleAccess2AccessTypedAsParentImpl();
		return ruleAccess2AccessTypedAsParent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleAccess2AccessRefined createRuleAccess2AccessRefined() {
		RuleAccess2AccessRefinedImpl ruleAccess2AccessRefined = new RuleAccess2AccessRefinedImpl();
		return ruleAccess2AccessRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleAccess2AccessTypedRefined createRuleAccess2AccessTypedRefined() {
		RuleAccess2AccessTypedRefinedImpl ruleAccess2AccessTypedRefined = new RuleAccess2AccessTypedRefinedImpl();
		return ruleAccess2AccessTypedRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleAccess2AccessTypedAsParentRefined createRuleAccess2AccessTypedAsParentRefined() {
		RuleAccess2AccessTypedAsParentRefinedImpl ruleAccess2AccessTypedAsParentRefined = new RuleAccess2AccessTypedAsParentRefinedImpl();
		return ruleAccess2AccessTypedAsParentRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleFeatGroupDirFeat2FeatGroupDirFeat createRuleFeatGroupDirFeat2FeatGroupDirFeat() {
		RuleFeatGroupDirFeat2FeatGroupDirFeatImpl ruleFeatGroupDirFeat2FeatGroupDirFeat = new RuleFeatGroupDirFeat2FeatGroupDirFeatImpl();
		return ruleFeatGroupDirFeat2FeatGroupDirFeat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleFeatGroupDirFeat2FeatGroupDirFeatTyped createRuleFeatGroupDirFeat2FeatGroupDirFeatTyped() {
		RuleFeatGroupDirFeat2FeatGroupDirFeatTypedImpl ruleFeatGroupDirFeat2FeatGroupDirFeatTyped = new RuleFeatGroupDirFeat2FeatGroupDirFeatTypedImpl();
		return ruleFeatGroupDirFeat2FeatGroupDirFeatTyped;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent() {
		RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentImpl ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent = new RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentImpl();
		return ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleFeatGroupDirFeat2FeatGroupDirFeatRefined createRuleFeatGroupDirFeat2FeatGroupDirFeatRefined() {
		RuleFeatGroupDirFeat2FeatGroupDirFeatRefinedImpl ruleFeatGroupDirFeat2FeatGroupDirFeatRefined = new RuleFeatGroupDirFeat2FeatGroupDirFeatRefinedImpl();
		return ruleFeatGroupDirFeat2FeatGroupDirFeatRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined() {
		RuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefinedImpl ruleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined = new RuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefinedImpl();
		return ruleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined() {
		RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefinedImpl ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined = new RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefinedImpl();
		return ruleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleFeatGroupAccess2FeatGroupAccess createRuleFeatGroupAccess2FeatGroupAccess() {
		RuleFeatGroupAccess2FeatGroupAccessImpl ruleFeatGroupAccess2FeatGroupAccess = new RuleFeatGroupAccess2FeatGroupAccessImpl();
		return ruleFeatGroupAccess2FeatGroupAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleFeatGroupAccess2FeatGroupAccessTyped createRuleFeatGroupAccess2FeatGroupAccessTyped() {
		RuleFeatGroupAccess2FeatGroupAccessTypedImpl ruleFeatGroupAccess2FeatGroupAccessTyped = new RuleFeatGroupAccess2FeatGroupAccessTypedImpl();
		return ruleFeatGroupAccess2FeatGroupAccessTyped;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleFeatGroupAccess2FeatGroupAccessRefined createRuleFeatGroupAccess2FeatGroupAccessRefined() {
		RuleFeatGroupAccess2FeatGroupAccessRefinedImpl ruleFeatGroupAccess2FeatGroupAccessRefined = new RuleFeatGroupAccess2FeatGroupAccessRefinedImpl();
		return ruleFeatGroupAccess2FeatGroupAccessRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleFeatGroupAccess2FeatGroupAccessTypedRefined createRuleFeatGroupAccess2FeatGroupAccessTypedRefined() {
		RuleFeatGroupAccess2FeatGroupAccessTypedRefinedImpl ruleFeatGroupAccess2FeatGroupAccessTypedRefined = new RuleFeatGroupAccess2FeatGroupAccessTypedRefinedImpl();
		return ruleFeatGroupAccess2FeatGroupAccessTypedRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDirFeatConn2DirFeatConnSrcParentDestSubcompo createRuleDirFeatConn2DirFeatConnSrcParentDestSubcompo() {
		RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoImpl ruleDirFeatConn2DirFeatConnSrcParentDestSubcompo = new RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoImpl();
		return ruleDirFeatConn2DirFeatConnSrcParentDestSubcompo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort createRuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort() {
		RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePortImpl ruleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort = new RuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePortImpl();
		return ruleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDirFeatConn2DirFeatConnSrcSubcompoDestParent createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParent() {
		RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentImpl ruleDirFeatConn2DirFeatConnSrcSubcompoDestParent = new RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentImpl();
		return ruleDirFeatConn2DirFeatConnSrcSubcompoDestParent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup() {
		RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupImpl ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup = new RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupImpl();
		return ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse() {
		RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverseImpl ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse = new RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverseImpl();
		return ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo createRuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo() {
		RuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompoImpl ruleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo = new RuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompoImpl();
		return ruleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo createRuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo() {
		RuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompoImpl ruleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo = new RuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompoImpl();
		return ruleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort() {
		RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePortImpl ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort = new RuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePortImpl();
		return ruleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo createRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo() {
		RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoImpl ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo = new RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoImpl();
		return ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort createRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort() {
		RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePortImpl ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort = new RuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePortImpl();
		return ruleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleAccessConn2AccessConnSrcParentDestSubcompoFeat createRuleAccessConn2AccessConnSrcParentDestSubcompoFeat() {
		RuleAccessConn2AccessConnSrcParentDestSubcompoFeatImpl ruleAccessConn2AccessConnSrcParentDestSubcompoFeat = new RuleAccessConn2AccessConnSrcParentDestSubcompoFeatImpl();
		return ruleAccessConn2AccessConnSrcParentDestSubcompoFeat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess createRuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess() {
		RuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccessImpl ruleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess = new RuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccessImpl();
		return ruleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleAccessConn2AccessConnSrcSubcompoFeatDestParent createRuleAccessConn2AccessConnSrcSubcompoFeatDestParent() {
		RuleAccessConn2AccessConnSrcSubcompoFeatDestParentImpl ruleAccessConn2AccessConnSrcSubcompoFeatDestParent = new RuleAccessConn2AccessConnSrcSubcompoFeatDestParentImpl();
		return ruleAccessConn2AccessConnSrcSubcompoFeatDestParent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess createRuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess() {
		RuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccessImpl ruleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess = new RuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccessImpl();
		return ruleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat createRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat() {
		RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeatImpl ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat = new RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeatImpl();
		return ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleAccessConn2AccessConnSrcSubcompoDestParent createRuleAccessConn2AccessConnSrcSubcompoDestParent() {
		RuleAccessConn2AccessConnSrcSubcompoDestParentImpl ruleAccessConn2AccessConnSrcSubcompoDestParent = new RuleAccessConn2AccessConnSrcSubcompoDestParentImpl();
		return ruleAccessConn2AccessConnSrcSubcompoDestParent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat createRuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat() {
		RuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeatImpl ruleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat = new RuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeatImpl();
		return ruleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleAccessConn2AccessConnSrcSubcompoDestSubcompo createRuleAccessConn2AccessConnSrcSubcompoDestSubcompo() {
		RuleAccessConn2AccessConnSrcSubcompoDestSubcompoImpl ruleAccessConn2AccessConnSrcSubcompoDestSubcompo = new RuleAccessConn2AccessConnSrcSubcompoDestSubcompoImpl();
		return ruleAccessConn2AccessConnSrcSubcompoDestSubcompo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleAccessConn2AccessConnSrcParentDestSubcompo createRuleAccessConn2AccessConnSrcParentDestSubcompo() {
		RuleAccessConn2AccessConnSrcParentDestSubcompoImpl ruleAccessConn2AccessConnSrcParentDestSubcompo = new RuleAccessConn2AccessConnSrcParentDestSubcompoImpl();
		return ruleAccessConn2AccessConnSrcParentDestSubcompo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo createRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo() {
		RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoImpl ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo = new RuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoImpl();
		return ruleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SDEEclipseSDMInterpreter createSDMInterpreterFromString(
			EDataType eDataType, String initialValue) {
		return (SDEEclipseSDMInterpreter)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSDMInterpreterToString(EDataType eDataType,
			Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Adele2aadlPackage getAdele2aadlPackage() {
		return (Adele2aadlPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Adele2aadlPackage getPackage() {
		return Adele2aadlPackage.eINSTANCE;
	}

} //Adele2aadlFactoryImpl
