/**
 */
package fr.labsticc.adosate.adele2aadl.impl;

import de.hpi.sam.mote.impl.TGGNodeImpl;
import fr.labsticc.adosate.adele2aadl.Adele2aadlPackage;
import fr.labsticc.adosate.adele2aadl.CorrFeature;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class CorrFeatureImpl extends TGGNodeImpl implements
		CorrFeature {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrFeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Adele2aadlPackage.eINSTANCE.getCorrFeature();
	}

} //CorrFeatureImpl
