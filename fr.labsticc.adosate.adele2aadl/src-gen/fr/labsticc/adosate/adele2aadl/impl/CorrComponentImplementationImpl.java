/**
 */
package fr.labsticc.adosate.adele2aadl.impl;

import fr.labsticc.adosate.adele2aadl.Adele2aadlPackage;
import fr.labsticc.adosate.adele2aadl.CorrComponentImplementation;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Component Implementation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CorrComponentImplementationImpl extends CorrComponentImpl
		implements CorrComponentImplementation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrComponentImplementationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Adele2aadlPackage.eINSTANCE.getCorrComponentImplementation();
	}

} //CorrComponentImplementationImpl
