/**
 */
package fr.labsticc.adosate.adele2aadl.impl;

import de.hpi.sam.mote.impl.TGGNodeImpl;
import fr.labsticc.adosate.adele2aadl.Adele2aadlPackage;
import fr.labsticc.adosate.adele2aadl.CorrAxiom;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Axiom</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CorrAxiomImpl extends TGGNodeImpl implements CorrAxiom {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrAxiomImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Adele2aadlPackage.eINSTANCE.getCorrAxiom();
	}

} //CorrAxiomImpl
