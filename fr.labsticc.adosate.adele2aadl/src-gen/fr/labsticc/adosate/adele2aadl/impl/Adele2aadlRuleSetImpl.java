/**
 */
package fr.labsticc.adosate.adele2aadl.impl;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;

import de.hpi.sam.mote.TransformationDirection;
import de.hpi.sam.mote.impl.TransformationException;
import de.hpi.sam.mote.rules.impl.TGGRuleSetImpl;
import de.mdelab.mlsdm.interpreter.debug.MLSDMDebugger;
//import de.mdelab.mlsdm.interpreter.debug.ui.launcher.MLSDMLaunchDebuggerPerfNotificationReceiver;
import de.mdelab.sdm.interpreter.sde.eclipse.SDEEclipseSDMInterpreter;
import fr.labsticc.adosate.adele2aadl.Adele2aadlPackage;
import fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet;
import fr.labsticc.adosate.adele2aadl.helpers.Adele2AadlHelper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rule Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.labsticc.adosate.adele2aadl.impl.Adele2aadlRuleSetImpl#getSdmInterpreter <em>Sdm Interpreter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class Adele2aadlRuleSetImpl extends TGGRuleSetImpl implements
		Adele2aadlRuleSet {
	
	/**
	 * The default value of the '{@link #getSdmInterpreter() <em>Sdm Interpreter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSdmInterpreter()
	 * @generated
	 * @ordered
	 */
	protected static final SDEEclipseSDMInterpreter SDM_INTERPRETER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSdmInterpreter() <em>Sdm Interpreter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSdmInterpreter()
	 * @generated
	 * @ordered
	 */
	protected SDEEclipseSDMInterpreter sdmInterpreter = SDM_INTERPRETER_EDEFAULT;
	
	private MLSDMDebugger debugger = null;
	
	//private MLSDMLaunchDebuggerPerfNotificationReceiver debugPerfNotificationReceiver = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Adele2aadlRuleSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Adele2aadlPackage.eINSTANCE.getAdele2aadlRuleSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SDEEclipseSDMInterpreter getSdmInterpreter() {
		return sdmInterpreter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSdmInterpreter(SDEEclipseSDMInterpreter newSdmInterpreter) {
		SDEEclipseSDMInterpreter oldSdmInterpreter = sdmInterpreter;
		sdmInterpreter = newSdmInterpreter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Adele2aadlPackage.ADELE2AADL_RULE_SET__SDM_INTERPRETER, oldSdmInterpreter, sdmInterpreter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void createRules() {
		
		try {
			de.mdelab.sdm.interpreter.sde.notifications.SDENotificationEmitter notificationEmitter = new de.mdelab.sdm.interpreter.sde.notifications.SDENotificationEmitter();
			final SDEEclipseSDMInterpreter interpreter = new SDEEclipseSDMInterpreter( getClass().getClassLoader(), notificationEmitter );
//			final int port =  ConnectionUtil.findNextAvailablePort( Integer.parseInt( ISDLaunchUIConstants.SERVER_DEFAULT_PORT ) );
//			debugger = new MLSDMDebugger( interpreter, port, ConnectionUtil.findNextAvailablePort( port ) );
		
			this.setSdmInterpreter( interpreter );
		} catch (de.mdelab.sdm.interpreter.core.SDMException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
		java.lang.reflect.Method method;
		
			
				this.setAxiom(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRulePackage2AadlPackage());
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleComponent2ComponentType());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleComponent2ComponentTypeWithExtends());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleFeatureGroup2FeatureGroupType());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleFeatureGroup2FeatureGroupTypeWithInverse());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleFeatureGroup2FeatureGroupTypeWithExtends());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleFeatureGroup2FeatureGroupTypeWithExtendsInverse());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleComponent2ComponentImplementation());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleComponent2ComponentImplementationWithExtends());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleComponent2Subcomponent());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleComponent2SubcomponentTyped());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleComponent2SubcomponentTypedAsParent());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleComponent2SubcomponentRefined());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleComponent2SubcomponentRefinedTyped());	
			
				try {
					method = fr.labsticc.adosate.adele2aadl.helpers.Adele2AadlHelper.class.getMethod( "rootSubcomponentType", new Class<?>[] { org.osate.aadl2.Subcomponent.class } );
					if ( !getExternalReferences().contains( method ) ) {
						getExternalReferences().add( method );
					}
				}
				catch ( final NoSuchMethodException p_ex ) {
					throw new RuntimeException( p_ex );
				}
				catch ( final SecurityException p_ex ) {
					throw new RuntimeException( p_ex );
				}
					
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleComponent2SubcomponentRefinedTypedAsParent());	
			
				try {
					method = fr.labsticc.adosate.adele2aadl.helpers.Adele2AadlHelper.class.getMethod( "rootSubcomponentType", new Class<?>[] { org.osate.aadl2.Subcomponent.class } );
					if ( !getExternalReferences().contains( method ) ) {
						getExternalReferences().add( method );
					}
				}
				catch ( final NoSuchMethodException p_ex ) {
					throw new RuntimeException( p_ex );
				}
				catch ( final SecurityException p_ex ) {
					throw new RuntimeException( p_ex );
				}
					
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleComponent2SubcomponentTypedRefined());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleComponent2SubcomponentTypedAsParentRefined());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleComponent2SubcomponentTypedRefinedTyped());	
			
				try {
					method = fr.labsticc.adosate.adele2aadl.helpers.Adele2AadlHelper.class.getMethod( "rootSubcomponentType", new Class<?>[] { org.osate.aadl2.Subcomponent.class } );
					if ( !getExternalReferences().contains( method ) ) {
						getExternalReferences().add( method );
					}
				}
				catch ( final NoSuchMethodException p_ex ) {
					throw new RuntimeException( p_ex );
				}
				catch ( final SecurityException p_ex ) {
					throw new RuntimeException( p_ex );
				}
					
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleComponent2SubcomponentTypedAsParentRefinedTyped());	
			
				try {
					method = fr.labsticc.adosate.adele2aadl.helpers.Adele2AadlHelper.class.getMethod( "rootSubcomponentType", new Class<?>[] { org.osate.aadl2.Subcomponent.class } );
					if ( !getExternalReferences().contains( method ) ) {
						getExternalReferences().add( method );
					}
				}
				catch ( final NoSuchMethodException p_ex ) {
					throw new RuntimeException( p_ex );
				}
				catch ( final SecurityException p_ex ) {
					throw new RuntimeException( p_ex );
				}
					
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleComponent2SubcomponentTypedRefinedTypedAsParent());	
			
				try {
					method = fr.labsticc.adosate.adele2aadl.helpers.Adele2AadlHelper.class.getMethod( "rootSubcomponentType", new Class<?>[] { org.osate.aadl2.Subcomponent.class } );
					if ( !getExternalReferences().contains( method ) ) {
						getExternalReferences().add( method );
					}
				}
				catch ( final NoSuchMethodException p_ex ) {
					throw new RuntimeException( p_ex );
				}
				catch ( final SecurityException p_ex ) {
					throw new RuntimeException( p_ex );
				}
					
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleDirectedFeat2DirectedFeat());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleDirectedFeat2DirectedFeatTyped());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleDirectedFeat2DirectedFeatRefined());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleDirectedFeat2DirectedFeatTypedRefined());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleAccess2Access());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleAccess2AccessTyped());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleAccess2AccessTypedAsParent());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleAccess2AccessRefined());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleAccess2AccessTypedRefined());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleAccess2AccessTypedAsParentRefined());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleFeatGroupDirFeat2FeatGroupDirFeat());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleFeatGroupDirFeat2FeatGroupDirFeatTyped());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleFeatGroupDirFeat2FeatGroupDirFeatRefined());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedRefined());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentRefined());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleFeatGroupAccess2FeatGroupAccess());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleFeatGroupAccess2FeatGroupAccessTyped());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleFeatGroupAccess2FeatGroupAccessRefined());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleFeatGroupAccess2FeatGroupAccessTypedRefined());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleDirFeatConn2DirFeatConnSrcParentDestSubcompo());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleDirFeatConn2DirFeatConnSrcParentDestSubcompoSamePort());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParent());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroup());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentFeatGroupInverse());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleDirFeatConn2DirFeatConnSrcParentFeatGroupDestSubcompo());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleDirFeatConn2DirFeatConnSrcParentFeatGroupInverseDestSubcompo());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleDirFeatConn2DirFeatConnSrcSubcompoDestParentSamePort());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompo());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleDirFeatConn2DirFeatConnSrcSubcompoDestSubcompoSamePort());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleAccessConn2AccessConnSrcParentDestSubcompoFeat());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleAccessConn2AccessConnSrcParentDestSubcompoFeatSameAccess());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleAccessConn2AccessConnSrcSubcompoFeatDestParent());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleAccessConn2AccessConnSrcSubcompoFeatDestParentSameAccess());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompoFeat());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleAccessConn2AccessConnSrcSubcompoDestParent());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleAccessConn2AccessConnSrcSubcompoDestSubcompoFeat());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleAccessConn2AccessConnSrcSubcompoDestSubcompo());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleAccessConn2AccessConnSrcParentDestSubcompo());	
			
			
				this.getRules().add(fr.labsticc.adosate.adele2aadl.Adele2aadlFactory.eINSTANCE.createRuleAccessConn2AccessConnSrcSubcompoFeatDestSubcompo());	
			
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Adele2aadlPackage.ADELE2AADL_RULE_SET__SDM_INTERPRETER:
				return getSdmInterpreter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Adele2aadlPackage.ADELE2AADL_RULE_SET__SDM_INTERPRETER:
				setSdmInterpreter((SDEEclipseSDMInterpreter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Adele2aadlPackage.ADELE2AADL_RULE_SET__SDM_INTERPRETER:
				setSdmInterpreter(SDM_INTERPRETER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Adele2aadlPackage.ADELE2AADL_RULE_SET__SDM_INTERPRETER:
				return SDM_INTERPRETER_EDEFAULT == null ? sdmInterpreter != null : !SDM_INTERPRETER_EDEFAULT.equals(sdmInterpreter);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (sdmInterpreter: ");
		result.append(sdmInterpreter);
		result.append(')');
		return result.toString();
	}
	
//	@Override
//	public void finalize() 
//	throws Throwable{
//		try {
//			if ( debugger != null ) {
//				debugger.terminate( true );
//				debugger = null;
//			}
//		}
//		catch ( final Exception p_ex ) {
//			p_ex.printStackTrace();
//		}
//
//		super.finalize();
//	}
	
	@Override
	public void transformationStarted(	final Resource sourceResource,
										final Resource targetResource,
										final TransformationDirection direction, 
										final boolean synchronize )
	throws TransformationException {
		//debugPerfNotificationReceiver = new MLSDMLaunchDebuggerPerfNotificationReceiver();
		//getSdmInterpreter().getNotificationEmitter().addNotificationReceiver( debugPerfNotificationReceiver );
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EObject transformationFinished(	final Resource p_sourceResource,
											final Resource p_targetResource,
											final TransformationDirection p_direction,
											final boolean pb_synchronize )
	throws TransformationException {
		try {
			Adele2AadlHelper.transformationFinished(p_sourceResource, p_targetResource, p_direction, pb_synchronize);
			
//			if ( debugPerfNotificationReceiver != null ) {
//				getSdmInterpreter().getNotificationEmitter().removeNotificationReceiver( debugPerfNotificationReceiver );
//				
//				return debugPerfNotificationReceiver.getExecutionTrace();
//			}
			
			return null;
		}
		catch( final CoreException p_ex ) {
			throw new TransformationException( p_ex );
		}
	}
} //Adele2aadlRuleSetImpl
