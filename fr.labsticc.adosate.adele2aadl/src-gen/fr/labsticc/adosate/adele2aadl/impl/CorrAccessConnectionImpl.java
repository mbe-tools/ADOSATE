/**
 */
package fr.labsticc.adosate.adele2aadl.impl;

import de.hpi.sam.mote.impl.TGGNodeImpl;
import fr.labsticc.adosate.adele2aadl.Adele2aadlPackage;
import fr.labsticc.adosate.adele2aadl.CorrAccessConnection;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Access Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CorrAccessConnectionImpl extends TGGNodeImpl implements
		CorrAccessConnection {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrAccessConnectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Adele2aadlPackage.eINSTANCE.getCorrAccessConnection();
	}

} //CorrAccessConnectionImpl
