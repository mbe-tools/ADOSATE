/**
 */
package fr.labsticc.adosate.adele2aadl.impl;

import fr.labsticc.adosate.adele2aadl.Adele2aadlPackage;
import fr.labsticc.adosate.adele2aadl.CorrComponentType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Component Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CorrComponentTypeImpl extends CorrComponentImpl implements
		CorrComponentType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrComponentTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Adele2aadlPackage.eINSTANCE.getCorrComponentType();
	}

} //CorrComponentTypeImpl
