/**
 */
package fr.labsticc.adosate.adele2aadl.impl;

import org.eclipse.emf.ecore.EClass;

import de.hpi.sam.mote.TGGNode;
import de.hpi.sam.mote.impl.TransformationException;
import de.hpi.sam.mote.rules.TransformationResult;
import de.hpi.sam.mote.rules.impl.TGGRuleImpl;
import fr.labsticc.adosate.adele2aadl.Adele2aadlPackage;
import fr.labsticc.adosate.adele2aadl.RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rule Feat Group Dir Feat2 Feat Group Dir Feat Typed As Parent</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentImpl extends
		TGGRuleImpl implements
		RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Adele2aadlPackage.eINSTANCE.getRuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationResult forwardTransformation(
			final TGGNode parentCorrNode) throws TransformationException {
			//System.out.println( new java.util.Date() + ": Starting to execute rule " + getClass().getSimpleName() + " on parent element " + parentCorrNode );
			org.eclipse.emf.ecore.resource.Resource r = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getResourceSet().getResource(
						org.eclipse.emf.common.util.URI.createPlatformPluginURI("/fr.labsticc.adosate.adele2aadl/model/story/RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent_forwardTransformation.story", true), true);
				
				de.hpi.sam.storyDiagramEcore.ActivityDiagram ad = (de.hpi.sam.storyDiagramEcore.ActivityDiagram) r.getContents().get(0);
				
				de.hpi.sam.storyDiagramEcore.Activity a = ad.getActivities().get(0); 
				
				de.mdelab.sdm.interpreter.sde.SDESDMInterpreter sdi = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getSdmInterpreter();
				//final de.mdelab.sdm.interpreter.sde.notifications.SDEOutputStreamNotificationReceiver receiver = new de.mdelab.sdm.interpreter.sde.notifications.SDEOutputStreamNotificationReceiver( null, (de.mdelab.sdm.interpreter.sde.facade.SDEMetamodelFacadeFactory) sdi.getFacadeFactory() );
				//sdi.getNotificationEmitter().addNotificationReceiver( receiver );
				
				java.util.List<Object> parameters = new java.util.ArrayList<Object>();
						
					parameters.add(parentCorrNode);
				
				try {
					final de.hpi.sam.mote.rules.TransformationResult result = (de.hpi.sam.mote.rules.TransformationResult) sdi.executeActivity(a, this, parameters);
					//System.out.println( new java.util.Date() + ": Ended executing rule " + getClass().getSimpleName() + " with result " + result.getName() + "." );
					
					return result;
				}
				catch (de.mdelab.sdm.interpreter.core.SDMException e) {
					e.printStackTrace();
					throw new de.hpi.sam.mote.impl.TransformationException("Error during execution of rule 'RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent_forwardTransformation'.", e);
				}
				//finally {
					//sdi.getNotificationEmitter().removeNotificationReceiver( receiver );
				//}
				
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationResult mappingTransformation(
			final TGGNode parentCorrNode) throws TransformationException {
			//System.out.println( new java.util.Date() + ": Starting to execute rule " + getClass().getSimpleName() + " on parent element " + parentCorrNode );
			org.eclipse.emf.ecore.resource.Resource r = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getResourceSet().getResource(
						org.eclipse.emf.common.util.URI.createPlatformPluginURI("/fr.labsticc.adosate.adele2aadl/model/story/RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent_mappingTransformation.story", true), true);
				
				de.hpi.sam.storyDiagramEcore.ActivityDiagram ad = (de.hpi.sam.storyDiagramEcore.ActivityDiagram) r.getContents().get(0);
				
				de.hpi.sam.storyDiagramEcore.Activity a = ad.getActivities().get(0); 
				
				de.mdelab.sdm.interpreter.sde.SDESDMInterpreter sdi = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getSdmInterpreter();
				//final de.mdelab.sdm.interpreter.sde.notifications.SDEOutputStreamNotificationReceiver receiver = new de.mdelab.sdm.interpreter.sde.notifications.SDEOutputStreamNotificationReceiver( null, (de.mdelab.sdm.interpreter.sde.facade.SDEMetamodelFacadeFactory) sdi.getFacadeFactory() );
				//sdi.getNotificationEmitter().addNotificationReceiver( receiver );
				
				java.util.List<Object> parameters = new java.util.ArrayList<Object>();
						
					parameters.add(parentCorrNode);
				
				try {
					final de.hpi.sam.mote.rules.TransformationResult result = (de.hpi.sam.mote.rules.TransformationResult) sdi.executeActivity(a, this, parameters);
					//System.out.println( new java.util.Date() + ": Ended executing rule " + getClass().getSimpleName() + " with result " + result.getName() + "." );
					
					return result;
				}
				catch (de.mdelab.sdm.interpreter.core.SDMException e) {
					e.printStackTrace();
					throw new de.hpi.sam.mote.impl.TransformationException("Error during execution of rule 'RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent_mappingTransformation'.", e);
				}
				//finally {
					//sdi.getNotificationEmitter().removeNotificationReceiver( receiver );
				//}
				
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationResult reverseTransformation(
			final TGGNode parentCorrNode) throws TransformationException {
			//System.out.println( new java.util.Date() + ": Starting to execute rule " + getClass().getSimpleName() + " on parent element " + parentCorrNode );
			org.eclipse.emf.ecore.resource.Resource r = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getResourceSet().getResource(
						org.eclipse.emf.common.util.URI.createPlatformPluginURI("/fr.labsticc.adosate.adele2aadl/model/story/RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent_reverseTransformation.story", true), true);
				
				de.hpi.sam.storyDiagramEcore.ActivityDiagram ad = (de.hpi.sam.storyDiagramEcore.ActivityDiagram) r.getContents().get(0);
				
				de.hpi.sam.storyDiagramEcore.Activity a = ad.getActivities().get(0); 
				
				de.mdelab.sdm.interpreter.sde.SDESDMInterpreter sdi = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getSdmInterpreter();
				//final de.mdelab.sdm.interpreter.sde.notifications.SDEOutputStreamNotificationReceiver receiver = new de.mdelab.sdm.interpreter.sde.notifications.SDEOutputStreamNotificationReceiver( null, (de.mdelab.sdm.interpreter.sde.facade.SDEMetamodelFacadeFactory) sdi.getFacadeFactory() );
				//sdi.getNotificationEmitter().addNotificationReceiver( receiver );
				
				java.util.List<Object> parameters = new java.util.ArrayList<Object>();
						
					parameters.add(parentCorrNode);
				
				try {
					final de.hpi.sam.mote.rules.TransformationResult result = (de.hpi.sam.mote.rules.TransformationResult) sdi.executeActivity(a, this, parameters);
					//System.out.println( new java.util.Date() + ": Ended executing rule " + getClass().getSimpleName() + " with result " + result.getName() + "." );
					
					return result;
				}
				catch (de.mdelab.sdm.interpreter.core.SDMException e) {
					e.printStackTrace();
					throw new de.hpi.sam.mote.impl.TransformationException("Error during execution of rule 'RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent_reverseTransformation'.", e);
				}
				//finally {
					//sdi.getNotificationEmitter().removeNotificationReceiver( receiver );
				//}
				
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean forwardSynchronization(final TGGNode corrNode)
			throws TransformationException {
		
		org.eclipse.emf.ecore.resource.Resource r = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getResourceSet().getResource(
						org.eclipse.emf.common.util.URI.createPlatformPluginURI("/fr.labsticc.adosate.adele2aadl/model/story/RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent_forwardSynchronization.story", true), true);
				
				de.hpi.sam.storyDiagramEcore.ActivityDiagram ad = (de.hpi.sam.storyDiagramEcore.ActivityDiagram) r.getContents().get(0);
				
				de.hpi.sam.storyDiagramEcore.Activity a = ad.getActivities().get(0); 
				
				de.mdelab.sdm.interpreter.sde.SDESDMInterpreter sdi = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getSdmInterpreter();
				
				java.util.List<Object> parameters = new java.util.ArrayList<Object>();
						
					parameters.add(corrNode);
				
				try {
					return (Boolean) sdi.executeActivity(a, this, parameters);
				} catch (de.mdelab.sdm.interpreter.core.SDMException e) {
					e.printStackTrace();
					throw new de.hpi.sam.mote.impl.TransformationException("Error during execution of rule 'RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent_forwardSynchronization'.", e);
				}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean mappingSynchronization(final TGGNode corrNode)
			throws TransformationException {
		
		org.eclipse.emf.ecore.resource.Resource r = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getResourceSet().getResource(
						org.eclipse.emf.common.util.URI.createPlatformPluginURI("/fr.labsticc.adosate.adele2aadl/model/story/RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent_mappingSynchronization.story", true), true);
				
				de.hpi.sam.storyDiagramEcore.ActivityDiagram ad = (de.hpi.sam.storyDiagramEcore.ActivityDiagram) r.getContents().get(0);
				
				de.hpi.sam.storyDiagramEcore.Activity a = ad.getActivities().get(0); 
				
				de.mdelab.sdm.interpreter.sde.SDESDMInterpreter sdi = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getSdmInterpreter();
				
				java.util.List<Object> parameters = new java.util.ArrayList<Object>();
						
					parameters.add(corrNode);
				
				try {
					return (Boolean) sdi.executeActivity(a, this, parameters);
				} catch (de.mdelab.sdm.interpreter.core.SDMException e) {
					e.printStackTrace();
					throw new de.hpi.sam.mote.impl.TransformationException("Error during execution of rule 'RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent_mappingSynchronization'.", e);
				}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean reverseSynchronization(final TGGNode corrNode)
			throws TransformationException {
		
		org.eclipse.emf.ecore.resource.Resource r = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getResourceSet().getResource(
						org.eclipse.emf.common.util.URI.createPlatformPluginURI("/fr.labsticc.adosate.adele2aadl/model/story/RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent_reverseSynchronization.story", true), true);
				
				de.hpi.sam.storyDiagramEcore.ActivityDiagram ad = (de.hpi.sam.storyDiagramEcore.ActivityDiagram) r.getContents().get(0);
				
				de.hpi.sam.storyDiagramEcore.Activity a = ad.getActivities().get(0); 
				
				de.mdelab.sdm.interpreter.sde.SDESDMInterpreter sdi = ((fr.labsticc.adosate.adele2aadl.Adele2aadlRuleSet) this.getRuleSet()).getSdmInterpreter();
				
				java.util.List<Object> parameters = new java.util.ArrayList<Object>();
						
					parameters.add(corrNode);
				
				try {
					return (Boolean) sdi.executeActivity(a, this, parameters);
				} catch (de.mdelab.sdm.interpreter.core.SDMException e) {
					e.printStackTrace();
					throw new de.hpi.sam.mote.impl.TransformationException("Error during execution of rule 'RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParent_reverseSynchronization'.", e);
				}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initInputCorrNodeTypes() {
		
		inputCorrNodeTypes.add( fr.labsticc.adosate.adele2aadl.CorrComponentType.class );
		
	}

} //RuleFeatGroupDirFeat2FeatGroupDirFeatTypedAsParentImpl
