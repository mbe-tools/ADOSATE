/**
 */
package fr.labsticc.adosate.adele2aadl;

import de.hpi.sam.mote.TGGNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Axiom</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.adosate.adele2aadl.Adele2aadlPackage#getCorrAxiom()
 * @model
 * @generated
 */
public interface CorrAxiom extends TGGNode {
} // CorrAxiom
