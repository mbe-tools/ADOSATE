package fr.labsticc.adosate.adele2aadl.helpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.osate.aadl2.Aadl2Package;
import org.osate.aadl2.AccessConnection;
import org.osate.aadl2.ComponentClassifier;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.ConnectedElement;
import org.osate.aadl2.Connection;
import org.osate.aadl2.ConnectionEnd;
import org.osate.aadl2.DataPort;
import org.osate.aadl2.EventDataPort;
import org.osate.aadl2.EventPort;
import org.osate.aadl2.FeatureConnection;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.SubcomponentType;
import org.topcased.adele.common.utils.AdeleCommonUtils;
import org.topcased.adele.model.ADELE.util.ADELEModelUtils;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Components.Package;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;
import org.topcased.adele.model.ADELE_Features.Access;
import org.topcased.adele.model.ADELE_Features.AccessDirection;
import org.topcased.adele.model.ADELE_Features.DirectedFeature;
import org.topcased.adele.model.ADELE_Features.Feature;
import org.topcased.adele.model.ADELE_Features.FeatureGroup;
import org.topcased.adele.model.ADELE_Features.PortDirection;
import org.topcased.adele.model.ADELE_Relations.ADELE_RelationsPackage;
import org.topcased.adele.model.ADELE_Relations.AbstractFeatureConnection;
import org.topcased.adele.model.ADELE_Relations.DataPortConnection;
import org.topcased.adele.model.ADELE_Relations.EventDataPortConnection;
import org.topcased.adele.model.ADELE_Relations.EventPortConnection;
import org.topcased.adele.model.ADELE_Relations.FeatureGroupConnection;
import org.topcased.adele.model.ADELE_Relations.ParameterConnection;
import org.topcased.adele.model.ADELE_Relations.Relation;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.adele.model.KernelSpices.SKFeature;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;

import de.hpi.sam.mote.TransformationDirection;
import fr.labsticc.framework.emf.core.util.EMFUtil;
import fr.openpeople.models.aadl.constants.AadlExtensionHelper;

public class Adele2AadlHelper {
//	
//	private static final String TYPE = "Type";
//	private static final String IMPLEMENTATION = "Implementation";
//	private static final String SUBCOMPONENT = "Subcomponent";
//	
//	private static final String[] STANDARD_PROPERTY_SETS = { 	"AADL_Project.aadl", 
//																"Communication_Properties.aadl",
//																"Deployment_Properties.aadl",
//																"Memory_Properties.aadl",
//																"Modeling_Properties.aadl",
//																"Programming_Properties.aadl",
//																"Thread_Properties.aadl",
//																"Timing_Properties.aadl" };
//	
//	private static final Collection<String> STANDARD_PROPERTY_SET_URIS = new ArrayList<String>();
//	
//	static  {
//		for ( final String stdPropSetUri : STANDARD_PROPERTY_SETS ) {
//			final URI platUri = URI.createPlatformResourceURI( OsateResourceUtil.PLUGIN_RESOURCES_DIRECTORY_NAME + "/" + stdPropSetUri, true );
//			STANDARD_PROPERTY_SET_URIS.add( platUri.toString() );
//		}
//	}

	private static final Collection<EClass> FEAT_GROUP_CLASS = Collections.singletonList( ADELE_FeaturesPackage.Literals.FEATURE_GROUP );
	
	private static void updateSubFeatures( final FeatureGroup p_featureGroup ) {
		// First create the child features if this is a feature group with classifier.
		final FeatureGroup featGroupType = (FeatureGroup) p_featureGroup.getClassifier();
		
		if ( featGroupType != null ) {
			if ( featGroupType.getInverseFeature() != null ) {
				for ( final SKFeature feature : featGroupType.getInverseFeature().getFeatures() ) {
					// Duplicate the features of the feature group inverse into this feature
					mergeFeature( p_featureGroup, feature );
				}
			}

			if ( featGroupType.getRefinedFeature() instanceof FeatureGroup ) {
				for ( final SKFeature feature : ( (FeatureGroup) featGroupType.getRefinedFeature() ).getFeatures() ) {
					// Duplicate the features of the feature group inverse into this feature
					mergeFeature( p_featureGroup, feature );
				}
			}

			for ( final SKFeature feature : featGroupType.getFeatures() ) {
				// Duplicate the features of the feature group type into this feature
				mergeFeature( p_featureGroup, feature );
			}
		}
	}
	
	/**
	 * When a feature is created, try to duplicate it on the subcomponents or components
	 * implementation that inherit the feature and that potentially need it because it may be a connection end.
	 * If the owner is a:
	 * 	component type
	 * 		duplicate the feature in all existing components that extends this component type.
	 * 		duplicate the feature in all component implementations of the type, and all extending implementations.
	 * 		duplicate the feature in all subcomponents of the type, including the refined subcomponents.
	 * 	feature group type
	 * 		duplicate the feature in all existing feature groups that extends this feature group type or that 
	 * 		are the inverse of this feature group type.
	 * 		duplicate the feature in all feature group features of implementations and subcomponents that have this type or are
	 *		refined to this type. Also do this recursively in child feature group features.
	 * 		This is to allow connecting individual features.
	 * @param p_feature
	 */
	public static void featureCreated( final SKFeature p_feature ) {
		final SKComponent component = p_feature.getComponent();
		
		if ( component instanceof Component ) {
			final Component componentType = (Component) component;
	
			assert componentType.isType() : "The parent of this feature should be a component type.";
	
			cloneFeature( p_feature, componentType, new HashSet<Component>() );
		}
		else if ( component instanceof FeatureGroup ) {
			final FeatureGroup featGrpType = (FeatureGroup) component;
			
			assert featGrpType.isType() : "The parent of this feature should be a feature group type.";

			cloneFeature( p_feature, featGrpType );
		}
		else {
			throw new IllegalArgumentException( "Unknown type of feature component " + component );
		}
	}

	/**
	 * Search
	 * @param p_feature
	 * @param p_featureGroupType
	 * @param p_searchedCompos
	 */
	private static void cloneFeature( 	final SKFeature p_feature,
										final FeatureGroup p_featureGroupType ) {
		final String featureName = p_feature.getName();
		assert featureName != null :  "Feature should have a name";
		
		//System.out.println( "Cloning feature " + featureName + " of feature group type " + p_featureGroupType.getName() );
		// Search for all feature group. If its classifier, refined feature or inverse feature is the same as the specified 
		// feature group, merge the new feature.
		for ( final Resource resource : p_featureGroupType.eResource().getResourceSet().getResources() ) {
			if ( AdeleCommonUtils.isAdeleResource( resource ) ) {
				// For some reason not all elements are retrieved when set is used so used list
				final Collection<EObject> allFeatureGroups = new ArrayList<EObject>();
				EMFUtil.fillContentOfTypes( resource, FEAT_GROUP_CLASS, allFeatureGroups );
				
				for ( final EObject eObject : allFeatureGroups ) {
					final FeatureGroup featGroup = (FeatureGroup) eObject;
					
					// If the feature group is a type, check that its inverse or extended feature group is
					// the same as the type and if so, duplicate the feature.
					if ( featGroup.isType() ) {
						if ( featGroup.getInverseFeature() == p_featureGroupType ) {
							mergeInverseFeature( featGroup, p_feature );
						}
						else if ( featGroup.getRefinedFeature() == p_featureGroupType ) {
							mergeFeature( featGroup, p_feature );
						}
					}
					else {
						// Do not duplicate features of features of feature group types
						final SKComponent compo = featGroup.getComponent();
						final boolean componentIsType;
						
						if ( compo instanceof Component ) {
							componentIsType = ( (Component) compo ).isType();
						}
						else if ( compo instanceof FeatureGroup ) {
							componentIsType = ( (FeatureGroup) compo ).isType();
						}
						else {
							componentIsType = false;
						}
						
						final SKComponent featClassifier = featGroup.getClassifier();
						
						if ( !componentIsType && featClassifier instanceof FeatureGroup ) {
							final FeatureGroup fgClassifier = (FeatureGroup) featClassifier;
							
							if ( 	fgClassifier == p_featureGroupType ||
									fgClassifier.getRefinedFeature() == p_featureGroupType ) {
								mergeFeature( featGroup, p_feature );
							}
							else if ( fgClassifier.getInverseFeature() == p_featureGroupType ) {
								mergeInverseFeature( featGroup, p_feature );
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Duplicate the created feature in:
	 * - implementations of the feature owning type
	 * - subcomponents of the feature owning type
	 * - type extending the feature owning type
	 * Limit the scope to the actual resource?
	 * @param p_feature
	 * @param p_componentType
	 * @param p_searchedCompos
	 */
	private static void cloneFeature( 	final SKFeature p_feature,
										final Component p_componentType,
										final Set<Component> p_searchedCompos ) {
		final String featureName = p_feature.getName();
		assert featureName != null :  "Feature should have a name";
		
		// Search if there are component implementation
		for ( final SKHierarchicalObject children : p_componentType.getChildren() ) {
			final Component childCompo = (Component) children;
			mergeFeature( childCompo, p_feature );
		}
		
		// Search if there are any subcomponents of the type
		for ( final Resource packRes : p_componentType.eResource().getResourceSet().getResources() ) {
			for ( final EObject rootObject : packRes.getContents() ) {
				if ( rootObject instanceof Package ) {
					final Package adelePackage = (Package) rootObject; 
	
					for ( final SKHierarchicalObject type : adelePackage.getChildren() ) {
						for ( final SKHierarchicalObject implementation : type.getChildren() ) {
							for ( final SKHierarchicalObject subcomponent : implementation.getChildren() ) {
								final Component subcompo = (Component) subcomponent; 
			
								if ( subcompo.getRefines() == p_componentType || subcompo.getType() == p_componentType ) {
									mergeFeature( subcompo, p_feature );
								}
							}
						}
	
						if ( type instanceof Component ) {
							final Component compoType = (Component) type;
							final Component extendedCompoType = compoType.getRefines();
						
							if ( extendedCompoType == p_componentType && !p_searchedCompos.contains( compoType ) ) {
								mergeFeature( compoType, p_feature );
								cloneFeature( p_feature, compoType, p_searchedCompos );
	
								p_searchedCompos.add( compoType );
							}
						}
					}
				}
			}
		}
	}
	
	private static void mergeFeature( 	final Component p_component,
										final SKFeature p_feature ) {
		mergeFeature( p_component.getFeatures(), p_feature );
	}
	
	private static <T extends SKFeature> void mergeFeature( 	final Collection<T> p_features,
																final T p_feature ) {
		final T clonedFeature = EcoreUtil.copy( p_feature );
		
		// This is to ensure features of feature group that have been duplicated have their own id.
		if ( clonedFeature instanceof FeatureGroup ) {
			// Clone features from feature group type
			updateSubFeatures( (FeatureGroup) clonedFeature );
		}
		
		final T feature = ADELEModelUtils.findElementByName( p_feature, p_features );

		if ( feature != null ) {
			p_features.remove( feature );
		}

		p_features.add( clonedFeature );
	}
	
	private static <T extends SKFeature> void mergeInverseFeature( 	final Collection<T> p_features,
																	final T p_feature ) {
		final T clonedFeature = EcoreUtil.copy( p_feature );
		final T feature = ADELEModelUtils.findElementByName( p_feature, p_features );
		
		if ( feature != null ) {
			p_features.remove( feature );
		}

		p_features.add( clonedFeature );

		if ( clonedFeature instanceof DirectedFeature ) {
			inverseDirection( (DirectedFeature) clonedFeature );
		}
		else if ( clonedFeature instanceof Access ) {
			inverseDirection( (Access) clonedFeature );
		}
		else {
			throw new IllegalArgumentException( "Unexpected type of feature " + p_feature );
		}
	}
	
	private static void inverseDirection( final Access p_feature ) {
		switch( p_feature.getDirection() ) {
			case PROVIDED_LITERAL:
				p_feature.setDirection( AccessDirection.REQUIRED_LITERAL );
				
				break;
			
			case REQUIRED_LITERAL:
				p_feature.setDirection( AccessDirection.PROVIDED_LITERAL );
				
				break;
			
			default:
				throw new IllegalArgumentException( "Unknown access direction " + p_feature.getDirection() );
		}
	}
	
	private static void inverseDirection( final DirectedFeature p_feature ) {
		switch( p_feature.getDirection() ) {
			case IN_LITERAL:
				p_feature.setDirection( PortDirection.OUT_LITERAL );
				
				break;
			
			case OUT_LITERAL:
				p_feature.setDirection( PortDirection.IN_LITERAL );
				
				break;
			
			case INOUT_LITERAL:
				break;
			
			default:
				throw new IllegalArgumentException( "Unknown access direction " + p_feature.getDirection() );
		}
	}

	private static void mergeFeature( 	final FeatureGroup p_featureGroup,
										final SKFeature p_feature ) {
		mergeFeature( p_featureGroup.getFeatures(), p_feature );
	}

	private static void mergeInverseFeature( 	final FeatureGroup p_featureGroup,
												final SKFeature p_feature ) {
		mergeInverseFeature( p_featureGroup.getFeatures(), p_feature );
	}
	
	private static void cloneFeatures( final Component p_component ) {
		final Component type;
		
		if ( p_component.isSubcomponent() ) {
			type = p_component.getSubcomponentType(); 
		}
		else if ( p_component.isImplementation() ) {
			type = p_component.getType();
		}
		else {
			type = p_component.getRefines();
		}

		
		if ( type != null ) {
			for ( final SKFeature feature : type.getAllRealFeatures() ) {
				mergeFeature( p_component, feature );
			}
		}
	}

	/**
	 * Called whenever a feature group type is created. Used to duplicate the features from 
	 * the extended or inverse feature if any.
	 * @param p_featureGroupType
	 */
	public static void featureGrouptTypeCreated( final FeatureGroup p_featureGroupType ) {
		assert p_featureGroupType.isType() : "Feature group " + p_featureGroupType + " is not a type.";
		
		final FeatureGroup extended = (FeatureGroup) p_featureGroupType.getRefinedFeature();
		
		if ( extended != null ) {
			for ( final SKFeature feature : extended.getFeatures() ) {
				mergeFeature( p_featureGroupType, feature );
			}
		}
		
		final FeatureGroup inverse = p_featureGroupType.getInverseFeature();

		if ( inverse != null ) {
			for ( final SKFeature feature : inverse.getFeatures() ) {
				mergeInverseFeature( p_featureGroupType, feature );
			}
		}
	}

	public static void componentTypeCreatedWithExtends( final Component p_componentType ) {
		assert p_componentType.isType() : "Component " + p_componentType + " is not a type.";
		
		cloneFeatures( p_componentType );
	}
	
	public static void componentImplementationCreated( final Component p_componentImpl ) {
		cloneFeatures( p_componentImpl );
	}

	public static void componentImplementationCreatedWithExtends( final Component p_componentImpl ) {
		assert p_componentImpl.isImplementation() : "Component " + p_componentImpl + " is not an implementation.";

		componentImplementationCreated( p_componentImpl );
		
		final Component extendedCompo = p_componentImpl.getRefines();
		
		if ( extendedCompo != null ) {
			for ( final SKHierarchicalObject subcompo : extendedCompo.getChildren() ) {
				cloneSubcomponent( p_componentImpl, subcompo );
			}
		}
	}
	
	private static Component cloneSubcomponent( final SKHierarchicalObject p_compImpl,
												final SKHierarchicalObject p_subcomponent ) {
		final SKHierarchicalObject subcompo = ADELEModelUtils.findElementByName( p_subcomponent, p_compImpl.getChildren() );
		
		if ( subcompo != null ) {
			return (Component) subcompo;
		}

		final Component clonedSubcompo = (Component) EcoreUtil.copy( p_subcomponent );
		clonedSubcompo.setSubcomponentRefinement( true );
		clonedSubcompo.setSubcomponentsLock( true );
		p_compImpl.getChildren().add( clonedSubcompo );
		
		return clonedSubcompo;
	}

	public static void refinedSubcomponentCreated( final Component p_subcomponent ) {
		
		// Replace the existing subcomponent with the refined one. Update the downstream cloned subcomponents.
		final SKHierarchicalObject parent = p_subcomponent.getParent();
		final SKHierarchicalObject existSubcompo = ADELEModelUtils.findElementByName( p_subcomponent, parent.getChildren() );
		
		if ( existSubcompo != null ) {
			parent.getChildren().remove( existSubcompo );
			parent.getChildren().add( p_subcomponent );
		}
		
		subcomponentCreated( p_subcomponent );
	}

	public static void refinedFeatureCreated( final SKFeature p_feature ) {

		// Replace the existing feature with the refined one. Update the downstream cloned features.
		final SKComponent component = p_feature.getComponent();
		final Collection<SKFeature> features = component.getFeatures();
		final SKFeature existFeature = ADELEModelUtils.findElementByName( p_feature, features );
		
		if ( existFeature != null ) {
			features.remove( existFeature );
			features.add( p_feature );
		}
		
		featureCreated( p_feature );
	}

	public static void subcomponentCreated( final Component p_subcomponent ) {
		
		// Search if there is any component impl that extends this subcomponent parent. If so, this subcomponent
		// must be cloned.
		final Component compoImpl = (Component) p_subcomponent.getParent();
		
		for ( final Resource packRes : p_subcomponent.eResource().getResourceSet().getResources() ) {
			for ( final EObject rootObject : packRes.getContents() ) {
				if ( rootObject instanceof Package ) {
					final Package adelePackage = (Package) rootObject; 
	
					for ( final SKHierarchicalObject type : adelePackage.getChildren() ) {
						for ( final SKHierarchicalObject implementation : type.getChildren() ) {
							final Component extendedComp = ( ( Component) implementation ).getRefines();

							if ( extendedComp == compoImpl ) {
								subcomponentCreated( cloneSubcomponent( implementation, p_subcomponent ) );
							}
						}
					}
				}
			}
		}

		cloneFeatures( p_subcomponent );
	}
	
	public static Component adeleComponent( final ComponentClassifier p_aadlCompoClass ) {
		// This may be called when not all story pattern objects are matched so not mandatory to find a result
		return adeleComponent( p_aadlCompoClass, false );
	}
	
	private static Component adeleComponent( 	final ComponentClassifier p_aadlCompoClass,
												final boolean pb_mustExist ) {
		if ( p_aadlCompoClass == null ) {
			return null;
		}
		
		final String className = p_aadlCompoClass.getName();
			
		if ( className != null ) {
			final Resource aadlRes = p_aadlCompoClass.eResource();
			final ResourceSet resSet = aadlRes.getResourceSet();
			final URI adeleResUri = adeleUri( aadlRes.getURI() );
			
			final Resource adeleRes = resSet.getResource( adeleResUri, resSet.getURIConverter().exists( adeleResUri, null ) );
			
			if ( adeleRes != null && !adeleRes.getContents().isEmpty() ) {
				final Package adelePackage = (Package) adeleRes.getContents().get( 0 );
				
				if ( p_aadlCompoClass instanceof ComponentType ) {
					return (Component) ADELEModelUtils.findElement( p_aadlCompoClass.getName(), adelePackage.getChildren() );
				}

				if ( p_aadlCompoClass instanceof ComponentImplementation ) {
					return ADELEModelUtils.findComponentImpl( (Package) adeleRes.getContents().get( 0 ), className );
				}
			}
		}
		
		if ( pb_mustExist ) {
			throw new IllegalStateException( "Adele component for AADL component classifier " + p_aadlCompoClass + " does not exist." );
		}
		
		return null;
	}

	public static void transformationFinished( 	final Resource p_sourceResource, 
												final Resource p_targetResource,
												final TransformationDirection p_direction,
												final boolean pb_synchronize ) 
	throws CoreException {
		if ( p_direction == TransformationDirection.FORWARD ) {
			AadlExtensionHelper.updateWithClauses( p_targetResource );
		}
	}
	
	private static URI adeleUri( final URI p_aadlUri ) {
		return p_aadlUri.trimFileExtension().appendFileExtension( AdeleCommonUtils.ADELE_EXT );
	}
	
	public static EClass aadlTypeClass( final Component p_adeleComponent ) {
		assert p_adeleComponent != null : "Component should not be null.";
		assert p_adeleComponent.isType() : "Component should be a type.";
		
		return (EClass) Aadl2Package.eINSTANCE.getEClassifier( p_adeleComponent.eClass().getName() + AadlExtensionHelper.TYPE );
	}
	
	public static EClass adeleTypeClass( final ComponentType p_aadlCompoType ) {
		assert p_aadlCompoType != null : "Component should not be null.";

		return (EClass) ADELE_ComponentsPackage.eINSTANCE.getEClassifier( p_aadlCompoType.eClass().getName().replace( AadlExtensionHelper.TYPE, "" ) );
	}
	
	public static EClass aadlImplementationClass( final Component p_adeleComponent ) {
		assert p_adeleComponent != null : "Component should not be null.";
		assert p_adeleComponent.isImplementation() : "Component should be an implementation.";
		
		return (EClass) Aadl2Package.eINSTANCE.getEClassifier( p_adeleComponent.eClass().getName() + AadlExtensionHelper.IMPLEMENTATION );
	}
	
	public static EClass adeleImplementationClass( final ComponentImplementation p_aadlCompoImpl ) {
		assert p_aadlCompoImpl != null : "Component should not be null.";

		return (EClass) ADELE_ComponentsPackage.eINSTANCE.getEClassifier( p_aadlCompoImpl.eClass().getName().replace( AadlExtensionHelper.IMPLEMENTATION, "" ) );
	}
	
	public static EClass aadlSubcomponentClass( final Component p_adeleComponent ) {
		assert p_adeleComponent != null : "Component should not be null.";
		assert p_adeleComponent.isSubcomponent() : "Component should be a subcomponent.";
		
		return (EClass) Aadl2Package.eINSTANCE.getEClassifier( p_adeleComponent.eClass().getName() + AadlExtensionHelper.SUBCOMPONENT );
	}
	
	public static EClass adeleSubcomponentClass( final Subcomponent p_aadlSubcompo ) {
		assert p_aadlSubcompo != null : "Subcomponent should not be null.";

		return (EClass) ADELE_ComponentsPackage.eINSTANCE.getEClassifier( p_aadlSubcompo.eClass().getName().replace( AadlExtensionHelper.SUBCOMPONENT, "" ) );
	}
	
	public static EClass aadlFeatureClass( final Feature p_adeleFeature ) {
		assert p_adeleFeature != null : "Adele feature should not be null.";
		
		return (EClass) Aadl2Package.eINSTANCE.getEClassifier( p_adeleFeature.eClass().getName() );
	}
	
	public static EClass adeleFeatureClass( final org.osate.aadl2.Feature p_aadlFeature ) {
		assert p_aadlFeature != null : "AADL feature should not be null.";

		return (EClass) ADELE_FeaturesPackage.eINSTANCE.getEClassifier( p_aadlFeature.eClass().getName() );
	}
	
	public static EClass aadlConnectionClass( final Relation p_adeleConnection ) {
		assert p_adeleConnection != null : "Adele connection should not be null.";
		
		if ( 	p_adeleConnection instanceof DataPortConnection || p_adeleConnection instanceof EventDataPortConnection || 
				p_adeleConnection instanceof EventPortConnection ) {
			return Aadl2Package.eINSTANCE.getPortConnection();
		}

		if ( p_adeleConnection instanceof ParameterConnection ) {
			return Aadl2Package.eINSTANCE.getParameterConnection();
		}

		if ( p_adeleConnection instanceof AbstractFeatureConnection ) {
			return Aadl2Package.eINSTANCE.getFeatureConnection();
		}

		if ( p_adeleConnection instanceof FeatureGroupConnection ) {
			return Aadl2Package.eINSTANCE.getFeatureGroupConnection();
		}
		
		return Aadl2Package.eINSTANCE.getAccessConnection();
	}
	
	public static EClass adeleConnectionClass( final Connection p_aadlConnection ) {
		assert p_aadlConnection != null : "AADL connection should not be null.";
		
		if ( p_aadlConnection instanceof org.osate.aadl2.PortConnection ) {
			final ConnectionEnd connEnd = ( (ConnectedElement) p_aadlConnection.getSource() ).getConnectionEnd();
			
			if ( connEnd instanceof DataPort ) {
				return ADELE_RelationsPackage.Literals.DATA_PORT_CONNECTION;
			}

			if ( connEnd instanceof EventDataPort ) {
				return ADELE_RelationsPackage.Literals.EVENT_DATA_PORT_CONNECTION;
			}

			if ( connEnd instanceof EventPort ) {
				return ADELE_RelationsPackage.Literals.EVENT_PORT_CONNECTION;
			}
			
			throw new IllegalArgumentException( "Unknown port type " + connEnd );
		}

		if ( p_aadlConnection instanceof org.osate.aadl2.ParameterConnection ) {
			return ADELE_RelationsPackage.Literals.PARAMETER_CONNECTION;
		}
		
		if ( p_aadlConnection instanceof org.osate.aadl2.FeatureGroupConnection ) {
			return ADELE_RelationsPackage.Literals.FEATURE_GROUP_CONNECTION;
		}
		
		if ( p_aadlConnection instanceof FeatureConnection ) {
			return ADELE_RelationsPackage.Literals.ABSTRACT_FEATURE_CONNECTION;
		}

		final String accessCatName = ( (AccessConnection) p_aadlConnection ).getAccessCategory().getName();

		return (EClass) ADELE_RelationsPackage.eINSTANCE.getEClassifier( accessCatName.substring( 0, 1 ).toUpperCase() + accessCatName.substring( 1 ) + p_aadlConnection.eClass().getName() );
	}
	
	public static SubcomponentType rootSubcomponentType( final Subcomponent p_subcomponent ) {
		final Subcomponent refSubcompo = p_subcomponent.getRefined();
		
		if ( refSubcompo == null ) {
			return p_subcomponent.getSubcomponentType();
		}
		
		return rootSubcomponentType( refSubcompo );
	}
//
//	public static boolean isAadlResource( final Resource p_resource ) {
//		if ( p_resource != null ) {
//			return isAadlResource( p_resource.getURI() );
//		}
//		
//		return false;
//	}
//	
//	public static boolean isAadlResource( final URI p_resourceUri ) {
//		if ( p_resourceUri != null ) {
//			return AadlExtensionHelper.AADL_EXT.equals( p_resourceUri.fileExtension() );
//		}
//		
//		return false;
//	}
}
