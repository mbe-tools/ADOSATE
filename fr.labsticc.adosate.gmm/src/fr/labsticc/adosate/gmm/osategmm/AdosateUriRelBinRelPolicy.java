/**
 */
package fr.labsticc.adosate.gmm.osategmm;

import de.hpi.sam.mote.MoteEngineRelationPolicy;

import fr.labsticc.gmm.model.megamodel.UriRelatedBinaryRelPolicy;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Adosate Uri Rel Bin Rel Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.adosate.gmm.osategmm.OsategmmPackage#getAdosateUriRelBinRelPolicy()
 * @model
 * @generated
 */
public interface AdosateUriRelBinRelPolicy extends UriRelatedBinaryRelPolicy, MoteEngineRelationPolicy {
} // AdosateUriRelBinRelPolicy
