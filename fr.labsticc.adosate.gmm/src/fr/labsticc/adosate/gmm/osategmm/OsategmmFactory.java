/**
 */
package fr.labsticc.adosate.gmm.osategmm;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.labsticc.adosate.gmm.osategmm.OsategmmPackage
 * @generated
 */
public interface OsategmmFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OsategmmFactory eINSTANCE = fr.labsticc.adosate.gmm.osategmm.impl.OsategmmFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Adele Osate Model Elements Cov Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Adele Osate Model Elements Cov Policy</em>'.
	 * @generated
	 */
	AdeleOsateModelElementsCovPolicy createAdeleOsateModelElementsCovPolicy();

	/**
	 * Returns a new object of class '<em>Adele Graph2 Sem Consistency Relation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Adele Graph2 Sem Consistency Relation</em>'.
	 * @generated
	 */
	AdeleGraph2SemConsistencyRelation createAdeleGraph2SemConsistencyRelation();

	/**
	 * Returns a new object of class '<em>Adele Osate Synchronization Relation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Adele Osate Synchronization Relation</em>'.
	 * @generated
	 */
	AdeleOsateSynchronizationRelation createAdeleOsateSynchronizationRelation();

	/**
	 * Returns a new object of class '<em>Adosate Uri Rel Bin Rel Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Adosate Uri Rel Bin Rel Policy</em>'.
	 * @generated
	 */
	AdosateUriRelBinRelPolicy createAdosateUriRelBinRelPolicy();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OsategmmPackage getOsategmmPackage();

} //OsategmmFactory
