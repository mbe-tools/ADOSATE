/**
 */
package fr.labsticc.adosate.gmm.osategmm;

import fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation;
import org.eclipse.emf.ecore.EObject;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Adele Osate Synchronization Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.adosate.gmm.osategmm.OsategmmPackage#getAdeleOsateSynchronizationRelation()
 * @model
 * @generated
 */
public interface AdeleOsateSynchronizationRelation extends MoteSynchronizationRelation {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model adeleObjectRequired="true"
	 * @generated
	 */
	EObject aadlElement(SKHierarchicalObject adeleObject);
} // AdeleOsateSynchronizationRelation
