/**
 */
package fr.labsticc.adosate.gmm.osategmm;

import de.hpi.sam.mote.MoteEngineCoveragePolicy;
import fr.labsticc.gmm.model.megamodel.ModelElementsCoveragePolicy;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Adele Osate Model Elements Cov Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.adosate.gmm.osategmm.OsategmmPackage#getAdeleOsateModelElementsCovPolicy()
 * @model
 * @generated
 */
public interface AdeleOsateModelElementsCovPolicy extends
		ModelElementsCoveragePolicy, MoteEngineCoveragePolicy {
} // AdeleOsateModelElementsCovPolicy
