/**
 */
package fr.labsticc.adosate.gmm.osategmm;

import fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation;
import fr.labsticc.gmm.model.megamodel.MetaModel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Adele Osate Graphical2 Semantic Sync Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.labsticc.adosate.gmm.osategmm.AdeleGraph2SemConsistencyRelation#getDiagramMetaModel <em>Diagram Meta Model</em>}</li>
 * </ul>
 *
 * @see fr.labsticc.adosate.gmm.osategmm.OsategmmPackage#getAdeleGraph2SemConsistencyRelation()
 * @model
 * @generated
 */
public interface AdeleGraph2SemConsistencyRelation extends
		BinaryMetaModelRelatedConsistencyRelation {

	/**
	 * Returns the value of the '<em><b>Diagram Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Diagram Meta Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Diagram Meta Model</em>' reference.
	 * @see #setDiagramMetaModel(MetaModel)
	 * @see fr.labsticc.adosate.gmm.osategmm.OsategmmPackage#getAdeleGraph2SemConsistencyRelation_DiagramMetaModel()
	 * @model required="true"
	 * @generated
	 */
	MetaModel getDiagramMetaModel();

	/**
	 * Sets the value of the '{@link fr.labsticc.adosate.gmm.osategmm.AdeleGraph2SemConsistencyRelation#getDiagramMetaModel <em>Diagram Meta Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Diagram Meta Model</em>' reference.
	 * @see #getDiagramMetaModel()
	 * @generated
	 */
	void setDiagramMetaModel(MetaModel value);
} // AdeleOsateGraphical2SemanticSyncRelation
