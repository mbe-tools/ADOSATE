/**
 */
package fr.labsticc.adosate.gmm.osategmm;

import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.mote.model.gmmmote.GmmmotePackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.labsticc.adosate.gmm.osategmm.OsategmmFactory
 * @model kind="package"
 * @generated
 */
public interface OsategmmPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "osategmm";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.labsticc.fr/osategmm/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "osategmm";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OsategmmPackage eINSTANCE = fr.labsticc.adosate.gmm.osategmm.impl.OsategmmPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.gmm.osategmm.impl.AdeleOsateModelElementsCovPolicyImpl <em>Adele Osate Model Elements Cov Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.gmm.osategmm.impl.AdeleOsateModelElementsCovPolicyImpl
	 * @see fr.labsticc.adosate.gmm.osategmm.impl.OsategmmPackageImpl#getAdeleOsateModelElementsCovPolicy()
	 * @generated
	 */
	int ADELE_OSATE_MODEL_ELEMENTS_COV_POLICY = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_MODEL_ELEMENTS_COV_POLICY__NAME = MegamodelPackage.MODEL_ELEMENTS_COVERAGE_POLICY__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_MODEL_ELEMENTS_COV_POLICY__DESCRIPTION = MegamodelPackage.MODEL_ELEMENTS_COVERAGE_POLICY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_MODEL_ELEMENTS_COV_POLICY__DISPLAY_NAME = MegamodelPackage.MODEL_ELEMENTS_COVERAGE_POLICY__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_MODEL_ELEMENTS_COV_POLICY__ID = MegamodelPackage.MODEL_ELEMENTS_COVERAGE_POLICY__ID;

	/**
	 * The number of structural features of the '<em>Adele Osate Model Elements Cov Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_MODEL_ELEMENTS_COV_POLICY_FEATURE_COUNT = MegamodelPackage.MODEL_ELEMENTS_COVERAGE_POLICY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.gmm.osategmm.impl.AdeleGraph2SemConsistencyRelationImpl <em>Adele Graph2 Sem Consistency Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.gmm.osategmm.impl.AdeleGraph2SemConsistencyRelationImpl
	 * @see fr.labsticc.adosate.gmm.osategmm.impl.OsategmmPackageImpl#getAdeleGraph2SemConsistencyRelation()
	 * @generated
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__NAME = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__DESCRIPTION = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__DISPLAY_NAME = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__ID = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__ID;

	/**
	 * The feature id for the '<em><b>Connected Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__CONNECTED_MODELS = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__CONNECTED_MODELS;

	/**
	 * The feature id for the '<em><b>Chained Relations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__CHAINED_RELATIONS = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__CHAINED_RELATIONS;

	/**
	 * The feature id for the '<em><b>Owned Relation Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__OWNED_RELATION_POLICY = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__OWNED_RELATION_POLICY;

	/**
	 * The feature id for the '<em><b>Intention</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__INTENTION = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__INTENTION;

	/**
	 * The feature id for the '<em><b>Owned Coverage Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__OWNED_COVERAGE_POLICY = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__OWNED_COVERAGE_POLICY;

	/**
	 * The feature id for the '<em><b>Enablement Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__ENABLEMENT_TYPE = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__ENABLEMENT_TYPE;

	/**
	 * The feature id for the '<em><b>Deleting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__DELETING = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING;

	/**
	 * The feature id for the '<em><b>Validity Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__VALIDITY_STATUS = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__VALIDITY_STATUS;

	/**
	 * The feature id for the '<em><b>Meta Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__META_MODELS = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__META_MODELS;

	/**
	 * The feature id for the '<em><b>Left Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__LEFT_META_MODEL = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__LEFT_META_MODEL;

	/**
	 * The feature id for the '<em><b>Right Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__RIGHT_META_MODEL = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__RIGHT_META_MODEL;

	/**
	 * The feature id for the '<em><b>Deleting Left</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__DELETING_LEFT = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_LEFT;

	/**
	 * The feature id for the '<em><b>Deleting Right</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__DELETING_RIGHT = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION__DELETING_RIGHT;

	/**
	 * The feature id for the '<em><b>Diagram Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__DIAGRAM_META_MODEL = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Adele Graph2 Sem Consistency Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_GRAPH2_SEM_CONSISTENCY_RELATION_FEATURE_COUNT = MegamodelPackage.BINARY_META_MODEL_RELATED_CONSISTENCY_RELATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.gmm.osategmm.impl.AdeleOsateSynchronizationRelationImpl <em>Adele Osate Synchronization Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.gmm.osategmm.impl.AdeleOsateSynchronizationRelationImpl
	 * @see fr.labsticc.adosate.gmm.osategmm.impl.OsategmmPackageImpl#getAdeleOsateSynchronizationRelation()
	 * @generated
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__NAME = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__DESCRIPTION = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__DISPLAY_NAME = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__ID = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__ID;

	/**
	 * The feature id for the '<em><b>Connected Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__CONNECTED_MODELS = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__CONNECTED_MODELS;

	/**
	 * The feature id for the '<em><b>Chained Relations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__CHAINED_RELATIONS = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__CHAINED_RELATIONS;

	/**
	 * The feature id for the '<em><b>Owned Relation Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__OWNED_RELATION_POLICY = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__OWNED_RELATION_POLICY;

	/**
	 * The feature id for the '<em><b>Intention</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__INTENTION = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__INTENTION;

	/**
	 * The feature id for the '<em><b>Owned Coverage Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__OWNED_COVERAGE_POLICY = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__OWNED_COVERAGE_POLICY;

	/**
	 * The feature id for the '<em><b>Enablement Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__ENABLEMENT_TYPE = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__ENABLEMENT_TYPE;

	/**
	 * The feature id for the '<em><b>Deleting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__DELETING = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__DELETING;

	/**
	 * The feature id for the '<em><b>Validity Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__VALIDITY_STATUS = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__VALIDITY_STATUS;

	/**
	 * The feature id for the '<em><b>Meta Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__META_MODELS = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__META_MODELS;

	/**
	 * The feature id for the '<em><b>Left Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__LEFT_META_MODEL = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__LEFT_META_MODEL;

	/**
	 * The feature id for the '<em><b>Right Meta Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__RIGHT_META_MODEL = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__RIGHT_META_MODEL;

	/**
	 * The feature id for the '<em><b>Deleting Left</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__DELETING_LEFT = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__DELETING_LEFT;

	/**
	 * The feature id for the '<em><b>Deleting Right</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__DELETING_RIGHT = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__DELETING_RIGHT;

	/**
	 * The feature id for the '<em><b>Engine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__ENGINE = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__ENGINE;

	/**
	 * The feature id for the '<em><b>Rule Set Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION__RULE_SET_ID = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION__RULE_SET_ID;

	/**
	 * The number of structural features of the '<em>Adele Osate Synchronization Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADELE_OSATE_SYNCHRONIZATION_RELATION_FEATURE_COUNT = GmmmotePackage.MOTE_SYNCHRONIZATION_RELATION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.adosate.gmm.osategmm.impl.AdosateUriRelBinRelPolicyImpl <em>Adosate Uri Rel Bin Rel Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.adosate.gmm.osategmm.impl.AdosateUriRelBinRelPolicyImpl
	 * @see fr.labsticc.adosate.gmm.osategmm.impl.OsategmmPackageImpl#getAdosateUriRelBinRelPolicy()
	 * @generated
	 */
	int ADOSATE_URI_REL_BIN_REL_POLICY = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADOSATE_URI_REL_BIN_REL_POLICY__NAME = MegamodelPackage.URI_RELATED_BINARY_REL_POLICY__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADOSATE_URI_REL_BIN_REL_POLICY__DESCRIPTION = MegamodelPackage.URI_RELATED_BINARY_REL_POLICY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADOSATE_URI_REL_BIN_REL_POLICY__DISPLAY_NAME = MegamodelPackage.URI_RELATED_BINARY_REL_POLICY__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADOSATE_URI_REL_BIN_REL_POLICY__ID = MegamodelPackage.URI_RELATED_BINARY_REL_POLICY__ID;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADOSATE_URI_REL_BIN_REL_POLICY__RELATION = MegamodelPackage.URI_RELATED_BINARY_REL_POLICY__RELATION;

	/**
	 * The number of structural features of the '<em>Adosate Uri Rel Bin Rel Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADOSATE_URI_REL_BIN_REL_POLICY_FEATURE_COUNT = MegamodelPackage.URI_RELATED_BINARY_REL_POLICY_FEATURE_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.gmm.osategmm.AdeleOsateModelElementsCovPolicy <em>Adele Osate Model Elements Cov Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Adele Osate Model Elements Cov Policy</em>'.
	 * @see fr.labsticc.adosate.gmm.osategmm.AdeleOsateModelElementsCovPolicy
	 * @generated
	 */
	EClass getAdeleOsateModelElementsCovPolicy();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.gmm.osategmm.AdeleGraph2SemConsistencyRelation <em>Adele Graph2 Sem Consistency Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Adele Graph2 Sem Consistency Relation</em>'.
	 * @see fr.labsticc.adosate.gmm.osategmm.AdeleGraph2SemConsistencyRelation
	 * @generated
	 */
	EClass getAdeleGraph2SemConsistencyRelation();

	/**
	 * Returns the meta object for the reference '{@link fr.labsticc.adosate.gmm.osategmm.AdeleGraph2SemConsistencyRelation#getDiagramMetaModel <em>Diagram Meta Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Diagram Meta Model</em>'.
	 * @see fr.labsticc.adosate.gmm.osategmm.AdeleGraph2SemConsistencyRelation#getDiagramMetaModel()
	 * @see #getAdeleGraph2SemConsistencyRelation()
	 * @generated
	 */
	EReference getAdeleGraph2SemConsistencyRelation_DiagramMetaModel();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.gmm.osategmm.AdeleOsateSynchronizationRelation <em>Adele Osate Synchronization Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Adele Osate Synchronization Relation</em>'.
	 * @see fr.labsticc.adosate.gmm.osategmm.AdeleOsateSynchronizationRelation
	 * @generated
	 */
	EClass getAdeleOsateSynchronizationRelation();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.adosate.gmm.osategmm.AdosateUriRelBinRelPolicy <em>Adosate Uri Rel Bin Rel Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Adosate Uri Rel Bin Rel Policy</em>'.
	 * @see fr.labsticc.adosate.gmm.osategmm.AdosateUriRelBinRelPolicy
	 * @generated
	 */
	EClass getAdosateUriRelBinRelPolicy();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OsategmmFactory getOsategmmFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.labsticc.adosate.gmm.osategmm.impl.AdeleOsateModelElementsCovPolicyImpl <em>Adele Osate Model Elements Cov Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.adosate.gmm.osategmm.impl.AdeleOsateModelElementsCovPolicyImpl
		 * @see fr.labsticc.adosate.gmm.osategmm.impl.OsategmmPackageImpl#getAdeleOsateModelElementsCovPolicy()
		 * @generated
		 */
		EClass ADELE_OSATE_MODEL_ELEMENTS_COV_POLICY = eINSTANCE.getAdeleOsateModelElementsCovPolicy();
		/**
		 * The meta object literal for the '{@link fr.labsticc.adosate.gmm.osategmm.impl.AdeleGraph2SemConsistencyRelationImpl <em>Adele Graph2 Sem Consistency Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.adosate.gmm.osategmm.impl.AdeleGraph2SemConsistencyRelationImpl
		 * @see fr.labsticc.adosate.gmm.osategmm.impl.OsategmmPackageImpl#getAdeleGraph2SemConsistencyRelation()
		 * @generated
		 */
		EClass ADELE_GRAPH2_SEM_CONSISTENCY_RELATION = eINSTANCE.getAdeleGraph2SemConsistencyRelation();
		/**
		 * The meta object literal for the '<em><b>Diagram Meta Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__DIAGRAM_META_MODEL = eINSTANCE.getAdeleGraph2SemConsistencyRelation_DiagramMetaModel();
		/**
		 * The meta object literal for the '{@link fr.labsticc.adosate.gmm.osategmm.impl.AdeleOsateSynchronizationRelationImpl <em>Adele Osate Synchronization Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.adosate.gmm.osategmm.impl.AdeleOsateSynchronizationRelationImpl
		 * @see fr.labsticc.adosate.gmm.osategmm.impl.OsategmmPackageImpl#getAdeleOsateSynchronizationRelation()
		 * @generated
		 */
		EClass ADELE_OSATE_SYNCHRONIZATION_RELATION = eINSTANCE.getAdeleOsateSynchronizationRelation();
		/**
		 * The meta object literal for the '{@link fr.labsticc.adosate.gmm.osategmm.impl.AdosateUriRelBinRelPolicyImpl <em>Adosate Uri Rel Bin Rel Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.labsticc.adosate.gmm.osategmm.impl.AdosateUriRelBinRelPolicyImpl
		 * @see fr.labsticc.adosate.gmm.osategmm.impl.OsategmmPackageImpl#getAdosateUriRelBinRelPolicy()
		 * @generated
		 */
		EClass ADOSATE_URI_REL_BIN_REL_POLICY = eINSTANCE.getAdosateUriRelBinRelPolicy();

	}

} //OsategmmPackage
