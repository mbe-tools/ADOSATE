/**
 */
package fr.labsticc.adosate.gmm.osategmm.impl;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.resource.Resource;
import org.osate.aadl2.PropertySet;

import fr.labsticc.adosate.gmm.osategmm.AdosateUriRelBinRelPolicy;
import fr.labsticc.adosate.gmm.osategmm.OsategmmPackage;
import fr.labsticc.gmm.model.megamodel.impl.UriRelatedBinaryRelPolicyImpl;
import fr.openpeople.models.aadl.constants.AadlExtensionHelper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Adosate Uri Rel Bin Rel Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AdosateUriRelBinRelPolicyImpl extends UriRelatedBinaryRelPolicyImpl implements AdosateUriRelBinRelPolicy {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AdosateUriRelBinRelPolicyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OsategmmPackage.Literals.ADOSATE_URI_REL_BIN_REL_POLICY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public URI correspondingUri( final Resource p_resource ) {
		if ( 	AadlExtensionHelper.isAadlResource( p_resource ) &&
				( p_resource.getContents().isEmpty() || p_resource.getContents().get( 0 ) instanceof PropertySet ) ) {
			return null;
		}

		return correspondingUri( p_resource.getURI() );
	}

} //AdosateUriRelBinRelPolicyImpl
