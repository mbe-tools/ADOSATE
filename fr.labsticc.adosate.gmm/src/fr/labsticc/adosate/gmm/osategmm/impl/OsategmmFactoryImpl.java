/**
 */
package fr.labsticc.adosate.gmm.osategmm.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import fr.labsticc.adosate.gmm.osategmm.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OsategmmFactoryImpl extends EFactoryImpl implements
		OsategmmFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OsategmmFactory init() {
		try {
			OsategmmFactory theOsategmmFactory = (OsategmmFactory)EPackage.Registry.INSTANCE.getEFactory(OsategmmPackage.eNS_URI);
			if (theOsategmmFactory != null) {
				return theOsategmmFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OsategmmFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OsategmmFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OsategmmPackage.ADELE_OSATE_MODEL_ELEMENTS_COV_POLICY: return createAdeleOsateModelElementsCovPolicy();
			case OsategmmPackage.ADELE_GRAPH2_SEM_CONSISTENCY_RELATION: return createAdeleGraph2SemConsistencyRelation();
			case OsategmmPackage.ADELE_OSATE_SYNCHRONIZATION_RELATION: return createAdeleOsateSynchronizationRelation();
			case OsategmmPackage.ADOSATE_URI_REL_BIN_REL_POLICY: return createAdosateUriRelBinRelPolicy();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdeleOsateModelElementsCovPolicy createAdeleOsateModelElementsCovPolicy() {
		AdeleOsateModelElementsCovPolicyImpl adeleOsateModelElementsCovPolicy = new AdeleOsateModelElementsCovPolicyImpl();
		return adeleOsateModelElementsCovPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdeleGraph2SemConsistencyRelation createAdeleGraph2SemConsistencyRelation() {
		AdeleGraph2SemConsistencyRelationImpl adeleGraph2SemConsistencyRelation = new AdeleGraph2SemConsistencyRelationImpl();
		return adeleGraph2SemConsistencyRelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdeleOsateSynchronizationRelation createAdeleOsateSynchronizationRelation() {
		AdeleOsateSynchronizationRelationImpl adeleOsateSynchronizationRelation = new AdeleOsateSynchronizationRelationImpl();
		return adeleOsateSynchronizationRelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdosateUriRelBinRelPolicy createAdosateUriRelBinRelPolicy() {
		AdosateUriRelBinRelPolicyImpl adosateUriRelBinRelPolicy = new AdosateUriRelBinRelPolicyImpl();
		return adosateUriRelBinRelPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OsategmmPackage getOsategmmPackage() {
		return (OsategmmPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OsategmmPackage getPackage() {
		return OsategmmPackage.eINSTANCE;
	}

} //OsategmmFactoryImpl
