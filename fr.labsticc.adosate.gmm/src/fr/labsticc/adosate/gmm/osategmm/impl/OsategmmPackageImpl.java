/**
 */
package fr.labsticc.adosate.gmm.osategmm.impl;

import de.hpi.sam.mote.MotePackage;
import fr.labsticc.adosate.gmm.osategmm.AdeleGraph2SemConsistencyRelation;
import fr.labsticc.adosate.gmm.osategmm.AdeleOsateModelElementsCovPolicy;
import fr.labsticc.adosate.gmm.osategmm.AdeleOsateSynchronizationRelation;
import fr.labsticc.adosate.gmm.osategmm.AdosateUriRelBinRelPolicy;
import fr.labsticc.adosate.gmm.osategmm.OsategmmFactory;
import fr.labsticc.adosate.gmm.osategmm.OsategmmPackage;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.mote.model.gmmmote.GmmmotePackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.topcased.adele.model.KernelSpices.KernelSpicesPackage;
import org.topcased.adele.model.ObjectDescriptionModel.ObjectDescriptionModelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OsategmmPackageImpl extends EPackageImpl implements
		OsategmmPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass adeleOsateModelElementsCovPolicyEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass adeleGraph2SemConsistencyRelationEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass adeleOsateSynchronizationRelationEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass adosateUriRelBinRelPolicyEClass = null;
	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.labsticc.adosate.gmm.osategmm.OsategmmPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OsategmmPackageImpl() {
		super(eNS_URI, OsategmmFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link OsategmmPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OsategmmPackage init() {
		if (isInited) return (OsategmmPackage)EPackage.Registry.INSTANCE.getEPackage(OsategmmPackage.eNS_URI);

		// Obtain or create and register package
		OsategmmPackageImpl theOsategmmPackage = (OsategmmPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof OsategmmPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new OsategmmPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		KernelSpicesPackage.eINSTANCE.eClass();
		ObjectDescriptionModelPackage.eINSTANCE.eClass();
		GmmmotePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theOsategmmPackage.createPackageContents();

		// Initialize created meta-data
		theOsategmmPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOsategmmPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OsategmmPackage.eNS_URI, theOsategmmPackage);
		return theOsategmmPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAdeleOsateModelElementsCovPolicy() {
		return adeleOsateModelElementsCovPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAdeleGraph2SemConsistencyRelation() {
		return adeleGraph2SemConsistencyRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAdeleGraph2SemConsistencyRelation_DiagramMetaModel() {
		return (EReference)adeleGraph2SemConsistencyRelationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAdeleOsateSynchronizationRelation() {
		return adeleOsateSynchronizationRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAdosateUriRelBinRelPolicy() {
		return adosateUriRelBinRelPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OsategmmFactory getOsategmmFactory() {
		return (OsategmmFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		adeleOsateModelElementsCovPolicyEClass = createEClass(ADELE_OSATE_MODEL_ELEMENTS_COV_POLICY);

		adeleGraph2SemConsistencyRelationEClass = createEClass(ADELE_GRAPH2_SEM_CONSISTENCY_RELATION);
		createEReference(adeleGraph2SemConsistencyRelationEClass, ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__DIAGRAM_META_MODEL);

		adeleOsateSynchronizationRelationEClass = createEClass(ADELE_OSATE_SYNCHRONIZATION_RELATION);

		adosateUriRelBinRelPolicyEClass = createEClass(ADOSATE_URI_REL_BIN_REL_POLICY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MegamodelPackage theMegamodelPackage = (MegamodelPackage)EPackage.Registry.INSTANCE.getEPackage(MegamodelPackage.eNS_URI);
		MotePackage theMotePackage = (MotePackage)EPackage.Registry.INSTANCE.getEPackage(MotePackage.eNS_URI);
		GmmmotePackage theGmmmotePackage = (GmmmotePackage)EPackage.Registry.INSTANCE.getEPackage(GmmmotePackage.eNS_URI);
		KernelSpicesPackage theKernelSpicesPackage = (KernelSpicesPackage)EPackage.Registry.INSTANCE.getEPackage(KernelSpicesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		adeleOsateModelElementsCovPolicyEClass.getESuperTypes().add(theMegamodelPackage.getModelElementsCoveragePolicy());
		adeleOsateModelElementsCovPolicyEClass.getESuperTypes().add(theMotePackage.getMoteEngineCoveragePolicy());
		adeleGraph2SemConsistencyRelationEClass.getESuperTypes().add(theMegamodelPackage.getBinaryMetaModelRelatedConsistencyRelation());
		adeleOsateSynchronizationRelationEClass.getESuperTypes().add(theGmmmotePackage.getMoteSynchronizationRelation());
		adosateUriRelBinRelPolicyEClass.getESuperTypes().add(theMegamodelPackage.getUriRelatedBinaryRelPolicy());
		adosateUriRelBinRelPolicyEClass.getESuperTypes().add(theMotePackage.getMoteEngineRelationPolicy());

		// Initialize classes and features; add operations and parameters
		initEClass(adeleOsateModelElementsCovPolicyEClass, AdeleOsateModelElementsCovPolicy.class, "AdeleOsateModelElementsCovPolicy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(adeleGraph2SemConsistencyRelationEClass, AdeleGraph2SemConsistencyRelation.class, "AdeleGraph2SemConsistencyRelation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAdeleGraph2SemConsistencyRelation_DiagramMetaModel(), theMegamodelPackage.getMetaModel(), null, "diagramMetaModel", null, 1, 1, AdeleGraph2SemConsistencyRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(adeleOsateSynchronizationRelationEClass, AdeleOsateSynchronizationRelation.class, "AdeleOsateSynchronizationRelation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(adeleOsateSynchronizationRelationEClass, ecorePackage.getEObject(), "aadlElement", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theKernelSpicesPackage.getSKHierarchicalObject(), "adeleObject", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(adosateUriRelBinRelPolicyEClass, AdosateUriRelBinRelPolicy.class, "AdosateUriRelBinRelPolicy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //OsategmmPackageImpl
