/**
 */
package fr.labsticc.adosate.gmm.osategmm.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.Classifier;
import org.osate.aadl2.Connection;
import org.osate.aadl2.Feature;
import org.osate.aadl2.PackageSection;
import org.osate.aadl2.Subcomponent;
import org.topcased.adele.model.ADELE.util.ADELEModelUtils;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;

import fr.labsticc.adosate.gmm.osategmm.AdeleOsateModelElementsCovPolicy;
import fr.labsticc.adosate.gmm.osategmm.OsategmmPackage;
import fr.labsticc.gmm.model.megamodel.impl.ModelElementsCoveragePolicyImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Adele Osate Model Elements Cov Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AdeleOsateModelElementsCovPolicyImpl extends
		ModelElementsCoveragePolicyImpl implements
		AdeleOsateModelElementsCovPolicy {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AdeleOsateModelElementsCovPolicyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OsategmmPackage.Literals.ADELE_OSATE_MODEL_ELEMENTS_COV_POLICY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public boolean isCovered(EObject modelElement) {
		return isConsidered(modelElement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public boolean isConsidered(EObject modelElement) {

		// Duplicated Adele elements created only for diagramming are not considered
		if ( modelElement instanceof SKHierarchicalObject ) {
			return ADELEModelUtils.isOriginalElement( (SKHierarchicalObject) modelElement );
		}

		if (modelElement instanceof Classifier
				|| modelElement instanceof Subcomponent
				|| modelElement instanceof Feature
				|| modelElement instanceof Connection
				|| modelElement instanceof AadlPackage
				|| modelElement instanceof PackageSection ) {
			return true;
		}

		return false;
	}
} //AdeleOsateModelElementsCovPolicyImpl
