/**
 */
package fr.labsticc.adosate.gmm.osategmm.impl;

import java.util.Map;

import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.resource.SaveOptions;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.PropertySet;
import org.osate.aadl2.modelsupport.resources.OsateResourceUtil;
import org.osate.xtext.aadl2.ui.contentassist.Aadl2ProposalProvider;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;

import de.hpi.sam.mote.impl.TransformationException;
import fr.labsticc.adosate.gmm.osategmm.AdeleOsateSynchronizationRelation;
import fr.labsticc.adosate.gmm.osategmm.OsategmmPackage;
import fr.labsticc.framework.core.exception.FunctionalException;
import fr.labsticc.framework.core.exception.SystemException;
import fr.labsticc.gmm.model.megamodel.ExecutionContext;
import fr.labsticc.gmm.model.megamodel.Model;
import fr.labsticc.gmm.model.megamodel.ModelingEnvironment;
import fr.labsticc.gmm.model.megamodel.UriRelatedBinaryRelPolicy;
import fr.labsticc.gmm.mote.model.gmmmote.impl.MoteSynchronizationRelationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Adele Osate Synchronization Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AdeleOsateSynchronizationRelationImpl extends MoteSynchronizationRelationImpl implements AdeleOsateSynchronizationRelation {
	
    //private final IResourceChangeListener resourceChangeListener;

	// Ensure the xtext resource set is created before it is needed by this model interface. This is important when a
	// workspace is opened with a RDAL model file open and shown as default. The xtext resource set is only created when
	// needed (when an AADL file is opened for example). Instantiating this class will force starting the
	// org.osate.xtext.aadl2.ui plugin that will then create the resource set. Using the MyAadl2Activator class instead of
	// the Aadl2ProposalProvider class would make more sense but it is not exported by the plugin. Any class will start
	// the plugin anyway.
	@SuppressWarnings("unused")
	private static final Object dummyObject = Aadl2ProposalProvider.class;
	
	//private static final IResourceServiceProvider.Registry managerRegistry = IResourceServiceProvider.Registry.INSTANCE;

    /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AdeleOsateSynchronizationRelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OsategmmPackage.Literals.ADELE_OSATE_SYNCHRONIZATION_RELATION;
	}

	private Resource getResource( final URI p_resUri ) {
		final ResourceSet resSet = getResourceSet();
		
		Resource res = resSet.getResource( p_resUri, false );
		
		if ( res != null ) {
			return res;
		}
		
		if ( resSet.getURIConverter().exists( p_resUri, null ) ) {
			return resSet.getResource( p_resUri, true );
		}
		
		return null;
	}
//
//    private IResourceDescription.Manager getResourceDescriptionManager( final URI p_uri ) {
//        final IResourceServiceProvider resourceServiceProvider = managerRegistry.getResourceServiceProvider( p_uri );
//       
//        if ( resourceServiceProvider == null ) {
//            return null;
//        }
//        
//        return resourceServiceProvider.getResourceDescriptionManager();
//    }
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EObject aadlElement(	final SKHierarchicalObject p_adeleObject ) {
		final Resource extRes = p_adeleObject.eResource();
		
		if ( extRes != null ) {
			final URI extResUri = extRes.getURI();
			final Resource adeleResFromCache = getResource( extResUri ); 
			
			if ( adeleResFromCache != null ) {
				final EObject adeleObjFromCache = adeleResFromCache.getEObject( EcoreUtil.getURI( p_adeleObject ).fragment() );
				final URI corrUri = ( (UriRelatedBinaryRelPolicy) getOwnedRelationPolicy() ).correspondingUri( extRes.getURI() );
				final Resource aadlResFromCache = getResource( corrUri );

				if ( adeleObjFromCache != null && aadlResFromCache != null ) {
					try {
						final String elemName = p_adeleObject.getName();
						
						for ( final EObject aadlObj : getEngine().correspondingTargetElements( adeleObjFromCache, aadlResFromCache, getRuleSetId() ) ) {
							if ( aadlObj instanceof NamedElement && elemName.equals( ( (NamedElement) aadlObj ).getName() ) ) {
								return aadlObj;
							}
						}
					}
					catch( final TransformationException p_ex ) {
						throw new RuntimeException( p_ex ); 
					}
				}
			}
		}
		
		return null;
	}

	@Override
	protected ResourceSet createResourceSet() {
		return new ResourceSetImpl();//OsateResourceUtil.createXtextResourceSet();
	}
	
	@Override
	public boolean concerns( final EObject p_modelElement ) {
		if ( p_modelElement instanceof PropertySet ) {
			return false;
		}
		
		return super.concerns( p_modelElement );
	}
 
	@Override
	protected Map<?, ?> getSaveOptions() {
		return SaveOptions.newBuilder().noValidation().getOptions().toOptionsMap();
	}
	static class IndexingAdapter extends AdapterImpl {
		@Override
		public boolean isAdapterForType( final Object p_type ) {
			return IndexingAdapter.class.equals( p_type );
		}
	}
	
	@Override
	public EList<Model> establishValidity( 	final ModelingEnvironment p_modelingEnvironment,
											final ExecutionContext p_execContext )
	throws SystemException, FunctionalException {
		//ResourceSet resSet = null;

//		try {
			final EList<Model> models = super.establishValidity( p_modelingEnvironment, p_execContext );
			
			return models;
//		}
//		catch( final RuntimeException p_ex ) {
//			final Throwable cause = p_ex.getCause();
//			
//			if ( cause instanceof UnresolvedRefTransException ) {
//				throw new RuntimeException( "External reference " + ( (UnresolvedRefTransException) cause ).getRefObjectUri() + " is unresolved. Ensure that its AADL model has no errors or refresh its resource.", 
//											cause );
//			}
//			
//			throw p_ex;
//		}
//		finally {
//			if ( resSet != null ) {
//				resSet.getLoadOptions().remove( ResourceDescriptionsProvider.NAMED_BUILDER_SCOPE );
//			}
//		}
	}
	
	@Override
	public boolean isEnabled(	final Model p_model,
								final ExecutionContext p_execContext ) {
		switch ( getEnablementType() ) {
			case ALWAYS:
			case MANUAL_ONLY:
			case MANUAL_ONLY_NO_ERROR:
			case NEVER:
				return super.isEnabled( p_model, p_execContext );
			case NO_ERROR:
				
				// We want to check consistency at reading time in case the error has been fixed
				//if ( OperationType.READ == p_execContext.getSourceModelOperationType() ) {
					return !hasErrorsOtherThanGmm( p_model );
				//}
				
				//return super.isEnabled( p_model, p_execContext );
			default:
				throw new IllegalArgumentException( "Unknown enablement type: " + getEnablementType() + "." );
		}
	}
} //AdeleOsateSynchronizationRelationImpl
