/**
 */
package fr.labsticc.adosate.gmm.osategmm.impl;

import java.io.IOException;
import java.util.Date;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.swt.widgets.Display;
import org.topcased.adele.common.utils.AdeleCommonUtils;
import org.topcased.adele.editor.wizards.AdeleDiagramInitializer;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;

import fr.labsticc.adosate.gmm.osategmm.AdeleGraph2SemConsistencyRelation;
import fr.labsticc.adosate.gmm.osategmm.OsategmmPackage;
import fr.labsticc.framework.core.util.ObjectReturnRunnable;
import fr.labsticc.framework.emf.core.util.EMFUtil;
import fr.labsticc.gmm.model.megamodel.ExecutionContext;
import fr.labsticc.gmm.model.megamodel.MegamodelFactory;
import fr.labsticc.gmm.model.megamodel.MetaModel;
import fr.labsticc.gmm.model.megamodel.Model;
import fr.labsticc.gmm.model.megamodel.ModelingEnvironment;
import fr.labsticc.gmm.model.megamodel.impl.BinaryMetaModelRelatedConsistencyRelationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Adele Osate Graphical2 Semantic Sync Relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.labsticc.adosate.gmm.osategmm.impl.AdeleGraph2SemConsistencyRelationImpl#getDiagramMetaModel <em>Diagram Meta Model</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AdeleGraph2SemConsistencyRelationImpl extends BinaryMetaModelRelatedConsistencyRelationImpl implements
		AdeleGraph2SemConsistencyRelation {
    
	/**
	 * The cached value of the '{@link #getDiagramMetaModel() <em>Diagram Meta Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiagramMetaModel()
	 * @generated
	 * @ordered
	 */
	protected MetaModel diagramMetaModel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AdeleGraph2SemConsistencyRelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OsategmmPackage.Literals.ADELE_GRAPH2_SEM_CONSISTENCY_RELATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModel getDiagramMetaModel() {
		if (diagramMetaModel != null && diagramMetaModel.eIsProxy()) {
			InternalEObject oldDiagramMetaModel = (InternalEObject)diagramMetaModel;
			diagramMetaModel = (MetaModel)eResolveProxy(oldDiagramMetaModel);
			if (diagramMetaModel != oldDiagramMetaModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OsategmmPackage.ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__DIAGRAM_META_MODEL, oldDiagramMetaModel, diagramMetaModel));
			}
		}
		return diagramMetaModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModel basicGetDiagramMetaModel() {
		return diagramMetaModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiagramMetaModel(MetaModel newDiagramMetaModel) {
		MetaModel oldDiagramMetaModel = diagramMetaModel;
		diagramMetaModel = newDiagramMetaModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OsategmmPackage.ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__DIAGRAM_META_MODEL, oldDiagramMetaModel, diagramMetaModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OsategmmPackage.ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__DIAGRAM_META_MODEL:
				if (resolve) return getDiagramMetaModel();
				return basicGetDiagramMetaModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OsategmmPackage.ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__DIAGRAM_META_MODEL:
				setDiagramMetaModel((MetaModel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OsategmmPackage.ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__DIAGRAM_META_MODEL:
				setDiagramMetaModel((MetaModel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OsategmmPackage.ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__DIAGRAM_META_MODEL:
				return diagramMetaModel != null;
		}
		return super.eIsSet(featureID);
	}

	private Model targetModel( final ModelingEnvironment p_modelingEnvironment ) {
		final Model sourceModel = p_modelingEnvironment.getSourceModel();

		for ( final Model model : p_modelingEnvironment.getOwnedModels() ) {
			if ( model != sourceModel ) {
				return model;
			}
		}

		throw new IllegalArgumentException("Should not happen.");
	}
	
	private boolean isDiagram( final Model p_model ) {
		final EObject rootElem = rootModelElement( p_model );
		
		if ( rootElem == null ) {
			return getDiagramMetaModel().getFileExtensions().containsAll( p_model.getFileExtensions() );
		}

		// Assumes right meta-model is diagram.
		return getDiagramMetaModel().declares( rootElem );
	}
//
//	@Override
//	public EList<ConsistencyReport> checkConsistency( 	final ModelingEnvironment p_modelingEnvironment,
//														final ExecutionContext p_execContext ) {
//		return new BasicEList<ConsistencyReport>();
//	}

	private EObject rootModelElement( final Model p_model ) {
		if ( !p_model.getResources().isEmpty() ) {
			final Resource semanticModelRes = p_model.getResources().get( 0 );
			
			if ( !semanticModelRes.getContents().isEmpty() ) {
				return semanticModelRes.getContents().get( 0 );
			}
		}
		
		return null;
	}

	@Override
	public EList<Model> establishValidity( 	final ModelingEnvironment p_modelingEnvironment,
											final ExecutionContext p_execContext ) {
		final EList<Model> updatedModels = new BasicEList<Model>();
		final Model sourceModel = p_modelingEnvironment.getSourceModel();
		final Model targetModel = targetModel( p_modelingEnvironment );

		if ( isDiagram( sourceModel ) && isDeleting() ) {
			if ( !sourceModel.getResources().isEmpty() ) {
				final IResource ideRes = EMFUtil.convertToIDEResource( sourceModel.getResources().get( 0 ) );
				if ( !ideRes.exists() ) {
					for ( final Resource res : targetModel.getResources() ) {
						res.getContents().clear();
					}
				}
			}

			// Important: Return an updated model so that the chained relations are called to create the traceability model between Adele and OSATE.
			updatedModels.add( targetModel );
		}
		else {
			if ( rootModelElement( targetModel ) == null ) {
				final SKHierarchicalObject rootSemElement = (SKHierarchicalObject) rootModelElement( sourceModel ); 
				
				if ( rootSemElement != null ) {
					final AdeleDiagramInitializer initializer = new AdeleDiagramInitializer();
					final ObjectReturnRunnable runnable = new ObjectReturnRunnable() {
					
						@Override
						public void run() {
							try {
								result = initializer.createDiagramIfDoesNotExists( rootSemElement, true, new NullProgressMonitor() );
							}
							catch ( final IOException p_ex ) {
								throw new RuntimeException( p_ex );
							}
						}
					};
					
					System.out.println( new Date() + ": Creating diagram for " + rootSemElement.eResource().getURI() + "." );
					Display.getDefault().syncExec( runnable );
					
					// Note. Unfortunately, the save cannot be performed by the GMM manager because the diagram initializer cannot be run 
					// synchronously (there seems to be a dead lock that occurs).
					if ( runnable.result != null ) {
						final Model updModel = MegamodelFactory.eINSTANCE.createModel();
						final Resource diagRes = (Resource) runnable.result; 
						updModel.getResources().add( diagRes );
						updModel.setName( diagRes.getURI().lastSegment() );
						updatedModels.add( updModel );
					}
				}
			}
		}
		
		return updatedModels;
	}
	
	@Override
	public String getDisplayName( final EList<URI> p_uris ) {
		boolean allDiagram = true;
		
		for ( final URI uri : p_uris ) {
			if ( AdeleCommonUtils.isAdeleResource(  uri ) ) {
				allDiagram = false;
			}
		}
		
		if ( allDiagram ) {
			return null;
		}
		
		return "Create Adele Diagram(s)";
	}
} //AdeleOsateGraphical2SemanticSyncRelationImpl
