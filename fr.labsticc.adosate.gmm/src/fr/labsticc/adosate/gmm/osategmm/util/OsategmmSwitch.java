/**
 */
package fr.labsticc.adosate.gmm.osategmm.util;

import de.hpi.sam.mote.MoteEngineCoveragePolicy;
import de.hpi.sam.mote.MoteEngineRelationPolicy;
import fr.labsticc.adosate.gmm.osategmm.*;
import fr.labsticc.gmm.model.megamodel.BinaryMetaModeRelatedlRelation;
import fr.labsticc.gmm.model.megamodel.BinaryMetaModelRelatedConsistencyRelation;
import fr.labsticc.gmm.model.megamodel.BinaryRelationPolicy;
import fr.labsticc.gmm.model.megamodel.FactualRelation;
import fr.labsticc.gmm.model.megamodel.MetaModelRelatedRelation;
import fr.labsticc.gmm.model.megamodel.ModelElementsCoveragePolicy;
import fr.labsticc.gmm.model.megamodel.NamedElement;
import fr.labsticc.gmm.model.megamodel.ObligationRelation;
import fr.labsticc.gmm.model.megamodel.Relation;
import fr.labsticc.gmm.model.megamodel.RelationPolicy;
import fr.labsticc.gmm.model.megamodel.UriRelatedBinaryRelPolicy;
import fr.labsticc.gmm.mote.model.gmmmote.MoteSynchronizationRelation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.labsticc.adosate.gmm.osategmm.OsategmmPackage
 * @generated
 */
public class OsategmmSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OsategmmPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OsategmmSwitch() {
		if (modelPackage == null) {
			modelPackage = OsategmmPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case OsategmmPackage.ADELE_OSATE_MODEL_ELEMENTS_COV_POLICY: {
				AdeleOsateModelElementsCovPolicy adeleOsateModelElementsCovPolicy = (AdeleOsateModelElementsCovPolicy)theEObject;
				T result = caseAdeleOsateModelElementsCovPolicy(adeleOsateModelElementsCovPolicy);
				if (result == null) result = caseModelElementsCoveragePolicy(adeleOsateModelElementsCovPolicy);
				if (result == null) result = caseMoteEngineCoveragePolicy(adeleOsateModelElementsCovPolicy);
				if (result == null) result = caseNamedElement(adeleOsateModelElementsCovPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OsategmmPackage.ADELE_GRAPH2_SEM_CONSISTENCY_RELATION: {
				AdeleGraph2SemConsistencyRelation adeleGraph2SemConsistencyRelation = (AdeleGraph2SemConsistencyRelation)theEObject;
				T result = caseAdeleGraph2SemConsistencyRelation(adeleGraph2SemConsistencyRelation);
				if (result == null) result = caseBinaryMetaModelRelatedConsistencyRelation(adeleGraph2SemConsistencyRelation);
				if (result == null) result = caseObligationRelation(adeleGraph2SemConsistencyRelation);
				if (result == null) result = caseBinaryMetaModeRelatedlRelation(adeleGraph2SemConsistencyRelation);
				if (result == null) result = caseMetaModelRelatedRelation(adeleGraph2SemConsistencyRelation);
				if (result == null) result = caseNamedElement(adeleGraph2SemConsistencyRelation);
				if (result == null) result = caseFactualRelation(adeleGraph2SemConsistencyRelation);
				if (result == null) result = caseRelation(adeleGraph2SemConsistencyRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OsategmmPackage.ADELE_OSATE_SYNCHRONIZATION_RELATION: {
				AdeleOsateSynchronizationRelation adeleOsateSynchronizationRelation = (AdeleOsateSynchronizationRelation)theEObject;
				T result = caseAdeleOsateSynchronizationRelation(adeleOsateSynchronizationRelation);
				if (result == null) result = caseMoteSynchronizationRelation(adeleOsateSynchronizationRelation);
				if (result == null) result = caseBinaryMetaModelRelatedConsistencyRelation(adeleOsateSynchronizationRelation);
				if (result == null) result = caseObligationRelation(adeleOsateSynchronizationRelation);
				if (result == null) result = caseBinaryMetaModeRelatedlRelation(adeleOsateSynchronizationRelation);
				if (result == null) result = caseMetaModelRelatedRelation(adeleOsateSynchronizationRelation);
				if (result == null) result = caseNamedElement(adeleOsateSynchronizationRelation);
				if (result == null) result = caseFactualRelation(adeleOsateSynchronizationRelation);
				if (result == null) result = caseRelation(adeleOsateSynchronizationRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OsategmmPackage.ADOSATE_URI_REL_BIN_REL_POLICY: {
				AdosateUriRelBinRelPolicy adosateUriRelBinRelPolicy = (AdosateUriRelBinRelPolicy)theEObject;
				T result = caseAdosateUriRelBinRelPolicy(adosateUriRelBinRelPolicy);
				if (result == null) result = caseUriRelatedBinaryRelPolicy(adosateUriRelBinRelPolicy);
				if (result == null) result = caseMoteEngineRelationPolicy(adosateUriRelBinRelPolicy);
				if (result == null) result = caseBinaryRelationPolicy(adosateUriRelBinRelPolicy);
				if (result == null) result = caseRelationPolicy(adosateUriRelBinRelPolicy);
				if (result == null) result = caseNamedElement(adosateUriRelBinRelPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Adele Osate Model Elements Cov Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Adele Osate Model Elements Cov Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAdeleOsateModelElementsCovPolicy(
			AdeleOsateModelElementsCovPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Adele Graph2 Sem Consistency Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Adele Graph2 Sem Consistency Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAdeleGraph2SemConsistencyRelation(AdeleGraph2SemConsistencyRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Adele Osate Synchronization Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Adele Osate Synchronization Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAdeleOsateSynchronizationRelation(AdeleOsateSynchronizationRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Adosate Uri Rel Bin Rel Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Adosate Uri Rel Bin Rel Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAdosateUriRelBinRelPolicy(AdosateUriRelBinRelPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Elements Coverage Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Elements Coverage Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelElementsCoveragePolicy(ModelElementsCoveragePolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Engine Coverage Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Engine Coverage Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMoteEngineCoveragePolicy(MoteEngineCoveragePolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRelation(Relation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Factual Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Factual Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFactualRelation(FactualRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Meta Model Related Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Meta Model Related Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMetaModelRelatedRelation(MetaModelRelatedRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Meta Mode Relatedl Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Meta Mode Relatedl Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryMetaModeRelatedlRelation(BinaryMetaModeRelatedlRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Obligation Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Obligation Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObligationRelation(ObligationRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Meta Model Related Consistency Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Meta Model Related Consistency Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryMetaModelRelatedConsistencyRelation(BinaryMetaModelRelatedConsistencyRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mote Synchronization Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mote Synchronization Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMoteSynchronizationRelation(MoteSynchronizationRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Relation Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relation Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRelationPolicy(RelationPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Relation Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Relation Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryRelationPolicy(BinaryRelationPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uri Related Binary Rel Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uri Related Binary Rel Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUriRelatedBinaryRelPolicy(UriRelatedBinaryRelPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Engine Relation Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Engine Relation Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMoteEngineRelationPolicy(MoteEngineRelationPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //OsategmmSwitch
