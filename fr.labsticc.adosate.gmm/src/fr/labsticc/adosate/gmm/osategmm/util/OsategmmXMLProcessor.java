/**
 */
package fr.labsticc.adosate.gmm.osategmm.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

import fr.labsticc.adosate.gmm.osategmm.OsategmmPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class OsategmmXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OsategmmXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		OsategmmPackage.eINSTANCE.eClass();
	}

	/**
	 * Register for "*" and "xml" file extensions the OsategmmResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new OsategmmResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new OsategmmResourceFactoryImpl());
		}
		return registrations;
	}

} //OsategmmXMLProcessor
