/**
 */
package fr.labsticc.adosate.gmm.osategmm.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see fr.labsticc.adosate.gmm.osategmm.util.OsategmmResourceFactoryImpl
 * @generated
 */
public class OsategmmResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public OsategmmResourceImpl(URI uri) {
		super(uri);
	}

} //OsategmmResourceImpl
