/**
 * <copyright>
 * </copyright>
 *
 * $Id: BehaviorAnnexImpl.java,v 1.2 2012-03-16 08:07:02 aschach Exp $
 */
package org.topcased.adele.model.ba_components.impl;

import org.eclipse.emf.ecore.EClass;
import org.topcased.adele.model.ba_components.Ba_componentsPackage;
import org.topcased.adele.model.ba_components.BehaviorAnnex;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Behavior Annex</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class BehaviorAnnexImpl extends BAComponentImpl implements BehaviorAnnex {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BehaviorAnnexImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ba_componentsPackage.Literals.BEHAVIOR_ANNEX;
	}

} //BehaviorAnnexImpl
