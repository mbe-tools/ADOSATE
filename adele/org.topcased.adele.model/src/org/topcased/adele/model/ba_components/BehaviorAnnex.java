/**
 * <copyright>
 * </copyright>
 *
 * $Id: BehaviorAnnex.java,v 1.2 2012-03-16 08:07:55 aschach Exp $
 */
package org.topcased.adele.model.ba_components;



/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Behavior Annex</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.adele.model.ba_components.Ba_componentsPackage#getBehaviorAnnex()
 * @model
 * @generated
 */
public interface BehaviorAnnex extends BAComponent {
} // BehaviorAnnex
