/**
 * 
 */
package org.topcased.adele.model.errors;

/**
 * @author Arnaud
 *
 */
public class UncompatibleRefinesReference extends Error {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4234415151436502039L;

	/**
	 * 
	 */
	public UncompatibleRefinesReference() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public UncompatibleRefinesReference(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public UncompatibleRefinesReference(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UncompatibleRefinesReference(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
