/**
 * <copyright>
 * </copyright>
 *
 * $Id: BARelation.java,v 1.1 2012-03-01 09:19:10 aschach Exp $
 */
package org.topcased.adele.model.ba_relations;

import org.topcased.adele.model.KernelSpices.SKRelation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BA Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.adele.model.ba_relations.Ba_relationsPackage#getBARelation()
 * @model abstract="true"
 * @generated
 */
public interface BARelation extends SKRelation {
} // BARelation
