/**
 * <copyright>
 * </copyright>
 *
 * $Id: BATransition.java,v 1.3 2012-04-30 12:07:36 aschach Exp $
 */
package org.topcased.adele.model.ba_relations;




/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BA Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.adele.model.ba_relations.Ba_relationsPackage#getBATransition()
 * @model
 * @generated
 */
public interface BATransition extends BARelation {
	//String aadlSerialization(String packageName,String annexName, ResourceSet res, SKODSFactory odStructure);
} // BATransition
