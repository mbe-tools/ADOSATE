/**
 * <copyright>
 * </copyright>
 *
 * $Id: BATransitionCondition.java,v 1.1 2012-03-01 09:19:11 aschach Exp $
 */
package org.topcased.adele.model.ba_relations;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BA Transition Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.adele.model.ba_relations.Ba_relationsPackage#getBATransitionCondition()
 * @model
 * @generated
 */
public interface BATransitionCondition extends BARelation {
} // BATransitionCondition
