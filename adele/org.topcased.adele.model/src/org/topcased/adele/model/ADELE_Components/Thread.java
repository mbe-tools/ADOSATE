/**
 * Copyright (c) 2009 Ellidiss Technologies
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	Arnaud Schach, arnaud.schach.com
 */
package org.topcased.adele.model.ADELE_Components;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ADELE Thread</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage#getThread()
 * @model
 * @generated
 */
public interface Thread extends Component {
} // ADELE_Thread
