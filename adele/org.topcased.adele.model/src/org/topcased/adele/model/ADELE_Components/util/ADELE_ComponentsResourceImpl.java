/**
 */
package org.topcased.adele.model.ADELE_Components.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsResourceFactoryImpl
 * @generated
 */
public class ADELE_ComponentsResourceImpl extends XMIResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public ADELE_ComponentsResourceImpl(URI uri) {
		super(uri);
	}

} //ADELE_ComponentsResourceImpl
