/**
 * <copyright>
 * </copyright>
 *
 * $Id: BAFeature.java,v 1.1 2012-03-01 09:19:14 aschach Exp $
 */
package org.topcased.adele.model.ba_features;

import org.topcased.adele.model.KernelSpices.SKFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BA Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.adele.model.ba_features.Ba_featuresPackage#getBAFeature()
 * @model abstract="true"
 * @generated
 */
public interface BAFeature extends SKFeature {
} // BAFeature
