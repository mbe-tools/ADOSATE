/**
 */
package org.topcased.adele.model.ADELE_Relations;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Directed Feature Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.adele.model.ADELE_Relations.ADELE_RelationsPackage#getDirectedFeatureConnection()
 * @model abstract="true"
 * @generated
 */
public interface DirectedFeatureConnection extends Relation {
} // DirectedFeatureConnection
