/**
 */
package org.topcased.adele.model.ADELE_Relations;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Access Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.adele.model.ADELE_Relations.ADELE_RelationsPackage#getAccessConnection()
 * @model abstract="true"
 * @generated
 */
public interface AccessConnection extends Relation {
} // AccessConnection
