/**
 * <copyright>
 * </copyright>
 *
 * $Id: ADELE_FeatureGroupConnection.java,v 1.1 2012-04-30 12:04:02 aschach Exp $
 */
package org.topcased.adele.model.ADELE_Relations;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ADELE Feature Group Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.adele.model.ADELE_Relations.ADELE_RelationsPackage#getFeatureGroupConnection()
 * @model
 * @generated
 */
public interface FeatureGroupConnection extends DirectedFeatureConnection {
} // ADELE_FeatureGroupConnection
