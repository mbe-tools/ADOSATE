package org.topcased.adele.model.ObjectDescriptionModel.impl;

public class SKODSFactoryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6675778153260392755L;

	public SKODSFactoryException() {
		// TODO Auto-generated constructor stub
	}

	public SKODSFactoryException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public SKODSFactoryException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public SKODSFactoryException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

}
