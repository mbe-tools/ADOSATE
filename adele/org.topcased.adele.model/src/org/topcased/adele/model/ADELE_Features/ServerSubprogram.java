/**
 * Copyright (c) 2009 Ellidiss Technologies
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	Arnaud Schach, arnaud.schach.com
 */
package org.topcased.adele.model.ADELE_Features;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ADELE Server Subprogram</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage#getADELE_ServerSubprogram()
 * @model
 * @generated
 */
public interface ServerSubprogram extends Feature {
} // ADELE_ServerSubprogram
