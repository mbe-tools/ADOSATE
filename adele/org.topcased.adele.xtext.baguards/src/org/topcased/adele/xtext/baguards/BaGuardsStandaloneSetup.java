
package org.topcased.adele.xtext.baguards;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class BaGuardsStandaloneSetup extends BaGuardsStandaloneSetupGenerated{

	public static void doSetup() {
		new BaGuardsStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

