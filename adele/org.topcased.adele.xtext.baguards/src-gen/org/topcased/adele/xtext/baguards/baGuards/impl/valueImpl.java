/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.value;
import org.topcased.adele.xtext.baguards.baGuards.value_constant;
import org.topcased.adele.xtext.baguards.baGuards.value_variable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.valueImpl#getVname1 <em>Vname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.valueImpl#getVname2 <em>Vname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class valueImpl extends MinimalEObjectImpl.Container implements value
{
  /**
   * The cached value of the '{@link #getVname1() <em>Vname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVname1()
   * @generated
   * @ordered
   */
  protected value_variable vname1;

  /**
   * The cached value of the '{@link #getVname2() <em>Vname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVname2()
   * @generated
   * @ordered
   */
  protected value_constant vname2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected valueImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.VALUE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value_variable getVname1()
  {
    return vname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetVname1(value_variable newVname1, NotificationChain msgs)
  {
    value_variable oldVname1 = vname1;
    vname1 = newVname1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.VALUE__VNAME1, oldVname1, newVname1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVname1(value_variable newVname1)
  {
    if (newVname1 != vname1)
    {
      NotificationChain msgs = null;
      if (vname1 != null)
        msgs = ((InternalEObject)vname1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.VALUE__VNAME1, null, msgs);
      if (newVname1 != null)
        msgs = ((InternalEObject)newVname1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.VALUE__VNAME1, null, msgs);
      msgs = basicSetVname1(newVname1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.VALUE__VNAME1, newVname1, newVname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value_constant getVname2()
  {
    return vname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetVname2(value_constant newVname2, NotificationChain msgs)
  {
    value_constant oldVname2 = vname2;
    vname2 = newVname2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.VALUE__VNAME2, oldVname2, newVname2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVname2(value_constant newVname2)
  {
    if (newVname2 != vname2)
    {
      NotificationChain msgs = null;
      if (vname2 != null)
        msgs = ((InternalEObject)vname2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.VALUE__VNAME2, null, msgs);
      if (newVname2 != null)
        msgs = ((InternalEObject)newVname2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.VALUE__VNAME2, null, msgs);
      msgs = basicSetVname2(newVname2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.VALUE__VNAME2, newVname2, newVname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.VALUE__VNAME1:
        return basicSetVname1(null, msgs);
      case BaGuardsPackage.VALUE__VNAME2:
        return basicSetVname2(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.VALUE__VNAME1:
        return getVname1();
      case BaGuardsPackage.VALUE__VNAME2:
        return getVname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.VALUE__VNAME1:
        setVname1((value_variable)newValue);
        return;
      case BaGuardsPackage.VALUE__VNAME2:
        setVname2((value_constant)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.VALUE__VNAME1:
        setVname1((value_variable)null);
        return;
      case BaGuardsPackage.VALUE__VNAME2:
        setVname2((value_constant)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.VALUE__VNAME1:
        return vname1 != null;
      case BaGuardsPackage.VALUE__VNAME2:
        return vname2 != null;
    }
    return super.eIsSet(featureID);
  }

} //valueImpl
