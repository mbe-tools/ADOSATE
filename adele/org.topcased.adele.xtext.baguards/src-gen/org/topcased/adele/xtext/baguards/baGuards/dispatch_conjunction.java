/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dispatch conjunction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_conjunction#getDcname1 <em>Dcname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_conjunction#getDcname2 <em>Dcname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdispatch_conjunction()
 * @model
 * @generated
 */
public interface dispatch_conjunction extends EObject
{
  /**
   * Returns the value of the '<em><b>Dcname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dcname1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dcname1</em>' containment reference.
   * @see #setDcname1(dispatch_trigger)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdispatch_conjunction_Dcname1()
   * @model containment="true"
   * @generated
   */
  dispatch_trigger getDcname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_conjunction#getDcname1 <em>Dcname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dcname1</em>' containment reference.
   * @see #getDcname1()
   * @generated
   */
  void setDcname1(dispatch_trigger value);

  /**
   * Returns the value of the '<em><b>Dcname2</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dcname2</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dcname2</em>' containment reference list.
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdispatch_conjunction_Dcname2()
   * @model containment="true"
   * @generated
   */
  EList<dispatch_trigger> getDcname2();

} // dispatch_conjunction
