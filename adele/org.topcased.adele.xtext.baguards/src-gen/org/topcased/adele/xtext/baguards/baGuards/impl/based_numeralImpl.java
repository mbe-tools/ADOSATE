/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.based_numeral;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>based numeral</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.based_numeralImpl#getBnname1 <em>Bnname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.based_numeralImpl#getBnname2 <em>Bnname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class based_numeralImpl extends MinimalEObjectImpl.Container implements based_numeral
{
  /**
   * The default value of the '{@link #getBnname1() <em>Bnname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBnname1()
   * @generated
   * @ordered
   */
  protected static final String BNNAME1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBnname1() <em>Bnname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBnname1()
   * @generated
   * @ordered
   */
  protected String bnname1 = BNNAME1_EDEFAULT;

  /**
   * The default value of the '{@link #getBnname2() <em>Bnname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBnname2()
   * @generated
   * @ordered
   */
  protected static final String BNNAME2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBnname2() <em>Bnname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBnname2()
   * @generated
   * @ordered
   */
  protected String bnname2 = BNNAME2_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected based_numeralImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.BASED_NUMERAL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBnname1()
  {
    return bnname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBnname1(String newBnname1)
  {
    String oldBnname1 = bnname1;
    bnname1 = newBnname1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.BASED_NUMERAL__BNNAME1, oldBnname1, bnname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBnname2()
  {
    return bnname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBnname2(String newBnname2)
  {
    String oldBnname2 = bnname2;
    bnname2 = newBnname2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.BASED_NUMERAL__BNNAME2, oldBnname2, bnname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.BASED_NUMERAL__BNNAME1:
        return getBnname1();
      case BaGuardsPackage.BASED_NUMERAL__BNNAME2:
        return getBnname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.BASED_NUMERAL__BNNAME1:
        setBnname1((String)newValue);
        return;
      case BaGuardsPackage.BASED_NUMERAL__BNNAME2:
        setBnname2((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.BASED_NUMERAL__BNNAME1:
        setBnname1(BNNAME1_EDEFAULT);
        return;
      case BaGuardsPackage.BASED_NUMERAL__BNNAME2:
        setBnname2(BNNAME2_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.BASED_NUMERAL__BNNAME1:
        return BNNAME1_EDEFAULT == null ? bnname1 != null : !BNNAME1_EDEFAULT.equals(bnname1);
      case BaGuardsPackage.BASED_NUMERAL__BNNAME2:
        return BNNAME2_EDEFAULT == null ? bnname2 != null : !BNNAME2_EDEFAULT.equals(bnname2);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (bnname1: ");
    result.append(bnname1);
    result.append(", bnname2: ");
    result.append(bnname2);
    result.append(')');
    return result.toString();
  }

} //based_numeralImpl
