/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dispatch trigger condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition#getDtcname1 <em>Dtcname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition#getDtcname4 <em>Dtcname4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition#getDtcname2 <em>Dtcname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdispatch_trigger_condition()
 * @model
 * @generated
 */
public interface dispatch_trigger_condition extends EObject
{
  /**
   * Returns the value of the '<em><b>Dtcname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dtcname1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dtcname1</em>' containment reference.
   * @see #setDtcname1(dispatch_trigger_logical_expression)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdispatch_trigger_condition_Dtcname1()
   * @model containment="true"
   * @generated
   */
  dispatch_trigger_logical_expression getDtcname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition#getDtcname1 <em>Dtcname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dtcname1</em>' containment reference.
   * @see #getDtcname1()
   * @generated
   */
  void setDtcname1(dispatch_trigger_logical_expression value);

  /**
   * Returns the value of the '<em><b>Dtcname4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dtcname4</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dtcname4</em>' attribute.
   * @see #setDtcname4(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdispatch_trigger_condition_Dtcname4()
   * @model
   * @generated
   */
  String getDtcname4();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition#getDtcname4 <em>Dtcname4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dtcname4</em>' attribute.
   * @see #getDtcname4()
   * @generated
   */
  void setDtcname4(String value);

  /**
   * Returns the value of the '<em><b>Dtcname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dtcname2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dtcname2</em>' containment reference.
   * @see #setDtcname2(completion_relative_timeout_condition_and_catch)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdispatch_trigger_condition_Dtcname2()
   * @model containment="true"
   * @generated
   */
  completion_relative_timeout_condition_and_catch getDtcname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition#getDtcname2 <em>Dtcname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dtcname2</em>' containment reference.
   * @see #getDtcname2()
   * @generated
   */
  void setDtcname2(completion_relative_timeout_condition_and_catch value);

} // dispatch_trigger_condition
