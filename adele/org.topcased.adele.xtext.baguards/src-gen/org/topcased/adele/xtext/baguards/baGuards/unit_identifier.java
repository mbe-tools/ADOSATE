/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>unit identifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname1 <em>Uiname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname2 <em>Uiname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname3 <em>Uiname3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname4 <em>Uiname4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname5 <em>Uiname5</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname6 <em>Uiname6</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname7 <em>Uiname7</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getunit_identifier()
 * @model
 * @generated
 */
public interface unit_identifier extends EObject
{
  /**
   * Returns the value of the '<em><b>Uiname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uiname1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uiname1</em>' attribute.
   * @see #setUiname1(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getunit_identifier_Uiname1()
   * @model
   * @generated
   */
  String getUiname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname1 <em>Uiname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uiname1</em>' attribute.
   * @see #getUiname1()
   * @generated
   */
  void setUiname1(String value);

  /**
   * Returns the value of the '<em><b>Uiname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uiname2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uiname2</em>' attribute.
   * @see #setUiname2(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getunit_identifier_Uiname2()
   * @model
   * @generated
   */
  String getUiname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname2 <em>Uiname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uiname2</em>' attribute.
   * @see #getUiname2()
   * @generated
   */
  void setUiname2(String value);

  /**
   * Returns the value of the '<em><b>Uiname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uiname3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uiname3</em>' attribute.
   * @see #setUiname3(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getunit_identifier_Uiname3()
   * @model
   * @generated
   */
  String getUiname3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname3 <em>Uiname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uiname3</em>' attribute.
   * @see #getUiname3()
   * @generated
   */
  void setUiname3(String value);

  /**
   * Returns the value of the '<em><b>Uiname4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uiname4</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uiname4</em>' attribute.
   * @see #setUiname4(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getunit_identifier_Uiname4()
   * @model
   * @generated
   */
  String getUiname4();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname4 <em>Uiname4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uiname4</em>' attribute.
   * @see #getUiname4()
   * @generated
   */
  void setUiname4(String value);

  /**
   * Returns the value of the '<em><b>Uiname5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uiname5</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uiname5</em>' attribute.
   * @see #setUiname5(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getunit_identifier_Uiname5()
   * @model
   * @generated
   */
  String getUiname5();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname5 <em>Uiname5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uiname5</em>' attribute.
   * @see #getUiname5()
   * @generated
   */
  void setUiname5(String value);

  /**
   * Returns the value of the '<em><b>Uiname6</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uiname6</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uiname6</em>' attribute.
   * @see #setUiname6(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getunit_identifier_Uiname6()
   * @model
   * @generated
   */
  String getUiname6();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname6 <em>Uiname6</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uiname6</em>' attribute.
   * @see #getUiname6()
   * @generated
   */
  void setUiname6(String value);

  /**
   * Returns the value of the '<em><b>Uiname7</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uiname7</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uiname7</em>' attribute.
   * @see #setUiname7(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getunit_identifier_Uiname7()
   * @model
   * @generated
   */
  String getUiname7();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname7 <em>Uiname7</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uiname7</em>' attribute.
   * @see #getUiname7()
   * @generated
   */
  void setUiname7(String value);

} // unit_identifier
