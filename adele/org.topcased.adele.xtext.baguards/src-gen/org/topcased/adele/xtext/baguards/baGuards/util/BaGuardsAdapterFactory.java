/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.topcased.adele.xtext.baguards.baGuards.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage
 * @generated
 */
public class BaGuardsAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static BaGuardsPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaGuardsAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = BaGuardsPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BaGuardsSwitch<Adapter> modelSwitch =
    new BaGuardsSwitch<Adapter>()
    {
      @Override
      public Adapter caseGuard(Guard object)
      {
        return createGuardAdapter();
      }
      @Override
      public Adapter caseexecute_condition(execute_condition object)
      {
        return createexecute_conditionAdapter();
      }
      @Override
      public Adapter casebehavior_action_block_timeout_catch(behavior_action_block_timeout_catch object)
      {
        return createbehavior_action_block_timeout_catchAdapter();
      }
      @Override
      public Adapter casevalue_expression(value_expression object)
      {
        return createvalue_expressionAdapter();
      }
      @Override
      public Adapter caserelation(relation object)
      {
        return createrelationAdapter();
      }
      @Override
      public Adapter casesimple_expression(simple_expression object)
      {
        return createsimple_expressionAdapter();
      }
      @Override
      public Adapter caseterm(term object)
      {
        return createtermAdapter();
      }
      @Override
      public Adapter casefactor(factor object)
      {
        return createfactorAdapter();
      }
      @Override
      public Adapter casevalue(value object)
      {
        return createvalueAdapter();
      }
      @Override
      public Adapter casevalue_variable(value_variable object)
      {
        return createvalue_variableAdapter();
      }
      @Override
      public Adapter casevalue_constant(value_constant object)
      {
        return createvalue_constantAdapter();
      }
      @Override
      public Adapter caselogical_operator(logical_operator object)
      {
        return createlogical_operatorAdapter();
      }
      @Override
      public Adapter caserelational_operator(relational_operator object)
      {
        return createrelational_operatorAdapter();
      }
      @Override
      public Adapter casebinary_adding_operator(binary_adding_operator object)
      {
        return createbinary_adding_operatorAdapter();
      }
      @Override
      public Adapter caseunary_adding_operator(unary_adding_operator object)
      {
        return createunary_adding_operatorAdapter();
      }
      @Override
      public Adapter casemultiplying_operator(multiplying_operator object)
      {
        return createmultiplying_operatorAdapter();
      }
      @Override
      public Adapter casebinary_numeric_operator(binary_numeric_operator object)
      {
        return createbinary_numeric_operatorAdapter();
      }
      @Override
      public Adapter caseunary_numeric_operator(unary_numeric_operator object)
      {
        return createunary_numeric_operatorAdapter();
      }
      @Override
      public Adapter caseunary_boolean_operator(unary_boolean_operator object)
      {
        return createunary_boolean_operatorAdapter();
      }
      @Override
      public Adapter caseboolean_literal(boolean_literal object)
      {
        return createboolean_literalAdapter();
      }
      @Override
      public Adapter caseinteger_value(integer_value object)
      {
        return createinteger_valueAdapter();
      }
      @Override
      public Adapter casebehavior_time(behavior_time object)
      {
        return createbehavior_timeAdapter();
      }
      @Override
      public Adapter caseunit_identifier(unit_identifier object)
      {
        return createunit_identifierAdapter();
      }
      @Override
      public Adapter casenumeric_literal(numeric_literal object)
      {
        return createnumeric_literalAdapter();
      }
      @Override
      public Adapter caseinteger_literal(integer_literal object)
      {
        return createinteger_literalAdapter();
      }
      @Override
      public Adapter casereal_literal(real_literal object)
      {
        return createreal_literalAdapter();
      }
      @Override
      public Adapter casedecimal_integer_literal(decimal_integer_literal object)
      {
        return createdecimal_integer_literalAdapter();
      }
      @Override
      public Adapter casedecimal_real_literal(decimal_real_literal object)
      {
        return createdecimal_real_literalAdapter();
      }
      @Override
      public Adapter casenumeral(numeral object)
      {
        return createnumeralAdapter();
      }
      @Override
      public Adapter caseexponent(exponent object)
      {
        return createexponentAdapter();
      }
      @Override
      public Adapter casepositive_exponent(positive_exponent object)
      {
        return createpositive_exponentAdapter();
      }
      @Override
      public Adapter casebased_integer_literal(based_integer_literal object)
      {
        return createbased_integer_literalAdapter();
      }
      @Override
      public Adapter casebase(base object)
      {
        return createbaseAdapter();
      }
      @Override
      public Adapter casebased_numeral(based_numeral object)
      {
        return createbased_numeralAdapter();
      }
      @Override
      public Adapter caseinteger(integer object)
      {
        return createintegerAdapter();
      }
      @Override
      public Adapter casedispatch_condition(dispatch_condition object)
      {
        return createdispatch_conditionAdapter();
      }
      @Override
      public Adapter casedispatch_trigger_condition(dispatch_trigger_condition object)
      {
        return createdispatch_trigger_conditionAdapter();
      }
      @Override
      public Adapter casedispatch_trigger_logical_expression(dispatch_trigger_logical_expression object)
      {
        return createdispatch_trigger_logical_expressionAdapter();
      }
      @Override
      public Adapter casedispatch_conjunction(dispatch_conjunction object)
      {
        return createdispatch_conjunctionAdapter();
      }
      @Override
      public Adapter casecompletion_relative_timeout_condition_and_catch(completion_relative_timeout_condition_and_catch object)
      {
        return createcompletion_relative_timeout_condition_and_catchAdapter();
      }
      @Override
      public Adapter casedispatch_trigger(dispatch_trigger object)
      {
        return createdispatch_triggerAdapter();
      }
      @Override
      public Adapter casefrozen_ports(frozen_ports object)
      {
        return createfrozen_portsAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.Guard <em>Guard</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.Guard
   * @generated
   */
  public Adapter createGuardAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.execute_condition <em>execute condition</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.execute_condition
   * @generated
   */
  public Adapter createexecute_conditionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.behavior_action_block_timeout_catch <em>behavior action block timeout catch</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.behavior_action_block_timeout_catch
   * @generated
   */
  public Adapter createbehavior_action_block_timeout_catchAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.value_expression <em>value expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_expression
   * @generated
   */
  public Adapter createvalue_expressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.relation <em>relation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.relation
   * @generated
   */
  public Adapter createrelationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.simple_expression <em>simple expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.simple_expression
   * @generated
   */
  public Adapter createsimple_expressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.term <em>term</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.term
   * @generated
   */
  public Adapter createtermAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.factor <em>factor</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.factor
   * @generated
   */
  public Adapter createfactorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.value <em>value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.value
   * @generated
   */
  public Adapter createvalueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.value_variable <em>value variable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_variable
   * @generated
   */
  public Adapter createvalue_variableAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.value_constant <em>value constant</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_constant
   * @generated
   */
  public Adapter createvalue_constantAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.logical_operator <em>logical operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.logical_operator
   * @generated
   */
  public Adapter createlogical_operatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator <em>relational operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.relational_operator
   * @generated
   */
  public Adapter createrelational_operatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.binary_adding_operator <em>binary adding operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.binary_adding_operator
   * @generated
   */
  public Adapter createbinary_adding_operatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.unary_adding_operator <em>unary adding operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.unary_adding_operator
   * @generated
   */
  public Adapter createunary_adding_operatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.multiplying_operator <em>multiplying operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.multiplying_operator
   * @generated
   */
  public Adapter createmultiplying_operatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.binary_numeric_operator <em>binary numeric operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.binary_numeric_operator
   * @generated
   */
  public Adapter createbinary_numeric_operatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.unary_numeric_operator <em>unary numeric operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.unary_numeric_operator
   * @generated
   */
  public Adapter createunary_numeric_operatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.unary_boolean_operator <em>unary boolean operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.unary_boolean_operator
   * @generated
   */
  public Adapter createunary_boolean_operatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.boolean_literal <em>boolean literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.boolean_literal
   * @generated
   */
  public Adapter createboolean_literalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.integer_value <em>integer value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.integer_value
   * @generated
   */
  public Adapter createinteger_valueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.behavior_time <em>behavior time</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.behavior_time
   * @generated
   */
  public Adapter createbehavior_timeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier <em>unit identifier</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.unit_identifier
   * @generated
   */
  public Adapter createunit_identifierAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.numeric_literal <em>numeric literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.numeric_literal
   * @generated
   */
  public Adapter createnumeric_literalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.integer_literal <em>integer literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.integer_literal
   * @generated
   */
  public Adapter createinteger_literalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.real_literal <em>real literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.real_literal
   * @generated
   */
  public Adapter createreal_literalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.decimal_integer_literal <em>decimal integer literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.decimal_integer_literal
   * @generated
   */
  public Adapter createdecimal_integer_literalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.decimal_real_literal <em>decimal real literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.decimal_real_literal
   * @generated
   */
  public Adapter createdecimal_real_literalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.numeral <em>numeral</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.numeral
   * @generated
   */
  public Adapter createnumeralAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.exponent <em>exponent</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.exponent
   * @generated
   */
  public Adapter createexponentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.positive_exponent <em>positive exponent</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.positive_exponent
   * @generated
   */
  public Adapter createpositive_exponentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.based_integer_literal <em>based integer literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.based_integer_literal
   * @generated
   */
  public Adapter createbased_integer_literalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.base <em>base</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.base
   * @generated
   */
  public Adapter createbaseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.based_numeral <em>based numeral</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.based_numeral
   * @generated
   */
  public Adapter createbased_numeralAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.integer <em>integer</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.integer
   * @generated
   */
  public Adapter createintegerAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_condition <em>dispatch condition</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_condition
   * @generated
   */
  public Adapter createdispatch_conditionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition <em>dispatch trigger condition</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition
   * @generated
   */
  public Adapter createdispatch_trigger_conditionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_logical_expression <em>dispatch trigger logical expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_logical_expression
   * @generated
   */
  public Adapter createdispatch_trigger_logical_expressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_conjunction <em>dispatch conjunction</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_conjunction
   * @generated
   */
  public Adapter createdispatch_conjunctionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.completion_relative_timeout_condition_and_catch <em>completion relative timeout condition and catch</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.completion_relative_timeout_condition_and_catch
   * @generated
   */
  public Adapter createcompletion_relative_timeout_condition_and_catchAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger <em>dispatch trigger</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger
   * @generated
   */
  public Adapter createdispatch_triggerAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baguards.baGuards.frozen_ports <em>frozen ports</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baguards.baGuards.frozen_ports
   * @generated
   */
  public Adapter createfrozen_portsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //BaGuardsAdapterFactory
