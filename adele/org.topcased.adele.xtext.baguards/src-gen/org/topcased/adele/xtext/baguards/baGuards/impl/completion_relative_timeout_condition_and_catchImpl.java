/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.behavior_time;
import org.topcased.adele.xtext.baguards.baGuards.completion_relative_timeout_condition_and_catch;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>completion relative timeout condition and catch</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.completion_relative_timeout_condition_and_catchImpl#getCdtcacname1 <em>Cdtcacname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.completion_relative_timeout_condition_and_catchImpl#getCdtcacname2 <em>Cdtcacname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class completion_relative_timeout_condition_and_catchImpl extends MinimalEObjectImpl.Container implements completion_relative_timeout_condition_and_catch
{
  /**
   * The default value of the '{@link #getCdtcacname1() <em>Cdtcacname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCdtcacname1()
   * @generated
   * @ordered
   */
  protected static final String CDTCACNAME1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCdtcacname1() <em>Cdtcacname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCdtcacname1()
   * @generated
   * @ordered
   */
  protected String cdtcacname1 = CDTCACNAME1_EDEFAULT;

  /**
   * The cached value of the '{@link #getCdtcacname2() <em>Cdtcacname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCdtcacname2()
   * @generated
   * @ordered
   */
  protected behavior_time cdtcacname2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected completion_relative_timeout_condition_and_catchImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCdtcacname1()
  {
    return cdtcacname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCdtcacname1(String newCdtcacname1)
  {
    String oldCdtcacname1 = cdtcacname1;
    cdtcacname1 = newCdtcacname1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME1, oldCdtcacname1, cdtcacname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_time getCdtcacname2()
  {
    return cdtcacname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCdtcacname2(behavior_time newCdtcacname2, NotificationChain msgs)
  {
    behavior_time oldCdtcacname2 = cdtcacname2;
    cdtcacname2 = newCdtcacname2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME2, oldCdtcacname2, newCdtcacname2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCdtcacname2(behavior_time newCdtcacname2)
  {
    if (newCdtcacname2 != cdtcacname2)
    {
      NotificationChain msgs = null;
      if (cdtcacname2 != null)
        msgs = ((InternalEObject)cdtcacname2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME2, null, msgs);
      if (newCdtcacname2 != null)
        msgs = ((InternalEObject)newCdtcacname2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME2, null, msgs);
      msgs = basicSetCdtcacname2(newCdtcacname2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME2, newCdtcacname2, newCdtcacname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME2:
        return basicSetCdtcacname2(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME1:
        return getCdtcacname1();
      case BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME2:
        return getCdtcacname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME1:
        setCdtcacname1((String)newValue);
        return;
      case BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME2:
        setCdtcacname2((behavior_time)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME1:
        setCdtcacname1(CDTCACNAME1_EDEFAULT);
        return;
      case BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME2:
        setCdtcacname2((behavior_time)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME1:
        return CDTCACNAME1_EDEFAULT == null ? cdtcacname1 != null : !CDTCACNAME1_EDEFAULT.equals(cdtcacname1);
      case BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME2:
        return cdtcacname2 != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (cdtcacname1: ");
    result.append(cdtcacname1);
    result.append(')');
    return result.toString();
  }

} //completion_relative_timeout_condition_and_catchImpl
