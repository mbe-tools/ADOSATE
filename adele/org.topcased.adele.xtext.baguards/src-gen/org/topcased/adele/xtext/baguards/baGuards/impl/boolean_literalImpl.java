/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.boolean_literal;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>boolean literal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.boolean_literalImpl#getBlname1 <em>Blname1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class boolean_literalImpl extends MinimalEObjectImpl.Container implements boolean_literal
{
  /**
   * The default value of the '{@link #getBlname1() <em>Blname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBlname1()
   * @generated
   * @ordered
   */
  protected static final String BLNAME1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBlname1() <em>Blname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBlname1()
   * @generated
   * @ordered
   */
  protected String blname1 = BLNAME1_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected boolean_literalImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.BOOLEAN_LITERAL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBlname1()
  {
    return blname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBlname1(String newBlname1)
  {
    String oldBlname1 = blname1;
    blname1 = newBlname1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.BOOLEAN_LITERAL__BLNAME1, oldBlname1, blname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.BOOLEAN_LITERAL__BLNAME1:
        return getBlname1();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.BOOLEAN_LITERAL__BLNAME1:
        setBlname1((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.BOOLEAN_LITERAL__BLNAME1:
        setBlname1(BLNAME1_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.BOOLEAN_LITERAL__BLNAME1:
        return BLNAME1_EDEFAULT == null ? blname1 != null : !BLNAME1_EDEFAULT.equals(blname1);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (blname1: ");
    result.append(blname1);
    result.append(')');
    return result.toString();
  }

} //boolean_literalImpl
