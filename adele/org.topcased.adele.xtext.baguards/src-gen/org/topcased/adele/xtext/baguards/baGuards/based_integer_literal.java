/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>based integer literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.based_integer_literal#getBilname1 <em>Bilname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.based_integer_literal#getBilname2 <em>Bilname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.based_integer_literal#getBilname3 <em>Bilname3</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getbased_integer_literal()
 * @model
 * @generated
 */
public interface based_integer_literal extends EObject
{
  /**
   * Returns the value of the '<em><b>Bilname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bilname1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bilname1</em>' containment reference.
   * @see #setBilname1(base)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getbased_integer_literal_Bilname1()
   * @model containment="true"
   * @generated
   */
  base getBilname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.based_integer_literal#getBilname1 <em>Bilname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bilname1</em>' containment reference.
   * @see #getBilname1()
   * @generated
   */
  void setBilname1(base value);

  /**
   * Returns the value of the '<em><b>Bilname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bilname2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bilname2</em>' containment reference.
   * @see #setBilname2(based_numeral)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getbased_integer_literal_Bilname2()
   * @model containment="true"
   * @generated
   */
  based_numeral getBilname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.based_integer_literal#getBilname2 <em>Bilname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bilname2</em>' containment reference.
   * @see #getBilname2()
   * @generated
   */
  void setBilname2(based_numeral value);

  /**
   * Returns the value of the '<em><b>Bilname3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bilname3</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bilname3</em>' containment reference.
   * @see #setBilname3(positive_exponent)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getbased_integer_literal_Bilname3()
   * @model containment="true"
   * @generated
   */
  positive_exponent getBilname3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.based_integer_literal#getBilname3 <em>Bilname3</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bilname3</em>' containment reference.
   * @see #getBilname3()
   * @generated
   */
  void setBilname3(positive_exponent value);

} // based_integer_literal
