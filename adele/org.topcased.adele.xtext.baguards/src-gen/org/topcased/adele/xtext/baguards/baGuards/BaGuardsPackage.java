/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsFactory
 * @model kind="package"
 * @generated
 */
public interface BaGuardsPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "baGuards";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.topcased.org/adele/xtext/baguards/BaGuards";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "baGuards";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  BaGuardsPackage eINSTANCE = org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl.init();

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.GuardImpl <em>Guard</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.GuardImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getGuard()
   * @generated
   */
  int GUARD = 0;

  /**
   * The feature id for the '<em><b>Dispatch</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD__DISPATCH = 0;

  /**
   * The feature id for the '<em><b>Execute</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD__EXECUTE = 1;

  /**
   * The number of structural features of the '<em>Guard</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GUARD_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.execute_conditionImpl <em>execute condition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.execute_conditionImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getexecute_condition()
   * @generated
   */
  int EXECUTE_CONDITION = 1;

  /**
   * The feature id for the '<em><b>Ecname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXECUTE_CONDITION__ECNAME1 = 0;

  /**
   * The feature id for the '<em><b>Ecname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXECUTE_CONDITION__ECNAME2 = 1;

  /**
   * The feature id for the '<em><b>Ecname3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXECUTE_CONDITION__ECNAME3 = 2;

  /**
   * The number of structural features of the '<em>execute condition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXECUTE_CONDITION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.behavior_action_block_timeout_catchImpl <em>behavior action block timeout catch</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.behavior_action_block_timeout_catchImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getbehavior_action_block_timeout_catch()
   * @generated
   */
  int BEHAVIOR_ACTION_BLOCK_TIMEOUT_CATCH = 2;

  /**
   * The feature id for the '<em><b>Babname</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_BLOCK_TIMEOUT_CATCH__BABNAME = 0;

  /**
   * The number of structural features of the '<em>behavior action block timeout catch</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_BLOCK_TIMEOUT_CATCH_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.value_expressionImpl <em>value expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.value_expressionImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getvalue_expression()
   * @generated
   */
  int VALUE_EXPRESSION = 3;

  /**
   * The feature id for the '<em><b>Vename1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_EXPRESSION__VENAME1 = 0;

  /**
   * The feature id for the '<em><b>Vename3</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_EXPRESSION__VENAME3 = 1;

  /**
   * The feature id for the '<em><b>Vename2</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_EXPRESSION__VENAME2 = 2;

  /**
   * The number of structural features of the '<em>value expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_EXPRESSION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.relationImpl <em>relation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.relationImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getrelation()
   * @generated
   */
  int RELATION = 4;

  /**
   * The feature id for the '<em><b>Rname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATION__RNAME1 = 0;

  /**
   * The feature id for the '<em><b>Rname3</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATION__RNAME3 = 1;

  /**
   * The feature id for the '<em><b>Rname2</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATION__RNAME2 = 2;

  /**
   * The number of structural features of the '<em>relation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.simple_expressionImpl <em>simple expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.simple_expressionImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getsimple_expression()
   * @generated
   */
  int SIMPLE_EXPRESSION = 5;

  /**
   * The feature id for the '<em><b>Sename2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_EXPRESSION__SENAME2 = 0;

  /**
   * The feature id for the '<em><b>Sename1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_EXPRESSION__SENAME1 = 1;

  /**
   * The feature id for the '<em><b>Sename3</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_EXPRESSION__SENAME3 = 2;

  /**
   * The feature id for the '<em><b>Sename4</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_EXPRESSION__SENAME4 = 3;

  /**
   * The number of structural features of the '<em>simple expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_EXPRESSION_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.termImpl <em>term</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.termImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getterm()
   * @generated
   */
  int TERM = 6;

  /**
   * The feature id for the '<em><b>Tname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__TNAME1 = 0;

  /**
   * The feature id for the '<em><b>Tname3</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__TNAME3 = 1;

  /**
   * The feature id for the '<em><b>Tname2</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__TNAME2 = 2;

  /**
   * The number of structural features of the '<em>term</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.factorImpl <em>factor</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.factorImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getfactor()
   * @generated
   */
  int FACTOR = 7;

  /**
   * The feature id for the '<em><b>Fname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FACTOR__FNAME1 = 0;

  /**
   * The feature id for the '<em><b>Fname3</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FACTOR__FNAME3 = 1;

  /**
   * The feature id for the '<em><b>Fname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FACTOR__FNAME2 = 2;

  /**
   * The feature id for the '<em><b>Fname4</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FACTOR__FNAME4 = 3;

  /**
   * The feature id for the '<em><b>Fname5</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FACTOR__FNAME5 = 4;

  /**
   * The feature id for the '<em><b>Fname6</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FACTOR__FNAME6 = 5;

  /**
   * The feature id for the '<em><b>Fname7</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FACTOR__FNAME7 = 6;

  /**
   * The number of structural features of the '<em>factor</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FACTOR_FEATURE_COUNT = 7;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.valueImpl <em>value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.valueImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getvalue()
   * @generated
   */
  int VALUE = 8;

  /**
   * The feature id for the '<em><b>Vname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE__VNAME1 = 0;

  /**
   * The feature id for the '<em><b>Vname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE__VNAME2 = 1;

  /**
   * The number of structural features of the '<em>value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.value_variableImpl <em>value variable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.value_variableImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getvalue_variable()
   * @generated
   */
  int VALUE_VARIABLE = 9;

  /**
   * The feature id for the '<em><b>Vvname0</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_VARIABLE__VVNAME0 = 0;

  /**
   * The feature id for the '<em><b>Vvname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_VARIABLE__VVNAME1 = 1;

  /**
   * The feature id for the '<em><b>Vvname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_VARIABLE__VVNAME2 = 2;

  /**
   * The feature id for the '<em><b>Vvname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_VARIABLE__VVNAME3 = 3;

  /**
   * The number of structural features of the '<em>value variable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_VARIABLE_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.value_constantImpl <em>value constant</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.value_constantImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getvalue_constant()
   * @generated
   */
  int VALUE_CONSTANT = 10;

  /**
   * The feature id for the '<em><b>Vcname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_CONSTANT__VCNAME1 = 0;

  /**
   * The feature id for the '<em><b>Vcname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_CONSTANT__VCNAME2 = 1;

  /**
   * The feature id for the '<em><b>Vcname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_CONSTANT__VCNAME3 = 2;

  /**
   * The feature id for the '<em><b>Vcname4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_CONSTANT__VCNAME4 = 3;

  /**
   * The number of structural features of the '<em>value constant</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_CONSTANT_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.logical_operatorImpl <em>logical operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.logical_operatorImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getlogical_operator()
   * @generated
   */
  int LOGICAL_OPERATOR = 11;

  /**
   * The feature id for the '<em><b>Loname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGICAL_OPERATOR__LONAME1 = 0;

  /**
   * The feature id for the '<em><b>Loname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGICAL_OPERATOR__LONAME2 = 1;

  /**
   * The feature id for the '<em><b>Loname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGICAL_OPERATOR__LONAME3 = 2;

  /**
   * The number of structural features of the '<em>logical operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGICAL_OPERATOR_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.relational_operatorImpl <em>relational operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.relational_operatorImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getrelational_operator()
   * @generated
   */
  int RELATIONAL_OPERATOR = 12;

  /**
   * The feature id for the '<em><b>Roname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_OPERATOR__RONAME1 = 0;

  /**
   * The feature id for the '<em><b>Roname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_OPERATOR__RONAME2 = 1;

  /**
   * The feature id for the '<em><b>Roname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_OPERATOR__RONAME3 = 2;

  /**
   * The feature id for the '<em><b>Roname4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_OPERATOR__RONAME4 = 3;

  /**
   * The feature id for the '<em><b>Roname5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_OPERATOR__RONAME5 = 4;

  /**
   * The feature id for the '<em><b>Roname6</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_OPERATOR__RONAME6 = 5;

  /**
   * The number of structural features of the '<em>relational operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_OPERATOR_FEATURE_COUNT = 6;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.binary_adding_operatorImpl <em>binary adding operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.binary_adding_operatorImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getbinary_adding_operator()
   * @generated
   */
  int BINARY_ADDING_OPERATOR = 13;

  /**
   * The feature id for the '<em><b>Baoname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BINARY_ADDING_OPERATOR__BAONAME1 = 0;

  /**
   * The feature id for the '<em><b>Baoname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BINARY_ADDING_OPERATOR__BAONAME2 = 1;

  /**
   * The number of structural features of the '<em>binary adding operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BINARY_ADDING_OPERATOR_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.unary_adding_operatorImpl <em>unary adding operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.unary_adding_operatorImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getunary_adding_operator()
   * @generated
   */
  int UNARY_ADDING_OPERATOR = 14;

  /**
   * The feature id for the '<em><b>Uaoname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_ADDING_OPERATOR__UAONAME1 = 0;

  /**
   * The feature id for the '<em><b>Uaoname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_ADDING_OPERATOR__UAONAME2 = 1;

  /**
   * The number of structural features of the '<em>unary adding operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_ADDING_OPERATOR_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.multiplying_operatorImpl <em>multiplying operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.multiplying_operatorImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getmultiplying_operator()
   * @generated
   */
  int MULTIPLYING_OPERATOR = 15;

  /**
   * The feature id for the '<em><b>Moname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLYING_OPERATOR__MONAME1 = 0;

  /**
   * The feature id for the '<em><b>Moname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLYING_OPERATOR__MONAME2 = 1;

  /**
   * The feature id for the '<em><b>Moname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLYING_OPERATOR__MONAME3 = 2;

  /**
   * The feature id for the '<em><b>Moname4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLYING_OPERATOR__MONAME4 = 3;

  /**
   * The number of structural features of the '<em>multiplying operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLYING_OPERATOR_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.binary_numeric_operatorImpl <em>binary numeric operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.binary_numeric_operatorImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getbinary_numeric_operator()
   * @generated
   */
  int BINARY_NUMERIC_OPERATOR = 16;

  /**
   * The feature id for the '<em><b>Bnoname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BINARY_NUMERIC_OPERATOR__BNONAME = 0;

  /**
   * The number of structural features of the '<em>binary numeric operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BINARY_NUMERIC_OPERATOR_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.unary_numeric_operatorImpl <em>unary numeric operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.unary_numeric_operatorImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getunary_numeric_operator()
   * @generated
   */
  int UNARY_NUMERIC_OPERATOR = 17;

  /**
   * The feature id for the '<em><b>Unoname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_NUMERIC_OPERATOR__UNONAME = 0;

  /**
   * The number of structural features of the '<em>unary numeric operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_NUMERIC_OPERATOR_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.unary_boolean_operatorImpl <em>unary boolean operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.unary_boolean_operatorImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getunary_boolean_operator()
   * @generated
   */
  int UNARY_BOOLEAN_OPERATOR = 18;

  /**
   * The feature id for the '<em><b>Uboname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_BOOLEAN_OPERATOR__UBONAME = 0;

  /**
   * The number of structural features of the '<em>unary boolean operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_BOOLEAN_OPERATOR_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.boolean_literalImpl <em>boolean literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.boolean_literalImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getboolean_literal()
   * @generated
   */
  int BOOLEAN_LITERAL = 19;

  /**
   * The feature id for the '<em><b>Blname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL__BLNAME1 = 0;

  /**
   * The number of structural features of the '<em>boolean literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.integer_valueImpl <em>integer value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.integer_valueImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getinteger_value()
   * @generated
   */
  int INTEGER_VALUE = 20;

  /**
   * The feature id for the '<em><b>Ivname</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_VALUE__IVNAME = 0;

  /**
   * The number of structural features of the '<em>integer value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_VALUE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.behavior_timeImpl <em>behavior time</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.behavior_timeImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getbehavior_time()
   * @generated
   */
  int BEHAVIOR_TIME = 21;

  /**
   * The feature id for the '<em><b>Btname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_TIME__BTNAME1 = 0;

  /**
   * The feature id for the '<em><b>Btname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_TIME__BTNAME2 = 1;

  /**
   * The number of structural features of the '<em>behavior time</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_TIME_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.unit_identifierImpl <em>unit identifier</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.unit_identifierImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getunit_identifier()
   * @generated
   */
  int UNIT_IDENTIFIER = 22;

  /**
   * The feature id for the '<em><b>Uiname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNIT_IDENTIFIER__UINAME1 = 0;

  /**
   * The feature id for the '<em><b>Uiname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNIT_IDENTIFIER__UINAME2 = 1;

  /**
   * The feature id for the '<em><b>Uiname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNIT_IDENTIFIER__UINAME3 = 2;

  /**
   * The feature id for the '<em><b>Uiname4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNIT_IDENTIFIER__UINAME4 = 3;

  /**
   * The feature id for the '<em><b>Uiname5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNIT_IDENTIFIER__UINAME5 = 4;

  /**
   * The feature id for the '<em><b>Uiname6</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNIT_IDENTIFIER__UINAME6 = 5;

  /**
   * The feature id for the '<em><b>Uiname7</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNIT_IDENTIFIER__UINAME7 = 6;

  /**
   * The number of structural features of the '<em>unit identifier</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNIT_IDENTIFIER_FEATURE_COUNT = 7;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.numeric_literalImpl <em>numeric literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.numeric_literalImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getnumeric_literal()
   * @generated
   */
  int NUMERIC_LITERAL = 23;

  /**
   * The feature id for the '<em><b>Nlname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERIC_LITERAL__NLNAME1 = 0;

  /**
   * The feature id for the '<em><b>Nlname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERIC_LITERAL__NLNAME2 = 1;

  /**
   * The number of structural features of the '<em>numeric literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERIC_LITERAL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.integer_literalImpl <em>integer literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.integer_literalImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getinteger_literal()
   * @generated
   */
  int INTEGER_LITERAL = 24;

  /**
   * The feature id for the '<em><b>Ilname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL__ILNAME1 = 0;

  /**
   * The feature id for the '<em><b>Ilname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL__ILNAME2 = 1;

  /**
   * The number of structural features of the '<em>integer literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.real_literalImpl <em>real literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.real_literalImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getreal_literal()
   * @generated
   */
  int REAL_LITERAL = 25;

  /**
   * The feature id for the '<em><b>Rlname</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_LITERAL__RLNAME = 0;

  /**
   * The number of structural features of the '<em>real literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_LITERAL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.decimal_integer_literalImpl <em>decimal integer literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.decimal_integer_literalImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getdecimal_integer_literal()
   * @generated
   */
  int DECIMAL_INTEGER_LITERAL = 26;

  /**
   * The feature id for the '<em><b>Dilname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_INTEGER_LITERAL__DILNAME1 = 0;

  /**
   * The feature id for the '<em><b>Dilname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_INTEGER_LITERAL__DILNAME2 = 1;

  /**
   * The number of structural features of the '<em>decimal integer literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_INTEGER_LITERAL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.decimal_real_literalImpl <em>decimal real literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.decimal_real_literalImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getdecimal_real_literal()
   * @generated
   */
  int DECIMAL_REAL_LITERAL = 27;

  /**
   * The feature id for the '<em><b>Drlname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_REAL_LITERAL__DRLNAME1 = 0;

  /**
   * The feature id for the '<em><b>Drlname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_REAL_LITERAL__DRLNAME2 = 1;

  /**
   * The feature id for the '<em><b>Drlname3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_REAL_LITERAL__DRLNAME3 = 2;

  /**
   * The number of structural features of the '<em>decimal real literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_REAL_LITERAL_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.numeralImpl <em>numeral</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.numeralImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getnumeral()
   * @generated
   */
  int NUMERAL = 28;

  /**
   * The feature id for the '<em><b>Nname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERAL__NNAME1 = 0;

  /**
   * The feature id for the '<em><b>Nname2</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERAL__NNAME2 = 1;

  /**
   * The number of structural features of the '<em>numeral</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERAL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.exponentImpl <em>exponent</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.exponentImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getexponent()
   * @generated
   */
  int EXPONENT = 29;

  /**
   * The feature id for the '<em><b>Ename1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPONENT__ENAME1 = 0;

  /**
   * The feature id for the '<em><b>Ename2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPONENT__ENAME2 = 1;

  /**
   * The number of structural features of the '<em>exponent</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPONENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.positive_exponentImpl <em>positive exponent</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.positive_exponentImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getpositive_exponent()
   * @generated
   */
  int POSITIVE_EXPONENT = 30;

  /**
   * The feature id for the '<em><b>Pename</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POSITIVE_EXPONENT__PENAME = 0;

  /**
   * The number of structural features of the '<em>positive exponent</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POSITIVE_EXPONENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.based_integer_literalImpl <em>based integer literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.based_integer_literalImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getbased_integer_literal()
   * @generated
   */
  int BASED_INTEGER_LITERAL = 31;

  /**
   * The feature id for the '<em><b>Bilname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASED_INTEGER_LITERAL__BILNAME1 = 0;

  /**
   * The feature id for the '<em><b>Bilname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASED_INTEGER_LITERAL__BILNAME2 = 1;

  /**
   * The feature id for the '<em><b>Bilname3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASED_INTEGER_LITERAL__BILNAME3 = 2;

  /**
   * The number of structural features of the '<em>based integer literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASED_INTEGER_LITERAL_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.baseImpl <em>base</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.baseImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getbase()
   * @generated
   */
  int BASE = 32;

  /**
   * The feature id for the '<em><b>Bname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE__BNAME1 = 0;

  /**
   * The feature id for the '<em><b>Bname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE__BNAME2 = 1;

  /**
   * The number of structural features of the '<em>base</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.based_numeralImpl <em>based numeral</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.based_numeralImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getbased_numeral()
   * @generated
   */
  int BASED_NUMERAL = 33;

  /**
   * The feature id for the '<em><b>Bnname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASED_NUMERAL__BNNAME1 = 0;

  /**
   * The feature id for the '<em><b>Bnname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASED_NUMERAL__BNNAME2 = 1;

  /**
   * The number of structural features of the '<em>based numeral</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASED_NUMERAL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.integerImpl <em>integer</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.integerImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getinteger()
   * @generated
   */
  int INTEGER = 34;

  /**
   * The feature id for the '<em><b>Value1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER__VALUE1 = 0;

  /**
   * The feature id for the '<em><b>Value2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER__VALUE2 = 1;

  /**
   * The number of structural features of the '<em>integer</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_conditionImpl <em>dispatch condition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_conditionImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getdispatch_condition()
   * @generated
   */
  int DISPATCH_CONDITION = 35;

  /**
   * The feature id for the '<em><b>Dcname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_CONDITION__DCNAME3 = 0;

  /**
   * The feature id for the '<em><b>Dcname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_CONDITION__DCNAME1 = 1;

  /**
   * The feature id for the '<em><b>Dcname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_CONDITION__DCNAME2 = 2;

  /**
   * The number of structural features of the '<em>dispatch condition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_CONDITION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_trigger_conditionImpl <em>dispatch trigger condition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_trigger_conditionImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getdispatch_trigger_condition()
   * @generated
   */
  int DISPATCH_TRIGGER_CONDITION = 36;

  /**
   * The feature id for the '<em><b>Dtcname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_TRIGGER_CONDITION__DTCNAME1 = 0;

  /**
   * The feature id for the '<em><b>Dtcname4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_TRIGGER_CONDITION__DTCNAME4 = 1;

  /**
   * The feature id for the '<em><b>Dtcname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_TRIGGER_CONDITION__DTCNAME2 = 2;

  /**
   * The number of structural features of the '<em>dispatch trigger condition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_TRIGGER_CONDITION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_trigger_logical_expressionImpl <em>dispatch trigger logical expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_trigger_logical_expressionImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getdispatch_trigger_logical_expression()
   * @generated
   */
  int DISPATCH_TRIGGER_LOGICAL_EXPRESSION = 37;

  /**
   * The feature id for the '<em><b>Dtlename1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME1 = 0;

  /**
   * The feature id for the '<em><b>Dtlename2</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME2 = 1;

  /**
   * The number of structural features of the '<em>dispatch trigger logical expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_TRIGGER_LOGICAL_EXPRESSION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_conjunctionImpl <em>dispatch conjunction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_conjunctionImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getdispatch_conjunction()
   * @generated
   */
  int DISPATCH_CONJUNCTION = 38;

  /**
   * The feature id for the '<em><b>Dcname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_CONJUNCTION__DCNAME1 = 0;

  /**
   * The feature id for the '<em><b>Dcname2</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_CONJUNCTION__DCNAME2 = 1;

  /**
   * The number of structural features of the '<em>dispatch conjunction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_CONJUNCTION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.completion_relative_timeout_condition_and_catchImpl <em>completion relative timeout condition and catch</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.completion_relative_timeout_condition_and_catchImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getcompletion_relative_timeout_condition_and_catch()
   * @generated
   */
  int COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH = 39;

  /**
   * The feature id for the '<em><b>Cdtcacname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME1 = 0;

  /**
   * The feature id for the '<em><b>Cdtcacname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME2 = 1;

  /**
   * The number of structural features of the '<em>completion relative timeout condition and catch</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_triggerImpl <em>dispatch trigger</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_triggerImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getdispatch_trigger()
   * @generated
   */
  int DISPATCH_TRIGGER = 40;

  /**
   * The feature id for the '<em><b>Dtname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_TRIGGER__DTNAME = 0;

  /**
   * The number of structural features of the '<em>dispatch trigger</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISPATCH_TRIGGER_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.frozen_portsImpl <em>frozen ports</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.frozen_portsImpl
   * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getfrozen_ports()
   * @generated
   */
  int FROZEN_PORTS = 41;

  /**
   * The feature id for the '<em><b>Fpname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FROZEN_PORTS__FPNAME1 = 0;

  /**
   * The feature id for the '<em><b>Fpname2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FROZEN_PORTS__FPNAME2 = 1;

  /**
   * The number of structural features of the '<em>frozen ports</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FROZEN_PORTS_FEATURE_COUNT = 2;


  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.Guard <em>Guard</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Guard</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.Guard
   * @generated
   */
  EClass getGuard();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.Guard#getDispatch <em>Dispatch</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Dispatch</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.Guard#getDispatch()
   * @see #getGuard()
   * @generated
   */
  EReference getGuard_Dispatch();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.Guard#getExecute <em>Execute</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Execute</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.Guard#getExecute()
   * @see #getGuard()
   * @generated
   */
  EReference getGuard_Execute();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.execute_condition <em>execute condition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>execute condition</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.execute_condition
   * @generated
   */
  EClass getexecute_condition();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.execute_condition#getEcname1 <em>Ecname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ecname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.execute_condition#getEcname1()
   * @see #getexecute_condition()
   * @generated
   */
  EAttribute getexecute_condition_Ecname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.execute_condition#getEcname2 <em>Ecname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ecname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.execute_condition#getEcname2()
   * @see #getexecute_condition()
   * @generated
   */
  EReference getexecute_condition_Ecname2();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.execute_condition#getEcname3 <em>Ecname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ecname3</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.execute_condition#getEcname3()
   * @see #getexecute_condition()
   * @generated
   */
  EReference getexecute_condition_Ecname3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.behavior_action_block_timeout_catch <em>behavior action block timeout catch</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>behavior action block timeout catch</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.behavior_action_block_timeout_catch
   * @generated
   */
  EClass getbehavior_action_block_timeout_catch();

  /**
   * Returns the meta object for the attribute list '{@link org.topcased.adele.xtext.baguards.baGuards.behavior_action_block_timeout_catch#getBabname <em>Babname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Babname</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.behavior_action_block_timeout_catch#getBabname()
   * @see #getbehavior_action_block_timeout_catch()
   * @generated
   */
  EAttribute getbehavior_action_block_timeout_catch_Babname();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.value_expression <em>value expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>value expression</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_expression
   * @generated
   */
  EClass getvalue_expression();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.value_expression#getVename1 <em>Vename1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Vename1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_expression#getVename1()
   * @see #getvalue_expression()
   * @generated
   */
  EReference getvalue_expression_Vename1();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baguards.baGuards.value_expression#getVename3 <em>Vename3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Vename3</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_expression#getVename3()
   * @see #getvalue_expression()
   * @generated
   */
  EReference getvalue_expression_Vename3();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baguards.baGuards.value_expression#getVename2 <em>Vename2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Vename2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_expression#getVename2()
   * @see #getvalue_expression()
   * @generated
   */
  EReference getvalue_expression_Vename2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.relation <em>relation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>relation</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.relation
   * @generated
   */
  EClass getrelation();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.relation#getRname1 <em>Rname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Rname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.relation#getRname1()
   * @see #getrelation()
   * @generated
   */
  EReference getrelation_Rname1();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baguards.baGuards.relation#getRname3 <em>Rname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rname3</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.relation#getRname3()
   * @see #getrelation()
   * @generated
   */
  EReference getrelation_Rname3();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baguards.baGuards.relation#getRname2 <em>Rname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.relation#getRname2()
   * @see #getrelation()
   * @generated
   */
  EReference getrelation_Rname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.simple_expression <em>simple expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>simple expression</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.simple_expression
   * @generated
   */
  EClass getsimple_expression();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.simple_expression#getSename2 <em>Sename2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sename2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.simple_expression#getSename2()
   * @see #getsimple_expression()
   * @generated
   */
  EReference getsimple_expression_Sename2();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.simple_expression#getSename1 <em>Sename1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sename1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.simple_expression#getSename1()
   * @see #getsimple_expression()
   * @generated
   */
  EReference getsimple_expression_Sename1();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baguards.baGuards.simple_expression#getSename3 <em>Sename3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sename3</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.simple_expression#getSename3()
   * @see #getsimple_expression()
   * @generated
   */
  EReference getsimple_expression_Sename3();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baguards.baGuards.simple_expression#getSename4 <em>Sename4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sename4</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.simple_expression#getSename4()
   * @see #getsimple_expression()
   * @generated
   */
  EReference getsimple_expression_Sename4();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.term <em>term</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>term</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.term
   * @generated
   */
  EClass getterm();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.term#getTname1 <em>Tname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Tname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.term#getTname1()
   * @see #getterm()
   * @generated
   */
  EReference getterm_Tname1();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baguards.baGuards.term#getTname3 <em>Tname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Tname3</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.term#getTname3()
   * @see #getterm()
   * @generated
   */
  EReference getterm_Tname3();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baguards.baGuards.term#getTname2 <em>Tname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Tname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.term#getTname2()
   * @see #getterm()
   * @generated
   */
  EReference getterm_Tname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.factor <em>factor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>factor</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.factor
   * @generated
   */
  EClass getfactor();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.factor#getFname1 <em>Fname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Fname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.factor#getFname1()
   * @see #getfactor()
   * @generated
   */
  EReference getfactor_Fname1();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baguards.baGuards.factor#getFname3 <em>Fname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Fname3</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.factor#getFname3()
   * @see #getfactor()
   * @generated
   */
  EReference getfactor_Fname3();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.factor#getFname2 <em>Fname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Fname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.factor#getFname2()
   * @see #getfactor()
   * @generated
   */
  EReference getfactor_Fname2();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.factor#getFname4 <em>Fname4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Fname4</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.factor#getFname4()
   * @see #getfactor()
   * @generated
   */
  EReference getfactor_Fname4();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.factor#getFname5 <em>Fname5</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Fname5</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.factor#getFname5()
   * @see #getfactor()
   * @generated
   */
  EReference getfactor_Fname5();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.factor#getFname6 <em>Fname6</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Fname6</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.factor#getFname6()
   * @see #getfactor()
   * @generated
   */
  EReference getfactor_Fname6();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.factor#getFname7 <em>Fname7</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Fname7</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.factor#getFname7()
   * @see #getfactor()
   * @generated
   */
  EReference getfactor_Fname7();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.value <em>value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>value</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value
   * @generated
   */
  EClass getvalue();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.value#getVname1 <em>Vname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Vname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value#getVname1()
   * @see #getvalue()
   * @generated
   */
  EReference getvalue_Vname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.value#getVname2 <em>Vname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Vname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value#getVname2()
   * @see #getvalue()
   * @generated
   */
  EReference getvalue_Vname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.value_variable <em>value variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>value variable</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_variable
   * @generated
   */
  EClass getvalue_variable();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.value_variable#getVvname0 <em>Vvname0</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Vvname0</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_variable#getVvname0()
   * @see #getvalue_variable()
   * @generated
   */
  EAttribute getvalue_variable_Vvname0();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.value_variable#getVvname1 <em>Vvname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Vvname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_variable#getVvname1()
   * @see #getvalue_variable()
   * @generated
   */
  EAttribute getvalue_variable_Vvname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.value_variable#getVvname2 <em>Vvname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Vvname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_variable#getVvname2()
   * @see #getvalue_variable()
   * @generated
   */
  EAttribute getvalue_variable_Vvname2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.value_variable#getVvname3 <em>Vvname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Vvname3</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_variable#getVvname3()
   * @see #getvalue_variable()
   * @generated
   */
  EAttribute getvalue_variable_Vvname3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.value_constant <em>value constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>value constant</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_constant
   * @generated
   */
  EClass getvalue_constant();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.value_constant#getVcname1 <em>Vcname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Vcname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_constant#getVcname1()
   * @see #getvalue_constant()
   * @generated
   */
  EReference getvalue_constant_Vcname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.value_constant#getVcname2 <em>Vcname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Vcname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_constant#getVcname2()
   * @see #getvalue_constant()
   * @generated
   */
  EReference getvalue_constant_Vcname2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.value_constant#getVcname3 <em>Vcname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Vcname3</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_constant#getVcname3()
   * @see #getvalue_constant()
   * @generated
   */
  EAttribute getvalue_constant_Vcname3();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.value_constant#getVcname4 <em>Vcname4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Vcname4</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.value_constant#getVcname4()
   * @see #getvalue_constant()
   * @generated
   */
  EAttribute getvalue_constant_Vcname4();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.logical_operator <em>logical operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>logical operator</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.logical_operator
   * @generated
   */
  EClass getlogical_operator();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.logical_operator#getLoname1 <em>Loname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Loname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.logical_operator#getLoname1()
   * @see #getlogical_operator()
   * @generated
   */
  EAttribute getlogical_operator_Loname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.logical_operator#getLoname2 <em>Loname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Loname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.logical_operator#getLoname2()
   * @see #getlogical_operator()
   * @generated
   */
  EAttribute getlogical_operator_Loname2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.logical_operator#getLoname3 <em>Loname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Loname3</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.logical_operator#getLoname3()
   * @see #getlogical_operator()
   * @generated
   */
  EAttribute getlogical_operator_Loname3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator <em>relational operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>relational operator</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.relational_operator
   * @generated
   */
  EClass getrelational_operator();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname1 <em>Roname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Roname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname1()
   * @see #getrelational_operator()
   * @generated
   */
  EAttribute getrelational_operator_Roname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname2 <em>Roname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Roname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname2()
   * @see #getrelational_operator()
   * @generated
   */
  EAttribute getrelational_operator_Roname2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname3 <em>Roname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Roname3</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname3()
   * @see #getrelational_operator()
   * @generated
   */
  EAttribute getrelational_operator_Roname3();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname4 <em>Roname4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Roname4</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname4()
   * @see #getrelational_operator()
   * @generated
   */
  EAttribute getrelational_operator_Roname4();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname5 <em>Roname5</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Roname5</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname5()
   * @see #getrelational_operator()
   * @generated
   */
  EAttribute getrelational_operator_Roname5();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname6 <em>Roname6</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Roname6</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname6()
   * @see #getrelational_operator()
   * @generated
   */
  EAttribute getrelational_operator_Roname6();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.binary_adding_operator <em>binary adding operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>binary adding operator</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.binary_adding_operator
   * @generated
   */
  EClass getbinary_adding_operator();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.binary_adding_operator#getBaoname1 <em>Baoname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Baoname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.binary_adding_operator#getBaoname1()
   * @see #getbinary_adding_operator()
   * @generated
   */
  EAttribute getbinary_adding_operator_Baoname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.binary_adding_operator#getBaoname2 <em>Baoname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Baoname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.binary_adding_operator#getBaoname2()
   * @see #getbinary_adding_operator()
   * @generated
   */
  EAttribute getbinary_adding_operator_Baoname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.unary_adding_operator <em>unary adding operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>unary adding operator</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.unary_adding_operator
   * @generated
   */
  EClass getunary_adding_operator();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.unary_adding_operator#getUaoname1 <em>Uaoname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uaoname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.unary_adding_operator#getUaoname1()
   * @see #getunary_adding_operator()
   * @generated
   */
  EAttribute getunary_adding_operator_Uaoname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.unary_adding_operator#getUaoname2 <em>Uaoname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uaoname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.unary_adding_operator#getUaoname2()
   * @see #getunary_adding_operator()
   * @generated
   */
  EAttribute getunary_adding_operator_Uaoname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.multiplying_operator <em>multiplying operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>multiplying operator</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.multiplying_operator
   * @generated
   */
  EClass getmultiplying_operator();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.multiplying_operator#getMoname1 <em>Moname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Moname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.multiplying_operator#getMoname1()
   * @see #getmultiplying_operator()
   * @generated
   */
  EAttribute getmultiplying_operator_Moname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.multiplying_operator#getMoname2 <em>Moname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Moname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.multiplying_operator#getMoname2()
   * @see #getmultiplying_operator()
   * @generated
   */
  EAttribute getmultiplying_operator_Moname2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.multiplying_operator#getMoname3 <em>Moname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Moname3</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.multiplying_operator#getMoname3()
   * @see #getmultiplying_operator()
   * @generated
   */
  EAttribute getmultiplying_operator_Moname3();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.multiplying_operator#getMoname4 <em>Moname4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Moname4</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.multiplying_operator#getMoname4()
   * @see #getmultiplying_operator()
   * @generated
   */
  EAttribute getmultiplying_operator_Moname4();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.binary_numeric_operator <em>binary numeric operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>binary numeric operator</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.binary_numeric_operator
   * @generated
   */
  EClass getbinary_numeric_operator();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.binary_numeric_operator#getBnoname <em>Bnoname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bnoname</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.binary_numeric_operator#getBnoname()
   * @see #getbinary_numeric_operator()
   * @generated
   */
  EAttribute getbinary_numeric_operator_Bnoname();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.unary_numeric_operator <em>unary numeric operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>unary numeric operator</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.unary_numeric_operator
   * @generated
   */
  EClass getunary_numeric_operator();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.unary_numeric_operator#getUnoname <em>Unoname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Unoname</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.unary_numeric_operator#getUnoname()
   * @see #getunary_numeric_operator()
   * @generated
   */
  EAttribute getunary_numeric_operator_Unoname();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.unary_boolean_operator <em>unary boolean operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>unary boolean operator</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.unary_boolean_operator
   * @generated
   */
  EClass getunary_boolean_operator();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.unary_boolean_operator#getUboname <em>Uboname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uboname</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.unary_boolean_operator#getUboname()
   * @see #getunary_boolean_operator()
   * @generated
   */
  EAttribute getunary_boolean_operator_Uboname();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.boolean_literal <em>boolean literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>boolean literal</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.boolean_literal
   * @generated
   */
  EClass getboolean_literal();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.boolean_literal#getBlname1 <em>Blname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Blname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.boolean_literal#getBlname1()
   * @see #getboolean_literal()
   * @generated
   */
  EAttribute getboolean_literal_Blname1();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.integer_value <em>integer value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>integer value</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.integer_value
   * @generated
   */
  EClass getinteger_value();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.integer_value#getIvname <em>Ivname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ivname</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.integer_value#getIvname()
   * @see #getinteger_value()
   * @generated
   */
  EReference getinteger_value_Ivname();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.behavior_time <em>behavior time</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>behavior time</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.behavior_time
   * @generated
   */
  EClass getbehavior_time();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.behavior_time#getBtname1 <em>Btname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Btname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.behavior_time#getBtname1()
   * @see #getbehavior_time()
   * @generated
   */
  EReference getbehavior_time_Btname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.behavior_time#getBtname2 <em>Btname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Btname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.behavior_time#getBtname2()
   * @see #getbehavior_time()
   * @generated
   */
  EReference getbehavior_time_Btname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier <em>unit identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>unit identifier</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.unit_identifier
   * @generated
   */
  EClass getunit_identifier();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname1 <em>Uiname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uiname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname1()
   * @see #getunit_identifier()
   * @generated
   */
  EAttribute getunit_identifier_Uiname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname2 <em>Uiname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uiname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname2()
   * @see #getunit_identifier()
   * @generated
   */
  EAttribute getunit_identifier_Uiname2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname3 <em>Uiname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uiname3</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname3()
   * @see #getunit_identifier()
   * @generated
   */
  EAttribute getunit_identifier_Uiname3();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname4 <em>Uiname4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uiname4</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname4()
   * @see #getunit_identifier()
   * @generated
   */
  EAttribute getunit_identifier_Uiname4();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname5 <em>Uiname5</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uiname5</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname5()
   * @see #getunit_identifier()
   * @generated
   */
  EAttribute getunit_identifier_Uiname5();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname6 <em>Uiname6</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uiname6</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname6()
   * @see #getunit_identifier()
   * @generated
   */
  EAttribute getunit_identifier_Uiname6();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname7 <em>Uiname7</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uiname7</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.unit_identifier#getUiname7()
   * @see #getunit_identifier()
   * @generated
   */
  EAttribute getunit_identifier_Uiname7();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.numeric_literal <em>numeric literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>numeric literal</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.numeric_literal
   * @generated
   */
  EClass getnumeric_literal();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.numeric_literal#getNlname1 <em>Nlname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Nlname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.numeric_literal#getNlname1()
   * @see #getnumeric_literal()
   * @generated
   */
  EReference getnumeric_literal_Nlname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.numeric_literal#getNlname2 <em>Nlname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Nlname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.numeric_literal#getNlname2()
   * @see #getnumeric_literal()
   * @generated
   */
  EReference getnumeric_literal_Nlname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.integer_literal <em>integer literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>integer literal</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.integer_literal
   * @generated
   */
  EClass getinteger_literal();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.integer_literal#getIlname1 <em>Ilname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ilname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.integer_literal#getIlname1()
   * @see #getinteger_literal()
   * @generated
   */
  EReference getinteger_literal_Ilname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.integer_literal#getIlname2 <em>Ilname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ilname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.integer_literal#getIlname2()
   * @see #getinteger_literal()
   * @generated
   */
  EReference getinteger_literal_Ilname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.real_literal <em>real literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>real literal</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.real_literal
   * @generated
   */
  EClass getreal_literal();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.real_literal#getRlname <em>Rlname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Rlname</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.real_literal#getRlname()
   * @see #getreal_literal()
   * @generated
   */
  EReference getreal_literal_Rlname();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.decimal_integer_literal <em>decimal integer literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>decimal integer literal</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.decimal_integer_literal
   * @generated
   */
  EClass getdecimal_integer_literal();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.decimal_integer_literal#getDilname1 <em>Dilname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Dilname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.decimal_integer_literal#getDilname1()
   * @see #getdecimal_integer_literal()
   * @generated
   */
  EReference getdecimal_integer_literal_Dilname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.decimal_integer_literal#getDilname2 <em>Dilname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Dilname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.decimal_integer_literal#getDilname2()
   * @see #getdecimal_integer_literal()
   * @generated
   */
  EReference getdecimal_integer_literal_Dilname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.decimal_real_literal <em>decimal real literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>decimal real literal</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.decimal_real_literal
   * @generated
   */
  EClass getdecimal_real_literal();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.decimal_real_literal#getDrlname1 <em>Drlname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Drlname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.decimal_real_literal#getDrlname1()
   * @see #getdecimal_real_literal()
   * @generated
   */
  EReference getdecimal_real_literal_Drlname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.decimal_real_literal#getDrlname2 <em>Drlname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Drlname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.decimal_real_literal#getDrlname2()
   * @see #getdecimal_real_literal()
   * @generated
   */
  EReference getdecimal_real_literal_Drlname2();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.decimal_real_literal#getDrlname3 <em>Drlname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Drlname3</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.decimal_real_literal#getDrlname3()
   * @see #getdecimal_real_literal()
   * @generated
   */
  EReference getdecimal_real_literal_Drlname3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.numeral <em>numeral</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>numeral</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.numeral
   * @generated
   */
  EClass getnumeral();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.numeral#getNname1 <em>Nname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Nname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.numeral#getNname1()
   * @see #getnumeral()
   * @generated
   */
  EReference getnumeral_Nname1();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baguards.baGuards.numeral#getNname2 <em>Nname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Nname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.numeral#getNname2()
   * @see #getnumeral()
   * @generated
   */
  EReference getnumeral_Nname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.exponent <em>exponent</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>exponent</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.exponent
   * @generated
   */
  EClass getexponent();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.exponent#getEname1 <em>Ename1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ename1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.exponent#getEname1()
   * @see #getexponent()
   * @generated
   */
  EReference getexponent_Ename1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.exponent#getEname2 <em>Ename2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ename2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.exponent#getEname2()
   * @see #getexponent()
   * @generated
   */
  EReference getexponent_Ename2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.positive_exponent <em>positive exponent</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>positive exponent</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.positive_exponent
   * @generated
   */
  EClass getpositive_exponent();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.positive_exponent#getPename <em>Pename</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Pename</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.positive_exponent#getPename()
   * @see #getpositive_exponent()
   * @generated
   */
  EReference getpositive_exponent_Pename();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.based_integer_literal <em>based integer literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>based integer literal</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.based_integer_literal
   * @generated
   */
  EClass getbased_integer_literal();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.based_integer_literal#getBilname1 <em>Bilname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bilname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.based_integer_literal#getBilname1()
   * @see #getbased_integer_literal()
   * @generated
   */
  EReference getbased_integer_literal_Bilname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.based_integer_literal#getBilname2 <em>Bilname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bilname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.based_integer_literal#getBilname2()
   * @see #getbased_integer_literal()
   * @generated
   */
  EReference getbased_integer_literal_Bilname2();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.based_integer_literal#getBilname3 <em>Bilname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bilname3</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.based_integer_literal#getBilname3()
   * @see #getbased_integer_literal()
   * @generated
   */
  EReference getbased_integer_literal_Bilname3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.base <em>base</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>base</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.base
   * @generated
   */
  EClass getbase();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.base#getBname1 <em>Bname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.base#getBname1()
   * @see #getbase()
   * @generated
   */
  EAttribute getbase_Bname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.base#getBname2 <em>Bname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.base#getBname2()
   * @see #getbase()
   * @generated
   */
  EAttribute getbase_Bname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.based_numeral <em>based numeral</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>based numeral</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.based_numeral
   * @generated
   */
  EClass getbased_numeral();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.based_numeral#getBnname1 <em>Bnname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bnname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.based_numeral#getBnname1()
   * @see #getbased_numeral()
   * @generated
   */
  EAttribute getbased_numeral_Bnname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.based_numeral#getBnname2 <em>Bnname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bnname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.based_numeral#getBnname2()
   * @see #getbased_numeral()
   * @generated
   */
  EAttribute getbased_numeral_Bnname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.integer <em>integer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>integer</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.integer
   * @generated
   */
  EClass getinteger();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.integer#getValue1 <em>Value1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.integer#getValue1()
   * @see #getinteger()
   * @generated
   */
  EAttribute getinteger_Value1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.integer#getValue2 <em>Value2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.integer#getValue2()
   * @see #getinteger()
   * @generated
   */
  EAttribute getinteger_Value2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_condition <em>dispatch condition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dispatch condition</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_condition
   * @generated
   */
  EClass getdispatch_condition();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_condition#getDcname3 <em>Dcname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dcname3</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_condition#getDcname3()
   * @see #getdispatch_condition()
   * @generated
   */
  EAttribute getdispatch_condition_Dcname3();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_condition#getDcname1 <em>Dcname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Dcname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_condition#getDcname1()
   * @see #getdispatch_condition()
   * @generated
   */
  EReference getdispatch_condition_Dcname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_condition#getDcname2 <em>Dcname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Dcname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_condition#getDcname2()
   * @see #getdispatch_condition()
   * @generated
   */
  EReference getdispatch_condition_Dcname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition <em>dispatch trigger condition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dispatch trigger condition</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition
   * @generated
   */
  EClass getdispatch_trigger_condition();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition#getDtcname1 <em>Dtcname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Dtcname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition#getDtcname1()
   * @see #getdispatch_trigger_condition()
   * @generated
   */
  EReference getdispatch_trigger_condition_Dtcname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition#getDtcname4 <em>Dtcname4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dtcname4</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition#getDtcname4()
   * @see #getdispatch_trigger_condition()
   * @generated
   */
  EAttribute getdispatch_trigger_condition_Dtcname4();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition#getDtcname2 <em>Dtcname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Dtcname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition#getDtcname2()
   * @see #getdispatch_trigger_condition()
   * @generated
   */
  EReference getdispatch_trigger_condition_Dtcname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_logical_expression <em>dispatch trigger logical expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dispatch trigger logical expression</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_logical_expression
   * @generated
   */
  EClass getdispatch_trigger_logical_expression();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_logical_expression#getDtlename1 <em>Dtlename1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Dtlename1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_logical_expression#getDtlename1()
   * @see #getdispatch_trigger_logical_expression()
   * @generated
   */
  EReference getdispatch_trigger_logical_expression_Dtlename1();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_logical_expression#getDtlename2 <em>Dtlename2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Dtlename2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_logical_expression#getDtlename2()
   * @see #getdispatch_trigger_logical_expression()
   * @generated
   */
  EReference getdispatch_trigger_logical_expression_Dtlename2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_conjunction <em>dispatch conjunction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dispatch conjunction</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_conjunction
   * @generated
   */
  EClass getdispatch_conjunction();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_conjunction#getDcname1 <em>Dcname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Dcname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_conjunction#getDcname1()
   * @see #getdispatch_conjunction()
   * @generated
   */
  EReference getdispatch_conjunction_Dcname1();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_conjunction#getDcname2 <em>Dcname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Dcname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_conjunction#getDcname2()
   * @see #getdispatch_conjunction()
   * @generated
   */
  EReference getdispatch_conjunction_Dcname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.completion_relative_timeout_condition_and_catch <em>completion relative timeout condition and catch</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>completion relative timeout condition and catch</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.completion_relative_timeout_condition_and_catch
   * @generated
   */
  EClass getcompletion_relative_timeout_condition_and_catch();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.completion_relative_timeout_condition_and_catch#getCdtcacname1 <em>Cdtcacname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Cdtcacname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.completion_relative_timeout_condition_and_catch#getCdtcacname1()
   * @see #getcompletion_relative_timeout_condition_and_catch()
   * @generated
   */
  EAttribute getcompletion_relative_timeout_condition_and_catch_Cdtcacname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baguards.baGuards.completion_relative_timeout_condition_and_catch#getCdtcacname2 <em>Cdtcacname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Cdtcacname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.completion_relative_timeout_condition_and_catch#getCdtcacname2()
   * @see #getcompletion_relative_timeout_condition_and_catch()
   * @generated
   */
  EReference getcompletion_relative_timeout_condition_and_catch_Cdtcacname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger <em>dispatch trigger</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>dispatch trigger</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger
   * @generated
   */
  EClass getdispatch_trigger();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger#getDtname <em>Dtname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dtname</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger#getDtname()
   * @see #getdispatch_trigger()
   * @generated
   */
  EAttribute getdispatch_trigger_Dtname();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baguards.baGuards.frozen_ports <em>frozen ports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>frozen ports</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.frozen_ports
   * @generated
   */
  EClass getfrozen_ports();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baguards.baGuards.frozen_ports#getFpname1 <em>Fpname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Fpname1</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.frozen_ports#getFpname1()
   * @see #getfrozen_ports()
   * @generated
   */
  EAttribute getfrozen_ports_Fpname1();

  /**
   * Returns the meta object for the attribute list '{@link org.topcased.adele.xtext.baguards.baGuards.frozen_ports#getFpname2 <em>Fpname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Fpname2</em>'.
   * @see org.topcased.adele.xtext.baguards.baGuards.frozen_ports#getFpname2()
   * @see #getfrozen_ports()
   * @generated
   */
  EAttribute getfrozen_ports_Fpname2();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  BaGuardsFactory getBaGuardsFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.GuardImpl <em>Guard</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.GuardImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getGuard()
     * @generated
     */
    EClass GUARD = eINSTANCE.getGuard();

    /**
     * The meta object literal for the '<em><b>Dispatch</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GUARD__DISPATCH = eINSTANCE.getGuard_Dispatch();

    /**
     * The meta object literal for the '<em><b>Execute</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GUARD__EXECUTE = eINSTANCE.getGuard_Execute();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.execute_conditionImpl <em>execute condition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.execute_conditionImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getexecute_condition()
     * @generated
     */
    EClass EXECUTE_CONDITION = eINSTANCE.getexecute_condition();

    /**
     * The meta object literal for the '<em><b>Ecname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EXECUTE_CONDITION__ECNAME1 = eINSTANCE.getexecute_condition_Ecname1();

    /**
     * The meta object literal for the '<em><b>Ecname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXECUTE_CONDITION__ECNAME2 = eINSTANCE.getexecute_condition_Ecname2();

    /**
     * The meta object literal for the '<em><b>Ecname3</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXECUTE_CONDITION__ECNAME3 = eINSTANCE.getexecute_condition_Ecname3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.behavior_action_block_timeout_catchImpl <em>behavior action block timeout catch</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.behavior_action_block_timeout_catchImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getbehavior_action_block_timeout_catch()
     * @generated
     */
    EClass BEHAVIOR_ACTION_BLOCK_TIMEOUT_CATCH = eINSTANCE.getbehavior_action_block_timeout_catch();

    /**
     * The meta object literal for the '<em><b>Babname</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION_BLOCK_TIMEOUT_CATCH__BABNAME = eINSTANCE.getbehavior_action_block_timeout_catch_Babname();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.value_expressionImpl <em>value expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.value_expressionImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getvalue_expression()
     * @generated
     */
    EClass VALUE_EXPRESSION = eINSTANCE.getvalue_expression();

    /**
     * The meta object literal for the '<em><b>Vename1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE_EXPRESSION__VENAME1 = eINSTANCE.getvalue_expression_Vename1();

    /**
     * The meta object literal for the '<em><b>Vename3</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE_EXPRESSION__VENAME3 = eINSTANCE.getvalue_expression_Vename3();

    /**
     * The meta object literal for the '<em><b>Vename2</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE_EXPRESSION__VENAME2 = eINSTANCE.getvalue_expression_Vename2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.relationImpl <em>relation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.relationImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getrelation()
     * @generated
     */
    EClass RELATION = eINSTANCE.getrelation();

    /**
     * The meta object literal for the '<em><b>Rname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RELATION__RNAME1 = eINSTANCE.getrelation_Rname1();

    /**
     * The meta object literal for the '<em><b>Rname3</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RELATION__RNAME3 = eINSTANCE.getrelation_Rname3();

    /**
     * The meta object literal for the '<em><b>Rname2</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RELATION__RNAME2 = eINSTANCE.getrelation_Rname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.simple_expressionImpl <em>simple expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.simple_expressionImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getsimple_expression()
     * @generated
     */
    EClass SIMPLE_EXPRESSION = eINSTANCE.getsimple_expression();

    /**
     * The meta object literal for the '<em><b>Sename2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMPLE_EXPRESSION__SENAME2 = eINSTANCE.getsimple_expression_Sename2();

    /**
     * The meta object literal for the '<em><b>Sename1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMPLE_EXPRESSION__SENAME1 = eINSTANCE.getsimple_expression_Sename1();

    /**
     * The meta object literal for the '<em><b>Sename3</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMPLE_EXPRESSION__SENAME3 = eINSTANCE.getsimple_expression_Sename3();

    /**
     * The meta object literal for the '<em><b>Sename4</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMPLE_EXPRESSION__SENAME4 = eINSTANCE.getsimple_expression_Sename4();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.termImpl <em>term</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.termImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getterm()
     * @generated
     */
    EClass TERM = eINSTANCE.getterm();

    /**
     * The meta object literal for the '<em><b>Tname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TERM__TNAME1 = eINSTANCE.getterm_Tname1();

    /**
     * The meta object literal for the '<em><b>Tname3</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TERM__TNAME3 = eINSTANCE.getterm_Tname3();

    /**
     * The meta object literal for the '<em><b>Tname2</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TERM__TNAME2 = eINSTANCE.getterm_Tname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.factorImpl <em>factor</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.factorImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getfactor()
     * @generated
     */
    EClass FACTOR = eINSTANCE.getfactor();

    /**
     * The meta object literal for the '<em><b>Fname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FACTOR__FNAME1 = eINSTANCE.getfactor_Fname1();

    /**
     * The meta object literal for the '<em><b>Fname3</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FACTOR__FNAME3 = eINSTANCE.getfactor_Fname3();

    /**
     * The meta object literal for the '<em><b>Fname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FACTOR__FNAME2 = eINSTANCE.getfactor_Fname2();

    /**
     * The meta object literal for the '<em><b>Fname4</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FACTOR__FNAME4 = eINSTANCE.getfactor_Fname4();

    /**
     * The meta object literal for the '<em><b>Fname5</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FACTOR__FNAME5 = eINSTANCE.getfactor_Fname5();

    /**
     * The meta object literal for the '<em><b>Fname6</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FACTOR__FNAME6 = eINSTANCE.getfactor_Fname6();

    /**
     * The meta object literal for the '<em><b>Fname7</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FACTOR__FNAME7 = eINSTANCE.getfactor_Fname7();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.valueImpl <em>value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.valueImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getvalue()
     * @generated
     */
    EClass VALUE = eINSTANCE.getvalue();

    /**
     * The meta object literal for the '<em><b>Vname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE__VNAME1 = eINSTANCE.getvalue_Vname1();

    /**
     * The meta object literal for the '<em><b>Vname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE__VNAME2 = eINSTANCE.getvalue_Vname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.value_variableImpl <em>value variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.value_variableImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getvalue_variable()
     * @generated
     */
    EClass VALUE_VARIABLE = eINSTANCE.getvalue_variable();

    /**
     * The meta object literal for the '<em><b>Vvname0</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VALUE_VARIABLE__VVNAME0 = eINSTANCE.getvalue_variable_Vvname0();

    /**
     * The meta object literal for the '<em><b>Vvname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VALUE_VARIABLE__VVNAME1 = eINSTANCE.getvalue_variable_Vvname1();

    /**
     * The meta object literal for the '<em><b>Vvname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VALUE_VARIABLE__VVNAME2 = eINSTANCE.getvalue_variable_Vvname2();

    /**
     * The meta object literal for the '<em><b>Vvname3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VALUE_VARIABLE__VVNAME3 = eINSTANCE.getvalue_variable_Vvname3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.value_constantImpl <em>value constant</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.value_constantImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getvalue_constant()
     * @generated
     */
    EClass VALUE_CONSTANT = eINSTANCE.getvalue_constant();

    /**
     * The meta object literal for the '<em><b>Vcname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE_CONSTANT__VCNAME1 = eINSTANCE.getvalue_constant_Vcname1();

    /**
     * The meta object literal for the '<em><b>Vcname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE_CONSTANT__VCNAME2 = eINSTANCE.getvalue_constant_Vcname2();

    /**
     * The meta object literal for the '<em><b>Vcname3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VALUE_CONSTANT__VCNAME3 = eINSTANCE.getvalue_constant_Vcname3();

    /**
     * The meta object literal for the '<em><b>Vcname4</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VALUE_CONSTANT__VCNAME4 = eINSTANCE.getvalue_constant_Vcname4();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.logical_operatorImpl <em>logical operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.logical_operatorImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getlogical_operator()
     * @generated
     */
    EClass LOGICAL_OPERATOR = eINSTANCE.getlogical_operator();

    /**
     * The meta object literal for the '<em><b>Loname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LOGICAL_OPERATOR__LONAME1 = eINSTANCE.getlogical_operator_Loname1();

    /**
     * The meta object literal for the '<em><b>Loname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LOGICAL_OPERATOR__LONAME2 = eINSTANCE.getlogical_operator_Loname2();

    /**
     * The meta object literal for the '<em><b>Loname3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LOGICAL_OPERATOR__LONAME3 = eINSTANCE.getlogical_operator_Loname3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.relational_operatorImpl <em>relational operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.relational_operatorImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getrelational_operator()
     * @generated
     */
    EClass RELATIONAL_OPERATOR = eINSTANCE.getrelational_operator();

    /**
     * The meta object literal for the '<em><b>Roname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RELATIONAL_OPERATOR__RONAME1 = eINSTANCE.getrelational_operator_Roname1();

    /**
     * The meta object literal for the '<em><b>Roname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RELATIONAL_OPERATOR__RONAME2 = eINSTANCE.getrelational_operator_Roname2();

    /**
     * The meta object literal for the '<em><b>Roname3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RELATIONAL_OPERATOR__RONAME3 = eINSTANCE.getrelational_operator_Roname3();

    /**
     * The meta object literal for the '<em><b>Roname4</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RELATIONAL_OPERATOR__RONAME4 = eINSTANCE.getrelational_operator_Roname4();

    /**
     * The meta object literal for the '<em><b>Roname5</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RELATIONAL_OPERATOR__RONAME5 = eINSTANCE.getrelational_operator_Roname5();

    /**
     * The meta object literal for the '<em><b>Roname6</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RELATIONAL_OPERATOR__RONAME6 = eINSTANCE.getrelational_operator_Roname6();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.binary_adding_operatorImpl <em>binary adding operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.binary_adding_operatorImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getbinary_adding_operator()
     * @generated
     */
    EClass BINARY_ADDING_OPERATOR = eINSTANCE.getbinary_adding_operator();

    /**
     * The meta object literal for the '<em><b>Baoname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BINARY_ADDING_OPERATOR__BAONAME1 = eINSTANCE.getbinary_adding_operator_Baoname1();

    /**
     * The meta object literal for the '<em><b>Baoname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BINARY_ADDING_OPERATOR__BAONAME2 = eINSTANCE.getbinary_adding_operator_Baoname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.unary_adding_operatorImpl <em>unary adding operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.unary_adding_operatorImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getunary_adding_operator()
     * @generated
     */
    EClass UNARY_ADDING_OPERATOR = eINSTANCE.getunary_adding_operator();

    /**
     * The meta object literal for the '<em><b>Uaoname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNARY_ADDING_OPERATOR__UAONAME1 = eINSTANCE.getunary_adding_operator_Uaoname1();

    /**
     * The meta object literal for the '<em><b>Uaoname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNARY_ADDING_OPERATOR__UAONAME2 = eINSTANCE.getunary_adding_operator_Uaoname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.multiplying_operatorImpl <em>multiplying operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.multiplying_operatorImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getmultiplying_operator()
     * @generated
     */
    EClass MULTIPLYING_OPERATOR = eINSTANCE.getmultiplying_operator();

    /**
     * The meta object literal for the '<em><b>Moname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MULTIPLYING_OPERATOR__MONAME1 = eINSTANCE.getmultiplying_operator_Moname1();

    /**
     * The meta object literal for the '<em><b>Moname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MULTIPLYING_OPERATOR__MONAME2 = eINSTANCE.getmultiplying_operator_Moname2();

    /**
     * The meta object literal for the '<em><b>Moname3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MULTIPLYING_OPERATOR__MONAME3 = eINSTANCE.getmultiplying_operator_Moname3();

    /**
     * The meta object literal for the '<em><b>Moname4</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MULTIPLYING_OPERATOR__MONAME4 = eINSTANCE.getmultiplying_operator_Moname4();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.binary_numeric_operatorImpl <em>binary numeric operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.binary_numeric_operatorImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getbinary_numeric_operator()
     * @generated
     */
    EClass BINARY_NUMERIC_OPERATOR = eINSTANCE.getbinary_numeric_operator();

    /**
     * The meta object literal for the '<em><b>Bnoname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BINARY_NUMERIC_OPERATOR__BNONAME = eINSTANCE.getbinary_numeric_operator_Bnoname();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.unary_numeric_operatorImpl <em>unary numeric operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.unary_numeric_operatorImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getunary_numeric_operator()
     * @generated
     */
    EClass UNARY_NUMERIC_OPERATOR = eINSTANCE.getunary_numeric_operator();

    /**
     * The meta object literal for the '<em><b>Unoname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNARY_NUMERIC_OPERATOR__UNONAME = eINSTANCE.getunary_numeric_operator_Unoname();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.unary_boolean_operatorImpl <em>unary boolean operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.unary_boolean_operatorImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getunary_boolean_operator()
     * @generated
     */
    EClass UNARY_BOOLEAN_OPERATOR = eINSTANCE.getunary_boolean_operator();

    /**
     * The meta object literal for the '<em><b>Uboname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNARY_BOOLEAN_OPERATOR__UBONAME = eINSTANCE.getunary_boolean_operator_Uboname();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.boolean_literalImpl <em>boolean literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.boolean_literalImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getboolean_literal()
     * @generated
     */
    EClass BOOLEAN_LITERAL = eINSTANCE.getboolean_literal();

    /**
     * The meta object literal for the '<em><b>Blname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_LITERAL__BLNAME1 = eINSTANCE.getboolean_literal_Blname1();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.integer_valueImpl <em>integer value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.integer_valueImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getinteger_value()
     * @generated
     */
    EClass INTEGER_VALUE = eINSTANCE.getinteger_value();

    /**
     * The meta object literal for the '<em><b>Ivname</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_VALUE__IVNAME = eINSTANCE.getinteger_value_Ivname();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.behavior_timeImpl <em>behavior time</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.behavior_timeImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getbehavior_time()
     * @generated
     */
    EClass BEHAVIOR_TIME = eINSTANCE.getbehavior_time();

    /**
     * The meta object literal for the '<em><b>Btname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_TIME__BTNAME1 = eINSTANCE.getbehavior_time_Btname1();

    /**
     * The meta object literal for the '<em><b>Btname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_TIME__BTNAME2 = eINSTANCE.getbehavior_time_Btname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.unit_identifierImpl <em>unit identifier</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.unit_identifierImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getunit_identifier()
     * @generated
     */
    EClass UNIT_IDENTIFIER = eINSTANCE.getunit_identifier();

    /**
     * The meta object literal for the '<em><b>Uiname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNIT_IDENTIFIER__UINAME1 = eINSTANCE.getunit_identifier_Uiname1();

    /**
     * The meta object literal for the '<em><b>Uiname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNIT_IDENTIFIER__UINAME2 = eINSTANCE.getunit_identifier_Uiname2();

    /**
     * The meta object literal for the '<em><b>Uiname3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNIT_IDENTIFIER__UINAME3 = eINSTANCE.getunit_identifier_Uiname3();

    /**
     * The meta object literal for the '<em><b>Uiname4</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNIT_IDENTIFIER__UINAME4 = eINSTANCE.getunit_identifier_Uiname4();

    /**
     * The meta object literal for the '<em><b>Uiname5</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNIT_IDENTIFIER__UINAME5 = eINSTANCE.getunit_identifier_Uiname5();

    /**
     * The meta object literal for the '<em><b>Uiname6</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNIT_IDENTIFIER__UINAME6 = eINSTANCE.getunit_identifier_Uiname6();

    /**
     * The meta object literal for the '<em><b>Uiname7</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNIT_IDENTIFIER__UINAME7 = eINSTANCE.getunit_identifier_Uiname7();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.numeric_literalImpl <em>numeric literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.numeric_literalImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getnumeric_literal()
     * @generated
     */
    EClass NUMERIC_LITERAL = eINSTANCE.getnumeric_literal();

    /**
     * The meta object literal for the '<em><b>Nlname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NUMERIC_LITERAL__NLNAME1 = eINSTANCE.getnumeric_literal_Nlname1();

    /**
     * The meta object literal for the '<em><b>Nlname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NUMERIC_LITERAL__NLNAME2 = eINSTANCE.getnumeric_literal_Nlname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.integer_literalImpl <em>integer literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.integer_literalImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getinteger_literal()
     * @generated
     */
    EClass INTEGER_LITERAL = eINSTANCE.getinteger_literal();

    /**
     * The meta object literal for the '<em><b>Ilname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_LITERAL__ILNAME1 = eINSTANCE.getinteger_literal_Ilname1();

    /**
     * The meta object literal for the '<em><b>Ilname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_LITERAL__ILNAME2 = eINSTANCE.getinteger_literal_Ilname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.real_literalImpl <em>real literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.real_literalImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getreal_literal()
     * @generated
     */
    EClass REAL_LITERAL = eINSTANCE.getreal_literal();

    /**
     * The meta object literal for the '<em><b>Rlname</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REAL_LITERAL__RLNAME = eINSTANCE.getreal_literal_Rlname();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.decimal_integer_literalImpl <em>decimal integer literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.decimal_integer_literalImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getdecimal_integer_literal()
     * @generated
     */
    EClass DECIMAL_INTEGER_LITERAL = eINSTANCE.getdecimal_integer_literal();

    /**
     * The meta object literal for the '<em><b>Dilname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DECIMAL_INTEGER_LITERAL__DILNAME1 = eINSTANCE.getdecimal_integer_literal_Dilname1();

    /**
     * The meta object literal for the '<em><b>Dilname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DECIMAL_INTEGER_LITERAL__DILNAME2 = eINSTANCE.getdecimal_integer_literal_Dilname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.decimal_real_literalImpl <em>decimal real literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.decimal_real_literalImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getdecimal_real_literal()
     * @generated
     */
    EClass DECIMAL_REAL_LITERAL = eINSTANCE.getdecimal_real_literal();

    /**
     * The meta object literal for the '<em><b>Drlname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DECIMAL_REAL_LITERAL__DRLNAME1 = eINSTANCE.getdecimal_real_literal_Drlname1();

    /**
     * The meta object literal for the '<em><b>Drlname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DECIMAL_REAL_LITERAL__DRLNAME2 = eINSTANCE.getdecimal_real_literal_Drlname2();

    /**
     * The meta object literal for the '<em><b>Drlname3</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DECIMAL_REAL_LITERAL__DRLNAME3 = eINSTANCE.getdecimal_real_literal_Drlname3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.numeralImpl <em>numeral</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.numeralImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getnumeral()
     * @generated
     */
    EClass NUMERAL = eINSTANCE.getnumeral();

    /**
     * The meta object literal for the '<em><b>Nname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NUMERAL__NNAME1 = eINSTANCE.getnumeral_Nname1();

    /**
     * The meta object literal for the '<em><b>Nname2</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NUMERAL__NNAME2 = eINSTANCE.getnumeral_Nname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.exponentImpl <em>exponent</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.exponentImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getexponent()
     * @generated
     */
    EClass EXPONENT = eINSTANCE.getexponent();

    /**
     * The meta object literal for the '<em><b>Ename1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPONENT__ENAME1 = eINSTANCE.getexponent_Ename1();

    /**
     * The meta object literal for the '<em><b>Ename2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPONENT__ENAME2 = eINSTANCE.getexponent_Ename2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.positive_exponentImpl <em>positive exponent</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.positive_exponentImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getpositive_exponent()
     * @generated
     */
    EClass POSITIVE_EXPONENT = eINSTANCE.getpositive_exponent();

    /**
     * The meta object literal for the '<em><b>Pename</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference POSITIVE_EXPONENT__PENAME = eINSTANCE.getpositive_exponent_Pename();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.based_integer_literalImpl <em>based integer literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.based_integer_literalImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getbased_integer_literal()
     * @generated
     */
    EClass BASED_INTEGER_LITERAL = eINSTANCE.getbased_integer_literal();

    /**
     * The meta object literal for the '<em><b>Bilname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BASED_INTEGER_LITERAL__BILNAME1 = eINSTANCE.getbased_integer_literal_Bilname1();

    /**
     * The meta object literal for the '<em><b>Bilname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BASED_INTEGER_LITERAL__BILNAME2 = eINSTANCE.getbased_integer_literal_Bilname2();

    /**
     * The meta object literal for the '<em><b>Bilname3</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BASED_INTEGER_LITERAL__BILNAME3 = eINSTANCE.getbased_integer_literal_Bilname3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.baseImpl <em>base</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.baseImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getbase()
     * @generated
     */
    EClass BASE = eINSTANCE.getbase();

    /**
     * The meta object literal for the '<em><b>Bname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BASE__BNAME1 = eINSTANCE.getbase_Bname1();

    /**
     * The meta object literal for the '<em><b>Bname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BASE__BNAME2 = eINSTANCE.getbase_Bname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.based_numeralImpl <em>based numeral</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.based_numeralImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getbased_numeral()
     * @generated
     */
    EClass BASED_NUMERAL = eINSTANCE.getbased_numeral();

    /**
     * The meta object literal for the '<em><b>Bnname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BASED_NUMERAL__BNNAME1 = eINSTANCE.getbased_numeral_Bnname1();

    /**
     * The meta object literal for the '<em><b>Bnname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BASED_NUMERAL__BNNAME2 = eINSTANCE.getbased_numeral_Bnname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.integerImpl <em>integer</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.integerImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getinteger()
     * @generated
     */
    EClass INTEGER = eINSTANCE.getinteger();

    /**
     * The meta object literal for the '<em><b>Value1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTEGER__VALUE1 = eINSTANCE.getinteger_Value1();

    /**
     * The meta object literal for the '<em><b>Value2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTEGER__VALUE2 = eINSTANCE.getinteger_Value2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_conditionImpl <em>dispatch condition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_conditionImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getdispatch_condition()
     * @generated
     */
    EClass DISPATCH_CONDITION = eINSTANCE.getdispatch_condition();

    /**
     * The meta object literal for the '<em><b>Dcname3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DISPATCH_CONDITION__DCNAME3 = eINSTANCE.getdispatch_condition_Dcname3();

    /**
     * The meta object literal for the '<em><b>Dcname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DISPATCH_CONDITION__DCNAME1 = eINSTANCE.getdispatch_condition_Dcname1();

    /**
     * The meta object literal for the '<em><b>Dcname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DISPATCH_CONDITION__DCNAME2 = eINSTANCE.getdispatch_condition_Dcname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_trigger_conditionImpl <em>dispatch trigger condition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_trigger_conditionImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getdispatch_trigger_condition()
     * @generated
     */
    EClass DISPATCH_TRIGGER_CONDITION = eINSTANCE.getdispatch_trigger_condition();

    /**
     * The meta object literal for the '<em><b>Dtcname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DISPATCH_TRIGGER_CONDITION__DTCNAME1 = eINSTANCE.getdispatch_trigger_condition_Dtcname1();

    /**
     * The meta object literal for the '<em><b>Dtcname4</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DISPATCH_TRIGGER_CONDITION__DTCNAME4 = eINSTANCE.getdispatch_trigger_condition_Dtcname4();

    /**
     * The meta object literal for the '<em><b>Dtcname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DISPATCH_TRIGGER_CONDITION__DTCNAME2 = eINSTANCE.getdispatch_trigger_condition_Dtcname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_trigger_logical_expressionImpl <em>dispatch trigger logical expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_trigger_logical_expressionImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getdispatch_trigger_logical_expression()
     * @generated
     */
    EClass DISPATCH_TRIGGER_LOGICAL_EXPRESSION = eINSTANCE.getdispatch_trigger_logical_expression();

    /**
     * The meta object literal for the '<em><b>Dtlename1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME1 = eINSTANCE.getdispatch_trigger_logical_expression_Dtlename1();

    /**
     * The meta object literal for the '<em><b>Dtlename2</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME2 = eINSTANCE.getdispatch_trigger_logical_expression_Dtlename2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_conjunctionImpl <em>dispatch conjunction</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_conjunctionImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getdispatch_conjunction()
     * @generated
     */
    EClass DISPATCH_CONJUNCTION = eINSTANCE.getdispatch_conjunction();

    /**
     * The meta object literal for the '<em><b>Dcname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DISPATCH_CONJUNCTION__DCNAME1 = eINSTANCE.getdispatch_conjunction_Dcname1();

    /**
     * The meta object literal for the '<em><b>Dcname2</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DISPATCH_CONJUNCTION__DCNAME2 = eINSTANCE.getdispatch_conjunction_Dcname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.completion_relative_timeout_condition_and_catchImpl <em>completion relative timeout condition and catch</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.completion_relative_timeout_condition_and_catchImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getcompletion_relative_timeout_condition_and_catch()
     * @generated
     */
    EClass COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH = eINSTANCE.getcompletion_relative_timeout_condition_and_catch();

    /**
     * The meta object literal for the '<em><b>Cdtcacname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME1 = eINSTANCE.getcompletion_relative_timeout_condition_and_catch_Cdtcacname1();

    /**
     * The meta object literal for the '<em><b>Cdtcacname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME2 = eINSTANCE.getcompletion_relative_timeout_condition_and_catch_Cdtcacname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_triggerImpl <em>dispatch trigger</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_triggerImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getdispatch_trigger()
     * @generated
     */
    EClass DISPATCH_TRIGGER = eINSTANCE.getdispatch_trigger();

    /**
     * The meta object literal for the '<em><b>Dtname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DISPATCH_TRIGGER__DTNAME = eINSTANCE.getdispatch_trigger_Dtname();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baguards.baGuards.impl.frozen_portsImpl <em>frozen ports</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.frozen_portsImpl
     * @see org.topcased.adele.xtext.baguards.baGuards.impl.BaGuardsPackageImpl#getfrozen_ports()
     * @generated
     */
    EClass FROZEN_PORTS = eINSTANCE.getfrozen_ports();

    /**
     * The meta object literal for the '<em><b>Fpname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FROZEN_PORTS__FPNAME1 = eINSTANCE.getfrozen_ports_Fpname1();

    /**
     * The meta object literal for the '<em><b>Fpname2</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FROZEN_PORTS__FPNAME2 = eINSTANCE.getfrozen_ports_Fpname2();

  }

} //BaGuardsPackage
