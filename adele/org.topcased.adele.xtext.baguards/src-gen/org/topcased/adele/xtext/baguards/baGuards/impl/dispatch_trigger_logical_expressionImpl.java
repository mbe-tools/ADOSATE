/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_conjunction;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_logical_expression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dispatch trigger logical expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_trigger_logical_expressionImpl#getDtlename1 <em>Dtlename1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_trigger_logical_expressionImpl#getDtlename2 <em>Dtlename2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class dispatch_trigger_logical_expressionImpl extends MinimalEObjectImpl.Container implements dispatch_trigger_logical_expression
{
  /**
   * The cached value of the '{@link #getDtlename1() <em>Dtlename1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDtlename1()
   * @generated
   * @ordered
   */
  protected dispatch_conjunction dtlename1;

  /**
   * The cached value of the '{@link #getDtlename2() <em>Dtlename2</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDtlename2()
   * @generated
   * @ordered
   */
  protected EList<dispatch_conjunction> dtlename2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected dispatch_trigger_logical_expressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.DISPATCH_TRIGGER_LOGICAL_EXPRESSION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public dispatch_conjunction getDtlename1()
  {
    return dtlename1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDtlename1(dispatch_conjunction newDtlename1, NotificationChain msgs)
  {
    dispatch_conjunction oldDtlename1 = dtlename1;
    dtlename1 = newDtlename1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME1, oldDtlename1, newDtlename1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDtlename1(dispatch_conjunction newDtlename1)
  {
    if (newDtlename1 != dtlename1)
    {
      NotificationChain msgs = null;
      if (dtlename1 != null)
        msgs = ((InternalEObject)dtlename1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME1, null, msgs);
      if (newDtlename1 != null)
        msgs = ((InternalEObject)newDtlename1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME1, null, msgs);
      msgs = basicSetDtlename1(newDtlename1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME1, newDtlename1, newDtlename1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<dispatch_conjunction> getDtlename2()
  {
    if (dtlename2 == null)
    {
      dtlename2 = new EObjectContainmentEList<dispatch_conjunction>(dispatch_conjunction.class, this, BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME2);
    }
    return dtlename2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME1:
        return basicSetDtlename1(null, msgs);
      case BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME2:
        return ((InternalEList<?>)getDtlename2()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME1:
        return getDtlename1();
      case BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME2:
        return getDtlename2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME1:
        setDtlename1((dispatch_conjunction)newValue);
        return;
      case BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME2:
        getDtlename2().clear();
        getDtlename2().addAll((Collection<? extends dispatch_conjunction>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME1:
        setDtlename1((dispatch_conjunction)null);
        return;
      case BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME2:
        getDtlename2().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME1:
        return dtlename1 != null;
      case BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME2:
        return dtlename2 != null && !dtlename2.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //dispatch_trigger_logical_expressionImpl
