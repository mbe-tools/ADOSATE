/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dispatch condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_condition#getDcname3 <em>Dcname3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_condition#getDcname1 <em>Dcname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_condition#getDcname2 <em>Dcname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdispatch_condition()
 * @model
 * @generated
 */
public interface dispatch_condition extends EObject
{
  /**
   * Returns the value of the '<em><b>Dcname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dcname3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dcname3</em>' attribute.
   * @see #setDcname3(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdispatch_condition_Dcname3()
   * @model
   * @generated
   */
  String getDcname3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_condition#getDcname3 <em>Dcname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dcname3</em>' attribute.
   * @see #getDcname3()
   * @generated
   */
  void setDcname3(String value);

  /**
   * Returns the value of the '<em><b>Dcname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dcname1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dcname1</em>' containment reference.
   * @see #setDcname1(dispatch_trigger_condition)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdispatch_condition_Dcname1()
   * @model containment="true"
   * @generated
   */
  dispatch_trigger_condition getDcname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_condition#getDcname1 <em>Dcname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dcname1</em>' containment reference.
   * @see #getDcname1()
   * @generated
   */
  void setDcname1(dispatch_trigger_condition value);

  /**
   * Returns the value of the '<em><b>Dcname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dcname2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dcname2</em>' containment reference.
   * @see #setDcname2(frozen_ports)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdispatch_condition_Dcname2()
   * @model containment="true"
   * @generated
   */
  frozen_ports getDcname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_condition#getDcname2 <em>Dcname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dcname2</em>' containment reference.
   * @see #getDcname2()
   * @generated
   */
  void setDcname2(frozen_ports value);

} // dispatch_condition
