/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>simple expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.simple_expression#getSename2 <em>Sename2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.simple_expression#getSename1 <em>Sename1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.simple_expression#getSename3 <em>Sename3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.simple_expression#getSename4 <em>Sename4</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getsimple_expression()
 * @model
 * @generated
 */
public interface simple_expression extends EObject
{
  /**
   * Returns the value of the '<em><b>Sename2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sename2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sename2</em>' containment reference.
   * @see #setSename2(unary_adding_operator)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getsimple_expression_Sename2()
   * @model containment="true"
   * @generated
   */
  unary_adding_operator getSename2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.simple_expression#getSename2 <em>Sename2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sename2</em>' containment reference.
   * @see #getSename2()
   * @generated
   */
  void setSename2(unary_adding_operator value);

  /**
   * Returns the value of the '<em><b>Sename1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sename1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sename1</em>' containment reference.
   * @see #setSename1(term)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getsimple_expression_Sename1()
   * @model containment="true"
   * @generated
   */
  term getSename1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.simple_expression#getSename1 <em>Sename1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sename1</em>' containment reference.
   * @see #getSename1()
   * @generated
   */
  void setSename1(term value);

  /**
   * Returns the value of the '<em><b>Sename3</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baguards.baGuards.binary_adding_operator}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sename3</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sename3</em>' containment reference list.
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getsimple_expression_Sename3()
   * @model containment="true"
   * @generated
   */
  EList<binary_adding_operator> getSename3();

  /**
   * Returns the value of the '<em><b>Sename4</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baguards.baGuards.term}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sename4</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sename4</em>' containment reference list.
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getsimple_expression_Sename4()
   * @model containment="true"
   * @generated
   */
  EList<term> getSename4();

} // simple_expression
