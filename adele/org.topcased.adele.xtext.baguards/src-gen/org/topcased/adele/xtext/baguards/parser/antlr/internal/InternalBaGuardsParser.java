package org.topcased.adele.xtext.baguards.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.topcased.adele.xtext.baguards.services.BaGuardsGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalBaGuardsParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING_LITERAL", "RULE_DIGIT", "RULE_EXTENDED_DIGIT", "RULE_INT", "RULE_ID", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'otherwise'", "'timeout'", "'?'", "'count'", "'fresh'", "'::'", "'and'", "'or'", "'xor'", "'='", "'!='", "'<'", "'<='", "'>'", "'>='", "'+'", "'-'", "'*'", "'/'", "'mod'", "'rem'", "'**'", "'abs'", "'not'", "'true'", "'false'", "'ps'", "'ns'", "'us'", "'ms'", "'sec'", "'min'", "'hr'", "'.'", "'_'", "'E'", "'E -'", "'#'", "'on dispatch'", "'frozen'", "'stop'", "','"
    };
    public static final int RULE_ID=8;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=13;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int EOF=-1;
    public static final int T__55=55;
    public static final int T__19=19;
    public static final int T__16=16;
    public static final int T__51=51;
    public static final int T__15=15;
    public static final int T__52=52;
    public static final int T__18=18;
    public static final int T__53=53;
    public static final int RULE_STRING_LITERAL=4;
    public static final int T__54=54;
    public static final int T__17=17;
    public static final int T__14=14;
    public static final int RULE_INT=7;
    public static final int T__50=50;
    public static final int RULE_EXTENDED_DIGIT=6;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RULE_SL_COMMENT=11;
    public static final int RULE_ML_COMMENT=10;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_STRING=9;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int RULE_WS=12;
    public static final int RULE_DIGIT=5;

    // delegates
    // delegators


        public InternalBaGuardsParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalBaGuardsParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalBaGuardsParser.tokenNames; }
    public String getGrammarFileName() { return "../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g"; }



     	private BaGuardsGrammarAccess grammarAccess;
     	
        public InternalBaGuardsParser(TokenStream input, BaGuardsGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Guard";	
       	}
       	
       	@Override
       	protected BaGuardsGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleGuard"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:67:1: entryRuleGuard returns [EObject current=null] : iv_ruleGuard= ruleGuard EOF ;
    public final EObject entryRuleGuard() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuard = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:68:2: (iv_ruleGuard= ruleGuard EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:69:2: iv_ruleGuard= ruleGuard EOF
            {
             newCompositeNode(grammarAccess.getGuardRule()); 
            pushFollow(FOLLOW_ruleGuard_in_entryRuleGuard75);
            iv_ruleGuard=ruleGuard();

            state._fsp--;

             current =iv_ruleGuard; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGuard85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuard"


    // $ANTLR start "ruleGuard"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:76:1: ruleGuard returns [EObject current=null] : ( ( (lv_dispatch_0_0= ruledispatch_condition ) ) | ( (lv_execute_1_0= ruleexecute_condition ) ) ) ;
    public final EObject ruleGuard() throws RecognitionException {
        EObject current = null;

        EObject lv_dispatch_0_0 = null;

        EObject lv_execute_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:79:28: ( ( ( (lv_dispatch_0_0= ruledispatch_condition ) ) | ( (lv_execute_1_0= ruleexecute_condition ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:80:1: ( ( (lv_dispatch_0_0= ruledispatch_condition ) ) | ( (lv_execute_1_0= ruleexecute_condition ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:80:1: ( ( (lv_dispatch_0_0= ruledispatch_condition ) ) | ( (lv_execute_1_0= ruleexecute_condition ) ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==52) ) {
                alt1=1;
            }
            else if ( ((LA1_0>=RULE_STRING_LITERAL && LA1_0<=RULE_DIGIT)||LA1_0==RULE_INT||(LA1_0>=14 && LA1_0<=15)||(LA1_0>=29 && LA1_0<=30)||(LA1_0>=36 && LA1_0<=39)) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:80:2: ( (lv_dispatch_0_0= ruledispatch_condition ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:80:2: ( (lv_dispatch_0_0= ruledispatch_condition ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:81:1: (lv_dispatch_0_0= ruledispatch_condition )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:81:1: (lv_dispatch_0_0= ruledispatch_condition )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:82:3: lv_dispatch_0_0= ruledispatch_condition
                    {
                     
                    	        newCompositeNode(grammarAccess.getGuardAccess().getDispatchDispatch_conditionParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruledispatch_condition_in_ruleGuard131);
                    lv_dispatch_0_0=ruledispatch_condition();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getGuardRule());
                    	        }
                           		set(
                           			current, 
                           			"dispatch",
                            		lv_dispatch_0_0, 
                            		"dispatch_condition");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:99:6: ( (lv_execute_1_0= ruleexecute_condition ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:99:6: ( (lv_execute_1_0= ruleexecute_condition ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:100:1: (lv_execute_1_0= ruleexecute_condition )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:100:1: (lv_execute_1_0= ruleexecute_condition )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:101:3: lv_execute_1_0= ruleexecute_condition
                    {
                     
                    	        newCompositeNode(grammarAccess.getGuardAccess().getExecuteExecute_conditionParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleexecute_condition_in_ruleGuard158);
                    lv_execute_1_0=ruleexecute_condition();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getGuardRule());
                    	        }
                           		set(
                           			current, 
                           			"execute",
                            		lv_execute_1_0, 
                            		"execute_condition");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuard"


    // $ANTLR start "entryRuleexecute_condition"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:125:1: entryRuleexecute_condition returns [EObject current=null] : iv_ruleexecute_condition= ruleexecute_condition EOF ;
    public final EObject entryRuleexecute_condition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleexecute_condition = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:126:2: (iv_ruleexecute_condition= ruleexecute_condition EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:127:2: iv_ruleexecute_condition= ruleexecute_condition EOF
            {
             newCompositeNode(grammarAccess.getExecute_conditionRule()); 
            pushFollow(FOLLOW_ruleexecute_condition_in_entryRuleexecute_condition194);
            iv_ruleexecute_condition=ruleexecute_condition();

            state._fsp--;

             current =iv_ruleexecute_condition; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleexecute_condition204); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleexecute_condition"


    // $ANTLR start "ruleexecute_condition"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:134:1: ruleexecute_condition returns [EObject current=null] : ( ( (lv_ecname1_0_0= 'otherwise' ) ) | ( (lv_ecname2_1_0= rulebehavior_action_block_timeout_catch ) ) | ( (lv_ecname3_2_0= rulevalue_expression ) ) ) ;
    public final EObject ruleexecute_condition() throws RecognitionException {
        EObject current = null;

        Token lv_ecname1_0_0=null;
        EObject lv_ecname2_1_0 = null;

        EObject lv_ecname3_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:137:28: ( ( ( (lv_ecname1_0_0= 'otherwise' ) ) | ( (lv_ecname2_1_0= rulebehavior_action_block_timeout_catch ) ) | ( (lv_ecname3_2_0= rulevalue_expression ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:138:1: ( ( (lv_ecname1_0_0= 'otherwise' ) ) | ( (lv_ecname2_1_0= rulebehavior_action_block_timeout_catch ) ) | ( (lv_ecname3_2_0= rulevalue_expression ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:138:1: ( ( (lv_ecname1_0_0= 'otherwise' ) ) | ( (lv_ecname2_1_0= rulebehavior_action_block_timeout_catch ) ) | ( (lv_ecname3_2_0= rulevalue_expression ) ) )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt2=1;
                }
                break;
            case 15:
                {
                alt2=2;
                }
                break;
            case RULE_STRING_LITERAL:
            case RULE_DIGIT:
            case RULE_INT:
            case 29:
            case 30:
            case 36:
            case 37:
            case 38:
            case 39:
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:138:2: ( (lv_ecname1_0_0= 'otherwise' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:138:2: ( (lv_ecname1_0_0= 'otherwise' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:139:1: (lv_ecname1_0_0= 'otherwise' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:139:1: (lv_ecname1_0_0= 'otherwise' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:140:3: lv_ecname1_0_0= 'otherwise'
                    {
                    lv_ecname1_0_0=(Token)match(input,14,FOLLOW_14_in_ruleexecute_condition247); 

                            newLeafNode(lv_ecname1_0_0, grammarAccess.getExecute_conditionAccess().getEcname1OtherwiseKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getExecute_conditionRule());
                    	        }
                           		setWithLastConsumed(current, "ecname1", lv_ecname1_0_0, "otherwise");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:154:6: ( (lv_ecname2_1_0= rulebehavior_action_block_timeout_catch ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:154:6: ( (lv_ecname2_1_0= rulebehavior_action_block_timeout_catch ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:155:1: (lv_ecname2_1_0= rulebehavior_action_block_timeout_catch )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:155:1: (lv_ecname2_1_0= rulebehavior_action_block_timeout_catch )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:156:3: lv_ecname2_1_0= rulebehavior_action_block_timeout_catch
                    {
                     
                    	        newCompositeNode(grammarAccess.getExecute_conditionAccess().getEcname2Behavior_action_block_timeout_catchParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulebehavior_action_block_timeout_catch_in_ruleexecute_condition287);
                    lv_ecname2_1_0=rulebehavior_action_block_timeout_catch();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getExecute_conditionRule());
                    	        }
                           		set(
                           			current, 
                           			"ecname2",
                            		lv_ecname2_1_0, 
                            		"behavior_action_block_timeout_catch");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:173:6: ( (lv_ecname3_2_0= rulevalue_expression ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:173:6: ( (lv_ecname3_2_0= rulevalue_expression ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:174:1: (lv_ecname3_2_0= rulevalue_expression )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:174:1: (lv_ecname3_2_0= rulevalue_expression )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:175:3: lv_ecname3_2_0= rulevalue_expression
                    {
                     
                    	        newCompositeNode(grammarAccess.getExecute_conditionAccess().getEcname3Value_expressionParserRuleCall_2_0()); 
                    	    
                    pushFollow(FOLLOW_rulevalue_expression_in_ruleexecute_condition314);
                    lv_ecname3_2_0=rulevalue_expression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getExecute_conditionRule());
                    	        }
                           		set(
                           			current, 
                           			"ecname3",
                            		lv_ecname3_2_0, 
                            		"value_expression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleexecute_condition"


    // $ANTLR start "entryRulebehavior_action_block_timeout_catch"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:199:1: entryRulebehavior_action_block_timeout_catch returns [EObject current=null] : iv_rulebehavior_action_block_timeout_catch= rulebehavior_action_block_timeout_catch EOF ;
    public final EObject entryRulebehavior_action_block_timeout_catch() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebehavior_action_block_timeout_catch = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:200:2: (iv_rulebehavior_action_block_timeout_catch= rulebehavior_action_block_timeout_catch EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:201:2: iv_rulebehavior_action_block_timeout_catch= rulebehavior_action_block_timeout_catch EOF
            {
             newCompositeNode(grammarAccess.getBehavior_action_block_timeout_catchRule()); 
            pushFollow(FOLLOW_rulebehavior_action_block_timeout_catch_in_entryRulebehavior_action_block_timeout_catch350);
            iv_rulebehavior_action_block_timeout_catch=rulebehavior_action_block_timeout_catch();

            state._fsp--;

             current =iv_rulebehavior_action_block_timeout_catch; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebehavior_action_block_timeout_catch360); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebehavior_action_block_timeout_catch"


    // $ANTLR start "rulebehavior_action_block_timeout_catch"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:208:1: rulebehavior_action_block_timeout_catch returns [EObject current=null] : ( (lv_babname_0_0= 'timeout' ) ) ;
    public final EObject rulebehavior_action_block_timeout_catch() throws RecognitionException {
        EObject current = null;

        Token lv_babname_0_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:211:28: ( ( (lv_babname_0_0= 'timeout' ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:212:1: ( (lv_babname_0_0= 'timeout' ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:212:1: ( (lv_babname_0_0= 'timeout' ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:213:1: (lv_babname_0_0= 'timeout' )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:213:1: (lv_babname_0_0= 'timeout' )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:214:3: lv_babname_0_0= 'timeout'
            {
            lv_babname_0_0=(Token)match(input,15,FOLLOW_15_in_rulebehavior_action_block_timeout_catch402); 

                    newLeafNode(lv_babname_0_0, grammarAccess.getBehavior_action_block_timeout_catchAccess().getBabnameTimeoutKeyword_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getBehavior_action_block_timeout_catchRule());
            	        }
                   		addWithLastConsumed(current, "babname", lv_babname_0_0, "timeout");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebehavior_action_block_timeout_catch"


    // $ANTLR start "entryRulevalue_expression"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:235:1: entryRulevalue_expression returns [EObject current=null] : iv_rulevalue_expression= rulevalue_expression EOF ;
    public final EObject entryRulevalue_expression() throws RecognitionException {
        EObject current = null;

        EObject iv_rulevalue_expression = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:236:2: (iv_rulevalue_expression= rulevalue_expression EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:237:2: iv_rulevalue_expression= rulevalue_expression EOF
            {
             newCompositeNode(grammarAccess.getValue_expressionRule()); 
            pushFollow(FOLLOW_rulevalue_expression_in_entryRulevalue_expression450);
            iv_rulevalue_expression=rulevalue_expression();

            state._fsp--;

             current =iv_rulevalue_expression; 
            match(input,EOF,FOLLOW_EOF_in_entryRulevalue_expression460); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulevalue_expression"


    // $ANTLR start "rulevalue_expression"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:244:1: rulevalue_expression returns [EObject current=null] : ( ( (lv_vename1_0_0= rulerelation ) ) ( ( (lv_vename3_1_0= rulelogical_operator ) ) ( (lv_vename2_2_0= rulerelation ) ) )* ) ;
    public final EObject rulevalue_expression() throws RecognitionException {
        EObject current = null;

        EObject lv_vename1_0_0 = null;

        EObject lv_vename3_1_0 = null;

        EObject lv_vename2_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:247:28: ( ( ( (lv_vename1_0_0= rulerelation ) ) ( ( (lv_vename3_1_0= rulelogical_operator ) ) ( (lv_vename2_2_0= rulerelation ) ) )* ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:248:1: ( ( (lv_vename1_0_0= rulerelation ) ) ( ( (lv_vename3_1_0= rulelogical_operator ) ) ( (lv_vename2_2_0= rulerelation ) ) )* )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:248:1: ( ( (lv_vename1_0_0= rulerelation ) ) ( ( (lv_vename3_1_0= rulelogical_operator ) ) ( (lv_vename2_2_0= rulerelation ) ) )* )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:248:2: ( (lv_vename1_0_0= rulerelation ) ) ( ( (lv_vename3_1_0= rulelogical_operator ) ) ( (lv_vename2_2_0= rulerelation ) ) )*
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:248:2: ( (lv_vename1_0_0= rulerelation ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:249:1: (lv_vename1_0_0= rulerelation )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:249:1: (lv_vename1_0_0= rulerelation )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:250:3: lv_vename1_0_0= rulerelation
            {
             
            	        newCompositeNode(grammarAccess.getValue_expressionAccess().getVename1RelationParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_rulerelation_in_rulevalue_expression506);
            lv_vename1_0_0=rulerelation();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getValue_expressionRule());
            	        }
                   		set(
                   			current, 
                   			"vename1",
                    		lv_vename1_0_0, 
                    		"relation");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:266:2: ( ( (lv_vename3_1_0= rulelogical_operator ) ) ( (lv_vename2_2_0= rulerelation ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>=20 && LA3_0<=22)) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:266:3: ( (lv_vename3_1_0= rulelogical_operator ) ) ( (lv_vename2_2_0= rulerelation ) )
            	    {
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:266:3: ( (lv_vename3_1_0= rulelogical_operator ) )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:267:1: (lv_vename3_1_0= rulelogical_operator )
            	    {
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:267:1: (lv_vename3_1_0= rulelogical_operator )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:268:3: lv_vename3_1_0= rulelogical_operator
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getValue_expressionAccess().getVename3Logical_operatorParserRuleCall_1_0_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulelogical_operator_in_rulevalue_expression528);
            	    lv_vename3_1_0=rulelogical_operator();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getValue_expressionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"vename3",
            	            		lv_vename3_1_0, 
            	            		"logical_operator");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }

            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:284:2: ( (lv_vename2_2_0= rulerelation ) )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:285:1: (lv_vename2_2_0= rulerelation )
            	    {
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:285:1: (lv_vename2_2_0= rulerelation )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:286:3: lv_vename2_2_0= rulerelation
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getValue_expressionAccess().getVename2RelationParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulerelation_in_rulevalue_expression549);
            	    lv_vename2_2_0=rulerelation();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getValue_expressionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"vename2",
            	            		lv_vename2_2_0, 
            	            		"relation");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulevalue_expression"


    // $ANTLR start "entryRulerelation"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:310:1: entryRulerelation returns [EObject current=null] : iv_rulerelation= rulerelation EOF ;
    public final EObject entryRulerelation() throws RecognitionException {
        EObject current = null;

        EObject iv_rulerelation = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:311:2: (iv_rulerelation= rulerelation EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:312:2: iv_rulerelation= rulerelation EOF
            {
             newCompositeNode(grammarAccess.getRelationRule()); 
            pushFollow(FOLLOW_rulerelation_in_entryRulerelation587);
            iv_rulerelation=rulerelation();

            state._fsp--;

             current =iv_rulerelation; 
            match(input,EOF,FOLLOW_EOF_in_entryRulerelation597); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulerelation"


    // $ANTLR start "rulerelation"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:319:1: rulerelation returns [EObject current=null] : ( ( (lv_rname1_0_0= rulesimple_expression ) ) ( ( (lv_rname3_1_0= rulerelational_operator ) ) ( (lv_rname2_2_0= rulesimple_expression ) ) )? ) ;
    public final EObject rulerelation() throws RecognitionException {
        EObject current = null;

        EObject lv_rname1_0_0 = null;

        EObject lv_rname3_1_0 = null;

        EObject lv_rname2_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:322:28: ( ( ( (lv_rname1_0_0= rulesimple_expression ) ) ( ( (lv_rname3_1_0= rulerelational_operator ) ) ( (lv_rname2_2_0= rulesimple_expression ) ) )? ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:323:1: ( ( (lv_rname1_0_0= rulesimple_expression ) ) ( ( (lv_rname3_1_0= rulerelational_operator ) ) ( (lv_rname2_2_0= rulesimple_expression ) ) )? )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:323:1: ( ( (lv_rname1_0_0= rulesimple_expression ) ) ( ( (lv_rname3_1_0= rulerelational_operator ) ) ( (lv_rname2_2_0= rulesimple_expression ) ) )? )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:323:2: ( (lv_rname1_0_0= rulesimple_expression ) ) ( ( (lv_rname3_1_0= rulerelational_operator ) ) ( (lv_rname2_2_0= rulesimple_expression ) ) )?
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:323:2: ( (lv_rname1_0_0= rulesimple_expression ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:324:1: (lv_rname1_0_0= rulesimple_expression )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:324:1: (lv_rname1_0_0= rulesimple_expression )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:325:3: lv_rname1_0_0= rulesimple_expression
            {
             
            	        newCompositeNode(grammarAccess.getRelationAccess().getRname1Simple_expressionParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_rulesimple_expression_in_rulerelation643);
            lv_rname1_0_0=rulesimple_expression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRelationRule());
            	        }
                   		set(
                   			current, 
                   			"rname1",
                    		lv_rname1_0_0, 
                    		"simple_expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:341:2: ( ( (lv_rname3_1_0= rulerelational_operator ) ) ( (lv_rname2_2_0= rulesimple_expression ) ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( ((LA4_0>=23 && LA4_0<=28)) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:341:3: ( (lv_rname3_1_0= rulerelational_operator ) ) ( (lv_rname2_2_0= rulesimple_expression ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:341:3: ( (lv_rname3_1_0= rulerelational_operator ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:342:1: (lv_rname3_1_0= rulerelational_operator )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:342:1: (lv_rname3_1_0= rulerelational_operator )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:343:3: lv_rname3_1_0= rulerelational_operator
                    {
                     
                    	        newCompositeNode(grammarAccess.getRelationAccess().getRname3Relational_operatorParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FOLLOW_rulerelational_operator_in_rulerelation665);
                    lv_rname3_1_0=rulerelational_operator();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getRelationRule());
                    	        }
                           		add(
                           			current, 
                           			"rname3",
                            		lv_rname3_1_0, 
                            		"relational_operator");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:359:2: ( (lv_rname2_2_0= rulesimple_expression ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:360:1: (lv_rname2_2_0= rulesimple_expression )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:360:1: (lv_rname2_2_0= rulesimple_expression )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:361:3: lv_rname2_2_0= rulesimple_expression
                    {
                     
                    	        newCompositeNode(grammarAccess.getRelationAccess().getRname2Simple_expressionParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulesimple_expression_in_rulerelation686);
                    lv_rname2_2_0=rulesimple_expression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getRelationRule());
                    	        }
                           		add(
                           			current, 
                           			"rname2",
                            		lv_rname2_2_0, 
                            		"simple_expression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulerelation"


    // $ANTLR start "entryRulesimple_expression"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:385:1: entryRulesimple_expression returns [EObject current=null] : iv_rulesimple_expression= rulesimple_expression EOF ;
    public final EObject entryRulesimple_expression() throws RecognitionException {
        EObject current = null;

        EObject iv_rulesimple_expression = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:386:2: (iv_rulesimple_expression= rulesimple_expression EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:387:2: iv_rulesimple_expression= rulesimple_expression EOF
            {
             newCompositeNode(grammarAccess.getSimple_expressionRule()); 
            pushFollow(FOLLOW_rulesimple_expression_in_entryRulesimple_expression724);
            iv_rulesimple_expression=rulesimple_expression();

            state._fsp--;

             current =iv_rulesimple_expression; 
            match(input,EOF,FOLLOW_EOF_in_entryRulesimple_expression734); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulesimple_expression"


    // $ANTLR start "rulesimple_expression"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:394:1: rulesimple_expression returns [EObject current=null] : ( ( (lv_sename2_0_0= ruleunary_adding_operator ) )? ( (lv_sename1_1_0= ruleterm ) ) ( ( (lv_sename3_2_0= rulebinary_adding_operator ) ) ( (lv_sename4_3_0= ruleterm ) ) )* ) ;
    public final EObject rulesimple_expression() throws RecognitionException {
        EObject current = null;

        EObject lv_sename2_0_0 = null;

        EObject lv_sename1_1_0 = null;

        EObject lv_sename3_2_0 = null;

        EObject lv_sename4_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:397:28: ( ( ( (lv_sename2_0_0= ruleunary_adding_operator ) )? ( (lv_sename1_1_0= ruleterm ) ) ( ( (lv_sename3_2_0= rulebinary_adding_operator ) ) ( (lv_sename4_3_0= ruleterm ) ) )* ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:398:1: ( ( (lv_sename2_0_0= ruleunary_adding_operator ) )? ( (lv_sename1_1_0= ruleterm ) ) ( ( (lv_sename3_2_0= rulebinary_adding_operator ) ) ( (lv_sename4_3_0= ruleterm ) ) )* )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:398:1: ( ( (lv_sename2_0_0= ruleunary_adding_operator ) )? ( (lv_sename1_1_0= ruleterm ) ) ( ( (lv_sename3_2_0= rulebinary_adding_operator ) ) ( (lv_sename4_3_0= ruleterm ) ) )* )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:398:2: ( (lv_sename2_0_0= ruleunary_adding_operator ) )? ( (lv_sename1_1_0= ruleterm ) ) ( ( (lv_sename3_2_0= rulebinary_adding_operator ) ) ( (lv_sename4_3_0= ruleterm ) ) )*
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:398:2: ( (lv_sename2_0_0= ruleunary_adding_operator ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( ((LA5_0>=29 && LA5_0<=30)) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:399:1: (lv_sename2_0_0= ruleunary_adding_operator )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:399:1: (lv_sename2_0_0= ruleunary_adding_operator )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:400:3: lv_sename2_0_0= ruleunary_adding_operator
                    {
                     
                    	        newCompositeNode(grammarAccess.getSimple_expressionAccess().getSename2Unary_adding_operatorParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleunary_adding_operator_in_rulesimple_expression780);
                    lv_sename2_0_0=ruleunary_adding_operator();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSimple_expressionRule());
                    	        }
                           		set(
                           			current, 
                           			"sename2",
                            		lv_sename2_0_0, 
                            		"unary_adding_operator");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:416:3: ( (lv_sename1_1_0= ruleterm ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:417:1: (lv_sename1_1_0= ruleterm )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:417:1: (lv_sename1_1_0= ruleterm )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:418:3: lv_sename1_1_0= ruleterm
            {
             
            	        newCompositeNode(grammarAccess.getSimple_expressionAccess().getSename1TermParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleterm_in_rulesimple_expression802);
            lv_sename1_1_0=ruleterm();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSimple_expressionRule());
            	        }
                   		set(
                   			current, 
                   			"sename1",
                    		lv_sename1_1_0, 
                    		"term");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:434:2: ( ( (lv_sename3_2_0= rulebinary_adding_operator ) ) ( (lv_sename4_3_0= ruleterm ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>=29 && LA6_0<=30)) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:434:3: ( (lv_sename3_2_0= rulebinary_adding_operator ) ) ( (lv_sename4_3_0= ruleterm ) )
            	    {
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:434:3: ( (lv_sename3_2_0= rulebinary_adding_operator ) )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:435:1: (lv_sename3_2_0= rulebinary_adding_operator )
            	    {
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:435:1: (lv_sename3_2_0= rulebinary_adding_operator )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:436:3: lv_sename3_2_0= rulebinary_adding_operator
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getSimple_expressionAccess().getSename3Binary_adding_operatorParserRuleCall_2_0_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulebinary_adding_operator_in_rulesimple_expression824);
            	    lv_sename3_2_0=rulebinary_adding_operator();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getSimple_expressionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"sename3",
            	            		lv_sename3_2_0, 
            	            		"binary_adding_operator");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }

            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:452:2: ( (lv_sename4_3_0= ruleterm ) )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:453:1: (lv_sename4_3_0= ruleterm )
            	    {
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:453:1: (lv_sename4_3_0= ruleterm )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:454:3: lv_sename4_3_0= ruleterm
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getSimple_expressionAccess().getSename4TermParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleterm_in_rulesimple_expression845);
            	    lv_sename4_3_0=ruleterm();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getSimple_expressionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"sename4",
            	            		lv_sename4_3_0, 
            	            		"term");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulesimple_expression"


    // $ANTLR start "entryRuleterm"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:478:1: entryRuleterm returns [EObject current=null] : iv_ruleterm= ruleterm EOF ;
    public final EObject entryRuleterm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleterm = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:479:2: (iv_ruleterm= ruleterm EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:480:2: iv_ruleterm= ruleterm EOF
            {
             newCompositeNode(grammarAccess.getTermRule()); 
            pushFollow(FOLLOW_ruleterm_in_entryRuleterm883);
            iv_ruleterm=ruleterm();

            state._fsp--;

             current =iv_ruleterm; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleterm893); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleterm"


    // $ANTLR start "ruleterm"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:487:1: ruleterm returns [EObject current=null] : ( ( (lv_tname1_0_0= rulefactor ) ) ( ( (lv_tname3_1_0= rulemultiplying_operator ) ) ( (lv_tname2_2_0= rulefactor ) ) )* ) ;
    public final EObject ruleterm() throws RecognitionException {
        EObject current = null;

        EObject lv_tname1_0_0 = null;

        EObject lv_tname3_1_0 = null;

        EObject lv_tname2_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:490:28: ( ( ( (lv_tname1_0_0= rulefactor ) ) ( ( (lv_tname3_1_0= rulemultiplying_operator ) ) ( (lv_tname2_2_0= rulefactor ) ) )* ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:491:1: ( ( (lv_tname1_0_0= rulefactor ) ) ( ( (lv_tname3_1_0= rulemultiplying_operator ) ) ( (lv_tname2_2_0= rulefactor ) ) )* )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:491:1: ( ( (lv_tname1_0_0= rulefactor ) ) ( ( (lv_tname3_1_0= rulemultiplying_operator ) ) ( (lv_tname2_2_0= rulefactor ) ) )* )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:491:2: ( (lv_tname1_0_0= rulefactor ) ) ( ( (lv_tname3_1_0= rulemultiplying_operator ) ) ( (lv_tname2_2_0= rulefactor ) ) )*
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:491:2: ( (lv_tname1_0_0= rulefactor ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:492:1: (lv_tname1_0_0= rulefactor )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:492:1: (lv_tname1_0_0= rulefactor )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:493:3: lv_tname1_0_0= rulefactor
            {
             
            	        newCompositeNode(grammarAccess.getTermAccess().getTname1FactorParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_rulefactor_in_ruleterm939);
            lv_tname1_0_0=rulefactor();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTermRule());
            	        }
                   		set(
                   			current, 
                   			"tname1",
                    		lv_tname1_0_0, 
                    		"factor");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:509:2: ( ( (lv_tname3_1_0= rulemultiplying_operator ) ) ( (lv_tname2_2_0= rulefactor ) ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>=31 && LA7_0<=34)) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:509:3: ( (lv_tname3_1_0= rulemultiplying_operator ) ) ( (lv_tname2_2_0= rulefactor ) )
            	    {
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:509:3: ( (lv_tname3_1_0= rulemultiplying_operator ) )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:510:1: (lv_tname3_1_0= rulemultiplying_operator )
            	    {
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:510:1: (lv_tname3_1_0= rulemultiplying_operator )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:511:3: lv_tname3_1_0= rulemultiplying_operator
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getTermAccess().getTname3Multiplying_operatorParserRuleCall_1_0_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulemultiplying_operator_in_ruleterm961);
            	    lv_tname3_1_0=rulemultiplying_operator();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getTermRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"tname3",
            	            		lv_tname3_1_0, 
            	            		"multiplying_operator");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }

            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:527:2: ( (lv_tname2_2_0= rulefactor ) )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:528:1: (lv_tname2_2_0= rulefactor )
            	    {
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:528:1: (lv_tname2_2_0= rulefactor )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:529:3: lv_tname2_2_0= rulefactor
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getTermAccess().getTname2FactorParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulefactor_in_ruleterm982);
            	    lv_tname2_2_0=rulefactor();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getTermRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"tname2",
            	            		lv_tname2_2_0, 
            	            		"factor");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleterm"


    // $ANTLR start "entryRulefactor"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:553:1: entryRulefactor returns [EObject current=null] : iv_rulefactor= rulefactor EOF ;
    public final EObject entryRulefactor() throws RecognitionException {
        EObject current = null;

        EObject iv_rulefactor = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:554:2: (iv_rulefactor= rulefactor EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:555:2: iv_rulefactor= rulefactor EOF
            {
             newCompositeNode(grammarAccess.getFactorRule()); 
            pushFollow(FOLLOW_rulefactor_in_entryRulefactor1020);
            iv_rulefactor=rulefactor();

            state._fsp--;

             current =iv_rulefactor; 
            match(input,EOF,FOLLOW_EOF_in_entryRulefactor1030); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulefactor"


    // $ANTLR start "rulefactor"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:562:1: rulefactor returns [EObject current=null] : ( ( ( (lv_fname1_0_0= rulevalue ) ) ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )? ) | ( ( (lv_fname4_3_0= ruleunary_numeric_operator ) ) ( (lv_fname5_4_0= rulevalue ) ) ) | ( ( (lv_fname6_5_0= ruleunary_boolean_operator ) ) ( (lv_fname7_6_0= rulevalue ) ) ) ) ;
    public final EObject rulefactor() throws RecognitionException {
        EObject current = null;

        EObject lv_fname1_0_0 = null;

        EObject lv_fname3_1_0 = null;

        EObject lv_fname2_2_0 = null;

        EObject lv_fname4_3_0 = null;

        EObject lv_fname5_4_0 = null;

        EObject lv_fname6_5_0 = null;

        EObject lv_fname7_6_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:565:28: ( ( ( ( (lv_fname1_0_0= rulevalue ) ) ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )? ) | ( ( (lv_fname4_3_0= ruleunary_numeric_operator ) ) ( (lv_fname5_4_0= rulevalue ) ) ) | ( ( (lv_fname6_5_0= ruleunary_boolean_operator ) ) ( (lv_fname7_6_0= rulevalue ) ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:566:1: ( ( ( (lv_fname1_0_0= rulevalue ) ) ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )? ) | ( ( (lv_fname4_3_0= ruleunary_numeric_operator ) ) ( (lv_fname5_4_0= rulevalue ) ) ) | ( ( (lv_fname6_5_0= ruleunary_boolean_operator ) ) ( (lv_fname7_6_0= rulevalue ) ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:566:1: ( ( ( (lv_fname1_0_0= rulevalue ) ) ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )? ) | ( ( (lv_fname4_3_0= ruleunary_numeric_operator ) ) ( (lv_fname5_4_0= rulevalue ) ) ) | ( ( (lv_fname6_5_0= ruleunary_boolean_operator ) ) ( (lv_fname7_6_0= rulevalue ) ) ) )
            int alt9=3;
            switch ( input.LA(1) ) {
            case RULE_STRING_LITERAL:
            case RULE_DIGIT:
            case RULE_INT:
            case 38:
            case 39:
                {
                alt9=1;
                }
                break;
            case 36:
                {
                alt9=2;
                }
                break;
            case 37:
                {
                alt9=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:566:2: ( ( (lv_fname1_0_0= rulevalue ) ) ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )? )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:566:2: ( ( (lv_fname1_0_0= rulevalue ) ) ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )? )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:566:3: ( (lv_fname1_0_0= rulevalue ) ) ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )?
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:566:3: ( (lv_fname1_0_0= rulevalue ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:567:1: (lv_fname1_0_0= rulevalue )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:567:1: (lv_fname1_0_0= rulevalue )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:568:3: lv_fname1_0_0= rulevalue
                    {
                     
                    	        newCompositeNode(grammarAccess.getFactorAccess().getFname1ValueParserRuleCall_0_0_0()); 
                    	    
                    pushFollow(FOLLOW_rulevalue_in_rulefactor1077);
                    lv_fname1_0_0=rulevalue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFactorRule());
                    	        }
                           		set(
                           			current, 
                           			"fname1",
                            		lv_fname1_0_0, 
                            		"value");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:584:2: ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )?
                    int alt8=2;
                    int LA8_0 = input.LA(1);

                    if ( (LA8_0==35) ) {
                        alt8=1;
                    }
                    switch (alt8) {
                        case 1 :
                            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:584:3: ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) )
                            {
                            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:584:3: ( (lv_fname3_1_0= rulebinary_numeric_operator ) )
                            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:585:1: (lv_fname3_1_0= rulebinary_numeric_operator )
                            {
                            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:585:1: (lv_fname3_1_0= rulebinary_numeric_operator )
                            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:586:3: lv_fname3_1_0= rulebinary_numeric_operator
                            {
                             
                            	        newCompositeNode(grammarAccess.getFactorAccess().getFname3Binary_numeric_operatorParserRuleCall_0_1_0_0()); 
                            	    
                            pushFollow(FOLLOW_rulebinary_numeric_operator_in_rulefactor1099);
                            lv_fname3_1_0=rulebinary_numeric_operator();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getFactorRule());
                            	        }
                                   		add(
                                   			current, 
                                   			"fname3",
                                    		lv_fname3_1_0, 
                                    		"binary_numeric_operator");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }

                            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:602:2: ( (lv_fname2_2_0= rulevalue ) )
                            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:603:1: (lv_fname2_2_0= rulevalue )
                            {
                            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:603:1: (lv_fname2_2_0= rulevalue )
                            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:604:3: lv_fname2_2_0= rulevalue
                            {
                             
                            	        newCompositeNode(grammarAccess.getFactorAccess().getFname2ValueParserRuleCall_0_1_1_0()); 
                            	    
                            pushFollow(FOLLOW_rulevalue_in_rulefactor1120);
                            lv_fname2_2_0=rulevalue();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getFactorRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"fname2",
                                    		lv_fname2_2_0, 
                                    		"value");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:621:6: ( ( (lv_fname4_3_0= ruleunary_numeric_operator ) ) ( (lv_fname5_4_0= rulevalue ) ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:621:6: ( ( (lv_fname4_3_0= ruleunary_numeric_operator ) ) ( (lv_fname5_4_0= rulevalue ) ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:621:7: ( (lv_fname4_3_0= ruleunary_numeric_operator ) ) ( (lv_fname5_4_0= rulevalue ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:621:7: ( (lv_fname4_3_0= ruleunary_numeric_operator ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:622:1: (lv_fname4_3_0= ruleunary_numeric_operator )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:622:1: (lv_fname4_3_0= ruleunary_numeric_operator )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:623:3: lv_fname4_3_0= ruleunary_numeric_operator
                    {
                     
                    	        newCompositeNode(grammarAccess.getFactorAccess().getFname4Unary_numeric_operatorParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleunary_numeric_operator_in_rulefactor1151);
                    lv_fname4_3_0=ruleunary_numeric_operator();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFactorRule());
                    	        }
                           		set(
                           			current, 
                           			"fname4",
                            		lv_fname4_3_0, 
                            		"unary_numeric_operator");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:639:2: ( (lv_fname5_4_0= rulevalue ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:640:1: (lv_fname5_4_0= rulevalue )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:640:1: (lv_fname5_4_0= rulevalue )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:641:3: lv_fname5_4_0= rulevalue
                    {
                     
                    	        newCompositeNode(grammarAccess.getFactorAccess().getFname5ValueParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulevalue_in_rulefactor1172);
                    lv_fname5_4_0=rulevalue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFactorRule());
                    	        }
                           		set(
                           			current, 
                           			"fname5",
                            		lv_fname5_4_0, 
                            		"value");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:658:6: ( ( (lv_fname6_5_0= ruleunary_boolean_operator ) ) ( (lv_fname7_6_0= rulevalue ) ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:658:6: ( ( (lv_fname6_5_0= ruleunary_boolean_operator ) ) ( (lv_fname7_6_0= rulevalue ) ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:658:7: ( (lv_fname6_5_0= ruleunary_boolean_operator ) ) ( (lv_fname7_6_0= rulevalue ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:658:7: ( (lv_fname6_5_0= ruleunary_boolean_operator ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:659:1: (lv_fname6_5_0= ruleunary_boolean_operator )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:659:1: (lv_fname6_5_0= ruleunary_boolean_operator )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:660:3: lv_fname6_5_0= ruleunary_boolean_operator
                    {
                     
                    	        newCompositeNode(grammarAccess.getFactorAccess().getFname6Unary_boolean_operatorParserRuleCall_2_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleunary_boolean_operator_in_rulefactor1201);
                    lv_fname6_5_0=ruleunary_boolean_operator();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFactorRule());
                    	        }
                           		set(
                           			current, 
                           			"fname6",
                            		lv_fname6_5_0, 
                            		"unary_boolean_operator");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:676:2: ( (lv_fname7_6_0= rulevalue ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:677:1: (lv_fname7_6_0= rulevalue )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:677:1: (lv_fname7_6_0= rulevalue )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:678:3: lv_fname7_6_0= rulevalue
                    {
                     
                    	        newCompositeNode(grammarAccess.getFactorAccess().getFname7ValueParserRuleCall_2_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulevalue_in_rulefactor1222);
                    lv_fname7_6_0=rulevalue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFactorRule());
                    	        }
                           		set(
                           			current, 
                           			"fname7",
                            		lv_fname7_6_0, 
                            		"value");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulefactor"


    // $ANTLR start "entryRulevalue"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:702:1: entryRulevalue returns [EObject current=null] : iv_rulevalue= rulevalue EOF ;
    public final EObject entryRulevalue() throws RecognitionException {
        EObject current = null;

        EObject iv_rulevalue = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:703:2: (iv_rulevalue= rulevalue EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:704:2: iv_rulevalue= rulevalue EOF
            {
             newCompositeNode(grammarAccess.getValueRule()); 
            pushFollow(FOLLOW_rulevalue_in_entryRulevalue1259);
            iv_rulevalue=rulevalue();

            state._fsp--;

             current =iv_rulevalue; 
            match(input,EOF,FOLLOW_EOF_in_entryRulevalue1269); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulevalue"


    // $ANTLR start "rulevalue"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:711:1: rulevalue returns [EObject current=null] : ( ( (lv_vname1_0_0= rulevalue_variable ) ) | ( (lv_vname2_1_0= rulevalue_constant ) ) ) ;
    public final EObject rulevalue() throws RecognitionException {
        EObject current = null;

        EObject lv_vname1_0_0 = null;

        EObject lv_vname2_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:714:28: ( ( ( (lv_vname1_0_0= rulevalue_variable ) ) | ( (lv_vname2_1_0= rulevalue_constant ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:715:1: ( ( (lv_vname1_0_0= rulevalue_variable ) ) | ( (lv_vname2_1_0= rulevalue_constant ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:715:1: ( ( (lv_vname1_0_0= rulevalue_variable ) ) | ( (lv_vname2_1_0= rulevalue_constant ) ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_STRING_LITERAL) ) {
                int LA10_1 = input.LA(2);

                if ( (LA10_1==EOF||(LA10_1>=16 && LA10_1<=18)||(LA10_1>=20 && LA10_1<=35)) ) {
                    alt10=1;
                }
                else if ( (LA10_1==19) ) {
                    alt10=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 10, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA10_0==RULE_DIGIT||LA10_0==RULE_INT||(LA10_0>=38 && LA10_0<=39)) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:715:2: ( (lv_vname1_0_0= rulevalue_variable ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:715:2: ( (lv_vname1_0_0= rulevalue_variable ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:716:1: (lv_vname1_0_0= rulevalue_variable )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:716:1: (lv_vname1_0_0= rulevalue_variable )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:717:3: lv_vname1_0_0= rulevalue_variable
                    {
                     
                    	        newCompositeNode(grammarAccess.getValueAccess().getVname1Value_variableParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_rulevalue_variable_in_rulevalue1315);
                    lv_vname1_0_0=rulevalue_variable();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getValueRule());
                    	        }
                           		set(
                           			current, 
                           			"vname1",
                            		lv_vname1_0_0, 
                            		"value_variable");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:734:6: ( (lv_vname2_1_0= rulevalue_constant ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:734:6: ( (lv_vname2_1_0= rulevalue_constant ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:735:1: (lv_vname2_1_0= rulevalue_constant )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:735:1: (lv_vname2_1_0= rulevalue_constant )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:736:3: lv_vname2_1_0= rulevalue_constant
                    {
                     
                    	        newCompositeNode(grammarAccess.getValueAccess().getVname2Value_constantParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulevalue_constant_in_rulevalue1342);
                    lv_vname2_1_0=rulevalue_constant();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getValueRule());
                    	        }
                           		set(
                           			current, 
                           			"vname2",
                            		lv_vname2_1_0, 
                            		"value_constant");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulevalue"


    // $ANTLR start "entryRulevalue_variable"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:760:1: entryRulevalue_variable returns [EObject current=null] : iv_rulevalue_variable= rulevalue_variable EOF ;
    public final EObject entryRulevalue_variable() throws RecognitionException {
        EObject current = null;

        EObject iv_rulevalue_variable = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:761:2: (iv_rulevalue_variable= rulevalue_variable EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:762:2: iv_rulevalue_variable= rulevalue_variable EOF
            {
             newCompositeNode(grammarAccess.getValue_variableRule()); 
            pushFollow(FOLLOW_rulevalue_variable_in_entryRulevalue_variable1378);
            iv_rulevalue_variable=rulevalue_variable();

            state._fsp--;

             current =iv_rulevalue_variable; 
            match(input,EOF,FOLLOW_EOF_in_entryRulevalue_variable1388); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulevalue_variable"


    // $ANTLR start "rulevalue_variable"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:769:1: rulevalue_variable returns [EObject current=null] : ( ( (lv_vvname0_0_0= RULE_STRING_LITERAL ) ) ( ( (lv_vvname1_1_0= '?' ) ) | ( (lv_vvname2_2_0= 'count' ) ) | ( (lv_vvname3_3_0= 'fresh' ) ) )? ) ;
    public final EObject rulevalue_variable() throws RecognitionException {
        EObject current = null;

        Token lv_vvname0_0_0=null;
        Token lv_vvname1_1_0=null;
        Token lv_vvname2_2_0=null;
        Token lv_vvname3_3_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:772:28: ( ( ( (lv_vvname0_0_0= RULE_STRING_LITERAL ) ) ( ( (lv_vvname1_1_0= '?' ) ) | ( (lv_vvname2_2_0= 'count' ) ) | ( (lv_vvname3_3_0= 'fresh' ) ) )? ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:773:1: ( ( (lv_vvname0_0_0= RULE_STRING_LITERAL ) ) ( ( (lv_vvname1_1_0= '?' ) ) | ( (lv_vvname2_2_0= 'count' ) ) | ( (lv_vvname3_3_0= 'fresh' ) ) )? )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:773:1: ( ( (lv_vvname0_0_0= RULE_STRING_LITERAL ) ) ( ( (lv_vvname1_1_0= '?' ) ) | ( (lv_vvname2_2_0= 'count' ) ) | ( (lv_vvname3_3_0= 'fresh' ) ) )? )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:773:2: ( (lv_vvname0_0_0= RULE_STRING_LITERAL ) ) ( ( (lv_vvname1_1_0= '?' ) ) | ( (lv_vvname2_2_0= 'count' ) ) | ( (lv_vvname3_3_0= 'fresh' ) ) )?
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:773:2: ( (lv_vvname0_0_0= RULE_STRING_LITERAL ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:774:1: (lv_vvname0_0_0= RULE_STRING_LITERAL )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:774:1: (lv_vvname0_0_0= RULE_STRING_LITERAL )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:775:3: lv_vvname0_0_0= RULE_STRING_LITERAL
            {
            lv_vvname0_0_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulevalue_variable1430); 

            			newLeafNode(lv_vvname0_0_0, grammarAccess.getValue_variableAccess().getVvname0String_literalTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getValue_variableRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"vvname0",
                    		lv_vvname0_0_0, 
                    		"string_literal");
            	    

            }


            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:791:2: ( ( (lv_vvname1_1_0= '?' ) ) | ( (lv_vvname2_2_0= 'count' ) ) | ( (lv_vvname3_3_0= 'fresh' ) ) )?
            int alt11=4;
            switch ( input.LA(1) ) {
                case 16:
                    {
                    alt11=1;
                    }
                    break;
                case 17:
                    {
                    alt11=2;
                    }
                    break;
                case 18:
                    {
                    alt11=3;
                    }
                    break;
            }

            switch (alt11) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:791:3: ( (lv_vvname1_1_0= '?' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:791:3: ( (lv_vvname1_1_0= '?' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:792:1: (lv_vvname1_1_0= '?' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:792:1: (lv_vvname1_1_0= '?' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:793:3: lv_vvname1_1_0= '?'
                    {
                    lv_vvname1_1_0=(Token)match(input,16,FOLLOW_16_in_rulevalue_variable1454); 

                            newLeafNode(lv_vvname1_1_0, grammarAccess.getValue_variableAccess().getVvname1QuestionMarkKeyword_1_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getValue_variableRule());
                    	        }
                           		setWithLastConsumed(current, "vvname1", lv_vvname1_1_0, "?");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:807:6: ( (lv_vvname2_2_0= 'count' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:807:6: ( (lv_vvname2_2_0= 'count' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:808:1: (lv_vvname2_2_0= 'count' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:808:1: (lv_vvname2_2_0= 'count' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:809:3: lv_vvname2_2_0= 'count'
                    {
                    lv_vvname2_2_0=(Token)match(input,17,FOLLOW_17_in_rulevalue_variable1491); 

                            newLeafNode(lv_vvname2_2_0, grammarAccess.getValue_variableAccess().getVvname2CountKeyword_1_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getValue_variableRule());
                    	        }
                           		setWithLastConsumed(current, "vvname2", lv_vvname2_2_0, "count");
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:823:6: ( (lv_vvname3_3_0= 'fresh' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:823:6: ( (lv_vvname3_3_0= 'fresh' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:824:1: (lv_vvname3_3_0= 'fresh' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:824:1: (lv_vvname3_3_0= 'fresh' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:825:3: lv_vvname3_3_0= 'fresh'
                    {
                    lv_vvname3_3_0=(Token)match(input,18,FOLLOW_18_in_rulevalue_variable1528); 

                            newLeafNode(lv_vvname3_3_0, grammarAccess.getValue_variableAccess().getVvname3FreshKeyword_1_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getValue_variableRule());
                    	        }
                           		setWithLastConsumed(current, "vvname3", lv_vvname3_3_0, "fresh");
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulevalue_variable"


    // $ANTLR start "entryRulevalue_constant"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:846:1: entryRulevalue_constant returns [EObject current=null] : iv_rulevalue_constant= rulevalue_constant EOF ;
    public final EObject entryRulevalue_constant() throws RecognitionException {
        EObject current = null;

        EObject iv_rulevalue_constant = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:847:2: (iv_rulevalue_constant= rulevalue_constant EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:848:2: iv_rulevalue_constant= rulevalue_constant EOF
            {
             newCompositeNode(grammarAccess.getValue_constantRule()); 
            pushFollow(FOLLOW_rulevalue_constant_in_entryRulevalue_constant1579);
            iv_rulevalue_constant=rulevalue_constant();

            state._fsp--;

             current =iv_rulevalue_constant; 
            match(input,EOF,FOLLOW_EOF_in_entryRulevalue_constant1589); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulevalue_constant"


    // $ANTLR start "rulevalue_constant"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:855:1: rulevalue_constant returns [EObject current=null] : ( ( (lv_vcname1_0_0= ruleboolean_literal ) ) | ( (lv_vcname2_1_0= rulenumeric_literal ) ) | ( ( (lv_vcname3_2_0= RULE_STRING_LITERAL ) ) otherlv_3= '::' ( (lv_vcname4_4_0= RULE_STRING_LITERAL ) ) ) ) ;
    public final EObject rulevalue_constant() throws RecognitionException {
        EObject current = null;

        Token lv_vcname3_2_0=null;
        Token otherlv_3=null;
        Token lv_vcname4_4_0=null;
        EObject lv_vcname1_0_0 = null;

        EObject lv_vcname2_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:858:28: ( ( ( (lv_vcname1_0_0= ruleboolean_literal ) ) | ( (lv_vcname2_1_0= rulenumeric_literal ) ) | ( ( (lv_vcname3_2_0= RULE_STRING_LITERAL ) ) otherlv_3= '::' ( (lv_vcname4_4_0= RULE_STRING_LITERAL ) ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:859:1: ( ( (lv_vcname1_0_0= ruleboolean_literal ) ) | ( (lv_vcname2_1_0= rulenumeric_literal ) ) | ( ( (lv_vcname3_2_0= RULE_STRING_LITERAL ) ) otherlv_3= '::' ( (lv_vcname4_4_0= RULE_STRING_LITERAL ) ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:859:1: ( ( (lv_vcname1_0_0= ruleboolean_literal ) ) | ( (lv_vcname2_1_0= rulenumeric_literal ) ) | ( ( (lv_vcname3_2_0= RULE_STRING_LITERAL ) ) otherlv_3= '::' ( (lv_vcname4_4_0= RULE_STRING_LITERAL ) ) ) )
            int alt12=3;
            switch ( input.LA(1) ) {
            case 38:
            case 39:
                {
                alt12=1;
                }
                break;
            case RULE_DIGIT:
            case RULE_INT:
                {
                alt12=2;
                }
                break;
            case RULE_STRING_LITERAL:
                {
                alt12=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:859:2: ( (lv_vcname1_0_0= ruleboolean_literal ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:859:2: ( (lv_vcname1_0_0= ruleboolean_literal ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:860:1: (lv_vcname1_0_0= ruleboolean_literal )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:860:1: (lv_vcname1_0_0= ruleboolean_literal )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:861:3: lv_vcname1_0_0= ruleboolean_literal
                    {
                     
                    	        newCompositeNode(grammarAccess.getValue_constantAccess().getVcname1Boolean_literalParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleboolean_literal_in_rulevalue_constant1635);
                    lv_vcname1_0_0=ruleboolean_literal();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getValue_constantRule());
                    	        }
                           		set(
                           			current, 
                           			"vcname1",
                            		lv_vcname1_0_0, 
                            		"boolean_literal");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:878:6: ( (lv_vcname2_1_0= rulenumeric_literal ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:878:6: ( (lv_vcname2_1_0= rulenumeric_literal ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:879:1: (lv_vcname2_1_0= rulenumeric_literal )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:879:1: (lv_vcname2_1_0= rulenumeric_literal )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:880:3: lv_vcname2_1_0= rulenumeric_literal
                    {
                     
                    	        newCompositeNode(grammarAccess.getValue_constantAccess().getVcname2Numeric_literalParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulenumeric_literal_in_rulevalue_constant1662);
                    lv_vcname2_1_0=rulenumeric_literal();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getValue_constantRule());
                    	        }
                           		set(
                           			current, 
                           			"vcname2",
                            		lv_vcname2_1_0, 
                            		"numeric_literal");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:897:6: ( ( (lv_vcname3_2_0= RULE_STRING_LITERAL ) ) otherlv_3= '::' ( (lv_vcname4_4_0= RULE_STRING_LITERAL ) ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:897:6: ( ( (lv_vcname3_2_0= RULE_STRING_LITERAL ) ) otherlv_3= '::' ( (lv_vcname4_4_0= RULE_STRING_LITERAL ) ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:897:7: ( (lv_vcname3_2_0= RULE_STRING_LITERAL ) ) otherlv_3= '::' ( (lv_vcname4_4_0= RULE_STRING_LITERAL ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:897:7: ( (lv_vcname3_2_0= RULE_STRING_LITERAL ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:898:1: (lv_vcname3_2_0= RULE_STRING_LITERAL )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:898:1: (lv_vcname3_2_0= RULE_STRING_LITERAL )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:899:3: lv_vcname3_2_0= RULE_STRING_LITERAL
                    {
                    lv_vcname3_2_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulevalue_constant1686); 

                    			newLeafNode(lv_vcname3_2_0, grammarAccess.getValue_constantAccess().getVcname3String_literalTerminalRuleCall_2_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getValue_constantRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"vcname3",
                            		lv_vcname3_2_0, 
                            		"string_literal");
                    	    

                    }


                    }

                    otherlv_3=(Token)match(input,19,FOLLOW_19_in_rulevalue_constant1703); 

                        	newLeafNode(otherlv_3, grammarAccess.getValue_constantAccess().getColonColonKeyword_2_1());
                        
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:919:1: ( (lv_vcname4_4_0= RULE_STRING_LITERAL ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:920:1: (lv_vcname4_4_0= RULE_STRING_LITERAL )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:920:1: (lv_vcname4_4_0= RULE_STRING_LITERAL )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:921:3: lv_vcname4_4_0= RULE_STRING_LITERAL
                    {
                    lv_vcname4_4_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulevalue_constant1720); 

                    			newLeafNode(lv_vcname4_4_0, grammarAccess.getValue_constantAccess().getVcname4String_literalTerminalRuleCall_2_2_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getValue_constantRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"vcname4",
                            		lv_vcname4_4_0, 
                            		"string_literal");
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulevalue_constant"


    // $ANTLR start "entryRulelogical_operator"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:945:1: entryRulelogical_operator returns [EObject current=null] : iv_rulelogical_operator= rulelogical_operator EOF ;
    public final EObject entryRulelogical_operator() throws RecognitionException {
        EObject current = null;

        EObject iv_rulelogical_operator = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:946:2: (iv_rulelogical_operator= rulelogical_operator EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:947:2: iv_rulelogical_operator= rulelogical_operator EOF
            {
             newCompositeNode(grammarAccess.getLogical_operatorRule()); 
            pushFollow(FOLLOW_rulelogical_operator_in_entryRulelogical_operator1762);
            iv_rulelogical_operator=rulelogical_operator();

            state._fsp--;

             current =iv_rulelogical_operator; 
            match(input,EOF,FOLLOW_EOF_in_entryRulelogical_operator1772); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulelogical_operator"


    // $ANTLR start "rulelogical_operator"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:954:1: rulelogical_operator returns [EObject current=null] : ( ( (lv_loname1_0_0= 'and' ) ) | ( (lv_loname2_1_0= 'or' ) ) | ( (lv_loname3_2_0= 'xor' ) ) ) ;
    public final EObject rulelogical_operator() throws RecognitionException {
        EObject current = null;

        Token lv_loname1_0_0=null;
        Token lv_loname2_1_0=null;
        Token lv_loname3_2_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:957:28: ( ( ( (lv_loname1_0_0= 'and' ) ) | ( (lv_loname2_1_0= 'or' ) ) | ( (lv_loname3_2_0= 'xor' ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:958:1: ( ( (lv_loname1_0_0= 'and' ) ) | ( (lv_loname2_1_0= 'or' ) ) | ( (lv_loname3_2_0= 'xor' ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:958:1: ( ( (lv_loname1_0_0= 'and' ) ) | ( (lv_loname2_1_0= 'or' ) ) | ( (lv_loname3_2_0= 'xor' ) ) )
            int alt13=3;
            switch ( input.LA(1) ) {
            case 20:
                {
                alt13=1;
                }
                break;
            case 21:
                {
                alt13=2;
                }
                break;
            case 22:
                {
                alt13=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:958:2: ( (lv_loname1_0_0= 'and' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:958:2: ( (lv_loname1_0_0= 'and' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:959:1: (lv_loname1_0_0= 'and' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:959:1: (lv_loname1_0_0= 'and' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:960:3: lv_loname1_0_0= 'and'
                    {
                    lv_loname1_0_0=(Token)match(input,20,FOLLOW_20_in_rulelogical_operator1815); 

                            newLeafNode(lv_loname1_0_0, grammarAccess.getLogical_operatorAccess().getLoname1AndKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getLogical_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "loname1", lv_loname1_0_0, "and");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:974:6: ( (lv_loname2_1_0= 'or' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:974:6: ( (lv_loname2_1_0= 'or' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:975:1: (lv_loname2_1_0= 'or' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:975:1: (lv_loname2_1_0= 'or' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:976:3: lv_loname2_1_0= 'or'
                    {
                    lv_loname2_1_0=(Token)match(input,21,FOLLOW_21_in_rulelogical_operator1852); 

                            newLeafNode(lv_loname2_1_0, grammarAccess.getLogical_operatorAccess().getLoname2OrKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getLogical_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "loname2", lv_loname2_1_0, "or");
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:990:6: ( (lv_loname3_2_0= 'xor' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:990:6: ( (lv_loname3_2_0= 'xor' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:991:1: (lv_loname3_2_0= 'xor' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:991:1: (lv_loname3_2_0= 'xor' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:992:3: lv_loname3_2_0= 'xor'
                    {
                    lv_loname3_2_0=(Token)match(input,22,FOLLOW_22_in_rulelogical_operator1889); 

                            newLeafNode(lv_loname3_2_0, grammarAccess.getLogical_operatorAccess().getLoname3XorKeyword_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getLogical_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "loname3", lv_loname3_2_0, "xor");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulelogical_operator"


    // $ANTLR start "entryRulerelational_operator"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1013:1: entryRulerelational_operator returns [EObject current=null] : iv_rulerelational_operator= rulerelational_operator EOF ;
    public final EObject entryRulerelational_operator() throws RecognitionException {
        EObject current = null;

        EObject iv_rulerelational_operator = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1014:2: (iv_rulerelational_operator= rulerelational_operator EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1015:2: iv_rulerelational_operator= rulerelational_operator EOF
            {
             newCompositeNode(grammarAccess.getRelational_operatorRule()); 
            pushFollow(FOLLOW_rulerelational_operator_in_entryRulerelational_operator1938);
            iv_rulerelational_operator=rulerelational_operator();

            state._fsp--;

             current =iv_rulerelational_operator; 
            match(input,EOF,FOLLOW_EOF_in_entryRulerelational_operator1948); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulerelational_operator"


    // $ANTLR start "rulerelational_operator"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1022:1: rulerelational_operator returns [EObject current=null] : ( ( (lv_roname1_0_0= '=' ) ) | ( (lv_roname2_1_0= '!=' ) ) | ( (lv_roname3_2_0= '<' ) ) | ( (lv_roname4_3_0= '<=' ) ) | ( (lv_roname5_4_0= '>' ) ) | ( (lv_roname6_5_0= '>=' ) ) ) ;
    public final EObject rulerelational_operator() throws RecognitionException {
        EObject current = null;

        Token lv_roname1_0_0=null;
        Token lv_roname2_1_0=null;
        Token lv_roname3_2_0=null;
        Token lv_roname4_3_0=null;
        Token lv_roname5_4_0=null;
        Token lv_roname6_5_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1025:28: ( ( ( (lv_roname1_0_0= '=' ) ) | ( (lv_roname2_1_0= '!=' ) ) | ( (lv_roname3_2_0= '<' ) ) | ( (lv_roname4_3_0= '<=' ) ) | ( (lv_roname5_4_0= '>' ) ) | ( (lv_roname6_5_0= '>=' ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1026:1: ( ( (lv_roname1_0_0= '=' ) ) | ( (lv_roname2_1_0= '!=' ) ) | ( (lv_roname3_2_0= '<' ) ) | ( (lv_roname4_3_0= '<=' ) ) | ( (lv_roname5_4_0= '>' ) ) | ( (lv_roname6_5_0= '>=' ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1026:1: ( ( (lv_roname1_0_0= '=' ) ) | ( (lv_roname2_1_0= '!=' ) ) | ( (lv_roname3_2_0= '<' ) ) | ( (lv_roname4_3_0= '<=' ) ) | ( (lv_roname5_4_0= '>' ) ) | ( (lv_roname6_5_0= '>=' ) ) )
            int alt14=6;
            switch ( input.LA(1) ) {
            case 23:
                {
                alt14=1;
                }
                break;
            case 24:
                {
                alt14=2;
                }
                break;
            case 25:
                {
                alt14=3;
                }
                break;
            case 26:
                {
                alt14=4;
                }
                break;
            case 27:
                {
                alt14=5;
                }
                break;
            case 28:
                {
                alt14=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1026:2: ( (lv_roname1_0_0= '=' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1026:2: ( (lv_roname1_0_0= '=' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1027:1: (lv_roname1_0_0= '=' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1027:1: (lv_roname1_0_0= '=' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1028:3: lv_roname1_0_0= '='
                    {
                    lv_roname1_0_0=(Token)match(input,23,FOLLOW_23_in_rulerelational_operator1991); 

                            newLeafNode(lv_roname1_0_0, grammarAccess.getRelational_operatorAccess().getRoname1EqualsSignKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getRelational_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "roname1", lv_roname1_0_0, "=");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1042:6: ( (lv_roname2_1_0= '!=' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1042:6: ( (lv_roname2_1_0= '!=' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1043:1: (lv_roname2_1_0= '!=' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1043:1: (lv_roname2_1_0= '!=' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1044:3: lv_roname2_1_0= '!='
                    {
                    lv_roname2_1_0=(Token)match(input,24,FOLLOW_24_in_rulerelational_operator2028); 

                            newLeafNode(lv_roname2_1_0, grammarAccess.getRelational_operatorAccess().getRoname2ExclamationMarkEqualsSignKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getRelational_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "roname2", lv_roname2_1_0, "!=");
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1058:6: ( (lv_roname3_2_0= '<' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1058:6: ( (lv_roname3_2_0= '<' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1059:1: (lv_roname3_2_0= '<' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1059:1: (lv_roname3_2_0= '<' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1060:3: lv_roname3_2_0= '<'
                    {
                    lv_roname3_2_0=(Token)match(input,25,FOLLOW_25_in_rulerelational_operator2065); 

                            newLeafNode(lv_roname3_2_0, grammarAccess.getRelational_operatorAccess().getRoname3LessThanSignKeyword_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getRelational_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "roname3", lv_roname3_2_0, "<");
                    	    

                    }


                    }


                    }
                    break;
                case 4 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1074:6: ( (lv_roname4_3_0= '<=' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1074:6: ( (lv_roname4_3_0= '<=' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1075:1: (lv_roname4_3_0= '<=' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1075:1: (lv_roname4_3_0= '<=' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1076:3: lv_roname4_3_0= '<='
                    {
                    lv_roname4_3_0=(Token)match(input,26,FOLLOW_26_in_rulerelational_operator2102); 

                            newLeafNode(lv_roname4_3_0, grammarAccess.getRelational_operatorAccess().getRoname4LessThanSignEqualsSignKeyword_3_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getRelational_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "roname4", lv_roname4_3_0, "<=");
                    	    

                    }


                    }


                    }
                    break;
                case 5 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1090:6: ( (lv_roname5_4_0= '>' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1090:6: ( (lv_roname5_4_0= '>' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1091:1: (lv_roname5_4_0= '>' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1091:1: (lv_roname5_4_0= '>' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1092:3: lv_roname5_4_0= '>'
                    {
                    lv_roname5_4_0=(Token)match(input,27,FOLLOW_27_in_rulerelational_operator2139); 

                            newLeafNode(lv_roname5_4_0, grammarAccess.getRelational_operatorAccess().getRoname5GreaterThanSignKeyword_4_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getRelational_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "roname5", lv_roname5_4_0, ">");
                    	    

                    }


                    }


                    }
                    break;
                case 6 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1106:6: ( (lv_roname6_5_0= '>=' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1106:6: ( (lv_roname6_5_0= '>=' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1107:1: (lv_roname6_5_0= '>=' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1107:1: (lv_roname6_5_0= '>=' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1108:3: lv_roname6_5_0= '>='
                    {
                    lv_roname6_5_0=(Token)match(input,28,FOLLOW_28_in_rulerelational_operator2176); 

                            newLeafNode(lv_roname6_5_0, grammarAccess.getRelational_operatorAccess().getRoname6GreaterThanSignEqualsSignKeyword_5_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getRelational_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "roname6", lv_roname6_5_0, ">=");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulerelational_operator"


    // $ANTLR start "entryRulebinary_adding_operator"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1129:1: entryRulebinary_adding_operator returns [EObject current=null] : iv_rulebinary_adding_operator= rulebinary_adding_operator EOF ;
    public final EObject entryRulebinary_adding_operator() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebinary_adding_operator = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1130:2: (iv_rulebinary_adding_operator= rulebinary_adding_operator EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1131:2: iv_rulebinary_adding_operator= rulebinary_adding_operator EOF
            {
             newCompositeNode(grammarAccess.getBinary_adding_operatorRule()); 
            pushFollow(FOLLOW_rulebinary_adding_operator_in_entryRulebinary_adding_operator2225);
            iv_rulebinary_adding_operator=rulebinary_adding_operator();

            state._fsp--;

             current =iv_rulebinary_adding_operator; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebinary_adding_operator2235); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebinary_adding_operator"


    // $ANTLR start "rulebinary_adding_operator"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1138:1: rulebinary_adding_operator returns [EObject current=null] : ( ( (lv_baoname1_0_0= '+' ) ) | ( (lv_baoname2_1_0= '-' ) ) ) ;
    public final EObject rulebinary_adding_operator() throws RecognitionException {
        EObject current = null;

        Token lv_baoname1_0_0=null;
        Token lv_baoname2_1_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1141:28: ( ( ( (lv_baoname1_0_0= '+' ) ) | ( (lv_baoname2_1_0= '-' ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1142:1: ( ( (lv_baoname1_0_0= '+' ) ) | ( (lv_baoname2_1_0= '-' ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1142:1: ( ( (lv_baoname1_0_0= '+' ) ) | ( (lv_baoname2_1_0= '-' ) ) )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==29) ) {
                alt15=1;
            }
            else if ( (LA15_0==30) ) {
                alt15=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1142:2: ( (lv_baoname1_0_0= '+' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1142:2: ( (lv_baoname1_0_0= '+' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1143:1: (lv_baoname1_0_0= '+' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1143:1: (lv_baoname1_0_0= '+' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1144:3: lv_baoname1_0_0= '+'
                    {
                    lv_baoname1_0_0=(Token)match(input,29,FOLLOW_29_in_rulebinary_adding_operator2278); 

                            newLeafNode(lv_baoname1_0_0, grammarAccess.getBinary_adding_operatorAccess().getBaoname1PlusSignKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBinary_adding_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "baoname1", lv_baoname1_0_0, "+");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1158:6: ( (lv_baoname2_1_0= '-' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1158:6: ( (lv_baoname2_1_0= '-' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1159:1: (lv_baoname2_1_0= '-' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1159:1: (lv_baoname2_1_0= '-' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1160:3: lv_baoname2_1_0= '-'
                    {
                    lv_baoname2_1_0=(Token)match(input,30,FOLLOW_30_in_rulebinary_adding_operator2315); 

                            newLeafNode(lv_baoname2_1_0, grammarAccess.getBinary_adding_operatorAccess().getBaoname2HyphenMinusKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBinary_adding_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "baoname2", lv_baoname2_1_0, "-");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebinary_adding_operator"


    // $ANTLR start "entryRuleunary_adding_operator"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1181:1: entryRuleunary_adding_operator returns [EObject current=null] : iv_ruleunary_adding_operator= ruleunary_adding_operator EOF ;
    public final EObject entryRuleunary_adding_operator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleunary_adding_operator = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1182:2: (iv_ruleunary_adding_operator= ruleunary_adding_operator EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1183:2: iv_ruleunary_adding_operator= ruleunary_adding_operator EOF
            {
             newCompositeNode(grammarAccess.getUnary_adding_operatorRule()); 
            pushFollow(FOLLOW_ruleunary_adding_operator_in_entryRuleunary_adding_operator2364);
            iv_ruleunary_adding_operator=ruleunary_adding_operator();

            state._fsp--;

             current =iv_ruleunary_adding_operator; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleunary_adding_operator2374); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleunary_adding_operator"


    // $ANTLR start "ruleunary_adding_operator"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1190:1: ruleunary_adding_operator returns [EObject current=null] : ( ( (lv_uaoname1_0_0= '+' ) ) | ( (lv_uaoname2_1_0= '-' ) ) ) ;
    public final EObject ruleunary_adding_operator() throws RecognitionException {
        EObject current = null;

        Token lv_uaoname1_0_0=null;
        Token lv_uaoname2_1_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1193:28: ( ( ( (lv_uaoname1_0_0= '+' ) ) | ( (lv_uaoname2_1_0= '-' ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1194:1: ( ( (lv_uaoname1_0_0= '+' ) ) | ( (lv_uaoname2_1_0= '-' ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1194:1: ( ( (lv_uaoname1_0_0= '+' ) ) | ( (lv_uaoname2_1_0= '-' ) ) )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==29) ) {
                alt16=1;
            }
            else if ( (LA16_0==30) ) {
                alt16=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1194:2: ( (lv_uaoname1_0_0= '+' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1194:2: ( (lv_uaoname1_0_0= '+' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1195:1: (lv_uaoname1_0_0= '+' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1195:1: (lv_uaoname1_0_0= '+' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1196:3: lv_uaoname1_0_0= '+'
                    {
                    lv_uaoname1_0_0=(Token)match(input,29,FOLLOW_29_in_ruleunary_adding_operator2417); 

                            newLeafNode(lv_uaoname1_0_0, grammarAccess.getUnary_adding_operatorAccess().getUaoname1PlusSignKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnary_adding_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "uaoname1", lv_uaoname1_0_0, "+");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1210:6: ( (lv_uaoname2_1_0= '-' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1210:6: ( (lv_uaoname2_1_0= '-' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1211:1: (lv_uaoname2_1_0= '-' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1211:1: (lv_uaoname2_1_0= '-' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1212:3: lv_uaoname2_1_0= '-'
                    {
                    lv_uaoname2_1_0=(Token)match(input,30,FOLLOW_30_in_ruleunary_adding_operator2454); 

                            newLeafNode(lv_uaoname2_1_0, grammarAccess.getUnary_adding_operatorAccess().getUaoname2HyphenMinusKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnary_adding_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "uaoname2", lv_uaoname2_1_0, "-");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleunary_adding_operator"


    // $ANTLR start "entryRulemultiplying_operator"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1233:1: entryRulemultiplying_operator returns [EObject current=null] : iv_rulemultiplying_operator= rulemultiplying_operator EOF ;
    public final EObject entryRulemultiplying_operator() throws RecognitionException {
        EObject current = null;

        EObject iv_rulemultiplying_operator = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1234:2: (iv_rulemultiplying_operator= rulemultiplying_operator EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1235:2: iv_rulemultiplying_operator= rulemultiplying_operator EOF
            {
             newCompositeNode(grammarAccess.getMultiplying_operatorRule()); 
            pushFollow(FOLLOW_rulemultiplying_operator_in_entryRulemultiplying_operator2503);
            iv_rulemultiplying_operator=rulemultiplying_operator();

            state._fsp--;

             current =iv_rulemultiplying_operator; 
            match(input,EOF,FOLLOW_EOF_in_entryRulemultiplying_operator2513); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulemultiplying_operator"


    // $ANTLR start "rulemultiplying_operator"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1242:1: rulemultiplying_operator returns [EObject current=null] : ( ( (lv_moname1_0_0= '*' ) ) | ( (lv_moname2_1_0= '/' ) ) | ( (lv_moname3_2_0= 'mod' ) ) | ( (lv_moname4_3_0= 'rem' ) ) ) ;
    public final EObject rulemultiplying_operator() throws RecognitionException {
        EObject current = null;

        Token lv_moname1_0_0=null;
        Token lv_moname2_1_0=null;
        Token lv_moname3_2_0=null;
        Token lv_moname4_3_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1245:28: ( ( ( (lv_moname1_0_0= '*' ) ) | ( (lv_moname2_1_0= '/' ) ) | ( (lv_moname3_2_0= 'mod' ) ) | ( (lv_moname4_3_0= 'rem' ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1246:1: ( ( (lv_moname1_0_0= '*' ) ) | ( (lv_moname2_1_0= '/' ) ) | ( (lv_moname3_2_0= 'mod' ) ) | ( (lv_moname4_3_0= 'rem' ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1246:1: ( ( (lv_moname1_0_0= '*' ) ) | ( (lv_moname2_1_0= '/' ) ) | ( (lv_moname3_2_0= 'mod' ) ) | ( (lv_moname4_3_0= 'rem' ) ) )
            int alt17=4;
            switch ( input.LA(1) ) {
            case 31:
                {
                alt17=1;
                }
                break;
            case 32:
                {
                alt17=2;
                }
                break;
            case 33:
                {
                alt17=3;
                }
                break;
            case 34:
                {
                alt17=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }

            switch (alt17) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1246:2: ( (lv_moname1_0_0= '*' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1246:2: ( (lv_moname1_0_0= '*' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1247:1: (lv_moname1_0_0= '*' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1247:1: (lv_moname1_0_0= '*' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1248:3: lv_moname1_0_0= '*'
                    {
                    lv_moname1_0_0=(Token)match(input,31,FOLLOW_31_in_rulemultiplying_operator2556); 

                            newLeafNode(lv_moname1_0_0, grammarAccess.getMultiplying_operatorAccess().getMoname1AsteriskKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getMultiplying_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "moname1", lv_moname1_0_0, "*");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1262:6: ( (lv_moname2_1_0= '/' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1262:6: ( (lv_moname2_1_0= '/' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1263:1: (lv_moname2_1_0= '/' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1263:1: (lv_moname2_1_0= '/' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1264:3: lv_moname2_1_0= '/'
                    {
                    lv_moname2_1_0=(Token)match(input,32,FOLLOW_32_in_rulemultiplying_operator2593); 

                            newLeafNode(lv_moname2_1_0, grammarAccess.getMultiplying_operatorAccess().getMoname2SolidusKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getMultiplying_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "moname2", lv_moname2_1_0, "/");
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1278:6: ( (lv_moname3_2_0= 'mod' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1278:6: ( (lv_moname3_2_0= 'mod' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1279:1: (lv_moname3_2_0= 'mod' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1279:1: (lv_moname3_2_0= 'mod' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1280:3: lv_moname3_2_0= 'mod'
                    {
                    lv_moname3_2_0=(Token)match(input,33,FOLLOW_33_in_rulemultiplying_operator2630); 

                            newLeafNode(lv_moname3_2_0, grammarAccess.getMultiplying_operatorAccess().getMoname3ModKeyword_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getMultiplying_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "moname3", lv_moname3_2_0, "mod");
                    	    

                    }


                    }


                    }
                    break;
                case 4 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1294:6: ( (lv_moname4_3_0= 'rem' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1294:6: ( (lv_moname4_3_0= 'rem' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1295:1: (lv_moname4_3_0= 'rem' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1295:1: (lv_moname4_3_0= 'rem' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1296:3: lv_moname4_3_0= 'rem'
                    {
                    lv_moname4_3_0=(Token)match(input,34,FOLLOW_34_in_rulemultiplying_operator2667); 

                            newLeafNode(lv_moname4_3_0, grammarAccess.getMultiplying_operatorAccess().getMoname4RemKeyword_3_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getMultiplying_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "moname4", lv_moname4_3_0, "rem");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulemultiplying_operator"


    // $ANTLR start "entryRulebinary_numeric_operator"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1317:1: entryRulebinary_numeric_operator returns [EObject current=null] : iv_rulebinary_numeric_operator= rulebinary_numeric_operator EOF ;
    public final EObject entryRulebinary_numeric_operator() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebinary_numeric_operator = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1318:2: (iv_rulebinary_numeric_operator= rulebinary_numeric_operator EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1319:2: iv_rulebinary_numeric_operator= rulebinary_numeric_operator EOF
            {
             newCompositeNode(grammarAccess.getBinary_numeric_operatorRule()); 
            pushFollow(FOLLOW_rulebinary_numeric_operator_in_entryRulebinary_numeric_operator2716);
            iv_rulebinary_numeric_operator=rulebinary_numeric_operator();

            state._fsp--;

             current =iv_rulebinary_numeric_operator; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebinary_numeric_operator2726); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebinary_numeric_operator"


    // $ANTLR start "rulebinary_numeric_operator"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1326:1: rulebinary_numeric_operator returns [EObject current=null] : ( (lv_bnoname_0_0= '**' ) ) ;
    public final EObject rulebinary_numeric_operator() throws RecognitionException {
        EObject current = null;

        Token lv_bnoname_0_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1329:28: ( ( (lv_bnoname_0_0= '**' ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1330:1: ( (lv_bnoname_0_0= '**' ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1330:1: ( (lv_bnoname_0_0= '**' ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1331:1: (lv_bnoname_0_0= '**' )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1331:1: (lv_bnoname_0_0= '**' )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1332:3: lv_bnoname_0_0= '**'
            {
            lv_bnoname_0_0=(Token)match(input,35,FOLLOW_35_in_rulebinary_numeric_operator2768); 

                    newLeafNode(lv_bnoname_0_0, grammarAccess.getBinary_numeric_operatorAccess().getBnonameAsteriskAsteriskKeyword_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getBinary_numeric_operatorRule());
            	        }
                   		setWithLastConsumed(current, "bnoname", lv_bnoname_0_0, "**");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebinary_numeric_operator"


    // $ANTLR start "entryRuleunary_numeric_operator"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1353:1: entryRuleunary_numeric_operator returns [EObject current=null] : iv_ruleunary_numeric_operator= ruleunary_numeric_operator EOF ;
    public final EObject entryRuleunary_numeric_operator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleunary_numeric_operator = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1354:2: (iv_ruleunary_numeric_operator= ruleunary_numeric_operator EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1355:2: iv_ruleunary_numeric_operator= ruleunary_numeric_operator EOF
            {
             newCompositeNode(grammarAccess.getUnary_numeric_operatorRule()); 
            pushFollow(FOLLOW_ruleunary_numeric_operator_in_entryRuleunary_numeric_operator2816);
            iv_ruleunary_numeric_operator=ruleunary_numeric_operator();

            state._fsp--;

             current =iv_ruleunary_numeric_operator; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleunary_numeric_operator2826); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleunary_numeric_operator"


    // $ANTLR start "ruleunary_numeric_operator"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1362:1: ruleunary_numeric_operator returns [EObject current=null] : ( (lv_unoname_0_0= 'abs' ) ) ;
    public final EObject ruleunary_numeric_operator() throws RecognitionException {
        EObject current = null;

        Token lv_unoname_0_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1365:28: ( ( (lv_unoname_0_0= 'abs' ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1366:1: ( (lv_unoname_0_0= 'abs' ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1366:1: ( (lv_unoname_0_0= 'abs' ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1367:1: (lv_unoname_0_0= 'abs' )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1367:1: (lv_unoname_0_0= 'abs' )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1368:3: lv_unoname_0_0= 'abs'
            {
            lv_unoname_0_0=(Token)match(input,36,FOLLOW_36_in_ruleunary_numeric_operator2868); 

                    newLeafNode(lv_unoname_0_0, grammarAccess.getUnary_numeric_operatorAccess().getUnonameAbsKeyword_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getUnary_numeric_operatorRule());
            	        }
                   		setWithLastConsumed(current, "unoname", lv_unoname_0_0, "abs");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleunary_numeric_operator"


    // $ANTLR start "entryRuleunary_boolean_operator"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1389:1: entryRuleunary_boolean_operator returns [EObject current=null] : iv_ruleunary_boolean_operator= ruleunary_boolean_operator EOF ;
    public final EObject entryRuleunary_boolean_operator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleunary_boolean_operator = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1390:2: (iv_ruleunary_boolean_operator= ruleunary_boolean_operator EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1391:2: iv_ruleunary_boolean_operator= ruleunary_boolean_operator EOF
            {
             newCompositeNode(grammarAccess.getUnary_boolean_operatorRule()); 
            pushFollow(FOLLOW_ruleunary_boolean_operator_in_entryRuleunary_boolean_operator2916);
            iv_ruleunary_boolean_operator=ruleunary_boolean_operator();

            state._fsp--;

             current =iv_ruleunary_boolean_operator; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleunary_boolean_operator2926); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleunary_boolean_operator"


    // $ANTLR start "ruleunary_boolean_operator"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1398:1: ruleunary_boolean_operator returns [EObject current=null] : ( (lv_uboname_0_0= 'not' ) ) ;
    public final EObject ruleunary_boolean_operator() throws RecognitionException {
        EObject current = null;

        Token lv_uboname_0_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1401:28: ( ( (lv_uboname_0_0= 'not' ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1402:1: ( (lv_uboname_0_0= 'not' ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1402:1: ( (lv_uboname_0_0= 'not' ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1403:1: (lv_uboname_0_0= 'not' )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1403:1: (lv_uboname_0_0= 'not' )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1404:3: lv_uboname_0_0= 'not'
            {
            lv_uboname_0_0=(Token)match(input,37,FOLLOW_37_in_ruleunary_boolean_operator2968); 

                    newLeafNode(lv_uboname_0_0, grammarAccess.getUnary_boolean_operatorAccess().getUbonameNotKeyword_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getUnary_boolean_operatorRule());
            	        }
                   		setWithLastConsumed(current, "uboname", lv_uboname_0_0, "not");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleunary_boolean_operator"


    // $ANTLR start "entryRuleboolean_literal"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1425:1: entryRuleboolean_literal returns [EObject current=null] : iv_ruleboolean_literal= ruleboolean_literal EOF ;
    public final EObject entryRuleboolean_literal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleboolean_literal = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1426:2: (iv_ruleboolean_literal= ruleboolean_literal EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1427:2: iv_ruleboolean_literal= ruleboolean_literal EOF
            {
             newCompositeNode(grammarAccess.getBoolean_literalRule()); 
            pushFollow(FOLLOW_ruleboolean_literal_in_entryRuleboolean_literal3016);
            iv_ruleboolean_literal=ruleboolean_literal();

            state._fsp--;

             current =iv_ruleboolean_literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleboolean_literal3026); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleboolean_literal"


    // $ANTLR start "ruleboolean_literal"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1434:1: ruleboolean_literal returns [EObject current=null] : ( ( (lv_blname1_0_0= 'true' ) ) | ( (lv_blname1_1_0= 'false' ) ) ) ;
    public final EObject ruleboolean_literal() throws RecognitionException {
        EObject current = null;

        Token lv_blname1_0_0=null;
        Token lv_blname1_1_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1437:28: ( ( ( (lv_blname1_0_0= 'true' ) ) | ( (lv_blname1_1_0= 'false' ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1438:1: ( ( (lv_blname1_0_0= 'true' ) ) | ( (lv_blname1_1_0= 'false' ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1438:1: ( ( (lv_blname1_0_0= 'true' ) ) | ( (lv_blname1_1_0= 'false' ) ) )
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==38) ) {
                alt18=1;
            }
            else if ( (LA18_0==39) ) {
                alt18=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }
            switch (alt18) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1438:2: ( (lv_blname1_0_0= 'true' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1438:2: ( (lv_blname1_0_0= 'true' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1439:1: (lv_blname1_0_0= 'true' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1439:1: (lv_blname1_0_0= 'true' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1440:3: lv_blname1_0_0= 'true'
                    {
                    lv_blname1_0_0=(Token)match(input,38,FOLLOW_38_in_ruleboolean_literal3069); 

                            newLeafNode(lv_blname1_0_0, grammarAccess.getBoolean_literalAccess().getBlname1TrueKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBoolean_literalRule());
                    	        }
                           		setWithLastConsumed(current, "blname1", lv_blname1_0_0, "true");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1454:6: ( (lv_blname1_1_0= 'false' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1454:6: ( (lv_blname1_1_0= 'false' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1455:1: (lv_blname1_1_0= 'false' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1455:1: (lv_blname1_1_0= 'false' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1456:3: lv_blname1_1_0= 'false'
                    {
                    lv_blname1_1_0=(Token)match(input,39,FOLLOW_39_in_ruleboolean_literal3106); 

                            newLeafNode(lv_blname1_1_0, grammarAccess.getBoolean_literalAccess().getBlname1FalseKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBoolean_literalRule());
                    	        }
                           		setWithLastConsumed(current, "blname1", lv_blname1_1_0, "false");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleboolean_literal"


    // $ANTLR start "entryRuleinteger_value"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1477:1: entryRuleinteger_value returns [EObject current=null] : iv_ruleinteger_value= ruleinteger_value EOF ;
    public final EObject entryRuleinteger_value() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleinteger_value = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1478:2: (iv_ruleinteger_value= ruleinteger_value EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1479:2: iv_ruleinteger_value= ruleinteger_value EOF
            {
             newCompositeNode(grammarAccess.getInteger_valueRule()); 
            pushFollow(FOLLOW_ruleinteger_value_in_entryRuleinteger_value3155);
            iv_ruleinteger_value=ruleinteger_value();

            state._fsp--;

             current =iv_ruleinteger_value; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleinteger_value3165); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleinteger_value"


    // $ANTLR start "ruleinteger_value"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1486:1: ruleinteger_value returns [EObject current=null] : ( (lv_ivname_0_0= rulenumeric_literal ) ) ;
    public final EObject ruleinteger_value() throws RecognitionException {
        EObject current = null;

        EObject lv_ivname_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1489:28: ( ( (lv_ivname_0_0= rulenumeric_literal ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1490:1: ( (lv_ivname_0_0= rulenumeric_literal ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1490:1: ( (lv_ivname_0_0= rulenumeric_literal ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1491:1: (lv_ivname_0_0= rulenumeric_literal )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1491:1: (lv_ivname_0_0= rulenumeric_literal )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1492:3: lv_ivname_0_0= rulenumeric_literal
            {
             
            	        newCompositeNode(grammarAccess.getInteger_valueAccess().getIvnameNumeric_literalParserRuleCall_0()); 
            	    
            pushFollow(FOLLOW_rulenumeric_literal_in_ruleinteger_value3210);
            lv_ivname_0_0=rulenumeric_literal();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getInteger_valueRule());
            	        }
                   		set(
                   			current, 
                   			"ivname",
                    		lv_ivname_0_0, 
                    		"numeric_literal");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleinteger_value"


    // $ANTLR start "entryRulebehavior_time"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1516:1: entryRulebehavior_time returns [EObject current=null] : iv_rulebehavior_time= rulebehavior_time EOF ;
    public final EObject entryRulebehavior_time() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebehavior_time = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1517:2: (iv_rulebehavior_time= rulebehavior_time EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1518:2: iv_rulebehavior_time= rulebehavior_time EOF
            {
             newCompositeNode(grammarAccess.getBehavior_timeRule()); 
            pushFollow(FOLLOW_rulebehavior_time_in_entryRulebehavior_time3245);
            iv_rulebehavior_time=rulebehavior_time();

            state._fsp--;

             current =iv_rulebehavior_time; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebehavior_time3255); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebehavior_time"


    // $ANTLR start "rulebehavior_time"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1525:1: rulebehavior_time returns [EObject current=null] : ( ( (lv_btname1_0_0= ruleinteger_value ) ) ( (lv_btname2_1_0= ruleunit_identifier ) ) ) ;
    public final EObject rulebehavior_time() throws RecognitionException {
        EObject current = null;

        EObject lv_btname1_0_0 = null;

        EObject lv_btname2_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1528:28: ( ( ( (lv_btname1_0_0= ruleinteger_value ) ) ( (lv_btname2_1_0= ruleunit_identifier ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1529:1: ( ( (lv_btname1_0_0= ruleinteger_value ) ) ( (lv_btname2_1_0= ruleunit_identifier ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1529:1: ( ( (lv_btname1_0_0= ruleinteger_value ) ) ( (lv_btname2_1_0= ruleunit_identifier ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1529:2: ( (lv_btname1_0_0= ruleinteger_value ) ) ( (lv_btname2_1_0= ruleunit_identifier ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1529:2: ( (lv_btname1_0_0= ruleinteger_value ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1530:1: (lv_btname1_0_0= ruleinteger_value )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1530:1: (lv_btname1_0_0= ruleinteger_value )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1531:3: lv_btname1_0_0= ruleinteger_value
            {
             
            	        newCompositeNode(grammarAccess.getBehavior_timeAccess().getBtname1Integer_valueParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleinteger_value_in_rulebehavior_time3301);
            lv_btname1_0_0=ruleinteger_value();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBehavior_timeRule());
            	        }
                   		set(
                   			current, 
                   			"btname1",
                    		lv_btname1_0_0, 
                    		"integer_value");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1547:2: ( (lv_btname2_1_0= ruleunit_identifier ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1548:1: (lv_btname2_1_0= ruleunit_identifier )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1548:1: (lv_btname2_1_0= ruleunit_identifier )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1549:3: lv_btname2_1_0= ruleunit_identifier
            {
             
            	        newCompositeNode(grammarAccess.getBehavior_timeAccess().getBtname2Unit_identifierParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleunit_identifier_in_rulebehavior_time3322);
            lv_btname2_1_0=ruleunit_identifier();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBehavior_timeRule());
            	        }
                   		set(
                   			current, 
                   			"btname2",
                    		lv_btname2_1_0, 
                    		"unit_identifier");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebehavior_time"


    // $ANTLR start "entryRuleunit_identifier"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1573:1: entryRuleunit_identifier returns [EObject current=null] : iv_ruleunit_identifier= ruleunit_identifier EOF ;
    public final EObject entryRuleunit_identifier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleunit_identifier = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1574:2: (iv_ruleunit_identifier= ruleunit_identifier EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1575:2: iv_ruleunit_identifier= ruleunit_identifier EOF
            {
             newCompositeNode(grammarAccess.getUnit_identifierRule()); 
            pushFollow(FOLLOW_ruleunit_identifier_in_entryRuleunit_identifier3358);
            iv_ruleunit_identifier=ruleunit_identifier();

            state._fsp--;

             current =iv_ruleunit_identifier; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleunit_identifier3368); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleunit_identifier"


    // $ANTLR start "ruleunit_identifier"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1582:1: ruleunit_identifier returns [EObject current=null] : ( ( (lv_uiname1_0_0= 'ps' ) ) | ( (lv_uiname2_1_0= 'ns' ) ) | ( (lv_uiname3_2_0= 'us' ) ) | ( (lv_uiname4_3_0= 'ms' ) ) | ( (lv_uiname5_4_0= 'sec' ) ) | ( (lv_uiname6_5_0= 'min' ) ) | ( (lv_uiname7_6_0= 'hr' ) ) ) ;
    public final EObject ruleunit_identifier() throws RecognitionException {
        EObject current = null;

        Token lv_uiname1_0_0=null;
        Token lv_uiname2_1_0=null;
        Token lv_uiname3_2_0=null;
        Token lv_uiname4_3_0=null;
        Token lv_uiname5_4_0=null;
        Token lv_uiname6_5_0=null;
        Token lv_uiname7_6_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1585:28: ( ( ( (lv_uiname1_0_0= 'ps' ) ) | ( (lv_uiname2_1_0= 'ns' ) ) | ( (lv_uiname3_2_0= 'us' ) ) | ( (lv_uiname4_3_0= 'ms' ) ) | ( (lv_uiname5_4_0= 'sec' ) ) | ( (lv_uiname6_5_0= 'min' ) ) | ( (lv_uiname7_6_0= 'hr' ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1586:1: ( ( (lv_uiname1_0_0= 'ps' ) ) | ( (lv_uiname2_1_0= 'ns' ) ) | ( (lv_uiname3_2_0= 'us' ) ) | ( (lv_uiname4_3_0= 'ms' ) ) | ( (lv_uiname5_4_0= 'sec' ) ) | ( (lv_uiname6_5_0= 'min' ) ) | ( (lv_uiname7_6_0= 'hr' ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1586:1: ( ( (lv_uiname1_0_0= 'ps' ) ) | ( (lv_uiname2_1_0= 'ns' ) ) | ( (lv_uiname3_2_0= 'us' ) ) | ( (lv_uiname4_3_0= 'ms' ) ) | ( (lv_uiname5_4_0= 'sec' ) ) | ( (lv_uiname6_5_0= 'min' ) ) | ( (lv_uiname7_6_0= 'hr' ) ) )
            int alt19=7;
            switch ( input.LA(1) ) {
            case 40:
                {
                alt19=1;
                }
                break;
            case 41:
                {
                alt19=2;
                }
                break;
            case 42:
                {
                alt19=3;
                }
                break;
            case 43:
                {
                alt19=4;
                }
                break;
            case 44:
                {
                alt19=5;
                }
                break;
            case 45:
                {
                alt19=6;
                }
                break;
            case 46:
                {
                alt19=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }

            switch (alt19) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1586:2: ( (lv_uiname1_0_0= 'ps' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1586:2: ( (lv_uiname1_0_0= 'ps' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1587:1: (lv_uiname1_0_0= 'ps' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1587:1: (lv_uiname1_0_0= 'ps' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1588:3: lv_uiname1_0_0= 'ps'
                    {
                    lv_uiname1_0_0=(Token)match(input,40,FOLLOW_40_in_ruleunit_identifier3411); 

                            newLeafNode(lv_uiname1_0_0, grammarAccess.getUnit_identifierAccess().getUiname1PsKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnit_identifierRule());
                    	        }
                           		setWithLastConsumed(current, "uiname1", lv_uiname1_0_0, "ps");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1602:6: ( (lv_uiname2_1_0= 'ns' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1602:6: ( (lv_uiname2_1_0= 'ns' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1603:1: (lv_uiname2_1_0= 'ns' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1603:1: (lv_uiname2_1_0= 'ns' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1604:3: lv_uiname2_1_0= 'ns'
                    {
                    lv_uiname2_1_0=(Token)match(input,41,FOLLOW_41_in_ruleunit_identifier3448); 

                            newLeafNode(lv_uiname2_1_0, grammarAccess.getUnit_identifierAccess().getUiname2NsKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnit_identifierRule());
                    	        }
                           		setWithLastConsumed(current, "uiname2", lv_uiname2_1_0, "ns");
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1618:6: ( (lv_uiname3_2_0= 'us' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1618:6: ( (lv_uiname3_2_0= 'us' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1619:1: (lv_uiname3_2_0= 'us' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1619:1: (lv_uiname3_2_0= 'us' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1620:3: lv_uiname3_2_0= 'us'
                    {
                    lv_uiname3_2_0=(Token)match(input,42,FOLLOW_42_in_ruleunit_identifier3485); 

                            newLeafNode(lv_uiname3_2_0, grammarAccess.getUnit_identifierAccess().getUiname3UsKeyword_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnit_identifierRule());
                    	        }
                           		setWithLastConsumed(current, "uiname3", lv_uiname3_2_0, "us");
                    	    

                    }


                    }


                    }
                    break;
                case 4 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1634:6: ( (lv_uiname4_3_0= 'ms' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1634:6: ( (lv_uiname4_3_0= 'ms' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1635:1: (lv_uiname4_3_0= 'ms' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1635:1: (lv_uiname4_3_0= 'ms' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1636:3: lv_uiname4_3_0= 'ms'
                    {
                    lv_uiname4_3_0=(Token)match(input,43,FOLLOW_43_in_ruleunit_identifier3522); 

                            newLeafNode(lv_uiname4_3_0, grammarAccess.getUnit_identifierAccess().getUiname4MsKeyword_3_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnit_identifierRule());
                    	        }
                           		setWithLastConsumed(current, "uiname4", lv_uiname4_3_0, "ms");
                    	    

                    }


                    }


                    }
                    break;
                case 5 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1650:6: ( (lv_uiname5_4_0= 'sec' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1650:6: ( (lv_uiname5_4_0= 'sec' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1651:1: (lv_uiname5_4_0= 'sec' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1651:1: (lv_uiname5_4_0= 'sec' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1652:3: lv_uiname5_4_0= 'sec'
                    {
                    lv_uiname5_4_0=(Token)match(input,44,FOLLOW_44_in_ruleunit_identifier3559); 

                            newLeafNode(lv_uiname5_4_0, grammarAccess.getUnit_identifierAccess().getUiname5SecKeyword_4_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnit_identifierRule());
                    	        }
                           		setWithLastConsumed(current, "uiname5", lv_uiname5_4_0, "sec");
                    	    

                    }


                    }


                    }
                    break;
                case 6 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1666:6: ( (lv_uiname6_5_0= 'min' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1666:6: ( (lv_uiname6_5_0= 'min' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1667:1: (lv_uiname6_5_0= 'min' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1667:1: (lv_uiname6_5_0= 'min' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1668:3: lv_uiname6_5_0= 'min'
                    {
                    lv_uiname6_5_0=(Token)match(input,45,FOLLOW_45_in_ruleunit_identifier3596); 

                            newLeafNode(lv_uiname6_5_0, grammarAccess.getUnit_identifierAccess().getUiname6MinKeyword_5_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnit_identifierRule());
                    	        }
                           		setWithLastConsumed(current, "uiname6", lv_uiname6_5_0, "min");
                    	    

                    }


                    }


                    }
                    break;
                case 7 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1682:6: ( (lv_uiname7_6_0= 'hr' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1682:6: ( (lv_uiname7_6_0= 'hr' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1683:1: (lv_uiname7_6_0= 'hr' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1683:1: (lv_uiname7_6_0= 'hr' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1684:3: lv_uiname7_6_0= 'hr'
                    {
                    lv_uiname7_6_0=(Token)match(input,46,FOLLOW_46_in_ruleunit_identifier3633); 

                            newLeafNode(lv_uiname7_6_0, grammarAccess.getUnit_identifierAccess().getUiname7HrKeyword_6_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnit_identifierRule());
                    	        }
                           		setWithLastConsumed(current, "uiname7", lv_uiname7_6_0, "hr");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleunit_identifier"


    // $ANTLR start "entryRulenumeric_literal"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1705:1: entryRulenumeric_literal returns [EObject current=null] : iv_rulenumeric_literal= rulenumeric_literal EOF ;
    public final EObject entryRulenumeric_literal() throws RecognitionException {
        EObject current = null;

        EObject iv_rulenumeric_literal = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1706:2: (iv_rulenumeric_literal= rulenumeric_literal EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1707:2: iv_rulenumeric_literal= rulenumeric_literal EOF
            {
             newCompositeNode(grammarAccess.getNumeric_literalRule()); 
            pushFollow(FOLLOW_rulenumeric_literal_in_entryRulenumeric_literal3682);
            iv_rulenumeric_literal=rulenumeric_literal();

            state._fsp--;

             current =iv_rulenumeric_literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRulenumeric_literal3692); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulenumeric_literal"


    // $ANTLR start "rulenumeric_literal"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1714:1: rulenumeric_literal returns [EObject current=null] : ( ( (lv_nlname1_0_0= ruleinteger_literal ) ) | ( (lv_nlname2_1_0= rulereal_literal ) ) ) ;
    public final EObject rulenumeric_literal() throws RecognitionException {
        EObject current = null;

        EObject lv_nlname1_0_0 = null;

        EObject lv_nlname2_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1717:28: ( ( ( (lv_nlname1_0_0= ruleinteger_literal ) ) | ( (lv_nlname2_1_0= rulereal_literal ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1718:1: ( ( (lv_nlname1_0_0= ruleinteger_literal ) ) | ( (lv_nlname2_1_0= rulereal_literal ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1718:1: ( ( (lv_nlname1_0_0= ruleinteger_literal ) ) | ( (lv_nlname2_1_0= rulereal_literal ) ) )
            int alt20=2;
            alt20 = dfa20.predict(input);
            switch (alt20) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1718:2: ( (lv_nlname1_0_0= ruleinteger_literal ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1718:2: ( (lv_nlname1_0_0= ruleinteger_literal ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1719:1: (lv_nlname1_0_0= ruleinteger_literal )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1719:1: (lv_nlname1_0_0= ruleinteger_literal )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1720:3: lv_nlname1_0_0= ruleinteger_literal
                    {
                     
                    	        newCompositeNode(grammarAccess.getNumeric_literalAccess().getNlname1Integer_literalParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleinteger_literal_in_rulenumeric_literal3738);
                    lv_nlname1_0_0=ruleinteger_literal();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getNumeric_literalRule());
                    	        }
                           		set(
                           			current, 
                           			"nlname1",
                            		lv_nlname1_0_0, 
                            		"integer_literal");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1737:6: ( (lv_nlname2_1_0= rulereal_literal ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1737:6: ( (lv_nlname2_1_0= rulereal_literal ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1738:1: (lv_nlname2_1_0= rulereal_literal )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1738:1: (lv_nlname2_1_0= rulereal_literal )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1739:3: lv_nlname2_1_0= rulereal_literal
                    {
                     
                    	        newCompositeNode(grammarAccess.getNumeric_literalAccess().getNlname2Real_literalParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulereal_literal_in_rulenumeric_literal3765);
                    lv_nlname2_1_0=rulereal_literal();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getNumeric_literalRule());
                    	        }
                           		set(
                           			current, 
                           			"nlname2",
                            		lv_nlname2_1_0, 
                            		"real_literal");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulenumeric_literal"


    // $ANTLR start "entryRuleinteger_literal"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1763:1: entryRuleinteger_literal returns [EObject current=null] : iv_ruleinteger_literal= ruleinteger_literal EOF ;
    public final EObject entryRuleinteger_literal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleinteger_literal = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1764:2: (iv_ruleinteger_literal= ruleinteger_literal EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1765:2: iv_ruleinteger_literal= ruleinteger_literal EOF
            {
             newCompositeNode(grammarAccess.getInteger_literalRule()); 
            pushFollow(FOLLOW_ruleinteger_literal_in_entryRuleinteger_literal3801);
            iv_ruleinteger_literal=ruleinteger_literal();

            state._fsp--;

             current =iv_ruleinteger_literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleinteger_literal3811); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleinteger_literal"


    // $ANTLR start "ruleinteger_literal"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1772:1: ruleinteger_literal returns [EObject current=null] : ( ( (lv_ilname1_0_0= ruledecimal_integer_literal ) ) | ( (lv_ilname2_1_0= rulebased_integer_literal ) ) ) ;
    public final EObject ruleinteger_literal() throws RecognitionException {
        EObject current = null;

        EObject lv_ilname1_0_0 = null;

        EObject lv_ilname2_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1775:28: ( ( ( (lv_ilname1_0_0= ruledecimal_integer_literal ) ) | ( (lv_ilname2_1_0= rulebased_integer_literal ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1776:1: ( ( (lv_ilname1_0_0= ruledecimal_integer_literal ) ) | ( (lv_ilname2_1_0= rulebased_integer_literal ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1776:1: ( ( (lv_ilname1_0_0= ruledecimal_integer_literal ) ) | ( (lv_ilname2_1_0= rulebased_integer_literal ) ) )
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==RULE_INT) ) {
                alt21=1;
            }
            else if ( (LA21_0==RULE_DIGIT) ) {
                switch ( input.LA(2) ) {
                case RULE_DIGIT:
                    {
                    int LA21_3 = input.LA(3);

                    if ( (LA21_3==EOF||LA21_3==RULE_DIGIT||LA21_3==RULE_INT||(LA21_3>=20 && LA21_3<=35)||(LA21_3>=40 && LA21_3<=46)||(LA21_3>=48 && LA21_3<=49)) ) {
                        alt21=1;
                    }
                    else if ( (LA21_3==51) ) {
                        alt21=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 21, 3, input);

                        throw nvae;
                    }
                    }
                    break;
                case 51:
                    {
                    alt21=2;
                    }
                    break;
                case EOF:
                case RULE_INT:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 48:
                case 49:
                    {
                    alt21=1;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 21, 2, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1776:2: ( (lv_ilname1_0_0= ruledecimal_integer_literal ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1776:2: ( (lv_ilname1_0_0= ruledecimal_integer_literal ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1777:1: (lv_ilname1_0_0= ruledecimal_integer_literal )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1777:1: (lv_ilname1_0_0= ruledecimal_integer_literal )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1778:3: lv_ilname1_0_0= ruledecimal_integer_literal
                    {
                     
                    	        newCompositeNode(grammarAccess.getInteger_literalAccess().getIlname1Decimal_integer_literalParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruledecimal_integer_literal_in_ruleinteger_literal3857);
                    lv_ilname1_0_0=ruledecimal_integer_literal();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getInteger_literalRule());
                    	        }
                           		set(
                           			current, 
                           			"ilname1",
                            		lv_ilname1_0_0, 
                            		"decimal_integer_literal");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1795:6: ( (lv_ilname2_1_0= rulebased_integer_literal ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1795:6: ( (lv_ilname2_1_0= rulebased_integer_literal ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1796:1: (lv_ilname2_1_0= rulebased_integer_literal )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1796:1: (lv_ilname2_1_0= rulebased_integer_literal )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1797:3: lv_ilname2_1_0= rulebased_integer_literal
                    {
                     
                    	        newCompositeNode(grammarAccess.getInteger_literalAccess().getIlname2Based_integer_literalParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulebased_integer_literal_in_ruleinteger_literal3884);
                    lv_ilname2_1_0=rulebased_integer_literal();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getInteger_literalRule());
                    	        }
                           		set(
                           			current, 
                           			"ilname2",
                            		lv_ilname2_1_0, 
                            		"based_integer_literal");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleinteger_literal"


    // $ANTLR start "entryRulereal_literal"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1821:1: entryRulereal_literal returns [EObject current=null] : iv_rulereal_literal= rulereal_literal EOF ;
    public final EObject entryRulereal_literal() throws RecognitionException {
        EObject current = null;

        EObject iv_rulereal_literal = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1822:2: (iv_rulereal_literal= rulereal_literal EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1823:2: iv_rulereal_literal= rulereal_literal EOF
            {
             newCompositeNode(grammarAccess.getReal_literalRule()); 
            pushFollow(FOLLOW_rulereal_literal_in_entryRulereal_literal3920);
            iv_rulereal_literal=rulereal_literal();

            state._fsp--;

             current =iv_rulereal_literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRulereal_literal3930); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulereal_literal"


    // $ANTLR start "rulereal_literal"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1830:1: rulereal_literal returns [EObject current=null] : ( (lv_rlname_0_0= ruledecimal_real_literal ) ) ;
    public final EObject rulereal_literal() throws RecognitionException {
        EObject current = null;

        EObject lv_rlname_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1833:28: ( ( (lv_rlname_0_0= ruledecimal_real_literal ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1834:1: ( (lv_rlname_0_0= ruledecimal_real_literal ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1834:1: ( (lv_rlname_0_0= ruledecimal_real_literal ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1835:1: (lv_rlname_0_0= ruledecimal_real_literal )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1835:1: (lv_rlname_0_0= ruledecimal_real_literal )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1836:3: lv_rlname_0_0= ruledecimal_real_literal
            {
             
            	        newCompositeNode(grammarAccess.getReal_literalAccess().getRlnameDecimal_real_literalParserRuleCall_0()); 
            	    
            pushFollow(FOLLOW_ruledecimal_real_literal_in_rulereal_literal3975);
            lv_rlname_0_0=ruledecimal_real_literal();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getReal_literalRule());
            	        }
                   		set(
                   			current, 
                   			"rlname",
                    		lv_rlname_0_0, 
                    		"decimal_real_literal");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulereal_literal"


    // $ANTLR start "entryRuledecimal_integer_literal"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1860:1: entryRuledecimal_integer_literal returns [EObject current=null] : iv_ruledecimal_integer_literal= ruledecimal_integer_literal EOF ;
    public final EObject entryRuledecimal_integer_literal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruledecimal_integer_literal = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1861:2: (iv_ruledecimal_integer_literal= ruledecimal_integer_literal EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1862:2: iv_ruledecimal_integer_literal= ruledecimal_integer_literal EOF
            {
             newCompositeNode(grammarAccess.getDecimal_integer_literalRule()); 
            pushFollow(FOLLOW_ruledecimal_integer_literal_in_entryRuledecimal_integer_literal4010);
            iv_ruledecimal_integer_literal=ruledecimal_integer_literal();

            state._fsp--;

             current =iv_ruledecimal_integer_literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRuledecimal_integer_literal4020); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuledecimal_integer_literal"


    // $ANTLR start "ruledecimal_integer_literal"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1869:1: ruledecimal_integer_literal returns [EObject current=null] : ( ( (lv_dilname1_0_0= rulenumeral ) ) ( (lv_dilname2_1_0= rulepositive_exponent ) )? ) ;
    public final EObject ruledecimal_integer_literal() throws RecognitionException {
        EObject current = null;

        EObject lv_dilname1_0_0 = null;

        EObject lv_dilname2_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1872:28: ( ( ( (lv_dilname1_0_0= rulenumeral ) ) ( (lv_dilname2_1_0= rulepositive_exponent ) )? ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1873:1: ( ( (lv_dilname1_0_0= rulenumeral ) ) ( (lv_dilname2_1_0= rulepositive_exponent ) )? )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1873:1: ( ( (lv_dilname1_0_0= rulenumeral ) ) ( (lv_dilname2_1_0= rulepositive_exponent ) )? )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1873:2: ( (lv_dilname1_0_0= rulenumeral ) ) ( (lv_dilname2_1_0= rulepositive_exponent ) )?
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1873:2: ( (lv_dilname1_0_0= rulenumeral ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1874:1: (lv_dilname1_0_0= rulenumeral )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1874:1: (lv_dilname1_0_0= rulenumeral )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1875:3: lv_dilname1_0_0= rulenumeral
            {
             
            	        newCompositeNode(grammarAccess.getDecimal_integer_literalAccess().getDilname1NumeralParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_rulenumeral_in_ruledecimal_integer_literal4066);
            lv_dilname1_0_0=rulenumeral();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getDecimal_integer_literalRule());
            	        }
                   		set(
                   			current, 
                   			"dilname1",
                    		lv_dilname1_0_0, 
                    		"numeral");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1891:2: ( (lv_dilname2_1_0= rulepositive_exponent ) )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==49) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1892:1: (lv_dilname2_1_0= rulepositive_exponent )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1892:1: (lv_dilname2_1_0= rulepositive_exponent )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1893:3: lv_dilname2_1_0= rulepositive_exponent
                    {
                     
                    	        newCompositeNode(grammarAccess.getDecimal_integer_literalAccess().getDilname2Positive_exponentParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulepositive_exponent_in_ruledecimal_integer_literal4087);
                    lv_dilname2_1_0=rulepositive_exponent();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDecimal_integer_literalRule());
                    	        }
                           		set(
                           			current, 
                           			"dilname2",
                            		lv_dilname2_1_0, 
                            		"positive_exponent");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruledecimal_integer_literal"


    // $ANTLR start "entryRuledecimal_real_literal"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1917:1: entryRuledecimal_real_literal returns [EObject current=null] : iv_ruledecimal_real_literal= ruledecimal_real_literal EOF ;
    public final EObject entryRuledecimal_real_literal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruledecimal_real_literal = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1918:2: (iv_ruledecimal_real_literal= ruledecimal_real_literal EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1919:2: iv_ruledecimal_real_literal= ruledecimal_real_literal EOF
            {
             newCompositeNode(grammarAccess.getDecimal_real_literalRule()); 
            pushFollow(FOLLOW_ruledecimal_real_literal_in_entryRuledecimal_real_literal4124);
            iv_ruledecimal_real_literal=ruledecimal_real_literal();

            state._fsp--;

             current =iv_ruledecimal_real_literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRuledecimal_real_literal4134); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuledecimal_real_literal"


    // $ANTLR start "ruledecimal_real_literal"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1926:1: ruledecimal_real_literal returns [EObject current=null] : ( ( (lv_drlname1_0_0= rulenumeral ) ) otherlv_1= '.' ( (lv_drlname2_2_0= rulenumeral ) ) ( (lv_drlname3_3_0= ruleexponent ) )? ) ;
    public final EObject ruledecimal_real_literal() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_drlname1_0_0 = null;

        EObject lv_drlname2_2_0 = null;

        EObject lv_drlname3_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1929:28: ( ( ( (lv_drlname1_0_0= rulenumeral ) ) otherlv_1= '.' ( (lv_drlname2_2_0= rulenumeral ) ) ( (lv_drlname3_3_0= ruleexponent ) )? ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1930:1: ( ( (lv_drlname1_0_0= rulenumeral ) ) otherlv_1= '.' ( (lv_drlname2_2_0= rulenumeral ) ) ( (lv_drlname3_3_0= ruleexponent ) )? )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1930:1: ( ( (lv_drlname1_0_0= rulenumeral ) ) otherlv_1= '.' ( (lv_drlname2_2_0= rulenumeral ) ) ( (lv_drlname3_3_0= ruleexponent ) )? )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1930:2: ( (lv_drlname1_0_0= rulenumeral ) ) otherlv_1= '.' ( (lv_drlname2_2_0= rulenumeral ) ) ( (lv_drlname3_3_0= ruleexponent ) )?
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1930:2: ( (lv_drlname1_0_0= rulenumeral ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1931:1: (lv_drlname1_0_0= rulenumeral )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1931:1: (lv_drlname1_0_0= rulenumeral )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1932:3: lv_drlname1_0_0= rulenumeral
            {
             
            	        newCompositeNode(grammarAccess.getDecimal_real_literalAccess().getDrlname1NumeralParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_rulenumeral_in_ruledecimal_real_literal4180);
            lv_drlname1_0_0=rulenumeral();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getDecimal_real_literalRule());
            	        }
                   		set(
                   			current, 
                   			"drlname1",
                    		lv_drlname1_0_0, 
                    		"numeral");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,47,FOLLOW_47_in_ruledecimal_real_literal4192); 

                	newLeafNode(otherlv_1, grammarAccess.getDecimal_real_literalAccess().getFullStopKeyword_1());
                
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1952:1: ( (lv_drlname2_2_0= rulenumeral ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1953:1: (lv_drlname2_2_0= rulenumeral )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1953:1: (lv_drlname2_2_0= rulenumeral )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1954:3: lv_drlname2_2_0= rulenumeral
            {
             
            	        newCompositeNode(grammarAccess.getDecimal_real_literalAccess().getDrlname2NumeralParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_rulenumeral_in_ruledecimal_real_literal4213);
            lv_drlname2_2_0=rulenumeral();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getDecimal_real_literalRule());
            	        }
                   		set(
                   			current, 
                   			"drlname2",
                    		lv_drlname2_2_0, 
                    		"numeral");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1970:2: ( (lv_drlname3_3_0= ruleexponent ) )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( ((LA23_0>=49 && LA23_0<=50)) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1971:1: (lv_drlname3_3_0= ruleexponent )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1971:1: (lv_drlname3_3_0= ruleexponent )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1972:3: lv_drlname3_3_0= ruleexponent
                    {
                     
                    	        newCompositeNode(grammarAccess.getDecimal_real_literalAccess().getDrlname3ExponentParserRuleCall_3_0()); 
                    	    
                    pushFollow(FOLLOW_ruleexponent_in_ruledecimal_real_literal4234);
                    lv_drlname3_3_0=ruleexponent();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDecimal_real_literalRule());
                    	        }
                           		set(
                           			current, 
                           			"drlname3",
                            		lv_drlname3_3_0, 
                            		"exponent");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruledecimal_real_literal"


    // $ANTLR start "entryRulenumeral"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1996:1: entryRulenumeral returns [EObject current=null] : iv_rulenumeral= rulenumeral EOF ;
    public final EObject entryRulenumeral() throws RecognitionException {
        EObject current = null;

        EObject iv_rulenumeral = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1997:2: (iv_rulenumeral= rulenumeral EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:1998:2: iv_rulenumeral= rulenumeral EOF
            {
             newCompositeNode(grammarAccess.getNumeralRule()); 
            pushFollow(FOLLOW_rulenumeral_in_entryRulenumeral4271);
            iv_rulenumeral=rulenumeral();

            state._fsp--;

             current =iv_rulenumeral; 
            match(input,EOF,FOLLOW_EOF_in_entryRulenumeral4281); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulenumeral"


    // $ANTLR start "rulenumeral"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2005:1: rulenumeral returns [EObject current=null] : ( ( (lv_nname1_0_0= ruleinteger ) ) ( (otherlv_1= '_' )? ( (lv_nname2_2_0= ruleinteger ) ) )* ) ;
    public final EObject rulenumeral() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_nname1_0_0 = null;

        EObject lv_nname2_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2008:28: ( ( ( (lv_nname1_0_0= ruleinteger ) ) ( (otherlv_1= '_' )? ( (lv_nname2_2_0= ruleinteger ) ) )* ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2009:1: ( ( (lv_nname1_0_0= ruleinteger ) ) ( (otherlv_1= '_' )? ( (lv_nname2_2_0= ruleinteger ) ) )* )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2009:1: ( ( (lv_nname1_0_0= ruleinteger ) ) ( (otherlv_1= '_' )? ( (lv_nname2_2_0= ruleinteger ) ) )* )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2009:2: ( (lv_nname1_0_0= ruleinteger ) ) ( (otherlv_1= '_' )? ( (lv_nname2_2_0= ruleinteger ) ) )*
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2009:2: ( (lv_nname1_0_0= ruleinteger ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2010:1: (lv_nname1_0_0= ruleinteger )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2010:1: (lv_nname1_0_0= ruleinteger )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2011:3: lv_nname1_0_0= ruleinteger
            {
             
            	        newCompositeNode(grammarAccess.getNumeralAccess().getNname1IntegerParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleinteger_in_rulenumeral4327);
            lv_nname1_0_0=ruleinteger();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getNumeralRule());
            	        }
                   		set(
                   			current, 
                   			"nname1",
                    		lv_nname1_0_0, 
                    		"integer");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2027:2: ( (otherlv_1= '_' )? ( (lv_nname2_2_0= ruleinteger ) ) )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==RULE_DIGIT||LA25_0==RULE_INT||LA25_0==48) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2027:3: (otherlv_1= '_' )? ( (lv_nname2_2_0= ruleinteger ) )
            	    {
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2027:3: (otherlv_1= '_' )?
            	    int alt24=2;
            	    int LA24_0 = input.LA(1);

            	    if ( (LA24_0==48) ) {
            	        alt24=1;
            	    }
            	    switch (alt24) {
            	        case 1 :
            	            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2027:5: otherlv_1= '_'
            	            {
            	            otherlv_1=(Token)match(input,48,FOLLOW_48_in_rulenumeral4341); 

            	                	newLeafNode(otherlv_1, grammarAccess.getNumeralAccess().get_Keyword_1_0());
            	                

            	            }
            	            break;

            	    }

            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2031:3: ( (lv_nname2_2_0= ruleinteger ) )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2032:1: (lv_nname2_2_0= ruleinteger )
            	    {
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2032:1: (lv_nname2_2_0= ruleinteger )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2033:3: lv_nname2_2_0= ruleinteger
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getNumeralAccess().getNname2IntegerParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleinteger_in_rulenumeral4364);
            	    lv_nname2_2_0=ruleinteger();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getNumeralRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"nname2",
            	            		lv_nname2_2_0, 
            	            		"integer");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulenumeral"


    // $ANTLR start "entryRuleexponent"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2057:1: entryRuleexponent returns [EObject current=null] : iv_ruleexponent= ruleexponent EOF ;
    public final EObject entryRuleexponent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleexponent = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2058:2: (iv_ruleexponent= ruleexponent EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2059:2: iv_ruleexponent= ruleexponent EOF
            {
             newCompositeNode(grammarAccess.getExponentRule()); 
            pushFollow(FOLLOW_ruleexponent_in_entryRuleexponent4402);
            iv_ruleexponent=ruleexponent();

            state._fsp--;

             current =iv_ruleexponent; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleexponent4412); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleexponent"


    // $ANTLR start "ruleexponent"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2066:1: ruleexponent returns [EObject current=null] : ( (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_ename1_2_0= rulenumeral ) ) ) | (otherlv_3= 'E -' ( (lv_ename2_4_0= rulenumeral ) ) ) ) ;
    public final EObject ruleexponent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_ename1_2_0 = null;

        EObject lv_ename2_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2069:28: ( ( (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_ename1_2_0= rulenumeral ) ) ) | (otherlv_3= 'E -' ( (lv_ename2_4_0= rulenumeral ) ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2070:1: ( (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_ename1_2_0= rulenumeral ) ) ) | (otherlv_3= 'E -' ( (lv_ename2_4_0= rulenumeral ) ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2070:1: ( (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_ename1_2_0= rulenumeral ) ) ) | (otherlv_3= 'E -' ( (lv_ename2_4_0= rulenumeral ) ) ) )
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==49) ) {
                alt27=1;
            }
            else if ( (LA27_0==50) ) {
                alt27=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }
            switch (alt27) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2070:2: (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_ename1_2_0= rulenumeral ) ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2070:2: (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_ename1_2_0= rulenumeral ) ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2070:4: otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_ename1_2_0= rulenumeral ) )
                    {
                    otherlv_0=(Token)match(input,49,FOLLOW_49_in_ruleexponent4450); 

                        	newLeafNode(otherlv_0, grammarAccess.getExponentAccess().getEKeyword_0_0());
                        
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2074:1: (otherlv_1= '+' )?
                    int alt26=2;
                    int LA26_0 = input.LA(1);

                    if ( (LA26_0==29) ) {
                        alt26=1;
                    }
                    switch (alt26) {
                        case 1 :
                            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2074:3: otherlv_1= '+'
                            {
                            otherlv_1=(Token)match(input,29,FOLLOW_29_in_ruleexponent4463); 

                                	newLeafNode(otherlv_1, grammarAccess.getExponentAccess().getPlusSignKeyword_0_1());
                                

                            }
                            break;

                    }

                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2078:3: ( (lv_ename1_2_0= rulenumeral ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2079:1: (lv_ename1_2_0= rulenumeral )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2079:1: (lv_ename1_2_0= rulenumeral )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2080:3: lv_ename1_2_0= rulenumeral
                    {
                     
                    	        newCompositeNode(grammarAccess.getExponentAccess().getEname1NumeralParserRuleCall_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_rulenumeral_in_ruleexponent4486);
                    lv_ename1_2_0=rulenumeral();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getExponentRule());
                    	        }
                           		set(
                           			current, 
                           			"ename1",
                            		lv_ename1_2_0, 
                            		"numeral");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2097:6: (otherlv_3= 'E -' ( (lv_ename2_4_0= rulenumeral ) ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2097:6: (otherlv_3= 'E -' ( (lv_ename2_4_0= rulenumeral ) ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2097:8: otherlv_3= 'E -' ( (lv_ename2_4_0= rulenumeral ) )
                    {
                    otherlv_3=(Token)match(input,50,FOLLOW_50_in_ruleexponent4506); 

                        	newLeafNode(otherlv_3, grammarAccess.getExponentAccess().getEKeyword_1_0());
                        
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2101:1: ( (lv_ename2_4_0= rulenumeral ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2102:1: (lv_ename2_4_0= rulenumeral )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2102:1: (lv_ename2_4_0= rulenumeral )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2103:3: lv_ename2_4_0= rulenumeral
                    {
                     
                    	        newCompositeNode(grammarAccess.getExponentAccess().getEname2NumeralParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulenumeral_in_ruleexponent4527);
                    lv_ename2_4_0=rulenumeral();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getExponentRule());
                    	        }
                           		set(
                           			current, 
                           			"ename2",
                            		lv_ename2_4_0, 
                            		"numeral");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleexponent"


    // $ANTLR start "entryRulepositive_exponent"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2127:1: entryRulepositive_exponent returns [EObject current=null] : iv_rulepositive_exponent= rulepositive_exponent EOF ;
    public final EObject entryRulepositive_exponent() throws RecognitionException {
        EObject current = null;

        EObject iv_rulepositive_exponent = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2128:2: (iv_rulepositive_exponent= rulepositive_exponent EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2129:2: iv_rulepositive_exponent= rulepositive_exponent EOF
            {
             newCompositeNode(grammarAccess.getPositive_exponentRule()); 
            pushFollow(FOLLOW_rulepositive_exponent_in_entryRulepositive_exponent4564);
            iv_rulepositive_exponent=rulepositive_exponent();

            state._fsp--;

             current =iv_rulepositive_exponent; 
            match(input,EOF,FOLLOW_EOF_in_entryRulepositive_exponent4574); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulepositive_exponent"


    // $ANTLR start "rulepositive_exponent"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2136:1: rulepositive_exponent returns [EObject current=null] : (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_pename_2_0= rulenumeral ) ) ) ;
    public final EObject rulepositive_exponent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_pename_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2139:28: ( (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_pename_2_0= rulenumeral ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2140:1: (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_pename_2_0= rulenumeral ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2140:1: (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_pename_2_0= rulenumeral ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2140:3: otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_pename_2_0= rulenumeral ) )
            {
            otherlv_0=(Token)match(input,49,FOLLOW_49_in_rulepositive_exponent4611); 

                	newLeafNode(otherlv_0, grammarAccess.getPositive_exponentAccess().getEKeyword_0());
                
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2144:1: (otherlv_1= '+' )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==29) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2144:3: otherlv_1= '+'
                    {
                    otherlv_1=(Token)match(input,29,FOLLOW_29_in_rulepositive_exponent4624); 

                        	newLeafNode(otherlv_1, grammarAccess.getPositive_exponentAccess().getPlusSignKeyword_1());
                        

                    }
                    break;

            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2148:3: ( (lv_pename_2_0= rulenumeral ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2149:1: (lv_pename_2_0= rulenumeral )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2149:1: (lv_pename_2_0= rulenumeral )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2150:3: lv_pename_2_0= rulenumeral
            {
             
            	        newCompositeNode(grammarAccess.getPositive_exponentAccess().getPenameNumeralParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_rulenumeral_in_rulepositive_exponent4647);
            lv_pename_2_0=rulenumeral();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPositive_exponentRule());
            	        }
                   		set(
                   			current, 
                   			"pename",
                    		lv_pename_2_0, 
                    		"numeral");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulepositive_exponent"


    // $ANTLR start "entryRulebased_integer_literal"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2174:1: entryRulebased_integer_literal returns [EObject current=null] : iv_rulebased_integer_literal= rulebased_integer_literal EOF ;
    public final EObject entryRulebased_integer_literal() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebased_integer_literal = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2175:2: (iv_rulebased_integer_literal= rulebased_integer_literal EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2176:2: iv_rulebased_integer_literal= rulebased_integer_literal EOF
            {
             newCompositeNode(grammarAccess.getBased_integer_literalRule()); 
            pushFollow(FOLLOW_rulebased_integer_literal_in_entryRulebased_integer_literal4683);
            iv_rulebased_integer_literal=rulebased_integer_literal();

            state._fsp--;

             current =iv_rulebased_integer_literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebased_integer_literal4693); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebased_integer_literal"


    // $ANTLR start "rulebased_integer_literal"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2183:1: rulebased_integer_literal returns [EObject current=null] : ( ( (lv_bilname1_0_0= rulebase ) ) otherlv_1= '#' ( (lv_bilname2_2_0= rulebased_numeral ) ) otherlv_3= '#' ( (lv_bilname3_4_0= rulepositive_exponent ) )? ) ;
    public final EObject rulebased_integer_literal() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_bilname1_0_0 = null;

        EObject lv_bilname2_2_0 = null;

        EObject lv_bilname3_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2186:28: ( ( ( (lv_bilname1_0_0= rulebase ) ) otherlv_1= '#' ( (lv_bilname2_2_0= rulebased_numeral ) ) otherlv_3= '#' ( (lv_bilname3_4_0= rulepositive_exponent ) )? ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2187:1: ( ( (lv_bilname1_0_0= rulebase ) ) otherlv_1= '#' ( (lv_bilname2_2_0= rulebased_numeral ) ) otherlv_3= '#' ( (lv_bilname3_4_0= rulepositive_exponent ) )? )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2187:1: ( ( (lv_bilname1_0_0= rulebase ) ) otherlv_1= '#' ( (lv_bilname2_2_0= rulebased_numeral ) ) otherlv_3= '#' ( (lv_bilname3_4_0= rulepositive_exponent ) )? )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2187:2: ( (lv_bilname1_0_0= rulebase ) ) otherlv_1= '#' ( (lv_bilname2_2_0= rulebased_numeral ) ) otherlv_3= '#' ( (lv_bilname3_4_0= rulepositive_exponent ) )?
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2187:2: ( (lv_bilname1_0_0= rulebase ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2188:1: (lv_bilname1_0_0= rulebase )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2188:1: (lv_bilname1_0_0= rulebase )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2189:3: lv_bilname1_0_0= rulebase
            {
             
            	        newCompositeNode(grammarAccess.getBased_integer_literalAccess().getBilname1BaseParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_rulebase_in_rulebased_integer_literal4739);
            lv_bilname1_0_0=rulebase();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBased_integer_literalRule());
            	        }
                   		set(
                   			current, 
                   			"bilname1",
                    		lv_bilname1_0_0, 
                    		"base");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,51,FOLLOW_51_in_rulebased_integer_literal4751); 

                	newLeafNode(otherlv_1, grammarAccess.getBased_integer_literalAccess().getNumberSignKeyword_1());
                
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2209:1: ( (lv_bilname2_2_0= rulebased_numeral ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2210:1: (lv_bilname2_2_0= rulebased_numeral )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2210:1: (lv_bilname2_2_0= rulebased_numeral )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2211:3: lv_bilname2_2_0= rulebased_numeral
            {
             
            	        newCompositeNode(grammarAccess.getBased_integer_literalAccess().getBilname2Based_numeralParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_rulebased_numeral_in_rulebased_integer_literal4772);
            lv_bilname2_2_0=rulebased_numeral();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBased_integer_literalRule());
            	        }
                   		set(
                   			current, 
                   			"bilname2",
                    		lv_bilname2_2_0, 
                    		"based_numeral");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,51,FOLLOW_51_in_rulebased_integer_literal4784); 

                	newLeafNode(otherlv_3, grammarAccess.getBased_integer_literalAccess().getNumberSignKeyword_3());
                
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2231:1: ( (lv_bilname3_4_0= rulepositive_exponent ) )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==49) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2232:1: (lv_bilname3_4_0= rulepositive_exponent )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2232:1: (lv_bilname3_4_0= rulepositive_exponent )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2233:3: lv_bilname3_4_0= rulepositive_exponent
                    {
                     
                    	        newCompositeNode(grammarAccess.getBased_integer_literalAccess().getBilname3Positive_exponentParserRuleCall_4_0()); 
                    	    
                    pushFollow(FOLLOW_rulepositive_exponent_in_rulebased_integer_literal4805);
                    lv_bilname3_4_0=rulepositive_exponent();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBased_integer_literalRule());
                    	        }
                           		set(
                           			current, 
                           			"bilname3",
                            		lv_bilname3_4_0, 
                            		"positive_exponent");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebased_integer_literal"


    // $ANTLR start "entryRulebase"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2257:1: entryRulebase returns [EObject current=null] : iv_rulebase= rulebase EOF ;
    public final EObject entryRulebase() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebase = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2258:2: (iv_rulebase= rulebase EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2259:2: iv_rulebase= rulebase EOF
            {
             newCompositeNode(grammarAccess.getBaseRule()); 
            pushFollow(FOLLOW_rulebase_in_entryRulebase4842);
            iv_rulebase=rulebase();

            state._fsp--;

             current =iv_rulebase; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebase4852); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebase"


    // $ANTLR start "rulebase"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2266:1: rulebase returns [EObject current=null] : ( ( (lv_bname1_0_0= RULE_DIGIT ) ) ( (lv_bname2_1_0= RULE_DIGIT ) )? ) ;
    public final EObject rulebase() throws RecognitionException {
        EObject current = null;

        Token lv_bname1_0_0=null;
        Token lv_bname2_1_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2269:28: ( ( ( (lv_bname1_0_0= RULE_DIGIT ) ) ( (lv_bname2_1_0= RULE_DIGIT ) )? ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2270:1: ( ( (lv_bname1_0_0= RULE_DIGIT ) ) ( (lv_bname2_1_0= RULE_DIGIT ) )? )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2270:1: ( ( (lv_bname1_0_0= RULE_DIGIT ) ) ( (lv_bname2_1_0= RULE_DIGIT ) )? )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2270:2: ( (lv_bname1_0_0= RULE_DIGIT ) ) ( (lv_bname2_1_0= RULE_DIGIT ) )?
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2270:2: ( (lv_bname1_0_0= RULE_DIGIT ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2271:1: (lv_bname1_0_0= RULE_DIGIT )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2271:1: (lv_bname1_0_0= RULE_DIGIT )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2272:3: lv_bname1_0_0= RULE_DIGIT
            {
            lv_bname1_0_0=(Token)match(input,RULE_DIGIT,FOLLOW_RULE_DIGIT_in_rulebase4894); 

            			newLeafNode(lv_bname1_0_0, grammarAccess.getBaseAccess().getBname1DigitTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getBaseRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"bname1",
                    		lv_bname1_0_0, 
                    		"digit");
            	    

            }


            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2288:2: ( (lv_bname2_1_0= RULE_DIGIT ) )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==RULE_DIGIT) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2289:1: (lv_bname2_1_0= RULE_DIGIT )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2289:1: (lv_bname2_1_0= RULE_DIGIT )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2290:3: lv_bname2_1_0= RULE_DIGIT
                    {
                    lv_bname2_1_0=(Token)match(input,RULE_DIGIT,FOLLOW_RULE_DIGIT_in_rulebase4916); 

                    			newLeafNode(lv_bname2_1_0, grammarAccess.getBaseAccess().getBname2DigitTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBaseRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"bname2",
                            		lv_bname2_1_0, 
                            		"digit");
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebase"


    // $ANTLR start "entryRulebased_numeral"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2314:1: entryRulebased_numeral returns [EObject current=null] : iv_rulebased_numeral= rulebased_numeral EOF ;
    public final EObject entryRulebased_numeral() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebased_numeral = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2315:2: (iv_rulebased_numeral= rulebased_numeral EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2316:2: iv_rulebased_numeral= rulebased_numeral EOF
            {
             newCompositeNode(grammarAccess.getBased_numeralRule()); 
            pushFollow(FOLLOW_rulebased_numeral_in_entryRulebased_numeral4958);
            iv_rulebased_numeral=rulebased_numeral();

            state._fsp--;

             current =iv_rulebased_numeral; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebased_numeral4968); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebased_numeral"


    // $ANTLR start "rulebased_numeral"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2323:1: rulebased_numeral returns [EObject current=null] : ( ( (lv_bnname1_0_0= RULE_EXTENDED_DIGIT ) ) ( (otherlv_1= '_' )? ( (lv_bnname2_2_0= RULE_EXTENDED_DIGIT ) ) )? ) ;
    public final EObject rulebased_numeral() throws RecognitionException {
        EObject current = null;

        Token lv_bnname1_0_0=null;
        Token otherlv_1=null;
        Token lv_bnname2_2_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2326:28: ( ( ( (lv_bnname1_0_0= RULE_EXTENDED_DIGIT ) ) ( (otherlv_1= '_' )? ( (lv_bnname2_2_0= RULE_EXTENDED_DIGIT ) ) )? ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2327:1: ( ( (lv_bnname1_0_0= RULE_EXTENDED_DIGIT ) ) ( (otherlv_1= '_' )? ( (lv_bnname2_2_0= RULE_EXTENDED_DIGIT ) ) )? )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2327:1: ( ( (lv_bnname1_0_0= RULE_EXTENDED_DIGIT ) ) ( (otherlv_1= '_' )? ( (lv_bnname2_2_0= RULE_EXTENDED_DIGIT ) ) )? )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2327:2: ( (lv_bnname1_0_0= RULE_EXTENDED_DIGIT ) ) ( (otherlv_1= '_' )? ( (lv_bnname2_2_0= RULE_EXTENDED_DIGIT ) ) )?
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2327:2: ( (lv_bnname1_0_0= RULE_EXTENDED_DIGIT ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2328:1: (lv_bnname1_0_0= RULE_EXTENDED_DIGIT )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2328:1: (lv_bnname1_0_0= RULE_EXTENDED_DIGIT )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2329:3: lv_bnname1_0_0= RULE_EXTENDED_DIGIT
            {
            lv_bnname1_0_0=(Token)match(input,RULE_EXTENDED_DIGIT,FOLLOW_RULE_EXTENDED_DIGIT_in_rulebased_numeral5010); 

            			newLeafNode(lv_bnname1_0_0, grammarAccess.getBased_numeralAccess().getBnname1Extended_digitTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getBased_numeralRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"bnname1",
                    		lv_bnname1_0_0, 
                    		"extended_digit");
            	    

            }


            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2345:2: ( (otherlv_1= '_' )? ( (lv_bnname2_2_0= RULE_EXTENDED_DIGIT ) ) )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==RULE_EXTENDED_DIGIT||LA32_0==48) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2345:3: (otherlv_1= '_' )? ( (lv_bnname2_2_0= RULE_EXTENDED_DIGIT ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2345:3: (otherlv_1= '_' )?
                    int alt31=2;
                    int LA31_0 = input.LA(1);

                    if ( (LA31_0==48) ) {
                        alt31=1;
                    }
                    switch (alt31) {
                        case 1 :
                            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2345:5: otherlv_1= '_'
                            {
                            otherlv_1=(Token)match(input,48,FOLLOW_48_in_rulebased_numeral5029); 

                                	newLeafNode(otherlv_1, grammarAccess.getBased_numeralAccess().get_Keyword_1_0());
                                

                            }
                            break;

                    }

                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2349:3: ( (lv_bnname2_2_0= RULE_EXTENDED_DIGIT ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2350:1: (lv_bnname2_2_0= RULE_EXTENDED_DIGIT )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2350:1: (lv_bnname2_2_0= RULE_EXTENDED_DIGIT )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2351:3: lv_bnname2_2_0= RULE_EXTENDED_DIGIT
                    {
                    lv_bnname2_2_0=(Token)match(input,RULE_EXTENDED_DIGIT,FOLLOW_RULE_EXTENDED_DIGIT_in_rulebased_numeral5048); 

                    			newLeafNode(lv_bnname2_2_0, grammarAccess.getBased_numeralAccess().getBnname2Extended_digitTerminalRuleCall_1_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBased_numeralRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"bnname2",
                            		lv_bnname2_2_0, 
                            		"extended_digit");
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebased_numeral"


    // $ANTLR start "entryRuleinteger"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2375:1: entryRuleinteger returns [EObject current=null] : iv_ruleinteger= ruleinteger EOF ;
    public final EObject entryRuleinteger() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleinteger = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2376:2: (iv_ruleinteger= ruleinteger EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2377:2: iv_ruleinteger= ruleinteger EOF
            {
             newCompositeNode(grammarAccess.getIntegerRule()); 
            pushFollow(FOLLOW_ruleinteger_in_entryRuleinteger5091);
            iv_ruleinteger=ruleinteger();

            state._fsp--;

             current =iv_ruleinteger; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleinteger5101); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleinteger"


    // $ANTLR start "ruleinteger"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2384:1: ruleinteger returns [EObject current=null] : ( ( (lv_value1_0_0= RULE_INT ) ) | ( (lv_value2_1_0= RULE_DIGIT ) ) ) ;
    public final EObject ruleinteger() throws RecognitionException {
        EObject current = null;

        Token lv_value1_0_0=null;
        Token lv_value2_1_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2387:28: ( ( ( (lv_value1_0_0= RULE_INT ) ) | ( (lv_value2_1_0= RULE_DIGIT ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2388:1: ( ( (lv_value1_0_0= RULE_INT ) ) | ( (lv_value2_1_0= RULE_DIGIT ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2388:1: ( ( (lv_value1_0_0= RULE_INT ) ) | ( (lv_value2_1_0= RULE_DIGIT ) ) )
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==RULE_INT) ) {
                alt33=1;
            }
            else if ( (LA33_0==RULE_DIGIT) ) {
                alt33=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }
            switch (alt33) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2388:2: ( (lv_value1_0_0= RULE_INT ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2388:2: ( (lv_value1_0_0= RULE_INT ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2389:1: (lv_value1_0_0= RULE_INT )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2389:1: (lv_value1_0_0= RULE_INT )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2390:3: lv_value1_0_0= RULE_INT
                    {
                    lv_value1_0_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleinteger5143); 

                    			newLeafNode(lv_value1_0_0, grammarAccess.getIntegerAccess().getValue1INTTerminalRuleCall_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getIntegerRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value1",
                            		lv_value1_0_0, 
                            		"INT");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2407:6: ( (lv_value2_1_0= RULE_DIGIT ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2407:6: ( (lv_value2_1_0= RULE_DIGIT ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2408:1: (lv_value2_1_0= RULE_DIGIT )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2408:1: (lv_value2_1_0= RULE_DIGIT )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2409:3: lv_value2_1_0= RULE_DIGIT
                    {
                    lv_value2_1_0=(Token)match(input,RULE_DIGIT,FOLLOW_RULE_DIGIT_in_ruleinteger5171); 

                    			newLeafNode(lv_value2_1_0, grammarAccess.getIntegerAccess().getValue2DigitTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getIntegerRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value2",
                            		lv_value2_1_0, 
                            		"digit");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleinteger"


    // $ANTLR start "entryRuledispatch_condition"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2433:1: entryRuledispatch_condition returns [EObject current=null] : iv_ruledispatch_condition= ruledispatch_condition EOF ;
    public final EObject entryRuledispatch_condition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruledispatch_condition = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2434:2: (iv_ruledispatch_condition= ruledispatch_condition EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2435:2: iv_ruledispatch_condition= ruledispatch_condition EOF
            {
             newCompositeNode(grammarAccess.getDispatch_conditionRule()); 
            pushFollow(FOLLOW_ruledispatch_condition_in_entryRuledispatch_condition5212);
            iv_ruledispatch_condition=ruledispatch_condition();

            state._fsp--;

             current =iv_ruledispatch_condition; 
            match(input,EOF,FOLLOW_EOF_in_entryRuledispatch_condition5222); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuledispatch_condition"


    // $ANTLR start "ruledispatch_condition"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2442:1: ruledispatch_condition returns [EObject current=null] : ( ( (lv_dcname3_0_0= 'on dispatch' ) ) ( (lv_dcname1_1_0= ruledispatch_trigger_condition ) )? (otherlv_2= 'frozen' ( (lv_dcname2_3_0= rulefrozen_ports ) ) )? ) ;
    public final EObject ruledispatch_condition() throws RecognitionException {
        EObject current = null;

        Token lv_dcname3_0_0=null;
        Token otherlv_2=null;
        EObject lv_dcname1_1_0 = null;

        EObject lv_dcname2_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2445:28: ( ( ( (lv_dcname3_0_0= 'on dispatch' ) ) ( (lv_dcname1_1_0= ruledispatch_trigger_condition ) )? (otherlv_2= 'frozen' ( (lv_dcname2_3_0= rulefrozen_ports ) ) )? ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2446:1: ( ( (lv_dcname3_0_0= 'on dispatch' ) ) ( (lv_dcname1_1_0= ruledispatch_trigger_condition ) )? (otherlv_2= 'frozen' ( (lv_dcname2_3_0= rulefrozen_ports ) ) )? )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2446:1: ( ( (lv_dcname3_0_0= 'on dispatch' ) ) ( (lv_dcname1_1_0= ruledispatch_trigger_condition ) )? (otherlv_2= 'frozen' ( (lv_dcname2_3_0= rulefrozen_ports ) ) )? )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2446:2: ( (lv_dcname3_0_0= 'on dispatch' ) ) ( (lv_dcname1_1_0= ruledispatch_trigger_condition ) )? (otherlv_2= 'frozen' ( (lv_dcname2_3_0= rulefrozen_ports ) ) )?
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2446:2: ( (lv_dcname3_0_0= 'on dispatch' ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2447:1: (lv_dcname3_0_0= 'on dispatch' )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2447:1: (lv_dcname3_0_0= 'on dispatch' )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2448:3: lv_dcname3_0_0= 'on dispatch'
            {
            lv_dcname3_0_0=(Token)match(input,52,FOLLOW_52_in_ruledispatch_condition5265); 

                    newLeafNode(lv_dcname3_0_0, grammarAccess.getDispatch_conditionAccess().getDcname3OnDispatchKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDispatch_conditionRule());
            	        }
                   		setWithLastConsumed(current, "dcname3", lv_dcname3_0_0, "on dispatch");
            	    

            }


            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2461:2: ( (lv_dcname1_1_0= ruledispatch_trigger_condition ) )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==RULE_STRING_LITERAL||LA34_0==15||LA34_0==54) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2462:1: (lv_dcname1_1_0= ruledispatch_trigger_condition )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2462:1: (lv_dcname1_1_0= ruledispatch_trigger_condition )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2463:3: lv_dcname1_1_0= ruledispatch_trigger_condition
                    {
                     
                    	        newCompositeNode(grammarAccess.getDispatch_conditionAccess().getDcname1Dispatch_trigger_conditionParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruledispatch_trigger_condition_in_ruledispatch_condition5299);
                    lv_dcname1_1_0=ruledispatch_trigger_condition();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDispatch_conditionRule());
                    	        }
                           		set(
                           			current, 
                           			"dcname1",
                            		lv_dcname1_1_0, 
                            		"dispatch_trigger_condition");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2479:3: (otherlv_2= 'frozen' ( (lv_dcname2_3_0= rulefrozen_ports ) ) )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==53) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2479:5: otherlv_2= 'frozen' ( (lv_dcname2_3_0= rulefrozen_ports ) )
                    {
                    otherlv_2=(Token)match(input,53,FOLLOW_53_in_ruledispatch_condition5313); 

                        	newLeafNode(otherlv_2, grammarAccess.getDispatch_conditionAccess().getFrozenKeyword_2_0());
                        
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2483:1: ( (lv_dcname2_3_0= rulefrozen_ports ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2484:1: (lv_dcname2_3_0= rulefrozen_ports )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2484:1: (lv_dcname2_3_0= rulefrozen_ports )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2485:3: lv_dcname2_3_0= rulefrozen_ports
                    {
                     
                    	        newCompositeNode(grammarAccess.getDispatch_conditionAccess().getDcname2Frozen_portsParserRuleCall_2_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulefrozen_ports_in_ruledispatch_condition5334);
                    lv_dcname2_3_0=rulefrozen_ports();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDispatch_conditionRule());
                    	        }
                           		set(
                           			current, 
                           			"dcname2",
                            		lv_dcname2_3_0, 
                            		"frozen_ports");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruledispatch_condition"


    // $ANTLR start "entryRuledispatch_trigger_condition"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2509:1: entryRuledispatch_trigger_condition returns [EObject current=null] : iv_ruledispatch_trigger_condition= ruledispatch_trigger_condition EOF ;
    public final EObject entryRuledispatch_trigger_condition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruledispatch_trigger_condition = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2510:2: (iv_ruledispatch_trigger_condition= ruledispatch_trigger_condition EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2511:2: iv_ruledispatch_trigger_condition= ruledispatch_trigger_condition EOF
            {
             newCompositeNode(grammarAccess.getDispatch_trigger_conditionRule()); 
            pushFollow(FOLLOW_ruledispatch_trigger_condition_in_entryRuledispatch_trigger_condition5372);
            iv_ruledispatch_trigger_condition=ruledispatch_trigger_condition();

            state._fsp--;

             current =iv_ruledispatch_trigger_condition; 
            match(input,EOF,FOLLOW_EOF_in_entryRuledispatch_trigger_condition5382); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuledispatch_trigger_condition"


    // $ANTLR start "ruledispatch_trigger_condition"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2518:1: ruledispatch_trigger_condition returns [EObject current=null] : ( ( (lv_dtcname1_0_0= ruledispatch_trigger_logical_expression ) ) | ( (lv_dtcname4_1_0= 'stop' ) ) | ( (lv_dtcname2_2_0= rulecompletion_relative_timeout_condition_and_catch ) ) ) ;
    public final EObject ruledispatch_trigger_condition() throws RecognitionException {
        EObject current = null;

        Token lv_dtcname4_1_0=null;
        EObject lv_dtcname1_0_0 = null;

        EObject lv_dtcname2_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2521:28: ( ( ( (lv_dtcname1_0_0= ruledispatch_trigger_logical_expression ) ) | ( (lv_dtcname4_1_0= 'stop' ) ) | ( (lv_dtcname2_2_0= rulecompletion_relative_timeout_condition_and_catch ) ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2522:1: ( ( (lv_dtcname1_0_0= ruledispatch_trigger_logical_expression ) ) | ( (lv_dtcname4_1_0= 'stop' ) ) | ( (lv_dtcname2_2_0= rulecompletion_relative_timeout_condition_and_catch ) ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2522:1: ( ( (lv_dtcname1_0_0= ruledispatch_trigger_logical_expression ) ) | ( (lv_dtcname4_1_0= 'stop' ) ) | ( (lv_dtcname2_2_0= rulecompletion_relative_timeout_condition_and_catch ) ) )
            int alt36=3;
            switch ( input.LA(1) ) {
            case RULE_STRING_LITERAL:
                {
                alt36=1;
                }
                break;
            case 54:
                {
                alt36=2;
                }
                break;
            case 15:
                {
                alt36=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 36, 0, input);

                throw nvae;
            }

            switch (alt36) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2522:2: ( (lv_dtcname1_0_0= ruledispatch_trigger_logical_expression ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2522:2: ( (lv_dtcname1_0_0= ruledispatch_trigger_logical_expression ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2523:1: (lv_dtcname1_0_0= ruledispatch_trigger_logical_expression )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2523:1: (lv_dtcname1_0_0= ruledispatch_trigger_logical_expression )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2524:3: lv_dtcname1_0_0= ruledispatch_trigger_logical_expression
                    {
                     
                    	        newCompositeNode(grammarAccess.getDispatch_trigger_conditionAccess().getDtcname1Dispatch_trigger_logical_expressionParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruledispatch_trigger_logical_expression_in_ruledispatch_trigger_condition5428);
                    lv_dtcname1_0_0=ruledispatch_trigger_logical_expression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDispatch_trigger_conditionRule());
                    	        }
                           		set(
                           			current, 
                           			"dtcname1",
                            		lv_dtcname1_0_0, 
                            		"dispatch_trigger_logical_expression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2541:6: ( (lv_dtcname4_1_0= 'stop' ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2541:6: ( (lv_dtcname4_1_0= 'stop' ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2542:1: (lv_dtcname4_1_0= 'stop' )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2542:1: (lv_dtcname4_1_0= 'stop' )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2543:3: lv_dtcname4_1_0= 'stop'
                    {
                    lv_dtcname4_1_0=(Token)match(input,54,FOLLOW_54_in_ruledispatch_trigger_condition5452); 

                            newLeafNode(lv_dtcname4_1_0, grammarAccess.getDispatch_trigger_conditionAccess().getDtcname4StopKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getDispatch_trigger_conditionRule());
                    	        }
                           		setWithLastConsumed(current, "dtcname4", lv_dtcname4_1_0, "stop");
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2557:6: ( (lv_dtcname2_2_0= rulecompletion_relative_timeout_condition_and_catch ) )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2557:6: ( (lv_dtcname2_2_0= rulecompletion_relative_timeout_condition_and_catch ) )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2558:1: (lv_dtcname2_2_0= rulecompletion_relative_timeout_condition_and_catch )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2558:1: (lv_dtcname2_2_0= rulecompletion_relative_timeout_condition_and_catch )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2559:3: lv_dtcname2_2_0= rulecompletion_relative_timeout_condition_and_catch
                    {
                     
                    	        newCompositeNode(grammarAccess.getDispatch_trigger_conditionAccess().getDtcname2Completion_relative_timeout_condition_and_catchParserRuleCall_2_0()); 
                    	    
                    pushFollow(FOLLOW_rulecompletion_relative_timeout_condition_and_catch_in_ruledispatch_trigger_condition5492);
                    lv_dtcname2_2_0=rulecompletion_relative_timeout_condition_and_catch();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDispatch_trigger_conditionRule());
                    	        }
                           		set(
                           			current, 
                           			"dtcname2",
                            		lv_dtcname2_2_0, 
                            		"completion_relative_timeout_condition_and_catch");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruledispatch_trigger_condition"


    // $ANTLR start "entryRuledispatch_trigger_logical_expression"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2583:1: entryRuledispatch_trigger_logical_expression returns [EObject current=null] : iv_ruledispatch_trigger_logical_expression= ruledispatch_trigger_logical_expression EOF ;
    public final EObject entryRuledispatch_trigger_logical_expression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruledispatch_trigger_logical_expression = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2584:2: (iv_ruledispatch_trigger_logical_expression= ruledispatch_trigger_logical_expression EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2585:2: iv_ruledispatch_trigger_logical_expression= ruledispatch_trigger_logical_expression EOF
            {
             newCompositeNode(grammarAccess.getDispatch_trigger_logical_expressionRule()); 
            pushFollow(FOLLOW_ruledispatch_trigger_logical_expression_in_entryRuledispatch_trigger_logical_expression5528);
            iv_ruledispatch_trigger_logical_expression=ruledispatch_trigger_logical_expression();

            state._fsp--;

             current =iv_ruledispatch_trigger_logical_expression; 
            match(input,EOF,FOLLOW_EOF_in_entryRuledispatch_trigger_logical_expression5538); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuledispatch_trigger_logical_expression"


    // $ANTLR start "ruledispatch_trigger_logical_expression"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2592:1: ruledispatch_trigger_logical_expression returns [EObject current=null] : ( ( (lv_dtlename1_0_0= ruledispatch_conjunction ) ) (otherlv_1= 'or' ( (lv_dtlename2_2_0= ruledispatch_conjunction ) ) )* ) ;
    public final EObject ruledispatch_trigger_logical_expression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_dtlename1_0_0 = null;

        EObject lv_dtlename2_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2595:28: ( ( ( (lv_dtlename1_0_0= ruledispatch_conjunction ) ) (otherlv_1= 'or' ( (lv_dtlename2_2_0= ruledispatch_conjunction ) ) )* ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2596:1: ( ( (lv_dtlename1_0_0= ruledispatch_conjunction ) ) (otherlv_1= 'or' ( (lv_dtlename2_2_0= ruledispatch_conjunction ) ) )* )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2596:1: ( ( (lv_dtlename1_0_0= ruledispatch_conjunction ) ) (otherlv_1= 'or' ( (lv_dtlename2_2_0= ruledispatch_conjunction ) ) )* )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2596:2: ( (lv_dtlename1_0_0= ruledispatch_conjunction ) ) (otherlv_1= 'or' ( (lv_dtlename2_2_0= ruledispatch_conjunction ) ) )*
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2596:2: ( (lv_dtlename1_0_0= ruledispatch_conjunction ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2597:1: (lv_dtlename1_0_0= ruledispatch_conjunction )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2597:1: (lv_dtlename1_0_0= ruledispatch_conjunction )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2598:3: lv_dtlename1_0_0= ruledispatch_conjunction
            {
             
            	        newCompositeNode(grammarAccess.getDispatch_trigger_logical_expressionAccess().getDtlename1Dispatch_conjunctionParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruledispatch_conjunction_in_ruledispatch_trigger_logical_expression5584);
            lv_dtlename1_0_0=ruledispatch_conjunction();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getDispatch_trigger_logical_expressionRule());
            	        }
                   		set(
                   			current, 
                   			"dtlename1",
                    		lv_dtlename1_0_0, 
                    		"dispatch_conjunction");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2614:2: (otherlv_1= 'or' ( (lv_dtlename2_2_0= ruledispatch_conjunction ) ) )*
            loop37:
            do {
                int alt37=2;
                int LA37_0 = input.LA(1);

                if ( (LA37_0==21) ) {
                    alt37=1;
                }


                switch (alt37) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2614:4: otherlv_1= 'or' ( (lv_dtlename2_2_0= ruledispatch_conjunction ) )
            	    {
            	    otherlv_1=(Token)match(input,21,FOLLOW_21_in_ruledispatch_trigger_logical_expression5597); 

            	        	newLeafNode(otherlv_1, grammarAccess.getDispatch_trigger_logical_expressionAccess().getOrKeyword_1_0());
            	        
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2618:1: ( (lv_dtlename2_2_0= ruledispatch_conjunction ) )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2619:1: (lv_dtlename2_2_0= ruledispatch_conjunction )
            	    {
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2619:1: (lv_dtlename2_2_0= ruledispatch_conjunction )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2620:3: lv_dtlename2_2_0= ruledispatch_conjunction
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getDispatch_trigger_logical_expressionAccess().getDtlename2Dispatch_conjunctionParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruledispatch_conjunction_in_ruledispatch_trigger_logical_expression5618);
            	    lv_dtlename2_2_0=ruledispatch_conjunction();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getDispatch_trigger_logical_expressionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"dtlename2",
            	            		lv_dtlename2_2_0, 
            	            		"dispatch_conjunction");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop37;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruledispatch_trigger_logical_expression"


    // $ANTLR start "entryRuledispatch_conjunction"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2644:1: entryRuledispatch_conjunction returns [EObject current=null] : iv_ruledispatch_conjunction= ruledispatch_conjunction EOF ;
    public final EObject entryRuledispatch_conjunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruledispatch_conjunction = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2645:2: (iv_ruledispatch_conjunction= ruledispatch_conjunction EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2646:2: iv_ruledispatch_conjunction= ruledispatch_conjunction EOF
            {
             newCompositeNode(grammarAccess.getDispatch_conjunctionRule()); 
            pushFollow(FOLLOW_ruledispatch_conjunction_in_entryRuledispatch_conjunction5656);
            iv_ruledispatch_conjunction=ruledispatch_conjunction();

            state._fsp--;

             current =iv_ruledispatch_conjunction; 
            match(input,EOF,FOLLOW_EOF_in_entryRuledispatch_conjunction5666); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuledispatch_conjunction"


    // $ANTLR start "ruledispatch_conjunction"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2653:1: ruledispatch_conjunction returns [EObject current=null] : ( ( (lv_dcname1_0_0= ruledispatch_trigger ) ) (otherlv_1= 'and' ( (lv_dcname2_2_0= ruledispatch_trigger ) ) )* ) ;
    public final EObject ruledispatch_conjunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_dcname1_0_0 = null;

        EObject lv_dcname2_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2656:28: ( ( ( (lv_dcname1_0_0= ruledispatch_trigger ) ) (otherlv_1= 'and' ( (lv_dcname2_2_0= ruledispatch_trigger ) ) )* ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2657:1: ( ( (lv_dcname1_0_0= ruledispatch_trigger ) ) (otherlv_1= 'and' ( (lv_dcname2_2_0= ruledispatch_trigger ) ) )* )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2657:1: ( ( (lv_dcname1_0_0= ruledispatch_trigger ) ) (otherlv_1= 'and' ( (lv_dcname2_2_0= ruledispatch_trigger ) ) )* )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2657:2: ( (lv_dcname1_0_0= ruledispatch_trigger ) ) (otherlv_1= 'and' ( (lv_dcname2_2_0= ruledispatch_trigger ) ) )*
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2657:2: ( (lv_dcname1_0_0= ruledispatch_trigger ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2658:1: (lv_dcname1_0_0= ruledispatch_trigger )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2658:1: (lv_dcname1_0_0= ruledispatch_trigger )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2659:3: lv_dcname1_0_0= ruledispatch_trigger
            {
             
            	        newCompositeNode(grammarAccess.getDispatch_conjunctionAccess().getDcname1Dispatch_triggerParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruledispatch_trigger_in_ruledispatch_conjunction5712);
            lv_dcname1_0_0=ruledispatch_trigger();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getDispatch_conjunctionRule());
            	        }
                   		set(
                   			current, 
                   			"dcname1",
                    		lv_dcname1_0_0, 
                    		"dispatch_trigger");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2675:2: (otherlv_1= 'and' ( (lv_dcname2_2_0= ruledispatch_trigger ) ) )*
            loop38:
            do {
                int alt38=2;
                int LA38_0 = input.LA(1);

                if ( (LA38_0==20) ) {
                    alt38=1;
                }


                switch (alt38) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2675:4: otherlv_1= 'and' ( (lv_dcname2_2_0= ruledispatch_trigger ) )
            	    {
            	    otherlv_1=(Token)match(input,20,FOLLOW_20_in_ruledispatch_conjunction5725); 

            	        	newLeafNode(otherlv_1, grammarAccess.getDispatch_conjunctionAccess().getAndKeyword_1_0());
            	        
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2679:1: ( (lv_dcname2_2_0= ruledispatch_trigger ) )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2680:1: (lv_dcname2_2_0= ruledispatch_trigger )
            	    {
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2680:1: (lv_dcname2_2_0= ruledispatch_trigger )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2681:3: lv_dcname2_2_0= ruledispatch_trigger
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getDispatch_conjunctionAccess().getDcname2Dispatch_triggerParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruledispatch_trigger_in_ruledispatch_conjunction5746);
            	    lv_dcname2_2_0=ruledispatch_trigger();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getDispatch_conjunctionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"dcname2",
            	            		lv_dcname2_2_0, 
            	            		"dispatch_trigger");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop38;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruledispatch_conjunction"


    // $ANTLR start "entryRulecompletion_relative_timeout_condition_and_catch"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2705:1: entryRulecompletion_relative_timeout_condition_and_catch returns [EObject current=null] : iv_rulecompletion_relative_timeout_condition_and_catch= rulecompletion_relative_timeout_condition_and_catch EOF ;
    public final EObject entryRulecompletion_relative_timeout_condition_and_catch() throws RecognitionException {
        EObject current = null;

        EObject iv_rulecompletion_relative_timeout_condition_and_catch = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2706:2: (iv_rulecompletion_relative_timeout_condition_and_catch= rulecompletion_relative_timeout_condition_and_catch EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2707:2: iv_rulecompletion_relative_timeout_condition_and_catch= rulecompletion_relative_timeout_condition_and_catch EOF
            {
             newCompositeNode(grammarAccess.getCompletion_relative_timeout_condition_and_catchRule()); 
            pushFollow(FOLLOW_rulecompletion_relative_timeout_condition_and_catch_in_entryRulecompletion_relative_timeout_condition_and_catch5784);
            iv_rulecompletion_relative_timeout_condition_and_catch=rulecompletion_relative_timeout_condition_and_catch();

            state._fsp--;

             current =iv_rulecompletion_relative_timeout_condition_and_catch; 
            match(input,EOF,FOLLOW_EOF_in_entryRulecompletion_relative_timeout_condition_and_catch5794); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulecompletion_relative_timeout_condition_and_catch"


    // $ANTLR start "rulecompletion_relative_timeout_condition_and_catch"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2714:1: rulecompletion_relative_timeout_condition_and_catch returns [EObject current=null] : ( ( (lv_cdtcacname1_0_0= 'timeout' ) ) ( (lv_cdtcacname2_1_0= rulebehavior_time ) )? ) ;
    public final EObject rulecompletion_relative_timeout_condition_and_catch() throws RecognitionException {
        EObject current = null;

        Token lv_cdtcacname1_0_0=null;
        EObject lv_cdtcacname2_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2717:28: ( ( ( (lv_cdtcacname1_0_0= 'timeout' ) ) ( (lv_cdtcacname2_1_0= rulebehavior_time ) )? ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2718:1: ( ( (lv_cdtcacname1_0_0= 'timeout' ) ) ( (lv_cdtcacname2_1_0= rulebehavior_time ) )? )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2718:1: ( ( (lv_cdtcacname1_0_0= 'timeout' ) ) ( (lv_cdtcacname2_1_0= rulebehavior_time ) )? )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2718:2: ( (lv_cdtcacname1_0_0= 'timeout' ) ) ( (lv_cdtcacname2_1_0= rulebehavior_time ) )?
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2718:2: ( (lv_cdtcacname1_0_0= 'timeout' ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2719:1: (lv_cdtcacname1_0_0= 'timeout' )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2719:1: (lv_cdtcacname1_0_0= 'timeout' )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2720:3: lv_cdtcacname1_0_0= 'timeout'
            {
            lv_cdtcacname1_0_0=(Token)match(input,15,FOLLOW_15_in_rulecompletion_relative_timeout_condition_and_catch5837); 

                    newLeafNode(lv_cdtcacname1_0_0, grammarAccess.getCompletion_relative_timeout_condition_and_catchAccess().getCdtcacname1TimeoutKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getCompletion_relative_timeout_condition_and_catchRule());
            	        }
                   		setWithLastConsumed(current, "cdtcacname1", lv_cdtcacname1_0_0, "timeout");
            	    

            }


            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2733:2: ( (lv_cdtcacname2_1_0= rulebehavior_time ) )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==RULE_DIGIT||LA39_0==RULE_INT) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2734:1: (lv_cdtcacname2_1_0= rulebehavior_time )
                    {
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2734:1: (lv_cdtcacname2_1_0= rulebehavior_time )
                    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2735:3: lv_cdtcacname2_1_0= rulebehavior_time
                    {
                     
                    	        newCompositeNode(grammarAccess.getCompletion_relative_timeout_condition_and_catchAccess().getCdtcacname2Behavior_timeParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulebehavior_time_in_rulecompletion_relative_timeout_condition_and_catch5871);
                    lv_cdtcacname2_1_0=rulebehavior_time();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCompletion_relative_timeout_condition_and_catchRule());
                    	        }
                           		set(
                           			current, 
                           			"cdtcacname2",
                            		lv_cdtcacname2_1_0, 
                            		"behavior_time");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulecompletion_relative_timeout_condition_and_catch"


    // $ANTLR start "entryRuledispatch_trigger"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2759:1: entryRuledispatch_trigger returns [EObject current=null] : iv_ruledispatch_trigger= ruledispatch_trigger EOF ;
    public final EObject entryRuledispatch_trigger() throws RecognitionException {
        EObject current = null;

        EObject iv_ruledispatch_trigger = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2760:2: (iv_ruledispatch_trigger= ruledispatch_trigger EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2761:2: iv_ruledispatch_trigger= ruledispatch_trigger EOF
            {
             newCompositeNode(grammarAccess.getDispatch_triggerRule()); 
            pushFollow(FOLLOW_ruledispatch_trigger_in_entryRuledispatch_trigger5908);
            iv_ruledispatch_trigger=ruledispatch_trigger();

            state._fsp--;

             current =iv_ruledispatch_trigger; 
            match(input,EOF,FOLLOW_EOF_in_entryRuledispatch_trigger5918); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuledispatch_trigger"


    // $ANTLR start "ruledispatch_trigger"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2768:1: ruledispatch_trigger returns [EObject current=null] : ( (lv_dtname_0_0= RULE_STRING_LITERAL ) ) ;
    public final EObject ruledispatch_trigger() throws RecognitionException {
        EObject current = null;

        Token lv_dtname_0_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2771:28: ( ( (lv_dtname_0_0= RULE_STRING_LITERAL ) ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2772:1: ( (lv_dtname_0_0= RULE_STRING_LITERAL ) )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2772:1: ( (lv_dtname_0_0= RULE_STRING_LITERAL ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2773:1: (lv_dtname_0_0= RULE_STRING_LITERAL )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2773:1: (lv_dtname_0_0= RULE_STRING_LITERAL )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2774:3: lv_dtname_0_0= RULE_STRING_LITERAL
            {
            lv_dtname_0_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_ruledispatch_trigger5959); 

            			newLeafNode(lv_dtname_0_0, grammarAccess.getDispatch_triggerAccess().getDtnameString_literalTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDispatch_triggerRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"dtname",
                    		lv_dtname_0_0, 
                    		"string_literal");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruledispatch_trigger"


    // $ANTLR start "entryRulefrozen_ports"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2798:1: entryRulefrozen_ports returns [EObject current=null] : iv_rulefrozen_ports= rulefrozen_ports EOF ;
    public final EObject entryRulefrozen_ports() throws RecognitionException {
        EObject current = null;

        EObject iv_rulefrozen_ports = null;


        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2799:2: (iv_rulefrozen_ports= rulefrozen_ports EOF )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2800:2: iv_rulefrozen_ports= rulefrozen_ports EOF
            {
             newCompositeNode(grammarAccess.getFrozen_portsRule()); 
            pushFollow(FOLLOW_rulefrozen_ports_in_entryRulefrozen_ports5999);
            iv_rulefrozen_ports=rulefrozen_ports();

            state._fsp--;

             current =iv_rulefrozen_ports; 
            match(input,EOF,FOLLOW_EOF_in_entryRulefrozen_ports6009); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulefrozen_ports"


    // $ANTLR start "rulefrozen_ports"
    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2807:1: rulefrozen_ports returns [EObject current=null] : ( ( (lv_fpname1_0_0= RULE_STRING_LITERAL ) ) (otherlv_1= ',' ( (lv_fpname2_2_0= RULE_STRING_LITERAL ) ) )* ) ;
    public final EObject rulefrozen_ports() throws RecognitionException {
        EObject current = null;

        Token lv_fpname1_0_0=null;
        Token otherlv_1=null;
        Token lv_fpname2_2_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2810:28: ( ( ( (lv_fpname1_0_0= RULE_STRING_LITERAL ) ) (otherlv_1= ',' ( (lv_fpname2_2_0= RULE_STRING_LITERAL ) ) )* ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2811:1: ( ( (lv_fpname1_0_0= RULE_STRING_LITERAL ) ) (otherlv_1= ',' ( (lv_fpname2_2_0= RULE_STRING_LITERAL ) ) )* )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2811:1: ( ( (lv_fpname1_0_0= RULE_STRING_LITERAL ) ) (otherlv_1= ',' ( (lv_fpname2_2_0= RULE_STRING_LITERAL ) ) )* )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2811:2: ( (lv_fpname1_0_0= RULE_STRING_LITERAL ) ) (otherlv_1= ',' ( (lv_fpname2_2_0= RULE_STRING_LITERAL ) ) )*
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2811:2: ( (lv_fpname1_0_0= RULE_STRING_LITERAL ) )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2812:1: (lv_fpname1_0_0= RULE_STRING_LITERAL )
            {
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2812:1: (lv_fpname1_0_0= RULE_STRING_LITERAL )
            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2813:3: lv_fpname1_0_0= RULE_STRING_LITERAL
            {
            lv_fpname1_0_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulefrozen_ports6051); 

            			newLeafNode(lv_fpname1_0_0, grammarAccess.getFrozen_portsAccess().getFpname1String_literalTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getFrozen_portsRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"fpname1",
                    		lv_fpname1_0_0, 
                    		"string_literal");
            	    

            }


            }

            // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2829:2: (otherlv_1= ',' ( (lv_fpname2_2_0= RULE_STRING_LITERAL ) ) )*
            loop40:
            do {
                int alt40=2;
                int LA40_0 = input.LA(1);

                if ( (LA40_0==55) ) {
                    alt40=1;
                }


                switch (alt40) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2829:4: otherlv_1= ',' ( (lv_fpname2_2_0= RULE_STRING_LITERAL ) )
            	    {
            	    otherlv_1=(Token)match(input,55,FOLLOW_55_in_rulefrozen_ports6069); 

            	        	newLeafNode(otherlv_1, grammarAccess.getFrozen_portsAccess().getCommaKeyword_1_0());
            	        
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2833:1: ( (lv_fpname2_2_0= RULE_STRING_LITERAL ) )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2834:1: (lv_fpname2_2_0= RULE_STRING_LITERAL )
            	    {
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2834:1: (lv_fpname2_2_0= RULE_STRING_LITERAL )
            	    // ../org.topcased.adele.xtext.baguards/src-gen/org/topcased/adele/xtext/baguards/parser/antlr/internal/InternalBaGuards.g:2835:3: lv_fpname2_2_0= RULE_STRING_LITERAL
            	    {
            	    lv_fpname2_2_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulefrozen_ports6086); 

            	    			newLeafNode(lv_fpname2_2_0, grammarAccess.getFrozen_portsAccess().getFpname2String_literalTerminalRuleCall_1_1_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getFrozen_portsRule());
            	    	        }
            	           		addWithLastConsumed(
            	           			current, 
            	           			"fpname2",
            	            		lv_fpname2_2_0, 
            	            		"string_literal");
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop40;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulefrozen_ports"

    // Delegated rules


    protected DFA20 dfa20 = new DFA20(this);
    static final String DFA20_eotS =
        "\11\uffff";
    static final String DFA20_eofS =
        "\1\uffff\2\7\1\uffff\2\7\2\uffff\1\7";
    static final String DFA20_minS =
        "\6\5\2\uffff\1\5";
    static final String DFA20_maxS =
        "\1\7\1\61\1\63\1\7\2\61\2\uffff\1\63";
    static final String DFA20_acceptS =
        "\6\uffff\1\2\1\1\1\uffff";
    static final String DFA20_specialS =
        "\11\uffff}>";
    static final String[] DFA20_transitionS = {
            "\1\2\1\uffff\1\1",
            "\1\5\1\uffff\1\4\14\uffff\20\7\4\uffff\7\7\1\6\1\3\1\7",
            "\1\10\1\uffff\1\4\14\uffff\20\7\4\uffff\7\7\1\6\1\3\1\7\1"+
            "\uffff\1\7",
            "\1\5\1\uffff\1\4",
            "\1\5\1\uffff\1\4\14\uffff\20\7\4\uffff\7\7\1\6\1\3\1\7",
            "\1\5\1\uffff\1\4\14\uffff\20\7\4\uffff\7\7\1\6\1\3\1\7",
            "",
            "",
            "\1\5\1\uffff\1\4\14\uffff\20\7\4\uffff\7\7\1\6\1\3\1\7\1\uffff"+
            "\1\7"
    };

    static final short[] DFA20_eot = DFA.unpackEncodedString(DFA20_eotS);
    static final short[] DFA20_eof = DFA.unpackEncodedString(DFA20_eofS);
    static final char[] DFA20_min = DFA.unpackEncodedStringToUnsignedChars(DFA20_minS);
    static final char[] DFA20_max = DFA.unpackEncodedStringToUnsignedChars(DFA20_maxS);
    static final short[] DFA20_accept = DFA.unpackEncodedString(DFA20_acceptS);
    static final short[] DFA20_special = DFA.unpackEncodedString(DFA20_specialS);
    static final short[][] DFA20_transition;

    static {
        int numStates = DFA20_transitionS.length;
        DFA20_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA20_transition[i] = DFA.unpackEncodedString(DFA20_transitionS[i]);
        }
    }

    class DFA20 extends DFA {

        public DFA20(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 20;
            this.eot = DFA20_eot;
            this.eof = DFA20_eof;
            this.min = DFA20_min;
            this.max = DFA20_max;
            this.accept = DFA20_accept;
            this.special = DFA20_special;
            this.transition = DFA20_transition;
        }
        public String getDescription() {
            return "1718:1: ( ( (lv_nlname1_0_0= ruleinteger_literal ) ) | ( (lv_nlname2_1_0= rulereal_literal ) ) )";
        }
    }
 

    public static final BitSet FOLLOW_ruleGuard_in_entryRuleGuard75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGuard85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_condition_in_ruleGuard131 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleexecute_condition_in_ruleGuard158 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleexecute_condition_in_entryRuleexecute_condition194 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleexecute_condition204 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_ruleexecute_condition247 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebehavior_action_block_timeout_catch_in_ruleexecute_condition287 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_expression_in_ruleexecute_condition314 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebehavior_action_block_timeout_catch_in_entryRulebehavior_action_block_timeout_catch350 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebehavior_action_block_timeout_catch360 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rulebehavior_action_block_timeout_catch402 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_expression_in_entryRulevalue_expression450 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulevalue_expression460 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulerelation_in_rulevalue_expression506 = new BitSet(new long[]{0x0000000000700002L});
    public static final BitSet FOLLOW_rulelogical_operator_in_rulevalue_expression528 = new BitSet(new long[]{0x000000F0600000B0L});
    public static final BitSet FOLLOW_rulerelation_in_rulevalue_expression549 = new BitSet(new long[]{0x0000000000700002L});
    public static final BitSet FOLLOW_rulerelation_in_entryRulerelation587 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulerelation597 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulesimple_expression_in_rulerelation643 = new BitSet(new long[]{0x000000001F800002L});
    public static final BitSet FOLLOW_rulerelational_operator_in_rulerelation665 = new BitSet(new long[]{0x000000F0600000B0L});
    public static final BitSet FOLLOW_rulesimple_expression_in_rulerelation686 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulesimple_expression_in_entryRulesimple_expression724 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulesimple_expression734 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunary_adding_operator_in_rulesimple_expression780 = new BitSet(new long[]{0x000000F0600000B0L});
    public static final BitSet FOLLOW_ruleterm_in_rulesimple_expression802 = new BitSet(new long[]{0x0000000060000002L});
    public static final BitSet FOLLOW_rulebinary_adding_operator_in_rulesimple_expression824 = new BitSet(new long[]{0x000000F0600000B0L});
    public static final BitSet FOLLOW_ruleterm_in_rulesimple_expression845 = new BitSet(new long[]{0x0000000060000002L});
    public static final BitSet FOLLOW_ruleterm_in_entryRuleterm883 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleterm893 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulefactor_in_ruleterm939 = new BitSet(new long[]{0x0000000780000002L});
    public static final BitSet FOLLOW_rulemultiplying_operator_in_ruleterm961 = new BitSet(new long[]{0x000000F0600000B0L});
    public static final BitSet FOLLOW_rulefactor_in_ruleterm982 = new BitSet(new long[]{0x0000000780000002L});
    public static final BitSet FOLLOW_rulefactor_in_entryRulefactor1020 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulefactor1030 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_in_rulefactor1077 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_rulebinary_numeric_operator_in_rulefactor1099 = new BitSet(new long[]{0x000000C0000000B0L});
    public static final BitSet FOLLOW_rulevalue_in_rulefactor1120 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunary_numeric_operator_in_rulefactor1151 = new BitSet(new long[]{0x000000C0000000B0L});
    public static final BitSet FOLLOW_rulevalue_in_rulefactor1172 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunary_boolean_operator_in_rulefactor1201 = new BitSet(new long[]{0x000000C0000000B0L});
    public static final BitSet FOLLOW_rulevalue_in_rulefactor1222 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_in_entryRulevalue1259 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulevalue1269 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_variable_in_rulevalue1315 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_constant_in_rulevalue1342 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_variable_in_entryRulevalue_variable1378 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulevalue_variable1388 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulevalue_variable1430 = new BitSet(new long[]{0x0000000000070002L});
    public static final BitSet FOLLOW_16_in_rulevalue_variable1454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rulevalue_variable1491 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rulevalue_variable1528 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_constant_in_entryRulevalue_constant1579 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulevalue_constant1589 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleboolean_literal_in_rulevalue_constant1635 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeric_literal_in_rulevalue_constant1662 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulevalue_constant1686 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_rulevalue_constant1703 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulevalue_constant1720 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulelogical_operator_in_entryRulelogical_operator1762 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulelogical_operator1772 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rulelogical_operator1815 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rulelogical_operator1852 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rulelogical_operator1889 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulerelational_operator_in_entryRulerelational_operator1938 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulerelational_operator1948 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rulerelational_operator1991 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rulerelational_operator2028 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rulerelational_operator2065 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rulerelational_operator2102 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rulerelational_operator2139 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rulerelational_operator2176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebinary_adding_operator_in_entryRulebinary_adding_operator2225 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebinary_adding_operator2235 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_rulebinary_adding_operator2278 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rulebinary_adding_operator2315 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunary_adding_operator_in_entryRuleunary_adding_operator2364 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleunary_adding_operator2374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_ruleunary_adding_operator2417 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_ruleunary_adding_operator2454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulemultiplying_operator_in_entryRulemultiplying_operator2503 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulemultiplying_operator2513 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rulemultiplying_operator2556 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_rulemultiplying_operator2593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_rulemultiplying_operator2630 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_rulemultiplying_operator2667 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebinary_numeric_operator_in_entryRulebinary_numeric_operator2716 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebinary_numeric_operator2726 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_rulebinary_numeric_operator2768 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunary_numeric_operator_in_entryRuleunary_numeric_operator2816 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleunary_numeric_operator2826 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_ruleunary_numeric_operator2868 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunary_boolean_operator_in_entryRuleunary_boolean_operator2916 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleunary_boolean_operator2926 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_ruleunary_boolean_operator2968 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleboolean_literal_in_entryRuleboolean_literal3016 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleboolean_literal3026 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_ruleboolean_literal3069 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_ruleboolean_literal3106 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_value_in_entryRuleinteger_value3155 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleinteger_value3165 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeric_literal_in_ruleinteger_value3210 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebehavior_time_in_entryRulebehavior_time3245 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebehavior_time3255 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_value_in_rulebehavior_time3301 = new BitSet(new long[]{0x00007F0000000000L});
    public static final BitSet FOLLOW_ruleunit_identifier_in_rulebehavior_time3322 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunit_identifier_in_entryRuleunit_identifier3358 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleunit_identifier3368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_ruleunit_identifier3411 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_ruleunit_identifier3448 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_ruleunit_identifier3485 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_ruleunit_identifier3522 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_ruleunit_identifier3559 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_ruleunit_identifier3596 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_ruleunit_identifier3633 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeric_literal_in_entryRulenumeric_literal3682 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulenumeric_literal3692 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_literal_in_rulenumeric_literal3738 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulereal_literal_in_rulenumeric_literal3765 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_literal_in_entryRuleinteger_literal3801 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleinteger_literal3811 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledecimal_integer_literal_in_ruleinteger_literal3857 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebased_integer_literal_in_ruleinteger_literal3884 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulereal_literal_in_entryRulereal_literal3920 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulereal_literal3930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledecimal_real_literal_in_rulereal_literal3975 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledecimal_integer_literal_in_entryRuledecimal_integer_literal4010 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledecimal_integer_literal4020 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeral_in_ruledecimal_integer_literal4066 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_rulepositive_exponent_in_ruledecimal_integer_literal4087 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledecimal_real_literal_in_entryRuledecimal_real_literal4124 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledecimal_real_literal4134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeral_in_ruledecimal_real_literal4180 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_ruledecimal_real_literal4192 = new BitSet(new long[]{0x00000000000000A0L});
    public static final BitSet FOLLOW_rulenumeral_in_ruledecimal_real_literal4213 = new BitSet(new long[]{0x0006000000000002L});
    public static final BitSet FOLLOW_ruleexponent_in_ruledecimal_real_literal4234 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeral_in_entryRulenumeral4271 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulenumeral4281 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_in_rulenumeral4327 = new BitSet(new long[]{0x00010000000000A2L});
    public static final BitSet FOLLOW_48_in_rulenumeral4341 = new BitSet(new long[]{0x00000000000000A0L});
    public static final BitSet FOLLOW_ruleinteger_in_rulenumeral4364 = new BitSet(new long[]{0x00010000000000A2L});
    public static final BitSet FOLLOW_ruleexponent_in_entryRuleexponent4402 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleexponent4412 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_ruleexponent4450 = new BitSet(new long[]{0x00000000200000A0L});
    public static final BitSet FOLLOW_29_in_ruleexponent4463 = new BitSet(new long[]{0x00000000000000A0L});
    public static final BitSet FOLLOW_rulenumeral_in_ruleexponent4486 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_ruleexponent4506 = new BitSet(new long[]{0x00000000000000A0L});
    public static final BitSet FOLLOW_rulenumeral_in_ruleexponent4527 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulepositive_exponent_in_entryRulepositive_exponent4564 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulepositive_exponent4574 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_rulepositive_exponent4611 = new BitSet(new long[]{0x00000000200000A0L});
    public static final BitSet FOLLOW_29_in_rulepositive_exponent4624 = new BitSet(new long[]{0x00000000000000A0L});
    public static final BitSet FOLLOW_rulenumeral_in_rulepositive_exponent4647 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebased_integer_literal_in_entryRulebased_integer_literal4683 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebased_integer_literal4693 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebase_in_rulebased_integer_literal4739 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_51_in_rulebased_integer_literal4751 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rulebased_numeral_in_rulebased_integer_literal4772 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_51_in_rulebased_integer_literal4784 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_rulepositive_exponent_in_rulebased_integer_literal4805 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebase_in_entryRulebase4842 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebase4852 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DIGIT_in_rulebase4894 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_RULE_DIGIT_in_rulebase4916 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebased_numeral_in_entryRulebased_numeral4958 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebased_numeral4968 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_EXTENDED_DIGIT_in_rulebased_numeral5010 = new BitSet(new long[]{0x0001000000000042L});
    public static final BitSet FOLLOW_48_in_rulebased_numeral5029 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_RULE_EXTENDED_DIGIT_in_rulebased_numeral5048 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_in_entryRuleinteger5091 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleinteger5101 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleinteger5143 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DIGIT_in_ruleinteger5171 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_condition_in_entryRuledispatch_condition5212 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledispatch_condition5222 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_ruledispatch_condition5265 = new BitSet(new long[]{0x0060000000008012L});
    public static final BitSet FOLLOW_ruledispatch_trigger_condition_in_ruledispatch_condition5299 = new BitSet(new long[]{0x0020000000000002L});
    public static final BitSet FOLLOW_53_in_ruledispatch_condition5313 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rulefrozen_ports_in_ruledispatch_condition5334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_trigger_condition_in_entryRuledispatch_trigger_condition5372 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledispatch_trigger_condition5382 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_trigger_logical_expression_in_ruledispatch_trigger_condition5428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_54_in_ruledispatch_trigger_condition5452 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulecompletion_relative_timeout_condition_and_catch_in_ruledispatch_trigger_condition5492 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_trigger_logical_expression_in_entryRuledispatch_trigger_logical_expression5528 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledispatch_trigger_logical_expression5538 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_conjunction_in_ruledispatch_trigger_logical_expression5584 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_21_in_ruledispatch_trigger_logical_expression5597 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruledispatch_conjunction_in_ruledispatch_trigger_logical_expression5618 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_ruledispatch_conjunction_in_entryRuledispatch_conjunction5656 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledispatch_conjunction5666 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_trigger_in_ruledispatch_conjunction5712 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_20_in_ruledispatch_conjunction5725 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruledispatch_trigger_in_ruledispatch_conjunction5746 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_rulecompletion_relative_timeout_condition_and_catch_in_entryRulecompletion_relative_timeout_condition_and_catch5784 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulecompletion_relative_timeout_condition_and_catch5794 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rulecompletion_relative_timeout_condition_and_catch5837 = new BitSet(new long[]{0x00000000000000A2L});
    public static final BitSet FOLLOW_rulebehavior_time_in_rulecompletion_relative_timeout_condition_and_catch5871 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_trigger_in_entryRuledispatch_trigger5908 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledispatch_trigger5918 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_ruledispatch_trigger5959 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulefrozen_ports_in_entryRulefrozen_ports5999 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulefrozen_ports6009 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulefrozen_ports6051 = new BitSet(new long[]{0x0080000000000002L});
    public static final BitSet FOLLOW_55_in_rulefrozen_ports6069 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulefrozen_ports6086 = new BitSet(new long[]{0x0080000000000002L});

}