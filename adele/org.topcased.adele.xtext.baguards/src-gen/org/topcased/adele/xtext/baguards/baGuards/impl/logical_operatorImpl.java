/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.logical_operator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>logical operator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.logical_operatorImpl#getLoname1 <em>Loname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.logical_operatorImpl#getLoname2 <em>Loname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.logical_operatorImpl#getLoname3 <em>Loname3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class logical_operatorImpl extends MinimalEObjectImpl.Container implements logical_operator
{
  /**
   * The default value of the '{@link #getLoname1() <em>Loname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLoname1()
   * @generated
   * @ordered
   */
  protected static final String LONAME1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLoname1() <em>Loname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLoname1()
   * @generated
   * @ordered
   */
  protected String loname1 = LONAME1_EDEFAULT;

  /**
   * The default value of the '{@link #getLoname2() <em>Loname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLoname2()
   * @generated
   * @ordered
   */
  protected static final String LONAME2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLoname2() <em>Loname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLoname2()
   * @generated
   * @ordered
   */
  protected String loname2 = LONAME2_EDEFAULT;

  /**
   * The default value of the '{@link #getLoname3() <em>Loname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLoname3()
   * @generated
   * @ordered
   */
  protected static final String LONAME3_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLoname3() <em>Loname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLoname3()
   * @generated
   * @ordered
   */
  protected String loname3 = LONAME3_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected logical_operatorImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.LOGICAL_OPERATOR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLoname1()
  {
    return loname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLoname1(String newLoname1)
  {
    String oldLoname1 = loname1;
    loname1 = newLoname1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.LOGICAL_OPERATOR__LONAME1, oldLoname1, loname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLoname2()
  {
    return loname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLoname2(String newLoname2)
  {
    String oldLoname2 = loname2;
    loname2 = newLoname2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.LOGICAL_OPERATOR__LONAME2, oldLoname2, loname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLoname3()
  {
    return loname3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLoname3(String newLoname3)
  {
    String oldLoname3 = loname3;
    loname3 = newLoname3;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.LOGICAL_OPERATOR__LONAME3, oldLoname3, loname3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.LOGICAL_OPERATOR__LONAME1:
        return getLoname1();
      case BaGuardsPackage.LOGICAL_OPERATOR__LONAME2:
        return getLoname2();
      case BaGuardsPackage.LOGICAL_OPERATOR__LONAME3:
        return getLoname3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.LOGICAL_OPERATOR__LONAME1:
        setLoname1((String)newValue);
        return;
      case BaGuardsPackage.LOGICAL_OPERATOR__LONAME2:
        setLoname2((String)newValue);
        return;
      case BaGuardsPackage.LOGICAL_OPERATOR__LONAME3:
        setLoname3((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.LOGICAL_OPERATOR__LONAME1:
        setLoname1(LONAME1_EDEFAULT);
        return;
      case BaGuardsPackage.LOGICAL_OPERATOR__LONAME2:
        setLoname2(LONAME2_EDEFAULT);
        return;
      case BaGuardsPackage.LOGICAL_OPERATOR__LONAME3:
        setLoname3(LONAME3_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.LOGICAL_OPERATOR__LONAME1:
        return LONAME1_EDEFAULT == null ? loname1 != null : !LONAME1_EDEFAULT.equals(loname1);
      case BaGuardsPackage.LOGICAL_OPERATOR__LONAME2:
        return LONAME2_EDEFAULT == null ? loname2 != null : !LONAME2_EDEFAULT.equals(loname2);
      case BaGuardsPackage.LOGICAL_OPERATOR__LONAME3:
        return LONAME3_EDEFAULT == null ? loname3 != null : !LONAME3_EDEFAULT.equals(loname3);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (loname1: ");
    result.append(loname1);
    result.append(", loname2: ");
    result.append(loname2);
    result.append(", loname3: ");
    result.append(loname3);
    result.append(')');
    return result.toString();
  }

} //logical_operatorImpl
