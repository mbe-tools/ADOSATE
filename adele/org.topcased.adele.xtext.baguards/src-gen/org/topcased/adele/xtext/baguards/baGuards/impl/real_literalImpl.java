/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.decimal_real_literal;
import org.topcased.adele.xtext.baguards.baGuards.real_literal;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>real literal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.real_literalImpl#getRlname <em>Rlname</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class real_literalImpl extends MinimalEObjectImpl.Container implements real_literal
{
  /**
   * The cached value of the '{@link #getRlname() <em>Rlname</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRlname()
   * @generated
   * @ordered
   */
  protected decimal_real_literal rlname;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected real_literalImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.REAL_LITERAL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public decimal_real_literal getRlname()
  {
    return rlname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRlname(decimal_real_literal newRlname, NotificationChain msgs)
  {
    decimal_real_literal oldRlname = rlname;
    rlname = newRlname;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.REAL_LITERAL__RLNAME, oldRlname, newRlname);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRlname(decimal_real_literal newRlname)
  {
    if (newRlname != rlname)
    {
      NotificationChain msgs = null;
      if (rlname != null)
        msgs = ((InternalEObject)rlname).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.REAL_LITERAL__RLNAME, null, msgs);
      if (newRlname != null)
        msgs = ((InternalEObject)newRlname).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.REAL_LITERAL__RLNAME, null, msgs);
      msgs = basicSetRlname(newRlname, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.REAL_LITERAL__RLNAME, newRlname, newRlname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.REAL_LITERAL__RLNAME:
        return basicSetRlname(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.REAL_LITERAL__RLNAME:
        return getRlname();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.REAL_LITERAL__RLNAME:
        setRlname((decimal_real_literal)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.REAL_LITERAL__RLNAME:
        setRlname((decimal_real_literal)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.REAL_LITERAL__RLNAME:
        return rlname != null;
    }
    return super.eIsSet(featureID);
  }

} //real_literalImpl
