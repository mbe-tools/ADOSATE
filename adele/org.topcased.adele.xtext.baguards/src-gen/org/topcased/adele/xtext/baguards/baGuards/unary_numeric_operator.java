/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>unary numeric operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.unary_numeric_operator#getUnoname <em>Unoname</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getunary_numeric_operator()
 * @model
 * @generated
 */
public interface unary_numeric_operator extends EObject
{
  /**
   * Returns the value of the '<em><b>Unoname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Unoname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Unoname</em>' attribute.
   * @see #setUnoname(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getunary_numeric_operator_Unoname()
   * @model
   * @generated
   */
  String getUnoname();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.unary_numeric_operator#getUnoname <em>Unoname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Unoname</em>' attribute.
   * @see #getUnoname()
   * @generated
   */
  void setUnoname(String value);

} // unary_numeric_operator
