/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>based numeral</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.based_numeral#getBnname1 <em>Bnname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.based_numeral#getBnname2 <em>Bnname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getbased_numeral()
 * @model
 * @generated
 */
public interface based_numeral extends EObject
{
  /**
   * Returns the value of the '<em><b>Bnname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bnname1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bnname1</em>' attribute.
   * @see #setBnname1(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getbased_numeral_Bnname1()
   * @model
   * @generated
   */
  String getBnname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.based_numeral#getBnname1 <em>Bnname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bnname1</em>' attribute.
   * @see #getBnname1()
   * @generated
   */
  void setBnname1(String value);

  /**
   * Returns the value of the '<em><b>Bnname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bnname2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bnname2</em>' attribute.
   * @see #setBnname2(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getbased_numeral_Bnname2()
   * @model
   * @generated
   */
  String getBnname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.based_numeral#getBnname2 <em>Bnname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bnname2</em>' attribute.
   * @see #getBnname2()
   * @generated
   */
  void setBnname2(String value);

} // based_numeral
