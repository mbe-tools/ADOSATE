package org.topcased.adele.xtext.baguards.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.Guard;
import org.topcased.adele.xtext.baguards.baGuards.base;
import org.topcased.adele.xtext.baguards.baGuards.based_integer_literal;
import org.topcased.adele.xtext.baguards.baGuards.based_numeral;
import org.topcased.adele.xtext.baguards.baGuards.behavior_action_block_timeout_catch;
import org.topcased.adele.xtext.baguards.baGuards.behavior_time;
import org.topcased.adele.xtext.baguards.baGuards.binary_adding_operator;
import org.topcased.adele.xtext.baguards.baGuards.binary_numeric_operator;
import org.topcased.adele.xtext.baguards.baGuards.boolean_literal;
import org.topcased.adele.xtext.baguards.baGuards.completion_relative_timeout_condition_and_catch;
import org.topcased.adele.xtext.baguards.baGuards.decimal_integer_literal;
import org.topcased.adele.xtext.baguards.baGuards.decimal_real_literal;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_condition;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_conjunction;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_logical_expression;
import org.topcased.adele.xtext.baguards.baGuards.execute_condition;
import org.topcased.adele.xtext.baguards.baGuards.exponent;
import org.topcased.adele.xtext.baguards.baGuards.factor;
import org.topcased.adele.xtext.baguards.baGuards.frozen_ports;
import org.topcased.adele.xtext.baguards.baGuards.integer;
import org.topcased.adele.xtext.baguards.baGuards.integer_literal;
import org.topcased.adele.xtext.baguards.baGuards.integer_value;
import org.topcased.adele.xtext.baguards.baGuards.logical_operator;
import org.topcased.adele.xtext.baguards.baGuards.multiplying_operator;
import org.topcased.adele.xtext.baguards.baGuards.numeral;
import org.topcased.adele.xtext.baguards.baGuards.numeric_literal;
import org.topcased.adele.xtext.baguards.baGuards.positive_exponent;
import org.topcased.adele.xtext.baguards.baGuards.real_literal;
import org.topcased.adele.xtext.baguards.baGuards.relation;
import org.topcased.adele.xtext.baguards.baGuards.relational_operator;
import org.topcased.adele.xtext.baguards.baGuards.simple_expression;
import org.topcased.adele.xtext.baguards.baGuards.term;
import org.topcased.adele.xtext.baguards.baGuards.unary_adding_operator;
import org.topcased.adele.xtext.baguards.baGuards.unary_boolean_operator;
import org.topcased.adele.xtext.baguards.baGuards.unary_numeric_operator;
import org.topcased.adele.xtext.baguards.baGuards.unit_identifier;
import org.topcased.adele.xtext.baguards.baGuards.value;
import org.topcased.adele.xtext.baguards.baGuards.value_constant;
import org.topcased.adele.xtext.baguards.baGuards.value_expression;
import org.topcased.adele.xtext.baguards.baGuards.value_variable;
import org.topcased.adele.xtext.baguards.services.BaGuardsGrammarAccess;

@SuppressWarnings("restriction")
public class AbstractBaGuardsSemanticSequencer extends AbstractSemanticSequencer {

	@Inject
	protected BaGuardsGrammarAccess grammarAccess;
	
	@Inject
	protected ISemanticSequencerDiagnosticProvider diagnosticProvider;
	
	@Inject
	protected ITransientValueService transientValues;
	
	@Inject
	@GenericSequencer
	protected Provider<ISemanticSequencer> genericSequencerProvider;
	
	protected ISemanticSequencer genericSequencer;
	
	
	@Override
	public void init(ISemanticSequencer sequencer, ISemanticSequenceAcceptor sequenceAcceptor, Acceptor errorAcceptor) {
		super.init(sequencer, sequenceAcceptor, errorAcceptor);
		this.genericSequencer = genericSequencerProvider.get();
		this.genericSequencer.init(sequencer, sequenceAcceptor, errorAcceptor);
	}
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == BaGuardsPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case BaGuardsPackage.GUARD:
				if(context == grammarAccess.getGuardRule()) {
					sequence_Guard(context, (Guard) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.BASE:
				if(context == grammarAccess.getBaseRule()) {
					sequence_base(context, (base) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.BASED_INTEGER_LITERAL:
				if(context == grammarAccess.getBased_integer_literalRule()) {
					sequence_based_integer_literal(context, (based_integer_literal) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.BASED_NUMERAL:
				if(context == grammarAccess.getBased_numeralRule()) {
					sequence_based_numeral(context, (based_numeral) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.BEHAVIOR_ACTION_BLOCK_TIMEOUT_CATCH:
				if(context == grammarAccess.getBehavior_action_block_timeout_catchRule()) {
					sequence_behavior_action_block_timeout_catch(context, (behavior_action_block_timeout_catch) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.BEHAVIOR_TIME:
				if(context == grammarAccess.getBehavior_timeRule()) {
					sequence_behavior_time(context, (behavior_time) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.BINARY_ADDING_OPERATOR:
				if(context == grammarAccess.getBinary_adding_operatorRule()) {
					sequence_binary_adding_operator(context, (binary_adding_operator) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.BINARY_NUMERIC_OPERATOR:
				if(context == grammarAccess.getBinary_numeric_operatorRule()) {
					sequence_binary_numeric_operator(context, (binary_numeric_operator) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.BOOLEAN_LITERAL:
				if(context == grammarAccess.getBoolean_literalRule()) {
					sequence_boolean_literal(context, (boolean_literal) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH:
				if(context == grammarAccess.getCompletion_relative_timeout_condition_and_catchRule()) {
					sequence_completion_relative_timeout_condition_and_catch(context, (completion_relative_timeout_condition_and_catch) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.DECIMAL_INTEGER_LITERAL:
				if(context == grammarAccess.getDecimal_integer_literalRule()) {
					sequence_decimal_integer_literal(context, (decimal_integer_literal) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.DECIMAL_REAL_LITERAL:
				if(context == grammarAccess.getDecimal_real_literalRule()) {
					sequence_decimal_real_literal(context, (decimal_real_literal) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.DISPATCH_CONDITION:
				if(context == grammarAccess.getDispatch_conditionRule()) {
					sequence_dispatch_condition(context, (dispatch_condition) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.DISPATCH_CONJUNCTION:
				if(context == grammarAccess.getDispatch_conjunctionRule()) {
					sequence_dispatch_conjunction(context, (dispatch_conjunction) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.DISPATCH_TRIGGER:
				if(context == grammarAccess.getDispatch_triggerRule()) {
					sequence_dispatch_trigger(context, (dispatch_trigger) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION:
				if(context == grammarAccess.getDispatch_trigger_conditionRule()) {
					sequence_dispatch_trigger_condition(context, (dispatch_trigger_condition) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION:
				if(context == grammarAccess.getDispatch_trigger_logical_expressionRule()) {
					sequence_dispatch_trigger_logical_expression(context, (dispatch_trigger_logical_expression) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.EXECUTE_CONDITION:
				if(context == grammarAccess.getExecute_conditionRule()) {
					sequence_execute_condition(context, (execute_condition) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.EXPONENT:
				if(context == grammarAccess.getExponentRule()) {
					sequence_exponent(context, (exponent) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.FACTOR:
				if(context == grammarAccess.getFactorRule()) {
					sequence_factor(context, (factor) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.FROZEN_PORTS:
				if(context == grammarAccess.getFrozen_portsRule()) {
					sequence_frozen_ports(context, (frozen_ports) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.INTEGER:
				if(context == grammarAccess.getIntegerRule()) {
					sequence_integer(context, (integer) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.INTEGER_LITERAL:
				if(context == grammarAccess.getInteger_literalRule()) {
					sequence_integer_literal(context, (integer_literal) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.INTEGER_VALUE:
				if(context == grammarAccess.getInteger_valueRule()) {
					sequence_integer_value(context, (integer_value) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.LOGICAL_OPERATOR:
				if(context == grammarAccess.getLogical_operatorRule()) {
					sequence_logical_operator(context, (logical_operator) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.MULTIPLYING_OPERATOR:
				if(context == grammarAccess.getMultiplying_operatorRule()) {
					sequence_multiplying_operator(context, (multiplying_operator) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.NUMERAL:
				if(context == grammarAccess.getNumeralRule()) {
					sequence_numeral(context, (numeral) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.NUMERIC_LITERAL:
				if(context == grammarAccess.getNumeric_literalRule()) {
					sequence_numeric_literal(context, (numeric_literal) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.POSITIVE_EXPONENT:
				if(context == grammarAccess.getPositive_exponentRule()) {
					sequence_positive_exponent(context, (positive_exponent) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.REAL_LITERAL:
				if(context == grammarAccess.getReal_literalRule()) {
					sequence_real_literal(context, (real_literal) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.RELATION:
				if(context == grammarAccess.getRelationRule()) {
					sequence_relation(context, (relation) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.RELATIONAL_OPERATOR:
				if(context == grammarAccess.getRelational_operatorRule()) {
					sequence_relational_operator(context, (relational_operator) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.SIMPLE_EXPRESSION:
				if(context == grammarAccess.getSimple_expressionRule()) {
					sequence_simple_expression(context, (simple_expression) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.TERM:
				if(context == grammarAccess.getTermRule()) {
					sequence_term(context, (term) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.UNARY_ADDING_OPERATOR:
				if(context == grammarAccess.getUnary_adding_operatorRule()) {
					sequence_unary_adding_operator(context, (unary_adding_operator) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.UNARY_BOOLEAN_OPERATOR:
				if(context == grammarAccess.getUnary_boolean_operatorRule()) {
					sequence_unary_boolean_operator(context, (unary_boolean_operator) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.UNARY_NUMERIC_OPERATOR:
				if(context == grammarAccess.getUnary_numeric_operatorRule()) {
					sequence_unary_numeric_operator(context, (unary_numeric_operator) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.UNIT_IDENTIFIER:
				if(context == grammarAccess.getUnit_identifierRule()) {
					sequence_unit_identifier(context, (unit_identifier) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.VALUE:
				if(context == grammarAccess.getValueRule()) {
					sequence_value(context, (value) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.VALUE_CONSTANT:
				if(context == grammarAccess.getValue_constantRule()) {
					sequence_value_constant(context, (value_constant) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.VALUE_EXPRESSION:
				if(context == grammarAccess.getValue_expressionRule()) {
					sequence_value_expression(context, (value_expression) semanticObject); 
					return; 
				}
				else break;
			case BaGuardsPackage.VALUE_VARIABLE:
				if(context == grammarAccess.getValue_variableRule()) {
					sequence_value_variable(context, (value_variable) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (dispatch=dispatch_condition | execute=execute_condition)
	 *
	 * Features:
	 *    dispatch[0, 1]
	 *         EXCLUDE_IF_SET execute
	 *    execute[0, 1]
	 *         EXCLUDE_IF_SET dispatch
	 */
	protected void sequence_Guard(EObject context, Guard semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (bname1=digit bname2=digit?)
	 *
	 * Features:
	 *    bname1[1, 1]
	 *    bname2[0, 1]
	 */
	protected void sequence_base(EObject context, base semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (bilname1=base bilname2=based_numeral bilname3=positive_exponent?)
	 *
	 * Features:
	 *    bilname1[1, 1]
	 *    bilname2[1, 1]
	 *    bilname3[0, 1]
	 */
	protected void sequence_based_integer_literal(EObject context, based_integer_literal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (bnname1=extended_digit bnname2=extended_digit?)
	 *
	 * Features:
	 *    bnname1[1, 1]
	 *    bnname2[0, 1]
	 */
	protected void sequence_based_numeral(EObject context, based_numeral semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     babname+='timeout'
	 *
	 * Features:
	 *    babname[1, 1]
	 */
	protected void sequence_behavior_action_block_timeout_catch(EObject context, behavior_action_block_timeout_catch semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (btname1=integer_value btname2=unit_identifier)
	 *
	 * Features:
	 *    btname1[1, 1]
	 *    btname2[1, 1]
	 */
	protected void sequence_behavior_time(EObject context, behavior_time semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaGuardsPackage.Literals.BEHAVIOR_TIME__BTNAME1) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaGuardsPackage.Literals.BEHAVIOR_TIME__BTNAME1));
			if(transientValues.isValueTransient(semanticObject, BaGuardsPackage.Literals.BEHAVIOR_TIME__BTNAME2) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaGuardsPackage.Literals.BEHAVIOR_TIME__BTNAME2));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getBehavior_timeAccess().getBtname1Integer_valueParserRuleCall_0_0(), semanticObject.getBtname1());
		feeder.accept(grammarAccess.getBehavior_timeAccess().getBtname2Unit_identifierParserRuleCall_1_0(), semanticObject.getBtname2());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (baoname1='+' | baoname2='-')
	 *
	 * Features:
	 *    baoname1[0, 1]
	 *         EXCLUDE_IF_SET baoname2
	 *    baoname2[0, 1]
	 *         EXCLUDE_IF_SET baoname1
	 */
	protected void sequence_binary_adding_operator(EObject context, binary_adding_operator semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     bnoname='**'
	 *
	 * Features:
	 *    bnoname[1, 1]
	 */
	protected void sequence_binary_numeric_operator(EObject context, binary_numeric_operator semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaGuardsPackage.Literals.BINARY_NUMERIC_OPERATOR__BNONAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaGuardsPackage.Literals.BINARY_NUMERIC_OPERATOR__BNONAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getBinary_numeric_operatorAccess().getBnonameAsteriskAsteriskKeyword_0(), semanticObject.getBnoname());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (blname1='true' | blname1='false')
	 *
	 * Features:
	 *    blname1[0, 2]
	 */
	protected void sequence_boolean_literal(EObject context, boolean_literal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (cdtcacname1='timeout' cdtcacname2=behavior_time?)
	 *
	 * Features:
	 *    cdtcacname1[1, 1]
	 *    cdtcacname2[0, 1]
	 */
	protected void sequence_completion_relative_timeout_condition_and_catch(EObject context, completion_relative_timeout_condition_and_catch semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (dilname1=numeral dilname2=positive_exponent?)
	 *
	 * Features:
	 *    dilname1[1, 1]
	 *    dilname2[0, 1]
	 */
	protected void sequence_decimal_integer_literal(EObject context, decimal_integer_literal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (drlname1=numeral drlname2=numeral drlname3=exponent?)
	 *
	 * Features:
	 *    drlname1[1, 1]
	 *    drlname2[1, 1]
	 *    drlname3[0, 1]
	 */
	protected void sequence_decimal_real_literal(EObject context, decimal_real_literal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (dcname3='on dispatch' dcname1=dispatch_trigger_condition? dcname2=frozen_ports?)
	 *
	 * Features:
	 *    dcname3[1, 1]
	 *    dcname1[0, 1]
	 *    dcname2[0, 1]
	 */
	protected void sequence_dispatch_condition(EObject context, dispatch_condition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (dcname1=dispatch_trigger dcname2+=dispatch_trigger*)
	 *
	 * Features:
	 *    dcname1[1, 1]
	 *    dcname2[0, *]
	 */
	protected void sequence_dispatch_conjunction(EObject context, dispatch_conjunction semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (dtcname1=dispatch_trigger_logical_expression | dtcname4='stop' | dtcname2=completion_relative_timeout_condition_and_catch)
	 *
	 * Features:
	 *    dtcname1[0, 1]
	 *         EXCLUDE_IF_SET dtcname4
	 *         EXCLUDE_IF_SET dtcname2
	 *    dtcname4[0, 1]
	 *         EXCLUDE_IF_SET dtcname1
	 *         EXCLUDE_IF_SET dtcname2
	 *    dtcname2[0, 1]
	 *         EXCLUDE_IF_SET dtcname1
	 *         EXCLUDE_IF_SET dtcname4
	 */
	protected void sequence_dispatch_trigger_condition(EObject context, dispatch_trigger_condition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     dtname=string_literal
	 *
	 * Features:
	 *    dtname[1, 1]
	 */
	protected void sequence_dispatch_trigger(EObject context, dispatch_trigger semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaGuardsPackage.Literals.DISPATCH_TRIGGER__DTNAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaGuardsPackage.Literals.DISPATCH_TRIGGER__DTNAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getDispatch_triggerAccess().getDtnameString_literalTerminalRuleCall_0(), semanticObject.getDtname());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (dtlename1=dispatch_conjunction dtlename2+=dispatch_conjunction*)
	 *
	 * Features:
	 *    dtlename1[1, 1]
	 *    dtlename2[0, *]
	 */
	protected void sequence_dispatch_trigger_logical_expression(EObject context, dispatch_trigger_logical_expression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (ecname1='otherwise' | ecname2=behavior_action_block_timeout_catch | ecname3=value_expression)
	 *
	 * Features:
	 *    ecname1[0, 1]
	 *         EXCLUDE_IF_SET ecname2
	 *         EXCLUDE_IF_SET ecname3
	 *    ecname2[0, 1]
	 *         EXCLUDE_IF_SET ecname1
	 *         EXCLUDE_IF_SET ecname3
	 *    ecname3[0, 1]
	 *         EXCLUDE_IF_SET ecname1
	 *         EXCLUDE_IF_SET ecname2
	 */
	protected void sequence_execute_condition(EObject context, execute_condition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (ename1=numeral | ename2=numeral)
	 *
	 * Features:
	 *    ename1[0, 1]
	 *         EXCLUDE_IF_SET ename2
	 *    ename2[0, 1]
	 *         EXCLUDE_IF_SET ename1
	 */
	protected void sequence_exponent(EObject context, exponent semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         (fname1=value (fname3+=binary_numeric_operator fname2=value)?) | 
	 *         (fname4=unary_numeric_operator fname5=value) | 
	 *         (fname6=unary_boolean_operator fname7=value)
	 *     )
	 *
	 * Features:
	 *    fname1[0, 1]
	 *         MANDATORY_IF_SET fname3
	 *         MANDATORY_IF_SET fname2
	 *         EXCLUDE_IF_SET fname4
	 *         EXCLUDE_IF_SET fname5
	 *         EXCLUDE_IF_SET fname6
	 *         EXCLUDE_IF_SET fname7
	 *    fname3[0, 1]
	 *         EXCLUDE_IF_UNSET fname2
	 *         MANDATORY_IF_SET fname2
	 *         EXCLUDE_IF_UNSET fname1
	 *         EXCLUDE_IF_SET fname4
	 *         EXCLUDE_IF_SET fname5
	 *         EXCLUDE_IF_SET fname6
	 *         EXCLUDE_IF_SET fname7
	 *    fname2[0, 1]
	 *         EXCLUDE_IF_UNSET fname3
	 *         MANDATORY_IF_SET fname3
	 *         EXCLUDE_IF_UNSET fname1
	 *         EXCLUDE_IF_SET fname4
	 *         EXCLUDE_IF_SET fname5
	 *         EXCLUDE_IF_SET fname6
	 *         EXCLUDE_IF_SET fname7
	 *    fname4[0, 1]
	 *         EXCLUDE_IF_UNSET fname5
	 *         MANDATORY_IF_SET fname5
	 *         EXCLUDE_IF_SET fname1
	 *         EXCLUDE_IF_SET fname3
	 *         EXCLUDE_IF_SET fname2
	 *         EXCLUDE_IF_SET fname6
	 *         EXCLUDE_IF_SET fname7
	 *    fname5[0, 1]
	 *         EXCLUDE_IF_UNSET fname4
	 *         MANDATORY_IF_SET fname4
	 *         EXCLUDE_IF_SET fname1
	 *         EXCLUDE_IF_SET fname3
	 *         EXCLUDE_IF_SET fname2
	 *         EXCLUDE_IF_SET fname6
	 *         EXCLUDE_IF_SET fname7
	 *    fname6[0, 1]
	 *         EXCLUDE_IF_UNSET fname7
	 *         MANDATORY_IF_SET fname7
	 *         EXCLUDE_IF_SET fname1
	 *         EXCLUDE_IF_SET fname3
	 *         EXCLUDE_IF_SET fname2
	 *         EXCLUDE_IF_SET fname4
	 *         EXCLUDE_IF_SET fname5
	 *    fname7[0, 1]
	 *         EXCLUDE_IF_UNSET fname6
	 *         MANDATORY_IF_SET fname6
	 *         EXCLUDE_IF_SET fname1
	 *         EXCLUDE_IF_SET fname3
	 *         EXCLUDE_IF_SET fname2
	 *         EXCLUDE_IF_SET fname4
	 *         EXCLUDE_IF_SET fname5
	 */
	protected void sequence_factor(EObject context, factor semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (fpname1=string_literal fpname2+=string_literal*)
	 *
	 * Features:
	 *    fpname1[1, 1]
	 *    fpname2[0, *]
	 */
	protected void sequence_frozen_ports(EObject context, frozen_ports semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (value1=INT | value2=digit)
	 *
	 * Features:
	 *    value1[0, 1]
	 *         EXCLUDE_IF_SET value2
	 *    value2[0, 1]
	 *         EXCLUDE_IF_SET value1
	 */
	protected void sequence_integer(EObject context, integer semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (ilname1=decimal_integer_literal | ilname2=based_integer_literal)
	 *
	 * Features:
	 *    ilname1[0, 1]
	 *         EXCLUDE_IF_SET ilname2
	 *    ilname2[0, 1]
	 *         EXCLUDE_IF_SET ilname1
	 */
	protected void sequence_integer_literal(EObject context, integer_literal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ivname=numeric_literal
	 *
	 * Features:
	 *    ivname[1, 1]
	 */
	protected void sequence_integer_value(EObject context, integer_value semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaGuardsPackage.Literals.INTEGER_VALUE__IVNAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaGuardsPackage.Literals.INTEGER_VALUE__IVNAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getInteger_valueAccess().getIvnameNumeric_literalParserRuleCall_0(), semanticObject.getIvname());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (loname1='and' | loname2='or' | loname3='xor')
	 *
	 * Features:
	 *    loname1[0, 1]
	 *         EXCLUDE_IF_SET loname2
	 *         EXCLUDE_IF_SET loname3
	 *    loname2[0, 1]
	 *         EXCLUDE_IF_SET loname1
	 *         EXCLUDE_IF_SET loname3
	 *    loname3[0, 1]
	 *         EXCLUDE_IF_SET loname1
	 *         EXCLUDE_IF_SET loname2
	 */
	protected void sequence_logical_operator(EObject context, logical_operator semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (moname1='*' | moname2='/' | moname3='mod' | moname4='rem')
	 *
	 * Features:
	 *    moname1[0, 1]
	 *         EXCLUDE_IF_SET moname2
	 *         EXCLUDE_IF_SET moname3
	 *         EXCLUDE_IF_SET moname4
	 *    moname2[0, 1]
	 *         EXCLUDE_IF_SET moname1
	 *         EXCLUDE_IF_SET moname3
	 *         EXCLUDE_IF_SET moname4
	 *    moname3[0, 1]
	 *         EXCLUDE_IF_SET moname1
	 *         EXCLUDE_IF_SET moname2
	 *         EXCLUDE_IF_SET moname4
	 *    moname4[0, 1]
	 *         EXCLUDE_IF_SET moname1
	 *         EXCLUDE_IF_SET moname2
	 *         EXCLUDE_IF_SET moname3
	 */
	protected void sequence_multiplying_operator(EObject context, multiplying_operator semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (nname1=integer nname2+=integer*)
	 *
	 * Features:
	 *    nname1[1, 1]
	 *    nname2[0, *]
	 */
	protected void sequence_numeral(EObject context, numeral semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (nlname1=integer_literal | nlname2=real_literal)
	 *
	 * Features:
	 *    nlname1[0, 1]
	 *         EXCLUDE_IF_SET nlname2
	 *    nlname2[0, 1]
	 *         EXCLUDE_IF_SET nlname1
	 */
	protected void sequence_numeric_literal(EObject context, numeric_literal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     pename=numeral
	 *
	 * Features:
	 *    pename[1, 1]
	 */
	protected void sequence_positive_exponent(EObject context, positive_exponent semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaGuardsPackage.Literals.POSITIVE_EXPONENT__PENAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaGuardsPackage.Literals.POSITIVE_EXPONENT__PENAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getPositive_exponentAccess().getPenameNumeralParserRuleCall_2_0(), semanticObject.getPename());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     rlname=decimal_real_literal
	 *
	 * Features:
	 *    rlname[1, 1]
	 */
	protected void sequence_real_literal(EObject context, real_literal semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaGuardsPackage.Literals.REAL_LITERAL__RLNAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaGuardsPackage.Literals.REAL_LITERAL__RLNAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getReal_literalAccess().getRlnameDecimal_real_literalParserRuleCall_0(), semanticObject.getRlname());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (rname1=simple_expression (rname3+=relational_operator rname2+=simple_expression)?)
	 *
	 * Features:
	 *    rname1[1, 1]
	 *    rname3[0, 1]
	 *         EXCLUDE_IF_UNSET rname2
	 *         MANDATORY_IF_SET rname2
	 *    rname2[0, 1]
	 *         EXCLUDE_IF_UNSET rname3
	 *         MANDATORY_IF_SET rname3
	 */
	protected void sequence_relation(EObject context, relation semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         roname1='=' | 
	 *         roname2='!=' | 
	 *         roname3='<' | 
	 *         roname4='<=' | 
	 *         roname5='>' | 
	 *         roname6='>='
	 *     )
	 *
	 * Features:
	 *    roname1[0, 1]
	 *         EXCLUDE_IF_SET roname2
	 *         EXCLUDE_IF_SET roname3
	 *         EXCLUDE_IF_SET roname4
	 *         EXCLUDE_IF_SET roname5
	 *         EXCLUDE_IF_SET roname6
	 *    roname2[0, 1]
	 *         EXCLUDE_IF_SET roname1
	 *         EXCLUDE_IF_SET roname3
	 *         EXCLUDE_IF_SET roname4
	 *         EXCLUDE_IF_SET roname5
	 *         EXCLUDE_IF_SET roname6
	 *    roname3[0, 1]
	 *         EXCLUDE_IF_SET roname1
	 *         EXCLUDE_IF_SET roname2
	 *         EXCLUDE_IF_SET roname4
	 *         EXCLUDE_IF_SET roname5
	 *         EXCLUDE_IF_SET roname6
	 *    roname4[0, 1]
	 *         EXCLUDE_IF_SET roname1
	 *         EXCLUDE_IF_SET roname2
	 *         EXCLUDE_IF_SET roname3
	 *         EXCLUDE_IF_SET roname5
	 *         EXCLUDE_IF_SET roname6
	 *    roname5[0, 1]
	 *         EXCLUDE_IF_SET roname1
	 *         EXCLUDE_IF_SET roname2
	 *         EXCLUDE_IF_SET roname3
	 *         EXCLUDE_IF_SET roname4
	 *         EXCLUDE_IF_SET roname6
	 *    roname6[0, 1]
	 *         EXCLUDE_IF_SET roname1
	 *         EXCLUDE_IF_SET roname2
	 *         EXCLUDE_IF_SET roname3
	 *         EXCLUDE_IF_SET roname4
	 *         EXCLUDE_IF_SET roname5
	 */
	protected void sequence_relational_operator(EObject context, relational_operator semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (sename2=unary_adding_operator? sename1=term (sename3+=binary_adding_operator sename4+=term)*)
	 *
	 * Features:
	 *    sename2[0, 1]
	 *    sename1[1, 1]
	 *    sename3[0, *]
	 *         SAME sename4
	 *    sename4[0, *]
	 *         SAME sename3
	 */
	protected void sequence_simple_expression(EObject context, simple_expression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (tname1=factor (tname3+=multiplying_operator tname2+=factor)*)
	 *
	 * Features:
	 *    tname1[1, 1]
	 *    tname3[0, *]
	 *         SAME tname2
	 *    tname2[0, *]
	 *         SAME tname3
	 */
	protected void sequence_term(EObject context, term semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (uaoname1='+' | uaoname2='-')
	 *
	 * Features:
	 *    uaoname1[0, 1]
	 *         EXCLUDE_IF_SET uaoname2
	 *    uaoname2[0, 1]
	 *         EXCLUDE_IF_SET uaoname1
	 */
	protected void sequence_unary_adding_operator(EObject context, unary_adding_operator semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     uboname='not'
	 *
	 * Features:
	 *    uboname[1, 1]
	 */
	protected void sequence_unary_boolean_operator(EObject context, unary_boolean_operator semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaGuardsPackage.Literals.UNARY_BOOLEAN_OPERATOR__UBONAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaGuardsPackage.Literals.UNARY_BOOLEAN_OPERATOR__UBONAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getUnary_boolean_operatorAccess().getUbonameNotKeyword_0(), semanticObject.getUboname());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     unoname='abs'
	 *
	 * Features:
	 *    unoname[1, 1]
	 */
	protected void sequence_unary_numeric_operator(EObject context, unary_numeric_operator semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaGuardsPackage.Literals.UNARY_NUMERIC_OPERATOR__UNONAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaGuardsPackage.Literals.UNARY_NUMERIC_OPERATOR__UNONAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getUnary_numeric_operatorAccess().getUnonameAbsKeyword_0(), semanticObject.getUnoname());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         uiname1='ps' | 
	 *         uiname2='ns' | 
	 *         uiname3='us' | 
	 *         uiname4='ms' | 
	 *         uiname5='sec' | 
	 *         uiname6='min' | 
	 *         uiname7='hr'
	 *     )
	 *
	 * Features:
	 *    uiname1[0, 1]
	 *         EXCLUDE_IF_SET uiname2
	 *         EXCLUDE_IF_SET uiname3
	 *         EXCLUDE_IF_SET uiname4
	 *         EXCLUDE_IF_SET uiname5
	 *         EXCLUDE_IF_SET uiname6
	 *         EXCLUDE_IF_SET uiname7
	 *    uiname2[0, 1]
	 *         EXCLUDE_IF_SET uiname1
	 *         EXCLUDE_IF_SET uiname3
	 *         EXCLUDE_IF_SET uiname4
	 *         EXCLUDE_IF_SET uiname5
	 *         EXCLUDE_IF_SET uiname6
	 *         EXCLUDE_IF_SET uiname7
	 *    uiname3[0, 1]
	 *         EXCLUDE_IF_SET uiname1
	 *         EXCLUDE_IF_SET uiname2
	 *         EXCLUDE_IF_SET uiname4
	 *         EXCLUDE_IF_SET uiname5
	 *         EXCLUDE_IF_SET uiname6
	 *         EXCLUDE_IF_SET uiname7
	 *    uiname4[0, 1]
	 *         EXCLUDE_IF_SET uiname1
	 *         EXCLUDE_IF_SET uiname2
	 *         EXCLUDE_IF_SET uiname3
	 *         EXCLUDE_IF_SET uiname5
	 *         EXCLUDE_IF_SET uiname6
	 *         EXCLUDE_IF_SET uiname7
	 *    uiname5[0, 1]
	 *         EXCLUDE_IF_SET uiname1
	 *         EXCLUDE_IF_SET uiname2
	 *         EXCLUDE_IF_SET uiname3
	 *         EXCLUDE_IF_SET uiname4
	 *         EXCLUDE_IF_SET uiname6
	 *         EXCLUDE_IF_SET uiname7
	 *    uiname6[0, 1]
	 *         EXCLUDE_IF_SET uiname1
	 *         EXCLUDE_IF_SET uiname2
	 *         EXCLUDE_IF_SET uiname3
	 *         EXCLUDE_IF_SET uiname4
	 *         EXCLUDE_IF_SET uiname5
	 *         EXCLUDE_IF_SET uiname7
	 *    uiname7[0, 1]
	 *         EXCLUDE_IF_SET uiname1
	 *         EXCLUDE_IF_SET uiname2
	 *         EXCLUDE_IF_SET uiname3
	 *         EXCLUDE_IF_SET uiname4
	 *         EXCLUDE_IF_SET uiname5
	 *         EXCLUDE_IF_SET uiname6
	 */
	protected void sequence_unit_identifier(EObject context, unit_identifier semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (vcname1=boolean_literal | vcname2=numeric_literal | (vcname3=string_literal vcname4=string_literal))
	 *
	 * Features:
	 *    vcname1[0, 1]
	 *         EXCLUDE_IF_SET vcname2
	 *         EXCLUDE_IF_SET vcname3
	 *         EXCLUDE_IF_SET vcname4
	 *    vcname2[0, 1]
	 *         EXCLUDE_IF_SET vcname1
	 *         EXCLUDE_IF_SET vcname3
	 *         EXCLUDE_IF_SET vcname4
	 *    vcname3[0, 1]
	 *         EXCLUDE_IF_UNSET vcname4
	 *         MANDATORY_IF_SET vcname4
	 *         EXCLUDE_IF_SET vcname1
	 *         EXCLUDE_IF_SET vcname2
	 *    vcname4[0, 1]
	 *         EXCLUDE_IF_UNSET vcname3
	 *         MANDATORY_IF_SET vcname3
	 *         EXCLUDE_IF_SET vcname1
	 *         EXCLUDE_IF_SET vcname2
	 */
	protected void sequence_value_constant(EObject context, value_constant semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (vename1=relation (vename3+=logical_operator vename2+=relation)*)
	 *
	 * Features:
	 *    vename1[1, 1]
	 *    vename3[0, *]
	 *         SAME vename2
	 *    vename2[0, *]
	 *         SAME vename3
	 */
	protected void sequence_value_expression(EObject context, value_expression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (vname1=value_variable | vname2=value_constant)
	 *
	 * Features:
	 *    vname1[0, 1]
	 *         EXCLUDE_IF_SET vname2
	 *    vname2[0, 1]
	 *         EXCLUDE_IF_SET vname1
	 */
	protected void sequence_value(EObject context, value semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (vvname0=string_literal (vvname1='?' | vvname2='count' | vvname3='fresh')?)
	 *
	 * Features:
	 *    vvname0[1, 1]
	 *    vvname1[0, 1]
	 *         EXCLUDE_IF_SET vvname2
	 *         EXCLUDE_IF_SET vvname3
	 *    vvname2[0, 1]
	 *         EXCLUDE_IF_SET vvname1
	 *         EXCLUDE_IF_SET vvname3
	 *    vvname3[0, 1]
	 *         EXCLUDE_IF_SET vvname1
	 *         EXCLUDE_IF_SET vvname2
	 */
	protected void sequence_value_variable(EObject context, value_variable semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
