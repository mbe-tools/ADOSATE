/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.binary_adding_operator;
import org.topcased.adele.xtext.baguards.baGuards.simple_expression;
import org.topcased.adele.xtext.baguards.baGuards.term;
import org.topcased.adele.xtext.baguards.baGuards.unary_adding_operator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>simple expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.simple_expressionImpl#getSename2 <em>Sename2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.simple_expressionImpl#getSename1 <em>Sename1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.simple_expressionImpl#getSename3 <em>Sename3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.simple_expressionImpl#getSename4 <em>Sename4</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class simple_expressionImpl extends MinimalEObjectImpl.Container implements simple_expression
{
  /**
   * The cached value of the '{@link #getSename2() <em>Sename2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSename2()
   * @generated
   * @ordered
   */
  protected unary_adding_operator sename2;

  /**
   * The cached value of the '{@link #getSename1() <em>Sename1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSename1()
   * @generated
   * @ordered
   */
  protected term sename1;

  /**
   * The cached value of the '{@link #getSename3() <em>Sename3</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSename3()
   * @generated
   * @ordered
   */
  protected EList<binary_adding_operator> sename3;

  /**
   * The cached value of the '{@link #getSename4() <em>Sename4</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSename4()
   * @generated
   * @ordered
   */
  protected EList<term> sename4;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected simple_expressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.SIMPLE_EXPRESSION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public unary_adding_operator getSename2()
  {
    return sename2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSename2(unary_adding_operator newSename2, NotificationChain msgs)
  {
    unary_adding_operator oldSename2 = sename2;
    sename2 = newSename2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.SIMPLE_EXPRESSION__SENAME2, oldSename2, newSename2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSename2(unary_adding_operator newSename2)
  {
    if (newSename2 != sename2)
    {
      NotificationChain msgs = null;
      if (sename2 != null)
        msgs = ((InternalEObject)sename2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.SIMPLE_EXPRESSION__SENAME2, null, msgs);
      if (newSename2 != null)
        msgs = ((InternalEObject)newSename2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.SIMPLE_EXPRESSION__SENAME2, null, msgs);
      msgs = basicSetSename2(newSename2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.SIMPLE_EXPRESSION__SENAME2, newSename2, newSename2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public term getSename1()
  {
    return sename1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSename1(term newSename1, NotificationChain msgs)
  {
    term oldSename1 = sename1;
    sename1 = newSename1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.SIMPLE_EXPRESSION__SENAME1, oldSename1, newSename1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSename1(term newSename1)
  {
    if (newSename1 != sename1)
    {
      NotificationChain msgs = null;
      if (sename1 != null)
        msgs = ((InternalEObject)sename1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.SIMPLE_EXPRESSION__SENAME1, null, msgs);
      if (newSename1 != null)
        msgs = ((InternalEObject)newSename1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.SIMPLE_EXPRESSION__SENAME1, null, msgs);
      msgs = basicSetSename1(newSename1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.SIMPLE_EXPRESSION__SENAME1, newSename1, newSename1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<binary_adding_operator> getSename3()
  {
    if (sename3 == null)
    {
      sename3 = new EObjectContainmentEList<binary_adding_operator>(binary_adding_operator.class, this, BaGuardsPackage.SIMPLE_EXPRESSION__SENAME3);
    }
    return sename3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<term> getSename4()
  {
    if (sename4 == null)
    {
      sename4 = new EObjectContainmentEList<term>(term.class, this, BaGuardsPackage.SIMPLE_EXPRESSION__SENAME4);
    }
    return sename4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME2:
        return basicSetSename2(null, msgs);
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME1:
        return basicSetSename1(null, msgs);
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME3:
        return ((InternalEList<?>)getSename3()).basicRemove(otherEnd, msgs);
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME4:
        return ((InternalEList<?>)getSename4()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME2:
        return getSename2();
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME1:
        return getSename1();
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME3:
        return getSename3();
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME4:
        return getSename4();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME2:
        setSename2((unary_adding_operator)newValue);
        return;
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME1:
        setSename1((term)newValue);
        return;
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME3:
        getSename3().clear();
        getSename3().addAll((Collection<? extends binary_adding_operator>)newValue);
        return;
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME4:
        getSename4().clear();
        getSename4().addAll((Collection<? extends term>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME2:
        setSename2((unary_adding_operator)null);
        return;
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME1:
        setSename1((term)null);
        return;
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME3:
        getSename3().clear();
        return;
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME4:
        getSename4().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME2:
        return sename2 != null;
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME1:
        return sename1 != null;
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME3:
        return sename3 != null && !sename3.isEmpty();
      case BaGuardsPackage.SIMPLE_EXPRESSION__SENAME4:
        return sename4 != null && !sename4.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //simple_expressionImpl
