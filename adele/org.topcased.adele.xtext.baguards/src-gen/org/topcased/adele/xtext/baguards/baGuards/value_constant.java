/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>value constant</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.value_constant#getVcname1 <em>Vcname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.value_constant#getVcname2 <em>Vcname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.value_constant#getVcname3 <em>Vcname3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.value_constant#getVcname4 <em>Vcname4</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getvalue_constant()
 * @model
 * @generated
 */
public interface value_constant extends EObject
{
  /**
   * Returns the value of the '<em><b>Vcname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Vcname1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Vcname1</em>' containment reference.
   * @see #setVcname1(boolean_literal)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getvalue_constant_Vcname1()
   * @model containment="true"
   * @generated
   */
  boolean_literal getVcname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.value_constant#getVcname1 <em>Vcname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Vcname1</em>' containment reference.
   * @see #getVcname1()
   * @generated
   */
  void setVcname1(boolean_literal value);

  /**
   * Returns the value of the '<em><b>Vcname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Vcname2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Vcname2</em>' containment reference.
   * @see #setVcname2(numeric_literal)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getvalue_constant_Vcname2()
   * @model containment="true"
   * @generated
   */
  numeric_literal getVcname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.value_constant#getVcname2 <em>Vcname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Vcname2</em>' containment reference.
   * @see #getVcname2()
   * @generated
   */
  void setVcname2(numeric_literal value);

  /**
   * Returns the value of the '<em><b>Vcname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Vcname3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Vcname3</em>' attribute.
   * @see #setVcname3(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getvalue_constant_Vcname3()
   * @model
   * @generated
   */
  String getVcname3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.value_constant#getVcname3 <em>Vcname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Vcname3</em>' attribute.
   * @see #getVcname3()
   * @generated
   */
  void setVcname3(String value);

  /**
   * Returns the value of the '<em><b>Vcname4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Vcname4</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Vcname4</em>' attribute.
   * @see #setVcname4(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getvalue_constant_Vcname4()
   * @model
   * @generated
   */
  String getVcname4();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.value_constant#getVcname4 <em>Vcname4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Vcname4</em>' attribute.
   * @see #getVcname4()
   * @generated
   */
  void setVcname4(String value);

} // value_constant
