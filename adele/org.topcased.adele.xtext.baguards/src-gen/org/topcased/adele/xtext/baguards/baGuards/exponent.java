/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>exponent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.exponent#getEname1 <em>Ename1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.exponent#getEname2 <em>Ename2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getexponent()
 * @model
 * @generated
 */
public interface exponent extends EObject
{
  /**
   * Returns the value of the '<em><b>Ename1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ename1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ename1</em>' containment reference.
   * @see #setEname1(numeral)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getexponent_Ename1()
   * @model containment="true"
   * @generated
   */
  numeral getEname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.exponent#getEname1 <em>Ename1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ename1</em>' containment reference.
   * @see #getEname1()
   * @generated
   */
  void setEname1(numeral value);

  /**
   * Returns the value of the '<em><b>Ename2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ename2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ename2</em>' containment reference.
   * @see #setEname2(numeral)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getexponent_Ename2()
   * @model containment="true"
   * @generated
   */
  numeral getEname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.exponent#getEname2 <em>Ename2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ename2</em>' containment reference.
   * @see #getEname2()
   * @generated
   */
  void setEname2(numeral value);

} // exponent
