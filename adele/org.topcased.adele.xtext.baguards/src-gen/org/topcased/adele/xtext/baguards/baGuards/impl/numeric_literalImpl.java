/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.integer_literal;
import org.topcased.adele.xtext.baguards.baGuards.numeric_literal;
import org.topcased.adele.xtext.baguards.baGuards.real_literal;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>numeric literal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.numeric_literalImpl#getNlname1 <em>Nlname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.numeric_literalImpl#getNlname2 <em>Nlname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class numeric_literalImpl extends MinimalEObjectImpl.Container implements numeric_literal
{
  /**
   * The cached value of the '{@link #getNlname1() <em>Nlname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNlname1()
   * @generated
   * @ordered
   */
  protected integer_literal nlname1;

  /**
   * The cached value of the '{@link #getNlname2() <em>Nlname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNlname2()
   * @generated
   * @ordered
   */
  protected real_literal nlname2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected numeric_literalImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.NUMERIC_LITERAL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public integer_literal getNlname1()
  {
    return nlname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNlname1(integer_literal newNlname1, NotificationChain msgs)
  {
    integer_literal oldNlname1 = nlname1;
    nlname1 = newNlname1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.NUMERIC_LITERAL__NLNAME1, oldNlname1, newNlname1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNlname1(integer_literal newNlname1)
  {
    if (newNlname1 != nlname1)
    {
      NotificationChain msgs = null;
      if (nlname1 != null)
        msgs = ((InternalEObject)nlname1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.NUMERIC_LITERAL__NLNAME1, null, msgs);
      if (newNlname1 != null)
        msgs = ((InternalEObject)newNlname1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.NUMERIC_LITERAL__NLNAME1, null, msgs);
      msgs = basicSetNlname1(newNlname1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.NUMERIC_LITERAL__NLNAME1, newNlname1, newNlname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public real_literal getNlname2()
  {
    return nlname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNlname2(real_literal newNlname2, NotificationChain msgs)
  {
    real_literal oldNlname2 = nlname2;
    nlname2 = newNlname2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.NUMERIC_LITERAL__NLNAME2, oldNlname2, newNlname2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNlname2(real_literal newNlname2)
  {
    if (newNlname2 != nlname2)
    {
      NotificationChain msgs = null;
      if (nlname2 != null)
        msgs = ((InternalEObject)nlname2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.NUMERIC_LITERAL__NLNAME2, null, msgs);
      if (newNlname2 != null)
        msgs = ((InternalEObject)newNlname2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.NUMERIC_LITERAL__NLNAME2, null, msgs);
      msgs = basicSetNlname2(newNlname2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.NUMERIC_LITERAL__NLNAME2, newNlname2, newNlname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.NUMERIC_LITERAL__NLNAME1:
        return basicSetNlname1(null, msgs);
      case BaGuardsPackage.NUMERIC_LITERAL__NLNAME2:
        return basicSetNlname2(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.NUMERIC_LITERAL__NLNAME1:
        return getNlname1();
      case BaGuardsPackage.NUMERIC_LITERAL__NLNAME2:
        return getNlname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.NUMERIC_LITERAL__NLNAME1:
        setNlname1((integer_literal)newValue);
        return;
      case BaGuardsPackage.NUMERIC_LITERAL__NLNAME2:
        setNlname2((real_literal)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.NUMERIC_LITERAL__NLNAME1:
        setNlname1((integer_literal)null);
        return;
      case BaGuardsPackage.NUMERIC_LITERAL__NLNAME2:
        setNlname2((real_literal)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.NUMERIC_LITERAL__NLNAME1:
        return nlname1 != null;
      case BaGuardsPackage.NUMERIC_LITERAL__NLNAME2:
        return nlname2 != null;
    }
    return super.eIsSet(featureID);
  }

} //numeric_literalImpl
