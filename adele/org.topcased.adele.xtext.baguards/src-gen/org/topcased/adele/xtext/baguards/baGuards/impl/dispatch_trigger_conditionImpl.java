/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.completion_relative_timeout_condition_and_catch;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_logical_expression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dispatch trigger condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_trigger_conditionImpl#getDtcname1 <em>Dtcname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_trigger_conditionImpl#getDtcname4 <em>Dtcname4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_trigger_conditionImpl#getDtcname2 <em>Dtcname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class dispatch_trigger_conditionImpl extends MinimalEObjectImpl.Container implements dispatch_trigger_condition
{
  /**
   * The cached value of the '{@link #getDtcname1() <em>Dtcname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDtcname1()
   * @generated
   * @ordered
   */
  protected dispatch_trigger_logical_expression dtcname1;

  /**
   * The default value of the '{@link #getDtcname4() <em>Dtcname4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDtcname4()
   * @generated
   * @ordered
   */
  protected static final String DTCNAME4_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDtcname4() <em>Dtcname4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDtcname4()
   * @generated
   * @ordered
   */
  protected String dtcname4 = DTCNAME4_EDEFAULT;

  /**
   * The cached value of the '{@link #getDtcname2() <em>Dtcname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDtcname2()
   * @generated
   * @ordered
   */
  protected completion_relative_timeout_condition_and_catch dtcname2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected dispatch_trigger_conditionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.DISPATCH_TRIGGER_CONDITION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public dispatch_trigger_logical_expression getDtcname1()
  {
    return dtcname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDtcname1(dispatch_trigger_logical_expression newDtcname1, NotificationChain msgs)
  {
    dispatch_trigger_logical_expression oldDtcname1 = dtcname1;
    dtcname1 = newDtcname1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME1, oldDtcname1, newDtcname1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDtcname1(dispatch_trigger_logical_expression newDtcname1)
  {
    if (newDtcname1 != dtcname1)
    {
      NotificationChain msgs = null;
      if (dtcname1 != null)
        msgs = ((InternalEObject)dtcname1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME1, null, msgs);
      if (newDtcname1 != null)
        msgs = ((InternalEObject)newDtcname1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME1, null, msgs);
      msgs = basicSetDtcname1(newDtcname1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME1, newDtcname1, newDtcname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDtcname4()
  {
    return dtcname4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDtcname4(String newDtcname4)
  {
    String oldDtcname4 = dtcname4;
    dtcname4 = newDtcname4;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME4, oldDtcname4, dtcname4));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public completion_relative_timeout_condition_and_catch getDtcname2()
  {
    return dtcname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDtcname2(completion_relative_timeout_condition_and_catch newDtcname2, NotificationChain msgs)
  {
    completion_relative_timeout_condition_and_catch oldDtcname2 = dtcname2;
    dtcname2 = newDtcname2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME2, oldDtcname2, newDtcname2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDtcname2(completion_relative_timeout_condition_and_catch newDtcname2)
  {
    if (newDtcname2 != dtcname2)
    {
      NotificationChain msgs = null;
      if (dtcname2 != null)
        msgs = ((InternalEObject)dtcname2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME2, null, msgs);
      if (newDtcname2 != null)
        msgs = ((InternalEObject)newDtcname2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME2, null, msgs);
      msgs = basicSetDtcname2(newDtcname2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME2, newDtcname2, newDtcname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME1:
        return basicSetDtcname1(null, msgs);
      case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME2:
        return basicSetDtcname2(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME1:
        return getDtcname1();
      case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME4:
        return getDtcname4();
      case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME2:
        return getDtcname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME1:
        setDtcname1((dispatch_trigger_logical_expression)newValue);
        return;
      case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME4:
        setDtcname4((String)newValue);
        return;
      case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME2:
        setDtcname2((completion_relative_timeout_condition_and_catch)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME1:
        setDtcname1((dispatch_trigger_logical_expression)null);
        return;
      case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME4:
        setDtcname4(DTCNAME4_EDEFAULT);
        return;
      case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME2:
        setDtcname2((completion_relative_timeout_condition_and_catch)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME1:
        return dtcname1 != null;
      case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME4:
        return DTCNAME4_EDEFAULT == null ? dtcname4 != null : !DTCNAME4_EDEFAULT.equals(dtcname4);
      case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION__DTCNAME2:
        return dtcname2 != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (dtcname4: ");
    result.append(dtcname4);
    result.append(')');
    return result.toString();
  }

} //dispatch_trigger_conditionImpl
