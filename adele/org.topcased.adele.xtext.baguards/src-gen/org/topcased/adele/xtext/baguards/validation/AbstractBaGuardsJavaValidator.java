package org.topcased.adele.xtext.baguards.validation;
 
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.validation.AbstractDeclarativeValidator;

public class AbstractBaGuardsJavaValidator extends AbstractDeclarativeValidator {

	@Override
	protected List<EPackage> getEPackages() {
	    List<EPackage> result = new ArrayList<EPackage>();
	    result.add(org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage.eINSTANCE);
		return result;
	}

}
