/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.integer_value;
import org.topcased.adele.xtext.baguards.baGuards.numeric_literal;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>integer value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.integer_valueImpl#getIvname <em>Ivname</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class integer_valueImpl extends MinimalEObjectImpl.Container implements integer_value
{
  /**
   * The cached value of the '{@link #getIvname() <em>Ivname</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIvname()
   * @generated
   * @ordered
   */
  protected numeric_literal ivname;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected integer_valueImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.INTEGER_VALUE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public numeric_literal getIvname()
  {
    return ivname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIvname(numeric_literal newIvname, NotificationChain msgs)
  {
    numeric_literal oldIvname = ivname;
    ivname = newIvname;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.INTEGER_VALUE__IVNAME, oldIvname, newIvname);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIvname(numeric_literal newIvname)
  {
    if (newIvname != ivname)
    {
      NotificationChain msgs = null;
      if (ivname != null)
        msgs = ((InternalEObject)ivname).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.INTEGER_VALUE__IVNAME, null, msgs);
      if (newIvname != null)
        msgs = ((InternalEObject)newIvname).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.INTEGER_VALUE__IVNAME, null, msgs);
      msgs = basicSetIvname(newIvname, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.INTEGER_VALUE__IVNAME, newIvname, newIvname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.INTEGER_VALUE__IVNAME:
        return basicSetIvname(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.INTEGER_VALUE__IVNAME:
        return getIvname();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.INTEGER_VALUE__IVNAME:
        setIvname((numeric_literal)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.INTEGER_VALUE__IVNAME:
        setIvname((numeric_literal)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.INTEGER_VALUE__IVNAME:
        return ivname != null;
    }
    return super.eIsSet(featureID);
  }

} //integer_valueImpl
