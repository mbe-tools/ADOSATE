/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.Guard;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_condition;
import org.topcased.adele.xtext.baguards.baGuards.execute_condition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Guard</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.GuardImpl#getDispatch <em>Dispatch</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.GuardImpl#getExecute <em>Execute</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GuardImpl extends MinimalEObjectImpl.Container implements Guard
{
  /**
   * The cached value of the '{@link #getDispatch() <em>Dispatch</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDispatch()
   * @generated
   * @ordered
   */
  protected dispatch_condition dispatch;

  /**
   * The cached value of the '{@link #getExecute() <em>Execute</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExecute()
   * @generated
   * @ordered
   */
  protected execute_condition execute;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected GuardImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.GUARD;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public dispatch_condition getDispatch()
  {
    return dispatch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDispatch(dispatch_condition newDispatch, NotificationChain msgs)
  {
    dispatch_condition oldDispatch = dispatch;
    dispatch = newDispatch;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.GUARD__DISPATCH, oldDispatch, newDispatch);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDispatch(dispatch_condition newDispatch)
  {
    if (newDispatch != dispatch)
    {
      NotificationChain msgs = null;
      if (dispatch != null)
        msgs = ((InternalEObject)dispatch).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.GUARD__DISPATCH, null, msgs);
      if (newDispatch != null)
        msgs = ((InternalEObject)newDispatch).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.GUARD__DISPATCH, null, msgs);
      msgs = basicSetDispatch(newDispatch, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.GUARD__DISPATCH, newDispatch, newDispatch));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execute_condition getExecute()
  {
    return execute;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExecute(execute_condition newExecute, NotificationChain msgs)
  {
    execute_condition oldExecute = execute;
    execute = newExecute;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.GUARD__EXECUTE, oldExecute, newExecute);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExecute(execute_condition newExecute)
  {
    if (newExecute != execute)
    {
      NotificationChain msgs = null;
      if (execute != null)
        msgs = ((InternalEObject)execute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.GUARD__EXECUTE, null, msgs);
      if (newExecute != null)
        msgs = ((InternalEObject)newExecute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.GUARD__EXECUTE, null, msgs);
      msgs = basicSetExecute(newExecute, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.GUARD__EXECUTE, newExecute, newExecute));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.GUARD__DISPATCH:
        return basicSetDispatch(null, msgs);
      case BaGuardsPackage.GUARD__EXECUTE:
        return basicSetExecute(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.GUARD__DISPATCH:
        return getDispatch();
      case BaGuardsPackage.GUARD__EXECUTE:
        return getExecute();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.GUARD__DISPATCH:
        setDispatch((dispatch_condition)newValue);
        return;
      case BaGuardsPackage.GUARD__EXECUTE:
        setExecute((execute_condition)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.GUARD__DISPATCH:
        setDispatch((dispatch_condition)null);
        return;
      case BaGuardsPackage.GUARD__EXECUTE:
        setExecute((execute_condition)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.GUARD__DISPATCH:
        return dispatch != null;
      case BaGuardsPackage.GUARD__EXECUTE:
        return execute != null;
    }
    return super.eIsSet(featureID);
  }

} //GuardImpl
