/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_conjunction;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dispatch conjunction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_conjunctionImpl#getDcname1 <em>Dcname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_conjunctionImpl#getDcname2 <em>Dcname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class dispatch_conjunctionImpl extends MinimalEObjectImpl.Container implements dispatch_conjunction
{
  /**
   * The cached value of the '{@link #getDcname1() <em>Dcname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDcname1()
   * @generated
   * @ordered
   */
  protected dispatch_trigger dcname1;

  /**
   * The cached value of the '{@link #getDcname2() <em>Dcname2</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDcname2()
   * @generated
   * @ordered
   */
  protected EList<dispatch_trigger> dcname2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected dispatch_conjunctionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.DISPATCH_CONJUNCTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public dispatch_trigger getDcname1()
  {
    return dcname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDcname1(dispatch_trigger newDcname1, NotificationChain msgs)
  {
    dispatch_trigger oldDcname1 = dcname1;
    dcname1 = newDcname1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DISPATCH_CONJUNCTION__DCNAME1, oldDcname1, newDcname1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDcname1(dispatch_trigger newDcname1)
  {
    if (newDcname1 != dcname1)
    {
      NotificationChain msgs = null;
      if (dcname1 != null)
        msgs = ((InternalEObject)dcname1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DISPATCH_CONJUNCTION__DCNAME1, null, msgs);
      if (newDcname1 != null)
        msgs = ((InternalEObject)newDcname1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DISPATCH_CONJUNCTION__DCNAME1, null, msgs);
      msgs = basicSetDcname1(newDcname1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DISPATCH_CONJUNCTION__DCNAME1, newDcname1, newDcname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<dispatch_trigger> getDcname2()
  {
    if (dcname2 == null)
    {
      dcname2 = new EObjectContainmentEList<dispatch_trigger>(dispatch_trigger.class, this, BaGuardsPackage.DISPATCH_CONJUNCTION__DCNAME2);
    }
    return dcname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_CONJUNCTION__DCNAME1:
        return basicSetDcname1(null, msgs);
      case BaGuardsPackage.DISPATCH_CONJUNCTION__DCNAME2:
        return ((InternalEList<?>)getDcname2()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_CONJUNCTION__DCNAME1:
        return getDcname1();
      case BaGuardsPackage.DISPATCH_CONJUNCTION__DCNAME2:
        return getDcname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_CONJUNCTION__DCNAME1:
        setDcname1((dispatch_trigger)newValue);
        return;
      case BaGuardsPackage.DISPATCH_CONJUNCTION__DCNAME2:
        getDcname2().clear();
        getDcname2().addAll((Collection<? extends dispatch_trigger>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_CONJUNCTION__DCNAME1:
        setDcname1((dispatch_trigger)null);
        return;
      case BaGuardsPackage.DISPATCH_CONJUNCTION__DCNAME2:
        getDcname2().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_CONJUNCTION__DCNAME1:
        return dcname1 != null;
      case BaGuardsPackage.DISPATCH_CONJUNCTION__DCNAME2:
        return dcname2 != null && !dcname2.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //dispatch_conjunctionImpl
