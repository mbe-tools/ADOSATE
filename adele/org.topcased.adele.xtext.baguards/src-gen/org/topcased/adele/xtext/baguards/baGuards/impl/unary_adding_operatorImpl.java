/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.unary_adding_operator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>unary adding operator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.unary_adding_operatorImpl#getUaoname1 <em>Uaoname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.unary_adding_operatorImpl#getUaoname2 <em>Uaoname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class unary_adding_operatorImpl extends MinimalEObjectImpl.Container implements unary_adding_operator
{
  /**
   * The default value of the '{@link #getUaoname1() <em>Uaoname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUaoname1()
   * @generated
   * @ordered
   */
  protected static final String UAONAME1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUaoname1() <em>Uaoname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUaoname1()
   * @generated
   * @ordered
   */
  protected String uaoname1 = UAONAME1_EDEFAULT;

  /**
   * The default value of the '{@link #getUaoname2() <em>Uaoname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUaoname2()
   * @generated
   * @ordered
   */
  protected static final String UAONAME2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUaoname2() <em>Uaoname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUaoname2()
   * @generated
   * @ordered
   */
  protected String uaoname2 = UAONAME2_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected unary_adding_operatorImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.UNARY_ADDING_OPERATOR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getUaoname1()
  {
    return uaoname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUaoname1(String newUaoname1)
  {
    String oldUaoname1 = uaoname1;
    uaoname1 = newUaoname1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.UNARY_ADDING_OPERATOR__UAONAME1, oldUaoname1, uaoname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getUaoname2()
  {
    return uaoname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUaoname2(String newUaoname2)
  {
    String oldUaoname2 = uaoname2;
    uaoname2 = newUaoname2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.UNARY_ADDING_OPERATOR__UAONAME2, oldUaoname2, uaoname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.UNARY_ADDING_OPERATOR__UAONAME1:
        return getUaoname1();
      case BaGuardsPackage.UNARY_ADDING_OPERATOR__UAONAME2:
        return getUaoname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.UNARY_ADDING_OPERATOR__UAONAME1:
        setUaoname1((String)newValue);
        return;
      case BaGuardsPackage.UNARY_ADDING_OPERATOR__UAONAME2:
        setUaoname2((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.UNARY_ADDING_OPERATOR__UAONAME1:
        setUaoname1(UAONAME1_EDEFAULT);
        return;
      case BaGuardsPackage.UNARY_ADDING_OPERATOR__UAONAME2:
        setUaoname2(UAONAME2_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.UNARY_ADDING_OPERATOR__UAONAME1:
        return UAONAME1_EDEFAULT == null ? uaoname1 != null : !UAONAME1_EDEFAULT.equals(uaoname1);
      case BaGuardsPackage.UNARY_ADDING_OPERATOR__UAONAME2:
        return UAONAME2_EDEFAULT == null ? uaoname2 != null : !UAONAME2_EDEFAULT.equals(uaoname2);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (uaoname1: ");
    result.append(uaoname1);
    result.append(", uaoname2: ");
    result.append(uaoname2);
    result.append(')');
    return result.toString();
  }

} //unary_adding_operatorImpl
