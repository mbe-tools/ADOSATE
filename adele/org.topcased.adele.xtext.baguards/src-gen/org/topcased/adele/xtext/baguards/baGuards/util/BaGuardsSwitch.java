/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.topcased.adele.xtext.baguards.baGuards.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage
 * @generated
 */
public class BaGuardsSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static BaGuardsPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaGuardsSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = BaGuardsPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @parameter ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case BaGuardsPackage.GUARD:
      {
        Guard guard = (Guard)theEObject;
        T result = caseGuard(guard);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.EXECUTE_CONDITION:
      {
        execute_condition execute_condition = (execute_condition)theEObject;
        T result = caseexecute_condition(execute_condition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.BEHAVIOR_ACTION_BLOCK_TIMEOUT_CATCH:
      {
        behavior_action_block_timeout_catch behavior_action_block_timeout_catch = (behavior_action_block_timeout_catch)theEObject;
        T result = casebehavior_action_block_timeout_catch(behavior_action_block_timeout_catch);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.VALUE_EXPRESSION:
      {
        value_expression value_expression = (value_expression)theEObject;
        T result = casevalue_expression(value_expression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.RELATION:
      {
        relation relation = (relation)theEObject;
        T result = caserelation(relation);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.SIMPLE_EXPRESSION:
      {
        simple_expression simple_expression = (simple_expression)theEObject;
        T result = casesimple_expression(simple_expression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.TERM:
      {
        term term = (term)theEObject;
        T result = caseterm(term);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.FACTOR:
      {
        factor factor = (factor)theEObject;
        T result = casefactor(factor);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.VALUE:
      {
        value value = (value)theEObject;
        T result = casevalue(value);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.VALUE_VARIABLE:
      {
        value_variable value_variable = (value_variable)theEObject;
        T result = casevalue_variable(value_variable);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.VALUE_CONSTANT:
      {
        value_constant value_constant = (value_constant)theEObject;
        T result = casevalue_constant(value_constant);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.LOGICAL_OPERATOR:
      {
        logical_operator logical_operator = (logical_operator)theEObject;
        T result = caselogical_operator(logical_operator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.RELATIONAL_OPERATOR:
      {
        relational_operator relational_operator = (relational_operator)theEObject;
        T result = caserelational_operator(relational_operator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.BINARY_ADDING_OPERATOR:
      {
        binary_adding_operator binary_adding_operator = (binary_adding_operator)theEObject;
        T result = casebinary_adding_operator(binary_adding_operator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.UNARY_ADDING_OPERATOR:
      {
        unary_adding_operator unary_adding_operator = (unary_adding_operator)theEObject;
        T result = caseunary_adding_operator(unary_adding_operator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.MULTIPLYING_OPERATOR:
      {
        multiplying_operator multiplying_operator = (multiplying_operator)theEObject;
        T result = casemultiplying_operator(multiplying_operator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.BINARY_NUMERIC_OPERATOR:
      {
        binary_numeric_operator binary_numeric_operator = (binary_numeric_operator)theEObject;
        T result = casebinary_numeric_operator(binary_numeric_operator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.UNARY_NUMERIC_OPERATOR:
      {
        unary_numeric_operator unary_numeric_operator = (unary_numeric_operator)theEObject;
        T result = caseunary_numeric_operator(unary_numeric_operator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.UNARY_BOOLEAN_OPERATOR:
      {
        unary_boolean_operator unary_boolean_operator = (unary_boolean_operator)theEObject;
        T result = caseunary_boolean_operator(unary_boolean_operator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.BOOLEAN_LITERAL:
      {
        boolean_literal boolean_literal = (boolean_literal)theEObject;
        T result = caseboolean_literal(boolean_literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.INTEGER_VALUE:
      {
        integer_value integer_value = (integer_value)theEObject;
        T result = caseinteger_value(integer_value);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.BEHAVIOR_TIME:
      {
        behavior_time behavior_time = (behavior_time)theEObject;
        T result = casebehavior_time(behavior_time);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.UNIT_IDENTIFIER:
      {
        unit_identifier unit_identifier = (unit_identifier)theEObject;
        T result = caseunit_identifier(unit_identifier);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.NUMERIC_LITERAL:
      {
        numeric_literal numeric_literal = (numeric_literal)theEObject;
        T result = casenumeric_literal(numeric_literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.INTEGER_LITERAL:
      {
        integer_literal integer_literal = (integer_literal)theEObject;
        T result = caseinteger_literal(integer_literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.REAL_LITERAL:
      {
        real_literal real_literal = (real_literal)theEObject;
        T result = casereal_literal(real_literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.DECIMAL_INTEGER_LITERAL:
      {
        decimal_integer_literal decimal_integer_literal = (decimal_integer_literal)theEObject;
        T result = casedecimal_integer_literal(decimal_integer_literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.DECIMAL_REAL_LITERAL:
      {
        decimal_real_literal decimal_real_literal = (decimal_real_literal)theEObject;
        T result = casedecimal_real_literal(decimal_real_literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.NUMERAL:
      {
        numeral numeral = (numeral)theEObject;
        T result = casenumeral(numeral);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.EXPONENT:
      {
        exponent exponent = (exponent)theEObject;
        T result = caseexponent(exponent);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.POSITIVE_EXPONENT:
      {
        positive_exponent positive_exponent = (positive_exponent)theEObject;
        T result = casepositive_exponent(positive_exponent);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.BASED_INTEGER_LITERAL:
      {
        based_integer_literal based_integer_literal = (based_integer_literal)theEObject;
        T result = casebased_integer_literal(based_integer_literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.BASE:
      {
        base base = (base)theEObject;
        T result = casebase(base);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.BASED_NUMERAL:
      {
        based_numeral based_numeral = (based_numeral)theEObject;
        T result = casebased_numeral(based_numeral);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.INTEGER:
      {
        integer integer = (integer)theEObject;
        T result = caseinteger(integer);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.DISPATCH_CONDITION:
      {
        dispatch_condition dispatch_condition = (dispatch_condition)theEObject;
        T result = casedispatch_condition(dispatch_condition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION:
      {
        dispatch_trigger_condition dispatch_trigger_condition = (dispatch_trigger_condition)theEObject;
        T result = casedispatch_trigger_condition(dispatch_trigger_condition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION:
      {
        dispatch_trigger_logical_expression dispatch_trigger_logical_expression = (dispatch_trigger_logical_expression)theEObject;
        T result = casedispatch_trigger_logical_expression(dispatch_trigger_logical_expression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.DISPATCH_CONJUNCTION:
      {
        dispatch_conjunction dispatch_conjunction = (dispatch_conjunction)theEObject;
        T result = casedispatch_conjunction(dispatch_conjunction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH:
      {
        completion_relative_timeout_condition_and_catch completion_relative_timeout_condition_and_catch = (completion_relative_timeout_condition_and_catch)theEObject;
        T result = casecompletion_relative_timeout_condition_and_catch(completion_relative_timeout_condition_and_catch);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.DISPATCH_TRIGGER:
      {
        dispatch_trigger dispatch_trigger = (dispatch_trigger)theEObject;
        T result = casedispatch_trigger(dispatch_trigger);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaGuardsPackage.FROZEN_PORTS:
      {
        frozen_ports frozen_ports = (frozen_ports)theEObject;
        T result = casefrozen_ports(frozen_ports);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Guard</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Guard</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGuard(Guard object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>execute condition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>execute condition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexecute_condition(execute_condition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>behavior action block timeout catch</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>behavior action block timeout catch</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebehavior_action_block_timeout_catch(behavior_action_block_timeout_catch object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>value expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>value expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casevalue_expression(value_expression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>relation</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>relation</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caserelation(relation object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>simple expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>simple expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casesimple_expression(simple_expression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>term</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>term</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseterm(term object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>factor</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>factor</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casefactor(factor object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casevalue(value object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>value variable</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>value variable</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casevalue_variable(value_variable object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>value constant</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>value constant</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casevalue_constant(value_constant object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>logical operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>logical operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caselogical_operator(logical_operator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>relational operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>relational operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caserelational_operator(relational_operator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>binary adding operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>binary adding operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebinary_adding_operator(binary_adding_operator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>unary adding operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>unary adding operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseunary_adding_operator(unary_adding_operator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>multiplying operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>multiplying operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casemultiplying_operator(multiplying_operator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>binary numeric operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>binary numeric operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebinary_numeric_operator(binary_numeric_operator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>unary numeric operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>unary numeric operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseunary_numeric_operator(unary_numeric_operator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>unary boolean operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>unary boolean operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseunary_boolean_operator(unary_boolean_operator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>boolean literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>boolean literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseboolean_literal(boolean_literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>integer value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>integer value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseinteger_value(integer_value object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>behavior time</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>behavior time</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebehavior_time(behavior_time object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>unit identifier</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>unit identifier</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseunit_identifier(unit_identifier object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>numeric literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>numeric literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casenumeric_literal(numeric_literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>integer literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>integer literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseinteger_literal(integer_literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>real literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>real literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casereal_literal(real_literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>decimal integer literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>decimal integer literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casedecimal_integer_literal(decimal_integer_literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>decimal real literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>decimal real literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casedecimal_real_literal(decimal_real_literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>numeral</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>numeral</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casenumeral(numeral object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>exponent</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>exponent</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexponent(exponent object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>positive exponent</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>positive exponent</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casepositive_exponent(positive_exponent object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>based integer literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>based integer literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebased_integer_literal(based_integer_literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>base</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>base</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebase(base object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>based numeral</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>based numeral</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebased_numeral(based_numeral object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>integer</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>integer</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseinteger(integer object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dispatch condition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dispatch condition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casedispatch_condition(dispatch_condition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dispatch trigger condition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dispatch trigger condition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casedispatch_trigger_condition(dispatch_trigger_condition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dispatch trigger logical expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dispatch trigger logical expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casedispatch_trigger_logical_expression(dispatch_trigger_logical_expression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dispatch conjunction</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dispatch conjunction</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casedispatch_conjunction(dispatch_conjunction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>completion relative timeout condition and catch</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>completion relative timeout condition and catch</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casecompletion_relative_timeout_condition_and_catch(completion_relative_timeout_condition_and_catch object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>dispatch trigger</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>dispatch trigger</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casedispatch_trigger(dispatch_trigger object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>frozen ports</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>frozen ports</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casefrozen_ports(frozen_ports object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //BaGuardsSwitch
