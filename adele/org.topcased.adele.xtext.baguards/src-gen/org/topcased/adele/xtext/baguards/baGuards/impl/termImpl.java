/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.factor;
import org.topcased.adele.xtext.baguards.baGuards.multiplying_operator;
import org.topcased.adele.xtext.baguards.baGuards.term;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>term</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.termImpl#getTname1 <em>Tname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.termImpl#getTname3 <em>Tname3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.termImpl#getTname2 <em>Tname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class termImpl extends MinimalEObjectImpl.Container implements term
{
  /**
   * The cached value of the '{@link #getTname1() <em>Tname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTname1()
   * @generated
   * @ordered
   */
  protected factor tname1;

  /**
   * The cached value of the '{@link #getTname3() <em>Tname3</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTname3()
   * @generated
   * @ordered
   */
  protected EList<multiplying_operator> tname3;

  /**
   * The cached value of the '{@link #getTname2() <em>Tname2</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTname2()
   * @generated
   * @ordered
   */
  protected EList<factor> tname2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected termImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.TERM;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public factor getTname1()
  {
    return tname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTname1(factor newTname1, NotificationChain msgs)
  {
    factor oldTname1 = tname1;
    tname1 = newTname1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.TERM__TNAME1, oldTname1, newTname1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTname1(factor newTname1)
  {
    if (newTname1 != tname1)
    {
      NotificationChain msgs = null;
      if (tname1 != null)
        msgs = ((InternalEObject)tname1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.TERM__TNAME1, null, msgs);
      if (newTname1 != null)
        msgs = ((InternalEObject)newTname1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.TERM__TNAME1, null, msgs);
      msgs = basicSetTname1(newTname1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.TERM__TNAME1, newTname1, newTname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<multiplying_operator> getTname3()
  {
    if (tname3 == null)
    {
      tname3 = new EObjectContainmentEList<multiplying_operator>(multiplying_operator.class, this, BaGuardsPackage.TERM__TNAME3);
    }
    return tname3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<factor> getTname2()
  {
    if (tname2 == null)
    {
      tname2 = new EObjectContainmentEList<factor>(factor.class, this, BaGuardsPackage.TERM__TNAME2);
    }
    return tname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.TERM__TNAME1:
        return basicSetTname1(null, msgs);
      case BaGuardsPackage.TERM__TNAME3:
        return ((InternalEList<?>)getTname3()).basicRemove(otherEnd, msgs);
      case BaGuardsPackage.TERM__TNAME2:
        return ((InternalEList<?>)getTname2()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.TERM__TNAME1:
        return getTname1();
      case BaGuardsPackage.TERM__TNAME3:
        return getTname3();
      case BaGuardsPackage.TERM__TNAME2:
        return getTname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.TERM__TNAME1:
        setTname1((factor)newValue);
        return;
      case BaGuardsPackage.TERM__TNAME3:
        getTname3().clear();
        getTname3().addAll((Collection<? extends multiplying_operator>)newValue);
        return;
      case BaGuardsPackage.TERM__TNAME2:
        getTname2().clear();
        getTname2().addAll((Collection<? extends factor>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.TERM__TNAME1:
        setTname1((factor)null);
        return;
      case BaGuardsPackage.TERM__TNAME3:
        getTname3().clear();
        return;
      case BaGuardsPackage.TERM__TNAME2:
        getTname2().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.TERM__TNAME1:
        return tname1 != null;
      case BaGuardsPackage.TERM__TNAME3:
        return tname3 != null && !tname3.isEmpty();
      case BaGuardsPackage.TERM__TNAME2:
        return tname2 != null && !tname2.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //termImpl
