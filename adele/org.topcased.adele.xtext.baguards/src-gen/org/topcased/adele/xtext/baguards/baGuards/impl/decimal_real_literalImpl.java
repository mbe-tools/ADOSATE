/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.decimal_real_literal;
import org.topcased.adele.xtext.baguards.baGuards.exponent;
import org.topcased.adele.xtext.baguards.baGuards.numeral;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>decimal real literal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.decimal_real_literalImpl#getDrlname1 <em>Drlname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.decimal_real_literalImpl#getDrlname2 <em>Drlname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.decimal_real_literalImpl#getDrlname3 <em>Drlname3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class decimal_real_literalImpl extends MinimalEObjectImpl.Container implements decimal_real_literal
{
  /**
   * The cached value of the '{@link #getDrlname1() <em>Drlname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDrlname1()
   * @generated
   * @ordered
   */
  protected numeral drlname1;

  /**
   * The cached value of the '{@link #getDrlname2() <em>Drlname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDrlname2()
   * @generated
   * @ordered
   */
  protected numeral drlname2;

  /**
   * The cached value of the '{@link #getDrlname3() <em>Drlname3</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDrlname3()
   * @generated
   * @ordered
   */
  protected exponent drlname3;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected decimal_real_literalImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.DECIMAL_REAL_LITERAL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public numeral getDrlname1()
  {
    return drlname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDrlname1(numeral newDrlname1, NotificationChain msgs)
  {
    numeral oldDrlname1 = drlname1;
    drlname1 = newDrlname1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME1, oldDrlname1, newDrlname1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDrlname1(numeral newDrlname1)
  {
    if (newDrlname1 != drlname1)
    {
      NotificationChain msgs = null;
      if (drlname1 != null)
        msgs = ((InternalEObject)drlname1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME1, null, msgs);
      if (newDrlname1 != null)
        msgs = ((InternalEObject)newDrlname1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME1, null, msgs);
      msgs = basicSetDrlname1(newDrlname1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME1, newDrlname1, newDrlname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public numeral getDrlname2()
  {
    return drlname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDrlname2(numeral newDrlname2, NotificationChain msgs)
  {
    numeral oldDrlname2 = drlname2;
    drlname2 = newDrlname2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME2, oldDrlname2, newDrlname2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDrlname2(numeral newDrlname2)
  {
    if (newDrlname2 != drlname2)
    {
      NotificationChain msgs = null;
      if (drlname2 != null)
        msgs = ((InternalEObject)drlname2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME2, null, msgs);
      if (newDrlname2 != null)
        msgs = ((InternalEObject)newDrlname2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME2, null, msgs);
      msgs = basicSetDrlname2(newDrlname2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME2, newDrlname2, newDrlname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public exponent getDrlname3()
  {
    return drlname3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDrlname3(exponent newDrlname3, NotificationChain msgs)
  {
    exponent oldDrlname3 = drlname3;
    drlname3 = newDrlname3;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME3, oldDrlname3, newDrlname3);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDrlname3(exponent newDrlname3)
  {
    if (newDrlname3 != drlname3)
    {
      NotificationChain msgs = null;
      if (drlname3 != null)
        msgs = ((InternalEObject)drlname3).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME3, null, msgs);
      if (newDrlname3 != null)
        msgs = ((InternalEObject)newDrlname3).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME3, null, msgs);
      msgs = basicSetDrlname3(newDrlname3, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME3, newDrlname3, newDrlname3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME1:
        return basicSetDrlname1(null, msgs);
      case BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME2:
        return basicSetDrlname2(null, msgs);
      case BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME3:
        return basicSetDrlname3(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME1:
        return getDrlname1();
      case BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME2:
        return getDrlname2();
      case BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME3:
        return getDrlname3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME1:
        setDrlname1((numeral)newValue);
        return;
      case BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME2:
        setDrlname2((numeral)newValue);
        return;
      case BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME3:
        setDrlname3((exponent)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME1:
        setDrlname1((numeral)null);
        return;
      case BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME2:
        setDrlname2((numeral)null);
        return;
      case BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME3:
        setDrlname3((exponent)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME1:
        return drlname1 != null;
      case BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME2:
        return drlname2 != null;
      case BaGuardsPackage.DECIMAL_REAL_LITERAL__DRLNAME3:
        return drlname3 != null;
    }
    return super.eIsSet(featureID);
  }

} //decimal_real_literalImpl
