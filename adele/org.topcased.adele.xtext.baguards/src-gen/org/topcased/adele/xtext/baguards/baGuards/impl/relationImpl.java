/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.relation;
import org.topcased.adele.xtext.baguards.baGuards.relational_operator;
import org.topcased.adele.xtext.baguards.baGuards.simple_expression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.relationImpl#getRname1 <em>Rname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.relationImpl#getRname3 <em>Rname3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.relationImpl#getRname2 <em>Rname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class relationImpl extends MinimalEObjectImpl.Container implements relation
{
  /**
   * The cached value of the '{@link #getRname1() <em>Rname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRname1()
   * @generated
   * @ordered
   */
  protected simple_expression rname1;

  /**
   * The cached value of the '{@link #getRname3() <em>Rname3</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRname3()
   * @generated
   * @ordered
   */
  protected EList<relational_operator> rname3;

  /**
   * The cached value of the '{@link #getRname2() <em>Rname2</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRname2()
   * @generated
   * @ordered
   */
  protected EList<simple_expression> rname2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected relationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.RELATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public simple_expression getRname1()
  {
    return rname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRname1(simple_expression newRname1, NotificationChain msgs)
  {
    simple_expression oldRname1 = rname1;
    rname1 = newRname1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.RELATION__RNAME1, oldRname1, newRname1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRname1(simple_expression newRname1)
  {
    if (newRname1 != rname1)
    {
      NotificationChain msgs = null;
      if (rname1 != null)
        msgs = ((InternalEObject)rname1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.RELATION__RNAME1, null, msgs);
      if (newRname1 != null)
        msgs = ((InternalEObject)newRname1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.RELATION__RNAME1, null, msgs);
      msgs = basicSetRname1(newRname1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.RELATION__RNAME1, newRname1, newRname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<relational_operator> getRname3()
  {
    if (rname3 == null)
    {
      rname3 = new EObjectContainmentEList<relational_operator>(relational_operator.class, this, BaGuardsPackage.RELATION__RNAME3);
    }
    return rname3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<simple_expression> getRname2()
  {
    if (rname2 == null)
    {
      rname2 = new EObjectContainmentEList<simple_expression>(simple_expression.class, this, BaGuardsPackage.RELATION__RNAME2);
    }
    return rname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.RELATION__RNAME1:
        return basicSetRname1(null, msgs);
      case BaGuardsPackage.RELATION__RNAME3:
        return ((InternalEList<?>)getRname3()).basicRemove(otherEnd, msgs);
      case BaGuardsPackage.RELATION__RNAME2:
        return ((InternalEList<?>)getRname2()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.RELATION__RNAME1:
        return getRname1();
      case BaGuardsPackage.RELATION__RNAME3:
        return getRname3();
      case BaGuardsPackage.RELATION__RNAME2:
        return getRname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.RELATION__RNAME1:
        setRname1((simple_expression)newValue);
        return;
      case BaGuardsPackage.RELATION__RNAME3:
        getRname3().clear();
        getRname3().addAll((Collection<? extends relational_operator>)newValue);
        return;
      case BaGuardsPackage.RELATION__RNAME2:
        getRname2().clear();
        getRname2().addAll((Collection<? extends simple_expression>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.RELATION__RNAME1:
        setRname1((simple_expression)null);
        return;
      case BaGuardsPackage.RELATION__RNAME3:
        getRname3().clear();
        return;
      case BaGuardsPackage.RELATION__RNAME2:
        getRname2().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.RELATION__RNAME1:
        return rname1 != null;
      case BaGuardsPackage.RELATION__RNAME3:
        return rname3 != null && !rname3.isEmpty();
      case BaGuardsPackage.RELATION__RNAME2:
        return rname2 != null && !rname2.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //relationImpl
