/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.multiplying_operator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>multiplying operator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.multiplying_operatorImpl#getMoname1 <em>Moname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.multiplying_operatorImpl#getMoname2 <em>Moname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.multiplying_operatorImpl#getMoname3 <em>Moname3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.multiplying_operatorImpl#getMoname4 <em>Moname4</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class multiplying_operatorImpl extends MinimalEObjectImpl.Container implements multiplying_operator
{
  /**
   * The default value of the '{@link #getMoname1() <em>Moname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMoname1()
   * @generated
   * @ordered
   */
  protected static final String MONAME1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getMoname1() <em>Moname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMoname1()
   * @generated
   * @ordered
   */
  protected String moname1 = MONAME1_EDEFAULT;

  /**
   * The default value of the '{@link #getMoname2() <em>Moname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMoname2()
   * @generated
   * @ordered
   */
  protected static final String MONAME2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getMoname2() <em>Moname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMoname2()
   * @generated
   * @ordered
   */
  protected String moname2 = MONAME2_EDEFAULT;

  /**
   * The default value of the '{@link #getMoname3() <em>Moname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMoname3()
   * @generated
   * @ordered
   */
  protected static final String MONAME3_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getMoname3() <em>Moname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMoname3()
   * @generated
   * @ordered
   */
  protected String moname3 = MONAME3_EDEFAULT;

  /**
   * The default value of the '{@link #getMoname4() <em>Moname4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMoname4()
   * @generated
   * @ordered
   */
  protected static final String MONAME4_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getMoname4() <em>Moname4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMoname4()
   * @generated
   * @ordered
   */
  protected String moname4 = MONAME4_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected multiplying_operatorImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.MULTIPLYING_OPERATOR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getMoname1()
  {
    return moname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMoname1(String newMoname1)
  {
    String oldMoname1 = moname1;
    moname1 = newMoname1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME1, oldMoname1, moname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getMoname2()
  {
    return moname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMoname2(String newMoname2)
  {
    String oldMoname2 = moname2;
    moname2 = newMoname2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME2, oldMoname2, moname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getMoname3()
  {
    return moname3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMoname3(String newMoname3)
  {
    String oldMoname3 = moname3;
    moname3 = newMoname3;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME3, oldMoname3, moname3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getMoname4()
  {
    return moname4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMoname4(String newMoname4)
  {
    String oldMoname4 = moname4;
    moname4 = newMoname4;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME4, oldMoname4, moname4));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME1:
        return getMoname1();
      case BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME2:
        return getMoname2();
      case BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME3:
        return getMoname3();
      case BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME4:
        return getMoname4();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME1:
        setMoname1((String)newValue);
        return;
      case BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME2:
        setMoname2((String)newValue);
        return;
      case BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME3:
        setMoname3((String)newValue);
        return;
      case BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME4:
        setMoname4((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME1:
        setMoname1(MONAME1_EDEFAULT);
        return;
      case BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME2:
        setMoname2(MONAME2_EDEFAULT);
        return;
      case BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME3:
        setMoname3(MONAME3_EDEFAULT);
        return;
      case BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME4:
        setMoname4(MONAME4_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME1:
        return MONAME1_EDEFAULT == null ? moname1 != null : !MONAME1_EDEFAULT.equals(moname1);
      case BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME2:
        return MONAME2_EDEFAULT == null ? moname2 != null : !MONAME2_EDEFAULT.equals(moname2);
      case BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME3:
        return MONAME3_EDEFAULT == null ? moname3 != null : !MONAME3_EDEFAULT.equals(moname3);
      case BaGuardsPackage.MULTIPLYING_OPERATOR__MONAME4:
        return MONAME4_EDEFAULT == null ? moname4 != null : !MONAME4_EDEFAULT.equals(moname4);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (moname1: ");
    result.append(moname1);
    result.append(", moname2: ");
    result.append(moname2);
    result.append(", moname3: ");
    result.append(moname3);
    result.append(", moname4: ");
    result.append(moname4);
    result.append(')');
    return result.toString();
  }

} //multiplying_operatorImpl
