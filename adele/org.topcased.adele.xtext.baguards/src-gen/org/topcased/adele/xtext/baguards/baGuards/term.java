/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>term</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.term#getTname1 <em>Tname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.term#getTname3 <em>Tname3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.term#getTname2 <em>Tname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getterm()
 * @model
 * @generated
 */
public interface term extends EObject
{
  /**
   * Returns the value of the '<em><b>Tname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Tname1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Tname1</em>' containment reference.
   * @see #setTname1(factor)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getterm_Tname1()
   * @model containment="true"
   * @generated
   */
  factor getTname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.term#getTname1 <em>Tname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Tname1</em>' containment reference.
   * @see #getTname1()
   * @generated
   */
  void setTname1(factor value);

  /**
   * Returns the value of the '<em><b>Tname3</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baguards.baGuards.multiplying_operator}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Tname3</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Tname3</em>' containment reference list.
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getterm_Tname3()
   * @model containment="true"
   * @generated
   */
  EList<multiplying_operator> getTname3();

  /**
   * Returns the value of the '<em><b>Tname2</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baguards.baGuards.factor}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Tname2</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Tname2</em>' containment reference list.
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getterm_Tname2()
   * @model containment="true"
   * @generated
   */
  EList<factor> getTname2();

} // term
