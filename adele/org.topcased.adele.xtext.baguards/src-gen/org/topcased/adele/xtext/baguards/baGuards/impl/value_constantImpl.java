/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.boolean_literal;
import org.topcased.adele.xtext.baguards.baGuards.numeric_literal;
import org.topcased.adele.xtext.baguards.baGuards.value_constant;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>value constant</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.value_constantImpl#getVcname1 <em>Vcname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.value_constantImpl#getVcname2 <em>Vcname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.value_constantImpl#getVcname3 <em>Vcname3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.value_constantImpl#getVcname4 <em>Vcname4</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class value_constantImpl extends MinimalEObjectImpl.Container implements value_constant
{
  /**
   * The cached value of the '{@link #getVcname1() <em>Vcname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVcname1()
   * @generated
   * @ordered
   */
  protected boolean_literal vcname1;

  /**
   * The cached value of the '{@link #getVcname2() <em>Vcname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVcname2()
   * @generated
   * @ordered
   */
  protected numeric_literal vcname2;

  /**
   * The default value of the '{@link #getVcname3() <em>Vcname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVcname3()
   * @generated
   * @ordered
   */
  protected static final String VCNAME3_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVcname3() <em>Vcname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVcname3()
   * @generated
   * @ordered
   */
  protected String vcname3 = VCNAME3_EDEFAULT;

  /**
   * The default value of the '{@link #getVcname4() <em>Vcname4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVcname4()
   * @generated
   * @ordered
   */
  protected static final String VCNAME4_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVcname4() <em>Vcname4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVcname4()
   * @generated
   * @ordered
   */
  protected String vcname4 = VCNAME4_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected value_constantImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.VALUE_CONSTANT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean_literal getVcname1()
  {
    return vcname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetVcname1(boolean_literal newVcname1, NotificationChain msgs)
  {
    boolean_literal oldVcname1 = vcname1;
    vcname1 = newVcname1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.VALUE_CONSTANT__VCNAME1, oldVcname1, newVcname1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVcname1(boolean_literal newVcname1)
  {
    if (newVcname1 != vcname1)
    {
      NotificationChain msgs = null;
      if (vcname1 != null)
        msgs = ((InternalEObject)vcname1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.VALUE_CONSTANT__VCNAME1, null, msgs);
      if (newVcname1 != null)
        msgs = ((InternalEObject)newVcname1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.VALUE_CONSTANT__VCNAME1, null, msgs);
      msgs = basicSetVcname1(newVcname1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.VALUE_CONSTANT__VCNAME1, newVcname1, newVcname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public numeric_literal getVcname2()
  {
    return vcname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetVcname2(numeric_literal newVcname2, NotificationChain msgs)
  {
    numeric_literal oldVcname2 = vcname2;
    vcname2 = newVcname2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.VALUE_CONSTANT__VCNAME2, oldVcname2, newVcname2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVcname2(numeric_literal newVcname2)
  {
    if (newVcname2 != vcname2)
    {
      NotificationChain msgs = null;
      if (vcname2 != null)
        msgs = ((InternalEObject)vcname2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.VALUE_CONSTANT__VCNAME2, null, msgs);
      if (newVcname2 != null)
        msgs = ((InternalEObject)newVcname2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.VALUE_CONSTANT__VCNAME2, null, msgs);
      msgs = basicSetVcname2(newVcname2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.VALUE_CONSTANT__VCNAME2, newVcname2, newVcname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVcname3()
  {
    return vcname3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVcname3(String newVcname3)
  {
    String oldVcname3 = vcname3;
    vcname3 = newVcname3;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.VALUE_CONSTANT__VCNAME3, oldVcname3, vcname3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVcname4()
  {
    return vcname4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVcname4(String newVcname4)
  {
    String oldVcname4 = vcname4;
    vcname4 = newVcname4;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.VALUE_CONSTANT__VCNAME4, oldVcname4, vcname4));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME1:
        return basicSetVcname1(null, msgs);
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME2:
        return basicSetVcname2(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME1:
        return getVcname1();
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME2:
        return getVcname2();
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME3:
        return getVcname3();
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME4:
        return getVcname4();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME1:
        setVcname1((boolean_literal)newValue);
        return;
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME2:
        setVcname2((numeric_literal)newValue);
        return;
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME3:
        setVcname3((String)newValue);
        return;
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME4:
        setVcname4((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME1:
        setVcname1((boolean_literal)null);
        return;
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME2:
        setVcname2((numeric_literal)null);
        return;
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME3:
        setVcname3(VCNAME3_EDEFAULT);
        return;
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME4:
        setVcname4(VCNAME4_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME1:
        return vcname1 != null;
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME2:
        return vcname2 != null;
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME3:
        return VCNAME3_EDEFAULT == null ? vcname3 != null : !VCNAME3_EDEFAULT.equals(vcname3);
      case BaGuardsPackage.VALUE_CONSTANT__VCNAME4:
        return VCNAME4_EDEFAULT == null ? vcname4 != null : !VCNAME4_EDEFAULT.equals(vcname4);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (vcname3: ");
    result.append(vcname3);
    result.append(", vcname4: ");
    result.append(vcname4);
    result.append(')');
    return result.toString();
  }

} //value_constantImpl
