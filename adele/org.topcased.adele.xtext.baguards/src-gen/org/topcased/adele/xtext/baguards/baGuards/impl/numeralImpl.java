/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.integer;
import org.topcased.adele.xtext.baguards.baGuards.numeral;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>numeral</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.numeralImpl#getNname1 <em>Nname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.numeralImpl#getNname2 <em>Nname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class numeralImpl extends MinimalEObjectImpl.Container implements numeral
{
  /**
   * The cached value of the '{@link #getNname1() <em>Nname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNname1()
   * @generated
   * @ordered
   */
  protected integer nname1;

  /**
   * The cached value of the '{@link #getNname2() <em>Nname2</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNname2()
   * @generated
   * @ordered
   */
  protected EList<integer> nname2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected numeralImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.NUMERAL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public integer getNname1()
  {
    return nname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNname1(integer newNname1, NotificationChain msgs)
  {
    integer oldNname1 = nname1;
    nname1 = newNname1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.NUMERAL__NNAME1, oldNname1, newNname1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNname1(integer newNname1)
  {
    if (newNname1 != nname1)
    {
      NotificationChain msgs = null;
      if (nname1 != null)
        msgs = ((InternalEObject)nname1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.NUMERAL__NNAME1, null, msgs);
      if (newNname1 != null)
        msgs = ((InternalEObject)newNname1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.NUMERAL__NNAME1, null, msgs);
      msgs = basicSetNname1(newNname1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.NUMERAL__NNAME1, newNname1, newNname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<integer> getNname2()
  {
    if (nname2 == null)
    {
      nname2 = new EObjectContainmentEList<integer>(integer.class, this, BaGuardsPackage.NUMERAL__NNAME2);
    }
    return nname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.NUMERAL__NNAME1:
        return basicSetNname1(null, msgs);
      case BaGuardsPackage.NUMERAL__NNAME2:
        return ((InternalEList<?>)getNname2()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.NUMERAL__NNAME1:
        return getNname1();
      case BaGuardsPackage.NUMERAL__NNAME2:
        return getNname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.NUMERAL__NNAME1:
        setNname1((integer)newValue);
        return;
      case BaGuardsPackage.NUMERAL__NNAME2:
        getNname2().clear();
        getNname2().addAll((Collection<? extends integer>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.NUMERAL__NNAME1:
        setNname1((integer)null);
        return;
      case BaGuardsPackage.NUMERAL__NNAME2:
        getNname2().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.NUMERAL__NNAME1:
        return nname1 != null;
      case BaGuardsPackage.NUMERAL__NNAME2:
        return nname2 != null && !nname2.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //numeralImpl
