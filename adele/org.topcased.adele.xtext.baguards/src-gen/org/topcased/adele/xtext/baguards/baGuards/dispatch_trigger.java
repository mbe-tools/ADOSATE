/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dispatch trigger</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger#getDtname <em>Dtname</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdispatch_trigger()
 * @model
 * @generated
 */
public interface dispatch_trigger extends EObject
{
  /**
   * Returns the value of the '<em><b>Dtname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dtname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dtname</em>' attribute.
   * @see #setDtname(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdispatch_trigger_Dtname()
   * @model
   * @generated
   */
  String getDtname();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger#getDtname <em>Dtname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dtname</em>' attribute.
   * @see #getDtname()
   * @generated
   */
  void setDtname(String value);

} // dispatch_trigger
