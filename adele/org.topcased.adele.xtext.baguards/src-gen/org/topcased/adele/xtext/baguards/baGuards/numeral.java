/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>numeral</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.numeral#getNname1 <em>Nname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.numeral#getNname2 <em>Nname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getnumeral()
 * @model
 * @generated
 */
public interface numeral extends EObject
{
  /**
   * Returns the value of the '<em><b>Nname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nname1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nname1</em>' containment reference.
   * @see #setNname1(integer)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getnumeral_Nname1()
   * @model containment="true"
   * @generated
   */
  integer getNname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.numeral#getNname1 <em>Nname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nname1</em>' containment reference.
   * @see #getNname1()
   * @generated
   */
  void setNname1(integer value);

  /**
   * Returns the value of the '<em><b>Nname2</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baguards.baGuards.integer}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nname2</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nname2</em>' containment reference list.
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getnumeral_Nname2()
   * @model containment="true"
   * @generated
   */
  EList<integer> getNname2();

} // numeral
