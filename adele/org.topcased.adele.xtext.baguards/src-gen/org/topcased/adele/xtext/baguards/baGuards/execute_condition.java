/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>execute condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.execute_condition#getEcname1 <em>Ecname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.execute_condition#getEcname2 <em>Ecname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.execute_condition#getEcname3 <em>Ecname3</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getexecute_condition()
 * @model
 * @generated
 */
public interface execute_condition extends EObject
{
  /**
   * Returns the value of the '<em><b>Ecname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ecname1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ecname1</em>' attribute.
   * @see #setEcname1(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getexecute_condition_Ecname1()
   * @model
   * @generated
   */
  String getEcname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.execute_condition#getEcname1 <em>Ecname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ecname1</em>' attribute.
   * @see #getEcname1()
   * @generated
   */
  void setEcname1(String value);

  /**
   * Returns the value of the '<em><b>Ecname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ecname2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ecname2</em>' containment reference.
   * @see #setEcname2(behavior_action_block_timeout_catch)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getexecute_condition_Ecname2()
   * @model containment="true"
   * @generated
   */
  behavior_action_block_timeout_catch getEcname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.execute_condition#getEcname2 <em>Ecname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ecname2</em>' containment reference.
   * @see #getEcname2()
   * @generated
   */
  void setEcname2(behavior_action_block_timeout_catch value);

  /**
   * Returns the value of the '<em><b>Ecname3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ecname3</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ecname3</em>' containment reference.
   * @see #setEcname3(value_expression)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getexecute_condition_Ecname3()
   * @model containment="true"
   * @generated
   */
  value_expression getEcname3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.execute_condition#getEcname3 <em>Ecname3</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ecname3</em>' containment reference.
   * @see #getEcname3()
   * @generated
   */
  void setEcname3(value_expression value);

} // execute_condition
