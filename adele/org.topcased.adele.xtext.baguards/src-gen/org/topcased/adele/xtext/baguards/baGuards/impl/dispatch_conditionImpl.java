/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_condition;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition;
import org.topcased.adele.xtext.baguards.baGuards.frozen_ports;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dispatch condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_conditionImpl#getDcname3 <em>Dcname3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_conditionImpl#getDcname1 <em>Dcname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.dispatch_conditionImpl#getDcname2 <em>Dcname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class dispatch_conditionImpl extends MinimalEObjectImpl.Container implements dispatch_condition
{
  /**
   * The default value of the '{@link #getDcname3() <em>Dcname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDcname3()
   * @generated
   * @ordered
   */
  protected static final String DCNAME3_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDcname3() <em>Dcname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDcname3()
   * @generated
   * @ordered
   */
  protected String dcname3 = DCNAME3_EDEFAULT;

  /**
   * The cached value of the '{@link #getDcname1() <em>Dcname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDcname1()
   * @generated
   * @ordered
   */
  protected dispatch_trigger_condition dcname1;

  /**
   * The cached value of the '{@link #getDcname2() <em>Dcname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDcname2()
   * @generated
   * @ordered
   */
  protected frozen_ports dcname2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected dispatch_conditionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.DISPATCH_CONDITION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDcname3()
  {
    return dcname3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDcname3(String newDcname3)
  {
    String oldDcname3 = dcname3;
    dcname3 = newDcname3;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DISPATCH_CONDITION__DCNAME3, oldDcname3, dcname3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public dispatch_trigger_condition getDcname1()
  {
    return dcname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDcname1(dispatch_trigger_condition newDcname1, NotificationChain msgs)
  {
    dispatch_trigger_condition oldDcname1 = dcname1;
    dcname1 = newDcname1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DISPATCH_CONDITION__DCNAME1, oldDcname1, newDcname1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDcname1(dispatch_trigger_condition newDcname1)
  {
    if (newDcname1 != dcname1)
    {
      NotificationChain msgs = null;
      if (dcname1 != null)
        msgs = ((InternalEObject)dcname1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DISPATCH_CONDITION__DCNAME1, null, msgs);
      if (newDcname1 != null)
        msgs = ((InternalEObject)newDcname1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DISPATCH_CONDITION__DCNAME1, null, msgs);
      msgs = basicSetDcname1(newDcname1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DISPATCH_CONDITION__DCNAME1, newDcname1, newDcname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public frozen_ports getDcname2()
  {
    return dcname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDcname2(frozen_ports newDcname2, NotificationChain msgs)
  {
    frozen_ports oldDcname2 = dcname2;
    dcname2 = newDcname2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DISPATCH_CONDITION__DCNAME2, oldDcname2, newDcname2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDcname2(frozen_ports newDcname2)
  {
    if (newDcname2 != dcname2)
    {
      NotificationChain msgs = null;
      if (dcname2 != null)
        msgs = ((InternalEObject)dcname2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DISPATCH_CONDITION__DCNAME2, null, msgs);
      if (newDcname2 != null)
        msgs = ((InternalEObject)newDcname2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.DISPATCH_CONDITION__DCNAME2, null, msgs);
      msgs = basicSetDcname2(newDcname2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.DISPATCH_CONDITION__DCNAME2, newDcname2, newDcname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_CONDITION__DCNAME1:
        return basicSetDcname1(null, msgs);
      case BaGuardsPackage.DISPATCH_CONDITION__DCNAME2:
        return basicSetDcname2(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_CONDITION__DCNAME3:
        return getDcname3();
      case BaGuardsPackage.DISPATCH_CONDITION__DCNAME1:
        return getDcname1();
      case BaGuardsPackage.DISPATCH_CONDITION__DCNAME2:
        return getDcname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_CONDITION__DCNAME3:
        setDcname3((String)newValue);
        return;
      case BaGuardsPackage.DISPATCH_CONDITION__DCNAME1:
        setDcname1((dispatch_trigger_condition)newValue);
        return;
      case BaGuardsPackage.DISPATCH_CONDITION__DCNAME2:
        setDcname2((frozen_ports)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_CONDITION__DCNAME3:
        setDcname3(DCNAME3_EDEFAULT);
        return;
      case BaGuardsPackage.DISPATCH_CONDITION__DCNAME1:
        setDcname1((dispatch_trigger_condition)null);
        return;
      case BaGuardsPackage.DISPATCH_CONDITION__DCNAME2:
        setDcname2((frozen_ports)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.DISPATCH_CONDITION__DCNAME3:
        return DCNAME3_EDEFAULT == null ? dcname3 != null : !DCNAME3_EDEFAULT.equals(dcname3);
      case BaGuardsPackage.DISPATCH_CONDITION__DCNAME1:
        return dcname1 != null;
      case BaGuardsPackage.DISPATCH_CONDITION__DCNAME2:
        return dcname2 != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (dcname3: ");
    result.append(dcname3);
    result.append(')');
    return result.toString();
  }

} //dispatch_conditionImpl
