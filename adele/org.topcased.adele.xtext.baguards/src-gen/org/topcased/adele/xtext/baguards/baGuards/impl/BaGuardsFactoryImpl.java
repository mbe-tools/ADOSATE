/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.topcased.adele.xtext.baguards.baGuards.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BaGuardsFactoryImpl extends EFactoryImpl implements BaGuardsFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static BaGuardsFactory init()
  {
    try
    {
      BaGuardsFactory theBaGuardsFactory = (BaGuardsFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.topcased.org/adele/xtext/baguards/BaGuards"); 
      if (theBaGuardsFactory != null)
      {
        return theBaGuardsFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new BaGuardsFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaGuardsFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case BaGuardsPackage.GUARD: return createGuard();
      case BaGuardsPackage.EXECUTE_CONDITION: return createexecute_condition();
      case BaGuardsPackage.BEHAVIOR_ACTION_BLOCK_TIMEOUT_CATCH: return createbehavior_action_block_timeout_catch();
      case BaGuardsPackage.VALUE_EXPRESSION: return createvalue_expression();
      case BaGuardsPackage.RELATION: return createrelation();
      case BaGuardsPackage.SIMPLE_EXPRESSION: return createsimple_expression();
      case BaGuardsPackage.TERM: return createterm();
      case BaGuardsPackage.FACTOR: return createfactor();
      case BaGuardsPackage.VALUE: return createvalue();
      case BaGuardsPackage.VALUE_VARIABLE: return createvalue_variable();
      case BaGuardsPackage.VALUE_CONSTANT: return createvalue_constant();
      case BaGuardsPackage.LOGICAL_OPERATOR: return createlogical_operator();
      case BaGuardsPackage.RELATIONAL_OPERATOR: return createrelational_operator();
      case BaGuardsPackage.BINARY_ADDING_OPERATOR: return createbinary_adding_operator();
      case BaGuardsPackage.UNARY_ADDING_OPERATOR: return createunary_adding_operator();
      case BaGuardsPackage.MULTIPLYING_OPERATOR: return createmultiplying_operator();
      case BaGuardsPackage.BINARY_NUMERIC_OPERATOR: return createbinary_numeric_operator();
      case BaGuardsPackage.UNARY_NUMERIC_OPERATOR: return createunary_numeric_operator();
      case BaGuardsPackage.UNARY_BOOLEAN_OPERATOR: return createunary_boolean_operator();
      case BaGuardsPackage.BOOLEAN_LITERAL: return createboolean_literal();
      case BaGuardsPackage.INTEGER_VALUE: return createinteger_value();
      case BaGuardsPackage.BEHAVIOR_TIME: return createbehavior_time();
      case BaGuardsPackage.UNIT_IDENTIFIER: return createunit_identifier();
      case BaGuardsPackage.NUMERIC_LITERAL: return createnumeric_literal();
      case BaGuardsPackage.INTEGER_LITERAL: return createinteger_literal();
      case BaGuardsPackage.REAL_LITERAL: return createreal_literal();
      case BaGuardsPackage.DECIMAL_INTEGER_LITERAL: return createdecimal_integer_literal();
      case BaGuardsPackage.DECIMAL_REAL_LITERAL: return createdecimal_real_literal();
      case BaGuardsPackage.NUMERAL: return createnumeral();
      case BaGuardsPackage.EXPONENT: return createexponent();
      case BaGuardsPackage.POSITIVE_EXPONENT: return createpositive_exponent();
      case BaGuardsPackage.BASED_INTEGER_LITERAL: return createbased_integer_literal();
      case BaGuardsPackage.BASE: return createbase();
      case BaGuardsPackage.BASED_NUMERAL: return createbased_numeral();
      case BaGuardsPackage.INTEGER: return createinteger();
      case BaGuardsPackage.DISPATCH_CONDITION: return createdispatch_condition();
      case BaGuardsPackage.DISPATCH_TRIGGER_CONDITION: return createdispatch_trigger_condition();
      case BaGuardsPackage.DISPATCH_TRIGGER_LOGICAL_EXPRESSION: return createdispatch_trigger_logical_expression();
      case BaGuardsPackage.DISPATCH_CONJUNCTION: return createdispatch_conjunction();
      case BaGuardsPackage.COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH: return createcompletion_relative_timeout_condition_and_catch();
      case BaGuardsPackage.DISPATCH_TRIGGER: return createdispatch_trigger();
      case BaGuardsPackage.FROZEN_PORTS: return createfrozen_ports();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Guard createGuard()
  {
    GuardImpl guard = new GuardImpl();
    return guard;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public execute_condition createexecute_condition()
  {
    execute_conditionImpl execute_condition = new execute_conditionImpl();
    return execute_condition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_action_block_timeout_catch createbehavior_action_block_timeout_catch()
  {
    behavior_action_block_timeout_catchImpl behavior_action_block_timeout_catch = new behavior_action_block_timeout_catchImpl();
    return behavior_action_block_timeout_catch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value_expression createvalue_expression()
  {
    value_expressionImpl value_expression = new value_expressionImpl();
    return value_expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public relation createrelation()
  {
    relationImpl relation = new relationImpl();
    return relation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public simple_expression createsimple_expression()
  {
    simple_expressionImpl simple_expression = new simple_expressionImpl();
    return simple_expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public term createterm()
  {
    termImpl term = new termImpl();
    return term;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public factor createfactor()
  {
    factorImpl factor = new factorImpl();
    return factor;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value createvalue()
  {
    valueImpl value = new valueImpl();
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value_variable createvalue_variable()
  {
    value_variableImpl value_variable = new value_variableImpl();
    return value_variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value_constant createvalue_constant()
  {
    value_constantImpl value_constant = new value_constantImpl();
    return value_constant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public logical_operator createlogical_operator()
  {
    logical_operatorImpl logical_operator = new logical_operatorImpl();
    return logical_operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public relational_operator createrelational_operator()
  {
    relational_operatorImpl relational_operator = new relational_operatorImpl();
    return relational_operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public binary_adding_operator createbinary_adding_operator()
  {
    binary_adding_operatorImpl binary_adding_operator = new binary_adding_operatorImpl();
    return binary_adding_operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public unary_adding_operator createunary_adding_operator()
  {
    unary_adding_operatorImpl unary_adding_operator = new unary_adding_operatorImpl();
    return unary_adding_operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public multiplying_operator createmultiplying_operator()
  {
    multiplying_operatorImpl multiplying_operator = new multiplying_operatorImpl();
    return multiplying_operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public binary_numeric_operator createbinary_numeric_operator()
  {
    binary_numeric_operatorImpl binary_numeric_operator = new binary_numeric_operatorImpl();
    return binary_numeric_operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public unary_numeric_operator createunary_numeric_operator()
  {
    unary_numeric_operatorImpl unary_numeric_operator = new unary_numeric_operatorImpl();
    return unary_numeric_operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public unary_boolean_operator createunary_boolean_operator()
  {
    unary_boolean_operatorImpl unary_boolean_operator = new unary_boolean_operatorImpl();
    return unary_boolean_operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean_literal createboolean_literal()
  {
    boolean_literalImpl boolean_literal = new boolean_literalImpl();
    return boolean_literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public integer_value createinteger_value()
  {
    integer_valueImpl integer_value = new integer_valueImpl();
    return integer_value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_time createbehavior_time()
  {
    behavior_timeImpl behavior_time = new behavior_timeImpl();
    return behavior_time;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public unit_identifier createunit_identifier()
  {
    unit_identifierImpl unit_identifier = new unit_identifierImpl();
    return unit_identifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public numeric_literal createnumeric_literal()
  {
    numeric_literalImpl numeric_literal = new numeric_literalImpl();
    return numeric_literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public integer_literal createinteger_literal()
  {
    integer_literalImpl integer_literal = new integer_literalImpl();
    return integer_literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public real_literal createreal_literal()
  {
    real_literalImpl real_literal = new real_literalImpl();
    return real_literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public decimal_integer_literal createdecimal_integer_literal()
  {
    decimal_integer_literalImpl decimal_integer_literal = new decimal_integer_literalImpl();
    return decimal_integer_literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public decimal_real_literal createdecimal_real_literal()
  {
    decimal_real_literalImpl decimal_real_literal = new decimal_real_literalImpl();
    return decimal_real_literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public numeral createnumeral()
  {
    numeralImpl numeral = new numeralImpl();
    return numeral;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public exponent createexponent()
  {
    exponentImpl exponent = new exponentImpl();
    return exponent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public positive_exponent createpositive_exponent()
  {
    positive_exponentImpl positive_exponent = new positive_exponentImpl();
    return positive_exponent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public based_integer_literal createbased_integer_literal()
  {
    based_integer_literalImpl based_integer_literal = new based_integer_literalImpl();
    return based_integer_literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public base createbase()
  {
    baseImpl base = new baseImpl();
    return base;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public based_numeral createbased_numeral()
  {
    based_numeralImpl based_numeral = new based_numeralImpl();
    return based_numeral;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public integer createinteger()
  {
    integerImpl integer = new integerImpl();
    return integer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public dispatch_condition createdispatch_condition()
  {
    dispatch_conditionImpl dispatch_condition = new dispatch_conditionImpl();
    return dispatch_condition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public dispatch_trigger_condition createdispatch_trigger_condition()
  {
    dispatch_trigger_conditionImpl dispatch_trigger_condition = new dispatch_trigger_conditionImpl();
    return dispatch_trigger_condition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public dispatch_trigger_logical_expression createdispatch_trigger_logical_expression()
  {
    dispatch_trigger_logical_expressionImpl dispatch_trigger_logical_expression = new dispatch_trigger_logical_expressionImpl();
    return dispatch_trigger_logical_expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public dispatch_conjunction createdispatch_conjunction()
  {
    dispatch_conjunctionImpl dispatch_conjunction = new dispatch_conjunctionImpl();
    return dispatch_conjunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public completion_relative_timeout_condition_and_catch createcompletion_relative_timeout_condition_and_catch()
  {
    completion_relative_timeout_condition_and_catchImpl completion_relative_timeout_condition_and_catch = new completion_relative_timeout_condition_and_catchImpl();
    return completion_relative_timeout_condition_and_catch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public dispatch_trigger createdispatch_trigger()
  {
    dispatch_triggerImpl dispatch_trigger = new dispatch_triggerImpl();
    return dispatch_trigger;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public frozen_ports createfrozen_ports()
  {
    frozen_portsImpl frozen_ports = new frozen_portsImpl();
    return frozen_ports;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaGuardsPackage getBaGuardsPackage()
  {
    return (BaGuardsPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static BaGuardsPackage getPackage()
  {
    return BaGuardsPackage.eINSTANCE;
  }

} //BaGuardsFactoryImpl
