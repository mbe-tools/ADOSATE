/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.behavior_action_block_timeout_catch;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>behavior action block timeout catch</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.behavior_action_block_timeout_catchImpl#getBabname <em>Babname</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class behavior_action_block_timeout_catchImpl extends MinimalEObjectImpl.Container implements behavior_action_block_timeout_catch
{
  /**
   * The cached value of the '{@link #getBabname() <em>Babname</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBabname()
   * @generated
   * @ordered
   */
  protected EList<String> babname;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected behavior_action_block_timeout_catchImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.BEHAVIOR_ACTION_BLOCK_TIMEOUT_CATCH;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getBabname()
  {
    if (babname == null)
    {
      babname = new EDataTypeEList<String>(String.class, this, BaGuardsPackage.BEHAVIOR_ACTION_BLOCK_TIMEOUT_CATCH__BABNAME);
    }
    return babname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.BEHAVIOR_ACTION_BLOCK_TIMEOUT_CATCH__BABNAME:
        return getBabname();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.BEHAVIOR_ACTION_BLOCK_TIMEOUT_CATCH__BABNAME:
        getBabname().clear();
        getBabname().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.BEHAVIOR_ACTION_BLOCK_TIMEOUT_CATCH__BABNAME:
        getBabname().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.BEHAVIOR_ACTION_BLOCK_TIMEOUT_CATCH__BABNAME:
        return babname != null && !babname.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (babname: ");
    result.append(babname);
    result.append(')');
    return result.toString();
  }

} //behavior_action_block_timeout_catchImpl
