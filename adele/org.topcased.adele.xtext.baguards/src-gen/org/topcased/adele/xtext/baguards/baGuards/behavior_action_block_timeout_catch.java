/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>behavior action block timeout catch</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.behavior_action_block_timeout_catch#getBabname <em>Babname</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getbehavior_action_block_timeout_catch()
 * @model
 * @generated
 */
public interface behavior_action_block_timeout_catch extends EObject
{
  /**
   * Returns the value of the '<em><b>Babname</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Babname</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Babname</em>' attribute list.
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getbehavior_action_block_timeout_catch_Babname()
   * @model unique="false"
   * @generated
   */
  EList<String> getBabname();

} // behavior_action_block_timeout_catch
