/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.frozen_ports;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>frozen ports</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.frozen_portsImpl#getFpname1 <em>Fpname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.frozen_portsImpl#getFpname2 <em>Fpname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class frozen_portsImpl extends MinimalEObjectImpl.Container implements frozen_ports
{
  /**
   * The default value of the '{@link #getFpname1() <em>Fpname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFpname1()
   * @generated
   * @ordered
   */
  protected static final String FPNAME1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getFpname1() <em>Fpname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFpname1()
   * @generated
   * @ordered
   */
  protected String fpname1 = FPNAME1_EDEFAULT;

  /**
   * The cached value of the '{@link #getFpname2() <em>Fpname2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFpname2()
   * @generated
   * @ordered
   */
  protected EList<String> fpname2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected frozen_portsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.FROZEN_PORTS;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getFpname1()
  {
    return fpname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFpname1(String newFpname1)
  {
    String oldFpname1 = fpname1;
    fpname1 = newFpname1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.FROZEN_PORTS__FPNAME1, oldFpname1, fpname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getFpname2()
  {
    if (fpname2 == null)
    {
      fpname2 = new EDataTypeEList<String>(String.class, this, BaGuardsPackage.FROZEN_PORTS__FPNAME2);
    }
    return fpname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.FROZEN_PORTS__FPNAME1:
        return getFpname1();
      case BaGuardsPackage.FROZEN_PORTS__FPNAME2:
        return getFpname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.FROZEN_PORTS__FPNAME1:
        setFpname1((String)newValue);
        return;
      case BaGuardsPackage.FROZEN_PORTS__FPNAME2:
        getFpname2().clear();
        getFpname2().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.FROZEN_PORTS__FPNAME1:
        setFpname1(FPNAME1_EDEFAULT);
        return;
      case BaGuardsPackage.FROZEN_PORTS__FPNAME2:
        getFpname2().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.FROZEN_PORTS__FPNAME1:
        return FPNAME1_EDEFAULT == null ? fpname1 != null : !FPNAME1_EDEFAULT.equals(fpname1);
      case BaGuardsPackage.FROZEN_PORTS__FPNAME2:
        return fpname2 != null && !fpname2.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (fpname1: ");
    result.append(fpname1);
    result.append(", fpname2: ");
    result.append(fpname2);
    result.append(')');
    return result.toString();
  }

} //frozen_portsImpl
