/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsFactory;
import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.Guard;
import org.topcased.adele.xtext.baguards.baGuards.base;
import org.topcased.adele.xtext.baguards.baGuards.based_integer_literal;
import org.topcased.adele.xtext.baguards.baGuards.based_numeral;
import org.topcased.adele.xtext.baguards.baGuards.behavior_action_block_timeout_catch;
import org.topcased.adele.xtext.baguards.baGuards.behavior_time;
import org.topcased.adele.xtext.baguards.baGuards.binary_adding_operator;
import org.topcased.adele.xtext.baguards.baGuards.binary_numeric_operator;
import org.topcased.adele.xtext.baguards.baGuards.boolean_literal;
import org.topcased.adele.xtext.baguards.baGuards.completion_relative_timeout_condition_and_catch;
import org.topcased.adele.xtext.baguards.baGuards.decimal_integer_literal;
import org.topcased.adele.xtext.baguards.baGuards.decimal_real_literal;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_condition;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_conjunction;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_condition;
import org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_logical_expression;
import org.topcased.adele.xtext.baguards.baGuards.execute_condition;
import org.topcased.adele.xtext.baguards.baGuards.exponent;
import org.topcased.adele.xtext.baguards.baGuards.factor;
import org.topcased.adele.xtext.baguards.baGuards.frozen_ports;
import org.topcased.adele.xtext.baguards.baGuards.integer;
import org.topcased.adele.xtext.baguards.baGuards.integer_literal;
import org.topcased.adele.xtext.baguards.baGuards.integer_value;
import org.topcased.adele.xtext.baguards.baGuards.logical_operator;
import org.topcased.adele.xtext.baguards.baGuards.multiplying_operator;
import org.topcased.adele.xtext.baguards.baGuards.numeral;
import org.topcased.adele.xtext.baguards.baGuards.numeric_literal;
import org.topcased.adele.xtext.baguards.baGuards.positive_exponent;
import org.topcased.adele.xtext.baguards.baGuards.real_literal;
import org.topcased.adele.xtext.baguards.baGuards.relation;
import org.topcased.adele.xtext.baguards.baGuards.relational_operator;
import org.topcased.adele.xtext.baguards.baGuards.simple_expression;
import org.topcased.adele.xtext.baguards.baGuards.term;
import org.topcased.adele.xtext.baguards.baGuards.unary_adding_operator;
import org.topcased.adele.xtext.baguards.baGuards.unary_boolean_operator;
import org.topcased.adele.xtext.baguards.baGuards.unary_numeric_operator;
import org.topcased.adele.xtext.baguards.baGuards.unit_identifier;
import org.topcased.adele.xtext.baguards.baGuards.value;
import org.topcased.adele.xtext.baguards.baGuards.value_constant;
import org.topcased.adele.xtext.baguards.baGuards.value_expression;
import org.topcased.adele.xtext.baguards.baGuards.value_variable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BaGuardsPackageImpl extends EPackageImpl implements BaGuardsPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass guardEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass execute_conditionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass behavior_action_block_timeout_catchEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass value_expressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass relationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simple_expressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass termEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass factorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass valueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass value_variableEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass value_constantEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass logical_operatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass relational_operatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass binary_adding_operatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unary_adding_operatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass multiplying_operatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass binary_numeric_operatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unary_numeric_operatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unary_boolean_operatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boolean_literalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integer_valueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass behavior_timeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unit_identifierEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass numeric_literalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integer_literalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass real_literalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass decimal_integer_literalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass decimal_real_literalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass numeralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass exponentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass positive_exponentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass based_integer_literalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass baseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass based_numeralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integerEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass dispatch_conditionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass dispatch_trigger_conditionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass dispatch_trigger_logical_expressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass dispatch_conjunctionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass completion_relative_timeout_condition_and_catchEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass dispatch_triggerEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass frozen_portsEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private BaGuardsPackageImpl()
  {
    super(eNS_URI, BaGuardsFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link BaGuardsPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static BaGuardsPackage init()
  {
    if (isInited) return (BaGuardsPackage)EPackage.Registry.INSTANCE.getEPackage(BaGuardsPackage.eNS_URI);

    // Obtain or create and register package
    BaGuardsPackageImpl theBaGuardsPackage = (BaGuardsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof BaGuardsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new BaGuardsPackageImpl());

    isInited = true;

    // Create package meta-data objects
    theBaGuardsPackage.createPackageContents();

    // Initialize created meta-data
    theBaGuardsPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theBaGuardsPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(BaGuardsPackage.eNS_URI, theBaGuardsPackage);
    return theBaGuardsPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGuard()
  {
    return guardEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGuard_Dispatch()
  {
    return (EReference)guardEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGuard_Execute()
  {
    return (EReference)guardEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexecute_condition()
  {
    return execute_conditionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getexecute_condition_Ecname1()
  {
    return (EAttribute)execute_conditionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getexecute_condition_Ecname2()
  {
    return (EReference)execute_conditionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getexecute_condition_Ecname3()
  {
    return (EReference)execute_conditionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbehavior_action_block_timeout_catch()
  {
    return behavior_action_block_timeout_catchEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_block_timeout_catch_Babname()
  {
    return (EAttribute)behavior_action_block_timeout_catchEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getvalue_expression()
  {
    return value_expressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getvalue_expression_Vename1()
  {
    return (EReference)value_expressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getvalue_expression_Vename3()
  {
    return (EReference)value_expressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getvalue_expression_Vename2()
  {
    return (EReference)value_expressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getrelation()
  {
    return relationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getrelation_Rname1()
  {
    return (EReference)relationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getrelation_Rname3()
  {
    return (EReference)relationEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getrelation_Rname2()
  {
    return (EReference)relationEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getsimple_expression()
  {
    return simple_expressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getsimple_expression_Sename2()
  {
    return (EReference)simple_expressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getsimple_expression_Sename1()
  {
    return (EReference)simple_expressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getsimple_expression_Sename3()
  {
    return (EReference)simple_expressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getsimple_expression_Sename4()
  {
    return (EReference)simple_expressionEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getterm()
  {
    return termEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getterm_Tname1()
  {
    return (EReference)termEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getterm_Tname3()
  {
    return (EReference)termEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getterm_Tname2()
  {
    return (EReference)termEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getfactor()
  {
    return factorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getfactor_Fname1()
  {
    return (EReference)factorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getfactor_Fname3()
  {
    return (EReference)factorEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getfactor_Fname2()
  {
    return (EReference)factorEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getfactor_Fname4()
  {
    return (EReference)factorEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getfactor_Fname5()
  {
    return (EReference)factorEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getfactor_Fname6()
  {
    return (EReference)factorEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getfactor_Fname7()
  {
    return (EReference)factorEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getvalue()
  {
    return valueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getvalue_Vname1()
  {
    return (EReference)valueEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getvalue_Vname2()
  {
    return (EReference)valueEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getvalue_variable()
  {
    return value_variableEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getvalue_variable_Vvname0()
  {
    return (EAttribute)value_variableEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getvalue_variable_Vvname1()
  {
    return (EAttribute)value_variableEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getvalue_variable_Vvname2()
  {
    return (EAttribute)value_variableEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getvalue_variable_Vvname3()
  {
    return (EAttribute)value_variableEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getvalue_constant()
  {
    return value_constantEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getvalue_constant_Vcname1()
  {
    return (EReference)value_constantEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getvalue_constant_Vcname2()
  {
    return (EReference)value_constantEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getvalue_constant_Vcname3()
  {
    return (EAttribute)value_constantEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getvalue_constant_Vcname4()
  {
    return (EAttribute)value_constantEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getlogical_operator()
  {
    return logical_operatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getlogical_operator_Loname1()
  {
    return (EAttribute)logical_operatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getlogical_operator_Loname2()
  {
    return (EAttribute)logical_operatorEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getlogical_operator_Loname3()
  {
    return (EAttribute)logical_operatorEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getrelational_operator()
  {
    return relational_operatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getrelational_operator_Roname1()
  {
    return (EAttribute)relational_operatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getrelational_operator_Roname2()
  {
    return (EAttribute)relational_operatorEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getrelational_operator_Roname3()
  {
    return (EAttribute)relational_operatorEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getrelational_operator_Roname4()
  {
    return (EAttribute)relational_operatorEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getrelational_operator_Roname5()
  {
    return (EAttribute)relational_operatorEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getrelational_operator_Roname6()
  {
    return (EAttribute)relational_operatorEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbinary_adding_operator()
  {
    return binary_adding_operatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbinary_adding_operator_Baoname1()
  {
    return (EAttribute)binary_adding_operatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbinary_adding_operator_Baoname2()
  {
    return (EAttribute)binary_adding_operatorEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getunary_adding_operator()
  {
    return unary_adding_operatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunary_adding_operator_Uaoname1()
  {
    return (EAttribute)unary_adding_operatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunary_adding_operator_Uaoname2()
  {
    return (EAttribute)unary_adding_operatorEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getmultiplying_operator()
  {
    return multiplying_operatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getmultiplying_operator_Moname1()
  {
    return (EAttribute)multiplying_operatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getmultiplying_operator_Moname2()
  {
    return (EAttribute)multiplying_operatorEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getmultiplying_operator_Moname3()
  {
    return (EAttribute)multiplying_operatorEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getmultiplying_operator_Moname4()
  {
    return (EAttribute)multiplying_operatorEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbinary_numeric_operator()
  {
    return binary_numeric_operatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbinary_numeric_operator_Bnoname()
  {
    return (EAttribute)binary_numeric_operatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getunary_numeric_operator()
  {
    return unary_numeric_operatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunary_numeric_operator_Unoname()
  {
    return (EAttribute)unary_numeric_operatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getunary_boolean_operator()
  {
    return unary_boolean_operatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunary_boolean_operator_Uboname()
  {
    return (EAttribute)unary_boolean_operatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getboolean_literal()
  {
    return boolean_literalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getboolean_literal_Blname1()
  {
    return (EAttribute)boolean_literalEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getinteger_value()
  {
    return integer_valueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getinteger_value_Ivname()
  {
    return (EReference)integer_valueEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbehavior_time()
  {
    return behavior_timeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_time_Btname1()
  {
    return (EReference)behavior_timeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_time_Btname2()
  {
    return (EReference)behavior_timeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getunit_identifier()
  {
    return unit_identifierEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunit_identifier_Uiname1()
  {
    return (EAttribute)unit_identifierEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunit_identifier_Uiname2()
  {
    return (EAttribute)unit_identifierEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunit_identifier_Uiname3()
  {
    return (EAttribute)unit_identifierEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunit_identifier_Uiname4()
  {
    return (EAttribute)unit_identifierEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunit_identifier_Uiname5()
  {
    return (EAttribute)unit_identifierEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunit_identifier_Uiname6()
  {
    return (EAttribute)unit_identifierEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunit_identifier_Uiname7()
  {
    return (EAttribute)unit_identifierEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getnumeric_literal()
  {
    return numeric_literalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getnumeric_literal_Nlname1()
  {
    return (EReference)numeric_literalEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getnumeric_literal_Nlname2()
  {
    return (EReference)numeric_literalEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getinteger_literal()
  {
    return integer_literalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getinteger_literal_Ilname1()
  {
    return (EReference)integer_literalEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getinteger_literal_Ilname2()
  {
    return (EReference)integer_literalEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getreal_literal()
  {
    return real_literalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getreal_literal_Rlname()
  {
    return (EReference)real_literalEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getdecimal_integer_literal()
  {
    return decimal_integer_literalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdecimal_integer_literal_Dilname1()
  {
    return (EReference)decimal_integer_literalEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdecimal_integer_literal_Dilname2()
  {
    return (EReference)decimal_integer_literalEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getdecimal_real_literal()
  {
    return decimal_real_literalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdecimal_real_literal_Drlname1()
  {
    return (EReference)decimal_real_literalEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdecimal_real_literal_Drlname2()
  {
    return (EReference)decimal_real_literalEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdecimal_real_literal_Drlname3()
  {
    return (EReference)decimal_real_literalEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getnumeral()
  {
    return numeralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getnumeral_Nname1()
  {
    return (EReference)numeralEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getnumeral_Nname2()
  {
    return (EReference)numeralEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexponent()
  {
    return exponentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getexponent_Ename1()
  {
    return (EReference)exponentEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getexponent_Ename2()
  {
    return (EReference)exponentEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getpositive_exponent()
  {
    return positive_exponentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getpositive_exponent_Pename()
  {
    return (EReference)positive_exponentEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbased_integer_literal()
  {
    return based_integer_literalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbased_integer_literal_Bilname1()
  {
    return (EReference)based_integer_literalEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbased_integer_literal_Bilname2()
  {
    return (EReference)based_integer_literalEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbased_integer_literal_Bilname3()
  {
    return (EReference)based_integer_literalEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbase()
  {
    return baseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbase_Bname1()
  {
    return (EAttribute)baseEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbase_Bname2()
  {
    return (EAttribute)baseEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbased_numeral()
  {
    return based_numeralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbased_numeral_Bnname1()
  {
    return (EAttribute)based_numeralEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbased_numeral_Bnname2()
  {
    return (EAttribute)based_numeralEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getinteger()
  {
    return integerEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getinteger_Value1()
  {
    return (EAttribute)integerEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getinteger_Value2()
  {
    return (EAttribute)integerEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getdispatch_condition()
  {
    return dispatch_conditionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getdispatch_condition_Dcname3()
  {
    return (EAttribute)dispatch_conditionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdispatch_condition_Dcname1()
  {
    return (EReference)dispatch_conditionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdispatch_condition_Dcname2()
  {
    return (EReference)dispatch_conditionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getdispatch_trigger_condition()
  {
    return dispatch_trigger_conditionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdispatch_trigger_condition_Dtcname1()
  {
    return (EReference)dispatch_trigger_conditionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getdispatch_trigger_condition_Dtcname4()
  {
    return (EAttribute)dispatch_trigger_conditionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdispatch_trigger_condition_Dtcname2()
  {
    return (EReference)dispatch_trigger_conditionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getdispatch_trigger_logical_expression()
  {
    return dispatch_trigger_logical_expressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdispatch_trigger_logical_expression_Dtlename1()
  {
    return (EReference)dispatch_trigger_logical_expressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdispatch_trigger_logical_expression_Dtlename2()
  {
    return (EReference)dispatch_trigger_logical_expressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getdispatch_conjunction()
  {
    return dispatch_conjunctionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdispatch_conjunction_Dcname1()
  {
    return (EReference)dispatch_conjunctionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdispatch_conjunction_Dcname2()
  {
    return (EReference)dispatch_conjunctionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getcompletion_relative_timeout_condition_and_catch()
  {
    return completion_relative_timeout_condition_and_catchEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcompletion_relative_timeout_condition_and_catch_Cdtcacname1()
  {
    return (EAttribute)completion_relative_timeout_condition_and_catchEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getcompletion_relative_timeout_condition_and_catch_Cdtcacname2()
  {
    return (EReference)completion_relative_timeout_condition_and_catchEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getdispatch_trigger()
  {
    return dispatch_triggerEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getdispatch_trigger_Dtname()
  {
    return (EAttribute)dispatch_triggerEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getfrozen_ports()
  {
    return frozen_portsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getfrozen_ports_Fpname1()
  {
    return (EAttribute)frozen_portsEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getfrozen_ports_Fpname2()
  {
    return (EAttribute)frozen_portsEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaGuardsFactory getBaGuardsFactory()
  {
    return (BaGuardsFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    guardEClass = createEClass(GUARD);
    createEReference(guardEClass, GUARD__DISPATCH);
    createEReference(guardEClass, GUARD__EXECUTE);

    execute_conditionEClass = createEClass(EXECUTE_CONDITION);
    createEAttribute(execute_conditionEClass, EXECUTE_CONDITION__ECNAME1);
    createEReference(execute_conditionEClass, EXECUTE_CONDITION__ECNAME2);
    createEReference(execute_conditionEClass, EXECUTE_CONDITION__ECNAME3);

    behavior_action_block_timeout_catchEClass = createEClass(BEHAVIOR_ACTION_BLOCK_TIMEOUT_CATCH);
    createEAttribute(behavior_action_block_timeout_catchEClass, BEHAVIOR_ACTION_BLOCK_TIMEOUT_CATCH__BABNAME);

    value_expressionEClass = createEClass(VALUE_EXPRESSION);
    createEReference(value_expressionEClass, VALUE_EXPRESSION__VENAME1);
    createEReference(value_expressionEClass, VALUE_EXPRESSION__VENAME3);
    createEReference(value_expressionEClass, VALUE_EXPRESSION__VENAME2);

    relationEClass = createEClass(RELATION);
    createEReference(relationEClass, RELATION__RNAME1);
    createEReference(relationEClass, RELATION__RNAME3);
    createEReference(relationEClass, RELATION__RNAME2);

    simple_expressionEClass = createEClass(SIMPLE_EXPRESSION);
    createEReference(simple_expressionEClass, SIMPLE_EXPRESSION__SENAME2);
    createEReference(simple_expressionEClass, SIMPLE_EXPRESSION__SENAME1);
    createEReference(simple_expressionEClass, SIMPLE_EXPRESSION__SENAME3);
    createEReference(simple_expressionEClass, SIMPLE_EXPRESSION__SENAME4);

    termEClass = createEClass(TERM);
    createEReference(termEClass, TERM__TNAME1);
    createEReference(termEClass, TERM__TNAME3);
    createEReference(termEClass, TERM__TNAME2);

    factorEClass = createEClass(FACTOR);
    createEReference(factorEClass, FACTOR__FNAME1);
    createEReference(factorEClass, FACTOR__FNAME3);
    createEReference(factorEClass, FACTOR__FNAME2);
    createEReference(factorEClass, FACTOR__FNAME4);
    createEReference(factorEClass, FACTOR__FNAME5);
    createEReference(factorEClass, FACTOR__FNAME6);
    createEReference(factorEClass, FACTOR__FNAME7);

    valueEClass = createEClass(VALUE);
    createEReference(valueEClass, VALUE__VNAME1);
    createEReference(valueEClass, VALUE__VNAME2);

    value_variableEClass = createEClass(VALUE_VARIABLE);
    createEAttribute(value_variableEClass, VALUE_VARIABLE__VVNAME0);
    createEAttribute(value_variableEClass, VALUE_VARIABLE__VVNAME1);
    createEAttribute(value_variableEClass, VALUE_VARIABLE__VVNAME2);
    createEAttribute(value_variableEClass, VALUE_VARIABLE__VVNAME3);

    value_constantEClass = createEClass(VALUE_CONSTANT);
    createEReference(value_constantEClass, VALUE_CONSTANT__VCNAME1);
    createEReference(value_constantEClass, VALUE_CONSTANT__VCNAME2);
    createEAttribute(value_constantEClass, VALUE_CONSTANT__VCNAME3);
    createEAttribute(value_constantEClass, VALUE_CONSTANT__VCNAME4);

    logical_operatorEClass = createEClass(LOGICAL_OPERATOR);
    createEAttribute(logical_operatorEClass, LOGICAL_OPERATOR__LONAME1);
    createEAttribute(logical_operatorEClass, LOGICAL_OPERATOR__LONAME2);
    createEAttribute(logical_operatorEClass, LOGICAL_OPERATOR__LONAME3);

    relational_operatorEClass = createEClass(RELATIONAL_OPERATOR);
    createEAttribute(relational_operatorEClass, RELATIONAL_OPERATOR__RONAME1);
    createEAttribute(relational_operatorEClass, RELATIONAL_OPERATOR__RONAME2);
    createEAttribute(relational_operatorEClass, RELATIONAL_OPERATOR__RONAME3);
    createEAttribute(relational_operatorEClass, RELATIONAL_OPERATOR__RONAME4);
    createEAttribute(relational_operatorEClass, RELATIONAL_OPERATOR__RONAME5);
    createEAttribute(relational_operatorEClass, RELATIONAL_OPERATOR__RONAME6);

    binary_adding_operatorEClass = createEClass(BINARY_ADDING_OPERATOR);
    createEAttribute(binary_adding_operatorEClass, BINARY_ADDING_OPERATOR__BAONAME1);
    createEAttribute(binary_adding_operatorEClass, BINARY_ADDING_OPERATOR__BAONAME2);

    unary_adding_operatorEClass = createEClass(UNARY_ADDING_OPERATOR);
    createEAttribute(unary_adding_operatorEClass, UNARY_ADDING_OPERATOR__UAONAME1);
    createEAttribute(unary_adding_operatorEClass, UNARY_ADDING_OPERATOR__UAONAME2);

    multiplying_operatorEClass = createEClass(MULTIPLYING_OPERATOR);
    createEAttribute(multiplying_operatorEClass, MULTIPLYING_OPERATOR__MONAME1);
    createEAttribute(multiplying_operatorEClass, MULTIPLYING_OPERATOR__MONAME2);
    createEAttribute(multiplying_operatorEClass, MULTIPLYING_OPERATOR__MONAME3);
    createEAttribute(multiplying_operatorEClass, MULTIPLYING_OPERATOR__MONAME4);

    binary_numeric_operatorEClass = createEClass(BINARY_NUMERIC_OPERATOR);
    createEAttribute(binary_numeric_operatorEClass, BINARY_NUMERIC_OPERATOR__BNONAME);

    unary_numeric_operatorEClass = createEClass(UNARY_NUMERIC_OPERATOR);
    createEAttribute(unary_numeric_operatorEClass, UNARY_NUMERIC_OPERATOR__UNONAME);

    unary_boolean_operatorEClass = createEClass(UNARY_BOOLEAN_OPERATOR);
    createEAttribute(unary_boolean_operatorEClass, UNARY_BOOLEAN_OPERATOR__UBONAME);

    boolean_literalEClass = createEClass(BOOLEAN_LITERAL);
    createEAttribute(boolean_literalEClass, BOOLEAN_LITERAL__BLNAME1);

    integer_valueEClass = createEClass(INTEGER_VALUE);
    createEReference(integer_valueEClass, INTEGER_VALUE__IVNAME);

    behavior_timeEClass = createEClass(BEHAVIOR_TIME);
    createEReference(behavior_timeEClass, BEHAVIOR_TIME__BTNAME1);
    createEReference(behavior_timeEClass, BEHAVIOR_TIME__BTNAME2);

    unit_identifierEClass = createEClass(UNIT_IDENTIFIER);
    createEAttribute(unit_identifierEClass, UNIT_IDENTIFIER__UINAME1);
    createEAttribute(unit_identifierEClass, UNIT_IDENTIFIER__UINAME2);
    createEAttribute(unit_identifierEClass, UNIT_IDENTIFIER__UINAME3);
    createEAttribute(unit_identifierEClass, UNIT_IDENTIFIER__UINAME4);
    createEAttribute(unit_identifierEClass, UNIT_IDENTIFIER__UINAME5);
    createEAttribute(unit_identifierEClass, UNIT_IDENTIFIER__UINAME6);
    createEAttribute(unit_identifierEClass, UNIT_IDENTIFIER__UINAME7);

    numeric_literalEClass = createEClass(NUMERIC_LITERAL);
    createEReference(numeric_literalEClass, NUMERIC_LITERAL__NLNAME1);
    createEReference(numeric_literalEClass, NUMERIC_LITERAL__NLNAME2);

    integer_literalEClass = createEClass(INTEGER_LITERAL);
    createEReference(integer_literalEClass, INTEGER_LITERAL__ILNAME1);
    createEReference(integer_literalEClass, INTEGER_LITERAL__ILNAME2);

    real_literalEClass = createEClass(REAL_LITERAL);
    createEReference(real_literalEClass, REAL_LITERAL__RLNAME);

    decimal_integer_literalEClass = createEClass(DECIMAL_INTEGER_LITERAL);
    createEReference(decimal_integer_literalEClass, DECIMAL_INTEGER_LITERAL__DILNAME1);
    createEReference(decimal_integer_literalEClass, DECIMAL_INTEGER_LITERAL__DILNAME2);

    decimal_real_literalEClass = createEClass(DECIMAL_REAL_LITERAL);
    createEReference(decimal_real_literalEClass, DECIMAL_REAL_LITERAL__DRLNAME1);
    createEReference(decimal_real_literalEClass, DECIMAL_REAL_LITERAL__DRLNAME2);
    createEReference(decimal_real_literalEClass, DECIMAL_REAL_LITERAL__DRLNAME3);

    numeralEClass = createEClass(NUMERAL);
    createEReference(numeralEClass, NUMERAL__NNAME1);
    createEReference(numeralEClass, NUMERAL__NNAME2);

    exponentEClass = createEClass(EXPONENT);
    createEReference(exponentEClass, EXPONENT__ENAME1);
    createEReference(exponentEClass, EXPONENT__ENAME2);

    positive_exponentEClass = createEClass(POSITIVE_EXPONENT);
    createEReference(positive_exponentEClass, POSITIVE_EXPONENT__PENAME);

    based_integer_literalEClass = createEClass(BASED_INTEGER_LITERAL);
    createEReference(based_integer_literalEClass, BASED_INTEGER_LITERAL__BILNAME1);
    createEReference(based_integer_literalEClass, BASED_INTEGER_LITERAL__BILNAME2);
    createEReference(based_integer_literalEClass, BASED_INTEGER_LITERAL__BILNAME3);

    baseEClass = createEClass(BASE);
    createEAttribute(baseEClass, BASE__BNAME1);
    createEAttribute(baseEClass, BASE__BNAME2);

    based_numeralEClass = createEClass(BASED_NUMERAL);
    createEAttribute(based_numeralEClass, BASED_NUMERAL__BNNAME1);
    createEAttribute(based_numeralEClass, BASED_NUMERAL__BNNAME2);

    integerEClass = createEClass(INTEGER);
    createEAttribute(integerEClass, INTEGER__VALUE1);
    createEAttribute(integerEClass, INTEGER__VALUE2);

    dispatch_conditionEClass = createEClass(DISPATCH_CONDITION);
    createEAttribute(dispatch_conditionEClass, DISPATCH_CONDITION__DCNAME3);
    createEReference(dispatch_conditionEClass, DISPATCH_CONDITION__DCNAME1);
    createEReference(dispatch_conditionEClass, DISPATCH_CONDITION__DCNAME2);

    dispatch_trigger_conditionEClass = createEClass(DISPATCH_TRIGGER_CONDITION);
    createEReference(dispatch_trigger_conditionEClass, DISPATCH_TRIGGER_CONDITION__DTCNAME1);
    createEAttribute(dispatch_trigger_conditionEClass, DISPATCH_TRIGGER_CONDITION__DTCNAME4);
    createEReference(dispatch_trigger_conditionEClass, DISPATCH_TRIGGER_CONDITION__DTCNAME2);

    dispatch_trigger_logical_expressionEClass = createEClass(DISPATCH_TRIGGER_LOGICAL_EXPRESSION);
    createEReference(dispatch_trigger_logical_expressionEClass, DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME1);
    createEReference(dispatch_trigger_logical_expressionEClass, DISPATCH_TRIGGER_LOGICAL_EXPRESSION__DTLENAME2);

    dispatch_conjunctionEClass = createEClass(DISPATCH_CONJUNCTION);
    createEReference(dispatch_conjunctionEClass, DISPATCH_CONJUNCTION__DCNAME1);
    createEReference(dispatch_conjunctionEClass, DISPATCH_CONJUNCTION__DCNAME2);

    completion_relative_timeout_condition_and_catchEClass = createEClass(COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH);
    createEAttribute(completion_relative_timeout_condition_and_catchEClass, COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME1);
    createEReference(completion_relative_timeout_condition_and_catchEClass, COMPLETION_RELATIVE_TIMEOUT_CONDITION_AND_CATCH__CDTCACNAME2);

    dispatch_triggerEClass = createEClass(DISPATCH_TRIGGER);
    createEAttribute(dispatch_triggerEClass, DISPATCH_TRIGGER__DTNAME);

    frozen_portsEClass = createEClass(FROZEN_PORTS);
    createEAttribute(frozen_portsEClass, FROZEN_PORTS__FPNAME1);
    createEAttribute(frozen_portsEClass, FROZEN_PORTS__FPNAME2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes

    // Initialize classes and features; add operations and parameters
    initEClass(guardEClass, Guard.class, "Guard", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getGuard_Dispatch(), this.getdispatch_condition(), null, "dispatch", null, 0, 1, Guard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getGuard_Execute(), this.getexecute_condition(), null, "execute", null, 0, 1, Guard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(execute_conditionEClass, execute_condition.class, "execute_condition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getexecute_condition_Ecname1(), ecorePackage.getEString(), "ecname1", null, 0, 1, execute_condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getexecute_condition_Ecname2(), this.getbehavior_action_block_timeout_catch(), null, "ecname2", null, 0, 1, execute_condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getexecute_condition_Ecname3(), this.getvalue_expression(), null, "ecname3", null, 0, 1, execute_condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(behavior_action_block_timeout_catchEClass, behavior_action_block_timeout_catch.class, "behavior_action_block_timeout_catch", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getbehavior_action_block_timeout_catch_Babname(), ecorePackage.getEString(), "babname", null, 0, -1, behavior_action_block_timeout_catch.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(value_expressionEClass, value_expression.class, "value_expression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getvalue_expression_Vename1(), this.getrelation(), null, "vename1", null, 0, 1, value_expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getvalue_expression_Vename3(), this.getlogical_operator(), null, "vename3", null, 0, -1, value_expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getvalue_expression_Vename2(), this.getrelation(), null, "vename2", null, 0, -1, value_expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(relationEClass, relation.class, "relation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getrelation_Rname1(), this.getsimple_expression(), null, "rname1", null, 0, 1, relation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getrelation_Rname3(), this.getrelational_operator(), null, "rname3", null, 0, -1, relation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getrelation_Rname2(), this.getsimple_expression(), null, "rname2", null, 0, -1, relation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(simple_expressionEClass, simple_expression.class, "simple_expression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getsimple_expression_Sename2(), this.getunary_adding_operator(), null, "sename2", null, 0, 1, simple_expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getsimple_expression_Sename1(), this.getterm(), null, "sename1", null, 0, 1, simple_expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getsimple_expression_Sename3(), this.getbinary_adding_operator(), null, "sename3", null, 0, -1, simple_expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getsimple_expression_Sename4(), this.getterm(), null, "sename4", null, 0, -1, simple_expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(termEClass, term.class, "term", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getterm_Tname1(), this.getfactor(), null, "tname1", null, 0, 1, term.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getterm_Tname3(), this.getmultiplying_operator(), null, "tname3", null, 0, -1, term.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getterm_Tname2(), this.getfactor(), null, "tname2", null, 0, -1, term.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(factorEClass, factor.class, "factor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getfactor_Fname1(), this.getvalue(), null, "fname1", null, 0, 1, factor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getfactor_Fname3(), this.getbinary_numeric_operator(), null, "fname3", null, 0, -1, factor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getfactor_Fname2(), this.getvalue(), null, "fname2", null, 0, 1, factor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getfactor_Fname4(), this.getunary_numeric_operator(), null, "fname4", null, 0, 1, factor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getfactor_Fname5(), this.getvalue(), null, "fname5", null, 0, 1, factor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getfactor_Fname6(), this.getunary_boolean_operator(), null, "fname6", null, 0, 1, factor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getfactor_Fname7(), this.getvalue(), null, "fname7", null, 0, 1, factor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(valueEClass, value.class, "value", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getvalue_Vname1(), this.getvalue_variable(), null, "vname1", null, 0, 1, value.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getvalue_Vname2(), this.getvalue_constant(), null, "vname2", null, 0, 1, value.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(value_variableEClass, value_variable.class, "value_variable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getvalue_variable_Vvname0(), ecorePackage.getEString(), "vvname0", null, 0, 1, value_variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getvalue_variable_Vvname1(), ecorePackage.getEString(), "vvname1", null, 0, 1, value_variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getvalue_variable_Vvname2(), ecorePackage.getEString(), "vvname2", null, 0, 1, value_variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getvalue_variable_Vvname3(), ecorePackage.getEString(), "vvname3", null, 0, 1, value_variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(value_constantEClass, value_constant.class, "value_constant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getvalue_constant_Vcname1(), this.getboolean_literal(), null, "vcname1", null, 0, 1, value_constant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getvalue_constant_Vcname2(), this.getnumeric_literal(), null, "vcname2", null, 0, 1, value_constant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getvalue_constant_Vcname3(), ecorePackage.getEString(), "vcname3", null, 0, 1, value_constant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getvalue_constant_Vcname4(), ecorePackage.getEString(), "vcname4", null, 0, 1, value_constant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(logical_operatorEClass, logical_operator.class, "logical_operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getlogical_operator_Loname1(), ecorePackage.getEString(), "loname1", null, 0, 1, logical_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getlogical_operator_Loname2(), ecorePackage.getEString(), "loname2", null, 0, 1, logical_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getlogical_operator_Loname3(), ecorePackage.getEString(), "loname3", null, 0, 1, logical_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(relational_operatorEClass, relational_operator.class, "relational_operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getrelational_operator_Roname1(), ecorePackage.getEString(), "roname1", null, 0, 1, relational_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getrelational_operator_Roname2(), ecorePackage.getEString(), "roname2", null, 0, 1, relational_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getrelational_operator_Roname3(), ecorePackage.getEString(), "roname3", null, 0, 1, relational_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getrelational_operator_Roname4(), ecorePackage.getEString(), "roname4", null, 0, 1, relational_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getrelational_operator_Roname5(), ecorePackage.getEString(), "roname5", null, 0, 1, relational_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getrelational_operator_Roname6(), ecorePackage.getEString(), "roname6", null, 0, 1, relational_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(binary_adding_operatorEClass, binary_adding_operator.class, "binary_adding_operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getbinary_adding_operator_Baoname1(), ecorePackage.getEString(), "baoname1", null, 0, 1, binary_adding_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbinary_adding_operator_Baoname2(), ecorePackage.getEString(), "baoname2", null, 0, 1, binary_adding_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(unary_adding_operatorEClass, unary_adding_operator.class, "unary_adding_operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getunary_adding_operator_Uaoname1(), ecorePackage.getEString(), "uaoname1", null, 0, 1, unary_adding_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getunary_adding_operator_Uaoname2(), ecorePackage.getEString(), "uaoname2", null, 0, 1, unary_adding_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(multiplying_operatorEClass, multiplying_operator.class, "multiplying_operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getmultiplying_operator_Moname1(), ecorePackage.getEString(), "moname1", null, 0, 1, multiplying_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getmultiplying_operator_Moname2(), ecorePackage.getEString(), "moname2", null, 0, 1, multiplying_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getmultiplying_operator_Moname3(), ecorePackage.getEString(), "moname3", null, 0, 1, multiplying_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getmultiplying_operator_Moname4(), ecorePackage.getEString(), "moname4", null, 0, 1, multiplying_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(binary_numeric_operatorEClass, binary_numeric_operator.class, "binary_numeric_operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getbinary_numeric_operator_Bnoname(), ecorePackage.getEString(), "bnoname", null, 0, 1, binary_numeric_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(unary_numeric_operatorEClass, unary_numeric_operator.class, "unary_numeric_operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getunary_numeric_operator_Unoname(), ecorePackage.getEString(), "unoname", null, 0, 1, unary_numeric_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(unary_boolean_operatorEClass, unary_boolean_operator.class, "unary_boolean_operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getunary_boolean_operator_Uboname(), ecorePackage.getEString(), "uboname", null, 0, 1, unary_boolean_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(boolean_literalEClass, boolean_literal.class, "boolean_literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getboolean_literal_Blname1(), ecorePackage.getEString(), "blname1", null, 0, 1, boolean_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integer_valueEClass, integer_value.class, "integer_value", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getinteger_value_Ivname(), this.getnumeric_literal(), null, "ivname", null, 0, 1, integer_value.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(behavior_timeEClass, behavior_time.class, "behavior_time", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getbehavior_time_Btname1(), this.getinteger_value(), null, "btname1", null, 0, 1, behavior_time.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_time_Btname2(), this.getunit_identifier(), null, "btname2", null, 0, 1, behavior_time.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(unit_identifierEClass, unit_identifier.class, "unit_identifier", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getunit_identifier_Uiname1(), ecorePackage.getEString(), "uiname1", null, 0, 1, unit_identifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getunit_identifier_Uiname2(), ecorePackage.getEString(), "uiname2", null, 0, 1, unit_identifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getunit_identifier_Uiname3(), ecorePackage.getEString(), "uiname3", null, 0, 1, unit_identifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getunit_identifier_Uiname4(), ecorePackage.getEString(), "uiname4", null, 0, 1, unit_identifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getunit_identifier_Uiname5(), ecorePackage.getEString(), "uiname5", null, 0, 1, unit_identifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getunit_identifier_Uiname6(), ecorePackage.getEString(), "uiname6", null, 0, 1, unit_identifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getunit_identifier_Uiname7(), ecorePackage.getEString(), "uiname7", null, 0, 1, unit_identifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(numeric_literalEClass, numeric_literal.class, "numeric_literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getnumeric_literal_Nlname1(), this.getinteger_literal(), null, "nlname1", null, 0, 1, numeric_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getnumeric_literal_Nlname2(), this.getreal_literal(), null, "nlname2", null, 0, 1, numeric_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integer_literalEClass, integer_literal.class, "integer_literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getinteger_literal_Ilname1(), this.getdecimal_integer_literal(), null, "ilname1", null, 0, 1, integer_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getinteger_literal_Ilname2(), this.getbased_integer_literal(), null, "ilname2", null, 0, 1, integer_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(real_literalEClass, real_literal.class, "real_literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getreal_literal_Rlname(), this.getdecimal_real_literal(), null, "rlname", null, 0, 1, real_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(decimal_integer_literalEClass, decimal_integer_literal.class, "decimal_integer_literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getdecimal_integer_literal_Dilname1(), this.getnumeral(), null, "dilname1", null, 0, 1, decimal_integer_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getdecimal_integer_literal_Dilname2(), this.getpositive_exponent(), null, "dilname2", null, 0, 1, decimal_integer_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(decimal_real_literalEClass, decimal_real_literal.class, "decimal_real_literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getdecimal_real_literal_Drlname1(), this.getnumeral(), null, "drlname1", null, 0, 1, decimal_real_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getdecimal_real_literal_Drlname2(), this.getnumeral(), null, "drlname2", null, 0, 1, decimal_real_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getdecimal_real_literal_Drlname3(), this.getexponent(), null, "drlname3", null, 0, 1, decimal_real_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(numeralEClass, numeral.class, "numeral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getnumeral_Nname1(), this.getinteger(), null, "nname1", null, 0, 1, numeral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getnumeral_Nname2(), this.getinteger(), null, "nname2", null, 0, -1, numeral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(exponentEClass, exponent.class, "exponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getexponent_Ename1(), this.getnumeral(), null, "ename1", null, 0, 1, exponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getexponent_Ename2(), this.getnumeral(), null, "ename2", null, 0, 1, exponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(positive_exponentEClass, positive_exponent.class, "positive_exponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getpositive_exponent_Pename(), this.getnumeral(), null, "pename", null, 0, 1, positive_exponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(based_integer_literalEClass, based_integer_literal.class, "based_integer_literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getbased_integer_literal_Bilname1(), this.getbase(), null, "bilname1", null, 0, 1, based_integer_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbased_integer_literal_Bilname2(), this.getbased_numeral(), null, "bilname2", null, 0, 1, based_integer_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbased_integer_literal_Bilname3(), this.getpositive_exponent(), null, "bilname3", null, 0, 1, based_integer_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(baseEClass, base.class, "base", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getbase_Bname1(), ecorePackage.getEString(), "bname1", null, 0, 1, base.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbase_Bname2(), ecorePackage.getEString(), "bname2", null, 0, 1, base.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(based_numeralEClass, based_numeral.class, "based_numeral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getbased_numeral_Bnname1(), ecorePackage.getEString(), "bnname1", null, 0, 1, based_numeral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbased_numeral_Bnname2(), ecorePackage.getEString(), "bnname2", null, 0, 1, based_numeral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integerEClass, integer.class, "integer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getinteger_Value1(), ecorePackage.getEInt(), "value1", null, 0, 1, integer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getinteger_Value2(), ecorePackage.getEString(), "value2", null, 0, 1, integer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(dispatch_conditionEClass, dispatch_condition.class, "dispatch_condition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getdispatch_condition_Dcname3(), ecorePackage.getEString(), "dcname3", null, 0, 1, dispatch_condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getdispatch_condition_Dcname1(), this.getdispatch_trigger_condition(), null, "dcname1", null, 0, 1, dispatch_condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getdispatch_condition_Dcname2(), this.getfrozen_ports(), null, "dcname2", null, 0, 1, dispatch_condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(dispatch_trigger_conditionEClass, dispatch_trigger_condition.class, "dispatch_trigger_condition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getdispatch_trigger_condition_Dtcname1(), this.getdispatch_trigger_logical_expression(), null, "dtcname1", null, 0, 1, dispatch_trigger_condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getdispatch_trigger_condition_Dtcname4(), ecorePackage.getEString(), "dtcname4", null, 0, 1, dispatch_trigger_condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getdispatch_trigger_condition_Dtcname2(), this.getcompletion_relative_timeout_condition_and_catch(), null, "dtcname2", null, 0, 1, dispatch_trigger_condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(dispatch_trigger_logical_expressionEClass, dispatch_trigger_logical_expression.class, "dispatch_trigger_logical_expression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getdispatch_trigger_logical_expression_Dtlename1(), this.getdispatch_conjunction(), null, "dtlename1", null, 0, 1, dispatch_trigger_logical_expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getdispatch_trigger_logical_expression_Dtlename2(), this.getdispatch_conjunction(), null, "dtlename2", null, 0, -1, dispatch_trigger_logical_expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(dispatch_conjunctionEClass, dispatch_conjunction.class, "dispatch_conjunction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getdispatch_conjunction_Dcname1(), this.getdispatch_trigger(), null, "dcname1", null, 0, 1, dispatch_conjunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getdispatch_conjunction_Dcname2(), this.getdispatch_trigger(), null, "dcname2", null, 0, -1, dispatch_conjunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(completion_relative_timeout_condition_and_catchEClass, completion_relative_timeout_condition_and_catch.class, "completion_relative_timeout_condition_and_catch", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getcompletion_relative_timeout_condition_and_catch_Cdtcacname1(), ecorePackage.getEString(), "cdtcacname1", null, 0, 1, completion_relative_timeout_condition_and_catch.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getcompletion_relative_timeout_condition_and_catch_Cdtcacname2(), this.getbehavior_time(), null, "cdtcacname2", null, 0, 1, completion_relative_timeout_condition_and_catch.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(dispatch_triggerEClass, dispatch_trigger.class, "dispatch_trigger", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getdispatch_trigger_Dtname(), ecorePackage.getEString(), "dtname", null, 0, 1, dispatch_trigger.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(frozen_portsEClass, frozen_ports.class, "frozen_ports", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getfrozen_ports_Fpname1(), ecorePackage.getEString(), "fpname1", null, 0, 1, frozen_ports.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getfrozen_ports_Fpname2(), ecorePackage.getEString(), "fpname2", null, 0, -1, frozen_ports.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);
  }

} //BaGuardsPackageImpl
