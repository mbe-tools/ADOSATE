/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dispatch trigger logical expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_logical_expression#getDtlename1 <em>Dtlename1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_logical_expression#getDtlename2 <em>Dtlename2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdispatch_trigger_logical_expression()
 * @model
 * @generated
 */
public interface dispatch_trigger_logical_expression extends EObject
{
  /**
   * Returns the value of the '<em><b>Dtlename1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dtlename1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dtlename1</em>' containment reference.
   * @see #setDtlename1(dispatch_conjunction)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdispatch_trigger_logical_expression_Dtlename1()
   * @model containment="true"
   * @generated
   */
  dispatch_conjunction getDtlename1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.dispatch_trigger_logical_expression#getDtlename1 <em>Dtlename1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dtlename1</em>' containment reference.
   * @see #getDtlename1()
   * @generated
   */
  void setDtlename1(dispatch_conjunction value);

  /**
   * Returns the value of the '<em><b>Dtlename2</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baguards.baGuards.dispatch_conjunction}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dtlename2</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dtlename2</em>' containment reference list.
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdispatch_trigger_logical_expression_Dtlename2()
   * @model containment="true"
   * @generated
   */
  EList<dispatch_conjunction> getDtlename2();

} // dispatch_trigger_logical_expression
