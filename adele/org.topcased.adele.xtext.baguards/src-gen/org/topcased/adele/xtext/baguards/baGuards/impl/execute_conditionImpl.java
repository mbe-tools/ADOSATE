/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.behavior_action_block_timeout_catch;
import org.topcased.adele.xtext.baguards.baGuards.execute_condition;
import org.topcased.adele.xtext.baguards.baGuards.value_expression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>execute condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.execute_conditionImpl#getEcname1 <em>Ecname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.execute_conditionImpl#getEcname2 <em>Ecname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.execute_conditionImpl#getEcname3 <em>Ecname3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class execute_conditionImpl extends MinimalEObjectImpl.Container implements execute_condition
{
  /**
   * The default value of the '{@link #getEcname1() <em>Ecname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEcname1()
   * @generated
   * @ordered
   */
  protected static final String ECNAME1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getEcname1() <em>Ecname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEcname1()
   * @generated
   * @ordered
   */
  protected String ecname1 = ECNAME1_EDEFAULT;

  /**
   * The cached value of the '{@link #getEcname2() <em>Ecname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEcname2()
   * @generated
   * @ordered
   */
  protected behavior_action_block_timeout_catch ecname2;

  /**
   * The cached value of the '{@link #getEcname3() <em>Ecname3</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEcname3()
   * @generated
   * @ordered
   */
  protected value_expression ecname3;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected execute_conditionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.EXECUTE_CONDITION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getEcname1()
  {
    return ecname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEcname1(String newEcname1)
  {
    String oldEcname1 = ecname1;
    ecname1 = newEcname1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.EXECUTE_CONDITION__ECNAME1, oldEcname1, ecname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_action_block_timeout_catch getEcname2()
  {
    return ecname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetEcname2(behavior_action_block_timeout_catch newEcname2, NotificationChain msgs)
  {
    behavior_action_block_timeout_catch oldEcname2 = ecname2;
    ecname2 = newEcname2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.EXECUTE_CONDITION__ECNAME2, oldEcname2, newEcname2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEcname2(behavior_action_block_timeout_catch newEcname2)
  {
    if (newEcname2 != ecname2)
    {
      NotificationChain msgs = null;
      if (ecname2 != null)
        msgs = ((InternalEObject)ecname2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.EXECUTE_CONDITION__ECNAME2, null, msgs);
      if (newEcname2 != null)
        msgs = ((InternalEObject)newEcname2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.EXECUTE_CONDITION__ECNAME2, null, msgs);
      msgs = basicSetEcname2(newEcname2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.EXECUTE_CONDITION__ECNAME2, newEcname2, newEcname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value_expression getEcname3()
  {
    return ecname3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetEcname3(value_expression newEcname3, NotificationChain msgs)
  {
    value_expression oldEcname3 = ecname3;
    ecname3 = newEcname3;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.EXECUTE_CONDITION__ECNAME3, oldEcname3, newEcname3);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEcname3(value_expression newEcname3)
  {
    if (newEcname3 != ecname3)
    {
      NotificationChain msgs = null;
      if (ecname3 != null)
        msgs = ((InternalEObject)ecname3).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.EXECUTE_CONDITION__ECNAME3, null, msgs);
      if (newEcname3 != null)
        msgs = ((InternalEObject)newEcname3).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.EXECUTE_CONDITION__ECNAME3, null, msgs);
      msgs = basicSetEcname3(newEcname3, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.EXECUTE_CONDITION__ECNAME3, newEcname3, newEcname3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.EXECUTE_CONDITION__ECNAME2:
        return basicSetEcname2(null, msgs);
      case BaGuardsPackage.EXECUTE_CONDITION__ECNAME3:
        return basicSetEcname3(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.EXECUTE_CONDITION__ECNAME1:
        return getEcname1();
      case BaGuardsPackage.EXECUTE_CONDITION__ECNAME2:
        return getEcname2();
      case BaGuardsPackage.EXECUTE_CONDITION__ECNAME3:
        return getEcname3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.EXECUTE_CONDITION__ECNAME1:
        setEcname1((String)newValue);
        return;
      case BaGuardsPackage.EXECUTE_CONDITION__ECNAME2:
        setEcname2((behavior_action_block_timeout_catch)newValue);
        return;
      case BaGuardsPackage.EXECUTE_CONDITION__ECNAME3:
        setEcname3((value_expression)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.EXECUTE_CONDITION__ECNAME1:
        setEcname1(ECNAME1_EDEFAULT);
        return;
      case BaGuardsPackage.EXECUTE_CONDITION__ECNAME2:
        setEcname2((behavior_action_block_timeout_catch)null);
        return;
      case BaGuardsPackage.EXECUTE_CONDITION__ECNAME3:
        setEcname3((value_expression)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.EXECUTE_CONDITION__ECNAME1:
        return ECNAME1_EDEFAULT == null ? ecname1 != null : !ECNAME1_EDEFAULT.equals(ecname1);
      case BaGuardsPackage.EXECUTE_CONDITION__ECNAME2:
        return ecname2 != null;
      case BaGuardsPackage.EXECUTE_CONDITION__ECNAME3:
        return ecname3 != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (ecname1: ");
    result.append(ecname1);
    result.append(')');
    return result.toString();
  }

} //execute_conditionImpl
