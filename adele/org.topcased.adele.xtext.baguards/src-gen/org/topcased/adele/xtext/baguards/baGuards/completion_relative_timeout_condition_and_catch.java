/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>completion relative timeout condition and catch</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.completion_relative_timeout_condition_and_catch#getCdtcacname1 <em>Cdtcacname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.completion_relative_timeout_condition_and_catch#getCdtcacname2 <em>Cdtcacname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getcompletion_relative_timeout_condition_and_catch()
 * @model
 * @generated
 */
public interface completion_relative_timeout_condition_and_catch extends EObject
{
  /**
   * Returns the value of the '<em><b>Cdtcacname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cdtcacname1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Cdtcacname1</em>' attribute.
   * @see #setCdtcacname1(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getcompletion_relative_timeout_condition_and_catch_Cdtcacname1()
   * @model
   * @generated
   */
  String getCdtcacname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.completion_relative_timeout_condition_and_catch#getCdtcacname1 <em>Cdtcacname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Cdtcacname1</em>' attribute.
   * @see #getCdtcacname1()
   * @generated
   */
  void setCdtcacname1(String value);

  /**
   * Returns the value of the '<em><b>Cdtcacname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cdtcacname2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Cdtcacname2</em>' containment reference.
   * @see #setCdtcacname2(behavior_time)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getcompletion_relative_timeout_condition_and_catch_Cdtcacname2()
   * @model containment="true"
   * @generated
   */
  behavior_time getCdtcacname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.completion_relative_timeout_condition_and_catch#getCdtcacname2 <em>Cdtcacname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Cdtcacname2</em>' containment reference.
   * @see #getCdtcacname2()
   * @generated
   */
  void setCdtcacname2(behavior_time value);

} // completion_relative_timeout_condition_and_catch
