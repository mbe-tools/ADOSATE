/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.unary_boolean_operator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>unary boolean operator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.unary_boolean_operatorImpl#getUboname <em>Uboname</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class unary_boolean_operatorImpl extends MinimalEObjectImpl.Container implements unary_boolean_operator
{
  /**
   * The default value of the '{@link #getUboname() <em>Uboname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUboname()
   * @generated
   * @ordered
   */
  protected static final String UBONAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUboname() <em>Uboname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUboname()
   * @generated
   * @ordered
   */
  protected String uboname = UBONAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected unary_boolean_operatorImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.UNARY_BOOLEAN_OPERATOR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getUboname()
  {
    return uboname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUboname(String newUboname)
  {
    String oldUboname = uboname;
    uboname = newUboname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.UNARY_BOOLEAN_OPERATOR__UBONAME, oldUboname, uboname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.UNARY_BOOLEAN_OPERATOR__UBONAME:
        return getUboname();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.UNARY_BOOLEAN_OPERATOR__UBONAME:
        setUboname((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.UNARY_BOOLEAN_OPERATOR__UBONAME:
        setUboname(UBONAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.UNARY_BOOLEAN_OPERATOR__UBONAME:
        return UBONAME_EDEFAULT == null ? uboname != null : !UBONAME_EDEFAULT.equals(uboname);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (uboname: ");
    result.append(uboname);
    result.append(')');
    return result.toString();
  }

} //unary_boolean_operatorImpl
