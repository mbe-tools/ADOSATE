/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>positive exponent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.positive_exponent#getPename <em>Pename</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getpositive_exponent()
 * @model
 * @generated
 */
public interface positive_exponent extends EObject
{
  /**
   * Returns the value of the '<em><b>Pename</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Pename</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pename</em>' containment reference.
   * @see #setPename(numeral)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getpositive_exponent_Pename()
   * @model containment="true"
   * @generated
   */
  numeral getPename();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.positive_exponent#getPename <em>Pename</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Pename</em>' containment reference.
   * @see #getPename()
   * @generated
   */
  void setPename(numeral value);

} // positive_exponent
