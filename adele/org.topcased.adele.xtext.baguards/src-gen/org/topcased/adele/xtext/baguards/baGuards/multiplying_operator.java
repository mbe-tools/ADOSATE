/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>multiplying operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.multiplying_operator#getMoname1 <em>Moname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.multiplying_operator#getMoname2 <em>Moname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.multiplying_operator#getMoname3 <em>Moname3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.multiplying_operator#getMoname4 <em>Moname4</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getmultiplying_operator()
 * @model
 * @generated
 */
public interface multiplying_operator extends EObject
{
  /**
   * Returns the value of the '<em><b>Moname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Moname1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Moname1</em>' attribute.
   * @see #setMoname1(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getmultiplying_operator_Moname1()
   * @model
   * @generated
   */
  String getMoname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.multiplying_operator#getMoname1 <em>Moname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Moname1</em>' attribute.
   * @see #getMoname1()
   * @generated
   */
  void setMoname1(String value);

  /**
   * Returns the value of the '<em><b>Moname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Moname2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Moname2</em>' attribute.
   * @see #setMoname2(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getmultiplying_operator_Moname2()
   * @model
   * @generated
   */
  String getMoname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.multiplying_operator#getMoname2 <em>Moname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Moname2</em>' attribute.
   * @see #getMoname2()
   * @generated
   */
  void setMoname2(String value);

  /**
   * Returns the value of the '<em><b>Moname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Moname3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Moname3</em>' attribute.
   * @see #setMoname3(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getmultiplying_operator_Moname3()
   * @model
   * @generated
   */
  String getMoname3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.multiplying_operator#getMoname3 <em>Moname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Moname3</em>' attribute.
   * @see #getMoname3()
   * @generated
   */
  void setMoname3(String value);

  /**
   * Returns the value of the '<em><b>Moname4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Moname4</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Moname4</em>' attribute.
   * @see #setMoname4(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getmultiplying_operator_Moname4()
   * @model
   * @generated
   */
  String getMoname4();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.multiplying_operator#getMoname4 <em>Moname4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Moname4</em>' attribute.
   * @see #getMoname4()
   * @generated
   */
  void setMoname4(String value);

} // multiplying_operator
