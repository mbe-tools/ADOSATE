/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Guard</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.Guard#getDispatch <em>Dispatch</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.Guard#getExecute <em>Execute</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getGuard()
 * @model
 * @generated
 */
public interface Guard extends EObject
{
  /**
   * Returns the value of the '<em><b>Dispatch</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dispatch</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dispatch</em>' containment reference.
   * @see #setDispatch(dispatch_condition)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getGuard_Dispatch()
   * @model containment="true"
   * @generated
   */
  dispatch_condition getDispatch();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.Guard#getDispatch <em>Dispatch</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dispatch</em>' containment reference.
   * @see #getDispatch()
   * @generated
   */
  void setDispatch(dispatch_condition value);

  /**
   * Returns the value of the '<em><b>Execute</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Execute</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Execute</em>' containment reference.
   * @see #setExecute(execute_condition)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getGuard_Execute()
   * @model containment="true"
   * @generated
   */
  execute_condition getExecute();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.Guard#getExecute <em>Execute</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Execute</em>' containment reference.
   * @see #getExecute()
   * @generated
   */
  void setExecute(execute_condition value);

} // Guard
