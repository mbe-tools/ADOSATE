/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>relational operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname1 <em>Roname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname2 <em>Roname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname3 <em>Roname3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname4 <em>Roname4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname5 <em>Roname5</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname6 <em>Roname6</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getrelational_operator()
 * @model
 * @generated
 */
public interface relational_operator extends EObject
{
  /**
   * Returns the value of the '<em><b>Roname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Roname1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Roname1</em>' attribute.
   * @see #setRoname1(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getrelational_operator_Roname1()
   * @model
   * @generated
   */
  String getRoname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname1 <em>Roname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Roname1</em>' attribute.
   * @see #getRoname1()
   * @generated
   */
  void setRoname1(String value);

  /**
   * Returns the value of the '<em><b>Roname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Roname2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Roname2</em>' attribute.
   * @see #setRoname2(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getrelational_operator_Roname2()
   * @model
   * @generated
   */
  String getRoname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname2 <em>Roname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Roname2</em>' attribute.
   * @see #getRoname2()
   * @generated
   */
  void setRoname2(String value);

  /**
   * Returns the value of the '<em><b>Roname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Roname3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Roname3</em>' attribute.
   * @see #setRoname3(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getrelational_operator_Roname3()
   * @model
   * @generated
   */
  String getRoname3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname3 <em>Roname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Roname3</em>' attribute.
   * @see #getRoname3()
   * @generated
   */
  void setRoname3(String value);

  /**
   * Returns the value of the '<em><b>Roname4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Roname4</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Roname4</em>' attribute.
   * @see #setRoname4(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getrelational_operator_Roname4()
   * @model
   * @generated
   */
  String getRoname4();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname4 <em>Roname4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Roname4</em>' attribute.
   * @see #getRoname4()
   * @generated
   */
  void setRoname4(String value);

  /**
   * Returns the value of the '<em><b>Roname5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Roname5</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Roname5</em>' attribute.
   * @see #setRoname5(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getrelational_operator_Roname5()
   * @model
   * @generated
   */
  String getRoname5();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname5 <em>Roname5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Roname5</em>' attribute.
   * @see #getRoname5()
   * @generated
   */
  void setRoname5(String value);

  /**
   * Returns the value of the '<em><b>Roname6</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Roname6</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Roname6</em>' attribute.
   * @see #setRoname6(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getrelational_operator_Roname6()
   * @model
   * @generated
   */
  String getRoname6();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.relational_operator#getRoname6 <em>Roname6</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Roname6</em>' attribute.
   * @see #getRoname6()
   * @generated
   */
  void setRoname6(String value);

} // relational_operator
