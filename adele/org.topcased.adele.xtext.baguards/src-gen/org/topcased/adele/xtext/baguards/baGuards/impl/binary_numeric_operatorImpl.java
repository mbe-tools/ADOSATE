/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.binary_numeric_operator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>binary numeric operator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.binary_numeric_operatorImpl#getBnoname <em>Bnoname</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class binary_numeric_operatorImpl extends MinimalEObjectImpl.Container implements binary_numeric_operator
{
  /**
   * The default value of the '{@link #getBnoname() <em>Bnoname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBnoname()
   * @generated
   * @ordered
   */
  protected static final String BNONAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBnoname() <em>Bnoname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBnoname()
   * @generated
   * @ordered
   */
  protected String bnoname = BNONAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected binary_numeric_operatorImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.BINARY_NUMERIC_OPERATOR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBnoname()
  {
    return bnoname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBnoname(String newBnoname)
  {
    String oldBnoname = bnoname;
    bnoname = newBnoname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.BINARY_NUMERIC_OPERATOR__BNONAME, oldBnoname, bnoname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.BINARY_NUMERIC_OPERATOR__BNONAME:
        return getBnoname();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.BINARY_NUMERIC_OPERATOR__BNONAME:
        setBnoname((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.BINARY_NUMERIC_OPERATOR__BNONAME:
        setBnoname(BNONAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.BINARY_NUMERIC_OPERATOR__BNONAME:
        return BNONAME_EDEFAULT == null ? bnoname != null : !BNONAME_EDEFAULT.equals(bnoname);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (bnoname: ");
    result.append(bnoname);
    result.append(')');
    return result.toString();
  }

} //binary_numeric_operatorImpl
