/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>frozen ports</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.frozen_ports#getFpname1 <em>Fpname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.frozen_ports#getFpname2 <em>Fpname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getfrozen_ports()
 * @model
 * @generated
 */
public interface frozen_ports extends EObject
{
  /**
   * Returns the value of the '<em><b>Fpname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fpname1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fpname1</em>' attribute.
   * @see #setFpname1(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getfrozen_ports_Fpname1()
   * @model
   * @generated
   */
  String getFpname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.frozen_ports#getFpname1 <em>Fpname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Fpname1</em>' attribute.
   * @see #getFpname1()
   * @generated
   */
  void setFpname1(String value);

  /**
   * Returns the value of the '<em><b>Fpname2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fpname2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fpname2</em>' attribute list.
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getfrozen_ports_Fpname2()
   * @model unique="false"
   * @generated
   */
  EList<String> getFpname2();

} // frozen_ports
