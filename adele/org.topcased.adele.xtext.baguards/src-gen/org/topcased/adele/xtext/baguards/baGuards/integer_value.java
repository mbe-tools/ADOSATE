/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>integer value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.integer_value#getIvname <em>Ivname</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getinteger_value()
 * @model
 * @generated
 */
public interface integer_value extends EObject
{
  /**
   * Returns the value of the '<em><b>Ivname</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ivname</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ivname</em>' containment reference.
   * @see #setIvname(numeric_literal)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getinteger_value_Ivname()
   * @model containment="true"
   * @generated
   */
  numeric_literal getIvname();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.integer_value#getIvname <em>Ivname</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ivname</em>' containment reference.
   * @see #getIvname()
   * @generated
   */
  void setIvname(numeric_literal value);

} // integer_value
