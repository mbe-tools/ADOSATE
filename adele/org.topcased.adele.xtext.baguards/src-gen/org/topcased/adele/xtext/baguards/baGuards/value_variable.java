/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>value variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.value_variable#getVvname0 <em>Vvname0</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.value_variable#getVvname1 <em>Vvname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.value_variable#getVvname2 <em>Vvname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.value_variable#getVvname3 <em>Vvname3</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getvalue_variable()
 * @model
 * @generated
 */
public interface value_variable extends EObject
{
  /**
   * Returns the value of the '<em><b>Vvname0</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Vvname0</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Vvname0</em>' attribute.
   * @see #setVvname0(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getvalue_variable_Vvname0()
   * @model
   * @generated
   */
  String getVvname0();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.value_variable#getVvname0 <em>Vvname0</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Vvname0</em>' attribute.
   * @see #getVvname0()
   * @generated
   */
  void setVvname0(String value);

  /**
   * Returns the value of the '<em><b>Vvname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Vvname1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Vvname1</em>' attribute.
   * @see #setVvname1(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getvalue_variable_Vvname1()
   * @model
   * @generated
   */
  String getVvname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.value_variable#getVvname1 <em>Vvname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Vvname1</em>' attribute.
   * @see #getVvname1()
   * @generated
   */
  void setVvname1(String value);

  /**
   * Returns the value of the '<em><b>Vvname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Vvname2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Vvname2</em>' attribute.
   * @see #setVvname2(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getvalue_variable_Vvname2()
   * @model
   * @generated
   */
  String getVvname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.value_variable#getVvname2 <em>Vvname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Vvname2</em>' attribute.
   * @see #getVvname2()
   * @generated
   */
  void setVvname2(String value);

  /**
   * Returns the value of the '<em><b>Vvname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Vvname3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Vvname3</em>' attribute.
   * @see #setVvname3(String)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getvalue_variable_Vvname3()
   * @model
   * @generated
   */
  String getVvname3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.value_variable#getVvname3 <em>Vvname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Vvname3</em>' attribute.
   * @see #getVvname3()
   * @generated
   */
  void setVvname3(String value);

} // value_variable
