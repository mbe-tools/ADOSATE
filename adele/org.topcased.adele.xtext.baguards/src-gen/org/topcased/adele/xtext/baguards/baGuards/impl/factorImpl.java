/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage;
import org.topcased.adele.xtext.baguards.baGuards.binary_numeric_operator;
import org.topcased.adele.xtext.baguards.baGuards.factor;
import org.topcased.adele.xtext.baguards.baGuards.unary_boolean_operator;
import org.topcased.adele.xtext.baguards.baGuards.unary_numeric_operator;
import org.topcased.adele.xtext.baguards.baGuards.value;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>factor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.factorImpl#getFname1 <em>Fname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.factorImpl#getFname3 <em>Fname3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.factorImpl#getFname2 <em>Fname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.factorImpl#getFname4 <em>Fname4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.factorImpl#getFname5 <em>Fname5</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.factorImpl#getFname6 <em>Fname6</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.impl.factorImpl#getFname7 <em>Fname7</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class factorImpl extends MinimalEObjectImpl.Container implements factor
{
  /**
   * The cached value of the '{@link #getFname1() <em>Fname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFname1()
   * @generated
   * @ordered
   */
  protected value fname1;

  /**
   * The cached value of the '{@link #getFname3() <em>Fname3</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFname3()
   * @generated
   * @ordered
   */
  protected EList<binary_numeric_operator> fname3;

  /**
   * The cached value of the '{@link #getFname2() <em>Fname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFname2()
   * @generated
   * @ordered
   */
  protected value fname2;

  /**
   * The cached value of the '{@link #getFname4() <em>Fname4</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFname4()
   * @generated
   * @ordered
   */
  protected unary_numeric_operator fname4;

  /**
   * The cached value of the '{@link #getFname5() <em>Fname5</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFname5()
   * @generated
   * @ordered
   */
  protected value fname5;

  /**
   * The cached value of the '{@link #getFname6() <em>Fname6</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFname6()
   * @generated
   * @ordered
   */
  protected unary_boolean_operator fname6;

  /**
   * The cached value of the '{@link #getFname7() <em>Fname7</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFname7()
   * @generated
   * @ordered
   */
  protected value fname7;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected factorImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaGuardsPackage.Literals.FACTOR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value getFname1()
  {
    return fname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFname1(value newFname1, NotificationChain msgs)
  {
    value oldFname1 = fname1;
    fname1 = newFname1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.FACTOR__FNAME1, oldFname1, newFname1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFname1(value newFname1)
  {
    if (newFname1 != fname1)
    {
      NotificationChain msgs = null;
      if (fname1 != null)
        msgs = ((InternalEObject)fname1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.FACTOR__FNAME1, null, msgs);
      if (newFname1 != null)
        msgs = ((InternalEObject)newFname1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.FACTOR__FNAME1, null, msgs);
      msgs = basicSetFname1(newFname1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.FACTOR__FNAME1, newFname1, newFname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<binary_numeric_operator> getFname3()
  {
    if (fname3 == null)
    {
      fname3 = new EObjectContainmentEList<binary_numeric_operator>(binary_numeric_operator.class, this, BaGuardsPackage.FACTOR__FNAME3);
    }
    return fname3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value getFname2()
  {
    return fname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFname2(value newFname2, NotificationChain msgs)
  {
    value oldFname2 = fname2;
    fname2 = newFname2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.FACTOR__FNAME2, oldFname2, newFname2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFname2(value newFname2)
  {
    if (newFname2 != fname2)
    {
      NotificationChain msgs = null;
      if (fname2 != null)
        msgs = ((InternalEObject)fname2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.FACTOR__FNAME2, null, msgs);
      if (newFname2 != null)
        msgs = ((InternalEObject)newFname2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.FACTOR__FNAME2, null, msgs);
      msgs = basicSetFname2(newFname2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.FACTOR__FNAME2, newFname2, newFname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public unary_numeric_operator getFname4()
  {
    return fname4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFname4(unary_numeric_operator newFname4, NotificationChain msgs)
  {
    unary_numeric_operator oldFname4 = fname4;
    fname4 = newFname4;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.FACTOR__FNAME4, oldFname4, newFname4);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFname4(unary_numeric_operator newFname4)
  {
    if (newFname4 != fname4)
    {
      NotificationChain msgs = null;
      if (fname4 != null)
        msgs = ((InternalEObject)fname4).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.FACTOR__FNAME4, null, msgs);
      if (newFname4 != null)
        msgs = ((InternalEObject)newFname4).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.FACTOR__FNAME4, null, msgs);
      msgs = basicSetFname4(newFname4, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.FACTOR__FNAME4, newFname4, newFname4));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value getFname5()
  {
    return fname5;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFname5(value newFname5, NotificationChain msgs)
  {
    value oldFname5 = fname5;
    fname5 = newFname5;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.FACTOR__FNAME5, oldFname5, newFname5);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFname5(value newFname5)
  {
    if (newFname5 != fname5)
    {
      NotificationChain msgs = null;
      if (fname5 != null)
        msgs = ((InternalEObject)fname5).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.FACTOR__FNAME5, null, msgs);
      if (newFname5 != null)
        msgs = ((InternalEObject)newFname5).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.FACTOR__FNAME5, null, msgs);
      msgs = basicSetFname5(newFname5, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.FACTOR__FNAME5, newFname5, newFname5));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public unary_boolean_operator getFname6()
  {
    return fname6;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFname6(unary_boolean_operator newFname6, NotificationChain msgs)
  {
    unary_boolean_operator oldFname6 = fname6;
    fname6 = newFname6;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.FACTOR__FNAME6, oldFname6, newFname6);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFname6(unary_boolean_operator newFname6)
  {
    if (newFname6 != fname6)
    {
      NotificationChain msgs = null;
      if (fname6 != null)
        msgs = ((InternalEObject)fname6).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.FACTOR__FNAME6, null, msgs);
      if (newFname6 != null)
        msgs = ((InternalEObject)newFname6).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.FACTOR__FNAME6, null, msgs);
      msgs = basicSetFname6(newFname6, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.FACTOR__FNAME6, newFname6, newFname6));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value getFname7()
  {
    return fname7;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFname7(value newFname7, NotificationChain msgs)
  {
    value oldFname7 = fname7;
    fname7 = newFname7;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaGuardsPackage.FACTOR__FNAME7, oldFname7, newFname7);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFname7(value newFname7)
  {
    if (newFname7 != fname7)
    {
      NotificationChain msgs = null;
      if (fname7 != null)
        msgs = ((InternalEObject)fname7).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.FACTOR__FNAME7, null, msgs);
      if (newFname7 != null)
        msgs = ((InternalEObject)newFname7).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaGuardsPackage.FACTOR__FNAME7, null, msgs);
      msgs = basicSetFname7(newFname7, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaGuardsPackage.FACTOR__FNAME7, newFname7, newFname7));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaGuardsPackage.FACTOR__FNAME1:
        return basicSetFname1(null, msgs);
      case BaGuardsPackage.FACTOR__FNAME3:
        return ((InternalEList<?>)getFname3()).basicRemove(otherEnd, msgs);
      case BaGuardsPackage.FACTOR__FNAME2:
        return basicSetFname2(null, msgs);
      case BaGuardsPackage.FACTOR__FNAME4:
        return basicSetFname4(null, msgs);
      case BaGuardsPackage.FACTOR__FNAME5:
        return basicSetFname5(null, msgs);
      case BaGuardsPackage.FACTOR__FNAME6:
        return basicSetFname6(null, msgs);
      case BaGuardsPackage.FACTOR__FNAME7:
        return basicSetFname7(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaGuardsPackage.FACTOR__FNAME1:
        return getFname1();
      case BaGuardsPackage.FACTOR__FNAME3:
        return getFname3();
      case BaGuardsPackage.FACTOR__FNAME2:
        return getFname2();
      case BaGuardsPackage.FACTOR__FNAME4:
        return getFname4();
      case BaGuardsPackage.FACTOR__FNAME5:
        return getFname5();
      case BaGuardsPackage.FACTOR__FNAME6:
        return getFname6();
      case BaGuardsPackage.FACTOR__FNAME7:
        return getFname7();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaGuardsPackage.FACTOR__FNAME1:
        setFname1((value)newValue);
        return;
      case BaGuardsPackage.FACTOR__FNAME3:
        getFname3().clear();
        getFname3().addAll((Collection<? extends binary_numeric_operator>)newValue);
        return;
      case BaGuardsPackage.FACTOR__FNAME2:
        setFname2((value)newValue);
        return;
      case BaGuardsPackage.FACTOR__FNAME4:
        setFname4((unary_numeric_operator)newValue);
        return;
      case BaGuardsPackage.FACTOR__FNAME5:
        setFname5((value)newValue);
        return;
      case BaGuardsPackage.FACTOR__FNAME6:
        setFname6((unary_boolean_operator)newValue);
        return;
      case BaGuardsPackage.FACTOR__FNAME7:
        setFname7((value)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.FACTOR__FNAME1:
        setFname1((value)null);
        return;
      case BaGuardsPackage.FACTOR__FNAME3:
        getFname3().clear();
        return;
      case BaGuardsPackage.FACTOR__FNAME2:
        setFname2((value)null);
        return;
      case BaGuardsPackage.FACTOR__FNAME4:
        setFname4((unary_numeric_operator)null);
        return;
      case BaGuardsPackage.FACTOR__FNAME5:
        setFname5((value)null);
        return;
      case BaGuardsPackage.FACTOR__FNAME6:
        setFname6((unary_boolean_operator)null);
        return;
      case BaGuardsPackage.FACTOR__FNAME7:
        setFname7((value)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaGuardsPackage.FACTOR__FNAME1:
        return fname1 != null;
      case BaGuardsPackage.FACTOR__FNAME3:
        return fname3 != null && !fname3.isEmpty();
      case BaGuardsPackage.FACTOR__FNAME2:
        return fname2 != null;
      case BaGuardsPackage.FACTOR__FNAME4:
        return fname4 != null;
      case BaGuardsPackage.FACTOR__FNAME5:
        return fname5 != null;
      case BaGuardsPackage.FACTOR__FNAME6:
        return fname6 != null;
      case BaGuardsPackage.FACTOR__FNAME7:
        return fname7 != null;
    }
    return super.eIsSet(featureID);
  }

} //factorImpl
