/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baguards.baGuards;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>decimal integer literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.decimal_integer_literal#getDilname1 <em>Dilname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baguards.baGuards.decimal_integer_literal#getDilname2 <em>Dilname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdecimal_integer_literal()
 * @model
 * @generated
 */
public interface decimal_integer_literal extends EObject
{
  /**
   * Returns the value of the '<em><b>Dilname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dilname1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dilname1</em>' containment reference.
   * @see #setDilname1(numeral)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdecimal_integer_literal_Dilname1()
   * @model containment="true"
   * @generated
   */
  numeral getDilname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.decimal_integer_literal#getDilname1 <em>Dilname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dilname1</em>' containment reference.
   * @see #getDilname1()
   * @generated
   */
  void setDilname1(numeral value);

  /**
   * Returns the value of the '<em><b>Dilname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dilname2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dilname2</em>' containment reference.
   * @see #setDilname2(positive_exponent)
   * @see org.topcased.adele.xtext.baguards.baGuards.BaGuardsPackage#getdecimal_integer_literal_Dilname2()
   * @model containment="true"
   * @generated
   */
  positive_exponent getDilname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baguards.baGuards.decimal_integer_literal#getDilname2 <em>Dilname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dilname2</em>' containment reference.
   * @see #getDilname2()
   * @generated
   */
  void setDilname2(positive_exponent value);

} // decimal_integer_literal
