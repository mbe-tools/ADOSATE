/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.virtualprocessor.preferences;

/**
 * A set of constant that are used in the diagram graphical preferences
 * 
 * @generated
 */
public interface VirtualProcessorDiagramPreferenceConstants {
	/**
	 * The key used to install a <i>Abstract Default Background Color</i> Preference.
	 * @generated
	 */
	String ABSTRACT_DEFAULT_BACKGROUND_COLOR = "Abstract Default Background Color";

	/**
	 * The key used to install a <i>Abstract Default Foreground Color</i> Preference.
	 * @generated
	 */
	String ABSTRACT_DEFAULT_FOREGROUND_COLOR = "Abstract Default Foreground Color";

	/**
	 * The key used to install a <i>Abstract Default Font</i> Preference.
	 * @generated
	 */
	String ABSTRACT_DEFAULT_FONT = "Abstract Default Font";

	/**
	 * The key used to install a <i>VirtualBus Default Background Color</i> Preference.
	 * @generated
	 */
	String VIRTUALBUS_DEFAULT_BACKGROUND_COLOR = "VirtualBus Default Background Color";

	/**
	 * The key used to install a <i>VirtualBus Default Foreground Color</i> Preference.
	 * @generated
	 */
	String VIRTUALBUS_DEFAULT_FOREGROUND_COLOR = "VirtualBus Default Foreground Color";

	/**
	 * The key used to install a <i>VirtualBus Default Font</i> Preference.
	 * @generated
	 */
	String VIRTUALBUS_DEFAULT_FONT = "VirtualBus Default Font";

	/**
	 * The key used to install a <i>VirtualProcessor Default Background Color</i> Preference.
	 * @generated
	 */
	String VIRTUALPROCESSOR_DEFAULT_BACKGROUND_COLOR = "VirtualProcessor Default Background Color";

	/**
	 * The key used to install a <i>VirtualProcessor Default Foreground Color</i> Preference.
	 * @generated
	 */
	String VIRTUALPROCESSOR_DEFAULT_FOREGROUND_COLOR = "VirtualProcessor Default Foreground Color";

	/**
	 * The key used to install a <i>VirtualProcessor Default Font</i> Preference.
	 * @generated
	 */
	String VIRTUALPROCESSOR_DEFAULT_FONT = "VirtualProcessor Default Font";

	/**
	 * The key used to install a <i>EventPort Default Background Color</i> Preference.
	 * @generated
	 */
	String EVENTPORT_DEFAULT_BACKGROUND_COLOR = "EventPort Default Background Color";

	/**
	 * The key used to install a <i>EventPort Default Foreground Color</i> Preference.
	 * @generated
	 */
	String EVENTPORT_DEFAULT_FOREGROUND_COLOR = "EventPort Default Foreground Color";

	/**
	 * The key used to install a <i>EventPort Default Font</i> Preference.
	 * @generated
	 */
	String EVENTPORT_DEFAULT_FONT = "EventPort Default Font";

	/**
	 * The key used to install a <i>DataPort Default Background Color</i> Preference.
	 * @generated
	 */
	String DATAPORT_DEFAULT_BACKGROUND_COLOR = "DataPort Default Background Color";

	/**
	 * The key used to install a <i>DataPort Default Foreground Color</i> Preference.
	 * @generated
	 */
	String DATAPORT_DEFAULT_FOREGROUND_COLOR = "DataPort Default Foreground Color";

	/**
	 * The key used to install a <i>DataPort Default Font</i> Preference.
	 * @generated
	 */
	String DATAPORT_DEFAULT_FONT = "DataPort Default Font";

	/**
	 * The key used to install a <i>EventDataPort Default Background Color</i> Preference.
	 * @generated
	 */
	String EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR = "EventDataPort Default Background Color";

	/**
	 * The key used to install a <i>EventDataPort Default Foreground Color</i> Preference.
	 * @generated
	 */
	String EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR = "EventDataPort Default Foreground Color";

	/**
	 * The key used to install a <i>EventDataPort Default Font</i> Preference.
	 * @generated
	 */
	String EVENTDATAPORT_DEFAULT_FONT = "EventDataPort Default Font";

	/**
	 * The key used to install a <i>SubprogramAccess Default Background Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR = "SubprogramAccess Default Background Color";

	/**
	 * The key used to install a <i>SubprogramAccess Default Foreground Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR = "SubprogramAccess Default Foreground Color";

	/**
	 * The key used to install a <i>SubprogramAccess Default Font</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMACCESS_DEFAULT_FONT = "SubprogramAccess Default Font";

	/**
	 * The key used to install a <i>SubprogramGroupAccess Default Background Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR = "SubprogramGroupAccess Default Background Color";

	/**
	 * The key used to install a <i>SubprogramGroupAccess Default Foreground Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR = "SubprogramGroupAccess Default Foreground Color";

	/**
	 * The key used to install a <i>SubprogramGroupAccess Default Font</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESS_DEFAULT_FONT = "SubprogramGroupAccess Default Font";

	/**
	 * The key used to install a <i>FeatureGroup Default Background Color</i> Preference.
	 * @generated
	 */
	String FEATUREGROUP_DEFAULT_BACKGROUND_COLOR = "FeatureGroup Default Background Color";

	/**
	 * The key used to install a <i>FeatureGroup Default Foreground Color</i> Preference.
	 * @generated
	 */
	String FEATUREGROUP_DEFAULT_FOREGROUND_COLOR = "FeatureGroup Default Foreground Color";

	/**
	 * The key used to install a <i>FeatureGroup Default Font</i> Preference.
	 * @generated
	 */
	String FEATUREGROUP_DEFAULT_FONT = "FeatureGroup Default Font";

	/**
	 * The key used to install a <i>AbstractFeature Default Background Color</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR = "AbstractFeature Default Background Color";

	/**
	 * The key used to install a <i>AbstractFeature Default Foreground Color</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR = "AbstractFeature Default Foreground Color";

	/**
	 * The key used to install a <i>AbstractFeature Default Font</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURE_DEFAULT_FONT = "AbstractFeature Default Font";

	/**
	 * The key used to install a <i>EventPortConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String EVENTPORTCONNECTION_EDGE_DEFAULT_FONT = "EventPortConnection Edge Default Font";

	/**
	 * The key used to install a <i>EventPortConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String EVENTPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "EventPortConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>EventPortConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String EVENTPORTCONNECTION_EDGE_DEFAULT_ROUTER = "EventPortConnection Edge Default Router";

	/**
	 * The key used to install a <i>DataPortConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String DATAPORTCONNECTION_EDGE_DEFAULT_FONT = "DataPortConnection Edge Default Font";

	/**
	 * The key used to install a <i>DataPortConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String DATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "DataPortConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>DataPortConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String DATAPORTCONNECTION_EDGE_DEFAULT_ROUTER = "DataPortConnection Edge Default Router";

	/**
	 * The key used to install a <i>EventDataPortConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FONT = "EventDataPortConnection Edge Default Font";

	/**
	 * The key used to install a <i>EventDataPortConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "EventDataPortConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>EventDataPortConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String EVENTDATAPORTCONNECTION_EDGE_DEFAULT_ROUTER = "EventDataPortConnection Edge Default Router";

	/**
	 * The key used to install a <i>AbstractFeatureConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FONT = "AbstractFeatureConnection Edge Default Font";

	/**
	 * The key used to install a <i>AbstractFeatureConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "AbstractFeatureConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>AbstractFeatureConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_ROUTER = "AbstractFeatureConnection Edge Default Router";

	/**
	 * The key used to install a <i>FeatureGroupConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String FEATUREGROUPCONNECTION_EDGE_DEFAULT_FONT = "FeatureGroupConnection Edge Default Font";

	/**
	 * The key used to install a <i>FeatureGroupConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String FEATUREGROUPCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "FeatureGroupConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>FeatureGroupConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String FEATUREGROUPCONNECTION_EDGE_DEFAULT_ROUTER = "FeatureGroupConnection Edge Default Router";

}