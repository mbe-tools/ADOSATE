/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.virtualprocessor.preferences;

import java.util.HashMap;

import org.eclipse.jface.resource.StringConverter;
import org.topcased.modeler.preferences.ITopcasedPreferenceInitializer;

/**
 * Initialize the preferences of VirtualProcessor diagram
 * 
 * @generated
 */
public class VirtualProcessorDiagramTopcasedPreferenceInitializer implements
		ITopcasedPreferenceInitializer {

	/** 
	 * @see org.topcased.modeler.preferences.ITopcasedPreferenceInitializer#getDefaultPreference()
	 *	@generated
	 */
	public HashMap<String, String> getDefaultPreference() {
		HashMap<String, String> defaultVirtualProcessorPreference = new HashMap<String, String>();
		// Initialize the default value of the ABSTRACT_DEFAULT_BACKGROUND_COLOR property 
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.ABSTRACT_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the ABSTRACT_DEFAULT_FOREGROUND_COLOR property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.ABSTRACT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACT_DEFAULT_FONT property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.ABSTRACT_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the VIRTUALBUS_DEFAULT_BACKGROUND_COLOR property 
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.VIRTUALBUS_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the VIRTUALBUS_DEFAULT_FOREGROUND_COLOR property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.VIRTUALBUS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the VIRTUALBUS_DEFAULT_FONT property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.VIRTUALBUS_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the VIRTUALPROCESSOR_DEFAULT_BACKGROUND_COLOR property 
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.VIRTUALPROCESSOR_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the VIRTUALPROCESSOR_DEFAULT_FOREGROUND_COLOR property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.VIRTUALPROCESSOR_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the VIRTUALPROCESSOR_DEFAULT_FONT property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.VIRTUALPROCESSOR_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the EVENTPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.EVENTPORT_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the EVENTPORT_DEFAULT_FOREGROUND_COLOR property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.EVENTPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORT_DEFAULT_FONT property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.EVENTPORT_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the DATAPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.DATAPORT_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the DATAPORT_DEFAULT_FOREGROUND_COLOR property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.DATAPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAPORT_DEFAULT_FONT property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.DATAPORT_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_FONT property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_FONT property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_FONT property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the FEATUREGROUP_DEFAULT_BACKGROUND_COLOR property 
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the FEATUREGROUP_DEFAULT_FOREGROUND_COLOR property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the FEATUREGROUP_DEFAULT_FONT property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR property 
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_FONT property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the EVENTPORTCONNECTION_EDGE_DEFAULT_FONT property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.EVENTPORTCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the EVENTPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.EVENTPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORTCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.EVENTPORTCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the DATAPORTCONNECTION_EDGE_DEFAULT_FONT property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.DATAPORTCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the DATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.DATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAPORTCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.DATAPORTCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FONT property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORTCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.EVENTDATAPORTCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FONT property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_ROUTER property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_FONT property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultVirtualProcessorPreference
				.put(VirtualProcessorDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		return defaultVirtualProcessorPreference;
	}
}
