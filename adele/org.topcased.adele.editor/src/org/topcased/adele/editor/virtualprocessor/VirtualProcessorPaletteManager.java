/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.virtualprocessor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.palette.CreationToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteStack;
import org.eclipse.gef.requests.CreationFactory;
import org.eclipse.ui.IWorkbenchPage;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.adele.editor.editor.ADELEEditor;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;
import org.topcased.adele.model.ADELE_Features.AbstractFeature;
import org.topcased.adele.model.ADELE_Features.AccessDirection;
import org.topcased.adele.model.ADELE_Features.DataPort;
import org.topcased.adele.model.ADELE_Features.EventDataPort;
import org.topcased.adele.model.ADELE_Features.EventPort;
import org.topcased.adele.model.ADELE_Features.PortDirection;
import org.topcased.adele.model.ADELE_Features.SubprogramAccess;
import org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess;
import org.topcased.adele.model.ADELE_Relations.ADELE_RelationsPackage;
import org.topcased.modeler.editor.GraphElementCreationFactory;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.palette.ModelerConnectionCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerPaletteManager;

/**
 * Generated Palette Manager
 *
 * @generated
 */
public class VirtualProcessorPaletteManager extends ModelerPaletteManager {
	// declare all the palette categories of the diagram
	/**
	 * @generated
	 */
	private PaletteDrawer componentsDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer featuresDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer connectionsDrawer;

	/**
	 * @generated
	 */
	private ICreationUtils creationUtils;

	/**
	 * The Constructor
	 *
	 * @param utils the creation utils for the tools of the palette 
	 * @generated
	 */
	public VirtualProcessorPaletteManager(ICreationUtils utils) {
		super();
		this.creationUtils = utils;
	}

	/**
	 * Creates the main categories of the palette
	 *
	 * @generated
	 */
	protected void createCategories() {
		createComponentsDrawer();
		createFeaturesDrawer();
		createConnectionsDrawer();
	}

	/**
	 * Updates the main categories of the palette
	 *
	 * @generated
	 */
	protected void updateCategories() {
		// deletion of the existing categories and creation of the updated categories

		getRoot().remove(componentsDrawer);
		createComponentsDrawer();

		getRoot().remove(featuresDrawer);
		createFeaturesDrawer();

		getRoot().remove(connectionsDrawer);
		createConnectionsDrawer();
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated NOT
	 */
	private void createComponentsDrawer() {
		componentsDrawer = new PaletteDrawer("Components", null);
		List<PaletteEntry> entries = new ArrayList<PaletteEntry>();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getVirtualProcessor(),
				"default");
		entries.add(new ModelerCreationToolEntry("Virtual Processor",
				"Virtual Processor", factory, VirtualProcessorImageRegistry
						.getImageDescriptor("VIRTUALPROCESSOR"),
				VirtualProcessorImageRegistry
						.getImageDescriptor("VIRTUALPROCESSOR_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getVirtualBus(), "default");
		entries.add(new ModelerCreationToolEntry("Virtual Bus", "Virtual Bus",
				factory, VirtualProcessorImageRegistry
						.getImageDescriptor("VIRTUALBUS"),
				VirtualProcessorImageRegistry
						.getImageDescriptor("VIRTUALBUS_LARGE")));

		IWorkbenchPage wPage = ADELEPlugin.getActivePage();
		if (wPage != null && wPage.getActiveEditor() instanceof ADELEEditor ) {
			ADELEEditor editor = (ADELEEditor) wPage.getActiveEditor();
			if (editor != null && editor.getSelectedObject() != null
					&& editor.getSelectedObject().getName().contains(".")) {
				factory = new GraphElementCreationFactory(creationUtils,
						ADELE_ComponentsPackage.eINSTANCE.getAbstract(),
						"default");
				entries.add(new ModelerCreationToolEntry("Abstract",
						"Abstract", factory, VirtualProcessorImageRegistry
								.getImageDescriptor("ABSTRACT"),
						VirtualProcessorImageRegistry
								.getImageDescriptor("ABSTRACT_LARGE")));
			}
		}

		componentsDrawer.addAll(entries);
		getRoot().add(componentsDrawer);
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated NOT
	 */
	private void createFeaturesDrawer() {
		if ( ADELEPlugin.isSystemActiveEditor() ) {
			featuresDrawer = new PaletteDrawer("Features", null);
			List<PaletteEntry> entries = new ArrayList<PaletteEntry>();
	
			CreationFactory factory;
			String newObjTxt;
			CreationToolEntry objectTool;
	
			// Event Ports
	
			PaletteStack eventPortStack = new PaletteStack("Event Port",
					"Event Port Selection",
					VirtualProcessorImageRegistry.getImageDescriptor("EVENTPORT"));
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getEventPort(), "default") {
				public EObject getNewModelObject() {
					EventPort port = (EventPort) super.getNewModelObject();
					port.setDirection(PortDirection.IN_LITERAL);
					return port;
				}
			};
			newObjTxt = "In Event Port";
			objectTool = new ModelerCreationToolEntry(
					newObjTxt,
					newObjTxt,
					factory,
					VirtualProcessorImageRegistry.getImageDescriptor("INEVENTPORT"),
					VirtualProcessorImageRegistry
							.getImageDescriptor("INEVENTPORT_LARGE"));
			eventPortStack.add(objectTool);
			eventPortStack.setActiveEntry(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getEventPort(), "default") {
				public EObject getNewModelObject() {
					EventPort port = (EventPort) super.getNewModelObject();
					port.setDirection(PortDirection.OUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "Out Event Port";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					VirtualProcessorImageRegistry
							.getImageDescriptor("OUTEVENTPORT"),
					VirtualProcessorImageRegistry
							.getImageDescriptor("OUTEVENTPORT_LARGE"));
			eventPortStack.add(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getEventPort(), "default") {
				public EObject getNewModelObject() {
					EventPort port = (EventPort) super.getNewModelObject();
					port.setDirection(PortDirection.INOUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "InOut Event Port";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					VirtualProcessorImageRegistry
							.getImageDescriptor("INOUTEVENTPORT"),
					VirtualProcessorImageRegistry
							.getImageDescriptor("INOUTEVENTPORT_LARGE"));
			eventPortStack.add(objectTool);
	
			entries.add(eventPortStack);
	
			// Data ports
	
			PaletteStack dataPortStack = new PaletteStack("Data Port",
					"Data Port Selection",
					VirtualProcessorImageRegistry.getImageDescriptor("DATAPORT"));
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getDataPort(), "default") {
				public EObject getNewModelObject() {
					DataPort port = (DataPort) super.getNewModelObject();
					port.setDirection(PortDirection.IN_LITERAL);
					return port;
				}
			};
			newObjTxt = "In Data Port";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					VirtualProcessorImageRegistry.getImageDescriptor("INDATAPORT"),
					VirtualProcessorImageRegistry
							.getImageDescriptor("INDATAPORT_LARGE"));
			dataPortStack.add(objectTool);
			dataPortStack.setActiveEntry(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getDataPort(), "default") {
				public EObject getNewModelObject() {
					DataPort port = (DataPort) super.getNewModelObject();
					port.setDirection(PortDirection.OUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "Out Data Port";
			objectTool = new ModelerCreationToolEntry(
					newObjTxt,
					newObjTxt,
					factory,
					VirtualProcessorImageRegistry.getImageDescriptor("OUTDATAPORT"),
					VirtualProcessorImageRegistry
							.getImageDescriptor("OUTDATAPORT_LARGE"));
			dataPortStack.add(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getDataPort(), "default") {
				public EObject getNewModelObject() {
					DataPort port = (DataPort) super.getNewModelObject();
					port.setDirection(PortDirection.INOUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "InOut Data Port";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					VirtualProcessorImageRegistry
							.getImageDescriptor("INOUTDATAPORT"),
					VirtualProcessorImageRegistry
							.getImageDescriptor("INOUTDATAPORT_LARGE"));
			dataPortStack.add(objectTool);
	
			entries.add(dataPortStack);
	
			// Event Data ports
	
			PaletteStack eventDataPortStack = new PaletteStack("Event Data Port",
					"Event Data Port Selection",
					VirtualProcessorImageRegistry
							.getImageDescriptor("EVENTDATAPORT"));
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getEventDataPort(), "default") {
				public EObject getNewModelObject() {
					EventDataPort port = (EventDataPort) super.getNewModelObject();
					port.setDirection(PortDirection.IN_LITERAL);
					return port;
				}
			};
			newObjTxt = "In Event Data Port";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					VirtualProcessorImageRegistry
							.getImageDescriptor("INEVENTDATAPORT"),
					VirtualProcessorImageRegistry
							.getImageDescriptor("INEVENTDATAPORT_LARGE"));
			eventDataPortStack.add(objectTool);
			eventDataPortStack.setActiveEntry(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getEventDataPort(), "default") {
				public EObject getNewModelObject() {
					EventDataPort port = (EventDataPort) super.getNewModelObject();
					port.setDirection(PortDirection.OUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "Out Event Data Port";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					VirtualProcessorImageRegistry
							.getImageDescriptor("OUTEVENTDATAPORT"),
					VirtualProcessorImageRegistry
							.getImageDescriptor("OUTEVENTDATAPORT_LARGE"));
			eventDataPortStack.add(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getEventDataPort(), "default") {
				public EObject getNewModelObject() {
					EventDataPort port = (EventDataPort) super.getNewModelObject();
					port.setDirection(PortDirection.INOUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "InOut Event Data Port";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					VirtualProcessorImageRegistry
							.getImageDescriptor("INOUTEVENTDATAPORT"),
					VirtualProcessorImageRegistry
							.getImageDescriptor("INOUTEVENTDATAPORT_LARGE"));
			eventDataPortStack.add(objectTool);
	
			entries.add(eventDataPortStack);
	
			// Feature Group
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getFeatureGroup(), "default");
			entries.add(new ModelerCreationToolEntry("Feature Group",
					"Feature Group", factory, VirtualProcessorImageRegistry
							.getImageDescriptor("FEATUREGROUP"),
					VirtualProcessorImageRegistry
							.getImageDescriptor("FEATUREGROUP_LARGE")));
	
			// Subprogram Access
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getSubprogramAccess(),
					"default") {
				public EObject getNewModelObject() {
					SubprogramAccess access = (SubprogramAccess) super
							.getNewModelObject();
					access.setDirection(AccessDirection.PROVIDED_LITERAL);
					return access;
				}
			};
			newObjTxt = "Provides subprogram access";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					VirtualProcessorImageRegistry
							.getImageDescriptor("PROVIDEDSUBPROGRAMACCESS"),
					VirtualProcessorImageRegistry
							.getImageDescriptor("PROVIDEDSUBPROGRAMACCESS_LARGE"));
	
			entries.add(objectTool);
	
			// Subprogram Group Access
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getSubprogramGroupAccess(),
					"default") {
				public EObject getNewModelObject() {
					SubprogramGroupAccess access = (SubprogramGroupAccess) super
							.getNewModelObject();
					access.setDirection(AccessDirection.PROVIDED_LITERAL);
					return access;
				}
			};
			newObjTxt = "Provides subprogram group access";
			objectTool = new ModelerCreationToolEntry(
					newObjTxt,
					newObjTxt,
					factory,
					VirtualProcessorImageRegistry
							.getImageDescriptor("PROVIDEDSUBPROGRAMGROUPACCESS"),
					VirtualProcessorImageRegistry
							.getImageDescriptor("PROVIDEDSUBPROGRAMGROUPACCESS_LARGE"));
			entries.add(objectTool);
	
			// Abstract Feature
	
			PaletteStack abstractFeatureStack = new PaletteStack("Feature",
					"Feature Selection",
					VirtualProcessorImageRegistry
							.getImageDescriptor("ABSTRACTFEATURE"));
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getAbstractFeature(), "default") {
				public EObject getNewModelObject() {
					AbstractFeature port = (AbstractFeature) super
							.getNewModelObject();
					//port.setDirection(AbstractFeatureDirection.NONE);
					return port;
				}
			};
			newObjTxt = "Feature";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					VirtualProcessorImageRegistry
							.getImageDescriptor("ABSTRACTFEATURE"),
					VirtualProcessorImageRegistry
							.getImageDescriptor("ABSTRACTFEATURE_LARGE"));
			abstractFeatureStack.add(objectTool);
			abstractFeatureStack.setActiveEntry(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getAbstractFeature(), "default") {
				public EObject getNewModelObject() {
					AbstractFeature port = (AbstractFeature) super
							.getNewModelObject();
					//port.setDirection(AbstractFeatureDirection.IN);
					return port;
				}
			};
			newObjTxt = "In Feature";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					VirtualProcessorImageRegistry
							.getImageDescriptor("INABSTRACTFEATURE"),
					VirtualProcessorImageRegistry
							.getImageDescriptor("INABSTRACTFEATURE_LARGE"));
			abstractFeatureStack.add(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getAbstractFeature(), "default") {
				public EObject getNewModelObject() {
					AbstractFeature port = (AbstractFeature) super
							.getNewModelObject();
					//port.setDirection(AbstractFeatureDirection.OUT);
					return port;
				}
			};
			newObjTxt = "Out Feature";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					VirtualProcessorImageRegistry
							.getImageDescriptor("OUTABSTRACTFEATURE"),
					VirtualProcessorImageRegistry
							.getImageDescriptor("OUTABSTRACTFEATURE_LARGE"));
			abstractFeatureStack.add(objectTool);
	
			entries.add(abstractFeatureStack);
	
			featuresDrawer.addAll(entries);
			getRoot().add(featuresDrawer);
		}
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated
	 */
	private void createConnectionsDrawer() {
		connectionsDrawer = new PaletteDrawer("Connections", null);
		List<PaletteEntry> entries = new ArrayList<PaletteEntry>();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getEventPortConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Event Port Connection", "Event Port Connection", factory,
				VirtualProcessorImageRegistry
						.getImageDescriptor("EVENTPORTCONNECTION"),
				VirtualProcessorImageRegistry
						.getImageDescriptor("EVENTPORTCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getDataPortConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Data Port Connection", "Data Port Connection", factory,
				VirtualProcessorImageRegistry
						.getImageDescriptor("DATAPORTCONNECTION"),
				VirtualProcessorImageRegistry
						.getImageDescriptor("DATAPORTCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getEventDataPortConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Event Data Port Connection", "Event Data Port Connection",
				factory, VirtualProcessorImageRegistry
						.getImageDescriptor("EVENTDATAPORTCONNECTION"),
				VirtualProcessorImageRegistry
						.getImageDescriptor("EVENTDATAPORTCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(
				creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getAbstractFeatureConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Feature Connection", "Feature Connection", factory,
				VirtualProcessorImageRegistry
						.getImageDescriptor("ABSTRACTFEATURECONNECTION"),
				VirtualProcessorImageRegistry
						.getImageDescriptor("ABSTRACTFEATURECONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getFeatureGroupConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Feature Group Connection", "Feature Group Connection",
				factory, VirtualProcessorImageRegistry
						.getImageDescriptor("FEATUREGROUPCONNECTION"),
				VirtualProcessorImageRegistry
						.getImageDescriptor("FEATUREGROUPCONNECTION_LARGE")));

		connectionsDrawer.addAll(entries);
		getRoot().add(connectionsDrawer);
	}

}
