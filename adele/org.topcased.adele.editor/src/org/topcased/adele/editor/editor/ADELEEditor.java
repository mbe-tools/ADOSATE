/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *  	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.editor;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.EventObject;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.ISection;
import org.eclipse.ui.views.properties.tabbed.TabContents;
import org.osgi.framework.Bundle;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.topcased.adele.admin.ADELEAdministrationPlugin;
import org.topcased.adele.common.objectDescriptionModel.OdsPropertySection;
import org.topcased.adele.common.objectDescriptionModel.OdsView;
import org.topcased.adele.common.utils.ADELEModeler;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.adele.editor.objectDescriptionModel.ADELEOdsView;
import org.topcased.adele.editor.preferences.ADELEPreferenceConstants;
import org.topcased.adele.editor.utils.edit.ADELE_DiagramEditPart;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Components.Package;
import org.topcased.adele.model.ADELE_Features.Feature;
import org.topcased.adele.model.ADELE_Relations.Relation;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.adele.model.KernelSpices.SKFeature;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.adele.model.KernelSpices.SKObject;
import org.topcased.adele.model.KernelSpices.SKRelation;
import org.topcased.adele.model.ObjectDescriptionModel.SKODSection;
import org.topcased.adele.model.ObjectDescriptionModel.SKODSystem;
import org.topcased.adele.model.ObjectDescriptionModel.impl.SKODSFactoryException;
import org.topcased.adele.model.ObjectDescriptionModel.impl.SKODSFactoryImpl;
import org.topcased.modeler.commands.AdvancedCommandStack;
import org.topcased.modeler.commands.GEFtoEMFCommandStackWrapper;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.documentation.EAnnotationDocPage;
import org.topcased.modeler.documentation.IDocPage;
import org.topcased.modeler.edit.EMFGraphEdgeEditPart;
import org.topcased.modeler.edit.EMFGraphNodeEditPart;
import org.topcased.modeler.editor.properties.ModelerPropertySheetPage;
import org.topcased.modeler.utils.Utils;

/**
 * Generated Model editor
 *
 * @generated
 */
public class ADELEEditor extends ADELEModeler {

	public static final String EDITOR_ID = "org.topcased.adele.editor.editor.ADELEEditor";

	public ADELEEditor() {
		super();

		initDatabase();

		ADELEOdsView.initOdsViewEditors();
	}

	private void initDatabase() {
		final IPreferenceStore store = ADELEPlugin.getDefault().getPreferenceStore();
		String databasePath = store.getString(ADELEPreferenceConstants.ADELE_DATABASE_PATH);
		
		try {
			initDatabase( databasePath );
		}
		catch( final IOException p_ex ) {
			handleErrorDatabaseNotSet();
		}
	}
	
	private void initDatabase( String p_databasePath ) 
	throws IOException {
		if ( !(new File( p_databasePath )).exists() ) {
			
			// DB: Search for default file in admin plugin
			final Bundle adminBundle = ADELEAdministrationPlugin.getDefault().getBundle();
			final String defaultFile = "/usr/config/database.odsconfig";
			final URI uri = URI.createPlatformPluginURI( adminBundle.getSymbolicName() + defaultFile, true );
			
			if ( new ExtensibleURIConverterImpl().exists( uri, null ) ) {
				final URL entry = adminBundle.getEntry( defaultFile );
				
				if ( entry != null ) {
					final String path = FileLocator.toFileURL( entry ).getPath();
					final IPreferenceStore store = ADELEPlugin.getDefault().getPreferenceStore();
					store.setValue( ADELEPreferenceConstants.ADELE_DATABASE_PATH, new Path( path ).toOSString() );
					store.needsSaving();
					initDatabase( path );
				}

			}
			else {
				handleErrorDatabaseNotSet();
			}
		}
		else {
			try {
				odStructure = new SKODSFactoryImpl( p_databasePath);
				odSystemArray = new SKODSystem[odStructure.getODSystemsId()
						.size()];
			} catch (SKODSFactoryException e) {
				Status status = new Status(IStatus.ERROR, "plugin", 0,
						"Database error", null);
				ErrorDialog.openError(Display.getCurrent().getActiveShell(),
						"Database error", e.getMessage(), status);
			}
		}
	}
	
	private void handleErrorDatabaseNotSet() {
		final Shell s = new Shell();
		final MessageBox messageBox = new MessageBox(s, SWT.ICON_QUESTION | SWT.YES | SWT.NO);
		messageBox.setMessage("Either database is not set or file does not exist.\n"
					+ "Do you want to set it now?\n"
					+ "If answering no the ods part of ADELE will not be usable.");
		messageBox.setText("Database not set");
		
		if (messageBox.open() == SWT.YES) {
			FileDialog fdlg = new FileDialog(s);
			final String databasePath = fdlg.open();
			
			final IPreferenceStore store = ADELEPlugin.getDefault().getPreferenceStore();
			store.setValue(ADELEPreferenceConstants.ADELE_DATABASE_PATH,
					(new Path( databasePath )).toOSString());
			store.needsSaving();
			try {
				odStructure = new SKODSFactoryImpl( databasePath );
				odSystemArray = new SKODSystem[odStructure.getODSystemsId()
						.size()];
			} catch (SKODSFactoryException e) {
				Status status = new Status(IStatus.ERROR, "plugin", 0,
						"Database error", null);
				ErrorDialog.openError(
						Display.getCurrent().getActiveShell(),
						"Database error", e.getMessage(), status);
			}
		}
	}

	@Override
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
		addSelectionChangedListener(new InnerSelectionChangedListener());
	}
//
//	/**
//	 * @see org.topcased.modeler.editor.Modeler#getAdapterFactories()
//	 * @generated
//	 */
//	@SuppressWarnings({ "unchecked", "rawtypes" })
//	@Override
//	protected List getAdapterFactories() {
//		List factories = new ArrayList();
//		factories
//				.add(new org.topcased.adele.model.ADELE_Components.provider.ADELE_ComponentsItemProviderAdapterFactory());
//		factories
//				.add(new org.topcased.adele.editor.providers.ADELE_ComponentsModelerProviderAdapterFactory());
//		factories
//				.add(new org.topcased.adele.model.ADELE_Features.provider.ADELE_FeaturesItemProviderAdapterFactory());
//		factories
//				.add(new org.topcased.adele.editor.providers.ADELE_FeaturesModelerProviderAdapterFactory());
//		factories
//				.add(new org.topcased.adele.model.ADELE_Relations.provider.ADELE_RelationsItemProviderAdapterFactory());
//		factories
//				.add(new org.topcased.adele.editor.providers.ADELE_RelationsModelerProviderAdapterFactory());
//		factories
//				.add(new org.topcased.adele.model.ba_components.provider.Ba_componentsItemProviderAdapterFactory());
//		factories
//				.add(new org.topcased.adele.editor.providers.Ba_componentsModelerProviderAdapterFactory());
//		factories
//				.add(new org.topcased.adele.model.ba_features.provider.Ba_featuresItemProviderAdapterFactory());
//		factories
//				.add(new org.topcased.adele.editor.providers.Ba_featuresModelerProviderAdapterFactory());
//		factories
//				.add(new org.topcased.adele.model.ba_relations.provider.Ba_relationsItemProviderAdapterFactory());
//		factories
//				.add(new org.topcased.adele.editor.providers.Ba_relationsModelerProviderAdapterFactory());
//		factories
//				.add(new org.topcased.adele.model.KernelSpices.provider.KernelSpicesItemProviderAdapterFactory());
//		factories
//				.add(new org.topcased.adele.editor.providers.KernelSpicesModelerProviderAdapterFactory());
//		factories
//				.add(new org.topcased.adele.model.ObjectDescriptionModel.provider.ObjectDescriptionModelItemProviderAdapterFactory());
//		factories
//				.add(new org.topcased.adele.editor.providers.ObjectDescriptionModelModelerProviderAdapterFactory());
//
//		factories.addAll(super.getAdapterFactories());
//
//		return factories;
//	}

	/**
	 * Returns the Editor Id
	 *
	 * @see org.topcased.modeler.editor.Modeler#getId()
	 * @return the String that identifies the Editor
	 * @generated
	 */
	public String getId() {
		return EDITOR_ID;
	}

	/**
	 * @see org.topcased.modeler.editor.Modeler#getAdapter(java.lang.Class)
	 * @generated
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	public Object getAdapter(Class type) {
		if (type == IDocPage.class) {
			GEFtoEMFCommandStackWrapper stack = new GEFtoEMFCommandStackWrapper(
					getCommandStack());
			return new EAnnotationDocPage(stack);
		}
		return super.getAdapter(type);
	}

	/**
	 * @see org.topcased.modeler.editor.Modeler#getPreferenceStore()
	 *
	 * @generated
	 */
	public IPreferenceStore getPreferenceStore() {
		if (getEditorInput() instanceof IFileEditorInput) {
			IProject project = (((IFileEditorInput) getEditorInput()).getFile())
					.getProject();

			Preferences root = Platform.getPreferencesService().getRootNode();
			try {
				if (root.node(ProjectScope.SCOPE).node(project.getName())
						.nodeExists(ADELEPlugin.getId())) {
					return new ScopedPreferenceStore(new ProjectScope(project),
							ADELEPlugin.getId());
				}
			} catch (BackingStoreException e) {
				e.printStackTrace();
			}
		}
		return ADELEPlugin.getDefault().getPreferenceStore();
	}

	@Override
	protected void createActions() {

		super.createActions();

		String id = getActionRegistry().getAction("delete").getId();
		while (getSelectionActions().contains(id)) {
			getSelectionActions().remove(id);
		}
		getActionRegistry().getAction("deleteModelObject").setText("Delete");
		
		// DB: Moved to create edit domain.
//		CommandStack stack = getEditingDomain().getCommandStack();
//		CommandStackListener stackListener = new InnerCommandStackListener();
//		stack.addCommandStackListener(stackListener);
	}
	
//	@Override
//    public void setMixedEditDomain( final IMixedEditDomain p_mixedEdDomain ) {
//		super.setMixedEditDomain( p_mixedEdDomain );
//		
//		CommandStack stack = getEditingDomain().getCommandStack();
//		CommandStackListener stackListener = new InnerCommandStackListener();
//		stack.addCommandStackListener(stackListener);
//	}

//	@Override
//	protected IContentOutlinePage createOutlinePage() {
//		IContentOutlinePage page = super.createOutlinePage();
//
//		loadADELEFiles();
//
//		return page;
//	}
//
//	protected void loadADELEFiles() {
//
//		StringTokenizer tok = new StringTokenizer(getResourceSet()
//				.getResources().get(0).getURI().toPlatformString(true), "/");
//		String projectFolderPath = "/" + tok.nextToken() + "/"
//				+ tok.nextToken();
//
//		IProject project = ResourcesPlugin.getWorkspace().getRoot()
//				.getFolder(new Path(projectFolderPath)).getProject();
//
//		EList<IResource> resources = ADELE_Utils.getAllMembers(project);
//
//		for (IResource res : resources) {
//			getResourceSet().getResource(
//					URI.createPlatformResourceURI(res.getFullPath().toString(),
//							true), true);
//		}
//	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		saveODSystems();
		
		final OdsView odsView = findOdsView();
		
		if ( odsView != null ) {
			odsView.setDirty( false );
		}
//		try {
//			((OdsView) PlatformUI.getWorkbench().getActiveWorkbenchWindow()
//					.getActivePage().findView("org.topcased.adele.ods"))
//					.setDirty(false);
//		} catch (NullPointerException e) {
//		}
		super.doSave(monitor);
		dirty = false;
		refreshDirty();
	}

	@Override
	public boolean isDirty() {
		if ( dirty || super.isDirty() ) {
			return true;
		}
		
		//try {
		final OdsView odsPanel = findOdsView();
		
		if ( odsPanel == null ) {
			return false;
		}
		
		return odsPanel.isDirty();
			
			
//					(OdsView) PlatformUI.getWorkbench()
//					.getActiveWorkbenchWindow().getActivePage()
//					.findView("org.topcased.adele.ods");
//			return tmp.isDirty() || dirty || super.isDirty();
//		} catch (Exception e) {
//			return super.isDirty() || dirty;
//		}
	}

//	public void refreshComponents() {
//		EList<DiagramElement> children = getActiveDiagram().getContained();
//		for (int i = 0; i < children.size(); i++) {
//			EObject tmp = Utils.getElement((GraphElement) children.get(i));
//			if (tmp instanceof Component
//					&& ((Component) tmp).getType() != null) {
//				RefreshFeaturesCommand refreshPortsCommand = new RefreshFeaturesCommand(
//						this, tmp);
//				if (refreshPortsCommand != null
//						&& refreshPortsCommand.canExecute()) {
//					refreshPortsCommand.execute();
//					dirty = true;
//				}
//			}
//		}
//	}

	@Override
	public String getEditorKind(SKObject compo) {
		if (editorKind == null)
			setEditorKind(compo);
		return editorKind;
	}

	public void setEditorKind( final SKObject compo ) {
		final EObject rootCompo = EcoreUtil.getRootContainer( compo );//ADELE_Utils.getRoot(compo);
		
		if (rootCompo instanceof Package) {
			editorKind = PACKAGE_EDITOR_KIND;
		}
		else {
			editorKind = SYSTEM_EDITOR_KIND;
		}
	}
	
	private OdsView findOdsView() {
		if ( PlatformUI.getWorkbench() != null ) {
			for ( final IWorkbenchWindow window : PlatformUI.getWorkbench().getWorkbenchWindows() ) {
				final IWorkbenchPage workbenchPage = window.getActivePage();
				
				if ( workbenchPage != null ) {
					final IViewPart propertyView = workbenchPage.findView( "org.eclipse.ui.views.PropertySheet" );
					if ( propertyView != null ) {
						final IPropertySheetPage page = (IPropertySheetPage) propertyView.getAdapter( IPropertySheetPage.class );
						
						if ( page instanceof ModelerPropertySheetPage ) {
							final ModelerPropertySheetPage modPropPage = (ModelerPropertySheetPage) page;
							final TabContents tabContent = modPropPage.getCurrentTab();
							
							if ( tabContent != null ) {
								for ( final ISection tabSection : tabContent.getSections() ) {
									if ( tabSection instanceof OdsPropertySection ) {
										return (OdsView) ( (OdsPropertySection) tabSection ).getPropertyPanel();
									}
								}
							}
						}
					}
				}
			}
		}
		
		return null;
	}

	protected class InnerSelectionChangedListener implements ISelectionChangedListener {

		@Override
		public void selectionChanged(SelectionChangedEvent arg0) {
			if (currentSelectedObject == null) {
				updatePaletteRoot();
			}

			final OdsView ods = findOdsView();
			
			if ( ods != null ) {
				if (ods.isDirty()) {
					saveODSystems();
					ods.setDirty(false);
				}
			}
	
			// DB: The Ods view in the properties may but have been created yet. But we need to change the selection
			// for when the button in the ods view is clicked.
			Object obj = ((IStructuredSelection) arg0.getSelection()).getFirstElement();
			SKHierarchicalObject skobj;
			if (obj instanceof SKComponent || obj instanceof SKFeature
					|| obj instanceof SKRelation) {
				skobj = (SKHierarchicalObject) obj;
			} else if (obj instanceof EMFGraphNodeEditPart
					&& ((EMFGraphNodeEditPart) obj).getEObject() instanceof SKHierarchicalObject) {
				skobj = (SKHierarchicalObject) ((EMFGraphNodeEditPart) obj)
						.getEObject();
			} else if (obj instanceof EMFGraphEdgeEditPart
					&& ((EMFGraphEdgeEditPart) obj).getEObject() instanceof SKHierarchicalObject) {
				skobj = (SKHierarchicalObject) ((EMFGraphEdgeEditPart) obj)
						.getEObject();
			} else if (obj instanceof ADELE_DiagramEditPart
					&& ((ADELE_DiagramEditPart) obj).getEObject() instanceof SKHierarchicalObject) {
				skobj = (SKHierarchicalObject) ((ADELE_DiagramEditPart) obj)
						.getEObject();
			} else {
				skobj = null;
			}

			if (obj != null && skobj != null
					&& !skobj.equals(currentSelectedObject)) {
				currentSelectedObject = skobj;
				for (int i = 0; i < odSystemArray.length; i++) {
					try {
						odSystemArray[i] = odStructure.getODSystem(odStructure.getODSystemsId().get(i), skobj);
					} catch (SKODSFactoryException msg) {
						msg.printStackTrace();
						Status status = new Status(
								IStatus.ERROR,
								"plugin",
								0,
								"Database error in current editor configuration.",
								null);
						ErrorDialog.openError(Display.getCurrent()
								.getActiveShell(), "Database error", msg
								.getMessage(), status);
					}
				}
			}
		}
	}

//	private class InnerCommandStackListener implements CommandStackListener
//
//	{
	@Override
	public void commandStackChanged( final EventObject p_eventObj ) {
		super.commandStackChanged( p_eventObj );
		
		Command cmd = ((AdvancedCommandStack) p_eventObj.getSource()).getUndoCommand();

		SKHierarchicalObject selected = getSelectedObject();
		if (cmd != null && cmd.getLabel() != null
				&& cmd.getLabel().equals("Delete Object from Model")) {
			if (selected instanceof Component
					&& selected.getParent() != null
					&& ((Component) selected.getParent())
							.isSubcomponentsLock())
				cmd.undo();
			if (selected instanceof Feature
					&& ((Feature) selected).getComponent() != null
					&& ((Component) ((Feature) selected)
							.getComponent()).isFeaturesLock())
				cmd.undo();
			if (selected instanceof Relation
					&& ((Component) Utils
							.getElement((GraphElement) getActiveDiagram()))
							.isSubcomponentsLock())
				cmd.undo();
		}
	}
	//}

	@Override
	public boolean manageODSection(String name) {
		if ( name == null ) {
			return false;
		}
		
		if (name.equalsIgnoreCase("classifier"))
			return true;
		else if (name.equalsIgnoreCase("refines"))
			return true;
		else if (name.equalsIgnoreCase("Condition"))
			return true;
		else if (name.equalsIgnoreCase("Action"))
			return true;
		else if (name.equalsIgnoreCase("AADLCode"))
			return true;
		else if (name.equalsIgnoreCase("Extends"))
			return true;
		else if (name.equalsIgnoreCase("BAType"))
			return true;
		else if (name.equalsIgnoreCase("BAKIND"))
			return true;
		else
			return false;
	}

	@Override
	public void setOdsView(OdsView ods, SKODSection name) {
		if (name.getId().equalsIgnoreCase("classifier"))
			ADELEOdsView.setOdsViewClassifier(ods);
		else if (name.getId().equalsIgnoreCase("AADLCode"))
			ADELEOdsView.setOdsViewAADLCode(name, ods);
		else if (name.getId().equalsIgnoreCase("Refines"))
			ADELEOdsView.setOdsViewRefines(ods);
		else if (name.getId().equalsIgnoreCase("Extends"))
			ADELEOdsView.setOdsViewExtends(ods);
		else if (name.getId().equalsIgnoreCase("Condition"))
			ADELEOdsView.setOdsViewGuard(name, ods);
		else if (name.getId().equalsIgnoreCase("Action"))
			ADELEOdsView.setOdsViewAction(name, ods);
		else if (name.getId().equalsIgnoreCase("BAType"))
			ADELEOdsView.setOdsViewBAType(ods);
		else if (name.getId().equalsIgnoreCase("BAKIND"))
			ADELEOdsView.setOdsViewBAKind(ods);
	}
	//
	//	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
	//		// TODO Auto-generated method stub
	//		
	//	}
	//
}
