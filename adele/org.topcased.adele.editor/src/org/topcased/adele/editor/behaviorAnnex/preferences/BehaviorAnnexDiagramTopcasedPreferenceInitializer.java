/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.behaviorAnnex.preferences;

import java.util.HashMap;

import org.eclipse.jface.resource.StringConverter;
import org.topcased.modeler.preferences.ITopcasedPreferenceInitializer;

/**
 * Initialize the preferences of BehaviorAnnex diagram
 * 
 * @generated
 */
public class BehaviorAnnexDiagramTopcasedPreferenceInitializer implements
		ITopcasedPreferenceInitializer {

	/** 
	 * @see org.topcased.modeler.preferences.ITopcasedPreferenceInitializer#getDefaultPreference()
	 *	@generated
	 */
	public HashMap<String, String> getDefaultPreference() {
		HashMap<String, String> defaultBehaviorAnnexPreference = new HashMap<String, String>();
		// Initialize the default value of the BASTATE_DEFAULT_BACKGROUND_COLOR property 
		defaultBehaviorAnnexPreference
				.put(BehaviorAnnexDiagramPreferenceConstants.BASTATE_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the BASTATE_DEFAULT_FOREGROUND_COLOR property
		defaultBehaviorAnnexPreference
				.put(BehaviorAnnexDiagramPreferenceConstants.BASTATE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BASTATE_DEFAULT_FONT property
		defaultBehaviorAnnexPreference.put(
				BehaviorAnnexDiagramPreferenceConstants.BASTATE_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the BAVARIABLE_DEFAULT_BACKGROUND_COLOR property 
		defaultBehaviorAnnexPreference
				.put(BehaviorAnnexDiagramPreferenceConstants.BAVARIABLE_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the BAVARIABLE_DEFAULT_FOREGROUND_COLOR property
		defaultBehaviorAnnexPreference
				.put(BehaviorAnnexDiagramPreferenceConstants.BAVARIABLE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BAVARIABLE_DEFAULT_FONT property
		defaultBehaviorAnnexPreference
				.put(BehaviorAnnexDiagramPreferenceConstants.BAVARIABLE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the BATRANSITION_EDGE_DEFAULT_FONT property
		defaultBehaviorAnnexPreference
				.put(BehaviorAnnexDiagramPreferenceConstants.BATRANSITION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the BATRANSITION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultBehaviorAnnexPreference
				.put(BehaviorAnnexDiagramPreferenceConstants.BATRANSITION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BATRANSITION_EDGE_DEFAULT_ROUTER property
		defaultBehaviorAnnexPreference
				.put(BehaviorAnnexDiagramPreferenceConstants.BATRANSITION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		return defaultBehaviorAnnexPreference;
	}
}
