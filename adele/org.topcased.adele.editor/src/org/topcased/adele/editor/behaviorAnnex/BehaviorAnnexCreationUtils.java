/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.behaviorAnnex;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch;
import org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch;
import org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch;
import org.topcased.adele.model.KernelSpices.util.KernelSpicesSwitch;
import org.topcased.adele.model.ObjectDescriptionModel.util.ObjectDescriptionModelSwitch;
import org.topcased.adele.model.ba_components.util.Ba_componentsSwitch;
import org.topcased.adele.model.ba_features.util.Ba_featuresSwitch;
import org.topcased.adele.model.ba_relations.util.Ba_relationsSwitch;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.editor.AbstractCreationUtils;
import org.topcased.modeler.graphconf.DiagramGraphConf;

/**
 * This utility class allows to create a GraphElement associated with a Model Object
 *
 * @generated
 */
public class BehaviorAnnexCreationUtils extends AbstractCreationUtils {

	/**
	 * Constructor
	 *
	 * @param diagramConf the Diagram Graphical Configuration
	 * @generated
	 */
	public BehaviorAnnexCreationUtils(DiagramGraphConf diagramConf) {
		super(diagramConf);
	}

	/**
	 * @generated
	 */
	private class GraphicADELE_ComponentsSwitch extends ADELE_ComponentsSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicADELE_ComponentsSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicADELE_FeaturesSwitch extends ADELE_FeaturesSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicADELE_FeaturesSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicADELE_RelationsSwitch extends ADELE_RelationsSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicADELE_RelationsSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicKernelSpicesSwitch extends KernelSpicesSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicKernelSpicesSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.KernelSpices.util.KernelSpicesSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicObjectDescriptionModelSwitch extends
			ObjectDescriptionModelSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicObjectDescriptionModelSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ObjectDescriptionModel.util.ObjectDescriptionModelSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicBa_componentsSwitch extends Ba_componentsSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicBa_componentsSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ba_components.util.Ba_componentsSwitch#caseBAState(org.topcased.adele.model.ba_components.BAState)
		 * @generated
		 */
		public Object caseBAState(
				org.topcased.adele.model.ba_components.BAState object) {
			if ("default".equals(presentation)) {
				return createGraphElementBAState(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ba_components.util.Ba_componentsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicBa_featuresSwitch extends Ba_featuresSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicBa_featuresSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ba_features.util.Ba_featuresSwitch#caseBAVariable(org.topcased.adele.model.ba_features.BAVariable)
		 * @generated
		 */
		public Object caseBAVariable(
				org.topcased.adele.model.ba_features.BAVariable object) {
			if ("default".equals(presentation)) {
				return createGraphElementBAVariable(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ba_features.util.Ba_featuresSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicBa_relationsSwitch extends Ba_relationsSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicBa_relationsSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ba_relations.util.Ba_relationsSwitch#caseBATransition(org.topcased.adele.model.ba_relations.BATransition)
		 * @generated
		 */
		public Object caseBATransition(
				org.topcased.adele.model.ba_relations.BATransition object) {
			if ("default".equals(presentation)) {
				return createGraphElementBATransition(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ba_relations.util.Ba_relationsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @see org.topcased.modeler.editor.ICreationUtils#createGraphElement(org.eclipse.emf.ecore.EObject, java.lang.String)
	 * @generated
	 */
	public GraphElement createGraphElement(EObject obj, String presentation) {
		Object graphElt = null;

		if ("http://ADELE_Components".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicADELE_ComponentsSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ADELE_Features".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicADELE_FeaturesSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ADELE_Relations".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicADELE_RelationsSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ba_components/1.0".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicBa_componentsSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ba_features/1.0".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicBa_featuresSwitch(presentation).doSwitch(obj);
		}
		if ("http://ba_relations/1.0".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicBa_relationsSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://KernelSpices".equals(obj.eClass().getEPackage().getNsURI())) {
			graphElt = new GraphicKernelSpicesSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ObjectDescriptionModel".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicObjectDescriptionModelSwitch(presentation)
					.doSwitch(obj);
		}

		return (GraphElement) graphElt;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementBAState(
			org.topcased.adele.model.ba_components.BAState element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementBAVariable(
			org.topcased.adele.model.ba_features.BAVariable element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementBATransition(
			org.topcased.adele.model.ba_relations.BATransition element,
			String presentation) {
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * Create the ModelObject with its initial children
	 * 
	 * @param obj the model object
	 * @return the model object with its children
	 * @generated
	 */
	public EObject createModelObject(EObject obj) {
		return obj;
	}

	/**
	 * Get the preference store associated with the current editor.
	 * 
	 * @return IPreferenceStore
	 * @generated
	 */
	private IPreferenceStore getPreferenceStore() {
		IEditorInput editorInput = ADELEPlugin.getActivePage()
				.getActiveEditor().getEditorInput();
		if (editorInput instanceof IFileEditorInput) {
			IProject project = ((IFileEditorInput) editorInput).getFile()
					.getProject();
			Preferences root = Platform.getPreferencesService().getRootNode();
			try {
				if (root.node(ProjectScope.SCOPE).node(project.getName())
						.nodeExists(ADELEPlugin.getId())) {
					return new ScopedPreferenceStore(new ProjectScope(project),
							ADELEPlugin.getId());
				}
			} catch (BackingStoreException e) {
				e.printStackTrace();
			}
		}
		return ADELEPlugin.getDefault().getPreferenceStore();
	}
}
