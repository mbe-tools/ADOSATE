/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.behaviorAnnex.preferences;

/**
 * A set of constant that are used in the diagram graphical preferences
 * 
 * @generated
 */
public interface BehaviorAnnexDiagramPreferenceConstants {
	/**
	 * The key used to install a <i>BAState Default Background Color</i> Preference.
	 * @generated
	 */
	String BASTATE_DEFAULT_BACKGROUND_COLOR = "BAState Default Background Color";

	/**
	 * The key used to install a <i>BAState Default Foreground Color</i> Preference.
	 * @generated
	 */
	String BASTATE_DEFAULT_FOREGROUND_COLOR = "BAState Default Foreground Color";

	/**
	 * The key used to install a <i>BAState Default Font</i> Preference.
	 * @generated
	 */
	String BASTATE_DEFAULT_FONT = "BAState Default Font";

	/**
	 * The key used to install a <i>BAVariable Default Background Color</i> Preference.
	 * @generated
	 */
	String BAVARIABLE_DEFAULT_BACKGROUND_COLOR = "BAVariable Default Background Color";

	/**
	 * The key used to install a <i>BAVariable Default Foreground Color</i> Preference.
	 * @generated
	 */
	String BAVARIABLE_DEFAULT_FOREGROUND_COLOR = "BAVariable Default Foreground Color";

	/**
	 * The key used to install a <i>BAVariable Default Font</i> Preference.
	 * @generated
	 */
	String BAVARIABLE_DEFAULT_FONT = "BAVariable Default Font";

	/**
	 * The key used to install a <i>BATransition Edge Default Font</i> Preference.
	 * @generated
	 */
	String BATRANSITION_EDGE_DEFAULT_FONT = "BATransition Edge Default Font";

	/**
	 * The key used to install a <i>BATransition Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String BATRANSITION_EDGE_DEFAULT_FOREGROUND_COLOR = "BATransition Edge Default Foreground Color";

	/**
	 * The key used to install a <i>BATransition Edge Default Router</i> Preference.
	 * @generated
	 */
	String BATRANSITION_EDGE_DEFAULT_ROUTER = "BATransition Edge Default Router";

}
