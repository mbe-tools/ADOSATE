/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.behaviorAnnex;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.requests.CreationFactory;
import org.topcased.adele.model.ba_components.Ba_componentsPackage;
import org.topcased.adele.model.ba_features.Ba_featuresPackage;
import org.topcased.adele.model.ba_relations.Ba_relationsPackage;
import org.topcased.modeler.editor.GraphElementCreationFactory;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.palette.ModelerConnectionCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerPaletteManager;

/**
 * Generated Palette Manager
 *
 * @generated
 */
public class BehaviorAnnexPaletteManager extends ModelerPaletteManager {
	// declare all the palette categories of the diagram
	/**
	 * @generated
	 */
	private PaletteDrawer componentsDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer featuresDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer connectionsDrawer;

	/**
	 * @generated
	 */
	private ICreationUtils creationUtils;

	/**
	 * The Constructor
	 *
	 * @param utils the creation utils for the tools of the palette 
	 * @generated
	 */
	public BehaviorAnnexPaletteManager(ICreationUtils utils) {
		super();
		this.creationUtils = utils;
	}

	/**
	 * Creates the main categories of the palette
	 *
	 * @generated
	 */
	protected void createCategories() {
		createComponentsDrawer();
		createFeaturesDrawer();
		createConnectionsDrawer();
	}

	/**
	 * Updates the main categories of the palette
	 *
	 * @generated
	 */
	protected void updateCategories() {
		// deletion of the existing categories and creation of the updated categories

		getRoot().remove(componentsDrawer);
		createComponentsDrawer();

		getRoot().remove(featuresDrawer);
		createFeaturesDrawer();

		getRoot().remove(connectionsDrawer);
		createConnectionsDrawer();
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated
	 */
	private void createComponentsDrawer() {
		componentsDrawer = new PaletteDrawer("Components", null);
		List<PaletteEntry> entries = new ArrayList<PaletteEntry>();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils,
				Ba_componentsPackage.eINSTANCE.getBAState(), "default");
		entries.add(new ModelerCreationToolEntry("State", "State", factory,
				BehaviorAnnexImageRegistry.getImageDescriptor("BASTATE"),
				BehaviorAnnexImageRegistry.getImageDescriptor("BASTATE_LARGE")));

		componentsDrawer.addAll(entries);
		getRoot().add(componentsDrawer);
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated
	 */
	private void createFeaturesDrawer() {
		featuresDrawer = new PaletteDrawer("Features", null);
		List<PaletteEntry> entries = new ArrayList<PaletteEntry>();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils,
				Ba_featuresPackage.eINSTANCE.getBAVariable(), "default");
		entries.add(new ModelerCreationToolEntry("Variable", "Variable",
				factory, BehaviorAnnexImageRegistry
						.getImageDescriptor("BAVARIABLE"),
				BehaviorAnnexImageRegistry
						.getImageDescriptor("BAVARIABLE_LARGE")));

		featuresDrawer.addAll(entries);
		getRoot().add(featuresDrawer);
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated
	 */
	private void createConnectionsDrawer() {
		connectionsDrawer = new PaletteDrawer("Connections", null);
		List<PaletteEntry> entries = new ArrayList<PaletteEntry>();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils,
				Ba_relationsPackage.eINSTANCE.getBATransition(), "default");
		entries.add(new ModelerConnectionCreationToolEntry("Transition",
				"Transition", factory, BehaviorAnnexImageRegistry
						.getImageDescriptor("BATRANSITION"),
				BehaviorAnnexImageRegistry
						.getImageDescriptor("BATRANSITION_LARGE")));

		connectionsDrawer.addAll(entries);
		getRoot().add(connectionsDrawer);
	}

}
