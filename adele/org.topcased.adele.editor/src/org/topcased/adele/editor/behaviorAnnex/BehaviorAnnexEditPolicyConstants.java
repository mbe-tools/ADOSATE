/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.behaviorAnnex;

/**
 * A collection of Roles. Each identifier is used to key the EditPolicy.
 *
 * @generated
 */
public interface BehaviorAnnexEditPolicyConstants {

	/**
	 * The key used to install an <i>BAState</i> EditPolicy.
	 * @generated
	 */
	String BASTATE_EDITPOLICY = "BAState EditPolicy";

	/**
	 * The key used to install an <i>BAVariable</i> EditPolicy.
	 * @generated
	 */
	String BAVARIABLE_EDITPOLICY = "BAVariable EditPolicy";

	/**
	 * The key used to install an <i>BATransition</i> EditPolicy.
	 * @generated
	 */
	String BATRANSITION_EDITPOLICY = "BATransition EditPolicy";

}