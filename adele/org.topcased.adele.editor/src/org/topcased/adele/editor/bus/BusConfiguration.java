/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.bus;

import java.net.URL;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.gef.EditPartFactory;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.adele.editor.bus.edit.AbstractEditPart;
import org.topcased.adele.editor.bus.edit.AbstractFeatureEditPart;
import org.topcased.adele.editor.bus.edit.BusAccessEditPart;
import org.topcased.adele.editor.bus.edit.BusEditPart;
import org.topcased.adele.editor.bus.edit.FeatureGroupEditPart;
import org.topcased.adele.editor.bus.edit.VirtualBusEditPart;
import org.topcased.modeler.editor.IConfiguration;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.IPaletteManager;
import org.topcased.modeler.graphconf.DiagramGraphConf;
import org.topcased.modeler.graphconf.exceptions.MissingGraphConfFileException;

/**
 * A diagram configuration : manages Palette, EditPartFactory for this diagram.
 *
 * @generated
 */
public class BusConfiguration implements IConfiguration {
	/**
	 * @generated
	 */
	private BusPaletteManager paletteManager;

	/**
	 * @generated
	 */
	private BusEditPartFactory editPartFactory;

	/**
	 * @generated
	 */
	private BusCreationUtils creationUtils;

	/**
	 * The DiagramGraphConf that contains graphical informations on the configuration
	 * @generated
	 */
	private DiagramGraphConf diagramGraphConf;

	/**
	 * Constructor. Initialize Adapter factories.
	 *
	 * @generated
	 */
	public BusConfiguration() {
		registerAdapters();
	}

	/**
	 * Registers the Adapter Factories for all the EditParts
	 *
	 * @generated
	 */
	private void registerAdapters() {
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								VirtualBusEditPart.class,
								org.topcased.adele.model.ADELE_Components.VirtualBus.class),
						VirtualBusEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								AbstractEditPart.class,
								org.topcased.adele.model.ADELE_Components.Abstract.class),
						AbstractEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								BusAccessEditPart.class,
								org.topcased.adele.model.ADELE_Features.BusAccess.class),
						BusAccessEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								FeatureGroupEditPart.class,
								org.topcased.adele.model.ADELE_Features.FeatureGroup.class),
						FeatureGroupEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								AbstractFeatureEditPart.class,
								org.topcased.adele.model.ADELE_Features.AbstractFeature.class),
						AbstractFeatureEditPart.class);
		Platform.getAdapterManager().registerAdapters(
				new EditPart2ModelAdapterFactory(BusEditPart.class,
						org.topcased.adele.model.ADELE_Components.Bus.class),
				BusEditPart.class);
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getId()
	 * @generated
	 */
	public String getId() {
		return new String("org.topcased.adele.editor.bus");
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getName()
	 * @generated
	 */
	public String getName() {
		return new String("ADELE Bus");
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getEditPartFactory()
	 * @generated
	 */
	public EditPartFactory getEditPartFactory() {
		if (editPartFactory == null) {
			editPartFactory = new BusEditPartFactory();
		}

		return editPartFactory;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getPaletteManager()
	 * @generated
	 */
	public IPaletteManager getPaletteManager() {
		if (paletteManager == null) {
			paletteManager = new BusPaletteManager(getCreationUtils());
		}

		return paletteManager;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getCreationUtils()
	 * @generated
	 */
	public ICreationUtils getCreationUtils() {
		if (creationUtils == null) {
			creationUtils = new BusCreationUtils(getDiagramGraphConf());
		}

		return creationUtils;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getDiagramGraphConf()
	 * @generated
	 */
	public DiagramGraphConf getDiagramGraphConf() {
		if (diagramGraphConf == null) {
			URL url = ADELEPlugin
					.getDefault()
					.getBundle()
					.getResource(
							"org/topcased/adele/editor/bus/diagram.graphconf");
			if (url != null) {
				URI fileURI = URI.createURI(url.toString());
				ResourceSet resourceSet = new ResourceSetImpl();
				Resource resource = resourceSet.getResource(fileURI, true);
				if (resource != null
						&& resource.getContents().get(0) instanceof DiagramGraphConf) {
					diagramGraphConf = (DiagramGraphConf) resource
							.getContents().get(0);
				}
			} else {
				new MissingGraphConfFileException(
						"The *.diagramgraphconf file can not be retrieved. Check if the path is correct in the Configuration class of your diagram.");
			}
		}

		return diagramGraphConf;
	}

}