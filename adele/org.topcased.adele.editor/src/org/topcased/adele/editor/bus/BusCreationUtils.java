/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.bus;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch;
import org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch;
import org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch;
import org.topcased.adele.model.KernelSpices.util.KernelSpicesSwitch;
import org.topcased.adele.model.ObjectDescriptionModel.util.ObjectDescriptionModelSwitch;
import org.topcased.adele.model.ba_components.util.Ba_componentsSwitch;
import org.topcased.adele.model.ba_features.util.Ba_featuresSwitch;
import org.topcased.adele.model.ba_relations.util.Ba_relationsSwitch;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.editor.AbstractCreationUtils;
import org.topcased.modeler.graphconf.DiagramGraphConf;

/**
 * This utility class allows to create a GraphElement associated with a Model Object
 *
 * @generated
 */
public class BusCreationUtils extends AbstractCreationUtils {

	/**
	 * Constructor
	 *
	 * @param diagramConf the Diagram Graphical Configuration
	 * @generated
	 */
	public BusCreationUtils(DiagramGraphConf diagramConf) {
		super(diagramConf);
	}

	/**
	 * @generated
	 */
	private class GraphicADELE_ComponentsSwitch extends ADELE_ComponentsSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicADELE_ComponentsSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseVirtualBus(org.topcased.adele.model.ADELE_Components.VirtualBus)
		 * @generated
		 */
		public Object caseVirtualBus(
				org.topcased.adele.model.ADELE_Components.VirtualBus object) {
			if ("default".equals(presentation)) {
				return createGraphElementVirtualBus(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseAbstract(org.topcased.adele.model.ADELE_Components.Abstract)
		 * @generated
		 */
		public Object caseAbstract(
				org.topcased.adele.model.ADELE_Components.Abstract object) {
			if ("default".equals(presentation)) {
				return createGraphElementAbstract(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicADELE_FeaturesSwitch extends ADELE_FeaturesSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicADELE_FeaturesSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseBusAccess(org.topcased.adele.model.ADELE_Features.BusAccess)
		 * @generated
		 */
		public Object caseBusAccess(
				org.topcased.adele.model.ADELE_Features.BusAccess object) {
			if ("default".equals(presentation)) {
				return createGraphElementBusAccess(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseFeatureGroup(org.topcased.adele.model.ADELE_Features.FeatureGroup)
		 * @generated
		 */
		public Object caseFeatureGroup(
				org.topcased.adele.model.ADELE_Features.FeatureGroup object) {
			if ("default".equals(presentation)) {
				return createGraphElementFeatureGroup(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseAbstractFeature(org.topcased.adele.model.ADELE_Features.AbstractFeature)
		 * @generated
		 */
		public Object caseAbstractFeature(
				org.topcased.adele.model.ADELE_Features.AbstractFeature object) {
			if ("default".equals(presentation)) {
				return createGraphElementAbstractFeature(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicADELE_RelationsSwitch extends ADELE_RelationsSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicADELE_RelationsSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicBa_componentsSwitch extends Ba_componentsSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicBa_componentsSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ba_components.util.Ba_componentsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicBa_featuresSwitch extends Ba_featuresSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicBa_featuresSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ba_features.util.Ba_featuresSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicBa_relationsSwitch extends Ba_relationsSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicBa_relationsSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ba_relations.util.Ba_relationsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicKernelSpicesSwitch extends KernelSpicesSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicKernelSpicesSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.KernelSpices.util.KernelSpicesSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicObjectDescriptionModelSwitch extends
			ObjectDescriptionModelSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicObjectDescriptionModelSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ObjectDescriptionModel.util.ObjectDescriptionModelSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @see org.topcased.modeler.editor.ICreationUtils#createGraphElement(org.eclipse.emf.ecore.EObject, java.lang.String)
	 * @generated
	 */
	public GraphElement createGraphElement(EObject obj, String presentation) {
		Object graphElt = null;

		if ("http://ADELE_Components".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicADELE_ComponentsSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ADELE_Features".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicADELE_FeaturesSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ADELE_Relations".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicADELE_RelationsSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ba_components/1.0".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicBa_componentsSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ba_features/1.0".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicBa_featuresSwitch(presentation).doSwitch(obj);
		}
		if ("http://ba_relations/1.0".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicBa_relationsSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://KernelSpices".equals(obj.eClass().getEPackage().getNsURI())) {
			graphElt = new GraphicKernelSpicesSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ObjectDescriptionModel".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicObjectDescriptionModelSwitch(presentation)
					.doSwitch(obj);
		}

		return (GraphElement) graphElt;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementVirtualBus(
			org.topcased.adele.model.ADELE_Components.VirtualBus element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementAbstract(
			org.topcased.adele.model.ADELE_Components.Abstract element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementBusAccess(
			org.topcased.adele.model.ADELE_Features.BusAccess element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementFeatureGroup(
			org.topcased.adele.model.ADELE_Features.FeatureGroup element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementAbstractFeature(
			org.topcased.adele.model.ADELE_Features.AbstractFeature element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * Create the ModelObject with its initial children
	 * 
	 * @param obj the model object
	 * @return the model object with its children
	 * @generated
	 */
	public EObject createModelObject(EObject obj) {
		return obj;
	}

	/**
	 * Get the preference store associated with the current editor.
	 * 
	 * @return IPreferenceStore
	 * @generated
	 */
	private IPreferenceStore getPreferenceStore() {
		IEditorInput editorInput = ADELEPlugin.getActivePage()
				.getActiveEditor().getEditorInput();
		if (editorInput instanceof IFileEditorInput) {
			IProject project = ((IFileEditorInput) editorInput).getFile()
					.getProject();
			Preferences root = Platform.getPreferencesService().getRootNode();
			try {
				if (root.node(ProjectScope.SCOPE).node(project.getName())
						.nodeExists(ADELEPlugin.getId())) {
					return new ScopedPreferenceStore(new ProjectScope(project),
							ADELEPlugin.getId());
				}
			} catch (BackingStoreException e) {
				e.printStackTrace();
			}
		}
		return ADELEPlugin.getDefault().getPreferenceStore();
	}
}
