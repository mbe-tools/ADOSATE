/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.bus;

/**
 * A collection of Roles. Each identifier is used to key the EditPolicy.
 *
 * @generated
 */
public interface BusEditPolicyConstants {

	/**
	 * The key used to install an <i>VirtualBus</i> EditPolicy.
	 * @generated
	 */
	String VIRTUALBUS_EDITPOLICY = "VirtualBus EditPolicy";

	/**
	 * The key used to install an <i>Abstract</i> EditPolicy.
	 * @generated
	 */
	String ABSTRACT_EDITPOLICY = "Abstract EditPolicy";

	/**
	 * The key used to install an <i>BusAccess</i> EditPolicy.
	 * @generated
	 */
	String BUSACCESS_EDITPOLICY = "BusAccess EditPolicy";

	/**
	 * The key used to install an <i>FeatureGroup</i> EditPolicy.
	 * @generated
	 */
	String FEATUREGROUP_EDITPOLICY = "FeatureGroup EditPolicy";

	/**
	 * The key used to install an <i>AbstractFeature</i> EditPolicy.
	 * @generated
	 */
	String ABSTRACTFEATURE_EDITPOLICY = "AbstractFeature EditPolicy";

	/**
	 * The key used to install an <i>Bus</i> EditPolicy.
	 * @generated
	 */
	String BUS_EDITPOLICY = "Bus EditPolicy";

}