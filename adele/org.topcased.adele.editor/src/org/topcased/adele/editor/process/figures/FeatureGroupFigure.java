/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.process.figures;


/**
 * @generated NOT
 */
public class FeatureGroupFigure extends org.topcased.adele.editor.utils.figures.port.FeatureGroupFigure {
	/**
	 * Constructor
	 *
	 * @generated
	 */
	public FeatureGroupFigure() {
		super();
	}
//
//	/**
//	 * @see org.topcased.draw2d.figures.ContainerFigure#createContainer()
//	 * @generated
//	 */
//	protected IFigure createContainer() {
//		IFigure container = super.createContainer();
//		container.setLayoutManager(new BorderAttachedLayout());
//		return container;
//	}

}