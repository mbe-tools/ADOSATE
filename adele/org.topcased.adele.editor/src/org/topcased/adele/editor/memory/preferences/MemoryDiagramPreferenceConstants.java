/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.memory.preferences;

/**
 * A set of constant that are used in the diagram graphical preferences
 * 
 * @generated
 */
public interface MemoryDiagramPreferenceConstants {
	/**
	 * The key used to install a <i>Memory Default Background Color</i> Preference.
	 * @generated
	 */
	String MEMORY_DEFAULT_BACKGROUND_COLOR = "Memory Default Background Color";

	/**
	 * The key used to install a <i>Memory Default Foreground Color</i> Preference.
	 * @generated
	 */
	String MEMORY_DEFAULT_FOREGROUND_COLOR = "Memory Default Foreground Color";

	/**
	 * The key used to install a <i>Memory Default Font</i> Preference.
	 * @generated
	 */
	String MEMORY_DEFAULT_FONT = "Memory Default Font";

	/**
	 * The key used to install a <i>Bus Default Background Color</i> Preference.
	 * @generated
	 */
	String BUS_DEFAULT_BACKGROUND_COLOR = "Bus Default Background Color";

	/**
	 * The key used to install a <i>Bus Default Foreground Color</i> Preference.
	 * @generated
	 */
	String BUS_DEFAULT_FOREGROUND_COLOR = "Bus Default Foreground Color";

	/**
	 * The key used to install a <i>Bus Default Font</i> Preference.
	 * @generated
	 */
	String BUS_DEFAULT_FONT = "Bus Default Font";

	/**
	 * The key used to install a <i>Abstract Default Background Color</i> Preference.
	 * @generated
	 */
	String ABSTRACT_DEFAULT_BACKGROUND_COLOR = "Abstract Default Background Color";

	/**
	 * The key used to install a <i>Abstract Default Foreground Color</i> Preference.
	 * @generated
	 */
	String ABSTRACT_DEFAULT_FOREGROUND_COLOR = "Abstract Default Foreground Color";

	/**
	 * The key used to install a <i>Abstract Default Font</i> Preference.
	 * @generated
	 */
	String ABSTRACT_DEFAULT_FONT = "Abstract Default Font";

	/**
	 * The key used to install a <i>BusAccess Default Background Color</i> Preference.
	 * @generated
	 */
	String BUSACCESS_DEFAULT_BACKGROUND_COLOR = "BusAccess Default Background Color";

	/**
	 * The key used to install a <i>BusAccess Default Foreground Color</i> Preference.
	 * @generated
	 */
	String BUSACCESS_DEFAULT_FOREGROUND_COLOR = "BusAccess Default Foreground Color";

	/**
	 * The key used to install a <i>BusAccess Default Font</i> Preference.
	 * @generated
	 */
	String BUSACCESS_DEFAULT_FONT = "BusAccess Default Font";

	/**
	 * The key used to install a <i>FeatureGroup Default Background Color</i> Preference.
	 * @generated
	 */
	String FEATUREGROUP_DEFAULT_BACKGROUND_COLOR = "FeatureGroup Default Background Color";

	/**
	 * The key used to install a <i>FeatureGroup Default Foreground Color</i> Preference.
	 * @generated
	 */
	String FEATUREGROUP_DEFAULT_FOREGROUND_COLOR = "FeatureGroup Default Foreground Color";

	/**
	 * The key used to install a <i>FeatureGroup Default Font</i> Preference.
	 * @generated
	 */
	String FEATUREGROUP_DEFAULT_FONT = "FeatureGroup Default Font";

	/**
	 * The key used to install a <i>AbstractFeature Default Background Color</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR = "AbstractFeature Default Background Color";

	/**
	 * The key used to install a <i>AbstractFeature Default Foreground Color</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR = "AbstractFeature Default Foreground Color";

	/**
	 * The key used to install a <i>AbstractFeature Default Font</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURE_DEFAULT_FONT = "AbstractFeature Default Font";

	/**
	 * The key used to install a <i>BusAccessConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String BUSACCESSCONNECTION_EDGE_DEFAULT_FONT = "BusAccessConnection Edge Default Font";

	/**
	 * The key used to install a <i>BusAccessConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String BUSACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "BusAccessConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>BusAccessConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String BUSACCESSCONNECTION_EDGE_DEFAULT_ROUTER = "BusAccessConnection Edge Default Router";

	/**
	 * The key used to install a <i>AbstractFeatureConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FONT = "AbstractFeatureConnection Edge Default Font";

	/**
	 * The key used to install a <i>AbstractFeatureConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "AbstractFeatureConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>AbstractFeatureConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_ROUTER = "AbstractFeatureConnection Edge Default Router";

	/**
	 * The key used to install a <i>FeatureGroupConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String FEATUREGROUPCONNECTION_EDGE_DEFAULT_FONT = "FeatureGroupConnection Edge Default Font";

	/**
	 * The key used to install a <i>FeatureGroupConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String FEATUREGROUPCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "FeatureGroupConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>FeatureGroupConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String FEATUREGROUPCONNECTION_EDGE_DEFAULT_ROUTER = "FeatureGroupConnection Edge Default Router";

}