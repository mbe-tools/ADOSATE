/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.memory;

/**
 * A Set of properties that are used for the graphical objects that are not
 * associated with a model object. Each name is used as the typeInfo attribute
 * in the DI file.
 * 
 * @generated
 */
public interface MemorySimpleObjectConstants {
}