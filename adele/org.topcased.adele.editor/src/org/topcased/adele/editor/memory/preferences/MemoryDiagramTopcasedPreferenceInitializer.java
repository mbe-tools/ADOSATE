/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.memory.preferences;

import java.util.HashMap;

import org.eclipse.jface.resource.StringConverter;
import org.topcased.modeler.preferences.ITopcasedPreferenceInitializer;

/**
 * Initialize the preferences of Memory diagram
 * 
 * @generated
 */
public class MemoryDiagramTopcasedPreferenceInitializer implements
		ITopcasedPreferenceInitializer {

	/** 
	 * @see org.topcased.modeler.preferences.ITopcasedPreferenceInitializer#getDefaultPreference()
	 *	@generated
	 */
	public HashMap<String, String> getDefaultPreference() {
		HashMap<String, String> defaultMemoryPreference = new HashMap<String, String>();
		// Initialize the default value of the MEMORY_DEFAULT_BACKGROUND_COLOR property 
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.MEMORY_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the MEMORY_DEFAULT_FOREGROUND_COLOR property
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.MEMORY_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the MEMORY_DEFAULT_FONT property
		defaultMemoryPreference.put(
				MemoryDiagramPreferenceConstants.MEMORY_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the BUS_DEFAULT_BACKGROUND_COLOR property 
		defaultMemoryPreference.put(
				MemoryDiagramPreferenceConstants.BUS_DEFAULT_BACKGROUND_COLOR,
				"255,255,255");

		// Initialize the default value of the BUS_DEFAULT_FOREGROUND_COLOR property
		defaultMemoryPreference.put(
				MemoryDiagramPreferenceConstants.BUS_DEFAULT_FOREGROUND_COLOR,
				"0,0,0");

		// Initialize the default value of the BUS_DEFAULT_FONT property
		defaultMemoryPreference.put(
				MemoryDiagramPreferenceConstants.BUS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the ABSTRACT_DEFAULT_BACKGROUND_COLOR property 
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.ABSTRACT_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the ABSTRACT_DEFAULT_FOREGROUND_COLOR property
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.ABSTRACT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACT_DEFAULT_FONT property
		defaultMemoryPreference.put(
				MemoryDiagramPreferenceConstants.ABSTRACT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the BUSACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.BUSACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BUSACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.BUSACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BUSACCESS_DEFAULT_FONT property
		defaultMemoryPreference.put(
				MemoryDiagramPreferenceConstants.BUSACCESS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the FEATUREGROUP_DEFAULT_BACKGROUND_COLOR property 
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the FEATUREGROUP_DEFAULT_FOREGROUND_COLOR property
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the FEATUREGROUP_DEFAULT_FONT property
		defaultMemoryPreference.put(
				MemoryDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR property 
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR property
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_FONT property
		defaultMemoryPreference.put(
				MemoryDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the BUSACCESSCONNECTION_EDGE_DEFAULT_FONT property
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.BUSACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the BUSACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.BUSACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BUSACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.BUSACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FONT property
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_ROUTER property
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_FONT property
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultMemoryPreference
				.put(MemoryDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		return defaultMemoryPreference;
	}
}
