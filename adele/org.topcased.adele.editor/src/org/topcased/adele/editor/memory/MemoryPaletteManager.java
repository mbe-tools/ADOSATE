/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.memory;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.palette.CreationToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteStack;
import org.eclipse.gef.requests.CreationFactory;
import org.eclipse.ui.IWorkbenchPage;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.adele.editor.editor.ADELEEditor;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;
import org.topcased.adele.model.ADELE_Features.AbstractFeature;
import org.topcased.adele.model.ADELE_Features.AccessDirection;
import org.topcased.adele.model.ADELE_Features.BusAccess;
import org.topcased.adele.model.ADELE_Relations.ADELE_RelationsPackage;
import org.topcased.modeler.editor.GraphElementCreationFactory;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.palette.ModelerConnectionCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerPaletteManager;

/**
 * Generated Palette Manager
 *
 * @generated
 */
public class MemoryPaletteManager extends ModelerPaletteManager {
	// declare all the palette categories of the diagram
	/**
	 * @generated
	 */
	private PaletteDrawer componentsDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer featuresDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer connectionsDrawer;

	/**
	 * @generated
	 */
	private ICreationUtils creationUtils;

	/**
	 * The Constructor
	 *
	 * @param utils the creation utils for the tools of the palette 
	 * @generated
	 */
	public MemoryPaletteManager(ICreationUtils utils) {
		super();
		this.creationUtils = utils;
	}

	/**
	 * Creates the main categories of the palette
	 *
	 * @generated
	 */
	protected void createCategories() {
		createComponentsDrawer();
		createFeaturesDrawer();
		createConnectionsDrawer();
	}

	/**
	 * Updates the main categories of the palette
	 *
	 * @generated
	 */
	protected void updateCategories() {
		// deletion of the existing categories and creation of the updated categories

		getRoot().remove(componentsDrawer);
		createComponentsDrawer();

		getRoot().remove(featuresDrawer);
		createFeaturesDrawer();

		getRoot().remove(connectionsDrawer);
		createConnectionsDrawer();
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated NOT
	 */
	private void createComponentsDrawer() {
		componentsDrawer = new PaletteDrawer("Components", null);
		List<PaletteEntry> entries = new ArrayList<PaletteEntry>();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getMemory(), "default");
		entries.add(new ModelerCreationToolEntry("Memory", "Memory", factory,
				MemoryImageRegistry.getImageDescriptor("MEMORY"),
				MemoryImageRegistry.getImageDescriptor("MEMORY_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getBus(), "default");
		entries.add(new ModelerCreationToolEntry("Bus", "Bus", factory,
				MemoryImageRegistry.getImageDescriptor("BUS"),
				MemoryImageRegistry.getImageDescriptor("BUS_LARGE")));

		IWorkbenchPage wPage = ADELEPlugin.getActivePage();
		if (wPage != null && wPage.getActiveEditor() instanceof ADELEEditor ) {
			ADELEEditor editor = (ADELEEditor) wPage.getActiveEditor();
			if (editor != null && editor.getSelectedObject() != null
					&& editor.getSelectedObject().getName().contains(".")) {
				factory = new GraphElementCreationFactory(creationUtils,
						ADELE_ComponentsPackage.eINSTANCE.getAbstract(),
						"default");
				entries.add(new ModelerCreationToolEntry("Abstract",
						"Abstract", factory, MemoryImageRegistry
								.getImageDescriptor("ABSTRACT"),
						MemoryImageRegistry
								.getImageDescriptor("ABSTRACT_LARGE")));
			}
		}

		componentsDrawer.addAll(entries);
		getRoot().add(componentsDrawer);
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated NOT
	 */
	private void createFeaturesDrawer() {
		if ( ADELEPlugin.isSystemActiveEditor() ) {
			featuresDrawer = new PaletteDrawer("Features", null);
			List<PaletteEntry> entries = new ArrayList<PaletteEntry>();
	
			CreationFactory factory;
			String newObjTxt;
			CreationToolEntry objectTool;
	
			// Feature Group
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getFeatureGroup(), "default");
			entries.add(new ModelerCreationToolEntry("Feature Group",
					"Feature Group", factory, MemoryImageRegistry
							.getImageDescriptor("FEATUREGROUP"),
					MemoryImageRegistry.getImageDescriptor("FEATUREGROUP_LARGE")));
	
			// Bus Access
	
			PaletteStack busAccessStack = new PaletteStack("Bus Access",
					"Bus Access Selection",
					MemoryImageRegistry.getImageDescriptor("BusAccess"));
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getBusAccess(), "default") {
				public EObject getNewModelObject() {
					BusAccess access = (BusAccess) super.getNewModelObject();
					access.setDirection(AccessDirection.PROVIDED_LITERAL);
					return access;
				}
			};
			newObjTxt = "Provides bus access";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					MemoryImageRegistry.getImageDescriptor("PROVIDEDBUSACCESS"),
					MemoryImageRegistry
							.getImageDescriptor("PROVIDEDBUSACCESS_LARGE"));
			busAccessStack.add(objectTool);
			busAccessStack.setActiveEntry(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getBusAccess(), "default") {
				public EObject getNewModelObject() {
					BusAccess access = (BusAccess) super.getNewModelObject();
					access.setDirection(AccessDirection.REQUIRED_LITERAL);
					return access;
				}
			};
			newObjTxt = "Requires bus access";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					MemoryImageRegistry.getImageDescriptor("REQUIREDBUSACCESS"),
					MemoryImageRegistry
							.getImageDescriptor("REQUIREDBUSACCESS_LARGE"));
			busAccessStack.add(objectTool);
	
			entries.add(busAccessStack);
	
			// Abstract Feature
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getAbstractFeature(), "default") {
				public EObject getNewModelObject() {
					AbstractFeature port = (AbstractFeature) super
							.getNewModelObject();
					//port.setDirection(AbstractFeatureDirection.NONE);
					return port;
				}
			};
			newObjTxt = "Feature";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					MemoryImageRegistry.getImageDescriptor("ABSTRACTFEATURE"),
					MemoryImageRegistry.getImageDescriptor("ABSTRACTFEATURE_LARGE"));
	
			entries.add(objectTool);
	
			featuresDrawer.addAll(entries);
			getRoot().add(featuresDrawer);
		}
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated
	 */
	private void createConnectionsDrawer() {
		connectionsDrawer = new PaletteDrawer("Connections", null);
		List<PaletteEntry> entries = new ArrayList<PaletteEntry>();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getBusAccessConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Bus access connection", "Bus access connection", factory,
				MemoryImageRegistry.getImageDescriptor("BUSACCESSCONNECTION"),
				MemoryImageRegistry
						.getImageDescriptor("BUSACCESSCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(
				creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getAbstractFeatureConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Feature Connection", "Feature Connection", factory,
				MemoryImageRegistry
						.getImageDescriptor("ABSTRACTFEATURECONNECTION"),
				MemoryImageRegistry
						.getImageDescriptor("ABSTRACTFEATURECONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getFeatureGroupConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Feature Group Connection", "Feature Group Connection",
				factory, MemoryImageRegistry
						.getImageDescriptor("FEATUREGROUPCONNECTION"),
				MemoryImageRegistry
						.getImageDescriptor("FEATUREGROUPCONNECTION_LARGE")));

		connectionsDrawer.addAll(entries);
		getRoot().add(connectionsDrawer);
	}

}
