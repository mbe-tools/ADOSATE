/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *  	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.facilities.preferences.AbstractTopcasedPreferencePage;
import org.topcased.modeler.preferences.ModelerPreferenceConstants;

/**
 * A preference page to configure all preferences of the ADELE editor.
 *
 * @generated
 */
public class ADELEPreferencePage extends AbstractTopcasedPreferencePage {
	/**
	 * @generated
	 */
	private BooleanFieldEditor deleteGraphElements;

	/**
	 * @generated
	 */
	private BooleanFieldEditor deleteModelElements;

	/**
	 * @generated
	 */
	private BooleanFieldEditor moveModelElements;

	private FileFieldEditor databasePath;

	private DirectoryFieldEditor lmpPath;

	private ComboFieldEditor aadlkwCase;

	private ComboFieldEditor aadlidCase;

	/**
	 * Creates this preference page contents.
	 *
	 * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
	 * @generated NOT
	 */
	protected Control createContents(Composite parent) {
		// Create the container composite
		Composite container = new Composite(parent, SWT.NONE);
		GridLayout containerLayout = new GridLayout();
		containerLayout.marginWidth = 0;
		containerLayout.marginHeight = 0;
		container.setLayout(containerLayout);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		createHeader(container);

		createDeleteActionsGroup(container);

		createMoveActionGroup(container);

		createLMPActionsGroup(container);

		createOdsActionsGroup(container);

		loadPreferences();

		return container;
	}

	/**
	 * Initializes the HMI with preference values.
	 *
	 * @generated NOT
	 */
	private void loadPreferences() {
		deleteGraphElements.load();
		deleteModelElements.load();

		moveModelElements.load();

		databasePath.load();
		lmpPath.load();
		aadlkwCase.load();
		aadlidCase.load();
	}

	/**
	 * Stores the HMI values into the preference store.
	 *
	 * @generated NOT
	 */
	private void storePreferences() {
		deleteGraphElements.store();
		deleteModelElements.store();

		moveModelElements.store();

		databasePath.store();
		lmpPath.store();
		aadlkwCase.store();
		aadlidCase.store();
	}

	/**
	 * Initializes the HMI with default preference values.
	 *
	 * @generated NOT
	 */
	private void loadDefaultPreferences() {
		deleteGraphElements.loadDefault();
		deleteModelElements.loadDefault();

		moveModelElements.loadDefault();

		databasePath.loadDefault();
		lmpPath.loadDefault();
		aadlkwCase.loadDefault();
		aadlidCase.loadDefault();
	}

	/**
	 * Creates this preference page header.
	 *
	 * @param parent the parent composite
	 * @generated
	 */
	private void createHeader(Composite parent) {
		new Label(parent, SWT.WRAP).setText("ADELE editor preference page");
	}

	/**
	 * Creates the group to configure the delete actions preferences.
	 * 
	 * @param parent the parent composite
	 * @generated
	 */
	private void createDeleteActionsGroup(Composite parent) {
		Group group = new Group(parent, SWT.SHADOW_ETCHED_OUT);
		group.setLayout(new GridLayout());
		group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		group.setText("Delete actions");

		Composite fieldsContainer = new Composite(group, SWT.NONE);
		fieldsContainer.setLayout(new GridLayout());
		fieldsContainer.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		deleteGraphElements = new BooleanFieldEditor(
				ModelerPreferenceConstants.DELETE_ACTION_CONFIRM,
				"Do not ask for confirmation before deleting graphical elements.",
				fieldsContainer);
		deleteGraphElements.setPreferenceStore(getPreferenceStore());

		deleteModelElements = new BooleanFieldEditor(
				ModelerPreferenceConstants.DELETE_MODEL_ACTION_CONFIRM,
				"Do not ask for confirmation before deleting model elements.",
				fieldsContainer);
		deleteModelElements.setPreferenceStore(getPreferenceStore());
	}

	/**
	 * Creates the group to configure the move actions preferences.
	 * 
	 * @param parent the parent composite
	 * @generated
	 */
	private void createMoveActionGroup(Composite parent) {
		Group groupMove = new Group(parent, SWT.SHADOW_ETCHED_OUT);
		groupMove.setLayout(new GridLayout());
		groupMove.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		groupMove.setText("Move actions");

		Composite fieldsContainerMove = new Composite(groupMove, SWT.NONE);
		fieldsContainerMove.setLayout(new GridLayout());
		fieldsContainerMove
				.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		moveModelElements = new BooleanFieldEditor(
				ModelerPreferenceConstants.MOVE_MODEL_ACTION_CONFIRM,
				"Do not ask for confirmation before moving model elements.",
				fieldsContainerMove);
		moveModelElements.setPreferenceStore(getPreferenceStore());
	}

	/**
	 * Creates the group to configure the LMP definition preferences.
	 * 
	 * @param parent the parent composite
	 * @generated NOT
	 */
	private void createLMPActionsGroup(Composite parent) {
		Group group = new Group(parent, SWT.SHADOW_ETCHED_OUT);
		group.setLayout(new GridLayout());
		group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		group.setText("LMP preferences");

		Composite fieldsContainer = new Composite(group, SWT.NONE);
		fieldsContainer.setLayout(new GridLayout());
		fieldsContainer.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		lmpPath = new DirectoryFieldEditor(
				ADELEPreferenceConstants.ADELE_LMP_PATH,
				"&Choose LMP directory:", fieldsContainer);
		lmpPath.setPreferenceStore(getPreferenceStore());

		Composite comboFieldsContainer1 = new Composite(group, SWT.NONE);
		comboFieldsContainer1.setLayout(new GridLayout());
		comboFieldsContainer1.setLayoutData(new GridData(
				GridData.FILL_HORIZONTAL));

		aadlkwCase = new ComboFieldEditor(
				ADELEPreferenceConstants.ADELE_AADLKW_CASE,
				"&Choose AADL keyword case for generation:",
				ADELEPreferenceConstants.ADELE_AADLKW_CASES,
				comboFieldsContainer1);
		aadlkwCase.setPreferenceStore(getPreferenceStore());

		Composite comboFieldsContainer2 = new Composite(group, SWT.NONE);
		comboFieldsContainer2.setLayout(new GridLayout());
		comboFieldsContainer2.setLayoutData(new GridData(
				GridData.FILL_HORIZONTAL));

		aadlidCase = new ComboFieldEditor(
				ADELEPreferenceConstants.ADELE_AADLID_CASE,
				"&Choose AADL component ID case for generation:",
				ADELEPreferenceConstants.ADELE_AADLID_CASES,
				comboFieldsContainer2);
		aadlidCase.setPreferenceStore(getPreferenceStore());
	}

	/**
	 * Creates the group to configure the ODS definition preferences.
	 * 
	 * @param parent the parent composite
	 * @generated NOT
	 */
	private void createOdsActionsGroup(Composite parent) {
		Group group = new Group(parent, SWT.SHADOW_ETCHED_OUT);
		group.setLayout(new GridLayout());
		group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		group.setText("ODS description location");

		Composite fieldsContainer = new Composite(group, SWT.NONE);
		fieldsContainer.setLayout(new GridLayout());
		fieldsContainer.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		databasePath = new FileFieldEditor(
				ADELEPreferenceConstants.ADELE_DATABASE_PATH,
				"&Choose database file:", fieldsContainer);
		String[] dbExtension = { "*.odsconfig", "*.xml" };
		databasePath.setFileExtensions(dbExtension);
		databasePath.setPreferenceStore(getPreferenceStore());
	}

	/**
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 * @generated
	 */
	public void init(IWorkbench workbench) {
		// Do nothing
	}

	/**
	 * @see org.eclipse.jface.preference.IPreferencePage#performOk()
	 * @generated
	 */
	public boolean performOk() {
		storePreferences();
		return super.performOk();
	}

	/**
	 * @see org.eclipse.jface.preference.PreferencePage#performDefaults()
	 * @generated
	 */
	protected void performDefaults() {
		loadDefaultPreferences();
		super.performDefaults();
	}

	/**
	 * @see org.topcased.facilities.preferences.AbstractTopcasedPreferencePage#getBundleId()
	 * @generated
	 */
	protected String getBundleId() {
		return ADELEPlugin.getId();
	}
}
