package org.topcased.adele.editor.preferences;

import org.topcased.adele.common.preferences.AdeleCommonPreferenceConstants;

/**
 * Constant definitions for plug-in preferences
 */
public class ADELEPreferenceConstants extends AdeleCommonPreferenceConstants {
	
}
