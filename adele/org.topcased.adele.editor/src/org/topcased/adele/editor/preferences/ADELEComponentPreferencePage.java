/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	Arnaud Schach, arnaud.schach.com
 ******************************************************************************/
package org.topcased.adele.editor.preferences;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.facilities.preferences.AbstractTopcasedPreferencePage;

/**
 * A preference page to configure all preferences of the ADELE editor.
 *
 * @generated NOT
 */
public class ADELEComponentPreferencePage extends AbstractTopcasedPreferencePage {
	

	/**
	 * Creates this preference page contents.
	 *
	 * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
	 * @generated 
	 */
	protected Control createContents(Composite parent) {
		// Create the container composite
		
		return new Composite(parent, SWT.NONE);
	}

	/**
	 * Initializes the HMI with preference values.
	 *
	 * @generated NOT
	 */
	@SuppressWarnings("unused")
	private void loadPreferences() {
	}

	/**
	 * Stores the HMI values into the preference store.
	 *
	 * @generated NOT
	 */
	@SuppressWarnings("unused")
	private void storePreferences() {
	}

	/**
	 * Initializes the HMI with default preference values.
	 *
	 * @generated NOT
	 */
	@SuppressWarnings("unused")
	private void loadDefaultPreferences() {
	}

	/**
	 * Creates this preference page header.
	 *
	 * @param parent the parent composite
	 * @generated
	 */
	@SuppressWarnings("unused")
	private void createHeader(Composite parent) {
		new Label(parent, SWT.WRAP).setText("ADELE components preference page");
	}

	/**
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 * @generated
	 */
	public void init(IWorkbench workbench) {
		// Do nothing
	}

	/**
	 * @see org.eclipse.jface.preference.IPreferencePage#performOk()
	 * @generated
	 */
	public boolean performOk() {
		return super.performOk();
	}

	/**
	 * @see org.eclipse.jface.preference.PreferencePage#performDefaults()
	 * @generated
	 */
	protected void performDefaults() {
		super.performDefaults();
	}

	/**
	 * @see org.topcased.facilities.preferences.AbstractTopcasedPreferencePage#getBundleId()
	 * @generated
	 */
	protected String getBundleId() {
		return ADELEPlugin.getId();
	}
}
