package org.topcased.adele.editor.preferences;

import java.io.File;

import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Preferences;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.topcased.adele.editor.ADELEPlugin;

/**
 * Class used to initialize default preference values.
 */
public class ADELEPreferenceInitializer extends AllDiagramPreferenceInitializer {

	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		
//		IEclipsePreferences ps = ADELEPlugin.getDefault().getPreferenceStore().getPluginPreferences();
//		
//		String adminPluginPath = Platform.getBundle("org.topcased.adele.admin").getDescriptor().getInstallURL().getPath();
//		adminPluginPath = adminPluginPath.replaceFirst("/plugin/", "/plugins/");
//		adminPluginPath = Platform.getInstallLocation().getURL().getFile()
//				+ adminPluginPath;
//		
//		String defaultDatabasePath = adminPluginPath + "usr" + File.separator
//				+ "config" + File.separator + "database.odsconfig";
//		if (!(new File(defaultDatabasePath)).exists())
//			defaultDatabasePath="";
//		ps.setDefault(ADELEPreferenceConstants.ADELE_DATABASE_PATH, (new Path(defaultDatabasePath)).toOSString());
//
//		String defaultLMPPath = adminPluginPath + "usr" + File.separator
//		+ "LMP";
//		if (!(new File(defaultLMPPath)).exists())
//			defaultLMPPath="";
//		ps.setDefault(ADELEPreferenceConstants.ADELE_LMP_PATH, (new Path(defaultLMPPath)).toOSString());

	}

}
