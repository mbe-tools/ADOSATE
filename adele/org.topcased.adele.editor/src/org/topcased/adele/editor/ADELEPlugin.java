/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *  	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.topcased.adele.common.utils.ADELEModeler;

/**
 * The main plugin class to be used in the desktop.
 *
 * @generated
 */
public class ADELEPlugin extends AbstractUIPlugin {
	/**
	 * The shared instance
	 * @generated
	 */
	private static ADELEPlugin plugin;

	public static int MaxLabelSize = 50;

	/**
	 * The constructor.
	 *
	 * @generated
	 */
	public ADELEPlugin() {
		super();
		plugin = this;
	}

	/**
	 * This method is called upon plug-in activation
	 *
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 * @generated
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
	}

	/**
	 * This method is called when the plug-in is stopped
	 *
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 * @generated
	 */
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
		plugin = null;
	}

	/**
	 * Returns the shared instance.
	 *
	 * @return the singleton
	 * @generated
	 */
	public static ADELEPlugin getDefault() {
		return plugin;
	}

	/**
	 * @return the Plugin Id
	 * @generated
	 */
	public static String getId() {
		return getDefault().getBundle().getSymbolicName();
	}

	/**
	 * Log a message with given level into the Eclipse log file
	 *
	 * @param message the message to log
	 * @param level the message priority
	 * @generated
	 */
	public static void log(String message, int level) {
		IStatus status = null;
		status = new Status(level, getId(), IStatus.OK, message, null);
		log(status);
	}

	/**
	 * Log an exception into the Eclipse log file
	 *
	 * @param e the exception to log
	 * @generated
	 */
	public static void log(Throwable e) {
		if (e instanceof InvocationTargetException)
			e = ((InvocationTargetException) e).getTargetException();

		IStatus status = null;
		if (e instanceof CoreException)
			status = ((CoreException) e).getStatus();
		else
			status = new Status(IStatus.ERROR, getId(), IStatus.OK, "Error", e);

		log(status);
	}

	/**
	 * Log an IStatus
	 *
	 * @param status the status to log
	 * @generated
	 */
	public static void log(IStatus status) {
		ResourcesPlugin.getPlugin().getLog().log(status);
	}

	/**
	 * Display a dialog box with the specified level
	 * 
	 * @param title title dialog box
	 * @param message message displayed
	 * @param level message level
	 * @generated
	 */
	public static void displayDialog(String p_title, String p_message, int level) {
		// Declare locally to avoid jmerge problem of loosing the final modifier of parameters when the method already exists.
		final String title = p_title;
		final String message = p_message;

		if (level == IStatus.INFO) {
			Display.getDefault().asyncExec(new Runnable() {
				public void run() {
					MessageDialog.openInformation(getActiveWorkbenchShell(),
							(title == null) ? "Information" : title,
							(message == null) ? "" : message);
				}
			});
		} else if (level == IStatus.WARNING) {
			Display.getDefault().asyncExec(new Runnable() {
				public void run() {
					MessageDialog.openWarning(getActiveWorkbenchShell(),
							(title == null) ? "Warning" : title,
							(message == null) ? "" : message);
				}
			});
		} else if (level == IStatus.ERROR) {
			Display.getDefault().asyncExec(new Runnable() {
				public void run() {
					MessageDialog.openError(getActiveWorkbenchShell(),
							(title == null) ? "Error" : title,
							(message == null) ? "" : message);
				}
			});
		}
	}

	/**
	 * Returns the active workbench shell
	 * 
	 * @return the active workbench shell
	 * @generated
	 */
	public static Shell getActiveWorkbenchShell() {
		IWorkbenchWindow workBenchWindow = getActiveWorkbenchWindow();
		if (workBenchWindow == null) {
			return null;
		}
		return workBenchWindow.getShell();
	}

	/**
	 * Returns the active workbench page or <code>null</code> if none.
	 * 
	 * @return the active workbench page
	 * @generated
	 */
	public static IWorkbenchPage getActivePage() {
		IWorkbenchWindow window = getActiveWorkbenchWindow();
		if (window != null) {
			return window.getActivePage();
		}
		return null;
	}
	
	public static boolean isSystemActiveEditor() {
		final IWorkbenchPage wPage = getActivePage();

		if ( wPage != null ) {
			final IEditorPart editorPart = wPage.getActiveEditor();

			if ( editorPart instanceof ADELEModeler ) {
				final ADELEModeler editor = (ADELEModeler) editorPart;

				return editor.isSystemKind();
			}
		}
		
		return false;
	}

	/**
	 * Returns the active workbench window
	 * 
	 * @return the active workbench window
	 * @generated
	 */
	public static IWorkbenchWindow getActiveWorkbenchWindow() {
		if (getDefault() == null) {
			return null;
		}
		IWorkbench workBench = getDefault().getWorkbench();
		if (workBench == null) {
			return null;
		}
		return workBench.getActiveWorkbenchWindow();
	}
}
