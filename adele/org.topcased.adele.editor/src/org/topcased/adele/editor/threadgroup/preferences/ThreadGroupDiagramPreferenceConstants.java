/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.threadgroup.preferences;

/**
 * A set of constant that are used in the diagram graphical preferences
 * 
 * @generated
 */
public interface ThreadGroupDiagramPreferenceConstants {
	/**
	 * The key used to install a <i>Data Default Background Color</i> Preference.
	 * @generated
	 */
	String DATA_DEFAULT_BACKGROUND_COLOR = "Data Default Background Color";

	/**
	 * The key used to install a <i>Data Default Foreground Color</i> Preference.
	 * @generated
	 */
	String DATA_DEFAULT_FOREGROUND_COLOR = "Data Default Foreground Color";

	/**
	 * The key used to install a <i>Data Default Font</i> Preference.
	 * @generated
	 */
	String DATA_DEFAULT_FONT = "Data Default Font";

	/**
	 * The key used to install a <i>Subprogram Default Background Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAM_DEFAULT_BACKGROUND_COLOR = "Subprogram Default Background Color";

	/**
	 * The key used to install a <i>Subprogram Default Foreground Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAM_DEFAULT_FOREGROUND_COLOR = "Subprogram Default Foreground Color";

	/**
	 * The key used to install a <i>Subprogram Default Font</i> Preference.
	 * @generated
	 */
	String SUBPROGRAM_DEFAULT_FONT = "Subprogram Default Font";

	/**
	 * The key used to install a <i>SubprogramGroup Default Background Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUP_DEFAULT_BACKGROUND_COLOR = "SubprogramGroup Default Background Color";

	/**
	 * The key used to install a <i>SubprogramGroup Default Foreground Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUP_DEFAULT_FOREGROUND_COLOR = "SubprogramGroup Default Foreground Color";

	/**
	 * The key used to install a <i>SubprogramGroup Default Font</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUP_DEFAULT_FONT = "SubprogramGroup Default Font";

	/**
	 * The key used to install a <i>Thread Default Background Color</i> Preference.
	 * @generated
	 */
	String THREAD_DEFAULT_BACKGROUND_COLOR = "Thread Default Background Color";

	/**
	 * The key used to install a <i>Thread Default Foreground Color</i> Preference.
	 * @generated
	 */
	String THREAD_DEFAULT_FOREGROUND_COLOR = "Thread Default Foreground Color";

	/**
	 * The key used to install a <i>Thread Default Font</i> Preference.
	 * @generated
	 */
	String THREAD_DEFAULT_FONT = "Thread Default Font";

	/**
	 * The key used to install a <i>ThreadGroup Default Background Color</i> Preference.
	 * @generated
	 */
	String THREADGROUP_DEFAULT_BACKGROUND_COLOR = "ThreadGroup Default Background Color";

	/**
	 * The key used to install a <i>ThreadGroup Default Foreground Color</i> Preference.
	 * @generated
	 */
	String THREADGROUP_DEFAULT_FOREGROUND_COLOR = "ThreadGroup Default Foreground Color";

	/**
	 * The key used to install a <i>ThreadGroup Default Font</i> Preference.
	 * @generated
	 */
	String THREADGROUP_DEFAULT_FONT = "ThreadGroup Default Font";

	/**
	 * The key used to install a <i>Abstract Default Background Color</i> Preference.
	 * @generated
	 */
	String ABSTRACT_DEFAULT_BACKGROUND_COLOR = "Abstract Default Background Color";

	/**
	 * The key used to install a <i>Abstract Default Foreground Color</i> Preference.
	 * @generated
	 */
	String ABSTRACT_DEFAULT_FOREGROUND_COLOR = "Abstract Default Foreground Color";

	/**
	 * The key used to install a <i>Abstract Default Font</i> Preference.
	 * @generated
	 */
	String ABSTRACT_DEFAULT_FONT = "Abstract Default Font";

	/**
	 * The key used to install a <i>EventPort Default Background Color</i> Preference.
	 * @generated
	 */
	String EVENTPORT_DEFAULT_BACKGROUND_COLOR = "EventPort Default Background Color";

	/**
	 * The key used to install a <i>EventPort Default Foreground Color</i> Preference.
	 * @generated
	 */
	String EVENTPORT_DEFAULT_FOREGROUND_COLOR = "EventPort Default Foreground Color";

	/**
	 * The key used to install a <i>EventPort Default Font</i> Preference.
	 * @generated
	 */
	String EVENTPORT_DEFAULT_FONT = "EventPort Default Font";

	/**
	 * The key used to install a <i>DataPort Default Background Color</i> Preference.
	 * @generated
	 */
	String DATAPORT_DEFAULT_BACKGROUND_COLOR = "DataPort Default Background Color";

	/**
	 * The key used to install a <i>DataPort Default Foreground Color</i> Preference.
	 * @generated
	 */
	String DATAPORT_DEFAULT_FOREGROUND_COLOR = "DataPort Default Foreground Color";

	/**
	 * The key used to install a <i>DataPort Default Font</i> Preference.
	 * @generated
	 */
	String DATAPORT_DEFAULT_FONT = "DataPort Default Font";

	/**
	 * The key used to install a <i>EventDataPort Default Background Color</i> Preference.
	 * @generated
	 */
	String EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR = "EventDataPort Default Background Color";

	/**
	 * The key used to install a <i>EventDataPort Default Foreground Color</i> Preference.
	 * @generated
	 */
	String EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR = "EventDataPort Default Foreground Color";

	/**
	 * The key used to install a <i>EventDataPort Default Font</i> Preference.
	 * @generated
	 */
	String EVENTDATAPORT_DEFAULT_FONT = "EventDataPort Default Font";

	/**
	 * The key used to install a <i>FeatureGroup Default Background Color</i> Preference.
	 * @generated
	 */
	String FEATUREGROUP_DEFAULT_BACKGROUND_COLOR = "FeatureGroup Default Background Color";

	/**
	 * The key used to install a <i>FeatureGroup Default Foreground Color</i> Preference.
	 * @generated
	 */
	String FEATUREGROUP_DEFAULT_FOREGROUND_COLOR = "FeatureGroup Default Foreground Color";

	/**
	 * The key used to install a <i>FeatureGroup Default Font</i> Preference.
	 * @generated
	 */
	String FEATUREGROUP_DEFAULT_FONT = "FeatureGroup Default Font";

	/**
	 * The key used to install a <i>DataAccess Default Background Color</i> Preference.
	 * @generated
	 */
	String DATAACCESS_DEFAULT_BACKGROUND_COLOR = "DataAccess Default Background Color";

	/**
	 * The key used to install a <i>DataAccess Default Foreground Color</i> Preference.
	 * @generated
	 */
	String DATAACCESS_DEFAULT_FOREGROUND_COLOR = "DataAccess Default Foreground Color";

	/**
	 * The key used to install a <i>DataAccess Default Font</i> Preference.
	 * @generated
	 */
	String DATAACCESS_DEFAULT_FONT = "DataAccess Default Font";

	/**
	 * The key used to install a <i>SubprogramAccess Default Background Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR = "SubprogramAccess Default Background Color";

	/**
	 * The key used to install a <i>SubprogramAccess Default Foreground Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR = "SubprogramAccess Default Foreground Color";

	/**
	 * The key used to install a <i>SubprogramAccess Default Font</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMACCESS_DEFAULT_FONT = "SubprogramAccess Default Font";

	/**
	 * The key used to install a <i>SubprogramGroupAccess Default Background Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR = "SubprogramGroupAccess Default Background Color";

	/**
	 * The key used to install a <i>SubprogramGroupAccess Default Foreground Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR = "SubprogramGroupAccess Default Foreground Color";

	/**
	 * The key used to install a <i>SubprogramGroupAccess Default Font</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESS_DEFAULT_FONT = "SubprogramGroupAccess Default Font";

	/**
	 * The key used to install a <i>AbstractFeature Default Background Color</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR = "AbstractFeature Default Background Color";

	/**
	 * The key used to install a <i>AbstractFeature Default Foreground Color</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR = "AbstractFeature Default Foreground Color";

	/**
	 * The key used to install a <i>AbstractFeature Default Font</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURE_DEFAULT_FONT = "AbstractFeature Default Font";

	/**
	 * The key used to install a <i>Parameter Default Background Color</i> Preference.
	 * @generated
	 */
	String PARAMETER_DEFAULT_BACKGROUND_COLOR = "Parameter Default Background Color";

	/**
	 * The key used to install a <i>Parameter Default Foreground Color</i> Preference.
	 * @generated
	 */
	String PARAMETER_DEFAULT_FOREGROUND_COLOR = "Parameter Default Foreground Color";

	/**
	 * The key used to install a <i>Parameter Default Font</i> Preference.
	 * @generated
	 */
	String PARAMETER_DEFAULT_FONT = "Parameter Default Font";

	/**
	 * The key used to install a <i>EventPortConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String EVENTPORTCONNECTION_EDGE_DEFAULT_FONT = "EventPortConnection Edge Default Font";

	/**
	 * The key used to install a <i>EventPortConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String EVENTPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "EventPortConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>EventPortConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String EVENTPORTCONNECTION_EDGE_DEFAULT_ROUTER = "EventPortConnection Edge Default Router";

	/**
	 * The key used to install a <i>DataPortConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String DATAPORTCONNECTION_EDGE_DEFAULT_FONT = "DataPortConnection Edge Default Font";

	/**
	 * The key used to install a <i>DataPortConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String DATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "DataPortConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>DataPortConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String DATAPORTCONNECTION_EDGE_DEFAULT_ROUTER = "DataPortConnection Edge Default Router";

	/**
	 * The key used to install a <i>EventDataPortConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FONT = "EventDataPortConnection Edge Default Font";

	/**
	 * The key used to install a <i>EventDataPortConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "EventDataPortConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>EventDataPortConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String EVENTDATAPORTCONNECTION_EDGE_DEFAULT_ROUTER = "EventDataPortConnection Edge Default Router";

	/**
	 * The key used to install a <i>DataAccessConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String DATAACCESSCONNECTION_EDGE_DEFAULT_FONT = "DataAccessConnection Edge Default Font";

	/**
	 * The key used to install a <i>DataAccessConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String DATAACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "DataAccessConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>DataAccessConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String DATAACCESSCONNECTION_EDGE_DEFAULT_ROUTER = "DataAccessConnection Edge Default Router";

	/**
	 * The key used to install a <i>SubprogramAccessConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FONT = "SubprogramAccessConnection Edge Default Font";

	/**
	 * The key used to install a <i>SubprogramAccessConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "SubprogramAccessConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>SubprogramAccessConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_ROUTER = "SubprogramAccessConnection Edge Default Router";

	/**
	 * The key used to install a <i>SubprogramGroupAccessConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FONT = "SubprogramGroupAccessConnection Edge Default Font";

	/**
	 * The key used to install a <i>SubprogramGroupAccessConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "SubprogramGroupAccessConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>SubprogramGroupAccessConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_ROUTER = "SubprogramGroupAccessConnection Edge Default Router";

	/**
	 * The key used to install a <i>ParameterConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String PARAMETERCONNECTION_EDGE_DEFAULT_FONT = "ParameterConnection Edge Default Font";

	/**
	 * The key used to install a <i>ParameterConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String PARAMETERCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "ParameterConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>ParameterConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String PARAMETERCONNECTION_EDGE_DEFAULT_ROUTER = "ParameterConnection Edge Default Router";

	/**
	 * The key used to install a <i>AbstractFeatureConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FONT = "AbstractFeatureConnection Edge Default Font";

	/**
	 * The key used to install a <i>AbstractFeatureConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "AbstractFeatureConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>AbstractFeatureConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_ROUTER = "AbstractFeatureConnection Edge Default Router";

	/**
	 * The key used to install a <i>FeatureGroupConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String FEATUREGROUPCONNECTION_EDGE_DEFAULT_FONT = "FeatureGroupConnection Edge Default Font";

	/**
	 * The key used to install a <i>FeatureGroupConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String FEATUREGROUPCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "FeatureGroupConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>FeatureGroupConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String FEATUREGROUPCONNECTION_EDGE_DEFAULT_ROUTER = "FeatureGroupConnection Edge Default Router";

}