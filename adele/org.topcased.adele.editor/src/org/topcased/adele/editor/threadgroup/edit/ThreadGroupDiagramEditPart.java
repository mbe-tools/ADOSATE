/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.threadgroup.edit;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPolicy;
import org.topcased.adele.editor.threadgroup.figures.ThreadGroupDiagramFigure;
import org.topcased.adele.editor.threadgroup.policies.ThreadGroupDiagramLayoutEditPolicy;
import org.topcased.adele.editor.utils.edit.ADELE_DiagramEditPart;
import org.topcased.modeler.di.model.Diagram;

/**
 * @generated NOT
 */
public class ThreadGroupDiagramEditPart extends ADELE_DiagramEditPart {

	/**
	 * The Constructor
	 *
	 * @param model the root model element
	 * @generated
	 */
	public ThreadGroupDiagramEditPart(Diagram model) {
		super(model);
	}

	/**
	 * @see org.topcased.modeler.edit.DiagramEditPart#getLayoutEditPolicy()
	 * @generated
	 */
	protected EditPolicy getLayoutEditPolicy() {
		return new ThreadGroupDiagramLayoutEditPolicy();
	}

	/**
	 * @see org.topcased.modeler.edit.DiagramEditPart#createBodyFigure()
	 * @generated NOT
	 */
	protected IFigure createBodyFigure() {

		IFigure page = new ThreadGroupDiagramFigure();

		super.createBodyFigure();

		return page;
	}
}