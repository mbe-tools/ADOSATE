/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.threadgroup;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.palette.CreationToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteStack;
import org.eclipse.gef.requests.CreationFactory;
import org.eclipse.ui.IWorkbenchPage;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.adele.editor.editor.ADELEEditor;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;
import org.topcased.adele.model.ADELE_Features.AbstractFeature;
import org.topcased.adele.model.ADELE_Features.AccessDirection;
import org.topcased.adele.model.ADELE_Features.DataAccess;
import org.topcased.adele.model.ADELE_Features.DataPort;
import org.topcased.adele.model.ADELE_Features.EventDataPort;
import org.topcased.adele.model.ADELE_Features.EventPort;
import org.topcased.adele.model.ADELE_Features.Parameter;
import org.topcased.adele.model.ADELE_Features.PortDirection;
import org.topcased.adele.model.ADELE_Features.SubprogramAccess;
import org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess;
import org.topcased.adele.model.ADELE_Relations.ADELE_RelationsPackage;
import org.topcased.modeler.editor.GraphElementCreationFactory;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.palette.ModelerConnectionCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerPaletteManager;

/**
 * Generated Palette Manager
 *
 * @generated
 */
public class ThreadGroupPaletteManager extends ModelerPaletteManager {
	// declare all the palette categories of the diagram
	/**
	 * @generated
	 */
	private PaletteDrawer componentsDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer featuresDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer connectionsDrawer;

	/**
	 * @generated
	 */
	private ICreationUtils creationUtils;

	/**
	 * The Constructor
	 *
	 * @param utils the creation utils for the tools of the palette 
	 * @generated
	 */
	public ThreadGroupPaletteManager(ICreationUtils utils) {
		super();
		this.creationUtils = utils;
	}

	/**
	 * Creates the main categories of the palette
	 *
	 * @generated
	 */
	protected void createCategories() {
		createComponentsDrawer();
		createFeaturesDrawer();
		createConnectionsDrawer();
	}

	/**
	 * Updates the main categories of the palette
	 *
	 * @generated
	 */
	protected void updateCategories() {
		// deletion of the existing categories and creation of the updated categories

		getRoot().remove(componentsDrawer);
		createComponentsDrawer();

		getRoot().remove(featuresDrawer);
		createFeaturesDrawer();

		getRoot().remove(connectionsDrawer);
		createConnectionsDrawer();
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated NOT
	 */
	private void createComponentsDrawer() {
		componentsDrawer = new PaletteDrawer("Components", null);
		List<PaletteEntry> entries = new ArrayList<PaletteEntry>();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getData(), "default");
		entries.add(new ModelerCreationToolEntry("Data", "Data", factory,
				ThreadGroupImageRegistry.getImageDescriptor("DATA"),
				ThreadGroupImageRegistry.getImageDescriptor("DATA_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getSubprogram(), "default");
		entries.add(new ModelerCreationToolEntry("Subprogram", "Subprogram",
				factory, ThreadGroupImageRegistry
						.getImageDescriptor("SUBPROGRAM"),
				ThreadGroupImageRegistry.getImageDescriptor("SUBPROGRAM_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getSubprogramGroup(),
				"default");
		entries.add(new ModelerCreationToolEntry("Subprogram Group",
				"Subprogram Group", factory, ThreadGroupImageRegistry
						.getImageDescriptor("SUBPROGRAMGROUP"),
				ThreadGroupImageRegistry
						.getImageDescriptor("SUBPROGRAMGROUP_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getThread(), "default");
		entries.add(new ModelerCreationToolEntry("Thread", "Thread", factory,
				ThreadGroupImageRegistry.getImageDescriptor("THREAD"),
				ThreadGroupImageRegistry.getImageDescriptor("THREAD_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getThreadGroup(), "default");
		entries.add(new ModelerCreationToolEntry("Thread group",
				"Thread group", factory, ThreadGroupImageRegistry
						.getImageDescriptor("THREADGROUP"),
				ThreadGroupImageRegistry
						.getImageDescriptor("THREADGROUP_LARGE")));

		IWorkbenchPage wPage = ADELEPlugin.getActivePage();
		if (wPage != null && wPage.getActiveEditor() instanceof ADELEEditor ) {
			ADELEEditor editor = (ADELEEditor) wPage.getActiveEditor();
			if (editor != null && editor.getSelectedObject() != null
					&& editor.getSelectedObject().getName().contains(".")) {
				factory = new GraphElementCreationFactory(creationUtils,
						ADELE_ComponentsPackage.eINSTANCE.getAbstract(),
						"default");
				entries.add(new ModelerCreationToolEntry("Abstract",
						"Abstract", factory, ThreadGroupImageRegistry
								.getImageDescriptor("ABSTRACT"),
						ThreadGroupImageRegistry
								.getImageDescriptor("ABSTRACT_LARGE")));
			}
		}

		componentsDrawer.addAll(entries);
		getRoot().add(componentsDrawer);
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated NOT
	 */
	private void createFeaturesDrawer() {
		if ( ADELEPlugin.isSystemActiveEditor() ) {
			featuresDrawer = new PaletteDrawer("Features", null);
			List<PaletteEntry> entries = new ArrayList<PaletteEntry>();
	
			CreationFactory factory;
			String newObjTxt;
			CreationToolEntry objectTool;
	
			// Event Ports
	
			PaletteStack eventPortStack = new PaletteStack("Event Port",
					"Event Port Selection",
					ThreadGroupImageRegistry.getImageDescriptor("EVENTPORT"));
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getEventPort(), "default") {
				public EObject getNewModelObject() {
					EventPort port = (EventPort) super.getNewModelObject();
					port.setDirection(PortDirection.IN_LITERAL);
					return port;
				}
			};
			newObjTxt = "In Event Port";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry.getImageDescriptor("INEVENTPORT"),
					ThreadGroupImageRegistry
							.getImageDescriptor("INEVENTPORT_LARGE"));
			eventPortStack.add(objectTool);
			eventPortStack.setActiveEntry(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getEventPort(), "default") {
				public EObject getNewModelObject() {
					EventPort port = (EventPort) super.getNewModelObject();
					port.setDirection(PortDirection.OUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "Out Event Port";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry.getImageDescriptor("OUTEVENTPORT"),
					ThreadGroupImageRegistry
							.getImageDescriptor("OUTEVENTPORT_LARGE"));
			eventPortStack.add(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getEventPort(), "default") {
				public EObject getNewModelObject() {
					EventPort port = (EventPort) super.getNewModelObject();
					port.setDirection(PortDirection.INOUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "InOut Event Port";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry.getImageDescriptor("INOUTEVENTPORT"),
					ThreadGroupImageRegistry
							.getImageDescriptor("INOUTEVENTPORT_LARGE"));
			eventPortStack.add(objectTool);
	
			entries.add(eventPortStack);
	
			// Data ports
	
			PaletteStack dataPortStack = new PaletteStack("Data Port",
					"Data Port Selection",
					ThreadGroupImageRegistry.getImageDescriptor("DATAPORT"));
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getDataPort(), "default") {
				public EObject getNewModelObject() {
					DataPort port = (DataPort) super.getNewModelObject();
					port.setDirection(PortDirection.IN_LITERAL);
					return port;
				}
			};
			newObjTxt = "In Data Port";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry.getImageDescriptor("INDATAPORT"),
					ThreadGroupImageRegistry.getImageDescriptor("INDATAPORT_LARGE"));
			dataPortStack.add(objectTool);
			dataPortStack.setActiveEntry(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getDataPort(), "default") {
				public EObject getNewModelObject() {
					DataPort port = (DataPort) super.getNewModelObject();
					port.setDirection(PortDirection.OUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "Out Data Port";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry.getImageDescriptor("OUTDATAPORT"),
					ThreadGroupImageRegistry
							.getImageDescriptor("OUTDATAPORT_LARGE"));
			dataPortStack.add(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getDataPort(), "default") {
				public EObject getNewModelObject() {
					DataPort port = (DataPort) super.getNewModelObject();
					port.setDirection(PortDirection.INOUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "InOut Data Port";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry.getImageDescriptor("INOUTDATAPORT"),
					ThreadGroupImageRegistry
							.getImageDescriptor("INOUTDATAPORT_LARGE"));
			dataPortStack.add(objectTool);
	
			entries.add(dataPortStack);
	
			// Event Data ports
	
			PaletteStack eventDataPortStack = new PaletteStack("Event Data Port",
					"Event Data Port Selection",
					ThreadGroupImageRegistry.getImageDescriptor("EVENTDATAPORT"));
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getEventDataPort(), "default") {
				public EObject getNewModelObject() {
					EventDataPort port = (EventDataPort) super.getNewModelObject();
					port.setDirection(PortDirection.IN_LITERAL);
					return port;
				}
			};
			newObjTxt = "In Event Data Port";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry.getImageDescriptor("INEVENTDATAPORT"),
					ThreadGroupImageRegistry
							.getImageDescriptor("INEVENTDATAPORT_LARGE"));
			eventDataPortStack.add(objectTool);
			eventDataPortStack.setActiveEntry(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getEventDataPort(), "default") {
				public EObject getNewModelObject() {
					EventDataPort port = (EventDataPort) super.getNewModelObject();
					port.setDirection(PortDirection.OUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "Out Event Data Port";
			objectTool = new ModelerCreationToolEntry(
					newObjTxt,
					newObjTxt,
					factory,
					ThreadGroupImageRegistry.getImageDescriptor("OUTEVENTDATAPORT"),
					ThreadGroupImageRegistry
							.getImageDescriptor("OUTEVENTDATAPORT_LARGE"));
			eventDataPortStack.add(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getEventDataPort(), "default") {
				public EObject getNewModelObject() {
					EventDataPort port = (EventDataPort) super.getNewModelObject();
					port.setDirection(PortDirection.INOUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "InOut Event Data Port";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry
							.getImageDescriptor("INOUTEVENTDATAPORT"),
					ThreadGroupImageRegistry
							.getImageDescriptor("INOUTEVENTDATAPORT_LARGE"));
			eventDataPortStack.add(objectTool);
	
			entries.add(eventDataPortStack);
	
			// Feature Group
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getFeatureGroup(), "default");
			entries.add(new ModelerCreationToolEntry("Feature Group",
					"Feature Group", factory, ThreadGroupImageRegistry
							.getImageDescriptor("FEATUREGROUP"),
					ThreadGroupImageRegistry
							.getImageDescriptor("FEATUREGROUP_LARGE")));
	
			//	Data access
	
			PaletteStack dataAccessStack = new PaletteStack("Data Access",
					"Data Access Selection",
					ThreadGroupImageRegistry.getImageDescriptor("DataAccess"));
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getDataAccess(), "default") {
				public EObject getNewModelObject() {
					DataAccess access = (DataAccess) super.getNewModelObject();
					access.setDirection(AccessDirection.PROVIDED_LITERAL);
					return access;
				}
			};
			newObjTxt = "Provides data access";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry
							.getImageDescriptor("PROVIDEDDATAACCESS"),
					ThreadGroupImageRegistry
							.getImageDescriptor("PROVIDEDDATAACCESS_LARGE"));
			dataAccessStack.add(objectTool);
			dataAccessStack.setActiveEntry(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getDataAccess(), "default") {
				public EObject getNewModelObject() {
					DataAccess access = (DataAccess) super.getNewModelObject();
					access.setDirection(AccessDirection.REQUIRED_LITERAL);
					return access;
				}
			};
			newObjTxt = "Requires data access";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry
							.getImageDescriptor("REQUIREDDATAACCESS"),
					ThreadGroupImageRegistry
							.getImageDescriptor("REQUIREDDATAACCESS_LARGE"));
			dataAccessStack.add(objectTool);
	
			entries.add(dataAccessStack);
	
			// Subprogram Access
	
			PaletteStack subprogramAccessStack = new PaletteStack(
					"Subprogram Access", "Subprogram Access Selection",
					ThreadGroupImageRegistry.getImageDescriptor("SubprogramAccess"));
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getSubprogramAccess(),
					"default") {
				public EObject getNewModelObject() {
					SubprogramAccess access = (SubprogramAccess) super
							.getNewModelObject();
					access.setDirection(AccessDirection.PROVIDED_LITERAL);
					return access;
				}
			};
			newObjTxt = "Provides subprogram access";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry
							.getImageDescriptor("PROVIDEDSUBPROGRAMACCESS"),
					ThreadGroupImageRegistry
							.getImageDescriptor("PROVIDEDSUBPROGRAMACCESS_LARGE"));
			subprogramAccessStack.add(objectTool);
			subprogramAccessStack.setActiveEntry(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getSubprogramAccess(),
					"default") {
				public EObject getNewModelObject() {
					SubprogramAccess access = (SubprogramAccess) super
							.getNewModelObject();
					access.setDirection(AccessDirection.REQUIRED_LITERAL);
					return access;
				}
			};
			newObjTxt = "Requires subprogram access";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry
							.getImageDescriptor("REQUIREDSUBPROGRAMACCESS"),
					ThreadGroupImageRegistry
							.getImageDescriptor("REQUIREDSUBPROGRAMACCESS_LARGE"));
			subprogramAccessStack.add(objectTool);
	
			entries.add(subprogramAccessStack);
	
			// Subprogram Group Access
	
			PaletteStack subprogramGroupAccessStack = new PaletteStack(
					"Subprogram group access", "Subprogram group access Selection",
					ThreadGroupImageRegistry
							.getImageDescriptor("SubprogramGroupAccess"));
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getSubprogramGroupAccess(),
					"default") {
				public EObject getNewModelObject() {
					SubprogramGroupAccess access = (SubprogramGroupAccess) super
							.getNewModelObject();
					access.setDirection(AccessDirection.PROVIDED_LITERAL);
					return access;
				}
			};
			newObjTxt = "Provides subprogram group access";
			objectTool = new ModelerCreationToolEntry(
					newObjTxt,
					newObjTxt,
					factory,
					ThreadGroupImageRegistry
							.getImageDescriptor("PROVIDEDSUBPROGRAMGROUPACCESS"),
					ThreadGroupImageRegistry
							.getImageDescriptor("PROVIDEDSUBPROGRAMGROUPACCESS_LARGE"));
			subprogramGroupAccessStack.add(objectTool);
			subprogramGroupAccessStack.setActiveEntry(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getSubprogramGroupAccess(),
					"default") {
				public EObject getNewModelObject() {
					SubprogramGroupAccess access = (SubprogramGroupAccess) super
							.getNewModelObject();
					access.setDirection(AccessDirection.REQUIRED_LITERAL);
					return access;
				}
			};
			newObjTxt = "Requires subprogram group access";
			objectTool = new ModelerCreationToolEntry(
					newObjTxt,
					newObjTxt,
					factory,
					ThreadGroupImageRegistry
							.getImageDescriptor("REQUIREDSUBPROGRAMGROUPACCESS"),
					ThreadGroupImageRegistry
							.getImageDescriptor("REQUIREDSUBPROGRAMGROUPACCESS_LARGE"));
			subprogramGroupAccessStack.add(objectTool);
	
			entries.add(subprogramGroupAccessStack);
	
			// Abstract Feature
	
			PaletteStack abstractFeatureStack = new PaletteStack("Feature",
					"Feature Selection",
					ThreadGroupImageRegistry.getImageDescriptor("ABSTRACTFEATURE"));
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getAbstractFeature(), "default") {
				public EObject getNewModelObject() {
					AbstractFeature port = (AbstractFeature) super
							.getNewModelObject();
					//port.setDirection(AbstractFeatureDirection.NONE);
					return port;
				}
			};
			newObjTxt = "Feature";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry.getImageDescriptor("ABSTRACTFEATURE"),
					ThreadGroupImageRegistry
							.getImageDescriptor("ABSTRACTFEATURE_LARGE"));
			abstractFeatureStack.add(objectTool);
			abstractFeatureStack.setActiveEntry(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getAbstractFeature(), "default") {
				public EObject getNewModelObject() {
					AbstractFeature port = (AbstractFeature) super
							.getNewModelObject();
					//port.setDirection(AbstractFeatureDirection.IN);
					return port;
				}
			};
			newObjTxt = "In Feature";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry
							.getImageDescriptor("INABSTRACTFEATURE"),
					ThreadGroupImageRegistry
							.getImageDescriptor("INABSTRACTFEATURE_LARGE"));
			abstractFeatureStack.add(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getAbstractFeature(), "default") {
				public EObject getNewModelObject() {
					AbstractFeature port = (AbstractFeature) super
							.getNewModelObject();
					//port.setDirection(AbstractFeatureDirection.OUT);
					return port;
				}
			};
			newObjTxt = "Out Feature";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry
							.getImageDescriptor("OUTABSTRACTFEATURE"),
					ThreadGroupImageRegistry
							.getImageDescriptor("OUTABSTRACTFEATURE_LARGE"));
			abstractFeatureStack.add(objectTool);
	
			entries.add(abstractFeatureStack);
	
			// Parameter
	
			PaletteStack parameterStack = new PaletteStack("Parameter",
					"Parameter Selection",
					ThreadGroupImageRegistry.getImageDescriptor("PARAMETER"));
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getParameter(), "default") {
				public EObject getNewModelObject() {
					Parameter port = (Parameter) super.getNewModelObject();
					port.setDirection(PortDirection.IN_LITERAL);
					return port;
				}
			};
			newObjTxt = "In Parameter";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry.getImageDescriptor("INPARAMETER"),
					ThreadGroupImageRegistry
							.getImageDescriptor("INPARAMETER_LARGE"));
			parameterStack.add(objectTool);
			parameterStack.setActiveEntry(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getParameter(), "default") {
				public EObject getNewModelObject() {
					Parameter port = (Parameter) super.getNewModelObject();
					port.setDirection(PortDirection.OUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "Out Parameter";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry.getImageDescriptor("OUTPARAMETER"),
					ThreadGroupImageRegistry
							.getImageDescriptor("OUTPARAMETER_LARGE"));
			parameterStack.add(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getParameter(), "default") {
				public EObject getNewModelObject() {
					Parameter port = (Parameter) super.getNewModelObject();
					port.setDirection(PortDirection.INOUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "InOut Parameter";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					ThreadGroupImageRegistry.getImageDescriptor("INOUTPARAMETER"),
					ThreadGroupImageRegistry
							.getImageDescriptor("INOUTPARAMETER_LARGE"));
			parameterStack.add(objectTool);
	
			entries.add(parameterStack);
	
			featuresDrawer.addAll(entries);
			getRoot().add(featuresDrawer);
		}
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated
	 */
	private void createConnectionsDrawer() {
		connectionsDrawer = new PaletteDrawer("Connections", null);
		List<PaletteEntry> entries = new ArrayList<PaletteEntry>();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getEventPortConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Event port connection", "Event port connection", factory,
				ThreadGroupImageRegistry
						.getImageDescriptor("EVENTPORTCONNECTION"),
				ThreadGroupImageRegistry
						.getImageDescriptor("EVENTPORTCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getDataPortConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Data port connection", "Data port connection", factory,
				ThreadGroupImageRegistry
						.getImageDescriptor("DATAPORTCONNECTION"),
				ThreadGroupImageRegistry
						.getImageDescriptor("DATAPORTCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getEventDataPortConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Event data port connection", "Event data port connection",
				factory, ThreadGroupImageRegistry
						.getImageDescriptor("EVENTDATAPORTCONNECTION"),
				ThreadGroupImageRegistry
						.getImageDescriptor("EVENTDATAPORTCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getDataAccessConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Data access connection", "Data access connection", factory,
				ThreadGroupImageRegistry
						.getImageDescriptor("DATAACCESSCONNECTION"),
				ThreadGroupImageRegistry
						.getImageDescriptor("DATAACCESSCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE
						.getSubprogramAccessConnection(), "default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Subprogram access connection", "Subprogram access connection",
				factory, ThreadGroupImageRegistry
						.getImageDescriptor("SUBPROGRAMACCESSCONNECTION"),
				ThreadGroupImageRegistry
						.getImageDescriptor("SUBPROGRAMACCESSCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE
						.getSubprogramGroupAccessConnection(), "default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Subprogram group access connection",
				"Subprogram group access connection",
				factory,
				ThreadGroupImageRegistry
						.getImageDescriptor("SUBPROGRAMGROUPACCESSCONNECTION"),
				ThreadGroupImageRegistry
						.getImageDescriptor("SUBPROGRAMGROUPACCESSCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(
				creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getAbstractFeatureConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Feature Connection", "Feature Connection", factory,
				ThreadGroupImageRegistry
						.getImageDescriptor("ABSTRACTFEATURECONNECTION"),
				ThreadGroupImageRegistry
						.getImageDescriptor("ABSTRACTFEATURECONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getParameterConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Parameter Connection", "Parameter Connection", factory,
				ThreadGroupImageRegistry
						.getImageDescriptor("PARAMETERCONNECTION"),
				ThreadGroupImageRegistry
						.getImageDescriptor("PARAMETERCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getFeatureGroupConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Feature Group Connection", "Feature Group Connection",
				factory, ThreadGroupImageRegistry
						.getImageDescriptor("FEATUREGROUPCONNECTION"),
				ThreadGroupImageRegistry
						.getImageDescriptor("FEATUREGROUPCONNECTION_LARGE")));

		connectionsDrawer.addAll(entries);
		getRoot().add(connectionsDrawer);
	}

}
