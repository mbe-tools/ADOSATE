/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.threadgroup.commands;

import org.eclipse.gef.EditPart;
import org.topcased.adele.editor.utils.commands.RestoreConnectionCommand;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;

/**
 * ADELE_EventPort restore connection command
 *
 * @generated NOT
 */
public class EventPortRestoreConnectionCommand extends RestoreConnectionCommand {

	public EventPortRestoreConnectionCommand(EditPart part) {
		super(part);
	}

	@Override
	protected void createRelationCommand(GraphElement edge,
			GraphElement srcElt, GraphElement targetElt) {
		EventPortConnectionEdgeCreationCommand cmd = new EventPortConnectionEdgeCreationCommand(
				getEditDomain(), (GraphEdge) edge, srcElt, false);
		cmd.setTarget(targetElt);
		add(cmd);
	}

}