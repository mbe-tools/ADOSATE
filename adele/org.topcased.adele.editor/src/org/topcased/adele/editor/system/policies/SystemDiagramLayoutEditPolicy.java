/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.system.policies;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.commands.Command;
import org.topcased.adele.editor.utils.commands.SetUpFromOneLevelCommand;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.draw2d.layout.PortConstraint;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy;

/**
 * @generated
 */
public class SystemDiagramLayoutEditPolicy extends ModelerLayoutEditPolicy {
	/**
	 * Default contructor.
	 *
	 * @generated
	 */
	public SystemDiagramLayoutEditPolicy() {
		super();
	}

	/**
	 * @see org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy#isAttachedToBorder(org.topcased.modeler.di.model.GraphNode)
	 * @generated NOT
	 */
	protected boolean isAttachedToBorder(GraphNode node) {
		return (new org.topcased.adele.editor.system.policies.SystemLayoutEditPolicy())
				.isAttachedToBorder(node);
	}

	/**
	 * @see org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy#isValid(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EObject)
	 * @generated NOT
	 */
	protected boolean isValid(EObject child, EObject parent) {
		if (child instanceof Component) {
			return ((Component) parent).getImplementation() == null;
		} else
			return (new org.topcased.adele.editor.system.policies.SystemLayoutEditPolicy())
					.isValid(child, parent);
	}

	@Override
	protected Command createChangeConstraintCommand(EditPart child,
			Object constraint, boolean checkConstraint) {
		if (constraint instanceof PortConstraint)
			return super.createChangeConstraintCommand(child, constraint,
					checkConstraint);
		if (isValidConstraint2((GraphicalEditPart) getHost(),
				(Rectangle) constraint))
			return super.createChangeConstraintCommand(child, constraint,
					checkConstraint);
		else {
			return new SetUpFromOneLevelCommand(child);
		}
	}

	@Override
	protected boolean isValidConstraint(GraphicalEditPart parent,
			Rectangle constraint) {
		return true;
	}

	protected boolean isValidConstraint2(GraphicalEditPart parent,
			Rectangle constraint) {
		return super.isValidConstraint(parent, constraint);
	}
}