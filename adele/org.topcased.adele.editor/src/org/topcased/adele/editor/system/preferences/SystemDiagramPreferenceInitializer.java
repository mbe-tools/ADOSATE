/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	Arnaud Schach, arnaud.schach.com
 ******************************************************************************/
package org.topcased.adele.editor.system.preferences;

import org.eclipse.core.runtime.Preferences;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.resource.StringConverter;
import org.topcased.adele.editor.ADELEPlugin;

/**
 * Initialize the preferences of System diagram
 * 
 * @generated
 */
public class SystemDiagramPreferenceInitializer extends
		AbstractPreferenceInitializer {
	private Preferences ps = ADELEPlugin.getDefault().getPluginPreferences();

	/**
	 * Initialize the default value of the preference's properties 
	 * @generated
	 */
	public void initializeDefaultPreferences() {
		// Initialize the default value of the DATA_DEFAULT_BACKGROUND_COLOR property 
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.DATA_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the DATA_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.DATA_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATA_DEFAULT_FONT property
		ps.setDefault(SystemDiagramPreferenceConstants.DATA_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the SUBPROGRAM_DEFAULT_BACKGROUND_COLOR property 
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.SUBPROGRAM_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the SUBPROGRAM_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.SUBPROGRAM_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAM_DEFAULT_FONT property
		ps.setDefault(
				SystemDiagramPreferenceConstants.SUBPROGRAM_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the PROCESS_DEFAULT_BACKGROUND_COLOR property 
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.PROCESS_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the PROCESS_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.PROCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PROCESS_DEFAULT_FONT property
		ps.setDefault(
				SystemDiagramPreferenceConstants.PROCESS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the PROCESSOR_DEFAULT_BACKGROUND_COLOR property 
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.PROCESSOR_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the PROCESSOR_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.PROCESSOR_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PROCESSOR_DEFAULT_FONT property
		ps.setDefault(
				SystemDiagramPreferenceConstants.PROCESSOR_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the MEMORY_DEFAULT_BACKGROUND_COLOR property 
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.MEMORY_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the MEMORY_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.MEMORY_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the MEMORY_DEFAULT_FONT property
		ps.setDefault(
				SystemDiagramPreferenceConstants.MEMORY_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the BUS_DEFAULT_BACKGROUND_COLOR property 
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.BUS_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the BUS_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.BUS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BUS_DEFAULT_FONT property
		ps.setDefault(SystemDiagramPreferenceConstants.BUS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the DEVICE_DEFAULT_BACKGROUND_COLOR property 
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.DEVICE_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the DEVICE_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.DEVICE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DEVICE_DEFAULT_FONT property
		ps.setDefault(
				SystemDiagramPreferenceConstants.DEVICE_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the SYSTEM_DEFAULT_BACKGROUND_COLOR property 
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.SYSTEM_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the SYSTEM_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.SYSTEM_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SYSTEM_DEFAULT_FONT property
		ps.setDefault(
				SystemDiagramPreferenceConstants.SYSTEM_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the EVENTPORT_DEFAULT_BACKGROUND_COLOR property 
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.EVENTPORT_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORT_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.EVENTPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORT_DEFAULT_FONT property
		ps.setDefault(
				SystemDiagramPreferenceConstants.EVENTPORT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the DATAPORT_DEFAULT_BACKGROUND_COLOR property 
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.DATAPORT_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAPORT_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.DATAPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAPORT_DEFAULT_FONT property
		ps.setDefault(
				SystemDiagramPreferenceConstants.DATAPORT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR property 
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_FONT property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR property 
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_FONT property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR property 
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_FONT property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the BUSACCESS_DEFAULT_BACKGROUND_COLOR property 
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.BUSACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BUSACCESS_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.BUSACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BUSACCESS_DEFAULT_FONT property
		ps.setDefault(
				SystemDiagramPreferenceConstants.BUSACCESS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the DATAACCESS_DEFAULT_BACKGROUND_COLOR property 
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.DATAACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAACCESS_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.DATAACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAACCESS_DEFAULT_FONT property
		ps.setDefault(
				SystemDiagramPreferenceConstants.DATAACCESS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the PARAMETER_DEFAULT_BACKGROUND_COLOR property 
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.PARAMETER_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PARAMETER_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.PARAMETER_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PARAMETER_DEFAULT_FONT property
		ps.setDefault(
				SystemDiagramPreferenceConstants.PARAMETER_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the EVENTPORTCONNECTION_EDGE_DEFAULT_FONT property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.EVENTPORTCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the EVENTPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.EVENTPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORTCONNECTION_EDGE_DEFAULT_ROUTER property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.EVENTPORTCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the DATAPORTCONNECTION_EDGE_DEFAULT_FONT property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.DATAPORTCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the DATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.DATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAPORTCONNECTION_EDGE_DEFAULT_ROUTER property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.DATAPORTCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FONT property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORTCONNECTION_EDGE_DEFAULT_ROUTER property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.EVENTDATAPORTCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FONT property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FONT property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the BUSACCESSCONNECTION_EDGE_DEFAULT_FONT property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.BUSACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the BUSACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.BUSACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BUSACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.BUSACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the DATAACCESSCONNECTION_EDGE_DEFAULT_FONT property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.DATAACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the DATAACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.DATAACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		ps
				.setDefault(
						SystemDiagramPreferenceConstants.DATAACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

	}
}
