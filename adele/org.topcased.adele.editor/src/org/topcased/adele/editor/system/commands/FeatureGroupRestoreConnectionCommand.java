/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.system.commands;

import org.eclipse.gef.EditPart;
import org.topcased.adele.editor.utils.commands.RestoreConnectionCommand;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;

/**
 * FeatureGroup restore connection command
 *
 * @generated NOT
 */
public class FeatureGroupRestoreConnectionCommand extends RestoreConnectionCommand {
	/**
	 * @param part the EditPart that is restored
	 * @generated
	 */
	public FeatureGroupRestoreConnectionCommand(EditPart part) {
		super(part);
	}

	/**
	 * @see org.topcased.modeler.commands.AbstractRestoreConnectionCommand#initializeCommands()
	 * @generated
	 */
//	protected void initializeCommands()
//    {
//    	
//
//    	
//        GraphElement graphElementSrc = getGraphElement();
//        EObject eObjectSrc = Utils.getElement(graphElementSrc);
//
//        if (eObjectSrc instanceof FeatureGroup)
//        {
//           	for(GraphElement graphElementTgt : getAllGraphElements())
//        	{               
//                    boolean autoRef = graphElementTgt.equals(graphElementSrc);
//                    
//                    EObject eObjectTgt = Utils.getElement(graphElementTgt);
//
//                    if (eObjectTgt instanceof FeatureGroup)
//                    {
//                        if (autoRef)
//                        {
//                            // autoRef not allowed
//                        }
//                        else
//                        {
//                            // if the graphElementSrc is the source of the edge or if it is the target and that the SourceTargetCouple is reversible
//                            createFeatureGroupConnectionFromFeatureGroupToFeatureGroup(graphElementSrc, graphElementTgt);
//                            // if graphElementSrc is the target of the edge or if it is the source and that the SourceTargetCouple is reversible
//                            createFeatureGroupConnectionFromFeatureGroupToFeatureGroup(graphElementTgt, graphElementSrc);
//                        }
//                    }
//
//                
//            }
//        }
//    }

	/**
	 * @param srcElt the source element
	 * @param targetElt the target element
	 * @generated
	 */
//	private void createFeatureGroupConnectionFromFeatureGroupToFeatureGroup(GraphElement srcElt, GraphElement targetElt)
//    {
//        FeatureGroup sourceObject = (FeatureGroup) Utils.getElement(srcElt);
//        FeatureGroup targetObject = (FeatureGroup) Utils.getElement(targetElt);
//
//    EList edgeObjectList = ((org.topcased.adele.model.ADELE_Components.System) Utils.getDiagramModelObject(srcElt)).getFeatureGroupConnection();
//        for (Iterator it = edgeObjectList.iterator(); it.hasNext();)
//        {
//        	Object obj = it.next();
//        	if (obj instanceof FeatureGroupConnection)
//        	{
//            FeatureGroupConnection edgeObject = (FeatureGroupConnection) obj;
//                if (false)
//                {
//                    // check if the relation does not exists yet
//                    List<GraphEdge> existing = getExistingEdges(srcElt, targetElt, FeatureGroupConnection.class);
//                    if (!isAlreadyPresent(existing, edgeObject))
//                    {
//                        ICreationUtils factory = getModeler().getActiveConfiguration().getCreationUtils();
//                        // restore the link with its default presentation
//                        GraphElement edge = factory.createGraphElement(edgeObject);
//                        if (edge instanceof GraphEdge)
//                        {
//                            FeatureGroupConnectionEdgeCreationCommand cmd = new FeatureGroupConnectionEdgeCreationCommand(getEditDomain(), (GraphEdge) edge, srcElt, false);
//                            cmd.setTarget(targetElt);
//                            add(cmd);
//                        }
//                    }
//                }
//            }
//        }
//    }

	@Override
	protected void createRelationCommand(GraphElement edge,
			GraphElement srcElt, GraphElement targetElt) {
		FeatureGroupConnectionEdgeCreationCommand cmd = new FeatureGroupConnectionEdgeCreationCommand(
				getEditDomain(), (GraphEdge) edge, srcElt, false);
		cmd.setTarget(targetElt);
		add(cmd);
	}
}