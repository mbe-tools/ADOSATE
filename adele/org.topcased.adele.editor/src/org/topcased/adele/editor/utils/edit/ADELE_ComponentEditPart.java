/**
 * 
 */
package org.topcased.adele.editor.utils.edit;

import org.topcased.adele.model.ADELE_Components.Bus;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Components.Data;
import org.topcased.adele.model.ADELE_Components.Device;
import org.topcased.adele.model.ADELE_Components.Memory;
import org.topcased.adele.model.ADELE_Components.Process;
import org.topcased.adele.model.ADELE_Components.Processor;
import org.topcased.adele.model.ADELE_Components.Subprogram;
import org.topcased.adele.model.ADELE_Components.SubprogramGroup;
import org.topcased.adele.model.ADELE_Components.System;
import org.topcased.adele.model.ADELE_Components.Thread;
import org.topcased.adele.model.ADELE_Components.ThreadGroup;
import org.topcased.adele.model.ADELE_Components.VirtualBus;
import org.topcased.adele.model.ADELE_Components.VirtualProcessor;

/**
 * @author Arnaud
 *
 */
public class ADELE_ComponentEditPart {

	public static boolean isValidSubcompponent (Component child, Component parent) {
		
		if (parent instanceof Data)
			return ( (child instanceof Data) ||
					 (child instanceof Subprogram)
					);
		if (parent instanceof Subprogram)
			return ( (child instanceof Data)
					);
		if (parent instanceof SubprogramGroup)
			return ( (child instanceof Subprogram) 
					);
		if (parent instanceof Thread)
			return ( (child instanceof Data) ||
					 (child instanceof Subprogram) ||
					 (child instanceof SubprogramGroup)
					);
		if (parent instanceof ThreadGroup)
			return ( (child instanceof Data) ||
					 (child instanceof Subprogram) ||
					 (child instanceof SubprogramGroup) ||
					 (child instanceof Thread) ||
					 (child instanceof ThreadGroup)
					);
		if (parent instanceof Process)
			return ( (child instanceof Data) ||
					 (child instanceof Subprogram) ||
					 (child instanceof SubprogramGroup) ||
					 (child instanceof Thread) ||
					 (child instanceof ThreadGroup)
					);
		if (parent instanceof Processor)
			return ( (child instanceof VirtualProcessor) ||
					 (child instanceof Memory) ||
					 (child instanceof Bus) ||
					 (child instanceof VirtualBus)
					);
		if (parent instanceof VirtualProcessor)
			return ( (child instanceof VirtualProcessor) ||
					 (child instanceof VirtualBus)
					);
		if (parent instanceof Memory)
			return ( (child instanceof Memory) ||
					 (child instanceof Bus)
					);
		if (parent instanceof Bus)
			return ( (child instanceof VirtualBus)
					);
		if (parent instanceof VirtualBus)
			return ( (child instanceof VirtualBus)
					);
		if (parent instanceof Device)
			return ( (child instanceof Bus)
					);
		if (parent instanceof System)
			return ( (child instanceof Data) ||
					 (child instanceof Subprogram) ||
					 (child instanceof SubprogramGroup) ||
					 (child instanceof Process) ||
					 (child instanceof Processor) ||
					 (child instanceof VirtualProcessor) ||
					 (child instanceof Memory) ||
					 (child instanceof Bus) ||
					 (child instanceof VirtualBus) ||
					 (child instanceof Device) ||
					 (child instanceof System)
					);
		
		return false;
	}
}
