package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EObject;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Features.AbstractFeature;
import org.topcased.adele.model.ADELE_Features.AccessDirection;
import org.topcased.adele.model.ADELE_Features.BusAccess;
import org.topcased.adele.model.ADELE_Features.PortDirection;
import org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy;

public class BusLayoutEditPolicy extends ModelerLayoutEditPolicy {

	public BusLayoutEditPolicy() {
		super();
	}

	@Override
	protected boolean isValid(EObject child, EObject parent) {
		return !isInvalid( child, parent );
	}

	private static boolean isInvalid(EObject child, EObject parent) {
		if (((Component)parent).isFeaturesLock())
				return true;
		if (child instanceof BusAccess) {
			return !((BusAccess) child).getDirection().equals(
					AccessDirection.REQUIRED_LITERAL);
		}
		if (child instanceof AbstractFeature) {
			return !((AbstractFeature) child).getDirection().equals( PortDirection.INOUT_LITERAL );
		}
		return false;
	}
}
	