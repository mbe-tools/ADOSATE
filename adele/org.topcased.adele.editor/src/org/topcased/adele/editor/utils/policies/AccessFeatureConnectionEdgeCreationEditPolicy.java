package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.topcased.adele.model.ADELE_Features.Access;
import org.topcased.adele.model.ADELE_Features.AccessDirection;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.utils.Utils;


public abstract class AccessFeatureConnectionEdgeCreationEditPolicy extends FeatureConnectionEdgeCreationEditPolicy {
	
	@Override
	protected boolean checkSource( final GraphElement p_source ) {
		if ( super.checkSource( p_source ) ) {
			return true;
		}

		final EObject object = Utils.getElement( p_source );

		return getComponentClass().isInstance( object );
	}
	
	protected abstract EClass getComponentClass();
	
	@Override
	protected boolean checkTargetForSource(	final GraphElement p_source,
											final GraphElement p_target ) {
		if ( super.checkTargetForSource( p_source, p_target ) ) {
			return true;
		}
		
		final EObject sourceObject = Utils.getElement( p_source );
		final EObject targetObject = Utils.getElement( p_target );
		
		if ( getComponentClass().isInstance( sourceObject ) && getFeatureClass().isInstance( targetObject ) ) {
			final SKHierarchicalObject sourceComp = (SKHierarchicalObject) sourceObject;
			final Access targetAcc = (Access) targetObject;
			
			if ( connected( sourceComp, targetAcc ) ) {
				return false;
			}

			final SKComponent featCompTarget = getComponent( targetAcc );
			
			if ( sourceComp.getParent() == featCompTarget ) {
				return AccessDirection.PROVIDED_LITERAL.equals( targetAcc.getDirection() );
			}

			return AccessDirection.REQUIRED_LITERAL.equals( targetAcc.getDirection() );
		} 
		else if ( getFeatureClass().isInstance( sourceObject ) && getComponentClass().isInstance( targetObject ) ) {
			final Access sourceAcc = (Access) sourceObject;
			final SKHierarchicalObject targetComp = (SKHierarchicalObject) targetObject;

			if ( connected( sourceAcc, targetComp ) ) {
				return false;
			}

			final SKComponent featCompSource = getComponent( sourceAcc );
			
			if ( ( targetComp ).getParent() == featCompSource ) {
				return AccessDirection.PROVIDED_LITERAL.equals( sourceAcc.getDirection() );
			}

			return AccessDirection.REQUIRED_LITERAL.equals( sourceAcc );
		}
		
		return false;
	}
}