package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EClass;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;

public abstract class AbstractFeatureConnectionEdgeCreationEditPolicy extends FeatureConnectionEdgeCreationEditPolicy {

	@Override
	protected EClass getFeatureClass() {
		return ADELE_FeaturesPackage.Literals.ABSTRACT_FEATURE;
	}
//	@Override
//	protected boolean checkTargetForSource(GraphElement source, GraphElement target) {
//		EObject sourceObject = Utils.getElement(source);
//		EObject targetObject = Utils.getElement(target);
//		PortDirection sourceDirection = ((AbstractFeature) sourceObject)
//				.getDirection();
//		PortDirection targetDirection = ((AbstractFeature) targetObject)
//				.getDirection();
//
//		if (sourceObject instanceof AbstractFeature
//				&& targetObject instanceof AbstractFeature
//				&& connectable(target, (AbstractFeature)targetObject)) {
//			if (!((sourceObject.eContainer()).equals(targetObject.eContainer()))) {
//				if (sourceDirection.equals(PortDirection.INOUT_LITERAL) || targetDirection.equals( PortDirection.INOUT_LITERAL ) ) {
//					return true;//targetDirection
//				}
//							//.equals(AbstractFeatureDirection.NONE);
////				else if (targetDirection
////						.equals(AbstractFeatureDirection.NONE))
////					return sourceDirection
////							.equals(AbstractFeatureDirection.NONE);
////				else {
//				final SKComponent compSource = ((AbstractFeature)sourceObject).getComponent();
//				final SKComponent compTarget = ((AbstractFeature)targetObject).getComponent();
//				
//				if (compSource==compTarget.getParent() || compSource.getParent()==compTarget) {
//					return (sourceDirection.equals(targetDirection));
//				}
//				//else
//				return !(sourceDirection.equals(targetDirection));
////				}
//			}
//		}
//		return false;
//	}
//	
//	@Override
//	protected boolean checkSource(GraphElement source) {
//		EObject object = Utils.getElement(source);
//		if (object instanceof AbstractFeature &&
//				connectable(source, (AbstractFeature)object)) {
//			if ( ((Feature)object).getParent() instanceof FeatureGroup ) {   
//				if (ADELE_Utils.getComponentNode(source) instanceof Diagram ) {
//					return true;
//				}
//				return false;
//			}
//			return true;
//		}
//		return false;
//	}
//
//	protected boolean connectable (GraphElement source, AbstractFeature port) {
//		
//		Component diagCompo;
//		final PortDirection direction = port.getDirection();
//		
//		if (ADELE_Utils.getComponentNode(source) instanceof Diagram) {
//			// broadcast event
//			if (direction== PortDirection.IN_LITERAL ) {
//				return true;
//			}
//			
//			diagCompo = (Component) port.getComponent();
//		}
//		else {
//			// broadcast event
//			if (direction== PortDirection.OUT_LITERAL )
//				return true;
//			diagCompo = (Component) port.getComponent().getParent();
//		}
//		for (AbstractFeatureConnection cnx : diagCompo.getAbstractFeatureConnection()) {
//			if (cnx.getObjects().contains(port))
//				return false;
//		}
//		return true;
//	}
}