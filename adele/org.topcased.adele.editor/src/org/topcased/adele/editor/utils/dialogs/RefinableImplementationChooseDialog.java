package org.topcased.adele.editor.utils.dialogs;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.topcased.adele.model.ADELE.util.ADELEModelUtils;
import org.topcased.adele.model.ADELE_Components.Component;

public class RefinableImplementationChooseDialog extends TitleAreaDialog {

	protected String matchingImplementationsName[];
	protected int selectedIndex=-1;
	protected Table table;
	protected Component currentCompo;
	
	public RefinableImplementationChooseDialog(Shell parentShell) {
		super(parentShell);
	}
	
	public void create(EList<Component> matchingImplementations, Component obj) {
		currentCompo = obj;
		matchingImplementationsName=new String[matchingImplementations.size()];
		for (int i=0; i<matchingImplementations.size(); i++){
			if (!(matchingImplementations.get(i).getImplementationName().equalsIgnoreCase("")))
				matchingImplementationsName[i]=ADELEModelUtils.getFullNamespace(matchingImplementations.get(i))+
					matchingImplementations.get(i).getName()+"."+matchingImplementations.get(i).getImplementationName();
			else
				matchingImplementationsName[i]=ADELEModelUtils.getFullNamespace(matchingImplementations.get(i))+matchingImplementations.get(i).getName();
			if (matchingImplementations.get(i)==currentCompo.getImplementation())
				selectedIndex=i;
		}
		super.create();
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {

		Composite composite = new Composite(parent, SWT.NONE);
		GridData data = new GridData(GridData.FILL_BOTH);
	    data.horizontalSpan = 2;
	    composite.setLayoutData(data);
	    GridLayout layout = new GridLayout();
	    layout.numColumns = 1;
	    composite.setLayout(layout);

	    data = new GridData(GridData.FILL_BOTH);
	    ScrolledComposite scrollPanel = new ScrolledComposite(composite, SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.BORDER);
	    scrollPanel.setLayoutData(data);
	    scrollPanel.setLayout(new GridLayout());
	    Composite tableContainer = new Composite(scrollPanel, SWT.NONE);
	    tableContainer.setLayout(new GridLayout(2,false));
		scrollPanel.setContent(tableContainer);
		
		
		table = new Table(tableContainer, SWT.BORDER	| SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		table.setLayoutData(new GridData());
		TableColumn column = new TableColumn(table, SWT.NONE);
		column.setText("Choose a classifier");

		for (int i = 0; i < matchingImplementationsName.length; i++) {
			TableItem item = new TableItem(table, SWT.NONE);
			item.setText(0, matchingImplementationsName[i]);
		}
		TableItem nullItem = new TableItem(table, SWT.NONE);
		nullItem.setText(0, "none");
		column.pack();
		column.setWidth(column.getWidth()*4);
		if (selectedIndex!=-1)
			table.select(selectedIndex);
		table.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				selectedIndex=((Table)arg0.getSource()).getSelectionIndex();
			}
		});

		table.addMouseListener(new MouseListener(){

			public void mouseDoubleClick(MouseEvent e) {
				okPressed();
			}

			public void mouseDown(MouseEvent e) {
			}

			public void mouseUp(MouseEvent e) {
			}
			
		});

		Point containerSize = tableContainer.computeSize(SWT.DEFAULT,
				SWT.DEFAULT);
		Point panelSize = scrollPanel.getSize();
		tableContainer.setSize(Math
				.max(panelSize.x - 5, containerSize.x), Math.max(
				panelSize.y - 5, containerSize.y));
		
		return super.createDialogArea(parent);
	}
	
	@Override
	public int open() {
		super.open();
		return selectedIndex;
	}
	
	@Override
	protected void cancelPressed() {
		selectedIndex=-1;
		super.cancelPressed();
	}

}
