/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE. All rights reserved. This program and the
 * accompanying materials are made available under the terms of the Eclipse
 * Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: David Sciamma (Anyware Technologies), Mathieu Garcia (Anyware
 * Technologies), Jacques Lescot (Anyware Technologies) - initial API and
 * implementation
 ******************************************************************************/
package org.topcased.adele.editor.utils.actions;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.jface.action.Action;
import org.topcased.adele.editor.utils.edit.FeatureGroupEditPart;
import org.topcased.adele.editor.utils.figures.port.FeatureGroupFigure;
import org.topcased.modeler.ModelerEditPolicyConstants;
import org.topcased.modeler.commands.ChangeBoundsCommand;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.edit.policies.NonResizableEditPolicy;
import org.topcased.modeler.edit.policies.ResizableEditPolicy;

/**
 * Action that send an AutoResize request to a set of GraphNode.<br>
 * Creation : 21 mars 2005
 * 
 * @author <a href="mailto:jacques.lescot@anyware-tech.com">Jacques LESCOT </a>
 */
public class CloseOpenFeatureGroupAction extends Action {
	/** The selected EditPart object */
	private FeatureGroupEditPart editPart;

	private CommandStack commandStack;

	/**
	 * The Constructor
	 * 
	 * @param part
	 *            the IWorkbenchPart
	 */
	public CloseOpenFeatureGroupAction(FeatureGroupEditPart portEditPart) {
		super("Open or Close the selected port group");
		editPart = portEditPart;
		commandStack = portEditPart.getViewer().getEditDomain()
				.getCommandStack();
	}

	/**
	 * Add a new diagram
	 * 
	 * @see org.eclipse.jface.action.IAction#run()
	 */
	public void run() {
		FeatureGroupFigure figure = (FeatureGroupFigure) editPart.getFigure();
		Rectangle newShape = new Rectangle();
		if (editPart.isClosed()) {
			newShape.setSize(editPart.getOpenSize());
			newShape.setLocation(figure.getLocation().x
					- figure.getParent().getBounds().getLocation().x, figure
					.getLocation().y
					- figure.getParent().getBounds().getLocation().y);
			editPart.setOpenClosedState(false);
			editPart.installEditPolicy(
					ModelerEditPolicyConstants.RESIZABLE_EDITPOLICY,
					new ResizableEditPolicy());
			editPart.removeEditPolicy("Not resizable");
		} else {
			editPart.setOpenSize(figure.getSize());
			newShape.setSize(figure.getLabel().getSize());
			newShape.setLocation(figure.getLocation().x
					- figure.getParent().getBounds().getLocation().x, figure
					.getLocation().y
					- figure.getParent().getBounds().getLocation().y);
			editPart.setOpenClosedState(true);
			editPart
					.removeEditPolicy(ModelerEditPolicyConstants.RESIZABLE_EDITPOLICY);
			editPart.installEditPolicy("Not resizable",
					new NonResizableEditPolicy());
		}
		CompoundCommand resizeCommand = new CompoundCommand();
		resizeCommand.add(new ChangeBoundsCommand((GraphNode) editPart
				.getModel(), newShape));

		commandStack.execute(resizeCommand);

	}

}
