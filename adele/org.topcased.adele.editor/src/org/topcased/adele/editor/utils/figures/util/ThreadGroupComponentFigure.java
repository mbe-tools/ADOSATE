/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/

package org.topcased.adele.editor.utils.figures.util;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.topcased.adele.common.utils.figures.ComponentFigure;

/**
 * A Figure that represent a Memory
 * 
 * Creation : 10 nov. 2005
 * 
 * @author <a href="mailto:jacques.lescot@anyware-tech.com">Jacques LESCOT</a>
 */
public class ThreadGroupComponentFigure extends ComponentFigure
{

    /**
     * @see org.eclipse.draw2d.Figure#paintBorder(org.eclipse.draw2d.Graphics)
     */
    protected void paintBorder(Graphics graphics)
    {
        graphics.pushState();
        graphics.setLineWidth(getLineWidth());
        graphics.setLineStyle(SWT.LINE_DOT);
        graphics.drawRoundRectangle(new Rectangle(bounds.getTopLeft().translate(1, 1), bounds.getBottomRight().translate(-2, -2)), 10, 10);
        graphics.popState();
    }

    /**
     * @see org.eclipse.draw2d.Figure#paintFigure(org.eclipse.draw2d.Graphics)
     */
    protected void paintFigure(Graphics graphics)
    {
        graphics.pushState();
        graphics.setLineStyle(SWT.LINE_DOT);
        graphics.fillRoundRectangle(new Rectangle(bounds.getTopLeft(), bounds.getBottomRight().translate(-2, -2)), 10, 10);
        graphics.popState();
    }

}
