/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	Arnaud Schach, arnaud.schach.com
 ******************************************************************************/
package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EClass;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;

/**
 * ADELE_BusAccessConnection utils
 */
public abstract class SubprogramGroupAccessConnectionEdgeCreationEditPolicy extends AccessFeatureConnectionEdgeCreationEditPolicy {
	
	@Override
	protected EClass getFeatureClass() {
		return ADELE_FeaturesPackage.Literals.SUBPROGRAM_GROUP_ACCESS;
	}

	@Override
	protected EClass getComponentClass() {
		return ADELE_ComponentsPackage.Literals.SUBPROGRAM_GROUP;
	}

//	@Override
//	protected boolean checkTargetForSource(GraphElement source,
//			GraphElement target) {
//		EObject sourceObject = Utils.getElement(source);
//		EObject targetObject = Utils.getElement(target);
//		if (sourceObject instanceof SubprogramGroupAccess
//				&& targetObject instanceof SubprogramGroupAccess
//				&& connectable(target, (SubprogramGroupAccess)targetObject)) {
//			if (!((sourceObject.eContainer()).equals(targetObject.eContainer()))) {
//				SKComponent compSource = ((SubprogramGroupAccess)sourceObject).getComponent();
//				SKComponent compTarget = ((SubprogramGroupAccess)targetObject).getComponent();
//				if (compSource==compTarget.getParent() || compSource.getParent()==compTarget) {
//					return (((SubprogramGroupAccess) sourceObject).getDirection()
//							.equals(((SubprogramGroupAccess) targetObject)
//									.getDirection()));
//				} else {
//					return !(((SubprogramGroupAccess) sourceObject).getDirection()
//							.equals(((SubprogramGroupAccess) targetObject)
//									.getDirection()));
//				}
//			}
//		} else if (sourceObject instanceof SubprogramGroup
//				&& targetObject instanceof SubprogramGroupAccess
//				&& connectable(target, (SubprogramGroupAccess)targetObject)) {
//			SKComponent compTarget = ((SubprogramGroupAccess)targetObject).getComponent();
//			if (((SubprogramGroup)sourceObject).getParent()==compTarget)
//				return ((SubprogramGroupAccess) targetObject).getDirection().equals(
//						AccessDirection.PROVIDED_LITERAL);
//			else
//				return ((SubprogramGroupAccess) targetObject).getDirection().equals(
//						AccessDirection.REQUIRED_LITERAL);
//		} else if (sourceObject instanceof SubprogramGroupAccess
//				&& targetObject instanceof SubprogramGroup) {
//			SKComponent compTarget = ((SubprogramGroupAccess)sourceObject).getComponent();
//			if (((SubprogramGroup)targetObject).getParent()==compTarget)
//				return ((SubprogramGroupAccess) sourceObject).getDirection().equals(
//						AccessDirection.PROVIDED_LITERAL);
//			else
//				return ((SubprogramGroupAccess) sourceObject).getDirection().equals(
//						AccessDirection.REQUIRED_LITERAL);
//		}
//		return false;
//	}
//	
//	@Override
//	protected boolean checkSource(GraphElement source) {
//		EObject object = Utils.getElement(source);
//		if ( object instanceof SubprogramGroupAccess&&
//				connectable(source, (SubprogramGroupAccess)object) ) {
//			if ( ((Feature)object).getParent() instanceof FeatureGroup ) {   
//				if (ADELE_Utils.getComponentNode(source) instanceof Diagram ) {
//					return true;
//				}
//				return false;
//			}
//			return true;
//		}
//		if (object instanceof SubprogramGroup ) {
//			return true;
//		}
//		return false;
//	}
//
//	protected boolean connectable (GraphElement source, SubprogramGroupAccess port) {
//		
//		Component diagCompo;
//		AccessDirection direction = port.getDirection();
//		
//		if (ADELE_Utils.getComponentNode(source) instanceof Diagram ) {
//			// broadcast event
//			if (direction==AccessDirection.REQUIRED_LITERAL)
//				return true;
//			diagCompo = (Component) port.getComponent();
//		} else {
//			// broadcast event
//			if (direction==AccessDirection.PROVIDED_LITERAL)
//				return true;
//			diagCompo = (Component) port.getComponent().getParent();
//		}
//		for (SubprogramGroupAccessConnection cnx : diagCompo.getSubprogramGroupAccessConnection()) {
//			if (cnx.getObjects().contains(port))
//				return false;
//		}
//		return true;
//	}
}