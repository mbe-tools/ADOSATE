package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EClass;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;


public abstract class DataAccessConnectionEdgeCreationEditPolicy extends AccessFeatureConnectionEdgeCreationEditPolicy {

	@Override
	protected EClass getFeatureClass() {
		return ADELE_FeaturesPackage.Literals.DATA_ACCESS;
	}

	@Override
	protected EClass getComponentClass() {
		return ADELE_ComponentsPackage.Literals.DATA;
	}
	
//	@Override
//	protected boolean checkTargetForSource(GraphElement source,
//			GraphElement target) {
//		EObject sourceObject = Utils.getElement(source);
//		EObject targetObject = Utils.getElement(target);
//		
////		if (targetObject!=sourceObject && sourceObject instanceof Data){
////			int tutu = 1;
////		}
//
//		if (sourceObject instanceof DataAccess
//				&& targetObject instanceof DataAccess
//				&& connectable(target, (DataAccess)targetObject)) {
//			if (!((sourceObject.eContainer()).equals(targetObject.eContainer()))) {
//				SKComponent compSource = ((DataAccess)sourceObject).getComponent();
//				SKComponent compTarget = ((DataAccess)targetObject).getComponent();
//				if (compSource==compTarget.getParent() || compSource.getParent()==compTarget){
//					return (((DataAccess) sourceObject).getDirection()
//							.equals(((DataAccess) targetObject).getDirection()));
//				} else {
//					return !(((DataAccess) sourceObject).getDirection()
//							.equals(((DataAccess) targetObject)
//									.getDirection()));
//				}
//			}
//		} else if (sourceObject instanceof Data
//				&& targetObject instanceof DataAccess
//				&& connectable(target, (DataAccess)targetObject)) {
//			SKComponent compTarget = ((DataAccess)targetObject).getComponent();
//			if (((Data)sourceObject).getParent()==compTarget)
//				return (((DataAccess) targetObject).getDirection()
//						.equals(AccessDirection.PROVIDED_LITERAL));
//			else
//				return (((DataAccess) targetObject).getDirection()
//						.equals(AccessDirection.REQUIRED_LITERAL));
//		} else if (sourceObject instanceof DataAccess
//				&& targetObject instanceof Data) {
//			SKComponent compTarget = ((DataAccess)sourceObject).getComponent();
//			if (((Data)targetObject).getParent()==compTarget)
//				return (((DataAccess) sourceObject).getDirection()
//						.equals(AccessDirection.PROVIDED_LITERAL));
//			else
//				return (((DataAccess) sourceObject).getDirection()
//						.equals(AccessDirection.REQUIRED_LITERAL));
//		}
//		return false;
//	}
//	
//	@Override
//	protected boolean checkSource(GraphElement source) {
//		EObject object = Utils.getElement(source);
//		if ( object instanceof DataAccess&&
//				connectable(source, (DataAccess)object) ) {
//			if ( ((Feature)object).getParent() instanceof FeatureGroup ) {   
//				if (ADELE_Utils.getComponentNode(source) instanceof Diagram ) {
//					return true;
//				}
//				return false;
//			}
//			return true;
//		}
//		if (object instanceof Data ) {
//			return true;
//		}
//		return false;
//	}
//
//	protected boolean connectable (GraphElement source, DataAccess port) {
//		
//		Component diagCompo;
//		AccessDirection direction = port.getDirection();
//		
//		if (ADELE_Utils.getComponentNode(source) instanceof Diagram) {
//			// broadcast event
//			if (direction==AccessDirection.REQUIRED_LITERAL)
//				return true;
//			diagCompo = (Component) port.getComponent();
//		} else {
//			// broadcast event
//			if (direction==AccessDirection.PROVIDED_LITERAL)
//				return true;
//			diagCompo = (Component) port.getComponent().getParent();
//		}
//		for (DataAccessConnection cnx : diagCompo.getDataAccessConnection()) {
//			if (cnx.getObjects().contains(port))
//				return false;
//		}
//		return true;
//	}
}