package org.topcased.adele.editor.utils;

import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.draw2d.Label;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.topcased.adele.common.utils.AdeleCommonUtils;
import org.topcased.adele.model.ADELE.util.ADELEModelUtils;
import org.topcased.adele.model.ADELE_Components.Abstract;
import org.topcased.adele.model.ADELE_Components.Bus;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Components.Data;
import org.topcased.adele.model.ADELE_Components.Device;
import org.topcased.adele.model.ADELE_Components.Memory;
import org.topcased.adele.model.ADELE_Components.Package;
import org.topcased.adele.model.ADELE_Components.Process;
import org.topcased.adele.model.ADELE_Components.Processor;
import org.topcased.adele.model.ADELE_Components.Subprogram;
import org.topcased.adele.model.ADELE_Components.SubprogramGroup;
import org.topcased.adele.model.ADELE_Components.System;
import org.topcased.adele.model.ADELE_Components.Thread;
import org.topcased.adele.model.ADELE_Components.ThreadGroup;
import org.topcased.adele.model.ADELE_Components.VirtualBus;
import org.topcased.adele.model.ADELE_Components.VirtualProcessor;
import org.topcased.adele.model.ADELE_Features.Feature;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.adele.model.ba_components.BehaviorAnnex;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.edit.GraphNodeEditPart;
import org.topcased.modeler.utils.Utils;

public class ADELE_Utils extends AdeleCommonUtils {

	public static final String PACKAGE_DIAGRAM_ID = "org.topcased.adele.editor.packages";
	
	public static void setToolTip(Component compo, GraphNodeEditPart ed){
		String text = "";
		if (getRoot(compo) instanceof System){
			if (compo.getType()!=null)
				text+="Type is: "+ADELEModelUtils.getFullNamespace(compo.getType())+compo.getType().getName()+"\n";
			else if (!(compo.getImplementationName().equalsIgnoreCase(ADELE_Utils.getDefaultImplementationName())))
				text+="Type is: "+ADELEModelUtils.getFullNamespace(compo)+compo.getName()+"\n";
			else
				text+="Type not set\n";
			if (compo.getImplementation()!=null){
				if (!(compo.getImplementation().getImplementationName().equalsIgnoreCase(ADELE_Utils.getDefaultImplementationName())))
					text+="Implementation is: "+ADELEModelUtils.getFullNamespace(compo.getImplementation())+compo.getImplementation().getName()+
					"."+compo.getImplementation().getImplementationName();
				else
					text+="Implementation is: "+ADELEModelUtils.getFullNamespace(compo.getImplementation())+compo.getImplementation().getName();
			}
			else if (!(compo.getImplementationName().equalsIgnoreCase(ADELE_Utils.getDefaultImplementationName())))
				text+="Implementation is: "+ADELEModelUtils.getFullNamespace(compo)+compo.getName()+"."+compo.getImplementationName();
			else
				text+="Implementation not set\n";
			ed.getEditableLabel().setToolTip(new Label(text));
		}
		else {
			if (compo.getType()!=null){
				text+="Type is: "+ADELEModelUtils.getFullNamespace(compo.getType())+compo.getType().getName();
				ed.getEditableLabel().setToolTip(new Label(text));
			}
		}
	}

	public static void setDefaultTypeAndImplementation(Component compo){
		if (getRoot(compo) instanceof System && 
				compo.getImplementation()==null &&
				!compo.isSubcomponentRefinement() &&
				(compo.getImplementationName()=="" || compo.getImplementationName()==null))
			compo.setImplementationName(getDefaultImplementationName());
		if (compo.getParent()!=null && !(compo.getParent() instanceof Package) &&
				compo.getParent().getParent()!=null &&
				compo.getParent().getParent().getParent()!=null &&
				compo.getParent().getParent().getParent() instanceof Package) {
			compo.setFeaturesLock(true);
		}
	}
	
	public static String getDefaultImplementationName(){
		return "impl";
	}
	
	public static String generateDefaultImplementationNameInPackage(SKHierarchicalObject newObj, EList<SKHierarchicalObject> children) {
		EList<SKHierarchicalObject> clonedChildrenList = new BasicEList<SKHierarchicalObject>();
		String parentName = newObj.getParent().getName();
		Iterator<SKHierarchicalObject> it = children.iterator();
		SKHierarchicalObject child;
		String childName = "";
		boolean nameAlreadyExists = false;
		while (it.hasNext()){
			child = it.next();
			if (child != newObj) {
				childName=child.getName();
				clonedChildrenList.add(child);
				if (newObj.getName().equalsIgnoreCase(childName.substring(childName.indexOf(".")+1)))
					nameAlreadyExists = true;
			}
		}
		if (nameAlreadyExists)
			return generateDefaultImplementationNameInPackage(newObj, clonedChildrenList,parentName,1);
		else
			return parentName + "." + newObj.getName();
	}
	
	protected  static String generateDefaultImplementationNameInPackage(SKHierarchicalObject newObj, EList<SKHierarchicalObject> children, String parentName,int cpt) {
		String name = newObj.getName();
		if (name.equalsIgnoreCase(newObj.eClass().getName()+"1"))
			name = name.substring(0, name.length()-1);
		name = name + cpt;
		Iterator<SKHierarchicalObject> it = children.iterator();
		String childName = "";
		SKHierarchicalObject child;
		while (it.hasNext()) {
			child=it.next();
			childName=child.getName();
			if (childName.substring(childName.indexOf(".")+1).equalsIgnoreCase(name) ) {
				children.remove(child);
				return generateDefaultImplementationNameInPackage(newObj, children,parentName,cpt+1);
			}
		}
		return parentName + "." + name;
	}
	
	public static EList<IResource> getAllMembers(IProject project){
		EList<IResource>allMembers=new BasicEList<IResource>();
		
		try {
			for (IResource res: project.members()){
				if (	res instanceof IFile && ((IFile)res).getFileExtension()!=null &&
						( isAdeleResource( (IFile) res ) || ((IFile)res).getFileExtension().equals("adeledi") ) )
					allMembers.add(res);
				if (res instanceof IFolder)
					allMembers.addAll(getAllMembers((IFolder)res));
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}
		
		return allMembers;
	}
	
	protected static EList<IResource> getAllMembers(IFolder folder){
		EList<IResource>allMembers=new BasicEList<IResource>();
		
		try {
			for (IResource res: folder.members()){
				if (	res instanceof IFile && ((IFile)res).getFileExtension()!=null &&
						( AdeleCommonUtils.isAdeleResource( (IFile) res ) || AdeleCommonUtils.isAdeleDiagramResource( (IFile)res ) ) )
					allMembers.add(res);
				if (res instanceof IFolder)
					allMembers.addAll(getAllMembers((IFolder)res));
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return allMembers;
	}
	
	public static GraphElement getComponentNode (GraphElement source) {
		GraphElement componentNode = (GraphElement) source.eContainer();
		
		while (Utils.getElement(componentNode) instanceof Feature) {
			componentNode = (GraphElement) componentNode.eContainer();
		}
		
		return componentNode;
	}
    
    public static String getDiagramId(EObject obj) {
    	if (obj instanceof Abstract)
    		return "org.topcased.adele.editor.adeleabstract";
    	if (obj instanceof BehaviorAnnex)
    		return "org.topcased.adele.editor.behaviorAnnex";
    	if (obj instanceof Bus)
    		return "org.topcased.adele.editor.bus";
    	if (obj instanceof Data)
    		return "org.topcased.adele.editor.data";
    	if (obj instanceof Device)
    		return "org.topcased.adele.editor.device";
    	if (obj instanceof Memory)
    		return "org.topcased.adele.editor.memory";
    	if (obj instanceof Package)
    		return PACKAGE_DIAGRAM_ID;
    	if (obj instanceof Process)
    		return "org.topcased.adele.editor.process";
    	if (obj instanceof Processor)
    		return "org.topcased.adele.editor.processor";
    	if (obj instanceof Subprogram)
    		return "org.topcased.adele.editor.subprogram";
    	if (obj instanceof SubprogramGroup)
    		return "org.topcased.adele.editor.subprogramgroup";
    	if (obj instanceof System)
    		return "org.topcased.adele.editor.system";
    	if (obj instanceof Thread)
    		return "org.topcased.adele.editor.thread";
    	if (obj instanceof ThreadGroup)
    		return "org.topcased.adele.editor.threadgroup";
    	if (obj instanceof VirtualBus)
    		return "org.topcased.adele.editor.virtualbus";
    	if (obj instanceof VirtualProcessor)
    		return "org.topcased.adele.editor.virtualprocessor";
    	return "";
    }
}
