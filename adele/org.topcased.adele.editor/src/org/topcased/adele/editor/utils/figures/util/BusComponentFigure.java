/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/

package org.topcased.adele.editor.utils.figures.util;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.PointList;
import org.topcased.adele.common.utils.figures.ComponentFigure;

/**
 * A Figure that represent a Bus
 * 
 * Creation : 10 nov. 2005
 * 
 * @author <a href="mailto:jacques.lescot@anyware-tech.com">Jacques LESCOT</a>
 */
public class BusComponentFigure extends ComponentFigure
{
    /**
     * @see org.eclipse.draw2d.Figure#paintBorder(org.eclipse.draw2d.Graphics)
     */
    protected void paintBorder(Graphics graphics)
    {
        graphics.pushState();
        graphics.setLineWidth(getLineWidth());
        PointList pl = createBusPerimeter();
        graphics.drawPolygon(pl);
        graphics.popState();
    }

    /**
     * @see org.eclipse.draw2d.Figure#paintFigure(org.eclipse.draw2d.Graphics)
     */
    protected void paintFigure(Graphics graphics)
    {
    	PointList pl = createBusPerimeter();
        graphics.fillPolygon(pl);
    }
    
    protected PointList createBusPerimeter() {
    	PointList pl = new PointList();
    	int arrow;
        
    	if (bounds.width>bounds.height) {
    		arrow = Math.max(bounds.height/10, 5);
    		
    		pl.addPoint(bounds.getLeft());
    		pl.addPoint(bounds.getTopLeft().translate(2*arrow, 0));
    		pl.addPoint(bounds.getTopLeft().translate(2*arrow, arrow));
    		pl.addPoint(bounds.getTopRight().translate(-2*arrow-1, arrow));
    		pl.addPoint(bounds.getTopRight().translate(-2*arrow-1, 0));
    		pl.addPoint(bounds.getRight().translate(-1, 0));
    		pl.addPoint(bounds.getBottomRight().translate(-2*arrow, 0));
    		pl.addPoint(bounds.getBottomRight().translate(-2*arrow-1, -arrow));
    		pl.addPoint(bounds.getBottomLeft().translate(2*arrow, -arrow));
    		pl.addPoint(bounds.getBottomLeft().translate(2*arrow, 0));
    	}
    	else {
    		arrow = Math.max(bounds.width/10, 5);
    		
    		pl.addPoint(bounds.getTop());
    		pl.addPoint(bounds.getTopRight().translate(0, 2*arrow));
    		pl.addPoint(bounds.getTopRight().translate(-arrow, 2*arrow));
    		pl.addPoint(bounds.getBottomRight().translate(-arrow, -2*arrow-1));
    		pl.addPoint(bounds.getBottomRight().translate(0, -2*arrow-1));
    		pl.addPoint(bounds.getBottom().translate(0, -1));
    		pl.addPoint(bounds.getBottomLeft().translate(0, -2*arrow-1));
    		pl.addPoint(bounds.getBottomLeft().translate(arrow, -2*arrow-1));
    		pl.addPoint(bounds.getTopLeft().translate(arrow, 2*arrow));
    		pl.addPoint(bounds.getTopLeft().translate(0, 2*arrow));
    	}
        
        return pl;
    }
}
