/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/

package org.topcased.adele.editor.utils.figures.util;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.PointList;
import org.topcased.adele.common.utils.figures.ComponentFigure;

/**
 * A Figure that represent a Processor
 * 
 * Creation : 10 nov. 2005
 * 
 * @author <a href="mailto:jacques.lescot@anyware-tech.com">Jacques LESCOT</a>
 */
public class ProcessorComponentFigure extends ComponentFigure
{

    /**
     * @see org.eclipse.draw2d.Figure#paintBorder(org.eclipse.draw2d.Graphics)
     */
    protected void paintBorder(Graphics graphics)
    {
        graphics.pushState();
        graphics.setLineWidth(getLineWidth());
        PointList pl = new PointList();
        pl.addPoint(bounds.getBottomLeft().translate(1, -2));
        pl.addPoint(bounds.getTopLeft().translate(1, 7));
        pl.addPoint(bounds.getTopLeft().translate(7, 1));
        pl.addPoint(bounds.getTopRight().translate(-1, 1));
        pl.addPoint(bounds.getBottomRight().translate(-1, -8));
        pl.addPoint(bounds.getBottomRight().translate(-7, -2));
        graphics.drawPolygon(pl);

        graphics.drawLine(bounds.getTopLeft().translate(1, 7), bounds.getTopRight().translate(-7, 7));
        graphics.drawLine(bounds.getTopRight().translate(-7, 7), bounds.getBottomRight().translate(-7, -2));
        graphics.drawLine(bounds.getTopRight().translate(-1, 1), bounds.getTopRight().translate(-7, 7));
        graphics.popState();
    }

    /**
     * @see org.eclipse.draw2d.Figure#paintFigure(org.eclipse.draw2d.Graphics)
     */
    protected void paintFigure(Graphics graphics)
    {
        PointList pl = new PointList();
        pl.addPoint(bounds.getBottomLeft().translate(1, -2));
        pl.addPoint(bounds.getTopLeft().translate(1, 7));
        pl.addPoint(bounds.getTopLeft().translate(7, 1));
        pl.addPoint(bounds.getTopRight().translate(-1, 1));
        pl.addPoint(bounds.getBottomRight().translate(-1, -8));
        pl.addPoint(bounds.getBottomRight().translate(-7, -2));
        graphics.fillPolygon(pl);
    }

}
