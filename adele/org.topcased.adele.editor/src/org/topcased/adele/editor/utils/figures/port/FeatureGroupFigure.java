/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	Arnaud Schach, arnaud.schach.com
 ******************************************************************************/
package org.topcased.adele.editor.utils.figures.port;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PositionConstants;
import org.topcased.adele.editor.ADELEImageRegistry;
import org.topcased.draw2d.figures.ContainerFigure;
import org.topcased.draw2d.figures.EditableLabel;
import org.topcased.draw2d.figures.ILabel;
import org.topcased.draw2d.figures.IPortFigure;
import org.topcased.draw2d.figures.Label;
import org.topcased.draw2d.layout.BorderAttachedLayout;

/**
 * @generated NOT
 */
public class FeatureGroupFigure extends ContainerFigure implements
		IPortFigure {

	int portPosition = 0;
	
	/**
	 * Constructor
	 * 
	 * @generated
	 */
	public FeatureGroupFigure() {
		super();
	}

	/**
	 * @see org.topcased.draw2d.figures.ContainerFigure#createContainer()
	 * @generated
	 */

	protected IFigure createContainer() {
		IFigure container = super.createContainer();
		container.setLayoutManager(new BorderAttachedLayout());
		return container;
	}

	/**
	 * Creates the header figure
	 * 
	 * @return the header figure
	 */
	protected ILabel createLabel() {		
		return new EditableLabel(ADELEImageRegistry.getImage("ADELE_FEATUREGROUP"));
	}

	/**
	 * Returns the current position of the port figure
	 * 
	 * @see org.topcased.draw2d.figures.IPortFigure#getPosition()
	 */
	public int getPosition() {
		return portPosition;
	}

	/**
	 * Set the direction
	 * 
	 * @see org.topcased.draw2d.figures.IPortFigure#setPosition(int)
	 */
	public void setPosition(int position) {
		portPosition = position;
		// figure moved event relocate the anchors
		// fireFigureMoved();
		// this.repaint();
	}
	
	@Override
	public void repaint() {
		// TODO Auto-generated method stub
		super.repaint();
		if (getPosition() == PositionConstants.RIGHT)
			((Label)getLabel()).setTextPlacement(PositionConstants.WEST);
		else
			((Label)getLabel()).setTextPlacement(PositionConstants.EAST);
			
	}
	
	@Override
	public void paint(Graphics arg0) {
		// TODO Auto-generated method stub
		super.paint(arg0);
		if (getPosition() == PositionConstants.RIGHT)
			((Label)getLabel()).setTextPlacement(PositionConstants.WEST);
		else
			((Label)getLabel()).setTextPlacement(PositionConstants.EAST);
	}
}