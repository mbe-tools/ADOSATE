package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EObject;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Features.AccessDirection;
import org.topcased.adele.model.ADELE_Features.SubprogramAccess;
import org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess;
import org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy;

public class DeviceLayoutEditPolicy extends ModelerLayoutEditPolicy {

	@Override
	protected boolean isValid(EObject child, EObject parent) {
		return !isInvalid( child, parent );
	}

	private static boolean isInvalid(EObject child, EObject parent) {
		if (((Component)parent).isFeaturesLock())
			return true;
		if (child instanceof SubprogramAccess) {
			return !((SubprogramAccess) child).getDirection().equals(
					AccessDirection.PROVIDED_LITERAL);
		}
		if (child instanceof SubprogramGroupAccess) {
			return !((SubprogramGroupAccess) child).getDirection().equals(
					AccessDirection.PROVIDED_LITERAL);
		}
		return false;
	}
}
	