package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EObject;
import org.topcased.adele.model.ADELE.util.ADELEModelUtils;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy;

public class ComponentLayoutEditPolicy extends ModelerLayoutEditPolicy {
	
	@Override
	protected boolean isValid(	final EObject p_child, 
								final EObject p_parent ) {
		if ( ( (Component) p_parent ).isFeaturesLock() ) {
			return false;
		}

		if ( p_child instanceof Component ) {
			return ADELEModelUtils.isLegalChildrenComponent( p_child, p_parent );
		}
		
		return true;
	}
}
