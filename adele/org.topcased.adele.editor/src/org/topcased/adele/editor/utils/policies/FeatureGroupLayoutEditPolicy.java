package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.topcased.adele.model.ADELE_Features.FeatureGroup;
import org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy;

public class FeatureGroupLayoutEditPolicy extends ModelerLayoutEditPolicy {
	
	@Override
	protected boolean isValid(EObject child, EObject parent) {
		return !isInvalid( child, parent, getHost() );
	}

	private static boolean isInvalid(EObject child, EObject parent, EditPart container) {
		final FeatureGroup featGroup = (FeatureGroup) parent;
		
		return !featGroup.isType();
		
//		try {
//			Class<?> parameters[] = new Class[2];
//			parameters[0]=EObject.class;
//			parameters[1]=EObject.class;
//			Object args[] = new Object[2];
//			args[0]=child;
//			args[1]=((FeatureGroup)parent).getComponent();
//			
//			while (Utils.getElement((GraphElement)container.getModel())!=((FeatureGroup)parent).getComponent())
//				container=container.getParent();
//			
//			// if the feature group is a feature of an implementation definition
//			// in a package return isInvalid=true
//			if ( !(((FeatureGroup)parent).getComponent().getParent() instanceof Package) &&
//					(((FeatureGroup)parent).getComponent().getParent().getParent() instanceof Package))
//				return true;
//			
//			EditPolicy policy = container.getEditPolicy("LayoutEditPolicy");
//			Method method = policy.getClass().getMethod("isValid", parameters);
//			return !((Boolean) method.invoke(policy, args));
//		} catch (SecurityException e) {
//			return false;
//		} catch (NoSuchMethodException e) {
//			return false;
//		} catch (IllegalArgumentException e) {
//			return false;
//		} catch (IllegalAccessException e) {
//			return false;
//		} catch (InvocationTargetException e) {
//			return false;
//		}
	}
}
	