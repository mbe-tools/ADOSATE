/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/

package org.topcased.adele.editor.utils.figures.util;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.topcased.adele.common.utils.figures.ComponentFigure;

/**
 * A Figure that represent a Data
 * 
 * Creation : 10 nov. 2005
 * 
 * @author <a href="mailto:jacques.lescot@anyware-tech.com">Jacques LESCOT</a>
 */
public class DataComponentFigure extends ComponentFigure
{

    /**
     * @see org.eclipse.draw2d.Figure#paintBorder(org.eclipse.draw2d.Graphics)
     */
    protected void paintBorder(Graphics graphics)
    {
        graphics.pushState();
        graphics.setLineWidth(getLineWidth());
        graphics.drawRectangle(new Rectangle(bounds.getTopLeft().translate(1, 1), new Dimension(bounds.width - 2, bounds.height - 2)));
        graphics.popState();
    }

    /**
     * @see org.eclipse.draw2d.Figure#paintFigure(org.eclipse.draw2d.Graphics)
     */
    protected void paintFigure(Graphics graphics)
    {
        graphics.fillRectangle(new Rectangle(bounds.getTopLeft(), new Dimension(bounds.width, bounds.height)));
    }

}
