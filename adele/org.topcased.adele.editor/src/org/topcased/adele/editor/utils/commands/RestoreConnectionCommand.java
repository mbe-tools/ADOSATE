/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.utils.commands;

import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.KernelSpices.SKRelation;
import org.topcased.modeler.commands.AbstractRestoreConnectionCommand;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.editor.IConfiguration;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.extensions.DiagramsManager;
import org.topcased.modeler.utils.Utils;


public abstract class RestoreConnectionCommand extends
AbstractRestoreConnectionCommand {

	public RestoreConnectionCommand(EditPart part) {
		super(part);
	}	
	
	private EList<SKRelation> getAllRelations(GraphElement obj) {
		EList<SKRelation> cnx = new BasicEList<SKRelation>();
		Component diagComp = (Component) Utils.getDiagramModelObject(obj);

//		cnx.addAll(diag.getAbstractFeatureConnection());
//		cnx.addAll(diag.getBusAccessConnection());
//		cnx.addAll(diag.getDataAccessConnection());
//		cnx.addAll(diag.getDataPortConnection());
//		cnx.addAll(diag.getEventDataPortConnection());
//		cnx.addAll(diag.getEventPortConnection());
//		cnx.addAll(diag.getParameterConnection());
//		cnx.addAll(diag.getSubprogramAccessConnection());
//		cnx.addAll(diag.getSubprogramGroupAccessConnection());
		cnx.addAll( diagComp.getAllRelations() );
		cnx.addAll( diagComp.getBaTransition() );
				
		return cnx;
	}

	protected void initializeCommands() {

		GraphElement graphElementSrc = getGraphElement();
		EObject eObjectSrc = Utils.getElement(graphElementSrc);
		
			EList<EObject> targets = new BasicEList<EObject>();
			HashMap<EObject, SKRelation>cnxMap = new HashMap<EObject, SKRelation>();

			for ( SKRelation cnx : getAllRelations(graphElementSrc)) {
				if (cnx.getObjects().get(0)==eObjectSrc) {
					targets.add(cnx.getObjects(). get(cnx.getObjects().size()-1));
					cnxMap.put(cnx.getObjects().get(cnx.getObjects().size()-1), cnx);
				}
			}

			for (GraphElement graphElementTgt : getAllGraphElements()) {
				boolean autoRef = graphElementTgt.equals(graphElementSrc);

				EObject eObjectTgt = Utils.getElement(graphElementTgt);

				if (targets.contains(eObjectTgt)) {
					if (autoRef) {
						// autoRef not allowed
					} 
					else {
						try {
							createRelation(graphElementSrc,graphElementTgt,cnxMap.get(eObjectTgt) );
						}
						catch( final CoreException p_ex ) {
							throw new RuntimeException( p_ex );
						}
					}
				}

			}
	}

//	protected void initializeCommands() {
//
//		GraphElement graphElementSrc = getGraphElement();
//		EObject eObjectSrc = Utils.getElement(graphElementSrc);
//
//		if (eObjectSrc instanceof ADELE_Data) {
//			EList<EObject> targets = new BasicEList<EObject>();
//			HashMap<EObject, ADELE_Relation>cnxMap = new HashMap<EObject, ADELE_Relation>();
//
//			for ( ADELE_DataAccessConnection cnx :
//				((ADELE_Data)eObjectSrc).getParent().getDataAccessConnection()) {
//				if (cnx.getObjects().get(0)==eObjectSrc) {
//					targets.add(cnx.getObjects().get(1));
//					cnxMap.put(cnx.getObjects().get(1), cnx);
//				}
//			}
//
//			for (GraphElement graphElementTgt : getAllGraphElements()) {
//				boolean autoRef = graphElementTgt.equals(graphElementSrc);
//
//				EObject eObjectTgt = Utils.getElement(graphElementTgt);
//
//				if (eObjectTgt instanceof ADELE_DataAccess &&
//						targets.contains(eObjectTgt)) {
//					if (autoRef) {
//						// autoRef not allowed
//					} else {
//						createRelation(graphElementSrc,graphElementTgt,cnxMap.get(eObjectTgt) );
//					}
//				}
//
//			}
//		}
//	}

	protected void createRelation( GraphElement srcElt, GraphElement targetElt, SKRelation cnx) 
	throws CoreException {

		// check if the relation does not exists yet
		List<GraphEdge> existing = getExistingEdges(srcElt,
				targetElt, cnx.getClass().getInterfaces()[0]);
		if (!isAlreadyPresent(existing, cnx)) {
//			ICreationUtils factory = getModeler().getActiveConfiguration().getCreationUtils();
			final Diagram diagram = getDiagram();
			final IConfiguration config = DiagramsManager.getInstance().getConfiguration( diagram.getSemanticModel().getPresentation() ); 
			ICreationUtils factory = config.getCreationUtils();
			// restore the link with its default presentation
			GraphElement edge = factory
					.createGraphElement(cnx);
			if (edge instanceof GraphEdge) {
				createRelationCommand(edge,srcElt,targetElt);
			}
		}
	}

	protected abstract void createRelationCommand(GraphElement edge, GraphElement srcElt, GraphElement targetElt) ;

}