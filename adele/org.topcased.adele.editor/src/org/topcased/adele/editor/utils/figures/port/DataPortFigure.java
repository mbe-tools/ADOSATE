/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies), Mathieu Garcia (Anyware Technologies),
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/
package org.topcased.adele.editor.utils.figures.port;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.PointList;
import org.topcased.adele.model.ADELE_Features.PortDirection;
import org.topcased.draw2d.figures.OrientedPortFigure;

/**
 * The figure to display a DataPort.
 * 
 * Creation : 19 sept. 2005
 * 
 * @author <a href="mailto:jacques.lescot@anyware-tech.com">Jacques LESCOT</a>
 */
public class DataPortFigure extends OrientedPortFigure
{
    private PortDirection portDirection;

    /**
     * Constructor
     */
    public DataPortFigure()
    {
        super();
    }

    /**
     * Constructor
     * 
     * @param portDirection the type of the Port (IN, OUT or INOUT)
     */
    public DataPortFigure(PortDirection portDirection)
    {
        super();
        this.portDirection = portDirection;
    }

    /**
     * @see org.topcased.modeler.figures.ModelerPortFigure#paintContentPaneFigure(org.eclipse.draw2d.Graphics)
     */
    @Override
    public void paintFigure(Graphics graphics)
    {
        graphics.pushState();

        graphics.setLineWidth(1);
        
        PointList pl = new PointList();

        if (getPosition() == PositionConstants.LEFT && portDirection == PortDirection.IN_LITERAL
                || getPosition() == PositionConstants.RIGHT && portDirection == PortDirection.OUT_LITERAL)
        {
            pl.addPoint(bounds.getTopLeft());
            pl.addPoint(bounds.getRight());
            pl.addPoint(bounds.getBottomLeft());
        }
        else if (getPosition() == PositionConstants.LEFT && portDirection == PortDirection.OUT_LITERAL
                || getPosition() == PositionConstants.RIGHT && portDirection == PortDirection.IN_LITERAL)
        {
            pl.addPoint(bounds.getTopRight());
            pl.addPoint(bounds.getLeft());
            pl.addPoint(bounds.getBottomRight());
        }
        else if (getPosition() == PositionConstants.BOTTOM && portDirection == PortDirection.IN_LITERAL
                || getPosition() == PositionConstants.TOP && portDirection == PortDirection.OUT_LITERAL)
        {
            pl.addPoint(bounds.getBottomLeft());
            pl.addPoint(bounds.getTop());
            pl.addPoint(bounds.getBottomRight());
        }
        else if (getPosition() == PositionConstants.BOTTOM && portDirection == PortDirection.OUT_LITERAL
                || getPosition() == PositionConstants.TOP && portDirection == PortDirection.IN_LITERAL)
        {
            pl.addPoint(bounds.getTopRight());
            pl.addPoint(bounds.getBottom());
            pl.addPoint(bounds.getTopLeft());
        }
        else
        {
            pl.addPoint(bounds.getLeft());
            pl.addPoint(bounds.getTop());
            pl.addPoint(bounds.getRight().translate(-1, 0));
            pl.addPoint(bounds.getBottom().translate(0, -1));
        }

        graphics.fillPolygon(pl);

        graphics.popState();
    }

    /**
     * @see org.eclipse.draw2d.IFigure#getPreferredSize(int, int)
     */
    public Dimension getPreferredSize(int wHint, int hHint)
    {
        return new Dimension(15, 15);
    }

    /**
     * Change the portDirection of the figure
     * 
     * @param newDir
     */
    public void setPortDirection(PortDirection newDir)
    {
        this.portDirection = newDir;
    }
}
