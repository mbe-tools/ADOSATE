/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/

package org.topcased.adele.editor.utils.figures.util;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.topcased.adele.common.utils.figures.ComponentFigure;

/**
 * A Figure that represent a Device
 * 
 * Creation : 10 nov. 2005
 * 
 * @author <a href="mailto:jacques.lescot@anyware-tech.com">Jacques LESCOT</a>
 */
public class DeviceComponentFigure extends ComponentFigure
{

    /**
     * @see org.eclipse.draw2d.Figure#paintBorder(org.eclipse.draw2d.Graphics)
     */
    protected void paintBorder(Graphics graphics)
    {
        graphics.pushState();
        graphics.setLineWidth(getLineWidth());
        graphics.drawRectangle(new Rectangle(bounds.getTopLeft().translate(1, 1), bounds.getBottomRight().translate(-2, -2)));
        graphics.drawRectangle(new Rectangle(bounds.getTopLeft().translate(6, 6), bounds.getBottomRight().translate(-7, -7)));
        graphics.drawLine(bounds.getTopLeft().translate(1, 1), bounds.getTopLeft().translate(6, 6));
        graphics.drawLine(bounds.getTopRight().translate(-1, 1), bounds.getTopRight().translate(-6, 6));
        graphics.drawLine(bounds.getBottomLeft().translate(1, -1), bounds.getBottomLeft().translate(6, -6));
        graphics.drawLine(bounds.getBottomRight().translate(-1, -1), bounds.getBottomRight().translate(-6, -6));
        graphics.popState();
    }

    /**
     * @see org.eclipse.draw2d.Figure#paintFigure(org.eclipse.draw2d.Graphics)
     */
    protected void paintFigure(Graphics graphics)
    {
        graphics.fillRectangle(new Rectangle(bounds.getTopLeft().translate(1, 1), bounds.getBottomRight().translate(-2, -2)));
    }

}
