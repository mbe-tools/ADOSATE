package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EClass;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;

public abstract class EventPortConnectionEdgeCreationEditPolicy extends FeatureConnectionEdgeCreationEditPolicy {

	protected EClass getFeatureClass() {
		return ADELE_FeaturesPackage.Literals.EVENT_PORT;
	}

//	@Override
//	protected boolean checkTargetForSource(GraphElement source, GraphElement target) {
//		EObject sourceObject = Utils.getElement(source);
//		EObject targetObject = Utils.getElement(target);
//
//		if (sourceObject instanceof EventPort
//				&& targetObject instanceof EventPort
//				&& connectable(target, (EventPort)targetObject)) {
//			if (!((sourceObject.eContainer()).equals(targetObject.eContainer()))) {
//				PortDirection sourceDirection = ((EventPort) sourceObject)
//						.getDirection();
//				PortDirection targetDirection = ((EventPort) targetObject)
//						.getDirection();
//
//				// DB Fix the connection rules.
//				if (sourceDirection.equals(PortDirection.INOUT_LITERAL) || targetDirection.equals(PortDirection.INOUT_LITERAL) ) {
//					return true;
//				}
////				targetDirection
////							.equals(PortDirection.INOUT_LITERAL);
////				else if (targetDirection
////						.equals(PortDirection.INOUT_LITERAL))
////					return sourceDirection
////							.equals(PortDirection.INOUT_LITERAL);
////				else {
//				final SKComponent compSource = ((EventPort)sourceObject).getComponent();
//				final SKComponent compTarget = ((EventPort)targetObject).getComponent();
//				
//				if (compSource==compTarget.getParent() || compSource.getParent()==compTarget) {
//					return (sourceDirection.equals(targetDirection));
//				}
////				else
//				return !(sourceDirection.equals(targetDirection));
////				}
//			}
//		}
//		
//		return false;
//	}
//	
//	@Override
//	protected boolean checkSource(GraphElement source) {
//		EObject object = Utils.getElement(source);
//		if (object instanceof EventPort &&
//				connectable(source, (EventPort)object)) {
//			if ( ((Feature)object).getParent() instanceof FeatureGroup ) {   
//				if (ADELE_Utils.getComponentNode(source) instanceof Diagram ) {
//					return true;
//				}
//				return false;
//			}
//			return true;
//		}
//		return false;
//	}
//
//	protected boolean connectable (GraphElement source, EventPort port) {
//		
//		Component diagCompo;
//		PortDirection direction = port.getDirection();
//		
//		if (ADELE_Utils.getComponentNode(source) instanceof Diagram ) {
//			// broadcast event
//			if (direction==PortDirection.IN_LITERAL)
//				return true;
//			diagCompo = (Component) port.getComponent();
//		} else {
//			// broadcast event
//			if (direction==PortDirection.OUT_LITERAL)
//				return true;
//			diagCompo = (Component) port.getComponent().getParent();
//		}
//		for (EventPortConnection cnx : diagCompo.getEventPortConnection()) {
//			if (cnx.getObjects().contains(port))
//				return false;
//		}
//		return true;
//	}
}