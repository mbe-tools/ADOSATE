package org.topcased.adele.editor.utils.edit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.Request;
import org.eclipse.gef.Tool;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.topcased.adele.common.utils.actions.CreateDiagramAction;
import org.topcased.adele.common.utils.commands.RefreshFeaturesCommand;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.adele.editor.utils.ADELE_Utils;
import org.topcased.adele.editor.utils.actions.ChangeActiveDiagramAction;
import org.topcased.adele.editor.utils.commands.SaveOldOdsValuesCommand;
import org.topcased.adele.editor.utils.dialogs.DiagramSelectionDialog;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Components.Package;
import org.topcased.adele.model.ADELE_Components.Thread;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.adele.model.KernelSpices.impl.SKHierarchicalObjectImpl;
import org.topcased.modeler.commands.GEFtoEMFCommandWrapper;
import org.topcased.modeler.commands.ReplaceNodeContainerCommand;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.diagrams.model.util.DiagramsUtils;
import org.topcased.modeler.edit.GraphNodeEditPart;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.editor.ModelerGraphicalViewer;
import org.topcased.modeler.extensions.DiagramDescriptor;
import org.topcased.modeler.extensions.DiagramsManager;
import org.topcased.modeler.utils.Utils;

public class ADELE_EMFGraphNodeEditPart extends org.topcased.adele.common.utils.edit.AdeleCommonEMFGraphNodeEditPart {

	public ADELE_EMFGraphNodeEditPart(GraphNode obj) {
		super(obj);
		
		ADELE_Utils.setParent(obj);
		ADELE_Utils.setDefaultTypeAndImplementation((Component) Utils
				.getElement(obj));
		ADELE_Utils.setToolTip((Component) Utils.getElement(obj), this);
	}

	@Override
	public void performRequest(Request request){
		if (request.getType().equals(REQ_OPEN)){

			boolean cannotCreateSubcomponents=false;
			EObject modelObject=getEObject();
			EditPartViewer viewer=getViewer();

			if (!(modelObject instanceof Package) && 
					((SKHierarchicalObject)modelObject).getParent() instanceof Package)
				cannotCreateSubcomponents=true;
			if (!(modelObject instanceof Package) &&
					!(((SKHierarchicalObject)modelObject).getParent() instanceof Package) &&
					((SKHierarchicalObject)modelObject).getParent()!=null &&
					!(((SKHierarchicalObject)modelObject).getParent().getParent() instanceof Package) &&
					((SKHierarchicalObject)modelObject).getParent().getParent()!=null &&
					((SKHierarchicalObject)modelObject).getParent().getParent().getParent() instanceof Package)
				cannotCreateSubcomponents=true;

			if (viewer instanceof ModelerGraphicalViewer && !cannotCreateSubcomponents )
			{

				// save the active tool
				Tool activeTool = viewer.getEditDomain().getActiveTool();

				// deactivate the active tool
				viewer.getEditDomain().setActiveTool(null);
				viewer.getEditDomain().getPaletteViewer().setActiveTool(null);

				// The action to execute if a the active diagram must change
				IAction action = createChangeDiagramAction(viewer, modelObject);

				if (action != null)
				{
					action.run();
				}
				else
				{
					// restore the active tool
					viewer.getEditDomain().setActiveTool(activeTool);
				}
			}
		}
		else
			super.performRequest(request);
	}

    protected IAction createChangeDiagramAction(	final EditPartViewer viewer,
    												final EObject modelObject ) {
    	if ( modelObject instanceof Component ) {
    		final Modeler editor = ((ModelerGraphicalViewer) viewer).getModelerEditor();
    		final List<DiagramDescriptor> diagramDescriptorsList = new ArrayList<DiagramDescriptor>();

	        // Retrieve the DiagramDescriptors that can be created into this EObject
	        final DiagramDescriptor[] diagramDescriptors = DiagramsManager.getInstance().getDiagrams();
	         
	        for (int i = 0; i < diagramDescriptors.length; i++) {
	        	if (diagramDescriptors[i].canCreateDiagramOn(modelObject, editor.getId())) {
	                 diagramDescriptorsList.add(diagramDescriptors[i]);
	            }
	        }
	         
	        final List<Diagram> existingDiagramList = DiagramsUtils.findAllExistingDiagram(editor.getDiagrams(), modelObject);
	        final Object res;
	         
	        if ( diagramDescriptorsList.size() == 1 && existingDiagramList.size() == 1 ) {
	        	res = existingDiagramList.get( 0 );
	        }
	        else {
	        	final DiagramSelectionDialog diagramsDlg = new DiagramSelectionDialog(	diagramDescriptorsList,
	        		 																	existingDiagramList,
	        		 																	ADELEPlugin.getActiveWorkbenchShell());
		        if (diagramsDlg.open() == Window.OK) {
		        	res = diagramsDlg.getResult()[0];
		        }
		        else {
		        	res = null;
		        }
	        }
            	
        	if ( res instanceof DiagramDescriptor ) {
        		return new CreateDiagramAction(editor, modelObject, (DiagramDescriptor) res, false);
        	}

        	if ( res instanceof Diagram ) {
        		return new ChangeActiveDiagramAction(editor, (Diagram) res, modelObject);
        	}
    		
    		return null;
    	} 

   		return super.createChangeDiagramAction(viewer, modelObject);
    }

    @Override
	protected void handleModelChanged(Notification msg) {
    	super.handleModelChanged(msg);
    	
    	// DB: Nothing to do there. We do not want to open the diagram. We can assume the name is correct since it has been
    	// generated automatically.
    	/*if (	msg.getEventType()==Notification.ADD &&
    			 ) {
    		final EList<SKHierarchicalObject> brothers = ((SKHierarchicalObject)msg.getNotifier()).getChildren();
    		String name = ((SKHierarchicalObjectImpl)msg.getNewValue()).getName();
    		if (name=="" || name==null)
    			return;
    		boolean invalideName = false;
    		for ( final SKHierarchicalObject brother : brothers ) {
   				if ( brother.getName().equals(name) && !brother.equals(msg.getNewValue())) {
    				invalideName = true;
   				}
    		}
    		
			if (!invalideName && 
					((Component)msg.getNotifier()).getImplementation()==null &&
					ComponentCompositionPolicy.isValid((EObject)msg.getNotifier(),(EObject)msg.getNewValue())) {
					
				((SKHierarchicalObjectImpl)msg.getNewValue()).setHirerarchicalChange(SKHierarchicalObjectImpl.VALID_HCHANGE);

				IAction action = createChangeDiagramAction(getViewer(), getEObject());
				if (action !=null) 
					action.run();
			}
			else{
				GraphElement elt = (GraphElement) ((GraphNodeEditPart)
						((IStructuredSelection)getViewer().getSelection()).getFirstElement()).getModel();
				((SKHierarchicalObjectImpl)msg.getNewValue()).setOldPosition(elt.getPosition());
				if (invalideName){
					Status status = new Status(IStatus.ERROR, "plugin", 0,
							"Design error", null);
					ErrorDialog.openError(Display.getCurrent().getActiveShell(),
							"Design error",
							"The component "+((SKHierarchicalObjectImpl)msg.getNotifier()).getName()
							+" already contains a component named "+name, status);
					((SKHierarchicalObjectImpl)msg.getNewValue()).setHirerarchicalChange(SKHierarchicalObjectImpl.INVALID_HCHANGE_NAME);
				}
				else if (((Component)msg.getNotifier()).getImplementation()!=null){
					Status status = new Status(IStatus.ERROR, "plugin", 0,
							"Design error", null);
					ErrorDialog.openError(Display.getCurrent().getActiveShell(),
							"Design error",
							"Components can not be added to a classifier instance.", status);
					((SKHierarchicalObjectImpl)msg.getNewValue()).setHirerarchicalChange(SKHierarchicalObjectImpl.INVALID_HCHANGE_NAME);
				}
				else
					((SKHierarchicalObjectImpl)msg.getNewValue()).setHirerarchicalChange(SKHierarchicalObjectImpl.INVALID_HCHANGE);
			}
		}
		else*/ 
    	if (msg.getEventType() == Notification.REMOVING_ADAPTER) {
			if (((SKHierarchicalObjectImpl) msg.getNotifier()).getHirerarchicalChange() == SKHierarchicalObjectImpl.INVALID_HCHANGE ||
					((SKHierarchicalObjectImpl) msg.getNotifier()).getHirerarchicalChange() == SKHierarchicalObjectImpl.INVALID_HCHANGE_NAME) {
				GraphElement elt = (GraphElement) getModel();
				GraphElement eltParent = (GraphElement) getViewer()
						.getRootEditPart().getContents().getModel();
				ReplaceNodeContainerCommand cmd = new ReplaceNodeContainerCommand(
						elt, eltParent, ((SKHierarchicalObjectImpl) msg
								.getNotifier()).getOldPosition());
				cmd.execute();
				((ModelerGraphicalViewer) getViewer()).getModelerEditor().refreshActiveDiagram();

				if (((SKHierarchicalObjectImpl) msg.getNotifier()).getHirerarchicalChange() == SKHierarchicalObjectImpl.INVALID_HCHANGE){
					Status status = new Status(IStatus.ERROR, "plugin", 0,
							"Design error", null);
					ErrorDialog.openError(Display.getCurrent().getActiveShell(),
							"Design error",
							"This composition is not allowed in AADL", status);
					((SKHierarchicalObjectImpl) msg.getNotifier())
					.setHirerarchicalChange(SKHierarchicalObjectImpl.NO_HCHANGE);
				}
			}
			else if (((SKHierarchicalObjectImpl) msg.getNotifier())
					.getHirerarchicalChange() == SKHierarchicalObjectImpl.VALID_HCHANGE){


				Modeler editor = ((ModelerGraphicalViewer) getParent().getViewer()).getModelerEditor();
				BasicEList<EModelElement> node = new BasicEList<EModelElement>();
				try {
					Iterator<?> sel = ((IStructuredSelection)getViewer().getSelection()).iterator();
					GraphNodeEditPart nodeEP;
					while (sel.hasNext()){
						nodeEP = (GraphNodeEditPart)sel.next();
						node.add((EModelElement) nodeEP.getModel());
						
						SaveOldOdsValuesCommand cmd = new SaveOldOdsValuesCommand(nodeEP);
						if (cmd.canExecute())
							cmd.execute();
						
					}
				}
				catch (Exception e){ 
					return;
				}

				SKHierarchicalObjectImpl compo = ((SKHierarchicalObjectImpl)Utils.getElement(editor.getActiveDiagram()));
				compo.setNewChildDown(node);

				((SKHierarchicalObjectImpl) msg.getNotifier())
						.setHirerarchicalChange(SKHierarchicalObjectImpl.NO_HCHANGE);
			}
		}
		else if ( msg.getEventType() == Notification.SET ) {
			if ( msg.getFeature() instanceof EReference ) {
				if ( ADELE_ComponentsPackage.Literals.COMPONENT__TYPE.equals( msg.getFeature() ) ) {//).getName().equalsIgnoreCase("type")){
					final Component comp = (Component) Utils.getElement( (GraphElement) getModel() );
			
					if ( comp.getParent() == null || !( comp.getParent().getParent() instanceof Package ) ) {
						try {
							refreshFeatures();
							
							final EditPart part = getParent();
							
							if ( part instanceof ADELE_DiagramEditPart ) {
								( (ADELE_DiagramEditPart) part ).refreshDiagramConnections();
							}
						}
						catch( final CoreException p_ex ) {
							p_ex.printStackTrace();
						}
					}
				}
				else if ( ( (EReference) msg.getFeature()).getName().equalsIgnoreCase("implementation") ) {
					Component comp = (Component)Utils.getElement((GraphElement)getModel());
					// DB: Should never set the list.
					comp.getChildren().clear();
					//((SKComponentImpl)comp).eSet(KernelSpicesPackage.SK_COMPONENT__CHILDREN, new BasicEList<SKComponent>());
				}
				else if ( ADELE_ComponentsPackage.Literals.COMPONENT__REFINES.equals( msg.getFeature() ) ) {
					final Component comp = (Component)Utils.getElement((GraphElement)getModel());
					
					if ( comp.getParent() == null || !( comp.getParent().getParent() instanceof Package ) ) {
		
						// DB: Added to fix the problem of inherited features not synchronized.
						//final Component oldRefined = (Component) msg.getOldValue();
						try {
							refreshFeatures();
						} 
						catch (CoreException e) {
							e.printStackTrace();
						}
					}
				}
			}
			else if ( 	msg.getFeature() instanceof EAttribute &&
						( (EAttribute) msg.getFeature() ).getName().equalsIgnoreCase( "name" ) ) {
				Component obj;
				if (Utils.getElement((GraphElement)getModel()) instanceof Component)
					obj = (Component)Utils.getElement((GraphElement)getModel());
				else
					return;
				
				if (((String)msg.getNewValue()).substring(((String)msg.getNewValue()).lastIndexOf(".")+1).length()>ADELEPlugin.MaxLabelSize) {
					riseNameTooLong();
				}
				
				if (!obj.getName().contains(".") && 
						obj.getParent()!=null && !(obj.getParent() instanceof Package) && 
						//obj.getParent().getParent()!=null &&
						obj.getParent().getParent() instanceof Package){
					
					String tmp = ADELE_Utils.generateDefaultImplementationNameInPackage(obj, obj.getParent().getChildren());
					obj.setName(tmp);
	
	//				obj.setName(obj.getParent().getName()+"."+obj.getName());
					obj.setFeaturesLock(true);
					obj.setType((Component)obj.getParent());
				}
				if ( obj.getParent()!=null && obj.getParent() instanceof Package){
					EList<SKHierarchicalObject> children = obj.getChildren();
					
					for ( final SKHierarchicalObject child : children ) {
						child.setName( obj.getName() + child.getName().substring( child.getName().indexOf( "." ) ) );
					}
				}
			}
		}
	}

    public void refreshFeatures() 
    throws CoreException {
    	final GraphElement graphEleme = (GraphElement) getModel();
		final EObject semanticElem = Utils.getElement( graphEleme );
		
		if ( semanticElem instanceof SKComponent ) {
			final Modeler modeler = ( (ModelerGraphicalViewer) getViewer() ).getModelerEditor();
			
			if ( modeler == null ) {
				final RefreshFeaturesCommand command = new RefreshFeaturesCommand( 	Utils.getDiagram( graphEleme ),
																					(SKComponent) semanticElem );
				
				if ( command.canExecute() ) {
					command.execute();
				}
			}
			else {
				final RefreshFeaturesCommand command = new RefreshFeaturesCommand( modeler, (SKComponent) semanticElem );
				modeler.getEditingDomain().getCommandStack().execute( new GEFtoEMFCommandWrapper( command ) );
			}
		}
	}
    
    @Override
    protected IAction createNewDiagram(Modeler editor, EObject modelObject) {
    	if (modelObject instanceof Thread) {
    		List<DiagramDescriptor> diagramDescriptorsList = new ArrayList<DiagramDescriptor>();

	         // Retrieve the DiagramDescriptors that can be created into this EObject
	         DiagramDescriptor[] diagramDescriptors = DiagramsManager.getInstance().getDiagrams();
	         for (int i = 0; i < diagramDescriptors.length; i++)
	         {
	             if (diagramDescriptors[i].canCreateDiagramOn(modelObject, editor.getId()))
	             {
	                 diagramDescriptorsList.add(diagramDescriptors[i]);
	             }
	         }
	
	         // at least one Diagram is available
	         if (diagramDescriptorsList.size() == 2)
	         {
	             return new CreateDiagramAction(editor, modelObject, (DiagramDescriptor) diagramDescriptorsList.get(0));
	         }
    	} 
    	return super.createNewDiagram(editor, modelObject);
    }
	
	private boolean riseNameTooLong() {
		Status status = new Status(IStatus.WARNING, "plugin", 0,
				"Rename warning", null);
		String errorMessage = "This component name is too long.\n";
		errorMessage += "It could result in aadl code generation errors.";
		ErrorDialog.openError(Display.getCurrent().getActiveShell(),
				"Rename warning", errorMessage, status);
		return false;
	}
}
