/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies), Mathieu Garcia (Anyware Technologies),
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/
package org.topcased.adele.editor.utils.figures.components;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.topcased.adele.common.utils.figures.ComponentContainerFigure;
import org.topcased.adele.editor.utils.figures.util.ProcessorComponentFigure;
import org.topcased.draw2d.layout.BorderAttachedLayout;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @author jako
 * @generated
 */
public class ProcessorFigure extends ComponentContainerFigure
{
    /**
     * @see com.ellidiss.modeler.aadl.utils.figures.util.ComponentContainerFigure#createContainer()
     */
    protected IFigure createContainer()
    {
        Figure container = new ProcessorComponentFigure();
        container.setLayoutManager(new BorderAttachedLayout());
        container.setOpaque(true);
        return container;
    }
}
