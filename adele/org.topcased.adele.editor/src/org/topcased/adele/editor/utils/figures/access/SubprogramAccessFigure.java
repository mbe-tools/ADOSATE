/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies), Mathieu Garcia (Anyware Technologies),
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/
package org.topcased.adele.editor.utils.figures.access;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.PointList;
import org.topcased.adele.editor.utils.figures.AdeleFeatureFigure;
import org.topcased.adele.model.ADELE_Features.AccessDirection;

/**
 * The figure to display a DataAccess.
 * 
 * Creation : 20 sept. 2005
 * 
 * @author <a href="mailto:jacques.lescot@anyware-tech.com">Jacques LESCOT</a>
 */
public class SubprogramAccessFigure extends AdeleFeatureFigure
{
    private AccessDirection accessDirection;

    /**
     * Constructor
     * 
     * @param accessDirection the type of the DataAccess
     */
    public SubprogramAccessFigure(AccessDirection accessDirection)
    {
        super();
        this.accessDirection = accessDirection;
    }

    /**
     * Constructor
     */
    public SubprogramAccessFigure()
    {
        super();
    }

    /**
     * @see org.topcased.modeler.figures.ModelerPortFigure#paintContentPaneFigure(org.eclipse.draw2d.Graphics)
     */
    protected void paintContentPaneFigure(Graphics graphics)
    {
        graphics.pushState();

        graphics.setLineWidth(1);

        PointList pl = new PointList();

        if (getPosition() == PositionConstants.LEFT && accessDirection == AccessDirection.PROVIDED_LITERAL
                || getPosition() == PositionConstants.RIGHT && accessDirection == AccessDirection.REQUIRED_LITERAL)
        {
            pl.addPoint(bounds.getTop());
            pl.addPoint(bounds.getRight().translate(-(bounds.width/5), 0));
            pl.addPoint(bounds.getBottom());
        }
        if (getPosition() == PositionConstants.LEFT && accessDirection == AccessDirection.REQUIRED_LITERAL
                || getPosition() == PositionConstants.RIGHT && accessDirection == AccessDirection.PROVIDED_LITERAL)
        {
            pl.addPoint(bounds.getTop());
            pl.addPoint(bounds.getLeft().translate(bounds.width/5, 0));
            pl.addPoint(bounds.getBottom());
        }
        if (getPosition() == PositionConstants.BOTTOM && accessDirection == AccessDirection.PROVIDED_LITERAL
                || getPosition() == PositionConstants.TOP && accessDirection == AccessDirection.REQUIRED_LITERAL)
        {
            pl.addPoint(bounds.getLeft());
            pl.addPoint(bounds.getTop().translate(0, bounds.height/5 ));
            pl.addPoint(bounds.getRight());
        }
        if (getPosition() == PositionConstants.BOTTOM && accessDirection == AccessDirection.REQUIRED_LITERAL
                || getPosition() == PositionConstants.TOP && accessDirection == AccessDirection.PROVIDED_LITERAL)
        {
            pl.addPoint(bounds.getLeft());
            pl.addPoint(bounds.getBottom().translate(0, -(bounds.height/5)) );
            pl.addPoint(bounds.getRight());
        }

        graphics.drawPolyline(pl);
        graphics.drawOval(bounds.x,bounds.y,bounds.width-1,bounds.height-1);
        

        graphics.popState();
    }

    /**
     * @see org.eclipse.draw2d.IFigure#getPreferredSize(int, int)
     */
    public Dimension getPreferredSize(int wHint, int hHint)
    {
    	if (getPosition() == PositionConstants.LEFT || getPosition() == PositionConstants.RIGHT)
    		return new Dimension(25, 15);
    	else
    		return new Dimension(15, 25);
    }

    /**
     * Change the accessDirection of the figure
     * 
     * @param newDir
     */
    public void setAccessDirection(AccessDirection newDir)
    {
        this.accessDirection = newDir;
    }
}
