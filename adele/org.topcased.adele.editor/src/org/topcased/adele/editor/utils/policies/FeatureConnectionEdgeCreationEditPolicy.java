package org.topcased.adele.editor.utils.policies;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.topcased.adele.editor.utils.ADELE_Utils;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Features.Access;
import org.topcased.adele.model.ADELE_Features.DirectedFeature;
import org.topcased.adele.model.ADELE_Features.Feature;
import org.topcased.adele.model.ADELE_Features.FeatureGroup;
import org.topcased.adele.model.ADELE_Features.PortDirection;
import org.topcased.adele.model.ADELE_Relations.Relation;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy;
import org.topcased.modeler.utils.Utils;

public abstract class FeatureConnectionEdgeCreationEditPolicy extends AbstractEdgeCreationEditPolicy {

	protected abstract EClass getFeatureClass();
	
	@Override
	protected boolean checkSource( final GraphElement p_source ) {
		final EObject object = Utils.getElement( p_source );
		
		if ( getFeatureClass().isInstance( object ) ) {
			final Feature feature = (Feature) object;
			
			if ( feature.getComponent() instanceof FeatureGroup ) {
				final SKComponent fgComponent = ( (FeatureGroup) feature.getComponent() ).getComponent();
				
				if ( fgComponent instanceof Component && ( (Component) fgComponent ).isSubcomponent() ) {
					return false;
				}
				
				if ( ADELE_Utils.getComponentNode( p_source ) instanceof Diagram ) {
					return true;
				}
				
				return false;
			}
			
			return true;
		}
		
		return false;
	}
	
	protected boolean checkClassifier( 	final Feature p_sourceFeature,
										final Feature p_targetFeature ) {
		// TODO
		return true;
	}

	protected boolean checkDirection( 	final Feature p_sourceFeature,
										final Feature p_targetFeature ) {
		if ( isBiDirectional( p_sourceFeature ) || isBiDirectional( p_targetFeature ) ) {
			return true;
		}

		final SKComponent sourceComp = getComponent( p_sourceFeature );
		final SKComponent targetComp = getComponent( p_targetFeature );

		if ( sourceComp == targetComp ) {
			return false; 
		}
		
		if( sourceComp == targetComp.getParent() || sourceComp.getParent() == targetComp ) {
			return equalsDirection( p_sourceFeature, p_targetFeature );
		}

		return !equalsDirection( p_sourceFeature, p_targetFeature );
	}
	
	protected boolean equalsDirection( 	final Feature p_feature1,
										final Feature p_feature2 ) {
		if ( p_feature1 instanceof DirectedFeature && p_feature2 instanceof DirectedFeature ) {
			final DirectedFeature dirFeat1 = (DirectedFeature) p_feature1;
			final DirectedFeature dirFeat2 = (DirectedFeature) p_feature2;
			
			return dirFeat1.getDirection() == null || dirFeat1.getDirection().equals( dirFeat2.getDirection() );
		}
		
		if ( p_feature1 instanceof Access && p_feature2 instanceof Access ) {
			final Access accFeat1 = (Access) p_feature1;
			final Access accFeat2 = (Access) p_feature2;
			
			return accFeat1.getDirection() == null || accFeat1.getDirection().equals( accFeat2.getDirection() );
		}
		
		return false;
	}
	
	protected boolean isBiDirectional( final Feature p_feature ) {
		if ( p_feature instanceof DirectedFeature ) {
			final DirectedFeature dirFeat = (DirectedFeature) p_feature;
			
			return dirFeat.getDirection() == null || PortDirection.INOUT_LITERAL.equals( dirFeat.getDirection() );
		}
		
		if ( p_feature instanceof Access ) {
			final Access dirFeat = (Access) p_feature;
			
			return dirFeat.getDirection() == null;
		}
		
		return false;
	}
	
	@Override
	protected boolean checkTargetForSource(	final GraphElement p_source,
											final GraphElement p_target ) {
		final EObject sourceObject = Utils.getElement( p_source );
		final EObject targetObject = Utils.getElement( p_target );
		
		if ( 	sourceObject instanceof Feature && targetObject instanceof Feature  &&
				sourceObject.eClass() == targetObject.eClass() ) {
			final Feature sourceFeature = (Feature) sourceObject;
			final Feature targetFeature = (Feature) targetObject;
			
			if ( targetFeature.getComponent() instanceof FeatureGroup ) {
				final SKComponent fgComponent = ( (FeatureGroup) targetFeature.getComponent() ).getComponent();
				
				if ( fgComponent instanceof Component && ( (Component) fgComponent ).isSubcomponent() ) {
					return false;
				}
 			}
			
			if ( 	checkClassifier( sourceFeature, targetFeature ) && 
					checkDirection( sourceFeature, targetFeature ) &&
					!connected( sourceFeature, targetFeature ) ) {
				return true;
			}
		}
		
		return false;

//		if (	sourceObject instanceof DataPort &&
//				targetObject instanceof DataPort &&
//				connectable(target, (DataPort)targetObject ) ) {
//			if (!((sourceObject.eContainer()).equals(targetObject.eContainer()))) {
//				final PortDirection sourceDirection = ((DataPort) sourceObject).getDirection();
//				final PortDirection targetDirection = ((DataPort) targetObject).getDirection();
//				
//				// DB Fix the connection rules.
//				if ( sourceDirection.equals( PortDirection.INOUT_LITERAL ) || targetDirection.equals( PortDirection.INOUT_LITERAL ) ) {
//					return true;
//				}
////				if (sourceDirection.equals(PortDirection.INOUT_LITERAL))
////					return targetDirection
////							.equals(PortDirection.INOUT_LITERAL);
////				else if (targetDirection
////						.equals(PortDirection.INOUT_LITERAL))
////					return sourceDirection
////							.equals(PortDirection.INOUT_LITERAL);
////				else {
//				final SKComponent compSource = ((DataPort)sourceObject).getComponent();
//				final SKComponent compTarget = ((DataPort)targetObject).getComponent();
//				
//				if ( compSource == compTarget.getParent() || compSource.getParent() == compTarget ) {
//					return (sourceDirection.equals(targetDirection));
//				}
//				//else
//				return !(sourceDirection.equals(targetDirection));
////				//}
//			}
//		}
//		
//		return false;
	}
	
	protected SKComponent getComponent( final Feature p_feature ) {
		final SKComponent comp = p_feature.getComponent();
		
		if ( comp instanceof FeatureGroup ) {
			return ( (FeatureGroup) comp ).getComponent();
		}
		
		return comp;
	}
	
	private Collection<Relation> getConnections( final SKHierarchicalObject p_object ) {
		final Component implementation = getImplementation( p_object );
		final Collection<Relation> connections = new ArrayList<Relation>();
		
		for ( final Relation relation : implementation.getAllRelations() ) {
			if ( relation.getObjects().contains( p_object ) ) {
				connections.add( relation );
			}
		}
		
		return connections;
	}
	
	private Component getImplementation( final EObject p_object ) {
		if ( p_object == null ) {
			return null;
		}
		
		final EObject parent = p_object.eContainer();
		
		if ( parent instanceof Component ) {
			final Component comp = (Component) parent;
			
			if ( comp.isImplementation() ) {
				return comp;
			}
		}
		
		return getImplementation( parent );
	}
	
	protected boolean connected(	final SKHierarchicalObject p_source,
									final SKHierarchicalObject p_target ) {
		for ( final Relation connection : getConnections( p_source ) ) {
			if ( connection.getObjects().contains( p_target ) ) {
				return true;
			}
		}
		
		return false;
		
//		if (ADELE_Utils.getComponentNode(source) instanceof Diagram ) {
//		// broadcast event
//		if (direction==PortDirection.IN_LITERAL)
//			return true;
//		//diagCompo = (Component) port.getComponent();
//	} else {
//		// broadcast event
//		if (direction==PortDirection.OUT_LITERAL)
//			return true;
		//diagCompo = (Component) port.getComponent().getParent();
	}
	
	
//	for (DataPortConnection cnx : diagCompo.getDataPortConnection()) {
//		if (cnx.getObjects().contains(port))
//			return false;
//	}
//	}

	/**
	 * This seems to prevent connecting an 
	 * @param source
	 * @param port
	 * @return
	 */
//	protected boolean connectable (GraphElement source, DataPort port) {
//		
//		//Component diagCompo;
//		PortDirection direction = port.getDirection();
//		
//		if (ADELE_Utils.getComponentNode(source) instanceof Diagram ) {
//			// broadcast event
//			if (direction==PortDirection.IN_LITERAL)
//				return true;
//			//diagCompo = (Component) port.getComponent();
//		} else {
//			// broadcast event
//			if (direction==PortDirection.OUT_LITERAL)
//				return true;
//			//diagCompo = (Component) port.getComponent().getParent();
//		}
//		
//		
////		for (DataPortConnection cnx : diagCompo.getDataPortConnection()) {
////			if (cnx.getObjects().contains(port))
////				return false;
////		}
//		return true;
//	}
}