package org.topcased.adele.editor.utils.edit;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.gef.EditPart;
import org.eclipse.jface.action.IAction;
import org.topcased.adele.common.utils.commands.RefreshChildrenCommand;
import org.topcased.adele.common.utils.commands.RefreshConnectionsCommand;
import org.topcased.adele.common.utils.commands.RefreshFeaturesCommand;
import org.topcased.adele.common.utils.edit.AdeleCommonDiagramEditPart;
import org.topcased.adele.editor.utils.actions.ChangeActiveDiagramAction;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Components.Package;
import org.topcased.adele.model.ADELE_Relations.Relation;
import org.topcased.adele.model.KernelSpices.KernelSpicesPackage;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.adele.model.KernelSpices.SKObject;
import org.topcased.adele.model.KernelSpices.SKRelation;
import org.topcased.adele.model.KernelSpices.impl.SKComponentImpl;
import org.topcased.adele.model.ba_components.BAState;
import org.topcased.adele.model.ba_relations.BATransition;
import org.topcased.modeler.commands.GEFtoEMFCommandWrapper;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.diagrams.model.util.DiagramsUtils;
import org.topcased.modeler.edit.DiagramsRootEditPart;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.editor.ModelerGraphicalViewer;
import org.topcased.modeler.utils.Utils;

public abstract class ADELE_DiagramEditPart extends AdeleCommonDiagramEditPart {
	
	/**
	 * The Constructor
	 *
	 * @param model the root model element
	 */
	public ADELE_DiagramEditPart(Diagram model) {
		super(model);
	}

	protected IAction changeDiagramAction(EObject modelObject, EditPart part) {
		Modeler editor = ((ModelerGraphicalViewer) part.getViewer())
				.getModelerEditor();

		IAction action = null;
		// We search if a parent diagram exists.

		List<Diagram> existingDiagramList;
		if (!(modelObject.eContainer() instanceof Package) &&
				modelObject.eContainer().eContainer()!=null && modelObject.eContainer().eContainer() instanceof Package)
			existingDiagramList= DiagramsUtils.findAllExistingDiagram(editor
					.getDiagrams(), modelObject.eContainer().eContainer());
		else
			existingDiagramList= DiagramsUtils.findAllExistingDiagram(editor
					.getDiagrams(), modelObject.eContainer());
		if (existingDiagramList != null && existingDiagramList.size() > 0) {
			Diagram selectedDiagram = (Diagram) ((Diagram[]) existingDiagramList
					.toArray(new Diagram[existingDiagramList.size()]))[0];
			if ( selectedDiagram != null && existingDiagramList.size() > 1 && selectedDiagram.getSemanticModel().getPresentation().endsWith("behaviorAnnex") ) {
				selectedDiagram = (Diagram) ((Diagram[]) existingDiagramList
						.toArray(new Diagram[existingDiagramList.size()]))[1];
			}
			if (selectedDiagram != null) {
				action = new ChangeActiveDiagramAction(editor, selectedDiagram, modelObject);
			}
		}

		return action;
	}
	
	protected void refreshFeatures( final EditPart p_editPart )
	throws CoreException {
		final GraphElement graphElem = (GraphElement) p_editPart.getModel();
		final EObject semanticElem = Utils.getElement( graphElem );
		
		if ( semanticElem instanceof SKComponent ) {
			final Modeler modeler = ( (ModelerGraphicalViewer) getViewer() ).getModelerEditor();
			
			if ( modeler == null ) {
				final RefreshFeaturesCommand command = new RefreshFeaturesCommand( 	Utils.getDiagram( graphElem ),
																					(SKComponent) semanticElem );
				
				if ( command.canExecute() ) {
					command.execute();
				}
			}
			else {
				final RefreshFeaturesCommand command = new RefreshFeaturesCommand( 	modeler,
																					(SKComponent) semanticElem );
				modeler.getEditingDomain().getCommandStack().execute( new GEFtoEMFCommandWrapper( command ) );
			}
		}
	}
	
	@Override
	protected void refreshDiagramFeatures()
	throws CoreException {
		refreshFeatures( this );
	}
	
	@Override
	protected void refreshDiagramSubcomponents() 
	throws CoreException {
		final GraphElement graphElementModel = (GraphElement) getModel();
		final EObject rootElement = Utils.getElement( graphElementModel );
		final Modeler modeler = ( (ModelerGraphicalViewer) getViewer() ).getModelerEditor();
		
		if ( rootElement instanceof SKHierarchicalObject ) {
			if ( modeler == null ) {
				final RefreshChildrenCommand command = new RefreshChildrenCommand( 	Utils.getDiagram( graphElementModel ),
																					(SKHierarchicalObject) rootElement );
				
				if ( command.canExecute() ) {
					command.execute();
				}
			}
			else {
				final RefreshChildrenCommand command = new RefreshChildrenCommand( modeler, (SKHierarchicalObject) rootElement );
				modeler.getEditingDomain().getCommandStack().execute( new GEFtoEMFCommandWrapper( command ) );
			}
		}
	}
	
	@Override
	protected void refreshDiagramConnections()
	throws CoreException {
		final Modeler modeler = ( (ModelerGraphicalViewer) getViewer() ).getModelerEditor();
		final GraphElement graphElem = (GraphElement) getModel();
		final EObject eObject = Utils.getElement( graphElem );
		
		if ( eObject instanceof SKHierarchicalObject ) {
			final RefreshConnectionsCommand command;
			
			if ( modeler == null ) {
				command = new RefreshConnectionsCommand( 	Utils.getDiagram( graphElem ),
															(SKHierarchicalObject) eObject );
				
				if ( command.canExecute() ) {
					command.execute();
				}
			}
			else {
				command = new RefreshConnectionsCommand( modeler, (SKHierarchicalObject) eObject );
				modeler.getEditingDomain().getCommandStack().execute( new GEFtoEMFCommandWrapper( command ) );
			}
	
			if ( !command.isEmpty() ) {
				( (DiagramsRootEditPart) getParent() ).getFigure().removeMouseListener(mouseListener);
				stack = ((ModelerGraphicalViewer) getViewer()).getModelerEditor().getEditingDomain().getCommandStack();
				stackListener = new InnerConnectionRefreshCommandStackListener(this);
				stack.addCommandStackListener(stackListener);
			}
		}
	}

//  DB: Moved call to refreshDiagramConnections to the same place where other types of refresh are called.
//	@Override
//	public void refresh() {
//		super.refresh();
//		//EObject element = Utils.getElement((GraphElement)getModel());
//		
//		//if (element instanceof Component /*&& (
//		//		((Component)element).isSubcomponentsLock() ||
//		//		((Component)element).getRefines()!=null)*/ ) {
//			refreshDiagramConnections();
//		//}
//	}
	
	@Override
	protected void addChildVisual(	final EditPart p_childEditPart, 
									final int pi_index ) {
		super.addChildVisual( p_childEditPart, pi_index );
		
		try {
			refreshFeatures( p_childEditPart );
		} 
		catch ( final CoreException p_ex ) {
			p_ex.printStackTrace();
		}
	}
	
	@Override
	protected void handleModelChanged(Notification msg) {
		if (msg.getEventType()== Notification.SET && msg.getFeature() instanceof EReference &&
				((EReference) msg.getFeature()).getName().equalsIgnoreCase("type")){

			Component comp = (Component)Utils.getElement((GraphElement) getModel());
			if (comp.getType()!=null){
				comp.getFeatures().clear();
			}
			
			try {
				refreshDiagramFeatures();
			} 
			catch (CoreException e) {
				e.printStackTrace();
			}
		}
		else if (msg.getEventType()== Notification.SET && msg.getFeature() instanceof EReference &&
				((EReference) msg.getFeature()).getName().equalsIgnoreCase("implementation")){

			Component comp = (Component)Utils.getElement((GraphElement)getModel());
			((SKComponentImpl)comp).eSet(KernelSpicesPackage.SK_COMPONENT__CHILDREN, new BasicEList<SKComponent>());
			
			try {
				refreshDiagramSubcomponents();
				refreshDiagramFeatures();
				refreshDiagramConnections();
			}
			catch (CoreException e) {
				e.printStackTrace();
			}
		}
		else if (	msg.getEventType() == Notification.SET && msg.getFeature() instanceof EReference &&
					ADELE_ComponentsPackage.Literals.COMPONENT__REFINES.equals( msg.getFeature() ) ){
			try {
				refreshDiagramSubcomponents();
				refreshDiagramFeatures();
				refreshDiagramConnections();
			} 
			catch (CoreException e) {
				e.printStackTrace();
			}
		}
		else if (msg.getEventType()== Notification.ADD) {
			Object newValue = msg.getNewValue();
			
			if (newValue instanceof SKComponent && ((SKComponent)newValue).getParent()==null) {
				((SKComponent)msg.getNewValue()).setParent((Component)Utils.getElement((GraphElement) getModel()));
			}
		}
		else if (msg.getEventType()==Notification.REMOVE) {
			
			if (msg.getOldValue() instanceof Component) {
				Component container = (Component)msg.getNotifier();
				Component removedObj = (Component)msg.getOldValue();
				
				//Removing related AbstractFeatureConnection
				EList<Relation> cnxToRemove = new BasicEList<Relation>();
				for (Relation cnn : container.getAbstractFeatureConnection()){
					 if (isConnectionToBeRemoved(cnn, removedObj) ) {
						cnxToRemove.add(cnn);
					}
				}
				container.getAbstractFeatureConnection().removeAll(cnxToRemove);

				//Removing related BusAccessConnection
				cnxToRemove = new BasicEList<Relation>();
				for (Relation cnn : container.getBusAccessConnection()){
					 if (isConnectionToBeRemoved(cnn, removedObj) ) {
						cnxToRemove.add(cnn);
					}
				}
				container.getBusAccessConnection().removeAll(cnxToRemove);

				//Removing related DataAccessConnection
				cnxToRemove = new BasicEList<Relation>();
				for (Relation cnn : container.getDataAccessConnection()){
					 if (isConnectionToBeRemoved(cnn, removedObj) ) {
						cnxToRemove.add(cnn);
					}
				}
				container.getDataAccessConnection().removeAll(cnxToRemove);

				//Removing related DataPortConnection
				cnxToRemove = new BasicEList<Relation>();
				for (Relation cnn : container.getDataPortConnection()){
					 if (isConnectionToBeRemoved(cnn, removedObj) ) {
						cnxToRemove.add(cnn);
					}
				}
				container.getDataPortConnection().removeAll(cnxToRemove);

				//Removing related EventDataPortConnection
				cnxToRemove = new BasicEList<Relation>();
				for (Relation cnn : container.getEventDataPortConnection()){
					 if (isConnectionToBeRemoved(cnn, removedObj) ) {
						cnxToRemove.add(cnn);
					}
				}
				container.getEventDataPortConnection().removeAll(cnxToRemove);

				//Removing related EventPortConnection
				cnxToRemove = new BasicEList<Relation>();
				for (Relation cnn : container.getEventPortConnection()){
					 if (isConnectionToBeRemoved(cnn, removedObj) ) {
						cnxToRemove.add(cnn);
					}
				}
				container.getEventPortConnection().removeAll(cnxToRemove);

				//Removing related ParameterConnection
				cnxToRemove = new BasicEList<Relation>();
				for (Relation cnn : container.getParameterConnection()){
					 if (isConnectionToBeRemoved(cnn, removedObj) ) {
						cnxToRemove.add(cnn);
					}
				}
				container.getParameterConnection().removeAll(cnxToRemove);

				//Removing related SubprogramAccessConnection
				cnxToRemove = new BasicEList<Relation>();
				for (Relation cnn : container.getSubprogramAccessConnection()){
					 if (isConnectionToBeRemoved(cnn, removedObj) ) {
						cnxToRemove.add(cnn);
					}
				}
				container.getSubprogramAccessConnection().removeAll(cnxToRemove);

				//Removing related SubprogramGroupAccessConnection
				cnxToRemove = new BasicEList<Relation>();
				for (Relation cnn : container.getSubprogramGroupAccessConnection()){
					 if (isConnectionToBeRemoved(cnn, removedObj) ) {
						cnxToRemove.add(cnn);
					}
				}
				container.getSubprogramGroupAccessConnection().removeAll(cnxToRemove);
			} else if (msg.getOldValue() instanceof BAState) {
				Component container = (Component)msg.getNotifier();
				BAState removedObj = (BAState)msg.getOldValue();
				
				//Removing related BaTransition
				EList<BATransition> baCnxToRemove = new BasicEList<BATransition>();
				for (BATransition cnn : container.getBaTransition()){
					 if (isConnectionToBeRemoved(cnn, removedObj) ) {
						baCnxToRemove.add(cnn);
					}
				}
				container.getBaTransition().removeAll(baCnxToRemove);

			}
			
		}
		super.handleModelChanged(msg);
	}
	
	private boolean isConnectionToBeRemoved(SKRelation cnn, SKComponent removedObject) {
		
		EList<SKObject> objects = cnn.getObjects();
		if (objects.size()==2) {
			if (objects.get(0)==removedObject || objects.get(1)==removedObject ||
					objects.get(0).eContainer()==removedObject || objects.get(1).eContainer()==removedObject) {
				return true;
			}
		} else if (objects.size()==1) {
			if (objects.get(0)==removedObject || objects.get(0).eContainer()==removedObject ) {
				return true;
			}
		} else {
			return true;
		}
		
		return false;
	}
}