/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies), Mathieu Garcia (Anyware Technologies),
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/
package org.topcased.adele.editor.utils.figures.port;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.PointList;
import org.topcased.adele.editor.utils.figures.AdeleFeatureFigure;
import org.topcased.adele.model.ADELE_Features.PortDirection;

/**
 * The figure to display an EventDataPort.
 * 
 * Creation : 19 sept. 2005
 * 
 * @author <a href="mailto:jacques.lescot@anyware-tech.com">Jacques LESCOT</a>
 */
public class EventDataPortFigure extends AdeleFeatureFigure
{
    private PortDirection portDirection;

    /**
     * Constructor
     * 
     * @param portDirection the type of the Port (IN, OUT or INOUT)
     */
    public EventDataPortFigure(PortDirection portDirection)
    {
        super();
        this.portDirection = portDirection;
    }

    /**
     * Constructor
     */
    public EventDataPortFigure()
    {
        super();
    }

    /**
     * @see org.topcased.modeler.figures.ModelerPortFigure#paintContentPaneFigure(org.eclipse.draw2d.Graphics)
     */
    protected void paintContentPaneFigure(Graphics graphics)
    {
        graphics.pushState();

        graphics.setLineWidth(1);

        PointList pl1 = new PointList();
        PointList pl2 = new PointList();

        if (getPosition() == PositionConstants.LEFT && portDirection == PortDirection.IN_LITERAL
                || getPosition() == PositionConstants.RIGHT && portDirection == PortDirection.OUT_LITERAL)
        {
            pl1.addPoint(bounds.getTopLeft());
            pl1.addPoint(bounds.getRight().translate(-1, 0));
            pl1.addPoint(bounds.getBottomLeft().translate(0, -1));
            pl2.addPoint(bounds.getTopLeft().translate(0, 2));
            pl2.addPoint(bounds.getRight().translate(-5, 0));
            pl2.addPoint(bounds.getBottomLeft().translate(0, -3));
        }
        else if (getPosition() == PositionConstants.LEFT && portDirection == PortDirection.OUT_LITERAL
                || getPosition() == PositionConstants.RIGHT && portDirection == PortDirection.IN_LITERAL)
        {
            pl1.addPoint(bounds.getTopRight().translate(-1, 0));
            pl1.addPoint(bounds.getLeft());
            pl1.addPoint(bounds.getBottomRight().translate(-1, -1));
            pl2.addPoint(bounds.getTopRight().translate(-1, 2));
            pl2.addPoint(bounds.getLeft().translate(4, 0));
            pl2.addPoint(bounds.getBottomRight().translate(-1, -3));
        }
        else if (getPosition() == PositionConstants.BOTTOM && portDirection == PortDirection.IN_LITERAL
                || getPosition() == PositionConstants.TOP && portDirection == PortDirection.OUT_LITERAL)
        {
            pl1.addPoint(bounds.getBottomLeft().translate(0, -1));
            pl1.addPoint(bounds.getTop());
            pl1.addPoint(bounds.getBottomRight().translate(-1, -1));
            pl2.addPoint(bounds.getBottomLeft().translate(2, -1));
            pl2.addPoint(bounds.getTop().translate(0, 4));
            pl2.addPoint(bounds.getBottomRight().translate(-3, -1));
        }
        else if (getPosition() == PositionConstants.BOTTOM && portDirection == PortDirection.OUT_LITERAL
                || getPosition() == PositionConstants.TOP && portDirection == PortDirection.IN_LITERAL)
        {
            pl1.addPoint(bounds.getTopLeft());
            pl1.addPoint(bounds.getBottom().translate(0, -1));
            pl1.addPoint(bounds.getTopRight().translate(-1, 0));
            pl2.addPoint(bounds.getTopLeft().translate(2, -1));
            pl2.addPoint(bounds.getBottom().translate(0, -5));
            pl2.addPoint(bounds.getTopRight().translate(-3, 0));
        }
        else
        {
            pl1.addPoint(bounds.getLeft());
            pl1.addPoint(bounds.getTop());
            pl1.addPoint(bounds.getRight().translate(-1, 0));
            pl1.addPoint(bounds.getBottom().translate(0, -1));
            pl1.addPoint(bounds.getLeft());
            pl2.addPoint(bounds.getLeft().translate(2, 0));
            pl2.addPoint(bounds.getTop().translate(0, 2));
            pl2.addPoint(bounds.getRight().translate(-3, 0));
            pl2.addPoint(bounds.getBottom().translate(0, -3));
        }

        graphics.drawPolyline(pl1);
        graphics.fillPolygon(pl2);

        graphics.popState();
    }

    /**
     * @see org.eclipse.draw2d.IFigure#getPreferredSize(int, int)
     */
    public Dimension getPreferredSize(int wHint, int hHint)
    {
        return new Dimension(15, 15);
    }

    /**
     * Change the portDirection of the figure
     * 
     * @param newDir
     */
    public void setPortDirection(PortDirection newDir)
    {
        this.portDirection = newDir;
    }
}
