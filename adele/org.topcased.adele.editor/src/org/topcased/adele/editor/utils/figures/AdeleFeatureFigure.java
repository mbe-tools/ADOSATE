package org.topcased.adele.editor.utils.figures;

import org.eclipse.draw2d.Graphics;
import org.topcased.draw2d.figures.OrientedPortFigure;

public abstract class AdeleFeatureFigure extends OrientedPortFigure {

    /**
     * Draw the port according its type and its position on the border
     * 
     * @see org.eclipse.draw2d.Figure#paintFigure(org.eclipse.draw2d.Graphics)
     */
	@Override
    public void paintFigure( final Graphics p_graphics ) {
        super.paintFigure( p_graphics );
        
        paintContentPaneFigure( p_graphics );
    }

	@Override
    protected void fillShape(Graphics graphics) {
		// Prevent the default shape to be drawn.
	}
	
	@Override
    protected void outlineShape(Graphics graphics) {
		// Prevent the default shape to be drawn.
	}

	protected abstract void paintContentPaneFigure( final Graphics p_graphics );
}
