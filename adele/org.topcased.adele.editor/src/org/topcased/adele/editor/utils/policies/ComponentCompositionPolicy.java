package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EObject;
import org.topcased.adele.model.ADELE_Components.Bus;
import org.topcased.adele.model.ADELE_Components.Data;
import org.topcased.adele.model.ADELE_Components.Device;
import org.topcased.adele.model.ADELE_Components.Memory;
import org.topcased.adele.model.ADELE_Components.Package;
import org.topcased.adele.model.ADELE_Components.Process;
import org.topcased.adele.model.ADELE_Components.Processor;
import org.topcased.adele.model.ADELE_Components.Subprogram;
import org.topcased.adele.model.ADELE_Components.System;
import org.topcased.adele.model.ADELE_Components.Thread;
import org.topcased.adele.model.ADELE_Components.ThreadGroup;

public class ComponentCompositionPolicy {

	public static boolean isValid (EObject parent, EObject child){
		if(parent instanceof Data)
			return 
				(child instanceof Data)||
				(child instanceof Subprogram);
		else if (parent instanceof Memory)
			return 
				(child instanceof Memory);
		else if (parent instanceof Package)
			return 
				(child instanceof Bus)||
				(child instanceof Data)||
				(child instanceof Device)||
				(child instanceof Memory)||
				(child instanceof Process)||
				(child instanceof Processor)||
				(child instanceof System);
		else if (parent instanceof Process)
			return 
				(child instanceof Data)||
				(child instanceof Subprogram)||
				(child instanceof Thread)||
				(child instanceof ThreadGroup);
		else if (parent instanceof Processor)
			return 
				(child instanceof Memory);	
		else if (parent instanceof Subprogram)
			return 
				(child instanceof Data);
		else if (parent instanceof System)
			return 
				(child instanceof Bus)||
				(child instanceof Data)||
				(child instanceof Device)||
				(child instanceof Memory)||
				(child instanceof Process)||
				(child instanceof Processor)||
				(child instanceof Subprogram)||
				(child instanceof System);
		else if (parent instanceof Thread)
			return 
				(child instanceof Data)||
				(child instanceof Subprogram);
		else if (parent instanceof ThreadGroup)
			return 
				(child instanceof Data)||
				(child instanceof Subprogram)||
				(child instanceof Thread)||
				(child instanceof ThreadGroup);
		else
			return false;
	}
}
