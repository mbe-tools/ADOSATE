package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EClass;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;

public abstract class FeatureGroupConnectionEdgeCreationEditPolicy extends FeatureConnectionEdgeCreationEditPolicy {

	protected EClass getFeatureClass() {
		return ADELE_FeaturesPackage.Literals.FEATURE_GROUP;
	}
}
