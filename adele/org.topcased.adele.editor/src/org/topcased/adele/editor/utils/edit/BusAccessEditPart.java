/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	Arnaud Schach, arnaud.schach.com
 ******************************************************************************/
package org.topcased.adele.editor.utils.edit;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.topcased.adele.editor.utils.figures.access.BusAccessFigure;
import org.topcased.adele.model.ADELE_Features.AccessDirection;
import org.topcased.adele.model.ADELE_Features.BusAccess;
import org.topcased.draw2d.figures.LabelledPortFigure;
import org.topcased.modeler.edit.EMFGraphNodeEditPart;

/**
 * The ADELE_BusAccess utils
 */
public class BusAccessEditPart {

	public static IFigure createFigure(BusAccess access) {
		return new LabelledPortFigure(new BusAccessFigure(access.getDirection()));
	}
	
	public static void handleModelChanged(Notification msg, EMFGraphNodeEditPart current) {
		Object newObject = msg.getNewValue();

		if (msg.getNotifier() instanceof BusAccess) {
			// Handle the refresh of the Parameter when the user changes the
			// Direction
			if (newObject instanceof AccessDirection
					&& ((BusAccess) msg.getNotifier()).getDirection() != msg
							.getOldValue()) {
				((BusAccessFigure) ((LabelledPortFigure) current.getFigure())
						.getPortFigure())
						.setAccessDirection(((BusAccess) msg
								.getNotifier()).getDirection());
			} else
	        	ADELE_FeatureEditPart.handleModelChanged( msg, current);
		}
	}

}