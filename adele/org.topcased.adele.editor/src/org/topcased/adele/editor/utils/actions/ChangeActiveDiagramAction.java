/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies), Mathieu Garcia (Anyware Technologies),
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/
package org.topcased.adele.editor.utils.actions;

import org.eclipse.emf.ecore.EObject;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.editor.Modeler;

/**
 * Create a new Diagram linked with a model object. <br>
 * 
 * @author jako
 */
public class ChangeActiveDiagramAction extends org.topcased.adele.common.utils.actions.ChangeActiveDiagramAction
{

    public ChangeActiveDiagramAction(Modeler ed, Diagram diag, EObject modelObject) {
		super(ed, diag, modelObject);
	}

}
