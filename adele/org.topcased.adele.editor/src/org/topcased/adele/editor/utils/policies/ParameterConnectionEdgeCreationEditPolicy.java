package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;
import org.topcased.adele.model.ADELE_Features.DataPort;
import org.topcased.adele.model.ADELE_Features.DirectedFeature;
import org.topcased.adele.model.ADELE_Features.EventDataPort;
import org.topcased.adele.model.ADELE_Features.Feature;
import org.topcased.adele.model.ADELE_Features.Parameter;
import org.topcased.adele.model.ADELE_Features.PortDirection;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.utils.Utils;


public abstract class ParameterConnectionEdgeCreationEditPolicy extends FeatureConnectionEdgeCreationEditPolicy {

	@Override
	protected EClass getFeatureClass() {
		return ADELE_FeaturesPackage.Literals.PARAMETER;
	}
	
	@Override
	protected boolean checkTargetForSource(	final GraphElement p_source,
											final GraphElement p_target ) {
		if ( super.checkTargetForSource( p_source, p_target ) ) {
			return true;
		}
		
		final EObject sourceObject = Utils.getElement( p_source );
		final EObject targetObject = Utils.getElement( p_target );

		if ( 	( sourceObject instanceof DataPort || sourceObject instanceof EventDataPort ) && 
				targetObject instanceof Parameter ) {
			if ( !sourceObject.eContainer().equals( targetObject.eContainer() ) ) {
				final DirectedFeature sourceFeature = (DirectedFeature) sourceObject ;
				final DirectedFeature targetFeature = (DirectedFeature) targetObject ;
				
				if ( connected( sourceFeature, targetFeature ) ) {
					return false;
				}
				
				final PortDirection sourceDirection = sourceFeature.getDirection();
				final PortDirection targetDirection = targetFeature.getDirection();
				
				if ( PortDirection.INOUT_LITERAL.equals( sourceDirection  ) || PortDirection.INOUT_LITERAL.equals( targetDirection  ) ) {
					return true;
				}

				final SKComponent compSource = getComponent( (Feature) sourceObject );
				final SKComponent compTarget = getComponent( (Feature)targetObject );

				if ( compSource == compTarget.getParent() || compSource.getParent() == compTarget ) {
					return sourceDirection.equals( targetDirection );
				}

				return !sourceDirection.equals(targetDirection);
			}
		}
		else if ( 	( targetObject instanceof DataPort || targetObject instanceof EventDataPort ) &&
					sourceObject instanceof Parameter ) {
			if ( !sourceObject.eContainer().equals( targetObject.eContainer() ) ) {
				final DirectedFeature sourceFeature = (DirectedFeature) sourceObject ;
				final DirectedFeature targetFeature = (DirectedFeature) targetObject ;
				
				if ( connected( sourceFeature, targetFeature ) ) {
					return false;
				}
				
				final PortDirection sourceDirection = sourceFeature.getDirection();
				final PortDirection targetDirection = targetFeature.getDirection();
				
				if ( PortDirection.INOUT_LITERAL.equals( sourceDirection ) || PortDirection.INOUT_LITERAL.equals( targetDirection ) ) {
					return true;
				}

				final SKComponent compSource = getComponent( (Feature) sourceObject );
				final SKComponent compTarget = getComponent( (Feature) targetObject );
				
				if ( compSource == compTarget.getParent() || compSource.getParent() == compTarget ) {
					return sourceDirection.equals( targetDirection );
				}

				return !sourceDirection.equals(targetDirection);
			}
		} 
//		else if (sourceObject instanceof EventDataPort
//				&& targetObject instanceof Parameter
//				&& connectable(target, (Feature)targetObject)) {
//			if (!((sourceObject.eContainer()).equals(targetObject.eContainer()))) {
//				PortDirection sourceDirection = ((EventDataPort) sourceObject)
//						.getDirection();
//				PortDirection targetDirection = ((Parameter) targetObject)
//						.getDirection();
//				if (sourceDirection.equals(PortDirection.INOUT_LITERAL))
//					return targetDirection
//							.equals(PortDirection.INOUT_LITERAL);
//				else if (targetDirection
//						.equals(PortDirection.INOUT_LITERAL))
//					return sourceDirection
//							.equals(PortDirection.INOUT_LITERAL);
//				else {
//					SKComponent compSource = ((Feature)sourceObject).getComponent();
//					SKComponent compTarget = ((Feature)targetObject).getComponent();
//					if (compSource==compTarget.getParent() || compSource.getParent()==compTarget)
//						return (sourceDirection.equals(targetDirection));
//					else
//						return !(sourceDirection.equals(targetDirection));
//				}
//			}
//		} else if (targetObject instanceof EventDataPort
//				&& sourceObject instanceof Parameter
//				&& connectable(target, (Feature)targetObject)) {
//			if (!((sourceObject.eContainer()).equals(targetObject.eContainer()))) {
//				PortDirection sourceDirection = ((Parameter) sourceObject)
//						.getDirection();
//				PortDirection targetDirection = ((EventDataPort) targetObject)
//						.getDirection();
//				if (sourceDirection.equals(PortDirection.INOUT_LITERAL))
//					return targetDirection
//							.equals(PortDirection.INOUT_LITERAL);
//				else if (targetDirection
//						.equals(PortDirection.INOUT_LITERAL))
//					return sourceDirection
//							.equals(PortDirection.INOUT_LITERAL);
//				else {
//					SKComponent compSource = ((Feature)sourceObject).getComponent();
//					SKComponent compTarget = ((Feature)targetObject).getComponent();
//					if (compSource==compTarget.getParent() || compSource.getParent()==compTarget)
//						return (sourceDirection.equals(targetDirection));
//					else
//						return !(sourceDirection.equals(targetDirection));
//				}
//			}
//		} 
		return false;
	}
	
//	@Override
//	protected boolean checkSource(GraphElement source) {
//		EObject object = Utils.getElement(source);
//		if ( (object instanceof Parameter ||
//				object instanceof DataPort ||
//				object instanceof EventDataPort) &&
//				connectable(source, (Feature)object)) {
//			if ( ((Feature)object).getParent() instanceof FeatureGroup ) {   
//				if (ADELE_Utils.getComponentNode(source) instanceof Diagram ) {
//					return true;
//				}
//				return false;
//			}
//			return true;
//		}
//		return false;
//	}

//	protected boolean connectable (GraphElement source, Feature port) {
//		
//		Component diagCompo;
//		PortDirection direction=null;
//		
//		if (port instanceof Parameter)
//			direction= ((Parameter) port).getDirection();
//		else if (port instanceof DataPort)
//			direction= ((DataPort) port).getDirection();
//		else if (port instanceof EventDataPort)
//			direction= ((EventDataPort) port).getDirection();
//		
//		if (ADELE_Utils.getComponentNode(source) instanceof Diagram ) {
//			// broadcast event
//			if (direction==PortDirection.IN_LITERAL)
//				return true;
//			diagCompo = (Component) port.getComponent();
//		} else {
//			// broadcast event
//			if (direction==PortDirection.OUT_LITERAL)
//				return true;
//			diagCompo = (Component) port.getComponent().getParent();
//		}
//		for (ParameterConnection cnx : diagCompo.getParameterConnection()) {
//			if (cnx.getObjects().contains(port))
//				return false;
//		}
//		return true;
//	}
}