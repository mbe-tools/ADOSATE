package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EObject;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Features.AccessDirection;
import org.topcased.adele.model.ADELE_Features.DataAccess;
import org.topcased.adele.model.ADELE_Features.EventDataPort;
import org.topcased.adele.model.ADELE_Features.EventPort;
import org.topcased.adele.model.ADELE_Features.PortDirection;
import org.topcased.adele.model.ADELE_Features.SubprogramAccess;
import org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess;
import org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy;

public class SubprogramLayoutEditPolicy extends ModelerLayoutEditPolicy {

	@Override
	protected boolean isValid(EObject child, EObject parent) {
		return !isInvalid( child, parent );
	}
	
	private static boolean isInvalid(EObject child, EObject parent) {
		if (((Component)parent).isFeaturesLock())
			return true;
		if (child instanceof DataAccess)
			return (((DataAccess)child).getDirection() 
					!= AccessDirection.REQUIRED_LITERAL);
		else if (child instanceof SubprogramAccess)
			return (((SubprogramAccess)child).getDirection() 
					!= AccessDirection.REQUIRED_LITERAL);
		else if (child instanceof EventPort)
			return (((EventPort)child).getDirection() 
					!= PortDirection.OUT_LITERAL);
		else if (child instanceof EventDataPort)
			return (((EventDataPort)child).getDirection() 
					!= PortDirection.OUT_LITERAL);
		else if (child instanceof SubprogramGroupAccess) {
			return !((SubprogramGroupAccess) child).getDirection().equals(
					AccessDirection.REQUIRED_LITERAL);
		}
		else return false;
	}
}
	