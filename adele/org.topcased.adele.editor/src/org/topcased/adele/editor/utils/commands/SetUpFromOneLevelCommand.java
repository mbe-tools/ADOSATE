/**
 * 
 */
package org.topcased.adele.editor.utils.commands;

import org.eclipse.emf.common.util.EList;
import org.eclipse.gef.EditPart;
import org.topcased.adele.editor.utils.policies.ComponentCompositionPolicy;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;

/**
 * @author Arnaud
 *
 */
public class SetUpFromOneLevelCommand extends org.topcased.adele.common.utils.commands.SetUpFromOneLevelCommand {
	
	public SetUpFromOneLevelCommand(EditPart _part) {
		super(_part);
	}
	
	@Override
	public boolean canExecute() {
		boolean valideName = true;
		try {
			EList<SKHierarchicalObject> brothers = element.getParent().getParent().getChildren();
			String name = element.getName();
			for (int i=0;i<brothers.size();i++){
				if (((SKComponent)brothers.get(i)).getName().equals(name) &&
						!((SKComponent)brothers.get(i)).equals(element))
				valideName = false;
			}
		}
		catch (Exception e) {
			valideName = false;
		}
		return valideName && ComponentCompositionPolicy.isValid(
				(Component) element.getParent().eContainer(),element);
	}
}
