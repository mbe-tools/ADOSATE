/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	Arnaud Schach, arnaud.schach.com
 ******************************************************************************/
package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EClass;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;

/**
 * ADELE_BusAccessConnection utils
 */
public abstract class BusAccessConnectionEdgeCreationEditPolicy extends AccessFeatureConnectionEdgeCreationEditPolicy {

	@Override
	protected EClass getFeatureClass() {
		return ADELE_FeaturesPackage.Literals.BUS_ACCESS;
	}

	@Override
	protected EClass getComponentClass() {
		return ADELE_ComponentsPackage.Literals.BUS;
	}
//	@Override
//	protected boolean checkTargetForSource(GraphElement source,
//			GraphElement target) {
//		EObject sourceObject = Utils.getElement(source);
//		EObject targetObject = Utils.getElement(target);
//		if (sourceObject instanceof BusAccess
//				&& targetObject instanceof BusAccess
//				&& connectable(target, (BusAccess)targetObject)) {
//			if (!((sourceObject.eContainer()).equals(targetObject.eContainer()))) {
//				SKComponent compSource = ((BusAccess)sourceObject).getComponent();
//				SKComponent compTarget = ((BusAccess)targetObject).getComponent();
//				if (compSource==compTarget.getParent() || compSource.getParent()==compTarget) {
//					return (((BusAccess) sourceObject).getDirection()
//							.equals(((BusAccess) targetObject)
//									.getDirection()));
//				} else {
//					return !(((BusAccess) sourceObject).getDirection()
//							.equals(((BusAccess) targetObject)
//									.getDirection()));
//				}
//			}
//		} else if (sourceObject instanceof Bus
//				&& targetObject instanceof BusAccess
//				&& connectable(target, (BusAccess)targetObject)) {
//			SKComponent compTarget = ((BusAccess)targetObject).getComponent();
//			if (((Bus)sourceObject).getParent()==compTarget)
//				return ((BusAccess) targetObject).getDirection().equals(
//						AccessDirection.PROVIDED_LITERAL);
//			else
//				return ((BusAccess) targetObject).getDirection().equals(
//						AccessDirection.REQUIRED_LITERAL);
//		} else if (sourceObject instanceof BusAccess
//			&& targetObject instanceof Bus) {
//		SKComponent compTarget = ((BusAccess)sourceObject).getComponent();
//		if (((Bus)targetObject).getParent()==compTarget)
//			return ((BusAccess) sourceObject).getDirection().equals(
//					AccessDirection.PROVIDED_LITERAL);
//		else
//			return ((BusAccess) sourceObject).getDirection().equals(
//					AccessDirection.REQUIRED_LITERAL);
//	}
//		return false;
//	}
//	
//	@Override
//	protected boolean checkSource(GraphElement source) {
//		EObject object = Utils.getElement(source);
//		if ( object instanceof BusAccess&&
//				connectable(source, (BusAccess)object) ) {
//			if ( ((Feature)object).getParent() instanceof FeatureGroup ) {   
//				if (ADELE_Utils.getComponentNode(source) instanceof Diagram ) {
//					return true;
//				}
//				return false;
//			}
//			return true;
//		}
//		if (object instanceof org.topcased.adele.model.ADELE_Components.Bus ) {
//			return true;
//		}
//		return false;
//	}
//
//	protected boolean connectable (GraphElement source, BusAccess port) {
//		
//		Component diagCompo;
//		AccessDirection direction = port.getDirection();
//		
//		if (ADELE_Utils.getComponentNode(source) instanceof Diagram) {
//			// broadcast event
//			if (direction==AccessDirection.REQUIRED_LITERAL)
//				return true;
//			diagCompo = (Component) port.getComponent();
//		} else {
//			// broadcast event
//			if (direction==AccessDirection.PROVIDED_LITERAL)
//				return true;
//			diagCompo = (Component) port.getComponent().getParent();
//		}
//		for (BusAccessConnection cnx : diagCompo.getBusAccessConnection()) {
//			if (cnx.getObjects().contains(port))
//				return false;
//		}
//		return true;
//	}
}