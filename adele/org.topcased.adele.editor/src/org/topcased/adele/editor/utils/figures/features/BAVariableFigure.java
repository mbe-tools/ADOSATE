/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/

package org.topcased.adele.editor.utils.figures.features;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.topcased.draw2d.figures.BorderedLabel;

/**
 * A Figure that represent a Subprogram
 * 
 * Creation : 28 nov. 2005
 * 
 * @author <a href="mailto:jacques.lescot@anyware-tech.com">Jacques LESCOT</a>
 */
public class BAVariableFigure extends BorderedLabel
{
    private static final int HORIZONTAL_MARGIN = 5;

    /**
     * Constructor <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public BAVariableFigure()
    {
        super();
    }

    /**
     * @see org.topcased.draw2d.figures.BorderedLabel#outlineShape(org.eclipse.draw2d.Graphics)
     */
    protected void outlineShape(Graphics graphics)
    {
        graphics.drawOval(getBounds().getTranslated(-1, -1));
    }

    /**
     * @see org.topcased.draw2d.figures.BorderedLabel#fillShape(org.eclipse.draw2d.Graphics)
     */
    protected void fillShape(Graphics graphics)
    {
        graphics.fillOval(getBounds());
    }

    /**
     * @see org.eclipse.draw2d.Figure#getPreferredSize(int, int)
     */
    public Dimension getPreferredSize(int wHint, int hHint)
    {
        Dimension optimalDim = super.getPreferredSize(wHint, hHint).getCopy();
        if (wHint == -1)
        {
            optimalDim.width = optimalDim.width + HORIZONTAL_MARGIN * 2;
        }
        return optimalDim;
    }
}
