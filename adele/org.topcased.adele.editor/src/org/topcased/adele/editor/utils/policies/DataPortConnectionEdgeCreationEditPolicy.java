package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EClass;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;

public abstract class DataPortConnectionEdgeCreationEditPolicy extends FeatureConnectionEdgeCreationEditPolicy {

	@Override
	protected EClass getFeatureClass() {
		return ADELE_FeaturesPackage.Literals.DATA_PORT;
	}
//	@Override
//	protected boolean checkTargetForSource(GraphElement source, GraphElement target) {
//		EObject sourceObject = Utils.getElement(source);
//		EObject targetObject = Utils.getElement(target);
//
//		if (	sourceObject instanceof DataPort &&
//				targetObject instanceof DataPort &&
//				connectable(target, (DataPort)targetObject ) ) {
//			if (!((sourceObject.eContainer()).equals(targetObject.eContainer()))) {
//				final PortDirection sourceDirection = ((DataPort) sourceObject).getDirection();
//				final PortDirection targetDirection = ((DataPort) targetObject).getDirection();
//				
//				// DB Fix the connection rules.
//				if ( sourceDirection.equals( PortDirection.INOUT_LITERAL ) || targetDirection.equals( PortDirection.INOUT_LITERAL ) ) {
//					return true;
//				}
////				if (sourceDirection.equals(PortDirection.INOUT_LITERAL))
////					return targetDirection
////							.equals(PortDirection.INOUT_LITERAL);
////				else if (targetDirection
////						.equals(PortDirection.INOUT_LITERAL))
////					return sourceDirection
////							.equals(PortDirection.INOUT_LITERAL);
////				else {
//				final SKComponent compSource = ((DataPort)sourceObject).getComponent();
//				final SKComponent compTarget = ((DataPort)targetObject).getComponent();
//				
//				if ( compSource == compTarget.getParent() || compSource.getParent() == compTarget ) {
//					return (sourceDirection.equals(targetDirection));
//				}
//				//else
//				return !(sourceDirection.equals(targetDirection));
////				//}
//			}
//		}
//		
//		return false;
//	}
//	
//	@Override
//	protected boolean checkSource(GraphElement source) {
//		EObject object = Utils.getElement(source);
//		if (object instanceof DataPort &&
//				connectable(source, (DataPort)object)) {
//			if ( ((Feature)object).getParent() instanceof FeatureGroup ) {   
//				if (ADELE_Utils.getComponentNode(source) instanceof Diagram ) {
//					return true;
//				}
//				return false;
//			}
//			return true;
//		}
//		return false;
//	}
//
//	protected boolean connectable (GraphElement source, DataPort port) {
//		
//		//Component diagCompo;
//		PortDirection direction = port.getDirection();
//		
//		if (ADELE_Utils.getComponentNode(source) instanceof Diagram ) {
//			// broadcast event
//			if (direction==PortDirection.IN_LITERAL)
//				return true;
//			//diagCompo = (Component) port.getComponent();
//		} else {
//			// broadcast event
//			if (direction==PortDirection.OUT_LITERAL)
//				return true;
//			//diagCompo = (Component) port.getComponent().getParent();
//		}
//		
//		
////		for (DataPortConnection cnx : diagCompo.getDataPortConnection()) {
////			if (cnx.getObjects().contains(port))
////				return false;
////		}
//		return true;
//	}
}