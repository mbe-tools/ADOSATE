/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	Arnaud Schach, arnaud.schach.com
 ******************************************************************************/
package org.topcased.adele.editor.utils.edit;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.gef.EditPart;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.widgets.Display;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.modeler.edit.EMFGraphNodeEditPart;

public class ADELE_FeatureEditPart {

    public static void handleModelChanged(	final Notification msg, 
    										final EMFGraphNodeEditPart p_editPart ) {
        if (msg.getEventType() == Notification.SET && msg.getFeature() instanceof EAttribute &&
				((EAttribute) msg.getFeature()).getName().equalsIgnoreCase("name")) {
        	if (((String)msg.getNewValue()).substring(((String)msg.getNewValue()).lastIndexOf(".")+1).length()>ADELEPlugin.MaxLabelSize) {
				riseNameTooLong();
			}
        	
        	try {
				refreshAllFeatures( p_editPart );
			} 
        	catch (CoreException e) {
				e.printStackTrace();
			}
        }
    }

	private static void riseNameTooLong() {
		Status status = new Status(IStatus.WARNING, "plugin", 0,
				"Rename warning", null);
		String errorMessage = "This feature name is too long.\n";
		errorMessage += "It could result in aadl code generation errors.";
		ErrorDialog.openError(Display.getCurrent().getActiveShell(),
				"Rename warning", errorMessage, status);
	}

	private static void refreshAllFeatures( final EditPart p_editPart ) 
	throws CoreException {
		final EditPart rootEditPart = p_editPart.getRoot();
		
		// Ensure that all inherited features are updated
    	if ( rootEditPart instanceof ADELE_DiagramEditPart ) {
    		( (ADELE_DiagramEditPart) rootEditPart ).refreshDiagramFeatures();
    	}
	}
}