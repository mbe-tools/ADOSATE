/**
 * 
 */
package org.topcased.adele.editor.utils.commands;

import org.eclipse.gef.EditPart;


/**
 * @author Arnaud
 *
 */
public class SaveOldOdsValuesCommand extends org.topcased.adele.common.utils.commands.SaveOldOdsValuesCommand {
	
	public SaveOldOdsValuesCommand(EditPart _part) {
		super(_part);
	}
}
