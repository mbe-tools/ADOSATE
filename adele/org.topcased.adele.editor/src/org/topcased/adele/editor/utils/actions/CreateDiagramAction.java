/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies), Mathieu Garcia (Anyware Technologies),
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/
package org.topcased.adele.editor.utils.actions;

import org.eclipse.emf.ecore.EObject;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.extensions.DiagramDescriptor;

/**
 * Create a new Diagram linked with a model object. <br>
 * 
 * @author jako
 */
public class CreateDiagramAction extends org.topcased.adele.common.utils.actions.CreateDiagramAction
{

    /**
     * Constructor
     * 
     * @param ed the Modeler object
     * @param obj the model object associated with the diagram
     * @param diagram the DiagramDescriptor associated with the diagram to create
     */
    public CreateDiagramAction(Modeler ed, EObject obj, DiagramDescriptor diagram)
    {
        super(ed, obj, diagram);
    }

}
