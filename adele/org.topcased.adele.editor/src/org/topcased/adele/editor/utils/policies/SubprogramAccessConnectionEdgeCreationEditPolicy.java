/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	Arnaud Schach, arnaud.schach.com
 ******************************************************************************/
package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EClass;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;

/**
 * ADELE_BusAccessConnection utils
 */
public abstract class SubprogramAccessConnectionEdgeCreationEditPolicy extends AccessFeatureConnectionEdgeCreationEditPolicy {
	
	@Override
	protected EClass getFeatureClass() {
		return ADELE_FeaturesPackage.Literals.SUBPROGRAM_ACCESS;
	}

	@Override
	protected EClass getComponentClass() {
		return ADELE_ComponentsPackage.Literals.SUBPROGRAM;
	}
//	@Override
//	protected boolean checkTargetForSource(GraphElement source,
//			GraphElement target) {
//		EObject sourceObject = Utils.getElement(source);
//		EObject targetObject = Utils.getElement(target);
//		if (sourceObject instanceof SubprogramAccess
//				&& targetObject instanceof SubprogramAccess
//				&& connectable(target, (SubprogramAccess)targetObject)) {
//			if (!((sourceObject.eContainer()).equals(targetObject.eContainer()))) {
//				SKComponent compSource = ((SubprogramAccess)sourceObject).getComponent();
//				SKComponent compTarget = ((SubprogramAccess)targetObject).getComponent();
//				if (compSource==compTarget.getParent() || compSource.getParent()==compTarget) {
//					return (((SubprogramAccess) sourceObject).getDirection()
//							.equals(((SubprogramAccess) targetObject)
//									.getDirection()));
//				} else {
//					return !(((SubprogramAccess) sourceObject).getDirection()
//							.equals(((SubprogramAccess) targetObject)
//									.getDirection()));
//				}
//			}
//		} else if (sourceObject instanceof Subprogram
//				&& targetObject instanceof SubprogramAccess
//				&& connectable(target, (SubprogramAccess)targetObject)) {
//			SKComponent compTarget = ((SubprogramAccess)targetObject).getComponent();
//			if (((Subprogram)sourceObject).getParent()==compTarget)
//				return ((SubprogramAccess) targetObject).getDirection().equals(
//						AccessDirection.PROVIDED_LITERAL);
//			else
//				return ((SubprogramAccess) targetObject).getDirection().equals(
//						AccessDirection.REQUIRED_LITERAL);
//		} else if (sourceObject instanceof SubprogramAccess
//				&& targetObject instanceof Subprogram) {
//			SKComponent compTarget = ((SubprogramAccess)sourceObject).getComponent();
//			if (((Subprogram)targetObject).getParent()==compTarget)
//				return ((SubprogramAccess) sourceObject).getDirection().equals(
//						AccessDirection.PROVIDED_LITERAL);
//			else
//				return ((SubprogramAccess) sourceObject).getDirection().equals(
//						AccessDirection.REQUIRED_LITERAL);
//		}
//		return false;
//	}
//	
//	@Override
//	protected boolean checkSource(GraphElement source) {
//		EObject object = Utils.getElement(source);
//		if ( object instanceof SubprogramAccess&&
//				connectable(source, (SubprogramAccess)object) ) {
//			if ( ((Feature)object).getParent() instanceof FeatureGroup ) {   
//				if (ADELE_Utils.getComponentNode(source) instanceof Diagram ) {
//					return true;
//				}
//				return false;
//			}
//			return true;
//		}
//		if (object instanceof Subprogram ) {
//			return true;
//		}
//		return false;
//	}
//
//	protected boolean connectable (GraphElement source, SubprogramAccess port) {
//		
//		Component diagCompo;
//		AccessDirection direction = port.getDirection();
//		
//		if (ADELE_Utils.getComponentNode(source) instanceof Diagram ) {
//			// broadcast event
//			if (direction==AccessDirection.REQUIRED_LITERAL)
//				return true;
//			diagCompo = (Component) port.getComponent();
//		} else {
//			// broadcast event
//			if (direction==AccessDirection.PROVIDED_LITERAL)
//				return true;
//			diagCompo = (Component) port.getComponent().getParent();
//		}
//		for (SubprogramAccessConnection cnx : diagCompo.getSubprogramAccessConnection()) {
//			if (cnx.getObjects().contains(port))
//				return false;
//		}
//		return true;
//	}
}