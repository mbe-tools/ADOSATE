package org.topcased.adele.editor.utils.edit;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.Request;
import org.eclipse.gef.Tool;
import org.eclipse.jface.action.IAction;
import org.topcased.adele.common.utils.actions.CreateDiagramAction;
import org.topcased.adele.model.ADELE_Components.Thread;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.editor.ModelerGraphicalViewer;
import org.topcased.modeler.extensions.DiagramDescriptor;
import org.topcased.modeler.extensions.DiagramsManager;

public class ADELE_EMFGraphEdgeEditPart extends org.topcased.adele.common.utils.edit.AdeleCommonEMFGraphEdgeEditPart {

	public ADELE_EMFGraphEdgeEditPart(GraphEdge conn) {
		super(conn);
	}

	@Override
	public void performRequest(Request request){
		if (request.getType().equals(REQ_OPEN)){

			EObject modelObject=getEObject();
			EditPartViewer viewer=getViewer();

			if (viewer instanceof ModelerGraphicalViewer  )
			{

				// save the active tool
				Tool activeTool = viewer.getEditDomain().getActiveTool();

				// deactivate the active tool
				viewer.getEditDomain().setActiveTool(null);
				viewer.getEditDomain().getPaletteViewer().setActiveTool(null);

				// The action to execute if a the active diagram must change
				IAction action = createChangeDiagramAction(viewer, modelObject);

				if (action != null)
				{
					action.run();
				}
				else
				{
					// restore the active tool
					viewer.getEditDomain().setActiveTool(activeTool);
				}
			}
		}
		else
			super.performRequest(request);
	}

//    protected IAction createChangeDiagramAction(EditPartViewer viewer, EObject modelObject)
//    {
//    	if (modelObject instanceof BATransition) {
//    		
//    		Modeler editor = ((ModelerGraphicalViewer) viewer).getModelerEditor();
//    		List<DiagramDescriptor> diagramDescriptorsList = new ArrayList<DiagramDescriptor>();
//
//	         // Retrieve the DiagramDescriptors that can be created into this EObject
//	         DiagramDescriptor[] diagramDescriptors = DiagramsManager.getInstance().getDiagrams();
//	         for (int i = 0; i < diagramDescriptors.length; i++)
//	         {
//	             try {
//					if (diagramDescriptors[i].getConfiguration().getId().equals("org.topcased.adele.editor.baTransitionCondition") ||
//							diagramDescriptors[i].getConfiguration().getId().equals("org.topcased.adele.editor.baActionBlock"))
//					 {
//					     diagramDescriptorsList.add(diagramDescriptors[i]);
//					 }
//				} catch (CoreException e) {
//				}
//	         }
//	         List<Diagram> existingDiagramList = DiagramsUtils.findAllExistingDiagram(editor.getDiagrams(), modelObject);
//
//	         DiagramSelectionDialog diagramsDlg = new  DiagramSelectionDialog(diagramDescriptorsList, existingDiagramList, ADELEPlugin.getActiveWorkbenchShell());
//            if (diagramsDlg.open() == Window.OK)
//            {
//            	Object res = diagramsDlg.getResult()[0];
//            	if (res instanceof DiagramDescriptor)
//            		return new CreateDiagramAction(editor, modelObject, (DiagramDescriptor) res, false);
//            	else if (res instanceof Diagram)
//            		return new ChangeActiveDiagramAction(editor, (Diagram) res, modelObject);
//            }
//    		
//    		return null;
//    	} else {
//    		return super.createChangeDiagramAction(viewer, modelObject);
//    	}
//    }
    

    
    @Override
    protected IAction createNewDiagram(Modeler editor, EObject modelObject) {
    	if (modelObject instanceof Thread) {
    		List<DiagramDescriptor> diagramDescriptorsList = new ArrayList<DiagramDescriptor>();

	         // Retrieve the DiagramDescriptors that can be created into this EObject
	         DiagramDescriptor[] diagramDescriptors = DiagramsManager.getInstance().getDiagrams();
	         for (int i = 0; i < diagramDescriptors.length; i++)
	         {
	             if (diagramDescriptors[i].canCreateDiagramOn(modelObject, editor.getId()))
	             {
	                 diagramDescriptorsList.add(diagramDescriptors[i]);
	             }
	         }
	
	         // at least one Diagram is available
	         if (diagramDescriptorsList.size() == 2)
	         {
	             return new CreateDiagramAction(editor, modelObject, (DiagramDescriptor) diagramDescriptorsList.get(0));
	         }
    	} 
    	return super.createNewDiagram(editor, modelObject);
    }
}
