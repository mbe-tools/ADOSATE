package org.topcased.adele.editor.utils.dialogs;

import java.util.Vector;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.topcased.adele.editor.utils.ADELE_Utils;
import org.topcased.adele.model.ADELE.util.ADELEModelUtils;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Components.System;
import org.topcased.adele.model.ADELE_Components.impl.ComponentImpl;

public class ImplementationChooseDialog extends TitleAreaDialog {

	protected Vector<String> matchingImplementationsName;
	protected int selectedIndex=-1;
	protected Text texte;
	protected Table table;
	protected Component currentCompo;
	
	public ImplementationChooseDialog(Shell parentShell) {
		super(parentShell);
	}
	
	public void create(EList<Component> matchingImplementations, Component obj) {
		currentCompo = obj;
		
		matchingImplementationsName = new Vector<String>(matchingImplementations.size(),1);
		for (int i=0; i<matchingImplementations.size(); i++){
			final Component compo = matchingImplementations.get(i);
			
			if ( compo == null ) {
				matchingImplementationsName.add( "none" );
			}
			else if (!(compo.getImplementationName().equalsIgnoreCase(""))) {
				// add implicit implementation created in system to possible classifiers 
				matchingImplementationsName.add( ADELEModelUtils.getFullNamespace( compo )+
						compo.getName()+"."+compo.getImplementationName());
			}
			else {
				matchingImplementationsName.add( ADELEModelUtils.getFullNamespace(compo)+ compo.getName());
			}
			
			if (compo != null && compo == currentCompo.getImplementation())
				selectedIndex=i;
		}
		super.create();
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {

		Composite composite = new Composite(parent, SWT.NONE);
		GridData data = new GridData(GridData.FILL_BOTH);
	    data.horizontalSpan = 2;
	    composite.setLayoutData(data);
	    GridLayout layout = new GridLayout();
	    layout.numColumns = 1;
	    composite.setLayout(layout);

	    data = new GridData(GridData.FILL_BOTH);
	    ScrolledComposite scrollPanel = new ScrolledComposite(composite, SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.BORDER);
	    scrollPanel.setLayoutData(data);
	    scrollPanel.setLayout(new GridLayout());
	    Composite tableContainer = new Composite(scrollPanel, SWT.NONE);
	    tableContainer.setLayout(new GridLayout(2,false));
		scrollPanel.setContent(tableContainer);
		
		
		table = new Table(tableContainer, SWT.BORDER	| SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		table.setLayoutData(new GridData());
		TableColumn column = new TableColumn(table, SWT.NONE);
		column.setText("Choose a classifier");

		for (int i = 0; i < matchingImplementationsName.size(); i++) {
			TableItem item = new TableItem(table, SWT.NONE);
			item.setText(0, matchingImplementationsName.get(i));
		}
		
		column.pack();
		column.setWidth(column.getWidth()*4);
		if (selectedIndex!=-1)
			table.select(selectedIndex);
		table.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				selectedIndex=((Table)arg0.getSource()).getSelectionIndex();
			}
		});

		table.addMouseListener(new MouseListener(){

			public void mouseDoubleClick(MouseEvent e) {
				okPressed();
			}

			public void mouseDown(MouseEvent e) {
			}

			public void mouseUp(MouseEvent e) {
			}
			
		});
		
		if (ADELE_Utils.getRoot(currentCompo) instanceof System){
			Button bouton = new Button(parent, SWT.CHECK);
			bouton.setText("Set classifier's implementation name");
			if (matchingImplementationsName.size()>0) {
				bouton.addSelectionListener(new SelectionListener(){

					public void widgetDefaultSelected(SelectionEvent arg0) {
					}

					public void widgetSelected(SelectionEvent arg0) {
						if (((Button)arg0.getSource()).getSelection()){
							texte.setEnabled(true);
							table.setEnabled(false);
						}
						else{
							texte.setEnabled(false);
							table.setEnabled(true);
						}
					}});
			} 

			texte = new Text(parent, SWT.NONE);
			texte.setText(currentCompo.getImplementationName());
			texte.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			texte.addKeyListener(new KeyAdapter(){
				public void keyPressed(KeyEvent e) {
					if (e.keyCode==SWT.LF)
						okPressed();
				}
			});

			if (!(currentCompo.getImplementationName().equalsIgnoreCase(ADELE_Utils.getDefaultImplementationName())) &&
					currentCompo.getImplementation()==null){
				bouton.setSelection(true);
				table.setEnabled(false);
			}
			else {
				bouton.setSelection(false);
				texte.setEnabled(false);
			}
			
			 if (matchingImplementationsName.size()==0) {
				bouton.setSelection(true);
				bouton.setEnabled(false);
				texte.setEnabled(true);
				table.setEnabled(false);
			}
			 
		}

		Point containerSize = tableContainer.computeSize(SWT.DEFAULT,
				SWT.DEFAULT);
		Point panelSize = scrollPanel.getSize();
		tableContainer.setSize(Math
				.max(panelSize.x - 5, containerSize.x), Math.max(
				panelSize.y - 5, containerSize.y));
		
		return super.createDialogArea(parent);
	}
	
	@Override
	public int open() {
		super.open();
		return selectedIndex;
	}
	
	@Override
	protected void cancelPressed() {
		selectedIndex=-1;
		super.cancelPressed();
	}
	
	@Override
	protected void okPressed() {
		if (texte!=null && texte.getEnabled()){
			((ComponentImpl)currentCompo).setImplementationName(texte.getText());
			selectedIndex=-2;
		}
		super.okPressed();
	}

}
