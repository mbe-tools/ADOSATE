package org.topcased.adele.editor.utils.policies;

import org.eclipse.emf.ecore.EObject;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy;

public class SystemLayoutEditPolicy extends ModelerLayoutEditPolicy {
	
	@Override
	protected boolean isValid(EObject child, EObject parent) {
		return !isInvalid( child, parent );
	}

	private static boolean isInvalid(EObject child, EObject parent) {
		if (((Component)parent).isFeaturesLock())
			return true;
		return false;
	}
}