package org.topcased.adele.editor.utils.edit;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.widgets.Display;
import org.topcased.adele.editor.utils.ADELE_Utils;
import org.topcased.adele.model.ADELE.util.ADELEModelUtils;
import org.topcased.adele.model.ADELE_Features.AbstractFeature;
import org.topcased.adele.model.ADELE_Features.BusAccess;
import org.topcased.adele.model.ADELE_Features.DataAccess;
import org.topcased.adele.model.ADELE_Features.DataPort;
import org.topcased.adele.model.ADELE_Features.EventDataPort;
import org.topcased.adele.model.ADELE_Features.EventPort;
import org.topcased.adele.model.ADELE_Features.Parameter;
import org.topcased.adele.model.ADELE_Features.SubprogramAccess;
import org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.edit.PortEditPart;

public class ADELE_PortEditPart extends PortEditPart {

	public ADELE_PortEditPart(GraphNode obj) {
		super(obj);

		ADELE_Utils.setParent(obj);
	}
	
	/* DB: Managing this in the super class instead of the subclass.
	 * (non-Javadoc)
	 * @see org.topcased.modeler.edit.PortEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		final EObject object = getEObject();
		final IFigure figure;
		
		if ( object instanceof BusAccess ) {
			figure = BusAccessEditPart.createFigure( (BusAccess) object );
		}
		else if ( object instanceof DataAccess ) {
			figure = DataAccessEditPart.createFigure( (DataAccess) object );
		}
		else if ( object instanceof Parameter ) {
			figure = ParameterEditPart.createFigure( (Parameter) object );
		}
		else if ( object instanceof EventPort ) {
			figure = EventPortEditPart.createFigure( (EventPort) object );
		}
		else if ( object instanceof DataPort ) {
			figure = DataPortEditPart.createFigure( (DataPort) object );
		}
		else if ( object instanceof EventDataPort ) {
			figure = EventDataPortEditPart.createFigure( (EventDataPort) object );
		}
		else if ( object instanceof SubprogramAccess ) {
			figure = SubprogramAccessEditPart.createFigure( (SubprogramAccess) object );
		}
		else if ( object instanceof SubprogramGroupAccess ) {
			figure = SubprogramGroupAccessEditPart.createFigure( (SubprogramGroupAccess) object );
		}
		else if ( object instanceof AbstractFeature ) {
			figure = AbstractFeatureEditPart.createFigure( (AbstractFeature) object );
		}
		else {
			throw new IllegalStateException( "Unknown feature type " + object );
		}
		
    	if ( 	object instanceof SKHierarchicalObject &&
    			!ADELEModelUtils.isOriginalElement( (SKHierarchicalObject) object ) ) {
    		Display display = Display.getCurrent();
    		
    		if (display == null) {
    			display = Display.getDefault();
    		}
        	
    		//figure.setForegroundColor( display.getSystemColor( SWT.COLOR_GRAY ) );
    	}
    	
    	return figure;
	}

	/* DB: Managing this in the super class instead of the subclass.
	 *  (non-Javadoc)
	 * @see org.topcased.modeler.edit.EMFGraphNodeEditPart#handleModelChanged(org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelChanged(Notification msg) {
		super.handleModelChanged(msg);
		
		final EObject object = getEObject();
		
		if ( object instanceof BusAccess ) {
			BusAccessEditPart.handleModelChanged(msg, this);
		}
		else if ( object instanceof DataAccess ) {
			DataAccessEditPart.handleModelChanged(msg, this);
		}
		else if ( object instanceof Parameter ) {
			ParameterEditPart.handleModelChanged(msg, this);
		}
		else if ( object instanceof EventPort ) {
			EventPortEditPart.handleModelChanged(msg, this);
		}
		else if ( object instanceof DataPort ) {
			DataPortEditPart.handleModelChanged(msg, this);
		}
		else if ( object instanceof EventDataPort ) {
			EventDataPortEditPart.handleModelChanged(msg, this);
		}
		else if ( object instanceof SubprogramAccess ) {
			SubprogramAccessEditPart.handleModelChanged(msg, this);
		}
		else if ( object instanceof SubprogramGroupAccess ) {
			SubprogramGroupAccessEditPart.handleModelChanged(msg, this);
		}
		else if ( object instanceof AbstractFeature ) {
			AbstractFeatureEditPart.handleModelChanged(msg, this);
		}
	}
}
