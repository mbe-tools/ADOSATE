/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/

package org.topcased.adele.editor.utils.figures.util;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.topcased.adele.common.utils.figures.ComponentFigure;

/**
 * A Figure that represent a Memory
 * 
 * Creation : 10 nov. 2005
 * 
 * @author <a href="mailto:jacques.lescot@anyware-tech.com">Jacques LESCOT</a>
 */
public class MemoryComponentFigure extends ComponentFigure
{

    /**
     * @see org.eclipse.draw2d.Figure#paintBorder(org.eclipse.draw2d.Graphics)
     */
    protected void paintBorder(Graphics graphics)
    {
        graphics.pushState();
        graphics.setLineWidth(getLineWidth());
        graphics.drawArc(new Rectangle(bounds.getTopLeft().translate(1, 0), bounds.getTopRight().translate(-3, 10)), 0, 360);
        graphics.drawLine(bounds.getTopLeft().translate(1, 6), bounds.getBottomLeft().translate(1, -6));
        graphics.drawLine(bounds.getTopRight().translate(-2, 6), bounds.getBottomRight().translate(-2, -6));
        graphics.drawArc(new Rectangle(bounds.getBottomLeft().translate(1, -12), bounds.getBottomRight().translate(-3, -2)), 180, 180);
        graphics.popState();
    }

    /**
     * @see org.eclipse.draw2d.Figure#paintFigure(org.eclipse.draw2d.Graphics)
     */
    protected void paintFigure(Graphics graphics)
    {
        graphics.fillArc(new Rectangle(bounds.getTopLeft().translate(1, 0), bounds.getTopRight().translate(-3, 10)), 0, 360);
        graphics.fillRectangle(new Rectangle(bounds.getTopLeft().translate(1, 6), bounds.getBottomRight().translate(-2, -6)));
        graphics.fillArc(new Rectangle(bounds.getBottomLeft().translate(1, -12), bounds.getBottomRight().translate(-3, -2)), 180, 180);
    }

}
