/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies), Mathieu Garcia (Anyware Technologies),
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/
package org.topcased.adele.editor.utils.figures.features;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.topcased.adele.editor.utils.figures.AdeleFeatureFigure;
import org.topcased.adele.model.ADELE_Features.PortDirection;

/**
 * The figure to display an Abstract Feature.
 * 
 * Creation : 19 sept. 2005
 * 
 * @author <a href="mailto:jacques.lescot@anyware-tech.com">Jacques LESCOT</a>
 */
public class AbstractFeatureFigure extends AdeleFeatureFigure
{
    private PortDirection featureDirection;

    /**
     * Constructor
     * 
     * @param featureDirection the type of the feature (IN, OUT or NONE)
     */
    public AbstractFeatureFigure( PortDirection featureDirection)
    {
        super();
        this.featureDirection = featureDirection;
    }

    /**
     * Constructor
     */
    public AbstractFeatureFigure()
    {
        super();
    }

    /**
     * @see org.topcased.modeler.figures.ModelerPortFigure#paintContentPaneFigure(org.eclipse.draw2d.Graphics)
     */
    protected void paintContentPaneFigure(Graphics graphics)
    {
        graphics.pushState();

        graphics.setLineWidth(1);

        PointList pl1 = new PointList();
        Rectangle circle;
        
        if (getPosition() == PositionConstants.LEFT && featureDirection == PortDirection.IN_LITERAL
                || getPosition() == PositionConstants.RIGHT && featureDirection == PortDirection.OUT_LITERAL)
        {
            pl1.addPoint(bounds.getTopLeft());
            pl1.addPoint(bounds.getRight().translate(-1, 0));
            pl1.addPoint(bounds.getBottomLeft().translate(0, -1));
            circle = new Rectangle(bounds.x, bounds.y+22*bounds.height/100,
            				56*bounds.width/100, 56*bounds.height/100);
        }
        else if (getPosition() == PositionConstants.LEFT && featureDirection == PortDirection.OUT_LITERAL
                || getPosition() == PositionConstants.RIGHT && featureDirection == PortDirection.IN_LITERAL)
        {
            pl1.addPoint(bounds.getTopRight().translate(-1, 0));
            pl1.addPoint(bounds.getLeft());
            pl1.addPoint(bounds.getBottomRight().translate(-1, -1));
            circle = new Rectangle(bounds.x+44*bounds.width/100, bounds.y+22*bounds.height/100,
            		56*bounds.width/100, 56*bounds.height/100);
        }
        else if (getPosition() == PositionConstants.BOTTOM && featureDirection == PortDirection.IN_LITERAL
                || getPosition() == PositionConstants.TOP && featureDirection == PortDirection.OUT_LITERAL)
        {
            pl1.addPoint(bounds.getBottomLeft().translate(0, -1));
            pl1.addPoint(bounds.getTop());
            pl1.addPoint(bounds.getBottomRight().translate(-1, -1));
            circle = new Rectangle(bounds.x+22*bounds.width/100, bounds.y+44*bounds.height/100,
            		56*bounds.width/100, 56*bounds.height/100);
        }
        else if (getPosition() == PositionConstants.BOTTOM && featureDirection == PortDirection.OUT_LITERAL
                || getPosition() == PositionConstants.TOP && featureDirection == PortDirection.IN_LITERAL)
        {
            pl1.addPoint(bounds.getTopLeft());
            pl1.addPoint(bounds.getBottom().translate(0, -1));
            pl1.addPoint(bounds.getTopRight().translate(-1, 0));
            circle = new Rectangle(bounds.x+22*bounds.width/100, bounds.y,
            		56*bounds.width/100, 56*bounds.height/100);
        }
        else
            circle = new Rectangle(bounds.x+22*bounds.width/100, bounds.y+22*bounds.height/100,
            		56*bounds.width/100, 56*bounds.height/100);

        graphics.drawPolyline(pl1);
        graphics.fillOval(circle);

        graphics.popState();
    }

    /**
     * @see org.eclipse.draw2d.IFigure#getPreferredSize(int, int)
     */
    public Dimension getPreferredSize(int wHint, int hHint)
    {
        return new Dimension(15, 15);
    }

    /**
     * Change the PortDirection of the figure
     * 
     * @param newDir
     */
    public void setPortDirection(PortDirection newDir)
    {
        this.featureDirection = newDir;
    }
}
