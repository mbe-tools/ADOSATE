package org.topcased.adele.editor.utils.edit;

import java.util.StringTokenizer;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.jface.action.Action;
import org.topcased.adele.common.utils.commands.RefreshFeaturesCommand;
import org.topcased.adele.editor.utils.ADELE_Utils;
import org.topcased.adele.editor.utils.actions.CloseOpenFeatureGroupAction;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.modeler.ModelerEditPolicyConstants;
import org.topcased.modeler.commands.GEFtoEMFCommandWrapper;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.di.model.util.DIUtils;
import org.topcased.modeler.edit.policies.NonResizableEditPolicy;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.editor.ModelerGraphicalViewer;
import org.topcased.modeler.utils.Utils;

public class FeatureGroupEditPart extends org.topcased.modeler.edit.PortEditPart {
	
	// false means opened and true closed
	boolean openClosedState = false;
	
	Dimension openSize;
	
	String suffix="InComponent";
	
	private GraphNode currentObject;
	
	public FeatureGroupEditPart(GraphNode obj) {
		super(obj);
		currentObject = obj;
		if (getComponentEditPart() instanceof ADELE_DiagramEditPart)
			suffix="InDiagram";
		try {
			openClosedState = new Boolean(DIUtils.getPropertyValue(obj, "openClosedState"+suffix));
			StringTokenizer tmp = new StringTokenizer(DIUtils.getPropertyValue(obj, "openSize"+suffix), ",");
			int width = new Integer(tmp.nextToken());
			int heigh = new Integer(tmp.nextToken());
			openSize = new Dimension(heigh, width);
		} catch(Exception e){}

		ADELE_Utils.setParent(obj);
	}

	/**
	 * Handles the double-click request. Used to open/close the PortGroup
	 * 
	 * @see org.eclipse.gef.EditPart#performRequest(org.eclipse.gef.Request)
	 */
	public void performRequest(Request request) {
		if (request.getType().equals(REQ_OPEN)) {
			// The action to execute if the active diagram must change
			Action action = new CloseOpenFeatureGroupAction(this);

			if (action != null) {
				action.run();
			}
		} else
			super.performRequest(request);

	}
	
	protected void refreshFeatureGroupFeatures() 
	throws CoreException {
		final GraphElement graphElem = (GraphElement) getModel();
		final EObject semanticElem = Utils.getElement( graphElem );
		
		if ( semanticElem instanceof SKComponent ) {
			final Modeler modeler = ( (ModelerGraphicalViewer) getViewer() ).getModelerEditor();
			
			if ( modeler == null ) {
				final RefreshFeaturesCommand command = new RefreshFeaturesCommand( 	Utils.getDiagram( graphElem ),
																					(SKComponent) semanticElem );
				
				if ( command.canExecute() ) {
					command.execute();
				}
			}
			else {
				final RefreshFeaturesCommand command = new RefreshFeaturesCommand( modeler, (SKComponent) semanticElem );
				modeler.getEditingDomain().getCommandStack().execute( new GEFtoEMFCommandWrapper( command ) );
			}
		}
	}

	public boolean isClosed(){
		return openClosedState;
	}
	
	public void setOpenClosedState (boolean state){
		openClosedState = state;
		DIUtils.setProperty(currentObject, "openClosedState"+suffix, (new Boolean(openClosedState)).toString());
	}
	
	public Dimension getOpenSize(){
		return openSize;
	}
	
	public void setOpenSize (Dimension size){
		openSize = size;
		String tmp = (new Integer(openSize.height)).toString() + "," +
						(new Integer(openSize.width)).toString();
		DIUtils.setProperty(currentObject, "openSize"+suffix, tmp);
	}
	
	protected EditPart getComponentEditPart (){
		EditPart res= getParent();
		while (res!=null){
			if (res.getModel() instanceof Component)
				return res;
			res=res.getParent();
		}
		return res;
	}
	
	@Override
	public void addNotify() {
		super.addNotify();

		if (isClosed()){
			removeEditPolicy(ModelerEditPolicyConstants.RESIZABLE_EDITPOLICY);
			installEditPolicy("Not resizable",new NonResizableEditPolicy());
		}
	}
	
	/* DB: Redefine this to avoid redefining the createFigure in sub classes.
	 * (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#getFigure()
	 */
	@Override
	public IFigure getFigure() {
		final boolean needsRefresh = figure == null;
		final IFigure newFigure = super.getFigure();
		
		if ( needsRefresh ) {
			try {
				refreshFeatureGroupFeatures();
			} 
			catch (CoreException e) {
				e.printStackTrace();
			}
		}
		
		return newFigure;
	}
	
}