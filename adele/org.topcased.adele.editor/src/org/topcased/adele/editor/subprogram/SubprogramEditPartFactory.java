/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.subprogram;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.topcased.adele.editor.subprogram.edit.AbstractEditPart;
import org.topcased.adele.editor.subprogram.edit.AbstractFeatureConnectionEditPart;
import org.topcased.adele.editor.subprogram.edit.AbstractFeatureEditPart;
import org.topcased.adele.editor.subprogram.edit.DataAccessEditPart;
import org.topcased.adele.editor.subprogram.edit.DataEditPart;
import org.topcased.adele.editor.subprogram.edit.EventDataPortEditPart;
import org.topcased.adele.editor.subprogram.edit.EventPortEditPart;
import org.topcased.adele.editor.subprogram.edit.FeatureGroupConnectionEditPart;
import org.topcased.adele.editor.subprogram.edit.FeatureGroupEditPart;
import org.topcased.adele.editor.subprogram.edit.ParameterEditPart;
import org.topcased.adele.editor.subprogram.edit.SubprogramAccessConnectionEditPart;
import org.topcased.adele.editor.subprogram.edit.SubprogramAccessEditPart;
import org.topcased.adele.editor.subprogram.edit.SubprogramDiagramEditPart;
import org.topcased.adele.editor.subprogram.edit.SubprogramEditPart;
import org.topcased.adele.editor.subprogram.edit.SubprogramGroupAccessConnectionEditPart;
import org.topcased.adele.editor.subprogram.edit.SubprogramGroupAccessEditPart;
import org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch;
import org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch;
import org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch;
import org.topcased.adele.model.KernelSpices.util.KernelSpicesSwitch;
import org.topcased.adele.model.ObjectDescriptionModel.util.ObjectDescriptionModelSwitch;
import org.topcased.adele.model.ba_components.util.Ba_componentsSwitch;
import org.topcased.adele.model.ba_features.util.Ba_featuresSwitch;
import org.topcased.adele.model.ba_relations.util.Ba_relationsSwitch;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.di.model.SimpleSemanticModelElement;
import org.topcased.modeler.edit.EMFGraphEdgeEditPart;
import org.topcased.modeler.edit.EMFGraphNodeEditPart;
import org.topcased.modeler.editor.ModelerEditPartFactory;
import org.topcased.modeler.utils.Utils;

/**
 * Part Factory : associates a model object to its controller. <br>
 *
 * @generated
 */
public class SubprogramEditPartFactory extends ModelerEditPartFactory {
	/**
	 * @see org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,java.lang.Object)
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof Diagram) {
			return new SubprogramDiagramEditPart((Diagram) model);
		} else if (model instanceof GraphNode) {
			final GraphNode node = (GraphNode) model;
			EObject element = Utils.getElement(node);
			if (element != null) {
				if ("http://ADELE_Components".equals(element.eClass()
						.getEPackage().getNsURI())) {
					return (EditPart) new NodeADELE_ComponentsSwitch(node)
							.doSwitch(element);
				}
				if ("http://ADELE_Features".equals(element.eClass()
						.getEPackage().getNsURI())) {
					return (EditPart) new NodeADELE_FeaturesSwitch(node)
							.doSwitch(element);
				}
				if ("http://ADELE_Relations".equals(element.eClass()
						.getEPackage().getNsURI())) {
					return (EditPart) new NodeADELE_RelationsSwitch(node)
							.doSwitch(element);
				}
				if ("http://ba_components/1.0".equals(element.eClass()
						.getEPackage().getNsURI())) {
					return (EditPart) new NodeBa_componentsSwitch(node)
							.doSwitch(element);
				}
				if ("http://ba_features/1.0".equals(element.eClass()
						.getEPackage().getNsURI())) {
					return (EditPart) new NodeBa_featuresSwitch(node)
							.doSwitch(element);
				}
				if ("http://ba_relations/1.0".equals(element.eClass()
						.getEPackage().getNsURI())) {
					return (EditPart) new NodeBa_relationsSwitch(node)
							.doSwitch(element);
				}
				if ("http://KernelSpices".equals(element.eClass().getEPackage()
						.getNsURI())) {
					return (EditPart) new NodeKernelSpicesSwitch(node)
							.doSwitch(element);
				}
				if ("http://ObjectDescriptionModel".equals(element.eClass()
						.getEPackage().getNsURI())) {
					return (EditPart) new NodeObjectDescriptionModelSwitch(node)
							.doSwitch(element);
				}
			}

			if (node.getSemanticModel() instanceof SimpleSemanticModelElement) {
				// Manage the Element that are not associated with a model object
			}
		} else if (model instanceof GraphEdge) {
			final GraphEdge edge = (GraphEdge) model;
			EObject element = Utils.getElement(edge);
			if (element != null) {
				if ("http://ADELE_Components".equals(element.eClass()
						.getEPackage().getNsURI())) {
					return (EditPart) new EdgeADELE_ComponentsSwitch(edge)
							.doSwitch(element);
				}
				if ("http://ADELE_Features".equals(element.eClass()
						.getEPackage().getNsURI())) {
					return (EditPart) new EdgeADELE_FeaturesSwitch(edge)
							.doSwitch(element);
				}
				if ("http://ADELE_Relations".equals(element.eClass()
						.getEPackage().getNsURI())) {
					return (EditPart) new EdgeADELE_RelationsSwitch(edge)
							.doSwitch(element);
				}
				if ("http://ba_components/1.0".equals(element.eClass()
						.getEPackage().getNsURI())) {
					return (EditPart) new EdgeBa_componentsSwitch(edge)
							.doSwitch(element);
				}
				if ("http://ba_features/1.0".equals(element.eClass()
						.getEPackage().getNsURI())) {
					return (EditPart) new EdgeBa_featuresSwitch(edge)
							.doSwitch(element);
				}
				if ("http://ba_relations/1.0".equals(element.eClass()
						.getEPackage().getNsURI())) {
					return (EditPart) new EdgeBa_relationsSwitch(edge)
							.doSwitch(element);
				}
				if ("http://KernelSpices".equals(element.eClass().getEPackage()
						.getNsURI())) {
					return (EditPart) new EdgeKernelSpicesSwitch(edge)
							.doSwitch(element);
				}
				if ("http://ObjectDescriptionModel".equals(element.eClass()
						.getEPackage().getNsURI())) {
					return (EditPart) new EdgeObjectDescriptionModelSwitch(edge)
							.doSwitch(element);
				}
			}

			if (edge.getSemanticModel() instanceof SimpleSemanticModelElement) {
				// Manage the Element that are not associated with a model object                    
			}
		}
		return super.createEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private class NodeADELE_ComponentsSwitch extends ADELE_ComponentsSwitch {
		/**
		 * The graphical node
		 * @generated
		 */
		private GraphNode node;

		/**
		 * Constructor
		 * 
		 * @param node the graphical node
		 * @generated
		 */
		public NodeADELE_ComponentsSwitch(GraphNode node) {
			this.node = node;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseData(org.topcased.adele.model.ADELE_Components.Data)
		 * @generated
		 */
		public Object caseData(
				org.topcased.adele.model.ADELE_Components.Data object) {
			return new DataEditPart(node);
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseAbstract(org.topcased.adele.model.ADELE_Components.Abstract)
		 * @generated
		 */
		public Object caseAbstract(
				org.topcased.adele.model.ADELE_Components.Abstract object) {
			return new AbstractEditPart(node);
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseSubprogram(org.topcased.adele.model.ADELE_Components.Subprogram)
		 * @generated
		 */
		public Object caseSubprogram(
				org.topcased.adele.model.ADELE_Components.Subprogram object) {
			return new SubprogramEditPart(node);
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return new EMFGraphNodeEditPart(node);
		}
	}

	/**
	 * @generated
	 */
	private class NodeADELE_FeaturesSwitch extends ADELE_FeaturesSwitch {
		/**
		 * The graphical node
		 * @generated
		 */
		private GraphNode node;

		/**
		 * Constructor
		 * 
		 * @param node the graphical node
		 * @generated
		 */
		public NodeADELE_FeaturesSwitch(GraphNode node) {
			this.node = node;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseEventPort(org.topcased.adele.model.ADELE_Features.EventPort)
		 * @generated
		 */
		public Object caseEventPort(
				org.topcased.adele.model.ADELE_Features.EventPort object) {
			return new EventPortEditPart(node);
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseEventDataPort(org.topcased.adele.model.ADELE_Features.EventDataPort)
		 * @generated
		 */
		public Object caseEventDataPort(
				org.topcased.adele.model.ADELE_Features.EventDataPort object) {
			return new EventDataPortEditPart(node);
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseFeatureGroup(org.topcased.adele.model.ADELE_Features.FeatureGroup)
		 * @generated
		 */
		public Object caseFeatureGroup(
				org.topcased.adele.model.ADELE_Features.FeatureGroup object) {
			return new FeatureGroupEditPart(node);
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseDataAccess(org.topcased.adele.model.ADELE_Features.DataAccess)
		 * @generated
		 */
		public Object caseDataAccess(
				org.topcased.adele.model.ADELE_Features.DataAccess object) {
			return new DataAccessEditPart(node);
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseSubprogramAccess(org.topcased.adele.model.ADELE_Features.SubprogramAccess)
		 * @generated
		 */
		public Object caseSubprogramAccess(
				org.topcased.adele.model.ADELE_Features.SubprogramAccess object) {
			return new SubprogramAccessEditPart(node);
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseSubprogramGroupAccess(org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess)
		 * @generated
		 */
		public Object caseSubprogramGroupAccess(
				org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess object) {
			return new SubprogramGroupAccessEditPart(node);
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseParameter(org.topcased.adele.model.ADELE_Features.Parameter)
		 * @generated
		 */
		public Object caseParameter(
				org.topcased.adele.model.ADELE_Features.Parameter object) {
			return new ParameterEditPart(node);
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseAbstractFeature(org.topcased.adele.model.ADELE_Features.AbstractFeature)
		 * @generated
		 */
		public Object caseAbstractFeature(
				org.topcased.adele.model.ADELE_Features.AbstractFeature object) {
			return new AbstractFeatureEditPart(node);
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return new EMFGraphNodeEditPart(node);
		}
	}

	/**
	 * @generated
	 */
	private class NodeADELE_RelationsSwitch extends ADELE_RelationsSwitch {
		/**
		 * The graphical node
		 * @generated
		 */
		private GraphNode node;

		/**
		 * Constructor
		 * 
		 * @param node the graphical node
		 * @generated
		 */
		public NodeADELE_RelationsSwitch(GraphNode node) {
			this.node = node;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return new EMFGraphNodeEditPart(node);
		}
	}

	/**
	 * @generated
	 */
	private class NodeBa_componentsSwitch extends Ba_componentsSwitch {
		/**
		 * The graphical node
		 * @generated
		 */
		private GraphNode node;

		/**
		 * Constructor
		 * 
		 * @param node the graphical node
		 * @generated
		 */
		public NodeBa_componentsSwitch(GraphNode node) {
			this.node = node;
		}

		/**
		 * @see org.topcased.adele.model.ba_components.util.Ba_componentsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return new EMFGraphNodeEditPart(node);
		}
	}

	/**
	 * @generated
	 */
	private class NodeBa_featuresSwitch extends Ba_featuresSwitch {
		/**
		 * The graphical node
		 * @generated
		 */
		private GraphNode node;

		/**
		 * Constructor
		 * 
		 * @param node the graphical node
		 * @generated
		 */
		public NodeBa_featuresSwitch(GraphNode node) {
			this.node = node;
		}

		/**
		 * @see org.topcased.adele.model.ba_features.util.Ba_featuresSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return new EMFGraphNodeEditPart(node);
		}
	}

	/**
	 * @generated
	 */
	private class NodeBa_relationsSwitch extends Ba_relationsSwitch {
		/**
		 * The graphical node
		 * @generated
		 */
		private GraphNode node;

		/**
		 * Constructor
		 * 
		 * @param node the graphical node
		 * @generated
		 */
		public NodeBa_relationsSwitch(GraphNode node) {
			this.node = node;
		}

		/**
		 * @see org.topcased.adele.model.ba_relations.util.Ba_relationsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return new EMFGraphNodeEditPart(node);
		}
	}

	/**
	 * @generated
	 */
	private class NodeKernelSpicesSwitch extends KernelSpicesSwitch {
		/**
		 * The graphical node
		 * @generated
		 */
		private GraphNode node;

		/**
		 * Constructor
		 * 
		 * @param node the graphical node
		 * @generated
		 */
		public NodeKernelSpicesSwitch(GraphNode node) {
			this.node = node;
		}

		/**
		 * @see org.topcased.adele.model.KernelSpices.util.KernelSpicesSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return new EMFGraphNodeEditPart(node);
		}
	}

	/**
	 * @generated
	 */
	private class NodeObjectDescriptionModelSwitch extends
			ObjectDescriptionModelSwitch {
		/**
		 * The graphical node
		 * @generated
		 */
		private GraphNode node;

		/**
		 * Constructor
		 * 
		 * @param node the graphical node
		 * @generated
		 */
		public NodeObjectDescriptionModelSwitch(GraphNode node) {
			this.node = node;
		}

		/**
		 * @see org.topcased.adele.model.ObjectDescriptionModel.util.ObjectDescriptionModelSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return new EMFGraphNodeEditPart(node);
		}
	}

	/**
	 * @generated
	 */
	private class EdgeADELE_ComponentsSwitch extends ADELE_ComponentsSwitch {
		/**
		 * The graphical edge
		 * @generated
		 */
		private GraphEdge edge;

		/**
		 * Constructor
		 * 
		 * @param edge the graphical edge
		 * @generated
		 */
		public EdgeADELE_ComponentsSwitch(GraphEdge edge) {
			this.edge = edge;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return new EMFGraphEdgeEditPart(edge);
		}
	}

	/**
	 * @generated
	 */
	private class EdgeADELE_FeaturesSwitch extends ADELE_FeaturesSwitch {
		/**
		 * The graphical edge
		 * @generated
		 */
		private GraphEdge edge;

		/**
		 * Constructor
		 * 
		 * @param edge the graphical edge
		 * @generated
		 */
		public EdgeADELE_FeaturesSwitch(GraphEdge edge) {
			this.edge = edge;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return new EMFGraphEdgeEditPart(edge);
		}
	}

	/**
	 * @generated
	 */
	private class EdgeADELE_RelationsSwitch extends ADELE_RelationsSwitch {
		/**
		 * The graphical edge
		 * @generated
		 */
		private GraphEdge edge;

		/**
		 * Constructor
		 * 
		 * @param edge the graphical edge
		 * @generated
		 */
		public EdgeADELE_RelationsSwitch(GraphEdge edge) {
			this.edge = edge;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseSubprogramAccessConnection(org.topcased.adele.model.ADELE_Relations.SubprogramAccessConnection)
		 * @generated
		 */
		public Object caseSubprogramAccessConnection(
				org.topcased.adele.model.ADELE_Relations.SubprogramAccessConnection object) {
			return new SubprogramAccessConnectionEditPart(edge);
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseSubprogramGroupAccessConnection(org.topcased.adele.model.ADELE_Relations.SubprogramGroupAccessConnection)
		 * @generated
		 */
		public Object caseSubprogramGroupAccessConnection(
				org.topcased.adele.model.ADELE_Relations.SubprogramGroupAccessConnection object) {
			return new SubprogramGroupAccessConnectionEditPart(edge);
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseAbstractFeatureConnection(org.topcased.adele.model.ADELE_Relations.AbstractFeatureConnection)
		 * @generated
		 */
		public Object caseAbstractFeatureConnection(
				org.topcased.adele.model.ADELE_Relations.AbstractFeatureConnection object) {
			return new AbstractFeatureConnectionEditPart(edge);
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseFeatureGroupConnection(org.topcased.adele.model.ADELE_Relations.FeatureGroupConnection)
		 * @generated
		 */
		public Object caseFeatureGroupConnection(
				org.topcased.adele.model.ADELE_Relations.FeatureGroupConnection object) {
			return new FeatureGroupConnectionEditPart(edge);
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return new EMFGraphEdgeEditPart(edge);
		}
	}

	/**
	 * @generated
	 */
	private class EdgeBa_componentsSwitch extends Ba_componentsSwitch {
		/**
		 * The graphical edge
		 * @generated
		 */
		private GraphEdge edge;

		/**
		 * Constructor
		 * 
		 * @param edge the graphical edge
		 * @generated
		 */
		public EdgeBa_componentsSwitch(GraphEdge edge) {
			this.edge = edge;
		}

		/**
		 * @see org.topcased.adele.model.ba_components.util.Ba_componentsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return new EMFGraphEdgeEditPart(edge);
		}
	}

	/**
	 * @generated
	 */
	private class EdgeBa_featuresSwitch extends Ba_featuresSwitch {
		/**
		 * The graphical edge
		 * @generated
		 */
		private GraphEdge edge;

		/**
		 * Constructor
		 * 
		 * @param edge the graphical edge
		 * @generated
		 */
		public EdgeBa_featuresSwitch(GraphEdge edge) {
			this.edge = edge;
		}

		/**
		 * @see org.topcased.adele.model.ba_features.util.Ba_featuresSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return new EMFGraphEdgeEditPart(edge);
		}
	}

	/**
	 * @generated
	 */
	private class EdgeBa_relationsSwitch extends Ba_relationsSwitch {
		/**
		 * The graphical edge
		 * @generated
		 */
		private GraphEdge edge;

		/**
		 * Constructor
		 * 
		 * @param edge the graphical edge
		 * @generated
		 */
		public EdgeBa_relationsSwitch(GraphEdge edge) {
			this.edge = edge;
		}

		/**
		 * @see org.topcased.adele.model.ba_relations.util.Ba_relationsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return new EMFGraphEdgeEditPart(edge);
		}
	}

	/**
	 * @generated
	 */
	private class EdgeKernelSpicesSwitch extends KernelSpicesSwitch {
		/**
		 * The graphical edge
		 * @generated
		 */
		private GraphEdge edge;

		/**
		 * Constructor
		 * 
		 * @param edge the graphical edge
		 * @generated
		 */
		public EdgeKernelSpicesSwitch(GraphEdge edge) {
			this.edge = edge;
		}

		/**
		 * @see org.topcased.adele.model.KernelSpices.util.KernelSpicesSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return new EMFGraphEdgeEditPart(edge);
		}
	}

	/**
	 * @generated
	 */
	private class EdgeObjectDescriptionModelSwitch extends
			ObjectDescriptionModelSwitch {
		/**
		 * The graphical edge
		 * @generated
		 */
		private GraphEdge edge;

		/**
		 * Constructor
		 * 
		 * @param edge the graphical edge
		 * @generated
		 */
		public EdgeObjectDescriptionModelSwitch(GraphEdge edge) {
			this.edge = edge;
		}

		/**
		 * @see org.topcased.adele.model.ObjectDescriptionModel.util.ObjectDescriptionModelSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return new EMFGraphEdgeEditPart(edge);
		}
	}

}