/*******************************************************************************
 * Copyright (c) 2005 Anyware Technologies
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies) - initial API and implementation
 *    Sebastien Gabel (CS) - Bug fix #2985 & 
 *******************************************************************************/

package org.topcased.adele.editor.wizards;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.commands.Command;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;
import org.topcased.adele.common.utils.commands.AbstractRefreshCommand;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.adele.editor.utils.ADELE_Utils;
import org.topcased.adele.editor.utils.actions.ChangeActiveDiagramAction;
import org.topcased.adele.model.ADELE_Components.Package;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.DiagramInterchangeFactory;
import org.topcased.modeler.di.model.EMFSemanticModelBridge;
import org.topcased.modeler.diagrams.model.Diagrams;
import org.topcased.modeler.diagrams.model.DiagramsFactory;
import org.topcased.modeler.diagrams.model.util.DiagramsUtils;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.exceptions.BoundsFormatException;
import org.topcased.modeler.l10n.Messages;
import org.topcased.modeler.requests.RestoreConnectionsRequest;
import org.topcased.modeler.tools.Importer;
import org.topcased.modeler.utils.Utils;

/**
 * TODO: Review to use the batch initializer
 * This class creates a Diagram from an existing model
 * 
 * @author <a href="david.sciamma@anyware-tech.com">David Sciamma</a>
 */
public class AdeleDiagramFileInitializer
{
    private ResourceSet rsrcSet;

    /**
     * Constructor
     */
    public AdeleDiagramFileInitializer()
    {
        this(new ResourceSetImpl());
    }

    /**
     * Constructor
     * 
     * @param resourceSet The ResourceSet to use to create the Diagrams resource. Should be used only when a custom
     *        ResourceSet should be used instead of the default EMF one
     */
    public AdeleDiagramFileInitializer(ResourceSet resourceSet)
    {
        this.rsrcSet = resourceSet;
    }

    /**
     * Creates the diagram file for the given existing model
     * 
     * @param root the root EObject associated with the diagram
     * @param diagramId the ID of the diagram to create
     * @param diagramName The diagram name
     * @param initializeContent if <code>true</code>, try to initialize the graphical objects with the existing model
     *        objects
     * @param monitor the progress monitor
     * @throws IOException Throws if the diagram file cannot be written
     */
    public Modeler createDiagram(	final SKHierarchicalObject root, 
	    							String diagramId,
	    							final String diagramName, 
	    							final boolean initializeContent,
	    							final boolean pb_closeDiagram,
	    							final boolean pb_displayDialogs,
									final IProgressMonitor monitor )
    throws IOException {
        assert root != null;
        //assert diagramId != null;
        Modeler modeler;
        
        if ( diagramId == null || diagramId.isEmpty() ) {
       		diagramId = ADELE_Utils.getDiagramId( root );
        }

        assert monitor != null;

        monitor.beginTask(Messages.getString("DiagramFileInitializer.1"), 5); //$NON-NLS-1$

        IFile modelFile = getFile(root.eResource());
        
        if ( modelFile != null && modelFile.exists() ) {
            final IContainer container = modelFile.getParent();
            final String diagramFileName = modelFile.getName() + DiagramsUtils.DIAGRAM_EXT_SUFFIX;

            IFile diagramFile = container.getFile(new Path(diagramFileName));

            // Create the diagram file
            Resource diagramResource = createDiagramFile(root, diagramId, diagramName, diagramFile);
            monitor.worked(1);
            
            if ( initializeContent ) {
                // Initialize diagrams with existing objects
                modeler = importObjects( diagramResource, root, pb_closeDiagram, pb_displayDialogs, new SubProgressMonitor( monitor, 4 ) );
            }
            // The diagram should be open in all case
            else {
                // TODO call in the display thread
                modeler = createDiagram( diagramResource );

                if (modeler == null) {
                	ADELEPlugin.displayDialog(Messages.getString("DiagramFileInitializer.2"), Messages.getString("DiagramFileInitializer.3"), IStatus.ERROR); //$NON-NLS-1$ //$NON-NLS-2$
                }
            }
        }
        else {
        	modeler = null;
        }
        
        monitor.done();
        
        return modeler;
    }

    public Resource createDiagramIfDoesNotExists(	final SKHierarchicalObject p_root,
													final boolean pb_initializeContent, 
													final IProgressMonitor p_monitor )
	throws IOException {
    	final Resource rootRes = p_root.eResource();
    	final URI rootDiagResUri = URI.createURI( rootRes.getURI().toString() + DiagramsUtils.DIAGRAM_EXT_SUFFIX );
    	final ResourceSet resSet = rootRes.getResourceSet();
    	
    	if ( resSet == null || !semModelExists( resSet, rootDiagResUri )/*resSet.getURIConverter().exists( rootDiagResUri, null )*/ ) {
        	final boolean showMessageReadOnly = Modeler.isDisplayReadOnlyResourcesMessage();
        	Modeler.setDisplayReadOnlyResourcesMessage( false );
        	
        	try {
	        	final Modeler modeler = createDiagram( p_root, null, pb_initializeContent, true, false, p_monitor );
	        	final Diagrams diag = modeler.getDiagrams();
	        	
	        	if ( diag != null ) {
	        	   	return diag.eResource();
	        	}
        	}
        	finally {
        		Modeler.setDisplayReadOnlyResourcesMessage( showMessageReadOnly );
        	}
    	}
    	
    	return null;
    }
    
    private boolean semModelExists( final ResourceSet p_resSet,
    								final URI p_resUri ) 
    throws IOException {
    	if ( p_resSet.getURIConverter().exists( p_resUri, null ) ) {
    		final Resource res = p_resSet.getResource( p_resUri, true );
    		
    		if ( res.getContents().isEmpty() ) {
    			res.delete( null );
    			
    			return false;
    		}
    	}
    	
    	return false;
    }

    /**
     * Convert an Package or a Java project into a UML2 model
     * 
     * @param root the root EObject associated with the diagram
     * @param diagramId the ID of the diagram to create
     * @param initializeContent if <code>true</code>, try to initialize the graphical objects with the existing model
     *        objects
     * @param monitor the progress monitor
     * @throws IOException Throws if the diagram file cannot be written
     */
    public Modeler createDiagram(	final SKHierarchicalObject root,
    								final String diagramId, 
    								final boolean initializeContent,
    								final boolean pb_closeDiagram,
    								final boolean pb_displayDialogs,
									final IProgressMonitor monitor )
    throws IOException {
        return createDiagram( 	root,
		        				diagramId,
		        				Messages.getString("DiagramFileInitializer.4"), //$NON-NLS-1$
		        				initializeContent,
		        				pb_closeDiagram,
		        				pb_displayDialogs,
		        				monitor );
    }

    private Resource createDiagramFile( final SKHierarchicalObject root, 
							    		String diagramId, 
							    		final String name, 
							    		final IFile diagramFile )
    throws IOException {
        // retrieve the Diagrams and the DiagramInterchange factory singletons
        DiagramsFactory factory = DiagramsFactory.eINSTANCE;
        DiagramInterchangeFactory diFactory = DiagramInterchangeFactory.eINSTANCE;

        // create the EObject of the diagram model
        Diagrams diagrams = factory.createDiagrams();
        Diagram rootDiagram = diFactory.createDiagram();
        EMFSemanticModelBridge emfSemanticModelBridge = diFactory.createEMFSemanticModelBridge();

        // set the properties of the diagrams model
        diagrams.setModel(root);
        diagrams.getDiagrams().add(rootDiagram);

        // set the properties of the Diagram
        rootDiagram.setSize(new Dimension(100, 100));
        rootDiagram.setViewport(new Point(0, 0));
        rootDiagram.setPosition(new Point(0, 0));
        rootDiagram.setName(name);
        rootDiagram.setSemanticModel(emfSemanticModelBridge);

        // set the properties of the SemanticModelBridge
        emfSemanticModelBridge.setElement(root);
        emfSemanticModelBridge.setPresentation( diagramId );
        
        for ( final SKHierarchicalObject child : root.getChildren() ) {
        	if ( !child.getChildren().isEmpty() ) {
	        	final String subDiagramId = ADELE_Utils.getDiagramId(child);
	        	
	        	if ( subDiagramId != "" ) {
	        		createSubDiagram(child, subDiagramId, name, diagramFile,diagrams);
	        		
	        		// TODO: Uncomment when BA diagram synchronized.
	        		//createSubDiagram(child, "org.topcased.adele.editor.behaviorAnnex", name, diagramFile,diagrams);
	        	}
        	}
        }

        // create the diagram file and add the created model into
        URI fileURI = URI.createPlatformResourceURI(diagramFile.getFullPath().toString(), true);
        
        Resource resource = rsrcSet.createResource(fileURI);
        resource.getContents().add(diagrams);

        // Save the resource contents to the file system.
        resource.save(Collections.EMPTY_MAP);

        return resource;
    }

    private void createSubDiagram( 	final SKHierarchicalObject subElement,
    								final String diagramId,
    								final String name, 
    								final IFile diagramFile, 
    								final Diagrams diagrams )
    throws IOException {
    	if ( !( ((SKHierarchicalObject)subElement).getParent() instanceof Package) ) {
	        // retrieve the Diagrams and the DiagramInterchange factory singletons
	        DiagramsFactory factory = DiagramsFactory.eINSTANCE;
	        DiagramInterchangeFactory diFactory = DiagramInterchangeFactory.eINSTANCE;
	
	        // create the EObject of the diagram model
	        Diagrams subdiagrams = factory.createDiagrams();
	        
	        // DB: Set the model for sub diagrams.
	        subdiagrams.setModel( subElement );
	        final Diagram elementDiagram = diFactory.createDiagram();
	        EMFSemanticModelBridge emfSemanticModelBridge = diFactory.createEMFSemanticModelBridge();
	    	
	        subdiagrams.getDiagrams().add(elementDiagram);
	        diagrams.getSubdiagrams().add(subdiagrams);
	
	        // set the properties of the Diagram
	        elementDiagram.setSize(new Dimension(100, 100));
	        elementDiagram.setViewport(new Point(0, 0));
	        elementDiagram.setPosition(new Point(0, 0));
	        elementDiagram.setName(name);
	        elementDiagram.setSemanticModel(emfSemanticModelBridge);
	
	        // set the properties of the SemanticModelBridge
	        emfSemanticModelBridge.setElement(subElement);
	        emfSemanticModelBridge.setPresentation(diagramId);         
    	}
         
        if (diagramId!="org.topcased.adele.editor.behaviorAnnex") {
	        for ( final SKHierarchicalObject child : subElement.getChildren() ) {
	        	
	        	// DB: Do not create a diagram when there is no children to display.
	        	if ( !child.getChildren().isEmpty() ) {
		        	final String subDiagramId = ADELE_Utils.getDiagramId(child);
		        	
		        	if ( subDiagramId != "" ) {
		        		createSubDiagram(child, subDiagramId, name, diagramFile,diagrams);
		        		
		        		// TODO: Un-comment when BA is synchronized.
		        		//createSubDiagram(child, "org.topcased.adele.editor.behaviorAnnex", name, diagramFile,diagrams);
		        	}
	        	}
	        }
        }
    }
    
    private abstract class ModelerReturnRunnable implements Runnable {
    	protected Modeler modeler;
    	
    	public ModelerReturnRunnable() {
    		modeler = null;
    	}
    	
    	public Modeler getModeler() {
    		return modeler;
    	}
    }

    private Modeler importObjects(	final Resource diagramResource, 
    								final EObject p_rootSemElement,
    								final boolean pb_closeDiagram,
    								final boolean pb_displayDialogs,
									final IProgressMonitor monitor) {
    	final ModelerReturnRunnable runnable = new ModelerReturnRunnable() {
            /**
             * @see java.lang.Runnable#run()
             */
    		@Override
            public void run() {
            	modeler = syncImportObjects( diagramResource, p_rootSemElement, pb_closeDiagram, pb_displayDialogs, monitor );
            }
        };
        
        Display.getDefault().syncExec( runnable );
        
        return runnable.getModeler();
    }

    private EObject getActiveRoot(Modeler editor)
    {
        return Utils.getElement(editor.getActiveDiagram());
    }

    /**
     * Execute the import. This method must be called in the UI thread
     * 
     * @param diagramResource the diagram
     * @param model the model
     * @param monitor
     */
    protected Modeler syncImportObjects(	final Resource diagramResource,
	    									final EObject p_rootSemElement,
	    									final boolean pb_closeDiagram,
	    									final boolean pb_displayDialogs,
	    									final IProgressMonitor monitor ) {
       	final Modeler modeler = createDiagram( diagramResource );

        if ( modeler == null ) {
        	ADELEPlugin.displayDialog(Messages.getString("DiagramFileInitializer.5"), Messages.getString("DiagramFileInitializer.6"), IStatus.ERROR); //$NON-NLS-1$ //$NON-NLS-2$
        }
        else {
        	try {
	            // Import graphical element
	        	final Importer importer = new Importer(modeler, getActiveRoot(modeler).eContents());
	        	importer.setDisplayDialogs( pb_displayDialogs );
	        	final GraphicalEditPart target;
	
	            GraphicalViewer viewer = (GraphicalViewer) modeler.getAdapter(GraphicalViewer.class);
	            target = (GraphicalEditPart) viewer.getEditPartRegistry().get(modeler.getActiveDiagram());
	            Dimension insets = new Dimension(10, 10);
	            target.getContentPane().translateToAbsolute(insets);
	            importer.setLocation(target.getContentPane().getBounds().getTopLeft().translate(insets));
	            importer.setTargetEditPart(target);
	
	
	            try {
	                importer.run(monitor);
	               	restoreRootFeatureConnection(target);
	            }
	            catch (BoundsFormatException bfe) {
	            	if ( pb_displayDialogs ) {
	            		ADELEPlugin.displayDialog(Messages.getString("DiagramFileInitializer.7"), Messages.getString("DiagramFileInitializer.8") + bfe.getMessage(), IStatus.ERROR); //$NON-NLS-1$ //$NON-NLS-2$
	            	}
	            }
	            catch (InterruptedException ie)
	            {
	            	if ( pb_displayDialogs ) {
	            		ADELEPlugin.displayDialog(Messages.getString("DiagramFileInitializer.9"), Messages.getString("DiagramFileInitializer.10"), IStatus.INFO); //$NON-NLS-1$ //$NON-NLS-2$
	            	}
	            }
	            
	            Diagram rootDiag = modeler.getActiveDiagram();
	            
	            for (Diagrams diags : modeler.getDiagrams().getSubdiagrams()) {
		            for (Diagram diag: diags.getDiagrams() ) {
		            	if ( ((SKHierarchicalObject)Utils.getElement(diag)).getLevel()==2) {
			            	(new ChangeActiveDiagramAction(modeler, diag, Utils.getElement(diag))).run();
			            	syncImportSubObjects(modeler, diagramResource, getActiveRoot(modeler), pb_displayDialogs, monitor);
		            	}
		            }
	            }
	        	
		        (new ChangeActiveDiagramAction( modeler, rootDiag, Utils.getElement(rootDiag))).run();
		        
		        if ( pb_closeDiagram ) {
		        	modeler.close( false );
		        }
        	}
        	finally {
        		AbstractRefreshCommand.activeOnRealElements = true;
        	}
        }
        
        return modeler;
    }
    
    protected void syncImportSubObjects(	final Modeler modeler,
    										final Resource diagramResource,
    										final EObject model,
    										final boolean pb_displayDialogs,
    										final IProgressMonitor monitor ) {

        // Import graphical element
    	final Importer importer = new Importer( modeler, getActiveRoot( modeler ).eContents() );
    	importer.setDisplayDialogs( pb_displayDialogs );
    	final GraphicalViewer viewer = (GraphicalViewer) modeler.getAdapter( GraphicalViewer.class );
    	final GraphicalEditPart target = (GraphicalEditPart) viewer.getEditPartRegistry().get(modeler.getActiveDiagram());
        importer.setTargetEditPart( target );
        final Dimension insets = new Dimension( 10, 10 );
        target.getContentPane().translateToAbsolute( insets );
        importer.setLocation( target.getContentPane().getBounds().getTopLeft().translate( insets ) );

        try {
            importer.run( monitor );                
            restoreRootFeatureConnection(target);
        }
        catch ( final BoundsFormatException bfe ) {
        	if ( pb_displayDialogs ) {
        		ADELEPlugin.displayDialog(Messages.getString("DiagramFileInitializer.7"), Messages.getString("DiagramFileInitializer.8") + bfe.getMessage(), IStatus.ERROR); //$NON-NLS-1$ //$NON-NLS-2$
        	}
        }
        catch ( final InterruptedException ie ) {
        	if ( pb_displayDialogs ) {
        		ADELEPlugin.displayDialog(Messages.getString("DiagramFileInitializer.9"), Messages.getString("DiagramFileInitializer.10"), IStatus.INFO); //$NON-NLS-1$ //$NON-NLS-2$
        	}
        }
    }
    
    private void restoreRootFeatureConnection(GraphicalEditPart target) {
    	@SuppressWarnings("unchecked")
		Iterator<GraphicalEditPart> it = target.getChildren().iterator();
        while (it.hasNext()) {
            RestoreConnectionsRequest request = new RestoreConnectionsRequest();
            Command command = it.next().getCommand(request);
            if (command != null && command.canExecute())
            {
                command.execute();
            }
        }
    }

    private Modeler createDiagram( final Resource diagramResource ) {
    	Modeler modeler = null;
        IFile file = getFile(diagramResource);
        
        if (file != null && file.exists()) {
        	AbstractRefreshCommand.activeOnRealElements = false;
            
        	// Open the newly created model
    		try {
    			IEditorPart part = IDE.openEditor(	ADELEPlugin.getDefault().getWorkbench().getActiveWorkbenchWindow().getActivePage(), 
              										file, 
              										true );
    			if (part instanceof Modeler) {
    				modeler = (Modeler) part;
    			}
    		}
    		catch (PartInitException pie) {
    			modeler = null;
    		}
        }

        return modeler;
    }

    private IFile getFile(Resource resource)
    {
        if (resource != null)
        {
            URI uri = resource.getURI();
            uri = resource.getResourceSet().getURIConverter().normalize(uri);
            String scheme = uri.scheme();
            if ("platform".equals(scheme) && uri.segmentCount() > 1 && "resource".equals(uri.segment(0))) //$NON-NLS-1$ //$NON-NLS-2$
            {
                StringBuffer platformResourcePath = new StringBuffer();
                for (int j = 1; j < uri.segmentCount(); ++j)
                {
                    platformResourcePath.append('/');
                    // Fix #2409 : segments need to be decoded to make a consistency path
                    platformResourcePath.append(URI.decode(uri.segment(j)));
                }
                return ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(platformResourcePath.toString()));
            }
        }
        return null;
    }
//    
//    public static String getDiagramId(EObject obj) {
//    	if (obj instanceof Abstract)
//    		return "org.topcased.adele.editor.adeleabstract";
//    	if (obj instanceof BehaviorAnnex)
//    		return "org.topcased.adele.editor.behaviorAnnex";
//    	if (obj instanceof Bus)
//    		return "org.topcased.adele.editor.bus";
//    	if (obj instanceof Data)
//    		return "org.topcased.adele.editor.data";
//    	if (obj instanceof Device)
//    		return "org.topcased.adele.editor.device";
//    	if (obj instanceof Memory)
//    		return "org.topcased.adele.editor.memory";
//    	if (obj instanceof Package)
//    		return "org.topcased.adele.editor.packages";
//    	if (obj instanceof Process)
//    		return "org.topcased.adele.editor.process";
//    	if (obj instanceof Processor)
//    		return "org.topcased.adele.editor.processor";
//    	if (obj instanceof Subprogram)
//    		return "org.topcased.adele.editor.subprogram";
//    	if (obj instanceof SubprogramGroup)
//    		return "org.topcased.adele.editor.subprogramgroup";
//    	if (obj instanceof System)
//    		return "org.topcased.adele.editor.system";
//    	if (obj instanceof Thread)
//    		return "org.topcased.adele.editor.thread";
//    	if (obj instanceof ThreadGroup)
//    		return "org.topcased.adele.editor.threadgroup";
//    	if (obj instanceof VirtualBus)
//    		return "org.topcased.adele.editor.virtualbus";
//    	if (obj instanceof VirtualProcessor)
//    		return "org.topcased.adele.editor.virtualprocessor";
//    	return "";
//    }
}
