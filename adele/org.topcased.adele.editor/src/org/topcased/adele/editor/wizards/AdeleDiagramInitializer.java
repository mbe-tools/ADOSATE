/*******************************************************************************
 * Copyright (c) 2005 Anyware Technologies
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies) - initial API and implementation
 *    Sebastien Gabel (CS) - Bug fix #2985 & 
 *******************************************************************************/
package org.topcased.adele.editor.wizards;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.swt.widgets.Display;
import org.topcased.adele.common.utils.commands.AbstractRefreshCommand;
import org.topcased.adele.editor.utils.ADELE_Utils;
import org.topcased.adele.model.ADELE_Components.Package;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.DiagramInterchangeFactory;
import org.topcased.modeler.di.model.EMFSemanticModelBridge;
import org.topcased.modeler.diagrams.model.Diagrams;
import org.topcased.modeler.diagrams.model.DiagramsFactory;
import org.topcased.modeler.diagrams.model.util.DiagramsUtils;
import org.topcased.modeler.edit.DiagramsRootEditPart;
import org.topcased.modeler.editor.ModelerGraphicalViewer;
import org.topcased.modeler.extensions.DiagramsManager;
import org.topcased.modeler.l10n.Messages;
import org.topcased.modeler.tools.BatchImporter;
import org.topcased.modeler.utils.Utils;

/**
 * This class creates a Diagram from an existing model
 * 
 */
public class AdeleDiagramInitializer {

    /**
     * Constructor
     */
    public AdeleDiagramInitializer()
    {
        super();
    }

    /**
     * Creates the diagram file for the given existing model
     * 
     * @param root the root EObject associated with the diagram
     * @param diagramId the ID of the diagram to create
     * @param diagramName The diagram name
     * @param initializeContent if <code>true</code>, try to initialize the graphical objects with the existing model
     *        objects
     * @param monitor the progress monitor
     * @throws IOException Throws if the diagram file cannot be written
     */
    private Resource createDiagram(	final SKHierarchicalObject root, 
	    							String diagramId,
	    							final String diagramName, 
	    							final boolean initializeContent,
									final IProgressMonitor monitor )
    throws IOException {
        assert root != null;
        //assert diagramId != null;
        Resource diagramResource;
        
        if ( diagramId == null || diagramId.isEmpty() ) {
       		diagramId = ADELE_Utils.getDiagramId( root );
        }

        assert monitor != null;

        monitor.beginTask(Messages.getString("DiagramFileInitializer.1"), 5); //$NON-NLS-1$

        final IFile modelFile = getFile( root.eResource() );
        
        if ( modelFile != null && modelFile.exists() ) {
        	AbstractRefreshCommand.activeOnRealElements = false;
            final IContainer container = modelFile.getParent();
            final String diagramFileName = modelFile.getName() + DiagramsUtils.DIAGRAM_EXT_SUFFIX;

            IFile diagramFile = container.getFile(new Path(diagramFileName));

            // Create the diagram file
            diagramResource = createDiagramFile(root, diagramId, diagramName, diagramFile);
            monitor.worked(1);
            
            if ( initializeContent ) {
                // Initialize diagrams with existing objects
                importObjects( diagramResource, root, new SubProgressMonitor( monitor, 4 ) );
            }
        }
        else {
        	diagramResource = null;
        }
        
        monitor.done();
        
        return diagramResource;
    }

    public Resource createDiagramIfDoesNotExists(	final SKHierarchicalObject p_root,
													final boolean pb_initializeContent,
													final IProgressMonitor p_monitor )
	throws IOException {
    	final Resource rootRes = p_root.eResource();
    	final URI rootDiagResUri = URI.createURI( rootRes.getURI().toString() + DiagramsUtils.DIAGRAM_EXT_SUFFIX );
    	final ResourceSet resSet = rootRes.getResourceSet();
    	
    	if ( resSet == null || !semModelExists( resSet, rootDiagResUri ) ) {
        	return createDiagram( p_root, null, true, p_monitor );
    	}
    	
    	return null;
    }
    
    private boolean semModelExists( final ResourceSet p_resSet,
    								final URI p_resUri ) 
    throws IOException {
    	if ( p_resSet.getURIConverter().exists( p_resUri, null ) ) {
    		final Resource res = p_resSet.getResource( p_resUri, true );
    		
    		if ( res.getContents().isEmpty() ) {
    			res.delete( null );
    			
    			return false;
    		}
    	}
    	
    	return false;
    }

    /**
     * Convert an Package or a Java project into a UML2 model
     * 
     * @param root the root EObject associated with the diagram
     * @param diagramId the ID of the diagram to create
     * @param initializeContent if <code>true</code>, try to initialize the graphical objects with the existing model
     *        objects
     * @param monitor the progress monitor
     * @throws IOException Throws if the diagram file cannot be written
     */
    private Resource createDiagram(	final SKHierarchicalObject root,
    								final String diagramId, 
    								final boolean initializeContent,
    								final IProgressMonitor monitor )
    throws IOException {
        return createDiagram( 	root,
		        				diagramId,
		        				Messages.getString("DiagramFileInitializer.4"), //$NON-NLS-1$
		        				initializeContent,
		        				monitor );
    }

    private Resource createDiagramFile( final SKHierarchicalObject root, 
							    		String diagramId, 
							    		final String name, 
							    		final IFile diagramFile )
    throws IOException {
        // retrieve the Diagrams and the DiagramInterchange factory singletons
        DiagramsFactory factory = DiagramsFactory.eINSTANCE;
        DiagramInterchangeFactory diFactory = DiagramInterchangeFactory.eINSTANCE;

        // create the EObject of the diagram model
        Diagrams diagrams = factory.createDiagrams();
        Diagram rootDiagram = diFactory.createDiagram();
        EMFSemanticModelBridge emfSemanticModelBridge = diFactory.createEMFSemanticModelBridge();

        // set the properties of the diagrams model
        diagrams.setModel(root);
        diagrams.getDiagrams().add(rootDiagram);

        // set the properties of the Diagram
        rootDiagram.setSize(new Dimension(100, 100));
        rootDiagram.setViewport(new Point(0, 0));
        rootDiagram.setPosition(new Point(0, 0));
        rootDiagram.setName(name);
        rootDiagram.setSemanticModel(emfSemanticModelBridge);

        // set the properties of the SemanticModelBridge
        emfSemanticModelBridge.setElement(root);
        emfSemanticModelBridge.setPresentation( diagramId );
        
        for ( final SKHierarchicalObject child : root.getChildren() ) {
        	if ( !child.getChildren().isEmpty() ) {
	        	final String subDiagramId = ADELE_Utils.getDiagramId(child);
	        	
	        	if ( subDiagramId != "" ) {
	        		createSubDiagram(child, subDiagramId, name, diagramFile,diagrams);
	        		
	        		// TODO: Uncomment when BA diagram synchronized.
	        		//createSubDiagram(child, "org.topcased.adele.editor.behaviorAnnex", name, diagramFile,diagrams);
	        	}
        	}
        }

        // create the diagram file and add the created model into
        URI fileURI = URI.createPlatformResourceURI(diagramFile.getFullPath().toString(), true);
        final ResourceSet resSet = root.eResource().getResourceSet();
        Resource diagRes = resSet.getResource( fileURI, false );
        
        if ( diagRes == null ) {
        	diagRes = resSet.createResource(fileURI);
        }
        else {
        	diagRes.getContents().clear();
        }
        
        diagRes.getContents().add(diagrams);

        return diagRes;
    }

    private void createSubDiagram( 	final SKHierarchicalObject subElement,
    								final String diagramId,
    								final String name, 
    								final IFile diagramFile, 
    								final Diagrams diagrams )
    throws IOException {
    	if ( !( ( (SKHierarchicalObject) subElement ).getParent() instanceof Package) ) {
	        // retrieve the Diagrams and the DiagramInterchange factory singletons
	        DiagramsFactory factory = DiagramsFactory.eINSTANCE;
	        DiagramInterchangeFactory diFactory = DiagramInterchangeFactory.eINSTANCE;
	
	        // create the EObject of the diagram model
	        Diagrams subdiagrams = factory.createDiagrams();
	        
	        // DB: Set the model for sub diagrams.
	        subdiagrams.setModel( subElement );
	        final Diagram elementDiagram = diFactory.createDiagram();
	        EMFSemanticModelBridge emfSemanticModelBridge = diFactory.createEMFSemanticModelBridge();
	    	
	        subdiagrams.getDiagrams().add(elementDiagram);
	        diagrams.getSubdiagrams().add(subdiagrams);
	
	        // set the properties of the Diagram
	        elementDiagram.setSize(new Dimension(100, 100));
	        elementDiagram.setViewport(new Point(0, 0));
	        elementDiagram.setPosition(new Point(0, 0));
	        elementDiagram.setName(name);
	        elementDiagram.setSemanticModel(emfSemanticModelBridge);
	
	        // set the properties of the SemanticModelBridge
	        emfSemanticModelBridge.setElement(subElement);
	        emfSemanticModelBridge.setPresentation(diagramId);         
    	}
         
        if ( diagramId != "org.topcased.adele.editor.behaviorAnnex" ) {
	        for ( final SKHierarchicalObject child : subElement.getChildren() ) {
	        	
	        	// DB: Do not create a diagram when there is no children to display.
	        	if ( !child.getChildren().isEmpty() ) {
		        	final String subDiagramId = ADELE_Utils.getDiagramId(child);
		        	
		        	if ( subDiagramId != "" ) {
		        		createSubDiagram(child, subDiagramId, name, diagramFile,diagrams);
		        		
		        		// TODO: Un-comment when BA is synchronized.
		        		//createSubDiagram(child, "org.topcased.adele.editor.behaviorAnnex", name, diagramFile,diagrams);
		        	}
	        	}
	        }
        }
    }

    private void importObjects(		final Resource diagramResource, 
    								final EObject p_rootSemElement,
									final IProgressMonitor monitor) {
    	final Runnable runnable = new Runnable() {
            /**
             * @see java.lang.Runnable#run()
             */
    		@Override
            public void run() {
	    		try {
	            	syncImportObjects( diagramResource, p_rootSemElement, monitor );
	    		}
    			catch( final CoreException p_ex ) {
    				throw new RuntimeException( p_ex );
    			}
            }
        };
        
        Display.getDefault().syncExec( runnable );
    }

    private EObject getActiveRoot( final Diagram p_diagram ) {
        return Utils.getElement( p_diagram );
    }

    private GraphicalViewer createGraphicalViewer( 	final Diagram p_diagram,
    												final EObject p_rootSemElement,
    												final String p_diagramId )
    throws CoreException {
        final GraphicalViewer viewer = new ModelerGraphicalViewer();
        ScalableFreeformRootEditPart root = new DiagramsRootEditPart();
        root.activate();
        viewer.setRootEditPart( root );
        final EditPartFactory factory = DiagramsManager.getInstance().getConfiguration( p_diagramId ).getEditPartFactory();
        viewer.setEditPartFactory( factory );
		viewer.setContents( p_diagram );
        
        return viewer;
    }
    
    /**
     * Execute the import. This method must be called in the UI thread
     * 
     * @param diagramResource the diagram
     * @param model the model
     * @param monitor
     */
    @SuppressWarnings("unchecked")
	protected void syncImportObjects(	final Resource diagramResource,
	    								final EObject p_rootSemElement,
	    								final IProgressMonitor monitor )
    throws CoreException {
        // Import graphical element
		final Diagrams diagrams = (Diagrams) diagramResource.getContents().get( 0 );
		final Diagram diagram = diagrams.getDiagrams().get( 0 );
		final String diagramId = ADELE_Utils.getDiagramId( p_rootSemElement );
		final GraphicalViewer viewer = createGraphicalViewer( diagram, p_rootSemElement, diagramId );
    	final GraphicalEditPart target = (GraphicalEditPart) viewer.getEditPartRegistry().get( diagram );
        
    	final Dimension insets = new Dimension(10, 10);
        target.getContentPane().translateToAbsolute(insets);
        
        try {
        	final BatchImporter importer = new BatchImporter( 	diagramId,
        														viewer.getEditPartRegistry(),
        														p_rootSemElement.eContents() );
            importer.setLocation(target.getContentPane().getBounds().getTopLeft().translate(insets));
            importer.setTargetEditPart(target);
            importer.run(monitor);
            
            for ( final Diagrams diags : diagrams.getSubdiagrams() ) {
                for ( final Diagram diag : diags.getDiagrams() ) {
                	if ( ((SKHierarchicalObject)Utils.getElement(diag)).getLevel()==2) {
    	            	syncImportSubObjects( viewer, diagramResource, diag, getActiveRoot( diag ), monitor );
                	}
                }
            }
        }
        catch ( final InvocationTargetException p_ex ) {
        	p_ex.printStackTrace();
        }
        catch ( final CoreException p_ex ) {
        	p_ex.printStackTrace();
        }
        catch (InterruptedException ie) {
        }
        finally {
        	AbstractRefreshCommand.activeOnRealElements = true;
        }
    }
    
    @SuppressWarnings("unchecked")
	protected void syncImportSubObjects(	final GraphicalViewer p_viewer,
											final Resource diagramResource,
    										final Diagram p_activeDiagram,
    										final EObject p_semElement,
    										final IProgressMonitor monitor )
    throws CoreException {
		final String diagramId = ADELE_Utils.getDiagramId( p_semElement );
        final EditPartFactory factory = DiagramsManager.getInstance().getConfiguration( diagramId ).getEditPartFactory();
        p_viewer.setEditPartFactory( factory );
		p_viewer.setContents( p_activeDiagram );
		final GraphicalEditPart target = (GraphicalEditPart) p_viewer.getEditPartRegistry().get( p_activeDiagram );

        final BatchImporter importer = new BatchImporter( 	diagramId,
        													p_viewer.getEditPartRegistry(),
        													p_semElement.eContents() );
        importer.setTargetEditPart( target );
        final Dimension insets = new Dimension( 10, 10 );
        target.getContentPane().translateToAbsolute( insets );

        try {
        	importer.setLocation( target.getContentPane().getBounds().getTopLeft().translate( insets ) );
            importer.run( monitor );                
        }
        catch ( final InvocationTargetException p_ex ) {
        	p_ex.printStackTrace();
        }
        catch ( final CoreException p_ex ) {
        	p_ex.printStackTrace();
        }
        catch ( final InterruptedException ie ) {
        }
    }

    private IFile getFile(Resource resource)
    {
        if (resource != null)
        {
            URI uri = resource.getURI();
            uri = resource.getResourceSet().getURIConverter().normalize(uri);
            //String scheme = uri.scheme();
            if ( uri.isPlatformResource()/*"platform".equals(scheme)*/ && uri.segmentCount() > 1 /*&& "resource".equals(uri.segment(0))*/) 
            {
                StringBuffer platformResourcePath = new StringBuffer();
                for (int j = 1; j < uri.segmentCount(); ++j)
                {
                    platformResourcePath.append('/');
                    // Fix #2409 : segments need to be decoded to make a consistency path
                    platformResourcePath.append(URI.decode(uri.segment(j)));
                }
                return ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(platformResourcePath.toString()));
            }
        }
        return null;
    }
}
