/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *  	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.wizards;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.topcased.adele.common.utils.AdeleCommonUtils;
import org.topcased.modeler.wizards.DiagramsPage;

/**
 * @generated
 */
public class ADELEDiagramsPage extends DiagramsPage {
	
	protected static final String TEMPL_ID_PACKAGE = "org.topcased.adele.editor.templates.packages";
	protected static final String TEMPL_ID_SYSTEM = "org.topcased.adele.editor.templates.syste";

	/**
	 * @param pageName
	 * @param selection
	 * @generated
	 */
	public ADELEDiagramsPage(	final String p_pageName,
								final IStructuredSelection p_selection ) {
		super( p_pageName, p_selection, true );
	}

	public ADELEDiagramsPage(	final String p_pageName,
								final IStructuredSelection p_selection,
								final boolean pb_allowDiagramInitialization,
	    						final boolean pb_canCreateFromExistModel,
	    						final boolean pb_canChooseTemplate ) {
		super( p_pageName, p_selection, pb_allowDiagramInitialization, pb_canCreateFromExistModel, pb_canChooseTemplate );
	}
	
	/**
	 * @see org.topcased.modeler.wizards.DiagramsPage#getEditorID()
	 * @generated
	 */
	public String getEditorID() {
		return "org.topcased.adele.editor.editor.ADELEEditor";
	}

	/**
	 * @see org.topcased.modeler.wizards.DiagramsPage#getFileExtension()
	 * @generated
	 */
	public String getFileExtension() {
		return AdeleCommonUtils.ADELE_EXT;
	}

	/**
	 * @see org.topcased.modeler.wizards.DiagramsPage#getAdapterFactory()
	 * @generated
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ComposedAdapterFactory getAdapterFactory() {
		List factories = new ArrayList();
		factories
				.add(new org.topcased.adele.model.ADELE_Components.provider.ADELE_ComponentsItemProviderAdapterFactory());
		factories
				.add(new org.topcased.adele.model.ADELE_Features.provider.ADELE_FeaturesItemProviderAdapterFactory());
		factories
				.add(new org.topcased.adele.model.ADELE_Relations.provider.ADELE_RelationsItemProviderAdapterFactory());
		factories
				.add(new org.topcased.adele.model.ba_components.provider.Ba_componentsItemProviderAdapterFactory());
		factories
				.add(new org.topcased.adele.model.ba_features.provider.Ba_featuresItemProviderAdapterFactory());
		factories
				.add(new org.topcased.adele.model.ba_relations.provider.Ba_relationsItemProviderAdapterFactory());
		factories
				.add(new org.topcased.adele.model.KernelSpices.provider.KernelSpicesItemProviderAdapterFactory());
		factories
				.add(new org.topcased.adele.model.ObjectDescriptionModel.provider.ObjectDescriptionModelItemProviderAdapterFactory());
		factories.add(new ResourceItemProviderAdapterFactory());
		factories.add(new ReflectiveItemProviderAdapterFactory());

		return new ComposedAdapterFactory(factories);
	}

	/**
	 * @see org.topcased.modeler.wizards.DiagramsPage#getDefaultTemplateId()
	 * @return String
	 * @generated NOT
	 */
	public String getDefaultTemplateId() {
		//		return "org.topcased.adele.editor.templates.system";
		return "org.topcased.adele.editor.templates.packages";
	}

}
