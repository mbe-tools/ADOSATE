/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *  	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.wizards;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.ide.IDE;
import org.topcased.adele.editor.ADELEImageRegistry;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.extensions.Template;
import org.topcased.modeler.extensions.TemplatesManager;
import org.topcased.modeler.wizards.DiagramsPage;

/**
 * Generated wizard that offers the model creation facilities.
 *
 * @generated
 */
public class NewADELEDiagrams extends Wizard implements INewWizard {
	/**
	 * @generated
	 */
	protected IStructuredSelection selection;

	/**
	 * @generated
	 */
	protected DiagramsPage diagPage;

	/**
	 * @generated
	 */
	private IFile createdFile;

	private IContainer folder;

	/**
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench, org.eclipse.jface.viewers.IStructuredSelection)
	 * @generated
	 */
	public void init(IWorkbench workbench, IStructuredSelection sel) {
		createdFile = null;
		selection = sel;

		// TODO put the Wizard image
		setDefaultPageImageDescriptor(ADELEImageRegistry
				.getImageDescriptor("NEW_PAGE_WZD"));
		setDialogSettings(ADELEPlugin.getDefault().getDialogSettings());
		setWindowTitle("Create new ADELE diagrams");
	}

	/**
	 * @see org.eclipse.jface.wizard.IWizard#performFinish()
	 * @generated NOT
	 */
	@Override
	public boolean performFinish() {
		if ( diagPage.isPageComplete() ) {
			final WorkspaceModifyOperation op = new WorkspaceModifyOperation() {
				/**
				 * @see org.eclipse.ui.actions.WorkspaceModifyOperation#execute(org.eclipse.core.runtime.IProgressMonitor)
				 */
				@Override
				protected void execute( final IProgressMonitor p_monitor )
				throws CoreException, InvocationTargetException, InterruptedException {
					if ( diagPage.isNewModel() ) {
						createNewDiagram();
					}
					else {
						try {
							createDiagramFromExistingModel( p_monitor );
						}
						catch( final IOException p_ex ) {
							throw new InvocationTargetException( p_ex );
						}
					}
				}
			};

			try {
				getContainer().run( false, true, op );
				
				if ( createdFile != null && diagPage.isNewModel() ) {
					IDE.openEditor(	ADELEPlugin.getDefault().getWorkbench().getActiveWorkbenchWindow().getActivePage(),
									createdFile,
									true );
				}

				return true;
			} 
			catch( final InvocationTargetException ite ) {
				ADELEPlugin.log( ite );
			}
			catch( final PartInitException p_ex ) {
				ADELEPlugin.log( p_ex );
			}
			catch(InterruptedException ie) {
				// Wizard stopped
			}
		}
		
		return false;
	}

	protected void createNewDiagram() 
	throws CoreException {
		folder = diagPage.getSelectedIContainer();
		
		if ( folder instanceof IFolder && !folder.exists() ) {
			( (IFolder) folder ).create( true, true, null );
		}
		
		createModelFile();
		createDiagramFile();
	}

	protected Modeler createDiagramFromExistingModel( 	final SKHierarchicalObject p_diagRootSemObject,
														final boolean pb_showDialogs,
														final boolean pb_initialize,
														final boolean pb_closeDiagram,
														final IProgressMonitor p_monitor )
	throws IOException {
		final AdeleDiagramFileInitializer initializer = new AdeleDiagramFileInitializer();
		
		return initializer.createDiagram( 	p_diagRootSemObject,
											diagPage.getDiagramId(),
											diagPage.isInitialized(),
											pb_closeDiagram,
											pb_showDialogs,
											p_monitor );
	}
	
	protected Modeler createDiagramFromExistingModel( final IProgressMonitor p_monitor )
	throws IOException {
		return createDiagramFromExistingModel( 	(SKHierarchicalObject) diagPage.getDiagramEObject(),
												true,
												diagPage.isInitialized(),
												false,
												p_monitor );
	}

	/**
	 * @see org.eclipse.jface.wizard.IWizard#addPages()
	 * @generated
	 */
	public void addPages() {
		diagPage = new ADELEDiagramsPage("New ADELE editor Diagram", selection);
		diagPage.setTitle("ADELE Diagrams");
		diagPage.setDescription("Define the model diagram informations.");
		addPage(diagPage);
	}

	/**
	 * @see org.eclipse.jface.wizard.IWizard#canFinish()
	 * @generated
	 */
	public boolean canFinish() {
		return diagPage.isPageComplete();
	}

	/**
	 * @return true if the model file was successfully created
	 * @generated NOT
	 */
	protected void createModelFile()
	throws CoreException {
		Template template = TemplatesManager.getInstance().find(diagPage.getTemplateId()).getTemplateModel();
		template.setDestination(folder);
		template.addVariable("name", diagPage.getModelName());

		template.generate(new NullProgressMonitor());
	}

	/**
	 * @return true if the diagram file was successfully created
	 * @generated NOT
	 */
	protected void createDiagramFile() 
	throws CoreException {
		Template template = TemplatesManager.getInstance().find(diagPage.getTemplateId()).getTemplateDI();
		template.setDestination( folder );
		template.addVariable( "name", diagPage.getModelName() );
		// Bug #1395 : Add an additional variable used to encode the model file name
		template.addVariable(	"escapedName",
								URI.encodeFragment( diagPage.getModelName(), false ) );

		createdFile = (IFile) template.generate( new NullProgressMonitor() );
	}
}
