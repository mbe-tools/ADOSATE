/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.data.policies;

import java.util.Collections;
import java.util.List;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.commands.Command;
import org.topcased.adele.model.ADELE_Features.AbstractFeature;
import org.topcased.adele.model.ADELE_Features.FeatureGroup;
import org.topcased.adele.model.ADELE_Features.SubprogramAccess;
import org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.utils.Utils;

/**
 * @generated
 */
public class DataLayoutEditPolicy extends
		org.topcased.adele.editor.utils.policies.DataLayoutEditPolicy {
	/**
	 * Default constructor.
	 *
	 * @generated
	 */
	public DataLayoutEditPolicy() {
		super();
	}

	/**
	 * @see org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy#isAttachedToBorder(org.topcased.modeler.di.model.GraphNode)
	 * @generated
	 */
	@Override
	protected boolean isAttachedToBorder(GraphNode node) {
		if (Utils.getElement(node) instanceof SubprogramAccess) {
			return true;
		}
		if (Utils.getElement(node) instanceof SubprogramGroupAccess) {
			return true;
		}
		if (Utils.getElement(node) instanceof FeatureGroup) {
			return true;
		}
		if (Utils.getElement(node) instanceof AbstractFeature) {
			return true;
		}
		return super.isAttachedToBorder(node);
	}

	/**
	 * @see org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy#isValid(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	protected boolean isValid(EObject child, EObject parent) {
		if (!super.isValid(child, parent)) {
			return false;
		}

		if (child instanceof SubprogramAccess) {
			return true;
		}
		if (child instanceof SubprogramGroupAccess) {
			return true;
		}
		if (child instanceof FeatureGroup) {
			return true;
		}
		if (child instanceof AbstractFeature) {
			return true;
		}
		return false;
	}

	/**
	 * Search for a default provided container feature in the model object configuration and use it
	 * to contain the created object.
	 * @generated
	 */
	@Override
	protected Command getCreateCommand(EditDomain domain, GraphNode newObject,
			GraphNode newParent, EObject newContainerParent, Point location,
			Dimension dimension, int attach, List featuresList,
			boolean needModelUpdate) {
		final EObject semanticObject = Utils.getElement(newObject);
		final String newObjectClassName = semanticObject == null ? null
				: semanticObject.eClass().getName();

		EReference reference = null;
		final EPackage.Registry packageRegistry = newParent.eResource()
				.getResourceSet().getPackageRegistry();

		if (newObjectClassName != null) {

			if ("SubprogramAccess".equals(newObjectClassName)) {
				reference = (EReference) ((EClass) packageRegistry.getEPackage(
						"http://KernelSpices").getEClassifier("SKComponent"))
						.getEStructuralFeature("features");
			}

			else if ("SubprogramGroupAccess".equals(newObjectClassName)) {
				reference = (EReference) ((EClass) packageRegistry.getEPackage(
						"http://KernelSpices").getEClassifier("SKComponent"))
						.getEStructuralFeature("features");
			}

			else if ("FeatureGroup".equals(newObjectClassName)) {
				reference = (EReference) ((EClass) packageRegistry.getEPackage(
						"http://KernelSpices").getEClassifier("SKComponent"))
						.getEStructuralFeature("features");
			}

			else if ("AbstractFeature".equals(newObjectClassName)) {
				reference = (EReference) ((EClass) packageRegistry.getEPackage(
						"http://KernelSpices").getEClassifier("SKComponent"))
						.getEStructuralFeature("features");
			}

		}

		final List<? extends EStructuralFeature> features;

		if (reference == null) {
			features = featuresList;
		} else {
			features = Collections.singletonList(reference);
		}

		return super.getCreateCommand(domain, newObject, newParent,
				newContainerParent, location, dimension, attach, features,
				needModelUpdate);
	}
}