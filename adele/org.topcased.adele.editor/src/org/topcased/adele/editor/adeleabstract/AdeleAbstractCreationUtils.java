/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.adeleabstract;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch;
import org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch;
import org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch;
import org.topcased.adele.model.KernelSpices.util.KernelSpicesSwitch;
import org.topcased.adele.model.ObjectDescriptionModel.util.ObjectDescriptionModelSwitch;
import org.topcased.adele.model.ba_components.util.Ba_componentsSwitch;
import org.topcased.adele.model.ba_features.util.Ba_featuresSwitch;
import org.topcased.adele.model.ba_relations.util.Ba_relationsSwitch;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.editor.AbstractCreationUtils;
import org.topcased.modeler.graphconf.DiagramGraphConf;

/**
 * This utility class allows to create a GraphElement associated with a Model Object
 *
 * @generated
 */
public class AdeleAbstractCreationUtils extends AbstractCreationUtils {

	/**
	 * Constructor
	 *
	 * @param diagramConf the Diagram Graphical Configuration
	 * @generated
	 */
	public AdeleAbstractCreationUtils(DiagramGraphConf diagramConf) {
		super(diagramConf);
	}

	/**
	 * @generated
	 */
	private class GraphicADELE_ComponentsSwitch extends ADELE_ComponentsSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicADELE_ComponentsSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseData(org.topcased.adele.model.ADELE_Components.Data)
		 * @generated
		 */
		public Object caseData(
				org.topcased.adele.model.ADELE_Components.Data object) {
			if ("default".equals(presentation)) {
				return createGraphElementData(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseSubprogram(org.topcased.adele.model.ADELE_Components.Subprogram)
		 * @generated
		 */
		public Object caseSubprogram(
				org.topcased.adele.model.ADELE_Components.Subprogram object) {
			if ("default".equals(presentation)) {
				return createGraphElementSubprogram(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseSubprogramGroup(org.topcased.adele.model.ADELE_Components.SubprogramGroup)
		 * @generated
		 */
		public Object caseSubprogramGroup(
				org.topcased.adele.model.ADELE_Components.SubprogramGroup object) {
			if ("default".equals(presentation)) {
				return createGraphElementSubprogramGroup(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseThread(org.topcased.adele.model.ADELE_Components.Thread)
		 * @generated
		 */
		public Object caseThread(
				org.topcased.adele.model.ADELE_Components.Thread object) {
			if ("default".equals(presentation)) {
				return createGraphElementThread(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseThreadGroup(org.topcased.adele.model.ADELE_Components.ThreadGroup)
		 * @generated
		 */
		public Object caseThreadGroup(
				org.topcased.adele.model.ADELE_Components.ThreadGroup object) {
			if ("default".equals(presentation)) {
				return createGraphElementThreadGroup(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseProcess(org.topcased.adele.model.ADELE_Components.Process)
		 * @generated
		 */
		public Object caseProcess(
				org.topcased.adele.model.ADELE_Components.Process object) {
			if ("default".equals(presentation)) {
				return createGraphElementProcess(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseProcessor(org.topcased.adele.model.ADELE_Components.Processor)
		 * @generated
		 */
		public Object caseProcessor(
				org.topcased.adele.model.ADELE_Components.Processor object) {
			if ("default".equals(presentation)) {
				return createGraphElementProcessor(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseVirtualProcessor(org.topcased.adele.model.ADELE_Components.VirtualProcessor)
		 * @generated
		 */
		public Object caseVirtualProcessor(
				org.topcased.adele.model.ADELE_Components.VirtualProcessor object) {
			if ("default".equals(presentation)) {
				return createGraphElementVirtualProcessor(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseMemory(org.topcased.adele.model.ADELE_Components.Memory)
		 * @generated
		 */
		public Object caseMemory(
				org.topcased.adele.model.ADELE_Components.Memory object) {
			if ("default".equals(presentation)) {
				return createGraphElementMemory(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseBus(org.topcased.adele.model.ADELE_Components.Bus)
		 * @generated
		 */
		public Object caseBus(
				org.topcased.adele.model.ADELE_Components.Bus object) {
			if ("default".equals(presentation)) {
				return createGraphElementBus(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseVirtualBus(org.topcased.adele.model.ADELE_Components.VirtualBus)
		 * @generated
		 */
		public Object caseVirtualBus(
				org.topcased.adele.model.ADELE_Components.VirtualBus object) {
			if ("default".equals(presentation)) {
				return createGraphElementVirtualBus(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseDevice(org.topcased.adele.model.ADELE_Components.Device)
		 * @generated
		 */
		public Object caseDevice(
				org.topcased.adele.model.ADELE_Components.Device object) {
			if ("default".equals(presentation)) {
				return createGraphElementDevice(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseSystem(org.topcased.adele.model.ADELE_Components.System)
		 * @generated
		 */
		public Object caseSystem(
				org.topcased.adele.model.ADELE_Components.System object) {
			if ("default".equals(presentation)) {
				return createGraphElementSystem(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseAbstract(org.topcased.adele.model.ADELE_Components.Abstract)
		 * @generated
		 */
		public Object caseAbstract(
				org.topcased.adele.model.ADELE_Components.Abstract object) {
			if ("default".equals(presentation)) {
				return createGraphElementAbstract(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	// TODO: Find out why this was not generated... 
	/**
	 * @generated
	 */
	private class GraphicADELE_FeaturesSwitch extends ADELE_FeaturesSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicADELE_FeaturesSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseEventPort(org.topcased.adele.model.ADELE_Features.EventPort)
		 * @generated
		 */
		public Object caseEventPort(
				org.topcased.adele.model.ADELE_Features.EventPort object) {
			if ("default".equals(presentation)) {
				return createGraphElementEventPort(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseDataPort(org.topcased.adele.model.ADELE_Features.DataPort)
		 * @generated
		 */
		public Object caseDataPort(
				org.topcased.adele.model.ADELE_Features.DataPort object) {
			if ("default".equals(presentation)) {
				return createGraphElementDataPort(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseEventDataPort(org.topcased.adele.model.ADELE_Features.EventDataPort)
		 * @generated
		 */
		public Object caseEventDataPort(
				org.topcased.adele.model.ADELE_Features.EventDataPort object) {
			if ("default".equals(presentation)) {
				return createGraphElementEventDataPort(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseFeatureGroup(org.topcased.adele.model.ADELE_Features.FeatureGroup)
		 * @generated
		 */
		public Object caseFeatureGroup(
				org.topcased.adele.model.ADELE_Features.FeatureGroup object) {
			if ("default".equals(presentation)) {
				return createGraphElementFeatureGroup(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseSubprogramAccess(org.topcased.adele.model.ADELE_Features.SubprogramAccess)
		 * @generated
		 */
		public Object caseSubprogramAccess(
				org.topcased.adele.model.ADELE_Features.SubprogramAccess object) {
			if ("default".equals(presentation)) {
				return createGraphElementSubprogramAccess(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseSubprogramGroupAccess(org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess)
		 * @generated
		 */
		public Object caseSubprogramGroupAccess(
				org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess object) {
			if ("default".equals(presentation)) {
				return createGraphElementSubprogramGroupAccess(object,
						presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseBusAccess(org.topcased.adele.model.ADELE_Features.BusAccess)
		 * @generated
		 */
		public Object caseBusAccess(
				org.topcased.adele.model.ADELE_Features.BusAccess object) {
			if ("default".equals(presentation)) {
				return createGraphElementBusAccess(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseDataAccess(org.topcased.adele.model.ADELE_Features.DataAccess)
		 * @generated
		 */
		public Object caseDataAccess(
				org.topcased.adele.model.ADELE_Features.DataAccess object) {
			if ("default".equals(presentation)) {
				return createGraphElementDataAccess(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseAbstractFeature(org.topcased.adele.model.ADELE_Features.AbstractFeature)
		 * @generated
		 */
		public Object caseAbstractFeature(
				org.topcased.adele.model.ADELE_Features.AbstractFeature object) {
			if ("default".equals(presentation)) {
				return createGraphElementAbstractFeature(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseParameter(org.topcased.adele.model.ADELE_Features.Parameter)
		 * @generated
		 */
		public Object caseParameter(
				org.topcased.adele.model.ADELE_Features.Parameter object) {
			if ("default".equals(presentation)) {
				return createGraphElementParameter(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}
//
//	/**
//	 * @generated
//	 */
//	private class GraphicADELE_FeaturesSwitch extends ADELE_FeaturesSwitch {
//		/**
//		 * The presentation of the graphical element
//		 *
//		 * @generated
//		 */
//		private String presentation;
//
//		/**
//		 * Constructor
//		 *
//		 * @param presentation the presentation of the graphical element
//		 * @generated
//		 */
//		public GraphicADELE_FeaturesSwitch(String presentation) {
//			this.presentation = presentation;
//		}
//
//		/**
//		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
//		 * @generated
//		 */
//		public Object defaultCase(EObject object) {
//			return null;
//		}
//	}

	/**
	 * @generated
	 */
	private class GraphicADELE_RelationsSwitch extends ADELE_RelationsSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicADELE_RelationsSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseEventPortConnection(org.topcased.adele.model.ADELE_Relations.EventPortConnection)
		 * @generated
		 */
		public Object caseEventPortConnection(
				org.topcased.adele.model.ADELE_Relations.EventPortConnection object) {
			if ("default".equals(presentation)) {
				return createGraphElementEventPortConnection(object,
						presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseDataPortConnection(org.topcased.adele.model.ADELE_Relations.DataPortConnection)
		 * @generated
		 */
		public Object caseDataPortConnection(
				org.topcased.adele.model.ADELE_Relations.DataPortConnection object) {
			if ("default".equals(presentation)) {
				return createGraphElementDataPortConnection(object,
						presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseEventDataPortConnection(org.topcased.adele.model.ADELE_Relations.EventDataPortConnection)
		 * @generated
		 */
		public Object caseEventDataPortConnection(
				org.topcased.adele.model.ADELE_Relations.EventDataPortConnection object) {
			if ("default".equals(presentation)) {
				return createGraphElementEventDataPortConnection(object,
						presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseDataAccessConnection(org.topcased.adele.model.ADELE_Relations.DataAccessConnection)
		 * @generated
		 */
		public Object caseDataAccessConnection(
				org.topcased.adele.model.ADELE_Relations.DataAccessConnection object) {
			if ("default".equals(presentation)) {
				return createGraphElementDataAccessConnection(object,
						presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseSubprogramAccessConnection(org.topcased.adele.model.ADELE_Relations.SubprogramAccessConnection)
		 * @generated
		 */
		public Object caseSubprogramAccessConnection(
				org.topcased.adele.model.ADELE_Relations.SubprogramAccessConnection object) {
			if ("default".equals(presentation)) {
				return createGraphElementSubprogramAccessConnection(object,
						presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseSubprogramGroupAccessConnection(org.topcased.adele.model.ADELE_Relations.SubprogramGroupAccessConnection)
		 * @generated
		 */
		public Object caseSubprogramGroupAccessConnection(
				org.topcased.adele.model.ADELE_Relations.SubprogramGroupAccessConnection object) {
			if ("default".equals(presentation)) {
				return createGraphElementSubprogramGroupAccessConnection(
						object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseBusAccessConnection(org.topcased.adele.model.ADELE_Relations.BusAccessConnection)
		 * @generated
		 */
		public Object caseBusAccessConnection(
				org.topcased.adele.model.ADELE_Relations.BusAccessConnection object) {
			if ("default".equals(presentation)) {
				return createGraphElementBusAccessConnection(object,
						presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseParameterConnection(org.topcased.adele.model.ADELE_Relations.ParameterConnection)
		 * @generated
		 */
		public Object caseParameterConnection(
				org.topcased.adele.model.ADELE_Relations.ParameterConnection object) {
			if ("default".equals(presentation)) {
				return createGraphElementParameterConnection(object,
						presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseAbstractFeatureConnection(org.topcased.adele.model.ADELE_Relations.AbstractFeatureConnection)
		 * @generated
		 */
		public Object caseAbstractFeatureConnection(
				org.topcased.adele.model.ADELE_Relations.AbstractFeatureConnection object) {
			if ("default".equals(presentation)) {
				return createGraphElementAbstractFeatureConnection(object,
						presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseFeatureGroupConnection(org.topcased.adele.model.ADELE_Relations.FeatureGroupConnection)
		 * @generated
		 */
		public Object caseFeatureGroupConnection(
				org.topcased.adele.model.ADELE_Relations.FeatureGroupConnection object) {
			if ("default".equals(presentation)) {
				return createGraphElementFeatureGroupConnection(object,
						presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicBa_componentsSwitch extends Ba_componentsSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicBa_componentsSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ba_components.util.Ba_componentsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicBa_featuresSwitch extends Ba_featuresSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicBa_featuresSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ba_features.util.Ba_featuresSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicBa_relationsSwitch extends Ba_relationsSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicBa_relationsSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ba_relations.util.Ba_relationsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicKernelSpicesSwitch extends KernelSpicesSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicKernelSpicesSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.KernelSpices.util.KernelSpicesSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicObjectDescriptionModelSwitch extends
			ObjectDescriptionModelSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicObjectDescriptionModelSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ObjectDescriptionModel.util.ObjectDescriptionModelSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @see org.topcased.modeler.editor.ICreationUtils#createGraphElement(org.eclipse.emf.ecore.EObject, java.lang.String)
	 * @generated
	 */
	public GraphElement createGraphElement(EObject obj, String presentation) {
		Object graphElt = null;

		if ("http://ADELE_Components".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicADELE_ComponentsSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ADELE_Features".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicADELE_FeaturesSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ADELE_Relations".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicADELE_RelationsSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ba_components/1.0".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicBa_componentsSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ba_features/1.0".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicBa_featuresSwitch(presentation).doSwitch(obj);
		}
		if ("http://ba_relations/1.0".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicBa_relationsSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://KernelSpices".equals(obj.eClass().getEPackage().getNsURI())) {
			graphElt = new GraphicKernelSpicesSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ObjectDescriptionModel".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicObjectDescriptionModelSwitch(presentation)
					.doSwitch(obj);
		}

		return (GraphElement) graphElt;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementData(
			org.topcased.adele.model.ADELE_Components.Data element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementSubprogram(
			org.topcased.adele.model.ADELE_Components.Subprogram element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementSubprogramGroup(
			org.topcased.adele.model.ADELE_Components.SubprogramGroup element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementThread(
			org.topcased.adele.model.ADELE_Components.Thread element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementThreadGroup(
			org.topcased.adele.model.ADELE_Components.ThreadGroup element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementProcess(
			org.topcased.adele.model.ADELE_Components.Process element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementProcessor(
			org.topcased.adele.model.ADELE_Components.Processor element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementVirtualProcessor(
			org.topcased.adele.model.ADELE_Components.VirtualProcessor element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementMemory(
			org.topcased.adele.model.ADELE_Components.Memory element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementBus(
			org.topcased.adele.model.ADELE_Components.Bus element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementVirtualBus(
			org.topcased.adele.model.ADELE_Components.VirtualBus element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementDevice(
			org.topcased.adele.model.ADELE_Components.Device element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementSystem(
			org.topcased.adele.model.ADELE_Components.System element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementAbstract(
			org.topcased.adele.model.ADELE_Components.Abstract element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementEventPortConnection(
			org.topcased.adele.model.ADELE_Relations.EventPortConnection element,
			String presentation) {
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementDataPortConnection(
			org.topcased.adele.model.ADELE_Relations.DataPortConnection element,
			String presentation) {
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementEventDataPortConnection(
			org.topcased.adele.model.ADELE_Relations.EventDataPortConnection element,
			String presentation) {
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementDataAccessConnection(
			org.topcased.adele.model.ADELE_Relations.DataAccessConnection element,
			String presentation) {
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementSubprogramAccessConnection(
			org.topcased.adele.model.ADELE_Relations.SubprogramAccessConnection element,
			String presentation) {
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementSubprogramGroupAccessConnection(
			org.topcased.adele.model.ADELE_Relations.SubprogramGroupAccessConnection element,
			String presentation) {
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementBusAccessConnection(
			org.topcased.adele.model.ADELE_Relations.BusAccessConnection element,
			String presentation) {
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementParameterConnection(
			org.topcased.adele.model.ADELE_Relations.ParameterConnection element,
			String presentation) {
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementAbstractFeatureConnection(
			org.topcased.adele.model.ADELE_Relations.AbstractFeatureConnection element,
			String presentation) {
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementFeatureGroupConnection(
			org.topcased.adele.model.ADELE_Relations.FeatureGroupConnection element,
			String presentation) {
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * Create the ModelObject with its initial children
	 * 
	 * @param obj the model object
	 * @return the model object with its children
	 * @generated
	 */
	public EObject createModelObject(EObject obj) {
		return obj;
	}

	/**
	 * Get the preference store associated with the current editor.
	 * 
	 * @return IPreferenceStore
	 * @generated
	 */
	private IPreferenceStore getPreferenceStore() {
		IEditorInput editorInput = ADELEPlugin.getActivePage()
				.getActiveEditor().getEditorInput();
		if (editorInput instanceof IFileEditorInput) {
			IProject project = ((IFileEditorInput) editorInput).getFile()
					.getProject();
			Preferences root = Platform.getPreferencesService().getRootNode();
			try {
				if (root.node(ProjectScope.SCOPE).node(project.getName())
						.nodeExists(ADELEPlugin.getId())) {
					return new ScopedPreferenceStore(new ProjectScope(project),
							ADELEPlugin.getId());
				}
			} catch (BackingStoreException e) {
				e.printStackTrace();
			}
		}
		return ADELEPlugin.getDefault().getPreferenceStore();
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementEventPort(
			org.topcased.adele.model.ADELE_Features.EventPort element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementDataPort(
			org.topcased.adele.model.ADELE_Features.DataPort element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementEventDataPort(
			org.topcased.adele.model.ADELE_Features.EventDataPort element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementFeatureGroup(
			org.topcased.adele.model.ADELE_Features.FeatureGroup element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementSubprogramAccess(
			org.topcased.adele.model.ADELE_Features.SubprogramAccess element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementSubprogramGroupAccess(
			org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementBusAccess(
			org.topcased.adele.model.ADELE_Features.BusAccess element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementDataAccess(
			org.topcased.adele.model.ADELE_Features.DataAccess element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementAbstractFeature(
			org.topcased.adele.model.ADELE_Features.AbstractFeature element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementParameter(
			org.topcased.adele.model.ADELE_Features.Parameter element,
			String presentation) {
		return createGraphNode(element, presentation);
	}
}
