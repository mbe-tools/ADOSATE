/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.adeleabstract;

import java.net.URL;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.gef.EditPartFactory;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.adele.editor.adeleabstract.edit.AbstractEditPart;
import org.topcased.adele.editor.adeleabstract.edit.AbstractFeatureConnectionEditPart;
import org.topcased.adele.editor.adeleabstract.edit.AbstractFeatureEditPart;
import org.topcased.adele.editor.adeleabstract.edit.BusAccessConnectionEditPart;
import org.topcased.adele.editor.adeleabstract.edit.BusAccessEditPart;
import org.topcased.adele.editor.adeleabstract.edit.BusEditPart;
import org.topcased.adele.editor.adeleabstract.edit.DataAccessConnectionEditPart;
import org.topcased.adele.editor.adeleabstract.edit.DataAccessEditPart;
import org.topcased.adele.editor.adeleabstract.edit.DataEditPart;
import org.topcased.adele.editor.adeleabstract.edit.DataPortConnectionEditPart;
import org.topcased.adele.editor.adeleabstract.edit.DataPortEditPart;
import org.topcased.adele.editor.adeleabstract.edit.DeviceEditPart;
import org.topcased.adele.editor.adeleabstract.edit.EventDataPortConnectionEditPart;
import org.topcased.adele.editor.adeleabstract.edit.EventDataPortEditPart;
import org.topcased.adele.editor.adeleabstract.edit.EventPortConnectionEditPart;
import org.topcased.adele.editor.adeleabstract.edit.EventPortEditPart;
import org.topcased.adele.editor.adeleabstract.edit.FeatureGroupConnectionEditPart;
import org.topcased.adele.editor.adeleabstract.edit.FeatureGroupEditPart;
import org.topcased.adele.editor.adeleabstract.edit.MemoryEditPart;
import org.topcased.adele.editor.adeleabstract.edit.ParameterConnectionEditPart;
import org.topcased.adele.editor.adeleabstract.edit.ParameterEditPart;
import org.topcased.adele.editor.adeleabstract.edit.ProcessEditPart;
import org.topcased.adele.editor.adeleabstract.edit.ProcessorEditPart;
import org.topcased.adele.editor.adeleabstract.edit.SubprogramAccessConnectionEditPart;
import org.topcased.adele.editor.adeleabstract.edit.SubprogramAccessEditPart;
import org.topcased.adele.editor.adeleabstract.edit.SubprogramEditPart;
import org.topcased.adele.editor.adeleabstract.edit.SubprogramGroupAccessConnectionEditPart;
import org.topcased.adele.editor.adeleabstract.edit.SubprogramGroupAccessEditPart;
import org.topcased.adele.editor.adeleabstract.edit.SubprogramGroupEditPart;
import org.topcased.adele.editor.adeleabstract.edit.SystemEditPart;
import org.topcased.adele.editor.adeleabstract.edit.ThreadEditPart;
import org.topcased.adele.editor.adeleabstract.edit.ThreadGroupEditPart;
import org.topcased.adele.editor.adeleabstract.edit.VirtualBusEditPart;
import org.topcased.adele.editor.adeleabstract.edit.VirtualProcessorEditPart;
import org.topcased.modeler.editor.IConfiguration;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.IPaletteManager;
import org.topcased.modeler.graphconf.DiagramGraphConf;
import org.topcased.modeler.graphconf.exceptions.MissingGraphConfFileException;

/**
 * A diagram configuration : manages Palette, EditPartFactory for this diagram.
 *
 * @generated
 */
public class AdeleAbstractConfiguration implements IConfiguration {
	/**
	 * @generated
	 */
	private AdeleAbstractPaletteManager paletteManager;

	/**
	 * @generated
	 */
	private AdeleAbstractEditPartFactory editPartFactory;

	/**
	 * @generated
	 */
	private AdeleAbstractCreationUtils creationUtils;

	/**
	 * The DiagramGraphConf that contains graphical informations on the configuration
	 * @generated
	 */
	private DiagramGraphConf diagramGraphConf;

	/**
	 * Constructor. Initialize Adapter factories.
	 *
	 * @generated
	 */
	public AdeleAbstractConfiguration() {
		registerAdapters();
	}

	/**
	 * Registers the Adapter Factories for all the EditParts
	 *
	 * @generated
	 */
	private void registerAdapters() {
		Platform.getAdapterManager().registerAdapters(
				new EditPart2ModelAdapterFactory(DataEditPart.class,
						org.topcased.adele.model.ADELE_Components.Data.class),
				DataEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								SubprogramEditPart.class,
								org.topcased.adele.model.ADELE_Components.Subprogram.class),
						SubprogramEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								SubprogramGroupEditPart.class,
								org.topcased.adele.model.ADELE_Components.SubprogramGroup.class),
						SubprogramGroupEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								ThreadEditPart.class,
								org.topcased.adele.model.ADELE_Components.Thread.class),
						ThreadEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								ThreadGroupEditPart.class,
								org.topcased.adele.model.ADELE_Components.ThreadGroup.class),
						ThreadGroupEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								ProcessEditPart.class,
								org.topcased.adele.model.ADELE_Components.Process.class),
						ProcessEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								ProcessorEditPart.class,
								org.topcased.adele.model.ADELE_Components.Processor.class),
						ProcessorEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								VirtualProcessorEditPart.class,
								org.topcased.adele.model.ADELE_Components.VirtualProcessor.class),
						VirtualProcessorEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								MemoryEditPart.class,
								org.topcased.adele.model.ADELE_Components.Memory.class),
						MemoryEditPart.class);
		Platform.getAdapterManager().registerAdapters(
				new EditPart2ModelAdapterFactory(BusEditPart.class,
						org.topcased.adele.model.ADELE_Components.Bus.class),
				BusEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								VirtualBusEditPart.class,
								org.topcased.adele.model.ADELE_Components.VirtualBus.class),
						VirtualBusEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								DeviceEditPart.class,
								org.topcased.adele.model.ADELE_Components.Device.class),
						DeviceEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								SystemEditPart.class,
								org.topcased.adele.model.ADELE_Components.System.class),
						SystemEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								AbstractEditPart.class,
								org.topcased.adele.model.ADELE_Components.Abstract.class),
						AbstractEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								EventPortEditPart.class,
								org.topcased.adele.model.ADELE_Features.EventPort.class),
						EventPortEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								DataPortEditPart.class,
								org.topcased.adele.model.ADELE_Features.DataPort.class),
						DataPortEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								EventDataPortEditPart.class,
								org.topcased.adele.model.ADELE_Features.EventDataPort.class),
						EventDataPortEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								FeatureGroupEditPart.class,
								org.topcased.adele.model.ADELE_Features.FeatureGroup.class),
						FeatureGroupEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								DataAccessEditPart.class,
								org.topcased.adele.model.ADELE_Features.DataAccess.class),
						DataAccessEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								SubprogramAccessEditPart.class,
								org.topcased.adele.model.ADELE_Features.SubprogramAccess.class),
						SubprogramAccessEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								SubprogramGroupAccessEditPart.class,
								org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess.class),
						SubprogramGroupAccessEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								BusAccessEditPart.class,
								org.topcased.adele.model.ADELE_Features.BusAccess.class),
						BusAccessEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								AbstractFeatureEditPart.class,
								org.topcased.adele.model.ADELE_Features.AbstractFeature.class),
						AbstractFeatureEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								ParameterEditPart.class,
								org.topcased.adele.model.ADELE_Features.Parameter.class),
						ParameterEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								BusAccessConnectionEditPart.class,
								org.topcased.adele.model.ADELE_Relations.BusAccessConnection.class),
						BusAccessConnectionEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								DataAccessConnectionEditPart.class,
								org.topcased.adele.model.ADELE_Relations.DataAccessConnection.class),
						DataAccessConnectionEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								EventPortConnectionEditPart.class,
								org.topcased.adele.model.ADELE_Relations.EventPortConnection.class),
						EventPortConnectionEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								DataPortConnectionEditPart.class,
								org.topcased.adele.model.ADELE_Relations.DataPortConnection.class),
						DataPortConnectionEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								EventDataPortConnectionEditPart.class,
								org.topcased.adele.model.ADELE_Relations.EventDataPortConnection.class),
						EventDataPortConnectionEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								SubprogramAccessConnectionEditPart.class,
								org.topcased.adele.model.ADELE_Relations.SubprogramAccessConnection.class),
						SubprogramAccessConnectionEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								SubprogramGroupAccessConnectionEditPart.class,
								org.topcased.adele.model.ADELE_Relations.SubprogramGroupAccessConnection.class),
						SubprogramGroupAccessConnectionEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								ParameterConnectionEditPart.class,
								org.topcased.adele.model.ADELE_Relations.ParameterConnection.class),
						ParameterConnectionEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								AbstractFeatureConnectionEditPart.class,
								org.topcased.adele.model.ADELE_Relations.AbstractFeatureConnection.class),
						AbstractFeatureConnectionEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								FeatureGroupConnectionEditPart.class,
								org.topcased.adele.model.ADELE_Relations.FeatureGroupConnection.class),
						FeatureGroupConnectionEditPart.class);
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getId()
	 * @generated
	 */
	public String getId() {
		return new String("org.topcased.adele.editor.adeleabstract");
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getName()
	 * @generated
	 */
	public String getName() {
		return new String("ADELE Abstract");
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getEditPartFactory()
	 * @generated
	 */
	public EditPartFactory getEditPartFactory() {
		if (editPartFactory == null) {
			editPartFactory = new AdeleAbstractEditPartFactory();
		}

		return editPartFactory;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getPaletteManager()
	 * @generated
	 */
	public IPaletteManager getPaletteManager() {
		if (paletteManager == null) {
			paletteManager = new AdeleAbstractPaletteManager(getCreationUtils());
		}

		return paletteManager;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getCreationUtils()
	 * @generated
	 */
	public ICreationUtils getCreationUtils() {
		if (creationUtils == null) {
			creationUtils = new AdeleAbstractCreationUtils(
					getDiagramGraphConf());
		}

		return creationUtils;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getDiagramGraphConf()
	 * @generated
	 */
	public DiagramGraphConf getDiagramGraphConf() {
		if (diagramGraphConf == null) {
			URL url = ADELEPlugin
					.getDefault()
					.getBundle()
					.getResource(
							"org/topcased/adele/editor/adeleabstract/diagram.graphconf");
			if (url != null) {
				URI fileURI = URI.createURI(url.toString());
				ResourceSet resourceSet = new ResourceSetImpl();
				Resource resource = resourceSet.getResource(fileURI, true);
				if (resource != null
						&& resource.getContents().get(0) instanceof DiagramGraphConf) {
					diagramGraphConf = (DiagramGraphConf) resource
							.getContents().get(0);
				}
			} else {
				new MissingGraphConfFileException(
						"The *.diagramgraphconf file can not be retrieved. Check if the path is correct in the Configuration class of your diagram.");
			}
		}

		return diagramGraphConf;
	}

}