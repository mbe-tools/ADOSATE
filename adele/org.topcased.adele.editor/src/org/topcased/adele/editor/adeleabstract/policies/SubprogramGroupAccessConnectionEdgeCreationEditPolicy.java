/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.adeleabstract.policies;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.commands.Command;
import org.topcased.adele.editor.adeleabstract.commands.SubprogramGroupAccessConnectionEdgeCreationCommand;
import org.topcased.adele.model.ADELE_Relations.SubprogramGroupAccessConnection;
import org.topcased.modeler.commands.CreateTypedEdgeCommand;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.utils.SourceTargetData;
import org.topcased.modeler.utils.Utils;

/**
 * ADELE_SubprogramGroupAccessConnection edge creation
 *
 * @generated
 */
public class SubprogramGroupAccessConnectionEdgeCreationEditPolicy
		extends
		org.topcased.adele.editor.utils.policies.SubprogramGroupAccessConnectionEdgeCreationEditPolicy {
	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#createCommand(org.eclipse.gef.EditDomain, org.topcased.modeler.di.model.GraphEdge, org.topcased.modeler.di.model.GraphElement)
	 * @generated
	 */
	protected CreateTypedEdgeCommand createCommand(EditDomain domain,
			GraphEdge edge, GraphElement source) {
		return new SubprogramGroupAccessConnectionEdgeCreationCommand(domain,
				edge, source);
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#checkEdge(org.topcased.modeler.di.model.GraphEdge)
	 * @generated
	 */
	protected boolean checkEdge(GraphEdge edge) {
		return Utils.getElement(edge) instanceof SubprogramGroupAccessConnection;
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#checkSource(org.topcased.modeler.di.model.GraphElement)
	 * @generated
	 */
	protected boolean checkSource(GraphElement source) {
		if (!super.checkSource(source)) {
			return false;
		}

		EObject object = Utils.getElement(source);
		if (object instanceof org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess) {
			return true;
		}
		if (object instanceof org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess) {
			return true;
		}
		if (object instanceof org.topcased.adele.model.ADELE_Components.SubprogramGroup) {
			return true;
		}
		return false;
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#checkTargetForSource(org.topcased.modeler.di.model.GraphElement, org.topcased.modeler.di.model.GraphElement)
	 * @generated
	 */
	protected boolean checkTargetForSource(GraphElement source,
			GraphElement target) {
		EObject sourceObject = Utils.getElement(source);
		EObject targetObject = Utils.getElement(target);
		if (!super.checkTargetForSource(source, target)) {
			return false;
		}

		if (sourceObject instanceof org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess
				&& targetObject instanceof org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess) {
			if (!sourceObject.equals(targetObject)) {
				return true;
			}
		}

		if (sourceObject instanceof org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess
				&& targetObject instanceof org.topcased.adele.model.ADELE_Components.SubprogramGroup) {
			if (!sourceObject.equals(targetObject)) {
				return true;
			}
		}

		if (sourceObject instanceof org.topcased.adele.model.ADELE_Components.SubprogramGroup
				&& targetObject instanceof org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess) {
			if (!sourceObject.equals(targetObject)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#checkCommand(org.eclipse.gef.commands.Command)
	 * @generated
	 */
	protected boolean checkCommand(Command command) {
		return command instanceof SubprogramGroupAccessConnectionEdgeCreationCommand;
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#getSourceTargetData(org.topcased.modeler.di.model.GraphElement, org.topcased.modeler.di.model.GraphElement)
	 * @generated
	 */
	protected SourceTargetData getSourceTargetData(GraphElement source,
			GraphElement target) {
		EObject sourceObject = Utils.getElement(source);
		EObject targetObject = Utils.getElement(target);

		if (sourceObject instanceof org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess
				&& targetObject instanceof org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess) {
			return new SourceTargetData(false, false, SourceTargetData.DIAGRAM,
					"org.topcased.adele.model.ADELE_Components.Abstract",
					"subprogramGroupAccessConnection", null, null, null, null,
					null, null);
		}
		if (sourceObject instanceof org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess
				&& targetObject instanceof org.topcased.adele.model.ADELE_Components.SubprogramGroup) {
			return new SourceTargetData(false, false, SourceTargetData.DIAGRAM,
					"org.topcased.adele.model.ADELE_Components.Abstract",
					"subprogramGroupAccessConnection", null, null, null, null,
					null, null);
		}
		if (sourceObject instanceof org.topcased.adele.model.ADELE_Components.SubprogramGroup
				&& targetObject instanceof org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess) {
			return new SourceTargetData(false, false, SourceTargetData.DIAGRAM,
					"org.topcased.adele.model.ADELE_Components.Abstract",
					"subprogramGroupAccessConnection", null, null, null, null,
					null, null);
		}
		return null;
	}

}