/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.adeleabstract.preferences;

import org.topcased.adele.editor.packages.preferences.PackageDiagramPreferenceConstants;

/**
 * A set of constant that are used in the diagram graphical preferences
 * 
 * @generated NOT
 */
public interface AdeleAbstractDiagramPreferenceConstants extends
		PackageDiagramPreferenceConstants {
}