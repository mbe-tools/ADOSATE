/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.adeleabstract.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LineBorder;
import org.topcased.draw2d.layout.BorderAttachedLayout;
import org.topcased.modeler.figures.DiagramFigure;

/**
 * The figure to display a ADELE Abstract.
 *
 * @generated
 */
public class AdeleAbstractDiagramFigure extends DiagramFigure {

	/**
	 * @see org.topcased.modeler.figures.DiagramFigure#createContainer()
	 * @generated
	 */
	protected IFigure createContainer() {
		Figure container = new Figure();
		container.setLayoutManager(new BorderAttachedLayout());
		container.setOpaque(true);
		container.setBorder(new LineBorder(1));
		return container;
	}
}
