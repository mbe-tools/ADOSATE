/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.adeleabstract.preferences;

import java.util.HashMap;

import org.eclipse.jface.resource.StringConverter;
import org.topcased.modeler.preferences.ITopcasedPreferenceInitializer;

/**
 * Initialize the preferences of AdeleAbstract diagram
 * 
 * @generated
 */
public class AdeleAbstractDiagramTopcasedPreferenceInitializer implements
		ITopcasedPreferenceInitializer {

	/** 
	 * @see org.topcased.modeler.preferences.ITopcasedPreferenceInitializer#getDefaultPreference()
	 *	@generated
	 */
	public HashMap<String, String> getDefaultPreference() {
		HashMap<String, String> defaultAdeleAbstractPreference = new HashMap<String, String>();
		// Initialize the default value of the DATA_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.DATA_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the DATA_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.DATA_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATA_DEFAULT_FONT property
		defaultAdeleAbstractPreference.put(
				AdeleAbstractDiagramPreferenceConstants.DATA_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the SUBPROGRAM_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAM_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the SUBPROGRAM_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAM_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAM_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAM_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMGROUP_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAMGROUP_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the SUBPROGRAMGROUP_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAMGROUP_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUP_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAMGROUP_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the THREAD_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.THREAD_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the THREAD_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.THREAD_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the THREAD_DEFAULT_FONT property
		defaultAdeleAbstractPreference.put(
				AdeleAbstractDiagramPreferenceConstants.THREAD_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the THREADGROUP_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.THREADGROUP_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the THREADGROUP_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.THREADGROUP_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the THREADGROUP_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.THREADGROUP_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the PROCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.PROCESS_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the PROCESS_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.PROCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PROCESS_DEFAULT_FONT property
		defaultAdeleAbstractPreference.put(
				AdeleAbstractDiagramPreferenceConstants.PROCESS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the PROCESSOR_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.PROCESSOR_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the PROCESSOR_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.PROCESSOR_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PROCESSOR_DEFAULT_FONT property
		defaultAdeleAbstractPreference.put(
				AdeleAbstractDiagramPreferenceConstants.PROCESSOR_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the VIRTUALPROCESSOR_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.VIRTUALPROCESSOR_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the VIRTUALPROCESSOR_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.VIRTUALPROCESSOR_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the VIRTUALPROCESSOR_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.VIRTUALPROCESSOR_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the MEMORY_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.MEMORY_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the MEMORY_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.MEMORY_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the MEMORY_DEFAULT_FONT property
		defaultAdeleAbstractPreference.put(
				AdeleAbstractDiagramPreferenceConstants.MEMORY_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the BUS_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.BUS_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the BUS_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.BUS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BUS_DEFAULT_FONT property
		defaultAdeleAbstractPreference.put(
				AdeleAbstractDiagramPreferenceConstants.BUS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the VIRTUALBUS_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.VIRTUALBUS_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the VIRTUALBUS_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.VIRTUALBUS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the VIRTUALBUS_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.VIRTUALBUS_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the DEVICE_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.DEVICE_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the DEVICE_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.DEVICE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DEVICE_DEFAULT_FONT property
		defaultAdeleAbstractPreference.put(
				AdeleAbstractDiagramPreferenceConstants.DEVICE_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the SYSTEM_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SYSTEM_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the SYSTEM_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SYSTEM_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SYSTEM_DEFAULT_FONT property
		defaultAdeleAbstractPreference.put(
				AdeleAbstractDiagramPreferenceConstants.SYSTEM_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the ABSTRACT_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.ABSTRACT_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the ABSTRACT_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.ABSTRACT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACT_DEFAULT_FONT property
		defaultAdeleAbstractPreference.put(
				AdeleAbstractDiagramPreferenceConstants.ABSTRACT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the EVENTPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.EVENTPORT_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the EVENTPORT_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.EVENTPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORT_DEFAULT_FONT property
		defaultAdeleAbstractPreference.put(
				AdeleAbstractDiagramPreferenceConstants.EVENTPORT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the DATAPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.DATAPORT_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the DATAPORT_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.DATAPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAPORT_DEFAULT_FONT property
		defaultAdeleAbstractPreference.put(
				AdeleAbstractDiagramPreferenceConstants.DATAPORT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the FEATUREGROUP_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the FEATUREGROUP_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the FEATUREGROUP_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the DATAACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.DATAACCESS_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the DATAACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.DATAACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAACCESS_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.DATAACCESS_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the BUSACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.BUSACCESS_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the BUSACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.BUSACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BUSACCESS_DEFAULT_FONT property
		defaultAdeleAbstractPreference.put(
				AdeleAbstractDiagramPreferenceConstants.BUSACCESS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the PARAMETER_DEFAULT_BACKGROUND_COLOR property 
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.PARAMETER_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the PARAMETER_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.PARAMETER_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PARAMETER_DEFAULT_FONT property
		defaultAdeleAbstractPreference.put(
				AdeleAbstractDiagramPreferenceConstants.PARAMETER_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the BUSACCESSCONNECTION_EDGE_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.BUSACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the BUSACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.BUSACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BUSACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.BUSACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the DATAACCESSCONNECTION_EDGE_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.DATAACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the DATAACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.DATAACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.DATAACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the EVENTPORTCONNECTION_EDGE_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.EVENTPORTCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the EVENTPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.EVENTPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORTCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.EVENTPORTCONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the DATAPORTCONNECTION_EDGE_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.DATAPORTCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the DATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.DATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAPORTCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.DATAPORTCONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORTCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.EVENTDATAPORTCONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the PARAMETERCONNECTION_EDGE_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.PARAMETERCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the PARAMETERCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.PARAMETERCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PARAMETERCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.PARAMETERCONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_ROUTER property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_FONT property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultAdeleAbstractPreference
				.put(AdeleAbstractDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		return defaultAdeleAbstractPreference;
	}
}
