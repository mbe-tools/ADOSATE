/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.adeleabstract;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.requests.CreationFactory;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;
import org.topcased.adele.model.ADELE_Relations.ADELE_RelationsPackage;
import org.topcased.modeler.editor.GraphElementCreationFactory;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.palette.ModelerConnectionCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerPaletteManager;

/**
 * Generated Palette Manager
 *
 * @generated
 */
public class AdeleAbstractPaletteManager extends ModelerPaletteManager {
	// declare all the palette categories of the diagram
	/**
	 * @generated
	 */
	private PaletteDrawer componentsDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer connectionsDrawer;

	/**
	 * @generated
	 */
	private ICreationUtils creationUtils;

	/**
	 * The Constructor
	 *
	 * @param utils the creation utils for the tools of the palette 
	 * @generated
	 */
	public AdeleAbstractPaletteManager(ICreationUtils utils) {
		super();
		this.creationUtils = utils;
	}

	/**
	 * Creates the main categories of the palette
	 *
	 * @generated
	 */
	protected void createCategories() {
		createComponentsDrawer();
		createConnectionsDrawer();
	}

	/**
	 * Updates the main categories of the palette
	 *
	 * @generated
	 */
	protected void updateCategories() {
		// deletion of the existing categories and creation of the updated categories

		getRoot().remove(componentsDrawer);
		createComponentsDrawer();

		getRoot().remove(connectionsDrawer);
		createConnectionsDrawer();
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated
	 */
	private void createComponentsDrawer() {
		componentsDrawer = new PaletteDrawer("Components", null);
		List<PaletteEntry> entries = new ArrayList<PaletteEntry>();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getData(), "default");
		entries.add(new ModelerCreationToolEntry("Data", "Data", factory,
				AdeleAbstractImageRegistry.getImageDescriptor("DATA"),
				AdeleAbstractImageRegistry.getImageDescriptor("DATA_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getSubprogram(), "default");
		entries.add(new ModelerCreationToolEntry("Subprogram", "Subprogram",
				factory, AdeleAbstractImageRegistry
						.getImageDescriptor("SUBPROGRAM"),
				AdeleAbstractImageRegistry
						.getImageDescriptor("SUBPROGRAM_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getSubprogramGroup(),
				"default");
		entries.add(new ModelerCreationToolEntry("Subprogram Group",
				"Subprogram Group", factory, AdeleAbstractImageRegistry
						.getImageDescriptor("SUBPROGRAMGROUP"),
				AdeleAbstractImageRegistry
						.getImageDescriptor("SUBPROGRAMGROUP_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getThread(), "default");
		entries.add(new ModelerCreationToolEntry("Thread", "Thread", factory,
				AdeleAbstractImageRegistry.getImageDescriptor("THREAD"),
				AdeleAbstractImageRegistry.getImageDescriptor("THREAD_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getThreadGroup(), "default");
		entries.add(new ModelerCreationToolEntry("Thread Group",
				"Thread Group", factory, AdeleAbstractImageRegistry
						.getImageDescriptor("THREADGROUP"),
				AdeleAbstractImageRegistry
						.getImageDescriptor("THREADGROUP_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getProcess(), "default");
		entries.add(new ModelerCreationToolEntry("Process", "Process", factory,
				AdeleAbstractImageRegistry.getImageDescriptor("PROCESS"),
				AdeleAbstractImageRegistry.getImageDescriptor("PROCESS_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getProcessor(), "default");
		entries.add(new ModelerCreationToolEntry("Processor", "Processor",
				factory, AdeleAbstractImageRegistry
						.getImageDescriptor("PROCESSOR"),
				AdeleAbstractImageRegistry
						.getImageDescriptor("PROCESSOR_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getVirtualProcessor(),
				"default");
		entries.add(new ModelerCreationToolEntry("Virtual Processor",
				"Virtual Processor", factory, AdeleAbstractImageRegistry
						.getImageDescriptor("VIRTUALPROCESSOR"),
				AdeleAbstractImageRegistry
						.getImageDescriptor("VIRTUALPROCESSOR_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getMemory(), "default");
		entries.add(new ModelerCreationToolEntry("Memory", "Memory", factory,
				AdeleAbstractImageRegistry.getImageDescriptor("MEMORY"),
				AdeleAbstractImageRegistry.getImageDescriptor("MEMORY_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getBus(), "default");
		entries.add(new ModelerCreationToolEntry("Bus", "Bus", factory,
				AdeleAbstractImageRegistry.getImageDescriptor("BUS"),
				AdeleAbstractImageRegistry.getImageDescriptor("BUS_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getVirtualBus(), "default");
		entries.add(new ModelerCreationToolEntry("Virtual Bus", "Virtual Bus",
				factory, AdeleAbstractImageRegistry
						.getImageDescriptor("VIRTUALBUS"),
				AdeleAbstractImageRegistry
						.getImageDescriptor("VIRTUALBUS_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getDevice(), "default");
		entries.add(new ModelerCreationToolEntry("Device", "Device", factory,
				AdeleAbstractImageRegistry.getImageDescriptor("DEVICE"),
				AdeleAbstractImageRegistry.getImageDescriptor("DEVICE_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getSystem(), "default");
		entries.add(new ModelerCreationToolEntry("System", "System", factory,
				AdeleAbstractImageRegistry.getImageDescriptor("SYSTEM"),
				AdeleAbstractImageRegistry.getImageDescriptor("SYSTEM_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getAbstract(), "default");
		entries.add(new ModelerCreationToolEntry("Abstract", "Abstract",
				factory, AdeleAbstractImageRegistry
						.getImageDescriptor("ABSTRACT"),
				AdeleAbstractImageRegistry.getImageDescriptor("ABSTRACT_LARGE")));

		componentsDrawer.addAll(entries);
		getRoot().add(componentsDrawer);
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated
	 */
	private void createConnectionsDrawer() {
		connectionsDrawer = new PaletteDrawer("Connections", null);
		List<PaletteEntry> entries = new ArrayList<PaletteEntry>();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getEventPortConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Event Port Connection", "Event Port Connection", factory,
				AdeleAbstractImageRegistry
						.getImageDescriptor("EVENTPORTCONNECTION"),
				AdeleAbstractImageRegistry
						.getImageDescriptor("EVENTPORTCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getDataPortConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Data Port Connection", "Data Port Connection", factory,
				AdeleAbstractImageRegistry
						.getImageDescriptor("DATAPORTCONNECTION"),
				AdeleAbstractImageRegistry
						.getImageDescriptor("DATAPORTCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getEventDataPortConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Event Data Port Connection", "Event Data Port Connection",
				factory, AdeleAbstractImageRegistry
						.getImageDescriptor("EVENTDATAPORTCONNECTION"),
				AdeleAbstractImageRegistry
						.getImageDescriptor("EVENTDATAPORTCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getDataAccessConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Data Access Connection", "Data Access Connection", factory,
				AdeleAbstractImageRegistry
						.getImageDescriptor("DATAACCESSCONNECTION"),
				AdeleAbstractImageRegistry
						.getImageDescriptor("DATAACCESSCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE
						.getSubprogramAccessConnection(), "default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Subprogram Access Connection", "Subprogram Access Connection",
				factory, AdeleAbstractImageRegistry
						.getImageDescriptor("SUBPROGRAMACCESSCONNECTION"),
				AdeleAbstractImageRegistry
						.getImageDescriptor("SUBPROGRAMACCESSCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE
						.getSubprogramGroupAccessConnection(), "default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Subprogram Group Access Connection",
				"Subprogram Group Access Connection",
				factory,
				AdeleAbstractImageRegistry
						.getImageDescriptor("SUBPROGRAMGROUPACCESSCONNECTION"),
				AdeleAbstractImageRegistry
						.getImageDescriptor("SUBPROGRAMGROUPACCESSCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getBusAccessConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Bus Access Connection", "Bus Access Connection", factory,
				AdeleAbstractImageRegistry
						.getImageDescriptor("BUSACCESSCONNECTION"),
				AdeleAbstractImageRegistry
						.getImageDescriptor("BUSACCESSCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getParameterConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Parameter Connection", "Parameter Connection", factory,
				AdeleAbstractImageRegistry
						.getImageDescriptor("PARAMETERCONNECTION"),
				AdeleAbstractImageRegistry
						.getImageDescriptor("PARAMETERCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(
				creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getAbstractFeatureConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Feature Connection", "Feature Connection", factory,
				AdeleAbstractImageRegistry
						.getImageDescriptor("ABSTRACTFEATURECONNECTION"),
				AdeleAbstractImageRegistry
						.getImageDescriptor("ABSTRACTFEATURECONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getFeatureGroupConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Feature Group Connection", "Feature Group Connection",
				factory, AdeleAbstractImageRegistry
						.getImageDescriptor("FEATUREGROUPCONNECTION"),
				AdeleAbstractImageRegistry
						.getImageDescriptor("FEATUREGROUPCONNECTION_LARGE")));

		connectionsDrawer.addAll(entries);
		getRoot().add(connectionsDrawer);
	}

}
