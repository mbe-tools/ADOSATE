/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *  	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.actions;

import org.topcased.modeler.actions.ModelerActionBarContributor;

/**
 * Generated Actions
 *
 * @generated
 */
public class ADELEEditorActionBarContributor extends
		ModelerActionBarContributor {
	// TODO defined customized actions
}