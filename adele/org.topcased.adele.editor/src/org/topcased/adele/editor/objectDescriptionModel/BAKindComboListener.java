package org.topcased.adele.editor.objectDescriptionModel;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Combo;
import org.topcased.adele.common.objectDescriptionModel.OdsView;
import org.topcased.adele.common.utils.ADELEModeler;
import org.topcased.adele.model.ba_components.BAState;
import org.topcased.adele.model.ba_components.BAStateKind;

public class BAKindComboListener implements SelectionListener {

	Combo c1;
	BAState compo;
	ADELEModeler currentEditor;
	OdsView parent;
	
	public BAKindComboListener(Combo _c1, OdsView _parent){
		parent=_parent;
		c1 = _c1;
		currentEditor = _parent.getCurrentEditor();
		compo = (BAState)currentEditor.getSelectedObject();
		c1.setText(compo.getKind().getLiteral());
	}
	
	public void widgetSelected(SelectionEvent e) {
		compo.setKind(BAStateKind.get(c1.getText()));
		currentEditor.dirtify();
		parent.setInput(currentEditor, currentEditor.getSelection());
	}

	public void widgetDefaultSelected(SelectionEvent e) {
	}

}
