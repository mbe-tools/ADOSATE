package org.topcased.adele.editor.objectDescriptionModel;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.swt.events.MouseEvent;
import org.topcased.adele.common.objectDescriptionModel.OdsView;
import org.topcased.adele.model.ADELE_Features.Feature;
import org.topcased.adele.model.ADELE_Features.FeatureGroup;
import org.topcased.adele.model.KernelSpices.SKComponent;

public class ClassifierFeatureMouseListener extends AdeleMouseListener {

	ClassifierFeatureMouseListener (OdsView _parent){
		super( _parent );
	}

	@Override
	public void mouseDown(MouseEvent arg0) {
		final EList<SKComponent> possibleTypes = new BasicEList<SKComponent>();
		final EList<Resource> resources = currentEditor.getResourceSet().getResources();
		Feature obj = (Feature)currentEditor.getSelectedObject();
		for (int i=0; i<resources.size(); i++){
			if (resources.get(i).getURI().lastSegment().endsWith(".adele")){
				final String classSubRule = getClassifierSubstitutionRule( obj.getComponent() );
				possibleTypes.addAll(ADELEOdsView.getValideImplementation(resources.get(i).getContents(),obj, classSubRule ));
				possibleTypes.addAll(ADELEOdsView.getValideTypes(resources.get(i).getContents(),obj, classSubRule ));
			}
		}

		final SKComponent choosedType = ADELEOdsView.openTypeChooseDialog( possibleTypes, obj.getClassifier(), false /*obj.isRefinement()*/ );
		
		if ( obj instanceof FeatureGroup && ( (FeatureGroup) obj ).isType() ) {
			final FeatureGroup featGroup = (FeatureGroup) obj;
			featGroup.setInverseFeature( (FeatureGroup) choosedType );
		}
		else {
			// No refined type selected do set back the classifier of the refined feature.
			if ( choosedType == null && obj.getRefinedFeature() != null ) {
				obj.setClassifier( obj.getRefinedFeature().getClassifier() );
			}
			else {
				obj.setClassifier( choosedType );
			}
		}
		
		parent.setDirty(true);

		currentEditor.dirtify();
		parent.setInput( currentEditor, currentEditor.getSelection() );
		
	}
}
