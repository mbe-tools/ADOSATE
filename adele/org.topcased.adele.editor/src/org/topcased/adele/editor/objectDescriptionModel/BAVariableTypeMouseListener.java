package org.topcased.adele.editor.objectDescriptionModel;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.topcased.adele.common.objectDescriptionModel.OdsView;
import org.topcased.adele.common.utils.ADELEModeler;
import org.topcased.adele.model.ADELE_Components.Data;
import org.topcased.adele.model.ADELE_Components.impl.DataImpl;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.adele.model.ba_features.BAVariable;

public class BAVariableTypeMouseListener implements MouseListener {
	
	OdsView parent;
	ADELEModeler currentEditor;
	
	public BAVariableTypeMouseListener(OdsView _parent){
		super();
		parent=_parent;
		currentEditor = parent.getCurrentEditor();
	}

	public void mouseDoubleClick(MouseEvent arg0) {
	}

	public void mouseDown(MouseEvent arg0) {
		BasicEList<SKComponent> possibleTypes = new BasicEList<SKComponent>();
		EList<Resource> resources = currentEditor.getResourceSet().getResources();
		BAVariable obj = (BAVariable)currentEditor.getSelectedObject();
		for (Resource resource : resources){
			if (resource.getURI().lastSegment().endsWith(".adele"))
				possibleTypes.addAll(getDataTypes(resource.getContents()));
		}

		Data choosedType = (Data)ADELEOdsView.openTypeChooseDialog(possibleTypes,obj.getType(),false);
		if (choosedType!=obj.getType()){
			obj.setType(choosedType);
		}

		currentEditor.dirtify();
		parent.setInput(currentEditor, currentEditor.getSelection());

	}
	
	private EList<Data> getDataTypes(EList<? extends EObject> content) {
		BasicEList<Data> possibleTypes = new BasicEList<Data>();
		for (EObject obj : content) {
			if(obj.getClass()==DataImpl.class  ) {
				possibleTypes.add((Data)obj);
			}
			if (((SKHierarchicalObject)obj).getChildren().size()>0) 
				possibleTypes.addAll(getDataTypes(((SKHierarchicalObject)obj).getChildren()));
		}
		return possibleTypes;
	}

	public void mouseUp(MouseEvent arg0) {
	}
	
}
