package org.topcased.adele.editor.objectDescriptionModel;

import org.eclipse.jface.text.ITextListener;
import org.eclipse.jface.text.TextEvent;
import org.topcased.adele.common.objectDescriptionModel.OdsView;

public class BaGuardKeyAdapter implements ITextListener {

	OdsView parent;
	
	public BaGuardKeyAdapter(OdsView _parent){
		super();
		parent=_parent;
	}

	public void textChanged(TextEvent event) {
		if (event.getDocumentEvent()!=null) {
			parent.setDirty(true);
			parent.setSpecificAttribut("baguardCode", event.getDocumentEvent().getDocument().get());
		}
	}

}
