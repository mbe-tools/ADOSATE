package org.topcased.adele.editor.objectDescriptionModel;

import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.topcased.adele.common.objectDescriptionModel.OdsView;
import org.topcased.adele.common.utils.ADELEModeler;
import org.topcased.adele.model.KernelSpices.SKComponent;

public abstract class AdeleMouseListener implements MouseListener{

	final protected OdsView parent;
	final protected ADELEModeler currentEditor;
	
	protected AdeleMouseListener(	final OdsView p_parent ){
		super();
		
		parent = p_parent;
		currentEditor = parent.getCurrentEditor();
	}

	@Override
	public void mouseDoubleClick(MouseEvent arg0) {
	}

	@Override
	public void mouseUp(MouseEvent arg0) {
	}
	
	protected String getClassifierSubstitutionRule( final SKComponent p_component ) {
		final String rule;

		if ( p_component == null ) {
			rule = parent.getOdsSectionValue( "Classifier_Substitution_Rule" );
		}
		else {
			rule = parent.getOdsSectionValue( "Classifier_Substitution_Rule", p_component );
		}
		
		if ( rule == null || "".equals( rule ) ) {
			return ADELEOdsView.CLASSIFIER_SUBSTITUTION_RULE_DEFAULT;
		}
		
		return rule;
	}
}
