package org.topcased.adele.editor.objectDescriptionModel;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.topcased.adele.common.objectDescriptionModel.OdsView;
import org.topcased.adele.editor.utils.ADELE_Utils;
import org.topcased.adele.model.ADELE_Components.Abstract;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.errors.UncompatibleRefinesReference;
import org.topcased.modeler.edit.GraphNodeEditPart;

public class RefinesImplementationMouseListener extends AdeleMouseListener {
	
	RefinesImplementationMouseListener (OdsView _parent){
		super( _parent );
	}

	@Override
	public void mouseDown(MouseEvent arg0) {
		BasicEList<Component> possibleClassifiers = new BasicEList<Component>();
		EList<Resource> resources = currentEditor.getResourceSet().getResources();
		Component obj = (Component)currentEditor.getSelectedObject();
		final String classSubRule = getClassifierSubstitutionRule( null );
		
		for (int i=0; i<resources.size(); i++){
			if (resources.get(i).getURI().lastSegment().endsWith(".adele")){
				possibleClassifiers.addAll(ADELEOdsView.getValideImplementation(resources.get(i).getContents(),obj, classSubRule ));
				if (obj.getType() == obj.getImplementation())
					possibleClassifiers.addAll(ADELEOdsView.getValideTypes(resources.get(i).getContents(),obj, classSubRule ));
				if (!(obj instanceof Abstract))
					possibleClassifiers.addAll(ADELEOdsView.getValideAbstractImplementation(resources.get(i).getContents(),obj));
			}
		}

		Component result = ADELEOdsView.openRefinesImplementationDialog(possibleClassifiers,obj);

		try {
			if (result!=obj.getRefines()){
				obj.setRefinesValidated(result);
			}

			currentEditor.dirtify();
			parent.setInput(currentEditor, currentEditor.getSelection());
			ADELE_Utils.setToolTip(obj, (GraphNodeEditPart)((IStructuredSelection)currentEditor.getSelection()).getFirstElement());
		}
		catch (UncompatibleRefinesReference e) {
			MessageBox messageBox = new MessageBox(Display.getCurrent().getActiveShell(), SWT.ICON_ERROR | SWT.OK);
			messageBox.setMessage("The current component and the choosed reference are not compatible:\n" +
					e.getMessage());
			messageBox.setText("Conflict in refinement");
			messageBox.open();
		}
	}
}
