package org.topcased.adele.editor.objectDescriptionModel;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.events.MouseEvent;
import org.topcased.adele.common.objectDescriptionModel.OdsView;
import org.topcased.adele.editor.utils.ADELE_Utils;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.modeler.edit.GraphNodeEditPart;

public class ClassifierMouseListener extends AdeleMouseListener {

	public ClassifierMouseListener(OdsView _parent){
		super( _parent );
	}

	public void mouseDown(MouseEvent arg0) {
		BasicEList<Component> possibleClassifiers = new BasicEList<Component>();
		EList<Resource> resources = currentEditor.getResourceSet().getResources();
		Component obj = (Component)currentEditor.getSelectedObject();
		if (obj.getRefines()==null) {
			for (int i=0; i<resources.size(); i++){
				if (resources.get(i).getURI().lastSegment().endsWith(".adele")){
					possibleClassifiers.addAll(ADELEOdsView.getValideImplementation(resources.get(i).getContents(),obj));
					possibleClassifiers.addAll(ADELEOdsView.getValideTypes(resources.get(i).getContents(),obj));
				}
			}
			
			// DB: Add a null to allow setting null classifier.
			possibleClassifiers.add( null );
		}

		List<?> result = ADELEOdsView.openImplementationChooseDialog(possibleClassifiers,obj);

		// DB: Handle the case when nothing was selected in the table.
		if ( !result.isEmpty() ) {
			if ( result.get(0)==null){
				obj.setFeaturesLock(false);
				if (obj.getType()!=null)
					obj.setType(null);
			}
			else {
				obj.setFeaturesLock(true);
				if (obj.getType()!=(Component)result.get(0))
					obj.setType((Component)result.get(0));
			}
			if (result.get(1)==null){
				obj.setSubcomponentsLock(false);
				if (obj.getImplementation()!=null)
					obj.setImplementation(null);
			}
			else {
				obj.setSubcomponentsLock(true);
				if (obj.getImplementation()!=result.get(1))
					obj.setImplementation((Component)result.get(1));
			}
			obj.setImplementationName((String)result.get(2));
			
			currentEditor.dirtify();
			parent.setInput(currentEditor, currentEditor.getSelection());
			ADELE_Utils.setToolTip(obj, (GraphNodeEditPart)((IStructuredSelection)currentEditor.getSelection()).getFirstElement());
		}
	}
}
