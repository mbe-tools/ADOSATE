/**
 * 
 */
package org.topcased.adele.editor.objectDescriptionModel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.FileEditorInput;
import org.topcased.adele.common.objectDescriptionModel.OdsView;
import org.topcased.adele.common.utils.ADELEModeler;
import org.topcased.adele.editor.baaction.EmbeddedBaActionEditor;
import org.topcased.adele.editor.baguard.EmbeddedBaGuardEditor;
import org.topcased.adele.editor.utils.dialogs.ImplementationChooseDialog;
import org.topcased.adele.editor.utils.dialogs.RefinableImplementationChooseDialog;
import org.topcased.adele.editor.utils.dialogs.TypeChooseDialog;
import org.topcased.adele.editor.utils.edit.ADELE_ComponentEditPart;
import org.topcased.adele.model.ADELE.util.ADELEModelUtils;
import org.topcased.adele.model.ADELE_Components.Abstract;
import org.topcased.adele.model.ADELE_Components.Bus;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Components.Data;
import org.topcased.adele.model.ADELE_Components.Package;
import org.topcased.adele.model.ADELE_Components.Subprogram;
import org.topcased.adele.model.ADELE_Components.SubprogramGroup;
import org.topcased.adele.model.ADELE_Features.Feature;
import org.topcased.adele.model.ADELE_Features.FeatureGroup;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.adele.model.KernelSpices.SKFeature;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.adele.model.ObjectDescriptionModel.SKODData;
import org.topcased.adele.model.ObjectDescriptionModel.SKODSection;
import org.topcased.adele.model.ObjectDescriptionModel.SKODSystem;
import org.topcased.adele.model.ba_components.BAStateKind;
import org.topcased.adele.model.ba_features.BAVariable;
import org.topcased.adele.model.ba_relations.BATransition;
import org.topcased.adele.xtext.baaction.ui.internal.BaActionActivator;
import org.topcased.adele.xtext.baguards.ui.internal.BaGuardsActivator;

import com.google.inject.Injector;

/**
 * @author Arnaud
 *
 */
public class ADELEOdsView {
	
	public static final String CLASSIFIER_SUBSTITUTION_RULE_DEFAULT = "Type_Extension";

	private static String baguardsFileExtension = "baguards";
	private static String baactionFileExtension = "baaction";

	private static Injector guardInjector;
	private static Injector actionInjector;
	
	public static void initOdsViewEditors() {
		guardInjector = BaGuardsActivator.getInstance().getInjector("org.topcased.adele.xtext.baguards.BaGuards");
		actionInjector = BaActionActivator.getInstance().getInjector("org.topcased.adele.xtext.baaction.BaAction");
	}
	
	public static void setOdsViewAADLCode(SKODSection section, OdsView ods){
		eraseOdsView(ods);

		Text txt = new Text(ods.getOdsFormContainer(), SWT.BORDER | SWT.MULTI);
		txt.setData("id", section.getId());
		txt.setLayoutData(new GridData(GridData.FILL_BOTH));

		File aadlCodeFile =  ((FileEditorInput)ods.getCurrentEditor().getEditorInput()).getPath().makeAbsolute().removeFileExtension().addFileExtension("aadl").toFile();
		
		if (aadlCodeFile.exists()){
			try {
				FileReader fileReader = new FileReader(aadlCodeFile);
				BufferedReader reader   = new BufferedReader(fileReader);
				String line = reader.readLine();
				while (line!=null){
					txt.append(line+"\n");
					line=reader.readLine();
				}
				fileReader.close();
				txt.setEditable(false);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void setOdsViewGuard(SKODSection section, OdsView ods){
		eraseOdsView(ods);
		
		ADELEModeler currentEditor = ods.getCurrentEditor();
		Composite odsFormContainer = ods.getOdsFormContainer();
		
		SKHierarchicalObject selectedObject=currentEditor.getSelectedObject();
		
		if (selectedObject instanceof BATransition) {
			
			new Label(odsFormContainer, SWT.NONE).setText(section.getLabel());

			EmbeddedBaGuardEditor editor =new EmbeddedBaGuardEditor(odsFormContainer, guardInjector, SWT.BORDER);

			SKODData data = section.getData();
			
			SKODSystem odsystem = currentEditor.getODSystem(ods.getTabFolder().getSelectionIndex());
			String baguardsFilePath = odsystem.getDomFilename(data.getDom());
			baguardsFilePath = baguardsFilePath.substring(0, baguardsFilePath.lastIndexOf(".")) +"."+ baguardsFileExtension;
			File baguardsFile =  new File(baguardsFilePath);
			(new File(baguardsFile.getParent())).mkdirs();
			
			try {
				baguardsFile.createNewFile();
				String txt = "";
				FileReader fileReader = new FileReader(baguardsFile);
				BufferedReader reader   = new BufferedReader(fileReader);
				String line = reader.readLine();
				while (line!=null){
					txt+= line+"\n";
					line=reader.readLine();
				}
				fileReader.close();

				editor.update(txt);
				
				ods.setSpecificAttribut("baguardsFile", baguardsFile);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
//			txt.addKeyListener(new BehaviorKeyAdapter(ods));
			editor.getViewer().addTextListener(new BaGuardKeyAdapter(ods));
		}
		
	}
	
	public static void setOdsViewAction(SKODSection section, OdsView ods){
		eraseOdsView(ods);
		
		ADELEModeler currentEditor = ods.getCurrentEditor();
		Composite odsFormContainer = ods.getOdsFormContainer();
		
		SKHierarchicalObject selectedObject=currentEditor.getSelectedObject();
		
		if (selectedObject instanceof BATransition) {
			
			new Label(odsFormContainer, SWT.NONE).setText(section.getLabel());

			EmbeddedBaActionEditor editor =new EmbeddedBaActionEditor(odsFormContainer, actionInjector, SWT.BORDER);

			SKODData data = section.getData();
			
			SKODSystem odsystem = currentEditor.getODSystem(ods.getTabFolder().getSelectionIndex());
			String baactionFilePath = odsystem.getDomFilename(data.getDom());
			baactionFilePath = baactionFilePath.substring(0, baactionFilePath.lastIndexOf(".")) +"."+ baactionFileExtension;
			File baactionFile =  new File(baactionFilePath);
			(new File(baactionFile.getParent())).mkdirs();
			
			try {
				baactionFile.createNewFile();
				String txt = "";
				FileReader fileReader = new FileReader(baactionFile);
				BufferedReader reader   = new BufferedReader(fileReader);
				String line = reader.readLine();
				while (line!=null){
					txt+= line+"\n";
					line=reader.readLine();
				}
				fileReader.close();

				editor.update(txt);
				
				ods.setSpecificAttribut("baactionFile", baactionFile);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
//			txt.addKeyListener(new BehaviorKeyAdapter(ods));
			editor.getViewer().addTextListener(new BaActionKeyAdapter(ods));
			
		}
		
	}

	public static void setOdsViewClassifier (OdsView ods){
		eraseOdsView(ods);
		
		ADELEModeler currentEditor = ods.getCurrentEditor();
		Composite odsFormContainer = ods.getOdsFormContainer();
		
		SKHierarchicalObject selectedObject=currentEditor.getSelectedObject();

		if (selectedObject instanceof Component) {
			Component obj = (Component)selectedObject;
		
			boolean isPackage = currentEditor.getEditorKind(obj).equalsIgnoreCase("package");
			
			Button bouton = new Button(odsFormContainer, SWT.NONE);
			bouton.setText("Choose a classifier");
			
			if ( !obj.isSubcomponentRefinement() &&
					((!isPackage && obj.getParent()==null) ||
					(!isPackage && obj.getParent()!=null &&
					!((Component)obj.getParent()).isSubcomponentsLock()) ||
					(isPackage && !(obj instanceof Package) &&
							obj.getParent()!=null && 
							!(obj.getParent() instanceof Package) &&
							obj.getParent().getParent()!=null &&
							!(obj.getParent().getParent() instanceof Package)
					 ))
					) {
				if (!(obj instanceof Package) &&
						/*obj.getParent()!=null &&*/ obj.getParent() instanceof Package)
					bouton.addMouseListener(new ClassifierTypeMouseListener(ods));
				else
					bouton.addMouseListener(new ClassifierMouseListener(ods));
			}
			else
				bouton.setEnabled(false);

			Text texte = new Text(odsFormContainer, SWT.NONE);

			if (obj.isSubcomponentRefinement() || !obj.isSubcomponentsLock()){
				if (obj.getType()!=null)
					texte.setText(ADELEModelUtils.getFullNamespace(obj.getType())+obj.getType().getName());
				else if (!(obj.getImplementationName().equalsIgnoreCase("")))
					texte.setText(ADELEModelUtils.getFullNamespace(obj)+obj.getName());
			}
			if (obj.isSubcomponentRefinement() || obj.isSubcomponentsLock()){
				if (obj.getImplementation()!=null){
					if (!(obj.getImplementationName().equalsIgnoreCase("")))
						texte.setText(ADELEModelUtils.getFullNamespace(obj.getImplementation())+obj.getImplementation().getName()+
								"."+obj.getImplementationName());
					else
						texte.setText(ADELEModelUtils.getFullNamespace(obj.getImplementation())+obj.getImplementation().getName());
				}
				else if (!(obj.getImplementationName().equalsIgnoreCase(""))){
					if (obj.getType()!=null)
						texte.setText(ADELEModelUtils.getFullNamespace(obj.getType())+obj.getType().getName()+
								"."+obj.getImplementationName());
					else
						texte.setText(ADELEModelUtils.getFullNamespace(obj)+obj.getName()+"."+obj.getImplementationName());
				}
			}

			texte.setEditable(false);
			texte.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		}
		else if ( selectedObject instanceof Feature ) {
			final Feature feat = (Feature)selectedObject;
			final String buttonLabel;
			final SKComponent obj;
			final SKComponent classifier;
			
			if ( feat instanceof FeatureGroup ) {
				final FeatureGroup featGroup = (FeatureGroup) feat;
				
				if ( featGroup.isType() ) {
					obj = (SKComponent) feat;
					classifier = featGroup.getInverseFeature();
					buttonLabel = "Choose Inverse Feature Group";
				}
				else {
					obj = feat.getComponent();
					classifier = feat.getClassifier();
					buttonLabel = "Choose Classifier";
				}
			}
			else {
				obj = feat.getComponent();
				classifier = feat.getClassifier();
				buttonLabel = "Choose Classifier";
			}
			
			Button bouton = new Button(odsFormContainer, SWT.NONE);
			bouton.setText( buttonLabel );
			Text texte = new Text(odsFormContainer, SWT.NONE);
			
			boolean isPackage = currentEditor.getEditorKind(feat).equalsIgnoreCase("package");

			// DB: Don't allow setting the classifier of features of subcomponents or implementations in package mode
			if (/*!feat.isRefinement() || */
					(isPackage && /*obj.getParent()!=null && */
							obj.getParent() instanceof Package) ) {
				bouton.addMouseListener(new ClassifierFeatureMouseListener(ods));
				if (classifier!=null)
					texte.setText(ADELEModelUtils.getFullNamespace(classifier)+classifier.getName());
				
				bouton.setEnabled( !feat.isRefinement() );
			}
			else if ( feat.isRefinement() ) {
				bouton.setEnabled(false);
				Component tmp = ((Component) feat.getComponent()).getRefines();
				if (tmp.getType()!=null)
					tmp = (Component)((Feature) tmp.getType().getNamedFeature(feat.getName())).getClassifier();
				else 
					tmp = (Component)((Feature) tmp.getNamedFeature(feat.getName())).getClassifier();
				if (tmp!=null)
					texte.setText(ADELEModelUtils.getFullNamespace(tmp)+tmp.getName());
			}
			else {
				bouton.setEnabled( false );
				
				if ( classifier != null ) {
					texte.setText( ADELEModelUtils.getFullNamespace( classifier ) + classifier.getName() );
				}
			}

			texte.setEditable(false);
			texte.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		}
	}

	public static void setOdsViewBAType (OdsView ods){
		eraseOdsView(ods);
		
		ADELEModeler currentEditor = ods.getCurrentEditor();
		Composite odsFormContainer = ods.getOdsFormContainer();
		
		BAVariable feat= (BAVariable)currentEditor.getSelectedObject();
			
		Button bouton = new Button(odsFormContainer, SWT.NONE);
		bouton.setText("Choose a type");

		Text texte = new Text(odsFormContainer, SWT.NONE);
		SKComponent classifier = feat.getType();
		
		bouton.addMouseListener(new BAVariableTypeMouseListener(ods));
		if (classifier!=null)
			texte.setText(ADELEModelUtils.getFullNamespace(classifier)+classifier.getName());

		texte.setEditable(false);
		texte.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

	}

	public static void setOdsViewBAKind (OdsView ods){
		eraseOdsView(ods);
		
		Composite odsFormContainer = ods.getOdsFormContainer();
			
		new Label(odsFormContainer, SWT.NONE).setText("State kind");
		
		Combo c1 = new Combo(odsFormContainer, SWT.READ_ONLY);
		for (BAStateKind kind : BAStateKind.values()) {
			c1.add(kind.getLiteral());
		}
		c1.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		c1.addSelectionListener(new BAKindComboListener(c1, ods ));

	}	

	public static void setOdsViewRefines (OdsView ods){
		eraseOdsView(ods);
		
		ADELEModeler currentEditor = ods.getCurrentEditor();
		Composite odsFormContainer = ods.getOdsFormContainer();
		
		SKHierarchicalObject selectedObject=currentEditor.getSelectedObject();
		
		Button bouton = new Button(odsFormContainer, SWT.NONE);
		bouton.setText("Choose a classifier to refine to");
		Text texte = new Text(odsFormContainer, SWT.NONE);

		if (selectedObject instanceof Component) {
			Component obj = (Component)selectedObject;
			
			boolean isPackage = currentEditor.getEditorKind(obj).equalsIgnoreCase("package");
			
			if (isPackage || !((Component)obj.getParent()).isSubcomponentsLock() ||
					(obj.getImplementation()==null &&
							!obj.getImplementationName().equalsIgnoreCase("")) ) {
				bouton.addMouseListener(new RefinesMouseListener(ods));
			}
			else
				bouton.setEnabled(false);
			
			if (obj.getRefines()!=null)
				texte.setText(ADELEModelUtils.getFullNamespace(obj.getRefines())+obj.getRefines().getName());
			
		} else if (selectedObject instanceof Feature) {
			Feature feat = (Feature)selectedObject;
			Component obj = (Component) feat.getComponent();
			
			boolean isPackage = currentEditor.getEditorKind(obj).equalsIgnoreCase("package");
			SKComponent classifier = feat.getClassifier();
			
			if (feat.isRefinement() ||
					(isPackage && obj.getParent()!=null && 
							obj.getParent() instanceof Package) ) {
				bouton.addMouseListener(new ClassifierFeatureMouseListener(ods));
			}
			else {
				bouton.setEnabled(false);
			}

			if (classifier!=null)
				texte.setText(ADELEModelUtils.getFullNamespace(classifier)+classifier.getName());
		}

		texte.setEditable(false);
		texte.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
	}

	public static void setOdsViewExtends (OdsView ods){
		eraseOdsView(ods);
		
		ADELEModeler currentEditor = ods.getCurrentEditor();
		Composite odsFormContainer = ods.getOdsFormContainer();
		
		SKHierarchicalObject selectedObject = currentEditor.getSelectedObject();

		// DB: Sometimes the view is not up to date with the selection and this is called for other elements
		// than component. So test the instance to avoid class cast exception.
		if ( selectedObject instanceof SKComponent ) {
			final SKComponent obj = (SKComponent) selectedObject;
			
			boolean isPackage = currentEditor.getEditorKind(obj).equalsIgnoreCase("package");
			
			Button bouton = new Button(odsFormContainer, SWT.NONE);
			final String textLabel;
			bouton.setText( "Choose Classifier to Extend" );
			
			if ( obj instanceof Component ) {
				final Component compo = (Component) obj;

				if (	isPackage || !( (Component) obj.getParent() ).isSubcomponentsLock() ||
						( compo.getImplementation() == null && !compo.getImplementationName().equalsIgnoreCase( "" ) ) ) {
					if ( !( obj instanceof Package ) && obj.getParent() instanceof Package ) {
						bouton.addMouseListener(new ExtendsTypeMouseListener(ods));
					}
					else {
						bouton.addMouseListener(new ExtendsImplementationMouseListener(ods));
					}
				}
				else {
					bouton.setEnabled(false);
				}
				
				if ( compo.getRefines() == null ) {
					textLabel = "";
				}
				else {
					textLabel = ADELEModelUtils.getFullNamespace( compo.getRefines() ) + compo.getRefines().getName();
				}
			}
			else if ( obj instanceof FeatureGroup ) {
				bouton.addMouseListener(new ExtendsTypeMouseListener(ods));
				
				final FeatureGroup featGroup = (FeatureGroup) obj;
				final Feature extendedFeat = featGroup.getRefinedFeature(); 
				
				if ( extendedFeat == null ) {
					textLabel = "";
				}
				else {
					textLabel = ADELEModelUtils.getFullNamespace( extendedFeat ) + extendedFeat.getName();
				}
			}
			else {
				bouton.setEnabled( false );
				textLabel = "";
			}
	
			Text texte = new Text(odsFormContainer, SWT.NONE);
			texte.setText( textLabel );
			texte.setEditable(false);
			texte.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		}
	}
	
	protected static void eraseOdsView (OdsView ods){
		if (ods.getSpecificAttribut("baguardsFile")!=null && ods.getSpecificAttribut("baguardCode")!=null)
			saveBaGuard(ods);
		if (ods.getSpecificAttribut("baactionFile")!=null && ods.getSpecificAttribut("baactionCode")!=null)
			saveBaAction(ods);
	}
	
//	protected void addAADLBooleanField (SKODData data, SKODSection section){
//		new Label(odsFormContainer, SWT.NONE).setText(section.getLabel());
//		Combo combo = new Combo(odsFormContainer,SWT.READ_ONLY );
//		String[] items = {"true","false"};
//		combo.setItems(items);
//		combo.select(0);
//	}
//	
//	protected void addAADLStringField (SKODData data, SKODSection section){
//		new Label(odsFormContainer, SWT.NONE).setText(section.getLabel());
//		Text txt = new Text(odsFormContainer, SWT.BORDER);
//		txt.setData("id", section.getId());
//		txt.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
//		if (data != null)
//			txt.setText(data.getValue());
//		/*SKProperty prop = currentSelectedHierarchicalObject.getProperty(section.getId());
//		if ( prop != null){
//			txt.setText(prop.getValue().toString());
//		}*/
//
//		txt.addKeyListener(new InnerKeyAdapter(this));
//	}
//	
//	protected void addEnumerationField (SKODData data, SKODSection section, EList<?>items){
//		new Label(odsFormContainer, SWT.NONE).setText(section.getLabel());
//		Combo combo = new Combo(odsFormContainer,SWT.READ_ONLY );
//		if (items != null) {
//			String[] a = {};
//			combo.setItems(items.toArray(a));
//			combo.select(0);
//		} 
//	}
	
	/*protected void addNumberField (SKODData data, SKODSection section, NumberType type){
				
		NumericType numType = type.getType();
		PropertyType units = type.getUnits();
		EList<Unit> unitsList = type.getUnitsList();
		
		if (units != null | unitsList != null) {
			odsFormContainer.setLayout(new GridLayout(3,false));
		}
		
		new Label(odsFormContainer, SWT.NONE).setText(section.getLabel());
		Text txt = new Text(odsFormContainer, SWT.BORDER);
		txt.setData("id", section.getId());

		ArrayList<String> unitsItem = new ArrayList<String>();
		if (units != null) {
			Iterator<Unit> it = units.getUnitsType().getUnit().iterator();
			while (it.hasNext()) {
				unitsItem.add(it.next().getName());
			}
		}
		else if (unitsList != null) {
			//
		}
		
		if (units != null | unitsList != null) {
			Combo combo = new Combo(odsFormContainer,SWT.READ_ONLY );
			String[] a = {};
			combo.setItems(unitsItem.toArray(a));
			combo.select(0);
		} 
		txt.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		if (data != null)
			txt.setText(data.getValue());

		txt.addKeyListener(new InnerKeyAdapter(this));
	}*/
	
//	protected void addClassifierField (SKODData data, SKODSection section, PropertyTypeImpl type){
//		
//	}
//	
//	protected void addReferenceField (SKODData data, SKODSection section, PropertyTypeImpl type){
//		
//	}
//	
//	protected void addRecordField (SKODData data, SKODSection section, PropertyTypeImpl type){
//		
//	}
	
	public static void saveBaGuard(OdsView ods) {				
		Writer output;
		try {
			output = new BufferedWriter(new FileWriter((File)ods.getSpecificAttribut("baguardsFile")));
		    output.write((String)ods.getSpecificAttribut("baguardCode"));
		    output.close();
		    ods.setSpecificAttribut("baguardsFile",null);
		    ods.setSpecificAttribut("baguardCode",null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void saveBaAction(OdsView ods) {				
		Writer output;
		try {
			output = new BufferedWriter(new FileWriter((File)ods.getSpecificAttribut("baactionFile")));
		    output.write((String)ods.getSpecificAttribut("baactionCode"));
		    output.close();
		    ods.setSpecificAttribut("baactionFile",null);
		    ods.setSpecificAttribut("baactionCode",null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static EList<Component> getValideTypes(EList<EObject> contents, Component current){
		return getValideTypes(contents, current, CLASSIFIER_SUBSTITUTION_RULE_DEFAULT );
	}
		
	public static EList<Component> getValideTypes(EList<? extends EObject> contents, Component current,String Classifier_Substitution_Rule){
		BasicEList<Component> possibleTypes = new BasicEList<Component>();
		
		for (int i=0; i<contents.size(); i++){
			EObject tmp=contents.get(i);
			if(tmp!=current &&
					tmp.getClass()==current.getClass() &&
					((Component)tmp).isType() &&
					((Component)tmp).getParent()!=null &&
					noCircularTypeDependency(current,(Component)tmp) ) {
				if (!(current.isSubcomponentRefinement() && !isValidTypeRefinement(current,(Component)tmp,Classifier_Substitution_Rule)))
					possibleTypes.add((Component)tmp);
			}
			if(((SKHierarchicalObject)tmp).getChildren().size()>0)
				possibleTypes.addAll(getValideTypes(((SKHierarchicalObject)tmp).getChildren(), current,Classifier_Substitution_Rule));
		}
		
		return possibleTypes;
	}
	
	public static EList<SKComponent> getValideTypes(EList<EObject> contents, Feature current){
		return getValideTypes(contents, current, CLASSIFIER_SUBSTITUTION_RULE_DEFAULT );
	}
		
	public static EList<SKComponent> getValideTypes(EList<? extends EObject> contents, Feature current,String Classifier_Substitution_Rule){
		BasicEList<SKComponent> possibleTypes = new BasicEList<SKComponent>();
		EObject tmp;
		for (int i=0; i<contents.size(); i++){
			tmp = contents.get(i);
			if( current.getClass().getName().contains("Abstract") &&
					((Component)tmp).isType() &&
					!((Component)tmp).isImplementation() &&
					((Component)tmp).getParent()!=null &&
					!((Component)tmp).getParent().getName().contains(".")){
				if (!(current.isRefinement() && !isValidTypeRefinement(current,(Component)tmp,Classifier_Substitution_Rule)))
					possibleTypes.add((Component)tmp);
			}
			else if( (current.getClass().getName().contains("Data") ||
					current.getClass().getName().contains("Parameter")) &&
					tmp instanceof Data &&
					((Component)tmp).isType() &&
					!((Component)tmp).isImplementation() &&
					((Component)tmp).getParent()!=null &&
					!((Component)tmp).getParent().getName().contains(".")){
				if (!(current.isRefinement() && !isValidTypeRefinement(current,(Component)tmp,Classifier_Substitution_Rule)))
					possibleTypes.add((Component)tmp);
			}
			else if(current.getClass().getName().contains("Bus") &&
					tmp instanceof Bus &&
					((Component)tmp).isType() &&
					!((Component)tmp).isImplementation() &&
					((Component)tmp).getParent()!=null &&
					!((Component)tmp).getParent().getName().contains(".")){
				if (!(current.isRefinement() && !isValidTypeRefinement(current,(Component)tmp,Classifier_Substitution_Rule)))
					possibleTypes.add((Component)tmp);
			}
			else if(current.getClass().getName().contains("SubprogramAccess") &&
					tmp instanceof Subprogram &&
					((Component)tmp).isType() &&
					!((Component)tmp).isImplementation() &&
					((Component)tmp).getParent()!=null &&
					!((Component)tmp).getParent().getName().contains(".")){
				if (!(current.isRefinement() && !isValidTypeRefinement(current,(Component)tmp,Classifier_Substitution_Rule)))
					possibleTypes.add((Component)tmp);
			}
			else if(current.getClass().getName().contains("SubprogramGroupAccess") &&
					tmp instanceof SubprogramGroup &&
					((Component)tmp).isType() &&
					!((Component)tmp).isImplementation() &&
					((Component)tmp).getParent()!=null &&
					!((Component)tmp).getParent().getName().contains(".")){
				if (!(current.isRefinement() && !isValidTypeRefinement(current,(Component)tmp,Classifier_Substitution_Rule)))
					possibleTypes.add((Component)tmp);
			}
			// DB: Handle feature group classifier
			else if ( current instanceof FeatureGroup ) {
				if ( tmp instanceof FeatureGroup ) {
					final FeatureGroup currentFeatGroupFeat = (FeatureGroup) current;
					final FeatureGroup newFeatGroupType = (FeatureGroup) tmp;
					
					if ( newFeatGroupType.isType() ) {
						if ( currentFeatGroupFeat.isType() ) {
							if ( !dependsOn( newFeatGroupType, currentFeatGroupFeat ) ) {
								possibleTypes.add( newFeatGroupType );
							}
						}
						else {
							if ( currentFeatGroupFeat.getClassifier() != newFeatGroupType ) {
								if ( currentFeatGroupFeat.getOriginalFeature() == null ) {
									possibleTypes.add( newFeatGroupType );
								}
								else if ( isValidTypeRefinement( currentFeatGroupFeat, newFeatGroupType, Classifier_Substitution_Rule ) ) {
									possibleTypes.add( newFeatGroupType );
								}
							}
						}
					}
				}
			}
			if(((SKHierarchicalObject)tmp).getChildren().size()>0)
				possibleTypes.addAll(getValideTypes(((SKHierarchicalObject)tmp).getChildren(), current,Classifier_Substitution_Rule));
		}
		
		return possibleTypes;
	}
	
	public static boolean isValidTypeRefinement(Component current, Component type) {
		return isValidTypeRefinement(current, type, CLASSIFIER_SUBSTITUTION_RULE_DEFAULT );
	}
	
	public static boolean isValidTypeRefinement( 	final FeatureGroup p_current,
													final FeatureGroup p_newType,
													final String p_classifierSubstitutionRule ) {
		final Component featureCompo = (Component) p_current.getComponent();
		Component refinedComponent = featureCompo.getRefines();
		final Feature baseFeature = (Feature) ADELEModelUtils.findElementByName( p_current,  refinedComponent.getFeatures() );
		
		if ( baseFeature == null ) {
			return false;
		}
		
		final SKComponent baseType = baseFeature.getClassifier();
		Feature testedType = p_newType;

		if ( p_classifierSubstitutionRule.trim().equalsIgnoreCase( "Type_Extension" ) ) {
			if ( baseType == null ) {
				return true;
			}
			
			while ( testedType.getRefinedFeature() != null ) {
				testedType = testedType.getRefinedFeature();
				
				if ( baseType.equals( testedType ) ) {
					return true;
				}
			}
		}
		
		return false;
	}

	public static boolean isValidTypeRefinement(Component current, Component type, String Classifier_Substitution_Rule) {
		if (((Component)current.getParent()).getRefines()==null)
			return false;
		Iterator<SKHierarchicalObject> childrenIt = ((Component)current.getParent()).getRefines().getChildren().iterator();
		Component testedType=null;
		while (childrenIt.hasNext()){
			SKHierarchicalObject tmp=childrenIt.next();
			if (tmp.getName().equalsIgnoreCase(current.getName())){
				testedType = ((Component)tmp).getType();
				break;
			}
		}
		if (testedType==null || testedType.equals(type))
			return true;
		if (Classifier_Substitution_Rule.trim().equalsIgnoreCase("Type_Extension")) {
			while (type.getRefines()!=null){
				type = type.getRefines();
				if (testedType.equals(type))
					return true;
			}
		}
		return false;
	}
	
	public static boolean isValidTypeRefinement(Feature current, Component type) {
		return isValidTypeRefinement(current, type, CLASSIFIER_SUBSTITUTION_RULE_DEFAULT );
	}
		
	public static boolean isValidTypeRefinement(Feature current, Component type,String Classifier_Substitution_Rule) {
		Component featureCompo=(Component)current.getComponent();
		Component refinedComponent;
		Component testedType=type;
		if (featureCompo.isSubcomponentRefinement()) {
			refinedComponent=((Component)featureCompo.getParent()).getRefines();
			Iterator<SKHierarchicalObject> childrenIt = refinedComponent.getChildren().iterator();
			while (childrenIt.hasNext()){
				Component tmp=(Component)childrenIt.next();
				if (tmp.getName().equalsIgnoreCase(featureCompo.getName())){
					refinedComponent = tmp;
					break;
				}
			}
		}
		else 
			refinedComponent=featureCompo.getRefines();
		
		Iterator<SKFeature> featureIt;
		if (refinedComponent.getType()!=null)
			featureIt = refinedComponent.getType().getFeatures().iterator();
		else
			featureIt = refinedComponent.getFeatures().iterator();
		Component baseType=null;
		while (featureIt.hasNext()){
			SKFeature tmp=featureIt.next();
			if (tmp.getName().equalsIgnoreCase(current.getName())){
				final SKComponent classifier = ((Feature)tmp).getClassifier();
				
				if ( classifier instanceof Component ) {
					baseType = (Component) classifier;
					break;
				}
			}
		}
		if (baseType==null)
			return true;
		
		if (baseType.equals(testedType))
			//return true;
			return false;
		
		if (Classifier_Substitution_Rule.trim().equalsIgnoreCase("Type_Extension")) {
			while (testedType.getRefines()!=null){
				testedType = testedType.getRefines();
				if (baseType.equals(testedType))
					return true;
			}
		}
		return false;
	}
	
	public static EList<Component> getValideImplementation(EList<EObject> contents, Component current){
		return getValideImplementation(contents,current, CLASSIFIER_SUBSTITUTION_RULE_DEFAULT );
	}
		
	public static EList<Component> getValideImplementation(EList<? extends EObject> contents, Component current,String Classifier_Substitution_Rule){
		BasicEList<Component> possibleImplementations = new BasicEList<Component>();
		
		for (int i=0; i<contents.size(); i++){
			EObject tmp=contents.get(i);
			if(tmp!=current &&
					tmp.getClass()==current.getClass()){
					if ( ((Component)tmp).isImplementation() &&
						((Component)tmp).getParent()!=null &&
						!((Component)tmp).getParent().getName().contains(".")&&
						noCircularImplementationDependency(current,(Component)tmp) ){
						if (current.isSubcomponentRefinement() && isValidImplementationRefinement(current,(Component)tmp,Classifier_Substitution_Rule))
							possibleImplementations.add((Component)tmp);
						if (!current.isSubcomponentRefinement() && isValidImplementationExtends(current,(Component)tmp))
							possibleImplementations.add((Component)tmp);
				}
			}
			if(((SKHierarchicalObject)contents.get(i)).getChildren().size()>0)
				possibleImplementations.addAll(getValideImplementation(((SKHierarchicalObject)contents.get(i)).getChildren(), current,Classifier_Substitution_Rule));
		}
		
		return possibleImplementations;
	}
	
	public static EList<Component> getValideImplementation(EList<EObject> contents, Feature current){
		return getValideImplementation(contents, current, CLASSIFIER_SUBSTITUTION_RULE_DEFAULT );
	}
	
	public static EList<Component> getValideImplementation(EList<? extends EObject> contents, Feature current,String Classifier_Substitution_Rule){
		BasicEList<Component> possibleImplementations = new BasicEList<Component>();
		EObject tmp;
		for (int i=0; i<contents.size(); i++){
			tmp = contents.get(i);
			if( current.getClass().getName().contains("Abstract") &&
					((Component)tmp).isImplementation() &&
					((Component)tmp).getParent()!=null &&
					!((Component)tmp).getParent().getName().contains(".")){
				if (!(current.isRefinement() && !isValidImplementationRefinement(current,(Component)tmp,Classifier_Substitution_Rule)))
					possibleImplementations.add((Component)tmp);
			}
			else if( (current.getClass().getName().contains("Data") ||
					current.getClass().getName().contains("Parameter")) &&
					tmp instanceof Data &&
					((Component)tmp).isImplementation() &&
					((Component)tmp).getParent()!=null &&
					!((Component)tmp).getParent().getName().contains(".")){
				if (!(current.isRefinement() && !isValidImplementationRefinement(current,(Component)tmp,Classifier_Substitution_Rule)))
					possibleImplementations.add((Component)tmp);
			}
			else if(current.getClass().getName().contains("Bus") &&
					tmp instanceof Bus &&
					((Component)tmp).isImplementation() &&
					((Component)tmp).getParent()!=null &&
					!((Component)tmp).getParent().getName().contains(".")){
				if (!(current.isRefinement() && !isValidImplementationRefinement(current,(Component)tmp,Classifier_Substitution_Rule)))
					possibleImplementations.add((Component)tmp);
			}
			else if(current.getClass().getName().contains("SubprogramAccess") &&
					tmp instanceof Subprogram &&
					((Component)tmp).isImplementation() &&
					((Component)tmp).getParent()!=null &&
					!((Component)tmp).getParent().getName().contains(".")){
				if (!(current.isRefinement() && !isValidImplementationRefinement(current,(Component)tmp,Classifier_Substitution_Rule)))
					possibleImplementations.add((Component)tmp);
			}
			else if(current.getClass().getName().contains("SubprogramGroupAccess") &&
					tmp instanceof SubprogramGroup &&
					((Component)tmp).isImplementation() &&
					((Component)tmp).getParent()!=null &&
					!((Component)tmp).getParent().getName().contains(".")){
				if (!(current.isRefinement() && !isValidImplementationRefinement(current,(Component)tmp,Classifier_Substitution_Rule)))
					possibleImplementations.add((Component)tmp);
			}
			if(((SKHierarchicalObject)tmp).getChildren().size()>0)
				possibleImplementations.addAll(getValideImplementation(((SKHierarchicalObject)tmp).getChildren(), current,Classifier_Substitution_Rule));
		}
		
		return possibleImplementations;
	}
	
	public static boolean isValidImplementationExtends(Component current, Component implementation) {
		Component currentType = current.getType();
		Component testedType = implementation.getType();
		
		if ( currentType == testedType || currentType == null ) {
			return true;
		}
		
		while ( currentType.getRefines()!=null) {
			currentType=currentType.getRefines();
			if ( currentType == testedType) {
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean isValidImplementationRefinement(Component current, Component implementation) {
		return isValidImplementationRefinement(current, implementation, CLASSIFIER_SUBSTITUTION_RULE_DEFAULT );
	}
		
	public static boolean isValidImplementationRefinement(Component current, Component implementation, String Classifier_Substitution_Rule) {
		Iterator<SKHierarchicalObject> childrenIt;
		
		if ( !Classifier_Substitution_Rule.trim().equalsIgnoreCase("Type_extension") ) {
			if (current.getType() == implementation.getType() )
				return true;
			else
				return false;
		}
		
		if (current.isImplementation() && ((Component)current.getParent()).getRefines()!=null )
			childrenIt = ((Component)current.getParent()).getRefines().getChildren().iterator();
		else {
			Component classifier = current.getClassifier();
			if (classifier.isInstance() || classifier.getRefines()==null)
				childrenIt = new BasicEList<SKHierarchicalObject>().iterator();
			else 
				childrenIt = classifier.getRefines().getChildren().iterator();
		}
		Component refinedImplementation=null;
		Component refinedType=null;
		Component testedImplementation=implementation;
		while (childrenIt.hasNext()){
			SKHierarchicalObject tmp=childrenIt.next();
			if (tmp.getName().equalsIgnoreCase(current.getName())){
				refinedImplementation = ((Component)tmp).getImplementation();
				if (refinedImplementation==null)
					refinedType = ((Component)tmp).getType();
				break;
			}
		}
		if (refinedImplementation==null && refinedType==null)
			return true;
		
		if (refinedImplementation!=null && refinedImplementation.equals(implementation))
			return true;
		
		while (Classifier_Substitution_Rule.trim().equalsIgnoreCase("Type_Extension") && 
				testedImplementation.getRefines()!=null){
			testedImplementation = testedImplementation.getRefines();
			if (testedImplementation.equals(refinedImplementation))
				return true;
		}
		if (refinedType!=null && implementation.getType().equals(refinedType) &&
				isValidTypeRefinement(current, refinedType))
			return true;
		return false;
	}
	
	public static boolean isValidImplementationRefinement(Feature current, Component implementation) {
		return isValidImplementationRefinement(current, implementation, CLASSIFIER_SUBSTITUTION_RULE_DEFAULT );
	}
		
	public static boolean isValidImplementationRefinement(Feature current, Component implementation,String Classifier_Substitution_Rule) {
		if (implementation.isSubcomponentRefinement())
			return false;
		Component featureCompo=(Component)current.getComponent();
		Component refinedComponent;
		Component testedImplementation=implementation;
		if (featureCompo.isSubcomponentRefinement()) {
			refinedComponent=((Component)featureCompo.getParent()).getRefines();
			Iterator<SKHierarchicalObject> childrenIt = refinedComponent.getChildren().iterator();
			while (childrenIt.hasNext()){
				Component tmp=(Component)childrenIt.next();
				if (tmp.getName().equalsIgnoreCase(featureCompo.getName())){
					refinedComponent = tmp;
					break;
				}
			}
		}
		else 
			refinedComponent=featureCompo.getRefines();
		
		Iterator<SKFeature> featureIt;
		if (refinedComponent.getType()!=null)
			featureIt = refinedComponent.getType().getFeatures().iterator();
		else
			featureIt = refinedComponent.getFeatures().iterator();
		Component baseImplementation=null;
		while (featureIt.hasNext()){
			SKFeature tmp=featureIt.next();
			if (tmp.getName().equalsIgnoreCase(current.getName())){
				final SKComponent classifier = ((Feature)tmp).getClassifier();
				
				if ( classifier instanceof Component ) {
					baseImplementation = (Component) classifier;
					break;
				}
			}
		}
		if (baseImplementation==null)
			return true;
		
		Component baseImplementationType = baseImplementation.getType();
		Component testedImplementationType = testedImplementation.getType();
		if (baseImplementationType == null)
			baseImplementationType = baseImplementation;
		if (testedImplementationType == null)
			testedImplementationType = testedImplementation;
		
		if ( baseImplementationType.equals(testedImplementationType)  )
			return true;
		if (Classifier_Substitution_Rule.trim().equalsIgnoreCase("Type_Extension")) {
			while (testedImplementationType.getRefines()!=null){
				testedImplementationType = testedImplementationType.getRefines();
				if (baseImplementationType.equals(testedImplementationType))
					return true;
			}
		}
		return false;
	}
	
	public static EList<Component> getRefinableImplementation(EList<EObject> contents, Component current){
		EList<Component> possibleImplementations = getValideImplementation(contents, current);
		BasicEList<Component> refinableImplementations = new BasicEList<Component>();
		Component impl;
		Component testedImplType;
		
		for (int i=0; i<possibleImplementations.size(); i++){
			impl=possibleImplementations.get(i);
			testedImplType=current.getType();
			if (testedImplType!=null) {
				while (testedImplType!=null){
					if (testedImplType.equals(impl.getType())) {
						if (isValidImplementationRefinement(current,impl))
							refinableImplementations.add(impl);
						break;
					}
					testedImplType = testedImplType.getRefines();
				}
			} else if (!current.getImplementationName().equalsIgnoreCase("") &&
					current.getClass().equals(impl.getClass())) {
				refinableImplementations.add(impl);
			}
		}
		
		return refinableImplementations;
	}
	
	public static EList<Component> getValideAbstractTypes(EList<? extends EObject> contents, Component current){
		BasicEList<Component> possibleTypes = new BasicEList<Component>();
		
		if (current.isSubcomponentRefinement() && current.getImplementation()!=null){
			return possibleTypes;
		}
		
		for (int i=0; i<contents.size(); i++){
			if(contents.get(i)!=current &&
					contents.get(i) instanceof Abstract &&
					((Component)contents.get(i)).isType() &&
					((Component)contents.get(i)).getParent()!=null){
				if (!(current.isSubcomponentRefinement() && !isValidTypeRefinement(current,(Component)contents.get(i))))
					possibleTypes.add((Component)contents.get(i));
			}
			if(((SKHierarchicalObject)contents.get(i)).getChildren().size()>0)
				possibleTypes.addAll(getValideAbstractTypes(((SKHierarchicalObject)contents.get(i)).getChildren(), current));
		}
		
		return possibleTypes;
	}
	
	public static EList<Component> getValideAbstractImplementation(EList<? extends EObject> contents, Component current){
		BasicEList<Component> possibleImplementations = new BasicEList<Component>();
		
		for (int i=0; i<contents.size(); i++){
			EObject tmp = contents.get(i);
			if(tmp!=current &&
					tmp instanceof Abstract){
				if ( ((Component)tmp).isImplementation() && 
						((Component)tmp).getParent()!=null &&
						!((Component)tmp).getParent().getName().contains(".")){
					if (isValidAbstractImplementationRefinement(current, (Component)tmp))
						possibleImplementations.add((Component)tmp);
				}
			}
			if(((SKHierarchicalObject)contents.get(i)).getChildren().size()>0)
				possibleImplementations.addAll(getValideAbstractImplementation(((SKHierarchicalObject)contents.get(i)).getChildren(), current));
		}
		
		return possibleImplementations;
	}
	
	public static boolean isValidAbstractImplementationRefinement(Component current, Component implementation) {
		EList<SKHierarchicalObject> children = implementation.getChildren();
		for (int i=0; i<children.size();i++){
			if (!(ADELE_ComponentEditPart.isValidSubcompponent((Component)children.get(i), current)))
				return false;
		}
		Component type = current.getType();
		Component testedType = implementation.getType();

		if (testedType.equals(type))
			return true;
		if (type!=null){
			while (type.getRefines()!=null){
				type = type.getRefines();
				if (testedType.equals(type))
					return true;
			}
		} else if (!current.getImplementationName().equalsIgnoreCase(""))
			return true;
		return false;
	}
	
	public static SKComponent openTypeChooseDialog(EList<SKComponent> matchingTypes, SKComponent type, boolean isRefinement){
		
		TypeChooseDialog window = new TypeChooseDialog(Display.getCurrent().getActiveShell());
		window.create(matchingTypes, type, isRefinement);
		window.setTitle("Classifier explorator");
		window.setMessage("Please select one of the classifiers available for this classifier instance in the following list.",IMessageProvider.WARNING);
		window.setBlockOnOpen(true);
		int index=window.open();
		
		// DB: Allow choosing null value
		if ( index == matchingTypes.size() ) {
			return null;
		}
		
		if (index!=-1 ) {
			try{
				return matchingTypes.get(index);
			}
			catch (Exception e){
				return null;
			}
		}

		return type;
	}
	
	public static List<?> openImplementationChooseDialog(EList<Component> matchingImplementations, Component obj){
		
		final List<Object> res=new BasicEList<Object>();
												// DB use LinkedHashMap to preserve order.
		final Map<String, Component> map = new LinkedHashMap<String, Component>();

		for ( final Component matchingCompo : matchingImplementations ) {
			final String id;
			
			if ( matchingCompo == null ) {
				id = "null";
			}
			else {
				id = matchingCompo.getId();
			}
			
			map.put( id, matchingCompo );
		}
		matchingImplementations = new BasicEList<Component>();
		matchingImplementations.addAll(map.values());
		
		ImplementationChooseDialog window = new ImplementationChooseDialog(Display.getCurrent().getActiveShell());
		window.create(matchingImplementations, obj);
		window.setTitle("Classifier explorator");
		window.setMessage("Please select one of the Classifiers available for this classifier instance in the following list, or enter a free form name.",IMessageProvider.WARNING);
		window.setBlockOnOpen(true);
		int index=window.open();
		
		if (index>-1){
			// DB: Handle null selected classifier
			final Component selected = matchingImplementations.get( index );

			if ( selected == null ) {
				res.add(selected);
				res.add(null);
				res.add("");
			}
			else if (selected.getType()==null ){
				res.add(selected);
				res.add(selected);
				res.add(selected.getImplementationName());
			}
			else{
				res.add(selected.getType());
				res.add(selected);
				res.add(selected.getImplementationName());
			}
		}
		else if (index == -2){
			res.add(null);
			res.add(null);
			res.add(obj.getImplementationName());
		}
		
		return res;
	}
	
	public static SKComponent openRefinesTypeDialog(EList<SKComponent> matchingTypes, Component type){
		
		TypeChooseDialog window = new TypeChooseDialog(Display.getCurrent().getActiveShell());
		window.create(matchingTypes, type, false);
		window.setTitle("Component type explorator");
		window.setMessage("Please select a component type to refine in the following list.",IMessageProvider.WARNING);
		window.setBlockOnOpen(true);
		int index=window.open();
		
		if (index!=-1){
			try{
				return matchingTypes.get(index);
			}
			catch (Exception e){
				return null;
			}
		}
		else
			return type;
	}
	
	public static Component openRefinesImplementationDialog(EList<Component> matchingImplementations, Component obj){
				
		HashMap<String, Component> map = new HashMap<String, Component>();
		for (int i=0; i<matchingImplementations.size(); i++){
			map.put(matchingImplementations.get(i).getId(), matchingImplementations.get(i));
		}
		matchingImplementations = new BasicEList<Component>();
		matchingImplementations.addAll(map.values());
		
		RefinableImplementationChooseDialog window = new RefinableImplementationChooseDialog(Display.getCurrent().getActiveShell());
		window.create(matchingImplementations, obj);
		window.setTitle("Component explorator");
		window.setMessage("Please select a component to refine in the following list.",IMessageProvider.WARNING);
		window.setBlockOnOpen(true);
		int index=window.open();
		
		if (index!=-1){
			try{
				return matchingImplementations.get(index);
			}
			catch (Exception e){
				return null;
			}
		}
		else
			return obj.getRefines();
	}
	
	public static boolean noCircularTypeDependency(Component current, Component type) {
		
		Component testedType = type;
		
		if (testedType==current)
			return false;
		
		while (testedType.getRefines()!=null){
			testedType = testedType.getRefines();
			if (testedType.getType()!=null)
				testedType = testedType.getType();
			if (testedType==current)
				return false;
		}
		
		return true;
	}
	
	public static boolean noCircularImplementationDependency(Component current, Component implementation) {
		
		Component testedImplementation = implementation;
		
		if (testedImplementation==current)
			return false;
		
		while (testedImplementation.getRefines()!=null){
			testedImplementation = testedImplementation.getRefines();
			if (testedImplementation==current)
				return false;
		}
		
		return true;
	}
	
	public static boolean dependsOn( 	final FeatureGroup p_feature1,
										final FeatureGroup p_feature2 ) {
		if ( p_feature1 == null || p_feature2 == null ) {
			return false;
		}
		
		if ( p_feature1 == p_feature2 ) {
			return true;
		}
		
		final FeatureGroup inverse = p_feature1.getInverseFeature();
		
		if ( dependsOn( inverse, p_feature2 ) ) {
			return true;
		}
		
		final Feature refined = p_feature1.getRefinedFeature();
		
		if ( refined instanceof FeatureGroup && dependsOn( (FeatureGroup) refined, p_feature2 ) ) {
			return true;
		}
		
		return false;
	}
}
