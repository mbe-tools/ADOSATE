package org.topcased.adele.editor.objectDescriptionModel;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.events.MouseEvent;
import org.topcased.adele.common.objectDescriptionModel.OdsView;
import org.topcased.adele.editor.utils.ADELE_Utils;
import org.topcased.adele.model.ADELE_Components.Abstract;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.modeler.edit.GraphNodeEditPart;

public class ClassifierTypeMouseListener extends AdeleMouseListener {
	
	public ClassifierTypeMouseListener(OdsView _parent){
		super( _parent );
	}

	@Override
	public void mouseDown(MouseEvent arg0) {
		final EList<SKComponent> possibleTypes = new BasicEList<SKComponent>();
		final EList<Resource> resources = currentEditor.getResourceSet().getResources();
		final Component obj = (Component)currentEditor.getSelectedObject();
		
		for (int i=0; i<resources.size(); i++){
			if (resources.get(i).getURI().lastSegment().endsWith(".adele")){
				possibleTypes.addAll(ADELEOdsView.getValideTypes(resources.get(i).getContents(),obj));
			}
			if (!(obj instanceof Abstract))
				possibleTypes.addAll(ADELEOdsView.getValideAbstractTypes(resources.get(i).getContents(),obj));
		}

		final SKComponent choosedType = ADELEOdsView.openTypeChooseDialog(possibleTypes,obj.getType(),false);
		
		if (choosedType!=obj.getType()){
			if (choosedType!=null)
				obj.setFeaturesLock(true);
			else
				obj.setFeaturesLock(false);
			obj.setSubcomponentsLock(false);
			if (obj.getImplementation()!=null )
				obj.setImplementation(null);
			if (!(obj.getImplementationName().equalsIgnoreCase(ADELE_Utils.getDefaultImplementationName())))
				obj.setImplementationName(ADELE_Utils.getDefaultImplementationName());
			obj.setType( (Component) choosedType);
		}

		currentEditor.dirtify();
		parent.setInput( currentEditor, currentEditor.getSelection() );
		ADELE_Utils.setToolTip(obj, (GraphNodeEditPart)((IStructuredSelection)currentEditor.getSelection()).getFirstElement());
	}
}
