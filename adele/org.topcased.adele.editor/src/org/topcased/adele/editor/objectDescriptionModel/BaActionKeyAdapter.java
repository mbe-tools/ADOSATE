package org.topcased.adele.editor.objectDescriptionModel;

import org.eclipse.jface.text.ITextListener;
import org.eclipse.jface.text.TextEvent;
import org.topcased.adele.common.objectDescriptionModel.OdsView;

public class BaActionKeyAdapter implements ITextListener {

	OdsView parent;
	
	public BaActionKeyAdapter(OdsView _parent){
		super();
		parent=_parent;
	}

	public void textChanged(TextEvent event) {
		if (event.getDocumentEvent()!=null) {
			parent.setDirty(true);
			parent.setSpecificAttribut("baactionCode", event.getDocumentEvent().getDocument().get());
		}
	}

}
