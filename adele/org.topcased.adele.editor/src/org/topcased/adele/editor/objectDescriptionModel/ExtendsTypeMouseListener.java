package org.topcased.adele.editor.objectDescriptionModel;

import java.util.Collection;
import java.util.Iterator;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.topcased.adele.common.objectDescriptionModel.OdsView;
import org.topcased.adele.common.utils.AdeleCommonUtils;
import org.topcased.adele.editor.utils.ADELE_Utils;
import org.topcased.adele.editor.utils.dialogs.TypeChooseDialog;
import org.topcased.adele.model.ADELE_Components.Abstract;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Features.Feature;
import org.topcased.adele.model.ADELE_Features.FeatureGroup;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.adele.model.KernelSpices.SKObject;
import org.topcased.adele.model.errors.UncompatibleRefinesReference;
import org.topcased.modeler.edit.GraphNodeEditPart;

public class ExtendsTypeMouseListener extends AdeleMouseListener {
	
	ExtendsTypeMouseListener (OdsView _parent){
		super( _parent );
	}
	
	private void openTypeSelectionDialog( final Component p_component ) {
		final EList<SKComponent> possibleTypes = new BasicEList<SKComponent>();
		final EList<Resource> resources = currentEditor.getResourceSet().getResources();
		final Component currentExtends; 
		final String classSubRule = getClassifierSubstitutionRule( p_component );
		
		for ( final Resource res : resources ) {
//			if ( "adele".equals( res.getURI().fileExtension() ) ) {
			if ( AdeleCommonUtils.isAdeleResource( res ) ) {
				possibleTypes.addAll( getValideTypes( res.getContents(), p_component, classSubRule ) );
				
				if ( !( p_component instanceof Abstract) ) {
					possibleTypes.addAll( getValideAbstractTypes( res.getContents(), p_component ) );
				}
			}
		}
		
		currentExtends = p_component.getRefines();
		final Component choosedType = (Component) openDialog( possibleTypes, currentExtends );
		
		try {
			if ( choosedType != currentExtends ) {
				p_component.setRefinesValidated( choosedType );
			}
			
			ADELE_Utils.setToolTip( p_component, (GraphNodeEditPart)((IStructuredSelection)currentEditor.getSelection()).getFirstElement());
		}
		catch (UncompatibleRefinesReference e) {
			MessageBox messageBox = new MessageBox(Display.getCurrent().getActiveShell(), SWT.ICON_ERROR | SWT.OK);
			messageBox.setMessage("The current component and the choosed reference are not compatible:\n" +
					e.getMessage());
			messageBox.setText("Conflict in refinement");
			messageBox.open();
		}
	}
	
	private void openTypeSelectionDialog( final FeatureGroup p_featGroup ) {
		final EList<SKComponent> possibleTypes = new BasicEList<SKComponent>();
		final EList<Resource> resources = currentEditor.getResourceSet().getResources();
		final SKComponent currentExtends; 
		
		for ( final Resource res : resources ) {
//			if ( "adele".equals( res.getURI().fileExtension() ) ) {
			if ( AdeleCommonUtils.isAdeleResource( res ) ) {
				possibleTypes.addAll( getValidTypes( res.getContents(), p_featGroup ) );
			}
		}
		
		final Feature refFeat = p_featGroup.getRefinedFeature(); 
		currentExtends = refFeat instanceof FeatureGroup ? (FeatureGroup) refFeat : null;
		final FeatureGroup choosedType = (FeatureGroup) openDialog( possibleTypes, currentExtends );
		
		if ( choosedType != currentExtends ) {
			p_featGroup.setRefinedFeature( choosedType );
		}
	}

	@Override
	public void mouseDown(MouseEvent arg0) {
		final SKHierarchicalObject obj = currentEditor.getSelectedObject();
		
		if ( obj instanceof Component ) {
			openTypeSelectionDialog( (Component) obj );
		}
		else if ( obj instanceof FeatureGroup ) {
			openTypeSelectionDialog( (FeatureGroup) obj );
		}

		currentEditor.dirtify();
		parent.setInput(currentEditor, currentEditor.getSelection());
	}
	
	public static EList<Component> getValideTypes(	EList<? extends EObject> contents, 
													Component current,
													String Classifier_Substitution_Rule ){
		final EList<Component> possibleTypes = new BasicEList<Component>();
		
		for (int i=0; i<contents.size(); i++){
			EObject tmp=contents.get(i);
			if(tmp!=current &&
					tmp.getClass()==current.getClass() &&
					((Component)tmp).isType() &&
					((Component)tmp).getParent()!=null &&
					ADELEOdsView.noCircularTypeDependency(current,(Component)tmp) ) {
				if (!(current.isSubcomponentRefinement() && !isValidTypeExtends(current,(Component)tmp,Classifier_Substitution_Rule)))
					possibleTypes.add((Component)tmp);
			}
			if(!((SKObject)tmp).getName().contains(".") && ((SKHierarchicalObject)tmp).getChildren().size()>0)
				possibleTypes.addAll(getValideTypes(((SKHierarchicalObject)tmp).getChildren(), current,Classifier_Substitution_Rule));
		}
		
		return possibleTypes;
	}
		
	
	public static EList<FeatureGroup> getValidTypes(	final Collection<EObject> p_content, 
														final FeatureGroup p_current ){
		final EList<FeatureGroup> possibleTypes = new BasicEList<FeatureGroup>();
		
		for ( final EObject element : p_content ) {
			if ( element instanceof SKHierarchicalObject ) {
				for ( final SKHierarchicalObject children : ( (SKHierarchicalObject) element ).getChildren() ) {
					if ( 	children != p_current && children instanceof FeatureGroup &&
							!ADELEOdsView.dependsOn( (FeatureGroup) children, p_current )) {
						possibleTypes.add( (FeatureGroup) children );
					}
				}
			}
		}
		
		return possibleTypes;
	}

	public static boolean isValidTypeExtends(Component current, Component type, String Classifier_Substitution_Rule) {
		if (((Component)current.getParent()).getRefines()==null)
			return false;
		Iterator<SKHierarchicalObject> childrenIt = ((Component)current.getParent()).getRefines().getChildren().iterator();
		Component testedType=null;
		while (childrenIt.hasNext()){
			SKHierarchicalObject tmp=childrenIt.next();
			if (tmp.getName().equalsIgnoreCase(current.getName())){
				testedType = ((Component)tmp).getType();
				break;
			}
		}
		if (testedType==null || testedType.equals(type))
			return true;
		if (Classifier_Substitution_Rule.trim().equalsIgnoreCase("Type_Extension")) {
			while (type.getRefines()!=null){
				type = type.getRefines();
				if (testedType.equals(type))
					return true;
			}
		}
		return false;
	}
	
	public static EList<Component> getValideAbstractTypes(EList<? extends EObject> contents, Component current){
		BasicEList<Component> possibleTypes = new BasicEList<Component>();
		
		if (current.isSubcomponentRefinement() && current.getImplementation()!=null){
			return possibleTypes;
		}
		
		for (int i=0; i<contents.size(); i++){
			if(contents.get(i)!=current &&
					contents.get(i) instanceof Abstract &&
					((Component)contents.get(i)).isType() &&
					((Component)contents.get(i)).getParent()!=null){
				if (!(current.isSubcomponentRefinement() && !isValidTypeExtends(current,(Component)contents.get(i),"")))
					possibleTypes.add((Component)contents.get(i));
			}
			if(((SKHierarchicalObject)contents.get(i)).getChildren().size()>0)
				possibleTypes.addAll(getValideAbstractTypes(((SKHierarchicalObject)contents.get(i)).getChildren(), current));
		}
		
		return possibleTypes;
	}
	
	public static SKComponent openDialog(EList<SKComponent> matchingTypes, SKComponent type){
		TypeChooseDialog window = new TypeChooseDialog(Display.getCurrent().getActiveShell());
		window.create(matchingTypes, type, false);
		window.setTitle("Component type explorator");
		window.setMessage("Please select a component type to extends in the following list.",IMessageProvider.WARNING);
		window.setBlockOnOpen(true);
		final int index = window.open();
		
		// None was selected
		if ( index == matchingTypes.size() ) {
			return null;
		}
		
		if ( index != -1 ){
			return matchingTypes.get(index);
		}

		return type;
	}
}
