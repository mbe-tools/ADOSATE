package org.topcased.adele.editor.objectDescriptionModel;

import java.util.HashMap;
import java.util.Iterator;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.topcased.adele.common.objectDescriptionModel.OdsView;
import org.topcased.adele.editor.utils.ADELE_Utils;
import org.topcased.adele.editor.utils.dialogs.RefinableImplementationChooseDialog;
import org.topcased.adele.editor.utils.edit.ADELE_ComponentEditPart;
import org.topcased.adele.model.ADELE_Components.Abstract;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.adele.model.KernelSpices.SKObject;
import org.topcased.adele.model.errors.UncompatibleRefinesReference;
import org.topcased.modeler.edit.GraphNodeEditPart;

public class ExtendsImplementationMouseListener extends AdeleMouseListener {
	
	ExtendsImplementationMouseListener (OdsView _parent){
		super( _parent );
	}

	@Override
	public void mouseDown(MouseEvent arg0) {
		BasicEList<Component> possibleClassifiers = new BasicEList<Component>();
		EList<Resource> resources = currentEditor.getResourceSet().getResources();
		Component obj = (Component)currentEditor.getSelectedObject();
		final String classSubRule = getClassifierSubstitutionRule( null );

		for (int i=0; i<resources.size(); i++){
			if (resources.get(i).getURI().lastSegment().endsWith(".adele")){
				possibleClassifiers.addAll(getValideImplementation(resources.get(i).getContents(),obj, classSubRule ));
				if (obj.getType() == obj.getImplementation())
					possibleClassifiers.addAll(getValideTypes(resources.get(i).getContents(),obj, classSubRule ));
				if (!(obj instanceof Abstract))
					possibleClassifiers.addAll(getValideAbstractImplementation(resources.get(i).getContents(),obj));
			}
		}

		Component result = openDialog(possibleClassifiers,obj);

		try {
			if (result!=obj.getRefines()){
				obj.setRefinesValidated(result);
			}

			currentEditor.dirtify();
			parent.setInput(currentEditor, currentEditor.getSelection());
			final Object selectedObj = ((IStructuredSelection)currentEditor.getSelection()).getFirstElement();
			
			if ( selectedObj instanceof GraphNodeEditPart ) {
				ADELE_Utils.setToolTip(obj, (GraphNodeEditPart)selectedObj );
			}
		}
		catch (UncompatibleRefinesReference e) {
			MessageBox messageBox = new MessageBox(Display.getCurrent().getActiveShell(), SWT.ICON_ERROR | SWT.OK);
			messageBox.setMessage("The current component and the choosed reference are not compatible:\n" +
					e.getMessage());
			messageBox.setText("Conflict in refinement");
			messageBox.open();
		}
	}
	
	public static EList<Component> getValideTypes(EList<? extends EObject> contents, Component current,String Classifier_Substitution_Rule){
		BasicEList<Component> possibleTypes = new BasicEList<Component>();
		
		for (int i=0; i<contents.size(); i++){
			EObject tmp=contents.get(i);
			if(tmp!=current &&
					tmp.getClass()==current.getClass() &&
					((Component)tmp).isType() &&
					((Component)tmp).getParent()!=null &&
					ADELEOdsView.noCircularTypeDependency(current,(Component)tmp) ) {
				if (!(current.isSubcomponentRefinement() && !isValidTypeExtends(current,(Component)tmp,Classifier_Substitution_Rule)))
					possibleTypes.add((Component)tmp);
			}
			if(!((SKObject)tmp).getName().contains(".") && ((SKHierarchicalObject)tmp).getChildren().size()>0)
				possibleTypes.addAll(getValideTypes(((SKHierarchicalObject)tmp).getChildren(), current,Classifier_Substitution_Rule));
		}
		
		return possibleTypes;
	}
		
	public static EList<Component> getValideImplementation(EList<? extends EObject> contents, Component current,String Classifier_Substitution_Rule){
		BasicEList<Component> possibleImplementations = new BasicEList<Component>();
		
		for (int i=0; i<contents.size(); i++){
			EObject tmp=contents.get(i);
			if(tmp!=current &&
					tmp.getClass()==current.getClass()){
					if ( ((Component)tmp).isImplementation() &&
						((Component)tmp).getParent()!=null &&
						!((Component)tmp).getParent().getName().contains(".")&&
						ADELEOdsView.noCircularImplementationDependency(current,(Component)tmp) ){
						if (!current.isSubcomponentRefinement() && isValidImplementationExtends(current,(Component)tmp))
							possibleImplementations.add((Component)tmp);
				}
			}
			if(((SKHierarchicalObject)contents.get(i)).getChildren().size()>0)
				possibleImplementations.addAll(getValideImplementation(((SKHierarchicalObject)contents.get(i)).getChildren(), current,Classifier_Substitution_Rule));
		}
		
		return possibleImplementations;
	}
	
	public static EList<Component> getValideAbstractImplementation(EList<? extends EObject> contents, Component current){
		BasicEList<Component> possibleImplementations = new BasicEList<Component>();
		
		for (int i=0; i<contents.size(); i++){
			EObject tmp = contents.get(i);
			if(tmp!=current &&
					tmp instanceof Abstract){
				if ( ((Component)tmp).isImplementation() && 
						((Component)tmp).getParent()!=null &&
						!((Component)tmp).getParent().getName().contains(".")){
					if (isValidAbstractImplementationRefinement(current, (Component)tmp))
						possibleImplementations.add((Component)tmp);
				}
			}
			if(((SKHierarchicalObject)contents.get(i)).getChildren().size()>0)
				possibleImplementations.addAll(getValideAbstractImplementation(((SKHierarchicalObject)contents.get(i)).getChildren(), current));
		}
		
		return possibleImplementations;
	}
		
	public static boolean isValidTypeExtends(Component current, Component type, String Classifier_Substitution_Rule) {
		if (((Component)current.getParent()).getRefines()==null)
			return false;
		Iterator<SKHierarchicalObject> childrenIt = ((Component)current.getParent()).getRefines().getChildren().iterator();
		Component testedType=null;
		while (childrenIt.hasNext()){
			SKHierarchicalObject tmp=childrenIt.next();
			if (tmp.getName().equalsIgnoreCase(current.getName())){
				testedType = ((Component)tmp).getType();
				break;
			}
		}
		if (testedType==null || testedType.equals(type))
			return true;
		if (Classifier_Substitution_Rule.trim().equalsIgnoreCase("Type_Extension")) {
			while (type.getRefines()!=null){
				type = type.getRefines();
				if (testedType.equals(type))
					return true;
			}
		}
		return false;
	}
	
	public static boolean isValidImplementationExtends(Component current, Component implementation) {
		Component currentType = current.getType();
		Component testedType = implementation.getType();
		
		if ( currentType == testedType || currentType == null ) {
			return true;
		}
		
		while ( currentType.getRefines()!=null) {
			currentType=currentType.getRefines();
			if ( currentType == testedType) {
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean isValidAbstractImplementationRefinement(Component current, Component implementation) {
		EList<SKHierarchicalObject> children = implementation.getChildren();
		for (int i=0; i<children.size();i++){
			if (!(ADELE_ComponentEditPart.isValidSubcompponent((Component)children.get(i), current)))
				return false;
		}
		Component type = current.getType();
		Component testedType = implementation.getType();

		if (testedType.equals(type))
			return true;
		if (type!=null){
			while (type.getRefines()!=null){
				type = type.getRefines();
				if (testedType.equals(type))
					return true;
			}
		} else if (!current.getImplementationName().equalsIgnoreCase(""))
			return true;
		return false;
	}
	
	public static Component openDialog(EList<Component> matchingImplementations, Component obj){
				
		HashMap<String, Component> map = new HashMap<String, Component>();
		for (int i=0; i<matchingImplementations.size(); i++){
			map.put(matchingImplementations.get(i).getId(), matchingImplementations.get(i));
		}
		matchingImplementations = new BasicEList<Component>();
		matchingImplementations.addAll(map.values());
		
		RefinableImplementationChooseDialog window = new RefinableImplementationChooseDialog(Display.getCurrent().getActiveShell());
		window.create(matchingImplementations, obj);
		window.setTitle("Component explorator");
		window.setMessage("Please select a component to extends in the following list.",IMessageProvider.WARNING);
		window.setBlockOnOpen(true);
		int index=window.open();
		
		if (index!=-1){
			try{
				return matchingImplementations.get(index);
			}
			catch (Exception e){
				return null;
			}
		}
		else
			return obj.getRefines();
	}
}
