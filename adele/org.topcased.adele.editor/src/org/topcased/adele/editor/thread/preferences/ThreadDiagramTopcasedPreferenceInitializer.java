/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.thread.preferences;

import java.util.HashMap;

import org.eclipse.jface.resource.StringConverter;
import org.topcased.modeler.preferences.ITopcasedPreferenceInitializer;

/**
 * Initialize the preferences of Thread diagram
 * 
 * @generated
 */
public class ThreadDiagramTopcasedPreferenceInitializer implements
		ITopcasedPreferenceInitializer {

	/** 
	 * @see org.topcased.modeler.preferences.ITopcasedPreferenceInitializer#getDefaultPreference()
	 *	@generated
	 */
	public HashMap<String, String> getDefaultPreference() {
		HashMap<String, String> defaultThreadPreference = new HashMap<String, String>();
		// Initialize the default value of the DATA_DEFAULT_BACKGROUND_COLOR property 
		defaultThreadPreference.put(
				ThreadDiagramPreferenceConstants.DATA_DEFAULT_BACKGROUND_COLOR,
				"255,255,255");

		// Initialize the default value of the DATA_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference.put(
				ThreadDiagramPreferenceConstants.DATA_DEFAULT_FOREGROUND_COLOR,
				"0,0,0");

		// Initialize the default value of the DATA_DEFAULT_FONT property
		defaultThreadPreference.put(
				ThreadDiagramPreferenceConstants.DATA_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the SUBPROGRAM_DEFAULT_BACKGROUND_COLOR property 
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.SUBPROGRAM_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the SUBPROGRAM_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.SUBPROGRAM_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAM_DEFAULT_FONT property
		defaultThreadPreference.put(
				ThreadDiagramPreferenceConstants.SUBPROGRAM_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the SUBPROGRAMGROUP_DEFAULT_BACKGROUND_COLOR property 
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.SUBPROGRAMGROUP_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the SUBPROGRAMGROUP_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.SUBPROGRAMGROUP_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUP_DEFAULT_FONT property
		defaultThreadPreference.put(
				ThreadDiagramPreferenceConstants.SUBPROGRAMGROUP_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the ABSTRACT_DEFAULT_BACKGROUND_COLOR property 
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.ABSTRACT_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the ABSTRACT_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.ABSTRACT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACT_DEFAULT_FONT property
		defaultThreadPreference.put(
				ThreadDiagramPreferenceConstants.ABSTRACT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the THREAD_DEFAULT_BACKGROUND_COLOR property 
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.THREAD_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the THREAD_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.THREAD_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the THREAD_DEFAULT_FONT property
		defaultThreadPreference.put(
				ThreadDiagramPreferenceConstants.THREAD_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the EVENTPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.EVENTPORT_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORT_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.EVENTPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORT_DEFAULT_FONT property
		defaultThreadPreference.put(
				ThreadDiagramPreferenceConstants.EVENTPORT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the DATAPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.DATAPORT_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAPORT_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.DATAPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAPORT_DEFAULT_FONT property
		defaultThreadPreference.put(
				ThreadDiagramPreferenceConstants.DATAPORT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_FONT property
		defaultThreadPreference.put(
				ThreadDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the FEATUREGROUP_DEFAULT_BACKGROUND_COLOR property 
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the FEATUREGROUP_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the FEATUREGROUP_DEFAULT_FONT property
		defaultThreadPreference.put(
				ThreadDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the DATAACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.DATAACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.DATAACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAACCESS_DEFAULT_FONT property
		defaultThreadPreference.put(
				ThreadDiagramPreferenceConstants.DATAACCESS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_FONT property
		defaultThreadPreference.put(
				ThreadDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_FONT property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR property 
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_FONT property
		defaultThreadPreference.put(
				ThreadDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the PARAMETER_DEFAULT_BACKGROUND_COLOR property 
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.PARAMETER_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PARAMETER_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.PARAMETER_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PARAMETER_DEFAULT_FONT property
		defaultThreadPreference.put(
				ThreadDiagramPreferenceConstants.PARAMETER_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the EVENTPORTCONNECTION_EDGE_DEFAULT_FONT property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.EVENTPORTCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the EVENTPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.EVENTPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORTCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.EVENTPORTCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the DATAPORTCONNECTION_EDGE_DEFAULT_FONT property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.DATAPORTCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the DATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.DATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAPORTCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.DATAPORTCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FONT property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORTCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.EVENTDATAPORTCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the DATAACCESSCONNECTION_EDGE_DEFAULT_FONT property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.DATAACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the DATAACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.DATAACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.DATAACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FONT property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FONT property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FONT property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_ROUTER property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the PARAMETERCONNECTION_EDGE_DEFAULT_FONT property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.PARAMETERCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the PARAMETERCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.PARAMETERCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PARAMETERCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.PARAMETERCONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_FONT property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultThreadPreference
				.put(ThreadDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		return defaultThreadPreference;
	}
}
