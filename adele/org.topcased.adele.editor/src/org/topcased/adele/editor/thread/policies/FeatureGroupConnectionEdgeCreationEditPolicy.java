/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.thread.policies;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.commands.Command;
import org.topcased.adele.editor.thread.commands.FeatureGroupConnectionEdgeCreationCommand;
import org.topcased.adele.model.ADELE_Relations.FeatureGroupConnection;
import org.topcased.modeler.commands.CreateTypedEdgeCommand;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.utils.SourceTargetData;
import org.topcased.modeler.utils.Utils;

/**
 * FeatureGroupConnection edge creation
 *
 * @generated
 */
public class FeatureGroupConnectionEdgeCreationEditPolicy
		extends
		org.topcased.adele.editor.utils.policies.FeatureGroupConnectionEdgeCreationEditPolicy {
	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#createCommand(org.eclipse.gef.EditDomain, org.topcased.modeler.di.model.GraphEdge, org.topcased.modeler.di.model.GraphElement)
	 * @generated
	 */
	protected CreateTypedEdgeCommand createCommand(EditDomain domain,
			GraphEdge edge, GraphElement source) {
		return new FeatureGroupConnectionEdgeCreationCommand(domain, edge,
				source);
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#checkEdge(org.topcased.modeler.di.model.GraphEdge)
	 * @generated
	 */
	protected boolean checkEdge(GraphEdge edge) {
		return Utils.getElement(edge) instanceof FeatureGroupConnection;
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#checkSource(org.topcased.modeler.di.model.GraphElement)
	 * @generated
	 */
	@Override
	protected boolean checkSource(GraphElement source) {
		if (!super.checkSource(source)) {
			return false;
		}

		EObject object = Utils.getElement(source);
		if (object instanceof org.topcased.adele.model.ADELE_Features.FeatureGroup) {
			return true;
		}
		return false;
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#checkTargetForSource(org.topcased.modeler.di.model.GraphElement, org.topcased.modeler.di.model.GraphElement)
	 * @generated
	 */
	@Override
	protected boolean checkTargetForSource(GraphElement source,
			GraphElement target) {
		EObject sourceObject = Utils.getElement(source);
		EObject targetObject = Utils.getElement(target);
		if (!super.checkTargetForSource(source, target)) {
			return false;
		}

		if (sourceObject instanceof org.topcased.adele.model.ADELE_Features.FeatureGroup
				&& targetObject instanceof org.topcased.adele.model.ADELE_Features.FeatureGroup) {
			if (!sourceObject.equals(targetObject)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#checkCommand(org.eclipse.gef.commands.Command)
	 * @generated
	 */
	protected boolean checkCommand(Command command) {
		return command instanceof FeatureGroupConnectionEdgeCreationCommand;
	}

	/**
	 * @see org.topcased.modeler.edit.policies.AbstractEdgeCreationEditPolicy#getSourceTargetData(org.topcased.modeler.di.model.GraphElement, org.topcased.modeler.di.model.GraphElement)
	 * @generated
	 */
	protected SourceTargetData getSourceTargetData(GraphElement source,
			GraphElement target) {
		EObject sourceObject = Utils.getElement(source);
		EObject targetObject = Utils.getElement(target);

		if (sourceObject instanceof org.topcased.adele.model.ADELE_Features.FeatureGroup
				&& targetObject instanceof org.topcased.adele.model.ADELE_Features.FeatureGroup) {
			return new SourceTargetData(false, false, SourceTargetData.DIAGRAM,
					"org.topcased.adele.model.ADELE_Components.Data",
					"featureGroupConnection", null, null, null, null, null,
					null);
		}
		return null;
	}

}