/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.thread;

/**
 * A collection of Roles. Each identifier is used to key the EditPolicy.
 *
 * @generated
 */
public interface ThreadEditPolicyConstants {

	/**
	 * The key used to install an <i>Data</i> EditPolicy.
	 * @generated
	 */
	String DATA_EDITPOLICY = "Data EditPolicy";

	/**
	 * The key used to install an <i>Subprogram</i> EditPolicy.
	 * @generated
	 */
	String SUBPROGRAM_EDITPOLICY = "Subprogram EditPolicy";

	/**
	 * The key used to install an <i>SubprogramGroup</i> EditPolicy.
	 * @generated
	 */
	String SUBPROGRAMGROUP_EDITPOLICY = "SubprogramGroup EditPolicy";

	/**
	 * The key used to install an <i>Abstract</i> EditPolicy.
	 * @generated
	 */
	String ABSTRACT_EDITPOLICY = "Abstract EditPolicy";

	/**
	 * The key used to install an <i>Thread</i> EditPolicy.
	 * @generated
	 */
	String THREAD_EDITPOLICY = "Thread EditPolicy";

	/**
	 * The key used to install an <i>EventPort</i> EditPolicy.
	 * @generated
	 */
	String EVENTPORT_EDITPOLICY = "EventPort EditPolicy";

	/**
	 * The key used to install an <i>DataPort</i> EditPolicy.
	 * @generated
	 */
	String DATAPORT_EDITPOLICY = "DataPort EditPolicy";

	/**
	 * The key used to install an <i>EventDataPort</i> EditPolicy.
	 * @generated
	 */
	String EVENTDATAPORT_EDITPOLICY = "EventDataPort EditPolicy";

	/**
	 * The key used to install an <i>FeatureGroup</i> EditPolicy.
	 * @generated
	 */
	String FEATUREGROUP_EDITPOLICY = "FeatureGroup EditPolicy";

	/**
	 * The key used to install an <i>DataAccess</i> EditPolicy.
	 * @generated
	 */
	String DATAACCESS_EDITPOLICY = "DataAccess EditPolicy";

	/**
	 * The key used to install an <i>SubprogramAccess</i> EditPolicy.
	 * @generated
	 */
	String SUBPROGRAMACCESS_EDITPOLICY = "SubprogramAccess EditPolicy";

	/**
	 * The key used to install an <i>SubprogramGroupAccess</i> EditPolicy.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESS_EDITPOLICY = "SubprogramGroupAccess EditPolicy";

	/**
	 * The key used to install an <i>AbstractFeature</i> EditPolicy.
	 * @generated
	 */
	String ABSTRACTFEATURE_EDITPOLICY = "AbstractFeature EditPolicy";

	/**
	 * The key used to install an <i>Parameter</i> EditPolicy.
	 * @generated
	 */
	String PARAMETER_EDITPOLICY = "Parameter EditPolicy";

	/**
	 * The key used to install an <i>EventPortConnection</i> EditPolicy.
	 * @generated
	 */
	String EVENTPORTCONNECTION_EDITPOLICY = "EventPortConnection EditPolicy";

	/**
	 * The key used to install an <i>DataPortConnection</i> EditPolicy.
	 * @generated
	 */
	String DATAPORTCONNECTION_EDITPOLICY = "DataPortConnection EditPolicy";

	/**
	 * The key used to install an <i>EventDataPortConnection</i> EditPolicy.
	 * @generated
	 */
	String EVENTDATAPORTCONNECTION_EDITPOLICY = "EventDataPortConnection EditPolicy";

	/**
	 * The key used to install an <i>DataAccessConnection</i> EditPolicy.
	 * @generated
	 */
	String DATAACCESSCONNECTION_EDITPOLICY = "DataAccessConnection EditPolicy";

	/**
	 * The key used to install an <i>SubprogramAccessConnection</i> EditPolicy.
	 * @generated
	 */
	String SUBPROGRAMACCESSCONNECTION_EDITPOLICY = "SubprogramAccessConnection EditPolicy";

	/**
	 * The key used to install an <i>SubprogramGroupAccessConnection</i> EditPolicy.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESSCONNECTION_EDITPOLICY = "SubprogramGroupAccessConnection EditPolicy";

	/**
	 * The key used to install an <i>AbstractFeatureConnection</i> EditPolicy.
	 * @generated
	 */
	String ABSTRACTFEATURECONNECTION_EDITPOLICY = "AbstractFeatureConnection EditPolicy";

	/**
	 * The key used to install an <i>ParameterConnection</i> EditPolicy.
	 * @generated
	 */
	String PARAMETERCONNECTION_EDITPOLICY = "ParameterConnection EditPolicy";

	/**
	 * The key used to install an <i>FeatureGroupConnection</i> EditPolicy.
	 * @generated
	 */
	String FEATUREGROUPCONNECTION_EDITPOLICY = "FeatureGroupConnection EditPolicy";

}