/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.thread.edit;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPolicy;
import org.topcased.adele.editor.thread.figures.ThreadDiagramFigure;
import org.topcased.adele.editor.thread.policies.ThreadDiagramLayoutEditPolicy;
import org.topcased.adele.editor.utils.edit.ADELE_DiagramEditPart;
import org.topcased.modeler.di.model.Diagram;

/**
 * @generated NOT
 */
public class ThreadDiagramEditPart extends ADELE_DiagramEditPart {

	/**
	 * The Constructor
	 *
	 * @param model the root model element
	 * @generated
	 */
	public ThreadDiagramEditPart(Diagram model) {
		super(model);
	}

	/**
	 * @see org.topcased.modeler.edit.DiagramEditPart#getLayoutEditPolicy()
	 * @generated
	 */
	protected EditPolicy getLayoutEditPolicy() {
		return new ThreadDiagramLayoutEditPolicy();
	}

	/**
	 * @see org.topcased.modeler.edit.DiagramEditPart#createBodyFigure()
	 * @generated NOT
	 */
	protected IFigure createBodyFigure() {

		IFigure page = new ThreadDiagramFigure();

		super.createBodyFigure();

		return page;
	}
}