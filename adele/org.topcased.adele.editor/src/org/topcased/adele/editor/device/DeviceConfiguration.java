/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.device;

import java.net.URL;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.gef.EditPartFactory;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.adele.editor.device.edit.AbstractEditPart;
import org.topcased.adele.editor.device.edit.AbstractFeatureEditPart;
import org.topcased.adele.editor.device.edit.BusAccessEditPart;
import org.topcased.adele.editor.device.edit.BusEditPart;
import org.topcased.adele.editor.device.edit.DataPortEditPart;
import org.topcased.adele.editor.device.edit.DeviceEditPart;
import org.topcased.adele.editor.device.edit.EventDataPortEditPart;
import org.topcased.adele.editor.device.edit.EventPortEditPart;
import org.topcased.adele.editor.device.edit.FeatureGroupEditPart;
import org.topcased.adele.editor.device.edit.SubprogramAccessEditPart;
import org.topcased.adele.editor.device.edit.SubprogramGroupAccessEditPart;
import org.topcased.modeler.editor.IConfiguration;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.IPaletteManager;
import org.topcased.modeler.graphconf.DiagramGraphConf;
import org.topcased.modeler.graphconf.exceptions.MissingGraphConfFileException;

/**
 * A diagram configuration : manages Palette, EditPartFactory for this diagram.
 *
 * @generated
 */
public class DeviceConfiguration implements IConfiguration {
	/**
	 * @generated
	 */
	private DevicePaletteManager paletteManager;

	/**
	 * @generated
	 */
	private DeviceEditPartFactory editPartFactory;

	/**
	 * @generated
	 */
	private DeviceCreationUtils creationUtils;

	/**
	 * The DiagramGraphConf that contains graphical informations on the configuration
	 * @generated
	 */
	private DiagramGraphConf diagramGraphConf;

	/**
	 * Constructor. Initialize Adapter factories.
	 *
	 * @generated
	 */
	public DeviceConfiguration() {
		registerAdapters();
	}

	/**
	 * Registers the Adapter Factories for all the EditParts
	 *
	 * @generated
	 */
	private void registerAdapters() {
		Platform.getAdapterManager().registerAdapters(
				new EditPart2ModelAdapterFactory(BusEditPart.class,
						org.topcased.adele.model.ADELE_Components.Bus.class),
				BusEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								AbstractEditPart.class,
								org.topcased.adele.model.ADELE_Components.Abstract.class),
						AbstractEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								EventPortEditPart.class,
								org.topcased.adele.model.ADELE_Features.EventPort.class),
						EventPortEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								DataPortEditPart.class,
								org.topcased.adele.model.ADELE_Features.DataPort.class),
						DataPortEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								EventDataPortEditPart.class,
								org.topcased.adele.model.ADELE_Features.EventDataPort.class),
						EventDataPortEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								FeatureGroupEditPart.class,
								org.topcased.adele.model.ADELE_Features.FeatureGroup.class),
						FeatureGroupEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								SubprogramAccessEditPart.class,
								org.topcased.adele.model.ADELE_Features.SubprogramAccess.class),
						SubprogramAccessEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								SubprogramGroupAccessEditPart.class,
								org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess.class),
						SubprogramGroupAccessEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								BusAccessEditPart.class,
								org.topcased.adele.model.ADELE_Features.BusAccess.class),
						BusAccessEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								AbstractFeatureEditPart.class,
								org.topcased.adele.model.ADELE_Features.AbstractFeature.class),
						AbstractFeatureEditPart.class);
		Platform.getAdapterManager()
				.registerAdapters(
						new EditPart2ModelAdapterFactory(
								DeviceEditPart.class,
								org.topcased.adele.model.ADELE_Components.Device.class),
						DeviceEditPart.class);
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getId()
	 * @generated
	 */
	public String getId() {
		return new String("org.topcased.adele.editor.device");
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getName()
	 * @generated
	 */
	public String getName() {
		return new String("ADELE Device");
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getEditPartFactory()
	 * @generated
	 */
	public EditPartFactory getEditPartFactory() {
		if (editPartFactory == null) {
			editPartFactory = new DeviceEditPartFactory();
		}

		return editPartFactory;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getPaletteManager()
	 * @generated
	 */
	public IPaletteManager getPaletteManager() {
		if (paletteManager == null) {
			paletteManager = new DevicePaletteManager(getCreationUtils());
		}

		return paletteManager;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getCreationUtils()
	 * @generated
	 */
	public ICreationUtils getCreationUtils() {
		if (creationUtils == null) {
			creationUtils = new DeviceCreationUtils(getDiagramGraphConf());
		}

		return creationUtils;
	}

	/**
	 * @see org.topcased.modeler.editor.IConfiguration#getDiagramGraphConf()
	 * @generated
	 */
	public DiagramGraphConf getDiagramGraphConf() {
		if (diagramGraphConf == null) {
			URL url = ADELEPlugin
					.getDefault()
					.getBundle()
					.getResource(
							"org/topcased/adele/editor/device/diagram.graphconf");
			if (url != null) {
				URI fileURI = URI.createURI(url.toString());
				ResourceSet resourceSet = new ResourceSetImpl();
				Resource resource = resourceSet.getResource(fileURI, true);
				if (resource != null
						&& resource.getContents().get(0) instanceof DiagramGraphConf) {
					diagramGraphConf = (DiagramGraphConf) resource
							.getContents().get(0);
				}
			} else {
				new MissingGraphConfFileException(
						"The *.diagramgraphconf file can not be retrieved. Check if the path is correct in the Configuration class of your diagram.");
			}
		}

		return diagramGraphConf;
	}

}