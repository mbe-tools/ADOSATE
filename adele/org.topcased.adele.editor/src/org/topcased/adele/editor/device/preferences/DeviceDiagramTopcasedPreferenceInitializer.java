/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.device.preferences;

import java.util.HashMap;

import org.eclipse.jface.resource.StringConverter;
import org.topcased.modeler.preferences.ITopcasedPreferenceInitializer;

/**
 * Initialize the preferences of Device diagram
 * 
 * @generated
 */
public class DeviceDiagramTopcasedPreferenceInitializer implements
		ITopcasedPreferenceInitializer {

	/** 
	 * @see org.topcased.modeler.preferences.ITopcasedPreferenceInitializer#getDefaultPreference()
	 *	@generated
	 */
	public HashMap<String, String> getDefaultPreference() {
		HashMap<String, String> defaultDevicePreference = new HashMap<String, String>();
		// Initialize the default value of the BUS_DEFAULT_BACKGROUND_COLOR property 
		defaultDevicePreference.put(
				DeviceDiagramPreferenceConstants.BUS_DEFAULT_BACKGROUND_COLOR,
				"255,255,255");

		// Initialize the default value of the BUS_DEFAULT_FOREGROUND_COLOR property
		defaultDevicePreference.put(
				DeviceDiagramPreferenceConstants.BUS_DEFAULT_FOREGROUND_COLOR,
				"0,0,0");

		// Initialize the default value of the BUS_DEFAULT_FONT property
		defaultDevicePreference.put(
				DeviceDiagramPreferenceConstants.BUS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the ABSTRACT_DEFAULT_BACKGROUND_COLOR property 
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.ABSTRACT_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the ABSTRACT_DEFAULT_FOREGROUND_COLOR property
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.ABSTRACT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACT_DEFAULT_FONT property
		defaultDevicePreference.put(
				DeviceDiagramPreferenceConstants.ABSTRACT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the EVENTPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.EVENTPORT_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORT_DEFAULT_FOREGROUND_COLOR property
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.EVENTPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORT_DEFAULT_FONT property
		defaultDevicePreference.put(
				DeviceDiagramPreferenceConstants.EVENTPORT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the DATAPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.DATAPORT_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAPORT_DEFAULT_FOREGROUND_COLOR property
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.DATAPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAPORT_DEFAULT_FONT property
		defaultDevicePreference.put(
				DeviceDiagramPreferenceConstants.DATAPORT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR property
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_FONT property
		defaultDevicePreference.put(
				DeviceDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the FEATUREGROUP_DEFAULT_BACKGROUND_COLOR property 
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the FEATUREGROUP_DEFAULT_FOREGROUND_COLOR property
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the FEATUREGROUP_DEFAULT_FONT property
		defaultDevicePreference.put(
				DeviceDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_FONT property
		defaultDevicePreference.put(
				DeviceDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_FONT property
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the BUSACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.BUSACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BUSACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.BUSACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BUSACCESS_DEFAULT_FONT property
		defaultDevicePreference.put(
				DeviceDiagramPreferenceConstants.BUSACCESS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR property 
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR property
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_FONT property
		defaultDevicePreference.put(
				DeviceDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the DEVICE_DEFAULT_BACKGROUND_COLOR property 
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.DEVICE_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the DEVICE_DEFAULT_FOREGROUND_COLOR property
		defaultDevicePreference
				.put(DeviceDiagramPreferenceConstants.DEVICE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DEVICE_DEFAULT_FONT property
		defaultDevicePreference.put(
				DeviceDiagramPreferenceConstants.DEVICE_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		return defaultDevicePreference;
	}
}
