/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.packages.edit;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPolicy;
import org.topcased.adele.editor.packages.figures.PackageDiagramFigure;
import org.topcased.adele.editor.packages.policies.PackageDiagramLayoutEditPolicy;
import org.topcased.adele.editor.utils.edit.ADELE_DiagramEditPart;
import org.topcased.adele.editor.utils.edit.ADELE_EMFGraphNodeEditPart;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.utils.Utils;

/**
 * @generated NOT
 */
public class PackageDiagramEditPart extends ADELE_DiagramEditPart {

	/**
	 * The Constructor
	 *
	 * @param model the root model element
	 * @generated
	 */
	public PackageDiagramEditPart(Diagram model) {
		super(model);
	}

	/**
	 * @see org.topcased.modeler.edit.DiagramEditPart#getLayoutEditPolicy()
	 * @generated
	 */
	protected EditPolicy getLayoutEditPolicy() {
		return new PackageDiagramLayoutEditPolicy();
	}

	/**
	 * @see org.topcased.modeler.edit.DiagramEditPart#createBodyFigure()
	 * @generated
	 */
	protected IFigure createBodyFigure() {

		IFigure page = new PackageDiagramFigure();

		super.createBodyFigure();

		return page;
	}
	
	@Override
	protected void refreshDiagramFeatures() 
	throws CoreException {
		super.refreshDiagramFeatures();

		final EObject semanticElem = Utils.getElement( (GraphElement) getModel() );
		
		if ( semanticElem instanceof SKComponent ) {
			for ( final Object child : getChildren() ) {
				if ( child instanceof ADELE_EMFGraphNodeEditPart ) {
					( (ADELE_EMFGraphNodeEditPart) child ).refreshFeatures();
				}
			}
		}
	}
}