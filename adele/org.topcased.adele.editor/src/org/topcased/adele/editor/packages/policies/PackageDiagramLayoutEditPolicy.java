/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.packages.policies;

import java.util.Collections;
import java.util.List;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.commands.Command;
import org.topcased.adele.editor.utils.commands.SetUpFromOneLevelCommand;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Features.FeatureGroup;
import org.topcased.adele.model.KernelSpices.KernelSpicesPackage;
import org.topcased.draw2d.layout.PortConstraint;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy;
import org.topcased.modeler.utils.Utils;

/**
 * @generated
 */
public class PackageDiagramLayoutEditPolicy extends ModelerLayoutEditPolicy {
	/**
	 * Default contructor.
	 *
	 * @generated
	 */
	public PackageDiagramLayoutEditPolicy() {
		super();
	}

	/**
	 * @see org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy#isAttachedToBorder(org.topcased.modeler.di.model.GraphNode)
	 * @generated
	 */
	protected boolean isAttachedToBorder(GraphNode node) {
		return super.isAttachedToBorder(node);
	}

	@Override
	protected Command createChangeConstraintCommand(EditPart child,
			Object constraint, boolean checkConstraint) {
		if (constraint instanceof PortConstraint)
			return super.createChangeConstraintCommand(child, constraint,
					checkConstraint);
		if (isValidConstraint2((GraphicalEditPart) getHost(),
				(Rectangle) constraint))
			return super.createChangeConstraintCommand(child, constraint,
					checkConstraint);
		else {
			return new SetUpFromOneLevelCommand(child);
		}
	}

	/**
	 * @see org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy#isValid(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EObject)
	 * @generated NOT
	 */
	@Override
	protected boolean isValid(final EObject child, final EObject parent) {
		if (super.isValid(child, parent)) {
			return child instanceof Component || child instanceof FeatureGroup;
		}

		return false;
	}

	@Override
	protected boolean isValidConstraint(GraphicalEditPart parent,
			Rectangle constraint) {
		return true;
	}
	protected boolean isValidConstraint2(GraphicalEditPart parent,
			Rectangle constraint) {
//		for ( final Object child : parent.getChildren() ) {
//			if ( child instanceof GraphicalEditPart ) {
//				Rectangle childConstraint = ( (GraphicalEditPart) child ).getFigure().getBounds();
//			}
//		}

		return super.isValidConstraint(parent, constraint);
	}

	/* DB: Treat feature group type declarations as children of the package.
	 * (non-Javadoc)
	 * @see org.topcased.modeler.edit.policies.ModelerLayoutEditPolicy#getCreateCommand(org.eclipse.gef.EditDomain, org.topcased.modeler.di.model.GraphNode, org.topcased.modeler.di.model.GraphNode, org.eclipse.emf.ecore.EObject, org.eclipse.draw2d.geometry.Point, org.eclipse.draw2d.geometry.Dimension, int, java.util.List, boolean)
	 */
	@Override
	protected Command getCreateCommand(EditDomain domain, GraphNode newObject,
			GraphNode newParent, EObject newContainerParent, Point location,
			Dimension dimension, int attach, List featuresList,
			boolean needModelUpdate) {
		final EObject semanticObject = Utils.getElement(newObject);
		
		if ( semanticObject instanceof FeatureGroup ) {
			if ( newContainerParent instanceof Package ) {
				featuresList = Collections.singletonList( KernelSpicesPackage.Literals.SK_HIERARCHICAL_OBJECT__CHILDREN );
			}
			else {
				featuresList = Collections.singletonList( KernelSpicesPackage.Literals.SK_COMPONENT__FEATURES );
			}
		}
		
		return super.getCreateCommand( domain, newObject, newParent, newContainerParent, location, dimension, attach, featuresList, needModelUpdate );
	}
}