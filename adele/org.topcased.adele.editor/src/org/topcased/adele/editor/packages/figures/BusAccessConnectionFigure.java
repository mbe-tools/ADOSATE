/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.packages.figures;

import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.swt.SWT;

/**
 * @generated
 */
public class BusAccessConnectionFigure extends PolylineConnectionEx {

	/**
	 * The constructor
	 *
	 * @generated
	 */
	public BusAccessConnectionFigure() {
		super();
		setLineStyle(SWT.LINE_SOLID);
	}

}