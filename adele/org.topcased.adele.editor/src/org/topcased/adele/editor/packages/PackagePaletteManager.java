/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.packages;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.palette.CreationToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteStack;
import org.eclipse.gef.requests.CreationFactory;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;
import org.topcased.adele.model.ADELE_Features.AbstractFeature;
import org.topcased.adele.model.ADELE_Features.AccessDirection;
import org.topcased.adele.model.ADELE_Features.BusAccess;
import org.topcased.adele.model.ADELE_Features.DataAccess;
import org.topcased.adele.model.ADELE_Features.DataPort;
import org.topcased.adele.model.ADELE_Features.EventDataPort;
import org.topcased.adele.model.ADELE_Features.EventPort;
import org.topcased.adele.model.ADELE_Features.Parameter;
import org.topcased.adele.model.ADELE_Features.PortDirection;
import org.topcased.adele.model.ADELE_Features.SubprogramAccess;
import org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess;
import org.topcased.modeler.editor.GraphElementCreationFactory;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.palette.ModelerCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerPaletteManager;

/**
 * Generated Palette Manager
 *
 * @generated
 */
public class PackagePaletteManager extends ModelerPaletteManager {
	// declare all the palette categories of the diagram
	/**
	 * @generated
	 */
	private PaletteDrawer componentsDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer featuresDrawer;

	/**
	 * @generated
	 */
	private ICreationUtils creationUtils;

	/**
	 * The Constructor
	 *
	 * @param utils the creation utils for the tools of the palette 
	 * @generated
	 */
	public PackagePaletteManager(ICreationUtils utils) {
		super();
		this.creationUtils = utils;
	}

	/**
	 * Creates the main categories of the palette
	 *
	 * @generated
	 */
	protected void createCategories() {
		createComponentsDrawer();
		createFeaturesDrawer();
	}

	/**
	 * Updates the main categories of the palette
	 *
	 * @generated
	 */
	protected void updateCategories() {
		// deletion of the existing categories and creation of the updated categories

		getRoot().remove(componentsDrawer);
		createComponentsDrawer();

		getRoot().remove(featuresDrawer);
		createFeaturesDrawer();
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated
	 */
	private void createComponentsDrawer() {
		componentsDrawer = new PaletteDrawer("Components", null);
		List<PaletteEntry> entries = new ArrayList<PaletteEntry>();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getData(), "default");
		entries.add(new ModelerCreationToolEntry("Data", "Data", factory,
				PackageImageRegistry.getImageDescriptor("DATA"),
				PackageImageRegistry.getImageDescriptor("DATA_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getSubprogram(), "default");
		entries.add(new ModelerCreationToolEntry("Subprogram", "Subprogram",
				factory, PackageImageRegistry.getImageDescriptor("SUBPROGRAM"),
				PackageImageRegistry.getImageDescriptor("SUBPROGRAM_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getSubprogramGroup(),
				"default");
		entries.add(new ModelerCreationToolEntry("Subprogram group",
				"Subprogram group", factory, PackageImageRegistry
						.getImageDescriptor("SUBPROGRAMGROUP"),
				PackageImageRegistry
						.getImageDescriptor("SUBPROGRAMGROUP_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getThread(), "default");
		entries.add(new ModelerCreationToolEntry("Thread", "Thread", factory,
				PackageImageRegistry.getImageDescriptor("THREAD"),
				PackageImageRegistry.getImageDescriptor("THREAD_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getThreadGroup(), "default");
		entries.add(new ModelerCreationToolEntry("Thread group",
				"Thread group", factory, PackageImageRegistry
						.getImageDescriptor("THREADGROUP"),
				PackageImageRegistry.getImageDescriptor("THREADGROUP_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getProcess(), "default");
		entries.add(new ModelerCreationToolEntry("Process", "Process", factory,
				PackageImageRegistry.getImageDescriptor("PROCESS"),
				PackageImageRegistry.getImageDescriptor("PROCESS_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getProcessor(), "default");
		entries.add(new ModelerCreationToolEntry("Processor", "Processor",
				factory, PackageImageRegistry.getImageDescriptor("PROCESSOR"),
				PackageImageRegistry.getImageDescriptor("PROCESSOR_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getVirtualProcessor(),
				"default");
		entries.add(new ModelerCreationToolEntry("Virtual Processor",
				"Virtual Processor", factory, PackageImageRegistry
						.getImageDescriptor("VIRTUALPROCESSOR"),
				PackageImageRegistry
						.getImageDescriptor("VIRTUALPROCESSOR_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getMemory(), "default");
		entries.add(new ModelerCreationToolEntry("Memory", "Memory", factory,
				PackageImageRegistry.getImageDescriptor("MEMORY"),
				PackageImageRegistry.getImageDescriptor("MEMORY_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getBus(), "default");
		entries.add(new ModelerCreationToolEntry("Bus", "Bus", factory,
				PackageImageRegistry.getImageDescriptor("BUS"),
				PackageImageRegistry.getImageDescriptor("BUS_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getVirtualBus(), "default");
		entries.add(new ModelerCreationToolEntry("Virtual Bus", "Virtual Bus",
				factory, PackageImageRegistry.getImageDescriptor("VIRTUALBUS"),
				PackageImageRegistry.getImageDescriptor("VIRTUALBUS_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getDevice(), "default");
		entries.add(new ModelerCreationToolEntry("Device", "Device", factory,
				PackageImageRegistry.getImageDescriptor("DEVICE"),
				PackageImageRegistry.getImageDescriptor("DEVICE_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getSystem(), "default");
		entries.add(new ModelerCreationToolEntry("System", "System", factory,
				PackageImageRegistry.getImageDescriptor("SYSTEM"),
				PackageImageRegistry.getImageDescriptor("SYSTEM_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getPackage(), "default");
		entries.add(new ModelerCreationToolEntry("Package", "Package", factory,
				PackageImageRegistry.getImageDescriptor("PACKAGE"),
				PackageImageRegistry.getImageDescriptor("PACKAGE_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getAbstract(), "default");
		entries.add(new ModelerCreationToolEntry("Abstract", "Abstract",
				factory, PackageImageRegistry.getImageDescriptor("ABSTRACT"),
				PackageImageRegistry.getImageDescriptor("ABSTRACT_LARGE")));

		componentsDrawer.addAll(entries);
		getRoot().add(componentsDrawer);
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated NOT 
	 * DB TODO: Handle drawers in TMF generator.
	 */
	private void createFeaturesDrawer() {
		featuresDrawer = new PaletteDrawer("Features", null);
		List<PaletteEntry> entries = new ArrayList<PaletteEntry>();

		CreationFactory factory;
		String newObjTxt;
		CreationToolEntry objectTool;

		// Event Ports

		PaletteStack eventPortStack = new PaletteStack("Event Port",
				"Event Port Selection",
				PackageImageRegistry.getImageDescriptor("EVENTPORT"));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getEventPort(), "default") {
			public EObject getNewModelObject() {
				EventPort port = (EventPort) super.getNewModelObject();
				port.setDirection(PortDirection.IN_LITERAL);
				return port;
			}
		};
		newObjTxt = "In Event Port";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("INEVENTPORT"),
				PackageImageRegistry.getImageDescriptor("INEVENTPORT_LARGE"));
		eventPortStack.add(objectTool);
		eventPortStack.setActiveEntry(objectTool);

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getEventPort(), "default") {
			public EObject getNewModelObject() {
				EventPort port = (EventPort) super.getNewModelObject();
				port.setDirection(PortDirection.OUT_LITERAL);
				return port;
			}
		};
		newObjTxt = "Out Event Port";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("OUTEVENTPORT"),
				PackageImageRegistry.getImageDescriptor("OUTEVENTPORT_LARGE"));
		eventPortStack.add(objectTool);

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getEventPort(), "default") {
			public EObject getNewModelObject() {
				EventPort port = (EventPort) super.getNewModelObject();
				port.setDirection(PortDirection.INOUT_LITERAL);
				return port;
			}
		};
		newObjTxt = "InOut Event Port";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("INOUTEVENTPORT"),
				PackageImageRegistry.getImageDescriptor("INOUTEVENTPORT_LARGE"));
		eventPortStack.add(objectTool);

		entries.add(eventPortStack);

		// Data ports

		PaletteStack dataPortStack = new PaletteStack("Data Port",
				"Data Port Selection",
				PackageImageRegistry.getImageDescriptor("DATAPORT"));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getDataPort(), "default") {
			public EObject getNewModelObject() {
				DataPort port = (DataPort) super.getNewModelObject();
				port.setDirection(PortDirection.IN_LITERAL);
				return port;
			}
		};
		newObjTxt = "In Data Port";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory, PackageImageRegistry.getImageDescriptor("INDATAPORT"),
				PackageImageRegistry.getImageDescriptor("INDATAPORT_LARGE"));
		dataPortStack.add(objectTool);
		dataPortStack.setActiveEntry(objectTool);

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getDataPort(), "default") {
			public EObject getNewModelObject() {
				DataPort port = (DataPort) super.getNewModelObject();
				port.setDirection(PortDirection.OUT_LITERAL);
				return port;
			}
		};
		newObjTxt = "Out Data Port";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("OUTDATAPORT"),
				PackageImageRegistry.getImageDescriptor("OUTDATAPORT_LARGE"));
		dataPortStack.add(objectTool);

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getDataPort(), "default") {
			public EObject getNewModelObject() {
				DataPort port = (DataPort) super.getNewModelObject();
				port.setDirection(PortDirection.INOUT_LITERAL);
				return port;
			}
		};
		newObjTxt = "InOut Data Port";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("INOUTDATAPORT"),
				PackageImageRegistry.getImageDescriptor("INOUTDATAPORT_LARGE"));
		dataPortStack.add(objectTool);

		entries.add(dataPortStack);

		// Event Data ports

		PaletteStack eventDataPortStack = new PaletteStack("Event Data Port",
				"Event Data Port Selection",
				PackageImageRegistry.getImageDescriptor("EVENTDATAPORT"));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getEventDataPort(), "default") {
			public EObject getNewModelObject() {
				EventDataPort port = (EventDataPort) super.getNewModelObject();
				port.setDirection(PortDirection.IN_LITERAL);
				return port;
			}
		};
		newObjTxt = "In Event Data Port";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("INEVENTDATAPORT"),
				PackageImageRegistry
						.getImageDescriptor("INEVENTDATAPORT_LARGE"));
		eventDataPortStack.add(objectTool);
		eventDataPortStack.setActiveEntry(objectTool);

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getEventDataPort(), "default") {
			public EObject getNewModelObject() {
				EventDataPort port = (EventDataPort) super.getNewModelObject();
				port.setDirection(PortDirection.OUT_LITERAL);
				return port;
			}
		};
		newObjTxt = "Out Event Data Port";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("OUTEVENTDATAPORT"),
				PackageImageRegistry
						.getImageDescriptor("OUTEVENTDATAPORT_LARGE"));
		eventDataPortStack.add(objectTool);

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getEventDataPort(), "default") {
			public EObject getNewModelObject() {
				EventDataPort port = (EventDataPort) super.getNewModelObject();
				port.setDirection(PortDirection.INOUT_LITERAL);
				return port;
			}
		};
		newObjTxt = "InOut Event Data Port";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("INOUTEVENTDATAPORT"),
				PackageImageRegistry
						.getImageDescriptor("INOUTEVENTDATAPORT_LARGE"));
		eventDataPortStack.add(objectTool);

		entries.add(eventDataPortStack);

		// Feature Group

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getFeatureGroup(), "default");
		entries.add(new ModelerCreationToolEntry("Feature Group",
				"Feature Group", factory, PackageImageRegistry
						.getImageDescriptor("FEATUREGROUP"),
				PackageImageRegistry.getImageDescriptor("FEATUREGROUP_LARGE")));

		//	Data access

		PaletteStack dataAccessStack = new PaletteStack("Data Access",
				"Data Access Selection",
				PackageImageRegistry.getImageDescriptor("DataAccess"));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getDataAccess(), "default") {
			public EObject getNewModelObject() {
				DataAccess access = (DataAccess) super.getNewModelObject();
				access.setDirection(AccessDirection.PROVIDED_LITERAL);
				return access;
			}
		};
		newObjTxt = "Provides data access";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("PROVIDEDDATAACCESS"),
				PackageImageRegistry
						.getImageDescriptor("PROVIDEDDATAACCESS_LARGE"));
		dataAccessStack.add(objectTool);
		dataAccessStack.setActiveEntry(objectTool);

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getDataAccess(), "default") {
			public EObject getNewModelObject() {
				DataAccess access = (DataAccess) super.getNewModelObject();
				access.setDirection(AccessDirection.REQUIRED_LITERAL);
				return access;
			}
		};
		newObjTxt = "Requires data access";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("REQUIREDDATAACCESS"),
				PackageImageRegistry
						.getImageDescriptor("REQUIREDDATAACCESS_LARGE"));
		dataAccessStack.add(objectTool);

		entries.add(dataAccessStack);

		// Subprogram Access

		PaletteStack subprogramAccessStack = new PaletteStack(
				"Subprogram Access", "Subprogram Access Selection",
				PackageImageRegistry.getImageDescriptor("SubprogramAccess"));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getSubprogramAccess(),
				"default") {
			public EObject getNewModelObject() {
				SubprogramAccess access = (SubprogramAccess) super
						.getNewModelObject();
				access.setDirection(AccessDirection.PROVIDED_LITERAL);
				return access;
			}
		};
		newObjTxt = "Provides subprogram access";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry
						.getImageDescriptor("PROVIDEDSUBPROGRAMACCESS"),
				PackageImageRegistry
						.getImageDescriptor("PROVIDEDSUBPROGRAMACCESS_LARGE"));
		subprogramAccessStack.add(objectTool);
		subprogramAccessStack.setActiveEntry(objectTool);

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getSubprogramAccess(),
				"default") {
			public EObject getNewModelObject() {
				SubprogramAccess access = (SubprogramAccess) super
						.getNewModelObject();
				access.setDirection(AccessDirection.REQUIRED_LITERAL);
				return access;
			}
		};
		newObjTxt = "Requires subprogram access";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry
						.getImageDescriptor("REQUIREDSUBPROGRAMACCESS"),
				PackageImageRegistry
						.getImageDescriptor("REQUIREDSUBPROGRAMACCESS_LARGE"));
		subprogramAccessStack.add(objectTool);

		entries.add(subprogramAccessStack);

		// Subprogram Group Access

		PaletteStack subprogramGroupAccessStack = new PaletteStack(
				"Subprogram group access", "Subprogram group access Selection",
				PackageImageRegistry
						.getImageDescriptor("SubprogramGroupAccess"));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getSubprogramGroupAccess(),
				"default") {
			public EObject getNewModelObject() {
				SubprogramGroupAccess access = (SubprogramGroupAccess) super
						.getNewModelObject();
				access.setDirection(AccessDirection.PROVIDED_LITERAL);
				return access;
			}
		};
		newObjTxt = "Provides subprogram group access";
		objectTool = new ModelerCreationToolEntry(
				newObjTxt,
				newObjTxt,
				factory,
				PackageImageRegistry
						.getImageDescriptor("PROVIDEDSUBPROGRAMGROUPACCESS"),
				PackageImageRegistry
						.getImageDescriptor("PROVIDEDSUBPROGRAMGROUPACCESS_LARGE"));
		subprogramGroupAccessStack.add(objectTool);
		subprogramGroupAccessStack.setActiveEntry(objectTool);

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getSubprogramGroupAccess(),
				"default") {
			public EObject getNewModelObject() {
				SubprogramGroupAccess access = (SubprogramGroupAccess) super
						.getNewModelObject();
				access.setDirection(AccessDirection.REQUIRED_LITERAL);
				return access;
			}
		};
		newObjTxt = "Requires subprogram group access";
		objectTool = new ModelerCreationToolEntry(
				newObjTxt,
				newObjTxt,
				factory,
				PackageImageRegistry
						.getImageDescriptor("REQUIREDSUBPROGRAMGROUPACCESS"),
				PackageImageRegistry
						.getImageDescriptor("REQUIREDSUBPROGRAMGROUPACCESS_LARGE"));
		subprogramGroupAccessStack.add(objectTool);

		entries.add(subprogramGroupAccessStack);

		// Bus Access

		PaletteStack busAccessStack = new PaletteStack("Bus Access",
				"Bus Access Selection",
				PackageImageRegistry.getImageDescriptor("BusAccess"));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getBusAccess(), "default") {
			public EObject getNewModelObject() {
				BusAccess access = (BusAccess) super.getNewModelObject();
				access.setDirection(AccessDirection.PROVIDED_LITERAL);
				return access;
			}
		};
		newObjTxt = "Provides bus access";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("PROVIDEDBUSACCESS"),
				PackageImageRegistry
						.getImageDescriptor("PROVIDEDBUSACCESS_LARGE"));
		busAccessStack.add(objectTool);
		busAccessStack.setActiveEntry(objectTool);

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getBusAccess(), "default") {
			public EObject getNewModelObject() {
				BusAccess access = (BusAccess) super.getNewModelObject();
				access.setDirection(AccessDirection.REQUIRED_LITERAL);
				return access;
			}
		};
		newObjTxt = "Requires bus access";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("REQUIREDBUSACCESS"),
				PackageImageRegistry
						.getImageDescriptor("REQUIREDBUSACCESS_LARGE"));
		busAccessStack.add(objectTool);

		entries.add(busAccessStack);

		// Abstract Feature

		PaletteStack abstractFeatureStack = new PaletteStack("Feature",
				"Feature Selection",
				PackageImageRegistry.getImageDescriptor("ABSTRACTFEATURE"));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getAbstractFeature(), "default") {
			public EObject getNewModelObject() {
				AbstractFeature port = (AbstractFeature) super
						.getNewModelObject();
				//port.setDirection(AbstractFeatureDirection.NONE);
				return port;
			}
		};
		newObjTxt = "Feature";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("ABSTRACTFEATURE"),
				PackageImageRegistry
						.getImageDescriptor("ABSTRACTFEATURE_LARGE"));
		abstractFeatureStack.add(objectTool);
		abstractFeatureStack.setActiveEntry(objectTool);

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getAbstractFeature(), "default") {
			public EObject getNewModelObject() {
				AbstractFeature port = (AbstractFeature) super
						.getNewModelObject();
				//port.setDirection(AbstractFeatureDirection.IN);
				return port;
			}
		};
		newObjTxt = "In Feature";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("INABSTRACTFEATURE"),
				PackageImageRegistry
						.getImageDescriptor("INABSTRACTFEATURE_LARGE"));
		abstractFeatureStack.add(objectTool);

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getAbstractFeature(), "default") {
			public EObject getNewModelObject() {
				AbstractFeature port = (AbstractFeature) super
						.getNewModelObject();
				//port.setDirection(AbstractFeatureDirection.OUT);
				return port;
			}
		};
		newObjTxt = "Out Feature";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("OUTABSTRACTFEATURE"),
				PackageImageRegistry
						.getImageDescriptor("OUTABSTRACTFEATURE_LARGE"));
		abstractFeatureStack.add(objectTool);

		entries.add(abstractFeatureStack);

		// Parameter

		PaletteStack parameterStack = new PaletteStack("Parameter",
				"Parameter Selection",
				PackageImageRegistry.getImageDescriptor("PARAMETER"));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getParameter(), "default") {
			public EObject getNewModelObject() {
				Parameter port = (Parameter) super.getNewModelObject();
				port.setDirection(PortDirection.IN_LITERAL);
				return port;
			}
		};
		newObjTxt = "In Parameter";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("INPARAMETER"),
				PackageImageRegistry.getImageDescriptor("INPARAMETER_LARGE"));
		parameterStack.add(objectTool);
		parameterStack.setActiveEntry(objectTool);

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getParameter(), "default") {
			public EObject getNewModelObject() {
				Parameter port = (Parameter) super.getNewModelObject();
				port.setDirection(PortDirection.OUT_LITERAL);
				return port;
			}
		};
		newObjTxt = "Out Parameter";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("OUTPARAMETER"),
				PackageImageRegistry.getImageDescriptor("OUTPARAMETER_LARGE"));
		parameterStack.add(objectTool);

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_FeaturesPackage.eINSTANCE.getParameter(), "default") {
			public EObject getNewModelObject() {
				Parameter port = (Parameter) super.getNewModelObject();
				port.setDirection(PortDirection.INOUT_LITERAL);
				return port;
			}
		};
		newObjTxt = "InOut Parameter";
		objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
				factory,
				PackageImageRegistry.getImageDescriptor("INOUTPARAMETER"),
				PackageImageRegistry.getImageDescriptor("INOUTPARAMETER_LARGE"));
		parameterStack.add(objectTool);

		entries.add(parameterStack);

		featuresDrawer.addAll(entries);
		getRoot().add(featuresDrawer);
	}

}
