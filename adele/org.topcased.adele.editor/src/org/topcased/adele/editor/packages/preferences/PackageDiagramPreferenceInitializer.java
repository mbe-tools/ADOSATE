/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	Arnaud Schach, arnaud.schach.com
 ******************************************************************************/
package org.topcased.adele.editor.packages.preferences;

import java.util.HashMap;
import java.util.Iterator;

import org.eclipse.core.runtime.Preferences;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.topcased.adele.editor.ADELEPlugin;

/**
 * Initialize the preferences of Package diagram
 * 
 * @generated
 */
public class PackageDiagramPreferenceInitializer extends
		AbstractPreferenceInitializer {
	private Preferences ps = ADELEPlugin.getDefault().getPluginPreferences();

	/**
	 * Initialize the default value of the preference's properties 
	 * @generated
	 */
	public void initializeDefaultPreferences() {
		
		HashMap<String, String> res = (new PackageDiagramTopcasedPreferenceInitializer()).getDefaultPreference();
		Iterator<String> keyIt = res.keySet().iterator();
		String key;
		while (keyIt.hasNext()) {
			key = keyIt.next();
			ps.setDefault(key,res.get(key));
		}
	}
}
