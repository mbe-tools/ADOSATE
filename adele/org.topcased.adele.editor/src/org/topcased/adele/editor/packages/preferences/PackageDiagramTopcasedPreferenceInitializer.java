/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.packages.preferences;

import java.util.HashMap;

import org.eclipse.jface.resource.StringConverter;
import org.topcased.modeler.preferences.ITopcasedPreferenceInitializer;

/**
 * Initialize the preferences of Package diagram
 * 
 * @generated
 */
public class PackageDiagramTopcasedPreferenceInitializer implements
		ITopcasedPreferenceInitializer {

	/** 
	 * @see org.topcased.modeler.preferences.ITopcasedPreferenceInitializer#getDefaultPreference()
	 *	@generated
	 */
	public HashMap<String, String> getDefaultPreference() {
		HashMap<String, String> defaultPackagePreference = new HashMap<String, String>();
		// Initialize the default value of the DATA_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.DATA_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the DATA_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.DATA_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATA_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.DATA_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the SUBPROGRAM_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SUBPROGRAM_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the SUBPROGRAM_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SUBPROGRAM_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAM_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.SUBPROGRAM_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the SUBPROGRAMGROUP_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SUBPROGRAMGROUP_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the SUBPROGRAMGROUP_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SUBPROGRAMGROUP_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUP_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.SUBPROGRAMGROUP_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the THREAD_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.THREAD_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the THREAD_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.THREAD_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the THREAD_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.THREAD_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the THREADGROUP_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.THREADGROUP_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the THREADGROUP_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.THREADGROUP_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the THREADGROUP_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.THREADGROUP_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the PROCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.PROCESS_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the PROCESS_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.PROCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PROCESS_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.PROCESS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the PROCESSOR_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.PROCESSOR_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the PROCESSOR_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.PROCESSOR_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PROCESSOR_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.PROCESSOR_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the VIRTUALPROCESSOR_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.VIRTUALPROCESSOR_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the VIRTUALPROCESSOR_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.VIRTUALPROCESSOR_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the VIRTUALPROCESSOR_DEFAULT_FONT property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.VIRTUALPROCESSOR_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the MEMORY_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.MEMORY_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the MEMORY_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.MEMORY_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the MEMORY_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.MEMORY_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the BUS_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.BUS_DEFAULT_BACKGROUND_COLOR,
				"255,255,255");

		// Initialize the default value of the BUS_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.BUS_DEFAULT_FOREGROUND_COLOR,
				"0,0,0");

		// Initialize the default value of the BUS_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.BUS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the VIRTUALBUS_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.VIRTUALBUS_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the VIRTUALBUS_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.VIRTUALBUS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the VIRTUALBUS_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.VIRTUALBUS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the DEVICE_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.DEVICE_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the DEVICE_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.DEVICE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DEVICE_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.DEVICE_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the SYSTEM_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SYSTEM_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the SYSTEM_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SYSTEM_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SYSTEM_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.SYSTEM_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the PACKAGE_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.PACKAGE_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the PACKAGE_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.PACKAGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PACKAGE_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.PACKAGE_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the ABSTRACT_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.ABSTRACT_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the ABSTRACT_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.ABSTRACT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACT_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.ABSTRACT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the EVENTPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.EVENTPORT_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORT_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.EVENTPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORT_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.EVENTPORT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the DATAPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.DATAPORT_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAPORT_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.DATAPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAPORT_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.DATAPORT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the FEATUREGROUP_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the FEATUREGROUP_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the FEATUREGROUP_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the DATAACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.DATAACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.DATAACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAACCESS_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.DATAACCESS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_FONT property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_FONT property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the BUSACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.BUSACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BUSACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.BUSACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BUSACCESS_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.BUSACCESS_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the PARAMETER_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.PARAMETER_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PARAMETER_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.PARAMETER_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PARAMETER_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.PARAMETER_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR property 
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_FONT property
		defaultPackagePreference.put(
				PackageDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_FONT,
				StringConverter.asFontData("Tahoma-regular-8").toString());

		// Initialize the default value of the EVENTPORTCONNECTION_EDGE_DEFAULT_FONT property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.EVENTPORTCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the EVENTPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.EVENTPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORTCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.EVENTPORTCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the DATAPORTCONNECTION_EDGE_DEFAULT_FONT property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.DATAPORTCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the DATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.DATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAPORTCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.DATAPORTCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FONT property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.EVENTDATAPORTCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORTCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.EVENTDATAPORTCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FONT property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FONT property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the BUSACCESSCONNECTION_EDGE_DEFAULT_FONT property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.BUSACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the BUSACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.BUSACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the BUSACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.BUSACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the DATAACCESSCONNECTION_EDGE_DEFAULT_FONT property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.DATAACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the DATAACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.DATAACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.DATAACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the PARAMETERCONNECTION_EDGE_DEFAULT_FONT property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.PARAMETERCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the PARAMETERCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.PARAMETERCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PARAMETERCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.PARAMETERCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FONT property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_ROUTER property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_FONT property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultPackagePreference
				.put(PackageDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		return defaultPackagePreference;
	}
}
