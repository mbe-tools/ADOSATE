/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.packages.commands;

import org.eclipse.gef.EditPart;
import org.topcased.adele.editor.system.commands.FeatureGroupConnectionEdgeCreationCommand;
import org.topcased.adele.editor.utils.commands.RestoreConnectionCommand;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;

/**
 * FeatureGroup restore connection command
 *
 * @generated NOT
 */
public class FeatureGroupRestoreConnectionCommand extends RestoreConnectionCommand {
	/**
	 * @param part the EditPart that is restored
	 * @generated
	 */
	public FeatureGroupRestoreConnectionCommand(EditPart part) {
		super(part);
	}

	@Override
	protected void createRelationCommand(GraphElement edge,
			GraphElement srcElt, GraphElement targetElt) {
		FeatureGroupConnectionEdgeCreationCommand cmd = new FeatureGroupConnectionEdgeCreationCommand(
				getEditDomain(), (GraphEdge) edge, srcElt, false);
		cmd.setTarget(targetElt);
		add(cmd);
	}
}