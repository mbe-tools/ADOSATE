/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.subprogramgroup.preferences;

/**
 * A set of constant that are used in the diagram graphical preferences
 * 
 * @generated
 */
public interface SubprogramGroupDiagramPreferenceConstants {
	/**
	 * The key used to install a <i>Subprogram Default Background Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAM_DEFAULT_BACKGROUND_COLOR = "Subprogram Default Background Color";

	/**
	 * The key used to install a <i>Subprogram Default Foreground Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAM_DEFAULT_FOREGROUND_COLOR = "Subprogram Default Foreground Color";

	/**
	 * The key used to install a <i>Subprogram Default Font</i> Preference.
	 * @generated
	 */
	String SUBPROGRAM_DEFAULT_FONT = "Subprogram Default Font";

	/**
	 * The key used to install a <i>SubprogramGroup Default Background Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUP_DEFAULT_BACKGROUND_COLOR = "SubprogramGroup Default Background Color";

	/**
	 * The key used to install a <i>SubprogramGroup Default Foreground Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUP_DEFAULT_FOREGROUND_COLOR = "SubprogramGroup Default Foreground Color";

	/**
	 * The key used to install a <i>SubprogramGroup Default Font</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUP_DEFAULT_FONT = "SubprogramGroup Default Font";

	/**
	 * The key used to install a <i>EventPort Default Background Color</i> Preference.
	 * @generated
	 */
	String EVENTPORT_DEFAULT_BACKGROUND_COLOR = "EventPort Default Background Color";

	/**
	 * The key used to install a <i>EventPort Default Foreground Color</i> Preference.
	 * @generated
	 */
	String EVENTPORT_DEFAULT_FOREGROUND_COLOR = "EventPort Default Foreground Color";

	/**
	 * The key used to install a <i>EventPort Default Font</i> Preference.
	 * @generated
	 */
	String EVENTPORT_DEFAULT_FONT = "EventPort Default Font";

	/**
	 * The key used to install a <i>EventDataPort Default Background Color</i> Preference.
	 * @generated
	 */
	String EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR = "EventDataPort Default Background Color";

	/**
	 * The key used to install a <i>EventDataPort Default Foreground Color</i> Preference.
	 * @generated
	 */
	String EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR = "EventDataPort Default Foreground Color";

	/**
	 * The key used to install a <i>EventDataPort Default Font</i> Preference.
	 * @generated
	 */
	String EVENTDATAPORT_DEFAULT_FONT = "EventDataPort Default Font";

	/**
	 * The key used to install a <i>FeatureGroup Default Background Color</i> Preference.
	 * @generated
	 */
	String FEATUREGROUP_DEFAULT_BACKGROUND_COLOR = "FeatureGroup Default Background Color";

	/**
	 * The key used to install a <i>FeatureGroup Default Foreground Color</i> Preference.
	 * @generated
	 */
	String FEATUREGROUP_DEFAULT_FOREGROUND_COLOR = "FeatureGroup Default Foreground Color";

	/**
	 * The key used to install a <i>FeatureGroup Default Font</i> Preference.
	 * @generated
	 */
	String FEATUREGROUP_DEFAULT_FONT = "FeatureGroup Default Font";

	/**
	 * The key used to install a <i>DataAccess Default Background Color</i> Preference.
	 * @generated
	 */
	String DATAACCESS_DEFAULT_BACKGROUND_COLOR = "DataAccess Default Background Color";

	/**
	 * The key used to install a <i>DataAccess Default Foreground Color</i> Preference.
	 * @generated
	 */
	String DATAACCESS_DEFAULT_FOREGROUND_COLOR = "DataAccess Default Foreground Color";

	/**
	 * The key used to install a <i>DataAccess Default Font</i> Preference.
	 * @generated
	 */
	String DATAACCESS_DEFAULT_FONT = "DataAccess Default Font";

	/**
	 * The key used to install a <i>SubprogramAccess Default Background Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR = "SubprogramAccess Default Background Color";

	/**
	 * The key used to install a <i>SubprogramAccess Default Foreground Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR = "SubprogramAccess Default Foreground Color";

	/**
	 * The key used to install a <i>SubprogramAccess Default Font</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMACCESS_DEFAULT_FONT = "SubprogramAccess Default Font";

	/**
	 * The key used to install a <i>SubprogramGroupAccess Default Background Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR = "SubprogramGroupAccess Default Background Color";

	/**
	 * The key used to install a <i>SubprogramGroupAccess Default Foreground Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR = "SubprogramGroupAccess Default Foreground Color";

	/**
	 * The key used to install a <i>SubprogramGroupAccess Default Font</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESS_DEFAULT_FONT = "SubprogramGroupAccess Default Font";

	/**
	 * The key used to install a <i>Parameter Default Background Color</i> Preference.
	 * @generated
	 */
	String PARAMETER_DEFAULT_BACKGROUND_COLOR = "Parameter Default Background Color";

	/**
	 * The key used to install a <i>Parameter Default Foreground Color</i> Preference.
	 * @generated
	 */
	String PARAMETER_DEFAULT_FOREGROUND_COLOR = "Parameter Default Foreground Color";

	/**
	 * The key used to install a <i>Parameter Default Font</i> Preference.
	 * @generated
	 */
	String PARAMETER_DEFAULT_FONT = "Parameter Default Font";

	/**
	 * The key used to install a <i>AbstractFeature Default Background Color</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR = "AbstractFeature Default Background Color";

	/**
	 * The key used to install a <i>AbstractFeature Default Foreground Color</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR = "AbstractFeature Default Foreground Color";

	/**
	 * The key used to install a <i>AbstractFeature Default Font</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURE_DEFAULT_FONT = "AbstractFeature Default Font";

	/**
	 * The key used to install a <i>SubprogramAccessConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FONT = "SubprogramAccessConnection Edge Default Font";

	/**
	 * The key used to install a <i>SubprogramAccessConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "SubprogramAccessConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>SubprogramAccessConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_ROUTER = "SubprogramAccessConnection Edge Default Router";

	/**
	 * The key used to install a <i>SubprogramGroupAccessConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FONT = "SubprogramGroupAccessConnection Edge Default Font";

	/**
	 * The key used to install a <i>SubprogramGroupAccessConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "SubprogramGroupAccessConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>SubprogramGroupAccessConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_ROUTER = "SubprogramGroupAccessConnection Edge Default Router";

	/**
	 * The key used to install a <i>AbstractFeatureConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FONT = "AbstractFeatureConnection Edge Default Font";

	/**
	 * The key used to install a <i>AbstractFeatureConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "AbstractFeatureConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>AbstractFeatureConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_ROUTER = "AbstractFeatureConnection Edge Default Router";

	/**
	 * The key used to install a <i>ParameterConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String PARAMETERCONNECTION_EDGE_DEFAULT_FONT = "ParameterConnection Edge Default Font";

	/**
	 * The key used to install a <i>ParameterConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String PARAMETERCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "ParameterConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>ParameterConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String PARAMETERCONNECTION_EDGE_DEFAULT_ROUTER = "ParameterConnection Edge Default Router";

	/**
	 * The key used to install a <i>FeatureGroupConnection Edge Default Font</i> Preference.
	 * @generated
	 */
	String FEATUREGROUPCONNECTION_EDGE_DEFAULT_FONT = "FeatureGroupConnection Edge Default Font";

	/**
	 * The key used to install a <i>FeatureGroupConnection Edge Default Foreground Color</i> Preference.
	 * @generated
	 */
	String FEATUREGROUPCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR = "FeatureGroupConnection Edge Default Foreground Color";

	/**
	 * The key used to install a <i>FeatureGroupConnection Edge Default Router</i> Preference.
	 * @generated
	 */
	String FEATUREGROUPCONNECTION_EDGE_DEFAULT_ROUTER = "FeatureGroupConnection Edge Default Router";

}