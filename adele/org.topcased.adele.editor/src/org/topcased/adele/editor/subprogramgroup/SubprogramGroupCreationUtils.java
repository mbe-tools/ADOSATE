/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.subprogramgroup;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch;
import org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch;
import org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch;
import org.topcased.adele.model.KernelSpices.util.KernelSpicesSwitch;
import org.topcased.adele.model.ObjectDescriptionModel.util.ObjectDescriptionModelSwitch;
import org.topcased.adele.model.ba_components.util.Ba_componentsSwitch;
import org.topcased.adele.model.ba_features.util.Ba_featuresSwitch;
import org.topcased.adele.model.ba_relations.util.Ba_relationsSwitch;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.editor.AbstractCreationUtils;
import org.topcased.modeler.graphconf.DiagramGraphConf;

/**
 * This utility class allows to create a GraphElement associated with a Model Object
 *
 * @generated
 */
public class SubprogramGroupCreationUtils extends AbstractCreationUtils {

	/**
	 * Constructor
	 *
	 * @param diagramConf the Diagram Graphical Configuration
	 * @generated
	 */
	public SubprogramGroupCreationUtils(DiagramGraphConf diagramConf) {
		super(diagramConf);
	}

	/**
	 * @generated
	 */
	private class GraphicADELE_ComponentsSwitch extends ADELE_ComponentsSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicADELE_ComponentsSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#caseSubprogram(org.topcased.adele.model.ADELE_Components.Subprogram)
		 * @generated
		 */
		public Object caseSubprogram(
				org.topcased.adele.model.ADELE_Components.Subprogram object) {
			if ("default".equals(presentation)) {
				return createGraphElementSubprogram(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Components.util.ADELE_ComponentsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicADELE_FeaturesSwitch extends ADELE_FeaturesSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicADELE_FeaturesSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseEventPort(org.topcased.adele.model.ADELE_Features.EventPort)
		 * @generated
		 */
		public Object caseEventPort(
				org.topcased.adele.model.ADELE_Features.EventPort object) {
			if ("default".equals(presentation)) {
				return createGraphElementEventPort(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseEventDataPort(org.topcased.adele.model.ADELE_Features.EventDataPort)
		 * @generated
		 */
		public Object caseEventDataPort(
				org.topcased.adele.model.ADELE_Features.EventDataPort object) {
			if ("default".equals(presentation)) {
				return createGraphElementEventDataPort(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseFeatureGroup(org.topcased.adele.model.ADELE_Features.FeatureGroup)
		 * @generated
		 */
		public Object caseFeatureGroup(
				org.topcased.adele.model.ADELE_Features.FeatureGroup object) {
			if ("default".equals(presentation)) {
				return createGraphElementFeatureGroup(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseDataAccess(org.topcased.adele.model.ADELE_Features.DataAccess)
		 * @generated
		 */
		public Object caseDataAccess(
				org.topcased.adele.model.ADELE_Features.DataAccess object) {
			if ("default".equals(presentation)) {
				return createGraphElementDataAccess(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseSubprogramAccess(org.topcased.adele.model.ADELE_Features.SubprogramAccess)
		 * @generated
		 */
		public Object caseSubprogramAccess(
				org.topcased.adele.model.ADELE_Features.SubprogramAccess object) {
			if ("default".equals(presentation)) {
				return createGraphElementSubprogramAccess(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseSubprogramGroupAccess(org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess)
		 * @generated
		 */
		public Object caseSubprogramGroupAccess(
				org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess object) {
			if ("default".equals(presentation)) {
				return createGraphElementSubprogramGroupAccess(object,
						presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseParameter(org.topcased.adele.model.ADELE_Features.Parameter)
		 * @generated
		 */
		public Object caseParameter(
				org.topcased.adele.model.ADELE_Features.Parameter object) {
			if ("default".equals(presentation)) {
				return createGraphElementParameter(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#caseAbstractFeature(org.topcased.adele.model.ADELE_Features.AbstractFeature)
		 * @generated
		 */
		public Object caseAbstractFeature(
				org.topcased.adele.model.ADELE_Features.AbstractFeature object) {
			if ("default".equals(presentation)) {
				return createGraphElementAbstractFeature(object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Features.util.ADELE_FeaturesSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicADELE_RelationsSwitch extends ADELE_RelationsSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicADELE_RelationsSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseSubprogramAccessConnection(org.topcased.adele.model.ADELE_Relations.SubprogramAccessConnection)
		 * @generated
		 */
		public Object caseSubprogramAccessConnection(
				org.topcased.adele.model.ADELE_Relations.SubprogramAccessConnection object) {
			if ("default".equals(presentation)) {
				return createGraphElementSubprogramAccessConnection(object,
						presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseSubprogramGroupAccessConnection(org.topcased.adele.model.ADELE_Relations.SubprogramGroupAccessConnection)
		 * @generated
		 */
		public Object caseSubprogramGroupAccessConnection(
				org.topcased.adele.model.ADELE_Relations.SubprogramGroupAccessConnection object) {
			if ("default".equals(presentation)) {
				return createGraphElementSubprogramGroupAccessConnection(
						object, presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseAbstractFeatureConnection(org.topcased.adele.model.ADELE_Relations.AbstractFeatureConnection)
		 * @generated
		 */
		public Object caseAbstractFeatureConnection(
				org.topcased.adele.model.ADELE_Relations.AbstractFeatureConnection object) {
			if ("default".equals(presentation)) {
				return createGraphElementAbstractFeatureConnection(object,
						presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseParameterConnection(org.topcased.adele.model.ADELE_Relations.ParameterConnection)
		 * @generated
		 */
		public Object caseParameterConnection(
				org.topcased.adele.model.ADELE_Relations.ParameterConnection object) {
			if ("default".equals(presentation)) {
				return createGraphElementParameterConnection(object,
						presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#caseFeatureGroupConnection(org.topcased.adele.model.ADELE_Relations.FeatureGroupConnection)
		 * @generated
		 */
		public Object caseFeatureGroupConnection(
				org.topcased.adele.model.ADELE_Relations.FeatureGroupConnection object) {
			if ("default".equals(presentation)) {
				return createGraphElementFeatureGroupConnection(object,
						presentation);
			}
			return null;
		}

		/**
		 * @see org.topcased.adele.model.ADELE_Relations.util.ADELE_RelationsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicBa_componentsSwitch extends Ba_componentsSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicBa_componentsSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ba_components.util.Ba_componentsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicBa_featuresSwitch extends Ba_featuresSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicBa_featuresSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ba_features.util.Ba_featuresSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicBa_relationsSwitch extends Ba_relationsSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicBa_relationsSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ba_relations.util.Ba_relationsSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicKernelSpicesSwitch extends KernelSpicesSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicKernelSpicesSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.KernelSpices.util.KernelSpicesSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @generated
	 */
	private class GraphicObjectDescriptionModelSwitch extends
			ObjectDescriptionModelSwitch {
		/**
		 * The presentation of the graphical element
		 *
		 * @generated
		 */
		private String presentation;

		/**
		 * Constructor
		 *
		 * @param presentation the presentation of the graphical element
		 * @generated
		 */
		public GraphicObjectDescriptionModelSwitch(String presentation) {
			this.presentation = presentation;
		}

		/**
		 * @see org.topcased.adele.model.ObjectDescriptionModel.util.ObjectDescriptionModelSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
		 * @generated
		 */
		public Object defaultCase(EObject object) {
			return null;
		}
	}

	/**
	 * @see org.topcased.modeler.editor.ICreationUtils#createGraphElement(org.eclipse.emf.ecore.EObject, java.lang.String)
	 * @generated
	 */
	public GraphElement createGraphElement(EObject obj, String presentation) {
		Object graphElt = null;

		if ("http://ADELE_Components".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicADELE_ComponentsSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ADELE_Features".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicADELE_FeaturesSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ADELE_Relations".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicADELE_RelationsSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ba_components/1.0".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicBa_componentsSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ba_features/1.0".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicBa_featuresSwitch(presentation).doSwitch(obj);
		}
		if ("http://ba_relations/1.0".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicBa_relationsSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://KernelSpices".equals(obj.eClass().getEPackage().getNsURI())) {
			graphElt = new GraphicKernelSpicesSwitch(presentation)
					.doSwitch(obj);
		}
		if ("http://ObjectDescriptionModel".equals(obj.eClass().getEPackage()
				.getNsURI())) {
			graphElt = new GraphicObjectDescriptionModelSwitch(presentation)
					.doSwitch(obj);
		}

		return (GraphElement) graphElt;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementSubprogram(
			org.topcased.adele.model.ADELE_Components.Subprogram element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementEventPort(
			org.topcased.adele.model.ADELE_Features.EventPort element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementEventDataPort(
			org.topcased.adele.model.ADELE_Features.EventDataPort element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementFeatureGroup(
			org.topcased.adele.model.ADELE_Features.FeatureGroup element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementDataAccess(
			org.topcased.adele.model.ADELE_Features.DataAccess element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementSubprogramAccess(
			org.topcased.adele.model.ADELE_Features.SubprogramAccess element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementSubprogramGroupAccess(
			org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementParameter(
			org.topcased.adele.model.ADELE_Features.Parameter element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementAbstractFeature(
			org.topcased.adele.model.ADELE_Features.AbstractFeature element,
			String presentation) {
		return createGraphNode(element, presentation);
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementSubprogramAccessConnection(
			org.topcased.adele.model.ADELE_Relations.SubprogramAccessConnection element,
			String presentation) {
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementSubprogramGroupAccessConnection(
			org.topcased.adele.model.ADELE_Relations.SubprogramGroupAccessConnection element,
			String presentation) {
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementAbstractFeatureConnection(
			org.topcased.adele.model.ADELE_Relations.AbstractFeatureConnection element,
			String presentation) {
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementParameterConnection(
			org.topcased.adele.model.ADELE_Relations.ParameterConnection element,
			String presentation) {
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * @param element the model element
	 * @param presentation the presentation of the graphical element
	 * @return the complete GraphElement
	 * @generated
	 */
	protected GraphElement createGraphElementFeatureGroupConnection(
			org.topcased.adele.model.ADELE_Relations.FeatureGroupConnection element,
			String presentation) {
		GraphEdge graphEdge = createGraphEdge(element, presentation);
		return graphEdge;
	}

	/**
	 * Create the ModelObject with its initial children
	 * 
	 * @param obj the model object
	 * @return the model object with its children
	 * @generated
	 */
	public EObject createModelObject(EObject obj) {
		return obj;
	}

	/**
	 * Get the preference store associated with the current editor.
	 * 
	 * @return IPreferenceStore
	 * @generated
	 */
	private IPreferenceStore getPreferenceStore() {
		IEditorInput editorInput = ADELEPlugin.getActivePage()
				.getActiveEditor().getEditorInput();
		if (editorInput instanceof IFileEditorInput) {
			IProject project = ((IFileEditorInput) editorInput).getFile()
					.getProject();
			Preferences root = Platform.getPreferencesService().getRootNode();
			try {
				if (root.node(ProjectScope.SCOPE).node(project.getName())
						.nodeExists(ADELEPlugin.getId())) {
					return new ScopedPreferenceStore(new ProjectScope(project),
							ADELEPlugin.getId());
				}
			} catch (BackingStoreException e) {
				e.printStackTrace();
			}
		}
		return ADELEPlugin.getDefault().getPreferenceStore();
	}
}
