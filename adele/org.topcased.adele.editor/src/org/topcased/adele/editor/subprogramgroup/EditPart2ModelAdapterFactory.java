/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.subprogramgroup;

/**
 * @generated
 */
public class EditPart2ModelAdapterFactory extends
		org.topcased.modeler.editor.EditPart2ModelAdapterFactory {

	/**
	 * Constructor
	 *
	 * @param adaptableClass
	 * @param adapterType
	 * @generated
	 */
	public EditPart2ModelAdapterFactory(Class adaptableClass, Class adapterType) {
		super(adaptableClass, adapterType);
	}

}