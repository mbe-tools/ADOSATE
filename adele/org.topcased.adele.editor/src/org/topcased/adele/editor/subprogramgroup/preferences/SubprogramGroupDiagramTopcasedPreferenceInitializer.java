/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.subprogramgroup.preferences;

import java.util.HashMap;

import org.eclipse.jface.resource.StringConverter;
import org.topcased.modeler.preferences.ITopcasedPreferenceInitializer;

/**
 * Initialize the preferences of SubprogramGroup diagram
 * 
 * @generated
 */
public class SubprogramGroupDiagramTopcasedPreferenceInitializer implements
		ITopcasedPreferenceInitializer {

	/** 
	 * @see org.topcased.modeler.preferences.ITopcasedPreferenceInitializer#getDefaultPreference()
	 *	@generated
	 */
	public HashMap<String, String> getDefaultPreference() {
		HashMap<String, String> defaultSubprogramGroupPreference = new HashMap<String, String>();
		// Initialize the default value of the SUBPROGRAM_DEFAULT_BACKGROUND_COLOR property 
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAM_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the SUBPROGRAM_DEFAULT_FOREGROUND_COLOR property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAM_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAM_DEFAULT_FONT property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAM_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMGROUP_DEFAULT_BACKGROUND_COLOR property 
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAMGROUP_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUP_DEFAULT_FOREGROUND_COLOR property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAMGROUP_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUP_DEFAULT_FONT property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAMGROUP_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the EVENTPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.EVENTPORT_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORT_DEFAULT_FOREGROUND_COLOR property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.EVENTPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTPORT_DEFAULT_FONT property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.EVENTPORT_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR property 
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the EVENTDATAPORT_DEFAULT_FONT property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.EVENTDATAPORT_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the FEATUREGROUP_DEFAULT_BACKGROUND_COLOR property 
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_BACKGROUND_COLOR,
						"255,255,255");

		// Initialize the default value of the FEATUREGROUP_DEFAULT_FOREGROUND_COLOR property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the FEATUREGROUP_DEFAULT_FONT property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.FEATUREGROUP_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the DATAACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.DATAACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.DATAACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the DATAACCESS_DEFAULT_FONT property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.DATAACCESS_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMACCESS_DEFAULT_FONT property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAMACCESS_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR property 
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESS_DEFAULT_FONT property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAMGROUPACCESS_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the PARAMETER_DEFAULT_BACKGROUND_COLOR property 
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.PARAMETER_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PARAMETER_DEFAULT_FOREGROUND_COLOR property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.PARAMETER_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PARAMETER_DEFAULT_FONT property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.PARAMETER_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR property 
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_BACKGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURE_DEFAULT_FONT property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.ABSTRACTFEATURE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FONT property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAMACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FONT property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.SUBPROGRAMGROUPACCESSCONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FONT property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_ROUTER property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.ABSTRACTFEATURECONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the PARAMETERCONNECTION_EDGE_DEFAULT_FONT property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.PARAMETERCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the PARAMETERCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.PARAMETERCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the PARAMETERCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.PARAMETERCONNECTION_EDGE_DEFAULT_ROUTER,
						"ObliqueRouter");

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_FONT property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_FONT,
						StringConverter.asFontData("Tahoma-regular-8")
								.toString());

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_FOREGROUND_COLOR,
						"0,0,0");

		// Initialize the default value of the FEATUREGROUPCONNECTION_EDGE_DEFAULT_ROUTER property
		defaultSubprogramGroupPreference
				.put(SubprogramGroupDiagramPreferenceConstants.FEATUREGROUPCONNECTION_EDGE_DEFAULT_ROUTER,
						"RectilinearRouter");

		return defaultSubprogramGroupPreference;
	}
}
