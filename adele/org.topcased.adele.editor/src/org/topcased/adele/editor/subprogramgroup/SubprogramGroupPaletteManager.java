/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.subprogramgroup;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.palette.CreationToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteStack;
import org.eclipse.gef.requests.CreationFactory;
import org.topcased.adele.editor.ADELEPlugin;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;
import org.topcased.adele.model.ADELE_Features.AbstractFeature;
import org.topcased.adele.model.ADELE_Features.AccessDirection;
import org.topcased.adele.model.ADELE_Features.DataAccess;
import org.topcased.adele.model.ADELE_Features.EventDataPort;
import org.topcased.adele.model.ADELE_Features.EventPort;
import org.topcased.adele.model.ADELE_Features.Parameter;
import org.topcased.adele.model.ADELE_Features.PortDirection;
import org.topcased.adele.model.ADELE_Features.SubprogramAccess;
import org.topcased.adele.model.ADELE_Features.SubprogramGroupAccess;
import org.topcased.adele.model.ADELE_Relations.ADELE_RelationsPackage;
import org.topcased.modeler.editor.GraphElementCreationFactory;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.palette.ModelerConnectionCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerCreationToolEntry;
import org.topcased.modeler.editor.palette.ModelerPaletteManager;

/**
 * Generated Palette Manager
 *
 * @generated
 */
public class SubprogramGroupPaletteManager extends ModelerPaletteManager {
	// declare all the palette categories of the diagram
	/**
	 * @generated
	 */
	private PaletteDrawer componentsDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer featuresDrawer;
	/**
	 * @generated
	 */
	private PaletteDrawer connectionsDrawer;

	/**
	 * @generated
	 */
	private ICreationUtils creationUtils;

	/**
	 * The Constructor
	 *
	 * @param utils the creation utils for the tools of the palette 
	 * @generated
	 */
	public SubprogramGroupPaletteManager(ICreationUtils utils) {
		super();
		this.creationUtils = utils;
	}

	/**
	 * Creates the main categories of the palette
	 *
	 * @generated
	 */
	protected void createCategories() {
		createComponentsDrawer();
		createFeaturesDrawer();
		createConnectionsDrawer();
	}

	/**
	 * Updates the main categories of the palette
	 *
	 * @generated
	 */
	protected void updateCategories() {
		// deletion of the existing categories and creation of the updated categories

		getRoot().remove(componentsDrawer);
		createComponentsDrawer();

		getRoot().remove(featuresDrawer);
		createFeaturesDrawer();

		getRoot().remove(connectionsDrawer);
		createConnectionsDrawer();
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated
	 */
	private void createComponentsDrawer() {
		componentsDrawer = new PaletteDrawer("Components", null);
		List<PaletteEntry> entries = new ArrayList<PaletteEntry>();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_ComponentsPackage.eINSTANCE.getSubprogram(), "default");
		entries.add(new ModelerCreationToolEntry("Subprogram", "Subprogram",
				factory, SubprogramGroupImageRegistry
						.getImageDescriptor("SUBPROGRAM"),
				SubprogramGroupImageRegistry
						.getImageDescriptor("SUBPROGRAM_LARGE")));

		componentsDrawer.addAll(entries);
		getRoot().add(componentsDrawer);
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated NOT
	 */
	private void createFeaturesDrawer() {
		if ( ADELEPlugin.isSystemActiveEditor() ) {
			featuresDrawer = new PaletteDrawer("Features", null);
			List<PaletteEntry> entries = new ArrayList<PaletteEntry>();
	
			CreationFactory factory;
			String newObjTxt;
			CreationToolEntry objectTool;
	
			// Event Ports
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getEventPort(), "default") {
				public EObject getNewModelObject() {
					EventPort port = (EventPort) super.getNewModelObject();
					port.setDirection(PortDirection.OUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "Out Event Port";
			objectTool = new ModelerCreationToolEntry(
					newObjTxt,
					newObjTxt,
					factory,
					SubprogramGroupImageRegistry.getImageDescriptor("OUTEVENTPORT"),
					SubprogramGroupImageRegistry
							.getImageDescriptor("OUTEVENTPORT_LARGE"));
			entries.add(objectTool);
	
			// Event Data ports
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getEventDataPort(), "default") {
				public EObject getNewModelObject() {
					EventDataPort port = (EventDataPort) super.getNewModelObject();
					port.setDirection(PortDirection.OUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "Out Event Data Port";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					SubprogramGroupImageRegistry
							.getImageDescriptor("OUTEVENTDATAPORT"),
					SubprogramGroupImageRegistry
							.getImageDescriptor("OUTEVENTDATAPORT_LARGE"));
			entries.add(objectTool);
	
			// Feature Group
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getFeatureGroup(), "default");
			entries.add(new ModelerCreationToolEntry("Feature Group",
					"Feature Group", factory, SubprogramGroupImageRegistry
							.getImageDescriptor("FEATUREGROUP"),
					SubprogramGroupImageRegistry
							.getImageDescriptor("FEATUREGROUP_LARGE")));
	
			//	Data access
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getDataAccess(), "default") {
				public EObject getNewModelObject() {
					DataAccess access = (DataAccess) super.getNewModelObject();
					access.setDirection(AccessDirection.REQUIRED_LITERAL);
					return access;
				}
			};
			newObjTxt = "Requires data access";
	
			entries.add(new ModelerCreationToolEntry(newObjTxt, newObjTxt, factory,
					SubprogramGroupImageRegistry
							.getImageDescriptor("REQUIREDDATAACCESS"),
					SubprogramGroupImageRegistry
							.getImageDescriptor("REQUIREDDATAACCESS_LARGE")));
	
			// Subprogram Access
	
			PaletteStack subprogramAccessStack = new PaletteStack(
					"Subprogram Access", "Subprogram Access Selection",
					SubprogramGroupImageRegistry
							.getImageDescriptor("SubprogramAccess"));
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getSubprogramAccess(),
					"default") {
				public EObject getNewModelObject() {
					SubprogramAccess access = (SubprogramAccess) super
							.getNewModelObject();
					access.setDirection(AccessDirection.PROVIDED_LITERAL);
					return access;
				}
			};
			newObjTxt = "Provides subprogram access";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					SubprogramGroupImageRegistry
							.getImageDescriptor("PROVIDEDSUBPROGRAMACCESS"),
					SubprogramGroupImageRegistry
							.getImageDescriptor("PROVIDEDSUBPROGRAMACCESS_LARGE"));
			subprogramAccessStack.add(objectTool);
			subprogramAccessStack.setActiveEntry(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getSubprogramAccess(),
					"default") {
				public EObject getNewModelObject() {
					SubprogramAccess access = (SubprogramAccess) super
							.getNewModelObject();
					access.setDirection(AccessDirection.REQUIRED_LITERAL);
					return access;
				}
			};
			newObjTxt = "Requires subprogram access";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					SubprogramGroupImageRegistry
							.getImageDescriptor("REQUIREDSUBPROGRAMACCESS"),
					SubprogramGroupImageRegistry
							.getImageDescriptor("REQUIREDSUBPROGRAMACCESS_LARGE"));
			subprogramAccessStack.add(objectTool);
	
			entries.add(subprogramAccessStack);
	
			// Subprogram Group Access
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getSubprogramGroupAccess(),
					"default") {
				public EObject getNewModelObject() {
					SubprogramGroupAccess access = (SubprogramGroupAccess) super
							.getNewModelObject();
					access.setDirection(AccessDirection.REQUIRED_LITERAL);
					return access;
				}
			};
			newObjTxt = "Requires subprogram group access";
	
			entries.add(new ModelerCreationToolEntry(
					newObjTxt,
					newObjTxt,
					factory,
					SubprogramGroupImageRegistry
							.getImageDescriptor("REQUIREDSUBPROGRAMGROUPACCESS"),
					SubprogramGroupImageRegistry
							.getImageDescriptor("REQUIREDSUBPROGRAMGROUPACCESS_LARGE")));
	
			// Abstract Feature
	
			PaletteStack abstractFeatureStack = new PaletteStack("Feature",
					"Feature Selection",
					SubprogramGroupImageRegistry
							.getImageDescriptor("ABSTRACTFEATURE"));
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getAbstractFeature(), "default") {
				public EObject getNewModelObject() {
					AbstractFeature port = (AbstractFeature) super
							.getNewModelObject();
					//port.setDirection(AbstractFeatureDirection.NONE);
					return port;
				}
			};
			newObjTxt = "Feature";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					SubprogramGroupImageRegistry
							.getImageDescriptor("ABSTRACTFEATURE"),
					SubprogramGroupImageRegistry
							.getImageDescriptor("ABSTRACTFEATURE_LARGE"));
			abstractFeatureStack.add(objectTool);
			abstractFeatureStack.setActiveEntry(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getAbstractFeature(), "default") {
				public EObject getNewModelObject() {
					AbstractFeature port = (AbstractFeature) super
							.getNewModelObject();
					//port.setDirection(AbstractFeatureDirection.IN);
					return port;
				}
			};
			newObjTxt = "In Feature";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					SubprogramGroupImageRegistry
							.getImageDescriptor("INABSTRACTFEATURE"),
					SubprogramGroupImageRegistry
							.getImageDescriptor("INABSTRACTFEATURE_LARGE"));
			abstractFeatureStack.add(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getAbstractFeature(), "default") {
				public EObject getNewModelObject() {
					AbstractFeature port = (AbstractFeature) super
							.getNewModelObject();
					//port.setDirection(AbstractFeatureDirection.OUT);
					return port;
				}
			};
			newObjTxt = "Out Feature";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					SubprogramGroupImageRegistry
							.getImageDescriptor("OUTABSTRACTFEATURE"),
					SubprogramGroupImageRegistry
							.getImageDescriptor("OUTABSTRACTFEATURE_LARGE"));
			abstractFeatureStack.add(objectTool);
	
			entries.add(abstractFeatureStack);
	
			// Parameter
	
			PaletteStack parameterStack = new PaletteStack("Parameter",
					"Parameter Selection",
					SubprogramGroupImageRegistry.getImageDescriptor("PARAMETER"));
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getParameter(), "default") {
				public EObject getNewModelObject() {
					Parameter port = (Parameter) super.getNewModelObject();
					port.setDirection(PortDirection.IN_LITERAL);
					return port;
				}
			};
			newObjTxt = "In Parameter";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					SubprogramGroupImageRegistry.getImageDescriptor("INPARAMETER"),
					SubprogramGroupImageRegistry
							.getImageDescriptor("INPARAMETER_LARGE"));
			parameterStack.add(objectTool);
			parameterStack.setActiveEntry(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getParameter(), "default") {
				public EObject getNewModelObject() {
					Parameter port = (Parameter) super.getNewModelObject();
					port.setDirection(PortDirection.OUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "Out Parameter";
			objectTool = new ModelerCreationToolEntry(
					newObjTxt,
					newObjTxt,
					factory,
					SubprogramGroupImageRegistry.getImageDescriptor("OUTPARAMETER"),
					SubprogramGroupImageRegistry
							.getImageDescriptor("OUTPARAMETER_LARGE"));
			parameterStack.add(objectTool);
	
			factory = new GraphElementCreationFactory(creationUtils,
					ADELE_FeaturesPackage.eINSTANCE.getParameter(), "default") {
				public EObject getNewModelObject() {
					Parameter port = (Parameter) super.getNewModelObject();
					port.setDirection(PortDirection.INOUT_LITERAL);
					return port;
				}
			};
			newObjTxt = "InOut Parameter";
			objectTool = new ModelerCreationToolEntry(newObjTxt, newObjTxt,
					factory,
					SubprogramGroupImageRegistry
							.getImageDescriptor("INOUTPARAMETER"),
					SubprogramGroupImageRegistry
							.getImageDescriptor("INOUTPARAMETER_LARGE"));
			parameterStack.add(objectTool);
	
			entries.add(parameterStack);
	
			featuresDrawer.addAll(entries);
			getRoot().add(featuresDrawer);
		}
	}

	/**
	 * Creates the Palette container containing all the Palette entries for each figure.
	 *
	 * @generated
	 */
	private void createConnectionsDrawer() {
		connectionsDrawer = new PaletteDrawer("Connections", null);
		List<PaletteEntry> entries = new ArrayList<PaletteEntry>();

		CreationFactory factory;

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE
						.getSubprogramAccessConnection(), "default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Subprogram Access Connection", "Subprogram Access Connection",
				factory, SubprogramGroupImageRegistry
						.getImageDescriptor("SUBPROGRAMACCESSCONNECTION"),
				SubprogramGroupImageRegistry
						.getImageDescriptor("SUBPROGRAMACCESSCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE
						.getSubprogramGroupAccessConnection(), "default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Subprogram Group Access Connection",
				"Subprogram Group Access Connection",
				factory,
				SubprogramGroupImageRegistry
						.getImageDescriptor("SUBPROGRAMGROUPACCESSCONNECTION"),
				SubprogramGroupImageRegistry
						.getImageDescriptor("SUBPROGRAMGROUPACCESSCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(
				creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getAbstractFeatureConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Feature Connection", "Feature Connection", factory,
				SubprogramGroupImageRegistry
						.getImageDescriptor("ABSTRACTFEATURECONNECTION"),
				SubprogramGroupImageRegistry
						.getImageDescriptor("ABSTRACTFEATURECONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getParameterConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Parameter Connection", "Parameter Connection", factory,
				SubprogramGroupImageRegistry
						.getImageDescriptor("PARAMETERCONNECTION"),
				SubprogramGroupImageRegistry
						.getImageDescriptor("PARAMETERCONNECTION_LARGE")));

		factory = new GraphElementCreationFactory(creationUtils,
				ADELE_RelationsPackage.eINSTANCE.getFeatureGroupConnection(),
				"default");
		entries.add(new ModelerConnectionCreationToolEntry(
				"Feature Group Connection", "Feature Group Connection",
				factory, SubprogramGroupImageRegistry
						.getImageDescriptor("FEATUREGROUPCONNECTION"),
				SubprogramGroupImageRegistry
						.getImageDescriptor("FEATUREGROUPCONNECTION_LARGE")));

		connectionsDrawer.addAll(entries);
		getRoot().add(connectionsDrawer);
	}

}
