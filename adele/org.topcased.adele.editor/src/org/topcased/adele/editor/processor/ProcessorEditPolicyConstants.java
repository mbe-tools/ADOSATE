/*******************************************************************************
 * Copyright (c) 2009 Ellidiss Technologies
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	Arnaud Schach, arnaud.schach@ellidiss.com
 ******************************************************************************/
package org.topcased.adele.editor.processor;

/**
 * A collection of Roles. Each identifier is used to key the EditPolicy.
 *
 * @generated
 */
public interface ProcessorEditPolicyConstants {

	/**
	 * The key used to install an <i>Memory</i> EditPolicy.
	 * @generated
	 */
	String MEMORY_EDITPOLICY = "Memory EditPolicy";

	/**
	 * The key used to install an <i>Bus</i> EditPolicy.
	 * @generated
	 */
	String BUS_EDITPOLICY = "Bus EditPolicy";

	/**
	 * The key used to install an <i>VirtualProcessor</i> EditPolicy.
	 * @generated
	 */
	String VIRTUALPROCESSOR_EDITPOLICY = "VirtualProcessor EditPolicy";

	/**
	 * The key used to install an <i>VirtualBus</i> EditPolicy.
	 * @generated
	 */
	String VIRTUALBUS_EDITPOLICY = "VirtualBus EditPolicy";

	/**
	 * The key used to install an <i>Abstract</i> EditPolicy.
	 * @generated
	 */
	String ABSTRACT_EDITPOLICY = "Abstract EditPolicy";

	/**
	 * The key used to install an <i>Processor</i> EditPolicy.
	 * @generated
	 */
	String PROCESSOR_EDITPOLICY = "Processor EditPolicy";

	/**
	 * The key used to install an <i>EventPort</i> EditPolicy.
	 * @generated
	 */
	String EVENTPORT_EDITPOLICY = "EventPort EditPolicy";

	/**
	 * The key used to install an <i>DataPort</i> EditPolicy.
	 * @generated
	 */
	String DATAPORT_EDITPOLICY = "DataPort EditPolicy";

	/**
	 * The key used to install an <i>EventDataPort</i> EditPolicy.
	 * @generated
	 */
	String EVENTDATAPORT_EDITPOLICY = "EventDataPort EditPolicy";

	/**
	 * The key used to install an <i>FeatureGroup</i> EditPolicy.
	 * @generated
	 */
	String FEATUREGROUP_EDITPOLICY = "FeatureGroup EditPolicy";

	/**
	 * The key used to install an <i>SubprogramAccess</i> EditPolicy.
	 * @generated
	 */
	String SUBPROGRAMACCESS_EDITPOLICY = "SubprogramAccess EditPolicy";

	/**
	 * The key used to install an <i>SubprogramGroupAccess</i> EditPolicy.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESS_EDITPOLICY = "SubprogramGroupAccess EditPolicy";

	/**
	 * The key used to install an <i>BusAccess</i> EditPolicy.
	 * @generated
	 */
	String BUSACCESS_EDITPOLICY = "BusAccess EditPolicy";

	/**
	 * The key used to install an <i>AbstractFeature</i> EditPolicy.
	 * @generated
	 */
	String ABSTRACTFEATURE_EDITPOLICY = "AbstractFeature EditPolicy";

	/**
	 * The key used to install an <i>BusAccessConnection</i> EditPolicy.
	 * @generated
	 */
	String BUSACCESSCONNECTION_EDITPOLICY = "BusAccessConnection EditPolicy";

	/**
	 * The key used to install an <i>AbstractFeatureConnection</i> EditPolicy.
	 * @generated
	 */
	String ABSTRACTFEATURECONNECTION_EDITPOLICY = "AbstractFeatureConnection EditPolicy";

	/**
	 * The key used to install an <i>EventPortConnection</i> EditPolicy.
	 * @generated
	 */
	String EVENTPORTCONNECTION_EDITPOLICY = "EventPortConnection EditPolicy";

	/**
	 * The key used to install an <i>DataPortConnection</i> EditPolicy.
	 * @generated
	 */
	String DATAPORTCONNECTION_EDITPOLICY = "DataPortConnection EditPolicy";

	/**
	 * The key used to install an <i>EventDataPortConnection</i> EditPolicy.
	 * @generated
	 */
	String EVENTDATAPORTCONNECTION_EDITPOLICY = "EventDataPortConnection EditPolicy";

	/**
	 * The key used to install an <i>SubprogramAccessConnection</i> EditPolicy.
	 * @generated
	 */
	String SUBPROGRAMACCESSCONNECTION_EDITPOLICY = "SubprogramAccessConnection EditPolicy";

	/**
	 * The key used to install an <i>SubprogramGroupAccessConnection</i> EditPolicy.
	 * @generated
	 */
	String SUBPROGRAMGROUPACCESSCONNECTION_EDITPOLICY = "SubprogramGroupAccessConnection EditPolicy";

	/**
	 * The key used to install an <i>FeatureGroupConnection</i> EditPolicy.
	 * @generated
	 */
	String FEATUREGROUPCONNECTION_EDITPOLICY = "FeatureGroupConnection EditPolicy";

}