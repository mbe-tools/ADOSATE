package org.topcased.adele.editor.actions.preconditions;

import java.util.Iterator;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.topcased.adele.model.ADELE.util.ADELEModelUtils;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.modeler.ActionConditionChecker;
import org.topcased.modeler.editor.Modeler;

public class AdeleDeleteEnablementChecker implements ActionConditionChecker {

	@Override
	public boolean checkCondition(	final Action p_actionToCheck,
									final Modeler p_modeler,
									final IStructuredSelection p_selection ) {
		final Iterator<?> elementsIt = p_selection.iterator();
		
		while ( elementsIt.hasNext()  ) {
			final Object element = AdapterFactoryEditingDomain.unwrap( elementsIt.next() );
			final SKHierarchicalObject adeleModelElem;

			if ( element instanceof SKHierarchicalObject ) {
				adeleModelElem = (SKHierarchicalObject) element;
			}
			else if ( element instanceof IAdaptable ) {
				adeleModelElem = (SKHierarchicalObject) ( (IAdaptable) element ).getAdapter( SKHierarchicalObject.class );
			}
			else {
				adeleModelElem = null;
			}
			
			if ( adeleModelElem != null ) {
				return ADELEModelUtils.isOriginalElement( adeleModelElem );
			}
		}
		
		return true;
	}
}
