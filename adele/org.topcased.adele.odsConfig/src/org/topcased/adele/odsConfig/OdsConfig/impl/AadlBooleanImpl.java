/**
 * <copyright>
 * </copyright>
 *
 * $Id: AadlBooleanImpl.java,v 1.2 2011-04-19 14:09:34 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import org.eclipse.emf.ecore.EClass;
import org.topcased.adele.odsConfig.OdsConfig.AadlBoolean;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aadl Boolean</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class AadlBooleanImpl extends PrimitivTypesTypeImpl implements AadlBoolean {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AadlBooleanImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdsConfigPackage.Literals.AADL_BOOLEAN;
	}

} //AadlBooleanImpl
