/**
 * <copyright>
 * </copyright>
 *
 * $Id: DataFileImpl.java,v 1.2 2011-04-19 14:09:34 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.topcased.adele.odsConfig.OdsConfig.DataFile;
import org.topcased.adele.odsConfig.OdsConfig.InternalProcedure;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;
import org.topcased.adele.odsConfig.OdsConfig.Slash;
import org.topcased.adele.odsConfig.OdsConfig.Text;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data File</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.DataFileImpl#getMixed <em>Mixed</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.DataFileImpl#getDataFileGroup <em>Data File Group</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.DataFileImpl#getSlash <em>Slash</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.DataFileImpl#getText <em>Text</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.DataFileImpl#getInternalProcedure <em>Internal Procedure</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DataFileImpl extends EObjectImpl implements DataFile {
	/**
	 * The cached value of the '{@link #getMixed() <em>Mixed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMixed()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap mixed;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataFileImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdsConfigPackage.Literals.DATA_FILE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getMixed() {
		if (mixed == null) {
			mixed = new BasicFeatureMap(this, OdsConfigPackage.DATA_FILE__MIXED);
		}
		return mixed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getDataFileGroup() {
		return (FeatureMap)getMixed().<FeatureMap.Entry>list(OdsConfigPackage.Literals.DATA_FILE__DATA_FILE_GROUP);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Slash> getSlash() {
		return getMixed().list(OdsConfigPackage.Literals.DATA_FILE__SLASH);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Text> getText() {
		return getMixed().list(OdsConfigPackage.Literals.DATA_FILE__TEXT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InternalProcedure> getInternalProcedure() {
		return getMixed().list(OdsConfigPackage.Literals.DATA_FILE__INTERNAL_PROCEDURE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdsConfigPackage.DATA_FILE__MIXED:
				return ((InternalEList<?>)getMixed()).basicRemove(otherEnd, msgs);
			case OdsConfigPackage.DATA_FILE__DATA_FILE_GROUP:
				return ((InternalEList<?>)getDataFileGroup()).basicRemove(otherEnd, msgs);
			case OdsConfigPackage.DATA_FILE__SLASH:
				return ((InternalEList<?>)getSlash()).basicRemove(otherEnd, msgs);
			case OdsConfigPackage.DATA_FILE__TEXT:
				return ((InternalEList<?>)getText()).basicRemove(otherEnd, msgs);
			case OdsConfigPackage.DATA_FILE__INTERNAL_PROCEDURE:
				return ((InternalEList<?>)getInternalProcedure()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdsConfigPackage.DATA_FILE__MIXED:
				if (coreType) return getMixed();
				return ((FeatureMap.Internal)getMixed()).getWrapper();
			case OdsConfigPackage.DATA_FILE__DATA_FILE_GROUP:
				if (coreType) return getDataFileGroup();
				return ((FeatureMap.Internal)getDataFileGroup()).getWrapper();
			case OdsConfigPackage.DATA_FILE__SLASH:
				return getSlash();
			case OdsConfigPackage.DATA_FILE__TEXT:
				return getText();
			case OdsConfigPackage.DATA_FILE__INTERNAL_PROCEDURE:
				return getInternalProcedure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdsConfigPackage.DATA_FILE__MIXED:
				((FeatureMap.Internal)getMixed()).set(newValue);
				return;
			case OdsConfigPackage.DATA_FILE__DATA_FILE_GROUP:
				((FeatureMap.Internal)getDataFileGroup()).set(newValue);
				return;
			case OdsConfigPackage.DATA_FILE__SLASH:
				getSlash().clear();
				getSlash().addAll((Collection<? extends Slash>)newValue);
				return;
			case OdsConfigPackage.DATA_FILE__TEXT:
				getText().clear();
				getText().addAll((Collection<? extends Text>)newValue);
				return;
			case OdsConfigPackage.DATA_FILE__INTERNAL_PROCEDURE:
				getInternalProcedure().clear();
				getInternalProcedure().addAll((Collection<? extends InternalProcedure>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.DATA_FILE__MIXED:
				getMixed().clear();
				return;
			case OdsConfigPackage.DATA_FILE__DATA_FILE_GROUP:
				getDataFileGroup().clear();
				return;
			case OdsConfigPackage.DATA_FILE__SLASH:
				getSlash().clear();
				return;
			case OdsConfigPackage.DATA_FILE__TEXT:
				getText().clear();
				return;
			case OdsConfigPackage.DATA_FILE__INTERNAL_PROCEDURE:
				getInternalProcedure().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.DATA_FILE__MIXED:
				return mixed != null && !mixed.isEmpty();
			case OdsConfigPackage.DATA_FILE__DATA_FILE_GROUP:
				return !getDataFileGroup().isEmpty();
			case OdsConfigPackage.DATA_FILE__SLASH:
				return !getSlash().isEmpty();
			case OdsConfigPackage.DATA_FILE__TEXT:
				return !getText().isEmpty();
			case OdsConfigPackage.DATA_FILE__INTERNAL_PROCEDURE:
				return !getInternalProcedure().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mixed: ");
		result.append(mixed);
		result.append(')');
		return result.toString();
	}

} //DataFileImpl
