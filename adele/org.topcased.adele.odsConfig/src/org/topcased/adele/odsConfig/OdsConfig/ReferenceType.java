/**
 * <copyright>
 * </copyright>
 *
 * $Id: ReferenceType.java,v 1.1 2010-10-27 09:04:22 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getReferenceType()
 * @model extendedMetaData="name='ReferenceType' kind='empty'"
 * @generated
 */
public interface ReferenceType extends PrimitivTypesType {
} // ReferenceType
