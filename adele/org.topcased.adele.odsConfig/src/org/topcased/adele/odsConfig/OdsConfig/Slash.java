/**
 * <copyright>
 * </copyright>
 *
 * $Id: Slash.java,v 1.1 2010-10-27 09:04:24 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Slash</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getSlash()
 * @model extendedMetaData="name='Slash' kind='empty'"
 * @generated
 */
public interface Slash extends EObject {
} // Slash
