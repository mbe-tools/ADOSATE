/**
 * <copyright>
 * </copyright>
 *
 * $Id: DataRecovering.java,v 1.1 2010-10-27 09:04:25 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Recovering</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.DataRecovering#getInternalProcedure <em>Internal Procedure</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.DataRecovering#getDataFile <em>Data File</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.DataRecovering#getDataField <em>Data Field</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getDataRecovering()
 * @model extendedMetaData="name='DataRecovering' kind='elementOnly'"
 * @generated
 */
public interface DataRecovering extends EObject {
	/**
	 * Returns the value of the '<em><b>Internal Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal Procedure</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Procedure</em>' containment reference.
	 * @see #setInternalProcedure(InternalProcedure)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getDataRecovering_InternalProcedure()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='InternalProcedure' namespace='##targetNamespace'"
	 * @generated
	 */
	InternalProcedure getInternalProcedure();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.DataRecovering#getInternalProcedure <em>Internal Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal Procedure</em>' containment reference.
	 * @see #getInternalProcedure()
	 * @generated
	 */
	void setInternalProcedure(InternalProcedure value);

	/**
	 * Returns the value of the '<em><b>Data File</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data File</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data File</em>' containment reference.
	 * @see #setDataFile(DataFile)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getDataRecovering_DataFile()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataFile' namespace='##targetNamespace'"
	 * @generated
	 */
	DataFile getDataFile();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.DataRecovering#getDataFile <em>Data File</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data File</em>' containment reference.
	 * @see #getDataFile()
	 * @generated
	 */
	void setDataFile(DataFile value);

	/**
	 * Returns the value of the '<em><b>Data Field</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Field</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Field</em>' containment reference.
	 * @see #setDataField(DataField)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getDataRecovering_DataField()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataField' namespace='##targetNamespace'"
	 * @generated
	 */
	DataField getDataField();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.DataRecovering#getDataField <em>Data Field</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Field</em>' containment reference.
	 * @see #getDataField()
	 * @generated
	 */
	void setDataField(DataField value);

} // DataRecovering
