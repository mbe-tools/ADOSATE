/**
 * <copyright>
 * </copyright>
 *
 * $Id: PropertyDefinitionImpl.java,v 1.2 2011-04-19 14:09:33 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.topcased.adele.odsConfig.OdsConfig.ObjectCondition;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;
import org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition;
import org.topcased.adele.odsConfig.OdsConfig.Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Property Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyDefinitionImpl#getDefault <em>Default</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyDefinitionImpl#isIsMultiValued <em>Is Multi Valued</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyDefinitionImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyDefinitionImpl#getPropertyCondition <em>Property Condition</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyDefinitionImpl#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PropertyDefinitionImpl extends EObjectImpl implements PropertyDefinition {
	/**
	 * The cached value of the '{@link #getDefault() <em>Default</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefault()
	 * @generated
	 * @ordered
	 */
	protected EList<String> default_;

	/**
	 * The default value of the '{@link #isIsMultiValued() <em>Is Multi Valued</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsMultiValued()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_MULTI_VALUED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsMultiValued() <em>Is Multi Valued</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsMultiValued()
	 * @generated
	 * @ordered
	 */
	protected boolean isMultiValued = IS_MULTI_VALUED_EDEFAULT;

	/**
	 * This is true if the Is Multi Valued attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean isMultiValuedESet;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPropertyCondition() <em>Property Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyCondition()
	 * @generated
	 * @ordered
	 */
	protected ObjectCondition propertyCondition;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected Type type;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PropertyDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdsConfigPackage.Literals.PROPERTY_DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getDefault() {
		if (default_ == null) {
			default_ = new EDataTypeUniqueEList<String>(String.class, this, OdsConfigPackage.PROPERTY_DEFINITION__DEFAULT);
		}
		return default_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsMultiValued() {
		return isMultiValued;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsMultiValued(boolean newIsMultiValued) {
		boolean oldIsMultiValued = isMultiValued;
		isMultiValued = newIsMultiValued;
		boolean oldIsMultiValuedESet = isMultiValuedESet;
		isMultiValuedESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_DEFINITION__IS_MULTI_VALUED, oldIsMultiValued, isMultiValued, !oldIsMultiValuedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIsMultiValued() {
		boolean oldIsMultiValued = isMultiValued;
		boolean oldIsMultiValuedESet = isMultiValuedESet;
		isMultiValued = IS_MULTI_VALUED_EDEFAULT;
		isMultiValuedESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdsConfigPackage.PROPERTY_DEFINITION__IS_MULTI_VALUED, oldIsMultiValued, IS_MULTI_VALUED_EDEFAULT, oldIsMultiValuedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIsMultiValued() {
		return isMultiValuedESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_DEFINITION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectCondition getPropertyCondition() {
		return propertyCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPropertyCondition(ObjectCondition newPropertyCondition, NotificationChain msgs) {
		ObjectCondition oldPropertyCondition = propertyCondition;
		propertyCondition = newPropertyCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_DEFINITION__PROPERTY_CONDITION, oldPropertyCondition, newPropertyCondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPropertyCondition(ObjectCondition newPropertyCondition) {
		if (newPropertyCondition != propertyCondition) {
			NotificationChain msgs = null;
			if (propertyCondition != null)
				msgs = ((InternalEObject)propertyCondition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_DEFINITION__PROPERTY_CONDITION, null, msgs);
			if (newPropertyCondition != null)
				msgs = ((InternalEObject)newPropertyCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_DEFINITION__PROPERTY_CONDITION, null, msgs);
			msgs = basicSetPropertyCondition(newPropertyCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_DEFINITION__PROPERTY_CONDITION, newPropertyCondition, newPropertyCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (Type)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdsConfigPackage.PROPERTY_DEFINITION__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(Type newType) {
		Type oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_DEFINITION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdsConfigPackage.PROPERTY_DEFINITION__PROPERTY_CONDITION:
				return basicSetPropertyCondition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdsConfigPackage.PROPERTY_DEFINITION__DEFAULT:
				return getDefault();
			case OdsConfigPackage.PROPERTY_DEFINITION__IS_MULTI_VALUED:
				return isIsMultiValued();
			case OdsConfigPackage.PROPERTY_DEFINITION__NAME:
				return getName();
			case OdsConfigPackage.PROPERTY_DEFINITION__PROPERTY_CONDITION:
				return getPropertyCondition();
			case OdsConfigPackage.PROPERTY_DEFINITION__TYPE:
				if (resolve) return getType();
				return basicGetType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdsConfigPackage.PROPERTY_DEFINITION__DEFAULT:
				getDefault().clear();
				getDefault().addAll((Collection<? extends String>)newValue);
				return;
			case OdsConfigPackage.PROPERTY_DEFINITION__IS_MULTI_VALUED:
				setIsMultiValued((Boolean)newValue);
				return;
			case OdsConfigPackage.PROPERTY_DEFINITION__NAME:
				setName((String)newValue);
				return;
			case OdsConfigPackage.PROPERTY_DEFINITION__PROPERTY_CONDITION:
				setPropertyCondition((ObjectCondition)newValue);
				return;
			case OdsConfigPackage.PROPERTY_DEFINITION__TYPE:
				setType((Type)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.PROPERTY_DEFINITION__DEFAULT:
				getDefault().clear();
				return;
			case OdsConfigPackage.PROPERTY_DEFINITION__IS_MULTI_VALUED:
				unsetIsMultiValued();
				return;
			case OdsConfigPackage.PROPERTY_DEFINITION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case OdsConfigPackage.PROPERTY_DEFINITION__PROPERTY_CONDITION:
				setPropertyCondition((ObjectCondition)null);
				return;
			case OdsConfigPackage.PROPERTY_DEFINITION__TYPE:
				setType((Type)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.PROPERTY_DEFINITION__DEFAULT:
				return default_ != null && !default_.isEmpty();
			case OdsConfigPackage.PROPERTY_DEFINITION__IS_MULTI_VALUED:
				return isSetIsMultiValued();
			case OdsConfigPackage.PROPERTY_DEFINITION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case OdsConfigPackage.PROPERTY_DEFINITION__PROPERTY_CONDITION:
				return propertyCondition != null;
			case OdsConfigPackage.PROPERTY_DEFINITION__TYPE:
				return type != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (default: ");
		result.append(default_);
		result.append(", isMultiValued: ");
		if (isMultiValuedESet) result.append(isMultiValued); else result.append("<unset>");
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //PropertyDefinitionImpl
