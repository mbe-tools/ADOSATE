/**
 * <copyright>
 * </copyright>
 *
 * $Id: NOTImpl.java,v 1.2 2011-04-19 14:09:33 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.topcased.adele.odsConfig.OdsConfig.AND;
import org.topcased.adele.odsConfig.OdsConfig.Condition;
import org.topcased.adele.odsConfig.OdsConfig.NOT;
import org.topcased.adele.odsConfig.OdsConfig.OR;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>NOT</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.NOTImpl#getMixed <em>Mixed</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.NOTImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.NOTImpl#getConditionGroup <em>Condition Group</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.NOTImpl#getAND <em>AND</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.NOTImpl#getOR <em>OR</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.NOTImpl#getNOT <em>NOT</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NOTImpl extends EObjectImpl implements NOT {
	/**
	 * The cached value of the '{@link #getMixed() <em>Mixed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMixed()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap mixed;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NOTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdsConfigPackage.Literals.NOT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getMixed() {
		if (mixed == null) {
			mixed = new BasicFeatureMap(this, OdsConfigPackage.NOT__MIXED);
		}
		return mixed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Condition> getCondition() {
		return getMixed().list(OdsConfigPackage.Literals.NOT__CONDITION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getConditionGroup() {
		return (FeatureMap)getMixed().<FeatureMap.Entry>list(OdsConfigPackage.Literals.NOT__CONDITION_GROUP);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AND> getAND() {
		return getMixed().list(OdsConfigPackage.Literals.NOT__AND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OR> getOR() {
		return getMixed().list(OdsConfigPackage.Literals.NOT__OR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NOT> getNOT() {
		return getMixed().list(OdsConfigPackage.Literals.NOT__NOT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdsConfigPackage.NOT__MIXED:
				return ((InternalEList<?>)getMixed()).basicRemove(otherEnd, msgs);
			case OdsConfigPackage.NOT__CONDITION:
				return ((InternalEList<?>)getCondition()).basicRemove(otherEnd, msgs);
			case OdsConfigPackage.NOT__CONDITION_GROUP:
				return ((InternalEList<?>)getConditionGroup()).basicRemove(otherEnd, msgs);
			case OdsConfigPackage.NOT__AND:
				return ((InternalEList<?>)getAND()).basicRemove(otherEnd, msgs);
			case OdsConfigPackage.NOT__OR:
				return ((InternalEList<?>)getOR()).basicRemove(otherEnd, msgs);
			case OdsConfigPackage.NOT__NOT:
				return ((InternalEList<?>)getNOT()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdsConfigPackage.NOT__MIXED:
				if (coreType) return getMixed();
				return ((FeatureMap.Internal)getMixed()).getWrapper();
			case OdsConfigPackage.NOT__CONDITION:
				return getCondition();
			case OdsConfigPackage.NOT__CONDITION_GROUP:
				if (coreType) return getConditionGroup();
				return ((FeatureMap.Internal)getConditionGroup()).getWrapper();
			case OdsConfigPackage.NOT__AND:
				return getAND();
			case OdsConfigPackage.NOT__OR:
				return getOR();
			case OdsConfigPackage.NOT__NOT:
				return getNOT();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdsConfigPackage.NOT__MIXED:
				((FeatureMap.Internal)getMixed()).set(newValue);
				return;
			case OdsConfigPackage.NOT__CONDITION:
				getCondition().clear();
				getCondition().addAll((Collection<? extends Condition>)newValue);
				return;
			case OdsConfigPackage.NOT__CONDITION_GROUP:
				((FeatureMap.Internal)getConditionGroup()).set(newValue);
				return;
			case OdsConfigPackage.NOT__AND:
				getAND().clear();
				getAND().addAll((Collection<? extends AND>)newValue);
				return;
			case OdsConfigPackage.NOT__OR:
				getOR().clear();
				getOR().addAll((Collection<? extends OR>)newValue);
				return;
			case OdsConfigPackage.NOT__NOT:
				getNOT().clear();
				getNOT().addAll((Collection<? extends NOT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.NOT__MIXED:
				getMixed().clear();
				return;
			case OdsConfigPackage.NOT__CONDITION:
				getCondition().clear();
				return;
			case OdsConfigPackage.NOT__CONDITION_GROUP:
				getConditionGroup().clear();
				return;
			case OdsConfigPackage.NOT__AND:
				getAND().clear();
				return;
			case OdsConfigPackage.NOT__OR:
				getOR().clear();
				return;
			case OdsConfigPackage.NOT__NOT:
				getNOT().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.NOT__MIXED:
				return mixed != null && !mixed.isEmpty();
			case OdsConfigPackage.NOT__CONDITION:
				return !getCondition().isEmpty();
			case OdsConfigPackage.NOT__CONDITION_GROUP:
				return !getConditionGroup().isEmpty();
			case OdsConfigPackage.NOT__AND:
				return !getAND().isEmpty();
			case OdsConfigPackage.NOT__OR:
				return !getOR().isEmpty();
			case OdsConfigPackage.NOT__NOT:
				return !getNOT().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mixed: ");
		result.append(mixed);
		result.append(')');
		return result.toString();
	}

} //NOTImpl
