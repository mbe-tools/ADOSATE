/**
 * <copyright>
 * </copyright>
 *
 * $Id: UnitsType.java,v 1.1 2010-10-27 09:04:25 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Units Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.UnitsType#getUnit <em>Unit</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getUnitsType()
 * @model extendedMetaData="name='UnitsType' kind='elementOnly'"
 * @generated
 */
public interface UnitsType extends PrimitivTypesType {
	/**
	 * Returns the value of the '<em><b>Unit</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.adele.odsConfig.OdsConfig.Unit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit</em>' containment reference list.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getUnitsType_Unit()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='unit' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Unit> getUnit();

} // UnitsType
