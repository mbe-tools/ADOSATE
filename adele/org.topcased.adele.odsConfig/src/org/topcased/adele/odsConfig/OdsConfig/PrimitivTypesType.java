/**
 * <copyright>
 * </copyright>
 *
 * $Id: PrimitivTypesType.java,v 1.1 2010-10-27 09:04:24 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Primitiv Types Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPrimitivTypesType()
 * @model abstract="true"
 *        extendedMetaData="name='PrimitivTypes_._type' kind='elementOnly'"
 * @generated
 */
public interface PrimitivTypesType extends Type {
} // PrimitivTypesType
