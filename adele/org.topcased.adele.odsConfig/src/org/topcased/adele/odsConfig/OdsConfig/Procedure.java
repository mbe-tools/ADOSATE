/**
 * <copyright>
 * </copyright>
 *
 * $Id: Procedure.java,v 1.1 2010-10-27 09:04:25 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Procedure</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getProcedure()
 * @model extendedMetaData="name='Procedure'"
 * @generated
 */
public enum Procedure implements Enumerator {
	/**
	 * The '<em><b>Get Current Project Directory</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_PROJECT_DIRECTORY_VALUE
	 * @generated
	 * @ordered
	 */
	GET_CURRENT_PROJECT_DIRECTORY(0, "getCurrentProjectDirectory", "getCurrentProjectDirectory"),

	/**
	 * The '<em><b>Get Current Workspace Directory</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_WORKSPACE_DIRECTORY_VALUE
	 * @generated
	 * @ordered
	 */
	GET_CURRENT_WORKSPACE_DIRECTORY(1, "getCurrentWorkspaceDirectory", "getCurrentWorkspaceDirectory"),

	/**
	 * The '<em><b>Get Current Design Directory</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_DESIGN_DIRECTORY_VALUE
	 * @generated
	 * @ordered
	 */
	GET_CURRENT_DESIGN_DIRECTORY(2, "getCurrentDesignDirectory", "getCurrentDesignDirectory"),

	/**
	 * The '<em><b>Get Current Object Directory</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_OBJECT_DIRECTORY_VALUE
	 * @generated
	 * @ordered
	 */
	GET_CURRENT_OBJECT_DIRECTORY(3, "getCurrentObjectDirectory", "getCurrentObjectDirectory"),

	/**
	 * The '<em><b>Get Current Object Location</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_OBJECT_LOCATION_VALUE
	 * @generated
	 * @ordered
	 */
	GET_CURRENT_OBJECT_LOCATION(4, "getCurrentObjectLocation", "getCurrentObjectLocation"),

	/**
	 * The '<em><b>Get Current Project Name</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_PROJECT_NAME_VALUE
	 * @generated
	 * @ordered
	 */
	GET_CURRENT_PROJECT_NAME(5, "getCurrentProjectName", "getCurrentProjectName"),

	/**
	 * The '<em><b>Get Current Design Name</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_DESIGN_NAME_VALUE
	 * @generated
	 * @ordered
	 */
	GET_CURRENT_DESIGN_NAME(6, "getCurrentDesignName", "getCurrentDesignName"),

	/**
	 * The '<em><b>Get Current Object Name</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_OBJECT_NAME_VALUE
	 * @generated
	 * @ordered
	 */
	GET_CURRENT_OBJECT_NAME(7, "getCurrentObjectName", "getCurrentObjectName"),

	/**
	 * The '<em><b>Get Current Object Id</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_OBJECT_ID_VALUE
	 * @generated
	 * @ordered
	 */
	GET_CURRENT_OBJECT_ID(8, "getCurrentObjectId", "getCurrentObjectId");

	/**
	 * The '<em><b>Get Current Project Directory</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Get Current Project Directory</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_PROJECT_DIRECTORY
	 * @model name="getCurrentProjectDirectory"
	 * @generated
	 * @ordered
	 */
	public static final int GET_CURRENT_PROJECT_DIRECTORY_VALUE = 0;

	/**
	 * The '<em><b>Get Current Workspace Directory</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Get Current Workspace Directory</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_WORKSPACE_DIRECTORY
	 * @model name="getCurrentWorkspaceDirectory"
	 * @generated
	 * @ordered
	 */
	public static final int GET_CURRENT_WORKSPACE_DIRECTORY_VALUE = 1;

	/**
	 * The '<em><b>Get Current Design Directory</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Get Current Design Directory</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_DESIGN_DIRECTORY
	 * @model name="getCurrentDesignDirectory"
	 * @generated
	 * @ordered
	 */
	public static final int GET_CURRENT_DESIGN_DIRECTORY_VALUE = 2;

	/**
	 * The '<em><b>Get Current Object Directory</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Get Current Object Directory</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_OBJECT_DIRECTORY
	 * @model name="getCurrentObjectDirectory"
	 * @generated
	 * @ordered
	 */
	public static final int GET_CURRENT_OBJECT_DIRECTORY_VALUE = 3;

	/**
	 * The '<em><b>Get Current Object Location</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Get Current Object Location</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_OBJECT_LOCATION
	 * @model name="getCurrentObjectLocation"
	 * @generated
	 * @ordered
	 */
	public static final int GET_CURRENT_OBJECT_LOCATION_VALUE = 4;

	/**
	 * The '<em><b>Get Current Project Name</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Get Current Project Name</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_PROJECT_NAME
	 * @model name="getCurrentProjectName"
	 * @generated
	 * @ordered
	 */
	public static final int GET_CURRENT_PROJECT_NAME_VALUE = 5;

	/**
	 * The '<em><b>Get Current Design Name</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Get Current Design Name</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_DESIGN_NAME
	 * @model name="getCurrentDesignName"
	 * @generated
	 * @ordered
	 */
	public static final int GET_CURRENT_DESIGN_NAME_VALUE = 6;

	/**
	 * The '<em><b>Get Current Object Name</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Get Current Object Name</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_OBJECT_NAME
	 * @model name="getCurrentObjectName"
	 * @generated
	 * @ordered
	 */
	public static final int GET_CURRENT_OBJECT_NAME_VALUE = 7;

	/**
	 * The '<em><b>Get Current Object Id</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Get Current Object Id</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GET_CURRENT_OBJECT_ID
	 * @model name="getCurrentObjectId"
	 * @generated
	 * @ordered
	 */
	public static final int GET_CURRENT_OBJECT_ID_VALUE = 8;

	/**
	 * An array of all the '<em><b>Procedure</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final Procedure[] VALUES_ARRAY =
		new Procedure[] {
			GET_CURRENT_PROJECT_DIRECTORY,
			GET_CURRENT_WORKSPACE_DIRECTORY,
			GET_CURRENT_DESIGN_DIRECTORY,
			GET_CURRENT_OBJECT_DIRECTORY,
			GET_CURRENT_OBJECT_LOCATION,
			GET_CURRENT_PROJECT_NAME,
			GET_CURRENT_DESIGN_NAME,
			GET_CURRENT_OBJECT_NAME,
			GET_CURRENT_OBJECT_ID,
		};

	/**
	 * A public read-only list of all the '<em><b>Procedure</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<Procedure> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Procedure</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Procedure get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Procedure result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Procedure</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Procedure getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Procedure result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Procedure</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Procedure get(int value) {
		switch (value) {
			case GET_CURRENT_PROJECT_DIRECTORY_VALUE: return GET_CURRENT_PROJECT_DIRECTORY;
			case GET_CURRENT_WORKSPACE_DIRECTORY_VALUE: return GET_CURRENT_WORKSPACE_DIRECTORY;
			case GET_CURRENT_DESIGN_DIRECTORY_VALUE: return GET_CURRENT_DESIGN_DIRECTORY;
			case GET_CURRENT_OBJECT_DIRECTORY_VALUE: return GET_CURRENT_OBJECT_DIRECTORY;
			case GET_CURRENT_OBJECT_LOCATION_VALUE: return GET_CURRENT_OBJECT_LOCATION;
			case GET_CURRENT_PROJECT_NAME_VALUE: return GET_CURRENT_PROJECT_NAME;
			case GET_CURRENT_DESIGN_NAME_VALUE: return GET_CURRENT_DESIGN_NAME;
			case GET_CURRENT_OBJECT_NAME_VALUE: return GET_CURRENT_OBJECT_NAME;
			case GET_CURRENT_OBJECT_ID_VALUE: return GET_CURRENT_OBJECT_ID;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private Procedure(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //Procedure
