/**
 * <copyright>
 * </copyright>
 *
 * $Id: ODSystemImpl.java,v 1.2 2011-04-19 14:09:35 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.topcased.adele.odsConfig.OdsConfig.DataRecovering;
import org.topcased.adele.odsConfig.OdsConfig.Mode;
import org.topcased.adele.odsConfig.OdsConfig.ODSection;
import org.topcased.adele.odsConfig.OdsConfig.ODSystem;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OD System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSystemImpl#getODSection <em>OD Section</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSystemImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSystemImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSystemImpl#getMode <em>Mode</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSystemImpl#getStyleSheet <em>Style Sheet</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSystemImpl#getDataRecovering <em>Data Recovering</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ODSystemImpl extends EObjectImpl implements ODSystem {
	/**
	 * The cached value of the '{@link #getODSection() <em>OD Section</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getODSection()
	 * @generated
	 * @ordered
	 */
	protected EList<ODSection> oDSection;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getMode() <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMode()
	 * @generated
	 * @ordered
	 */
	protected static final Mode MODE_EDEFAULT = Mode.HIERARCHICAL;

	/**
	 * The cached value of the '{@link #getMode() <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMode()
	 * @generated
	 * @ordered
	 */
	protected Mode mode = MODE_EDEFAULT;

	/**
	 * This is true if the Mode attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean modeESet;

	/**
	 * The default value of the '{@link #getStyleSheet() <em>Style Sheet</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStyleSheet()
	 * @generated
	 * @ordered
	 */
	protected static final String STYLE_SHEET_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStyleSheet() <em>Style Sheet</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStyleSheet()
	 * @generated
	 * @ordered
	 */
	protected String styleSheet = STYLE_SHEET_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDataRecovering() <em>Data Recovering</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataRecovering()
	 * @generated
	 * @ordered
	 */
	protected DataRecovering dataRecovering;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ODSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdsConfigPackage.Literals.OD_SYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ODSection> getODSection() {
		if (oDSection == null) {
			oDSection = new EObjectContainmentEList<ODSection>(ODSection.class, this, OdsConfigPackage.OD_SYSTEM__OD_SECTION);
		}
		return oDSection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.OD_SYSTEM__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.OD_SYSTEM__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mode getMode() {
		return mode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMode(Mode newMode) {
		Mode oldMode = mode;
		mode = newMode == null ? MODE_EDEFAULT : newMode;
		boolean oldModeESet = modeESet;
		modeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.OD_SYSTEM__MODE, oldMode, mode, !oldModeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMode() {
		Mode oldMode = mode;
		boolean oldModeESet = modeESet;
		mode = MODE_EDEFAULT;
		modeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdsConfigPackage.OD_SYSTEM__MODE, oldMode, MODE_EDEFAULT, oldModeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMode() {
		return modeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStyleSheet() {
		return styleSheet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStyleSheet(String newStyleSheet) {
		String oldStyleSheet = styleSheet;
		styleSheet = newStyleSheet;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.OD_SYSTEM__STYLE_SHEET, oldStyleSheet, styleSheet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataRecovering getDataRecovering() {
		return dataRecovering;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataRecovering(DataRecovering newDataRecovering, NotificationChain msgs) {
		DataRecovering oldDataRecovering = dataRecovering;
		dataRecovering = newDataRecovering;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.OD_SYSTEM__DATA_RECOVERING, oldDataRecovering, newDataRecovering);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataRecovering(DataRecovering newDataRecovering) {
		if (newDataRecovering != dataRecovering) {
			NotificationChain msgs = null;
			if (dataRecovering != null)
				msgs = ((InternalEObject)dataRecovering).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.OD_SYSTEM__DATA_RECOVERING, null, msgs);
			if (newDataRecovering != null)
				msgs = ((InternalEObject)newDataRecovering).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.OD_SYSTEM__DATA_RECOVERING, null, msgs);
			msgs = basicSetDataRecovering(newDataRecovering, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.OD_SYSTEM__DATA_RECOVERING, newDataRecovering, newDataRecovering));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdsConfigPackage.OD_SYSTEM__OD_SECTION:
				return ((InternalEList<?>)getODSection()).basicRemove(otherEnd, msgs);
			case OdsConfigPackage.OD_SYSTEM__DATA_RECOVERING:
				return basicSetDataRecovering(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdsConfigPackage.OD_SYSTEM__OD_SECTION:
				return getODSection();
			case OdsConfigPackage.OD_SYSTEM__ID:
				return getId();
			case OdsConfigPackage.OD_SYSTEM__LABEL:
				return getLabel();
			case OdsConfigPackage.OD_SYSTEM__MODE:
				return getMode();
			case OdsConfigPackage.OD_SYSTEM__STYLE_SHEET:
				return getStyleSheet();
			case OdsConfigPackage.OD_SYSTEM__DATA_RECOVERING:
				return getDataRecovering();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdsConfigPackage.OD_SYSTEM__OD_SECTION:
				getODSection().clear();
				getODSection().addAll((Collection<? extends ODSection>)newValue);
				return;
			case OdsConfigPackage.OD_SYSTEM__ID:
				setId((String)newValue);
				return;
			case OdsConfigPackage.OD_SYSTEM__LABEL:
				setLabel((String)newValue);
				return;
			case OdsConfigPackage.OD_SYSTEM__MODE:
				setMode((Mode)newValue);
				return;
			case OdsConfigPackage.OD_SYSTEM__STYLE_SHEET:
				setStyleSheet((String)newValue);
				return;
			case OdsConfigPackage.OD_SYSTEM__DATA_RECOVERING:
				setDataRecovering((DataRecovering)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.OD_SYSTEM__OD_SECTION:
				getODSection().clear();
				return;
			case OdsConfigPackage.OD_SYSTEM__ID:
				setId(ID_EDEFAULT);
				return;
			case OdsConfigPackage.OD_SYSTEM__LABEL:
				setLabel(LABEL_EDEFAULT);
				return;
			case OdsConfigPackage.OD_SYSTEM__MODE:
				unsetMode();
				return;
			case OdsConfigPackage.OD_SYSTEM__STYLE_SHEET:
				setStyleSheet(STYLE_SHEET_EDEFAULT);
				return;
			case OdsConfigPackage.OD_SYSTEM__DATA_RECOVERING:
				setDataRecovering((DataRecovering)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.OD_SYSTEM__OD_SECTION:
				return oDSection != null && !oDSection.isEmpty();
			case OdsConfigPackage.OD_SYSTEM__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case OdsConfigPackage.OD_SYSTEM__LABEL:
				return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
			case OdsConfigPackage.OD_SYSTEM__MODE:
				return isSetMode();
			case OdsConfigPackage.OD_SYSTEM__STYLE_SHEET:
				return STYLE_SHEET_EDEFAULT == null ? styleSheet != null : !STYLE_SHEET_EDEFAULT.equals(styleSheet);
			case OdsConfigPackage.OD_SYSTEM__DATA_RECOVERING:
				return dataRecovering != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", label: ");
		result.append(label);
		result.append(", mode: ");
		if (modeESet) result.append(mode); else result.append("<unset>");
		result.append(", styleSheet: ");
		result.append(styleSheet);
		result.append(')');
		return result.toString();
	}

} //ODSystemImpl
