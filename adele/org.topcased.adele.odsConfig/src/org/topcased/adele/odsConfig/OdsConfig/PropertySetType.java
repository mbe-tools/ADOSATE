/**
 * <copyright>
 * </copyright>
 *
 * $Id: PropertySetType.java,v 1.2 2011-04-19 14:13:29 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property Set Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertySetType#getPropertyType <em>Property Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertySetType#getPropertyDefinition <em>Property Definition</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertySetType#getPropertyConstant <em>Property Constant</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertySetType#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertySetType()
 * @model extendedMetaData="name='PropertySet_._type' kind='elementOnly'"
 * @generated
 */
public interface PropertySetType extends EObject {
	/**
	 * Returns the value of the '<em><b>Property Type</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.adele.odsConfig.OdsConfig.PropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Type</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Type</em>' containment reference list.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertySetType_PropertyType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PropertyType' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PropertyType> getPropertyType();

	/**
	 * Returns the value of the '<em><b>Property Definition</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Definition</em>' containment reference list.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertySetType_PropertyDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PropertyDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PropertyDefinition> getPropertyDefinition();

	/**
	 * Returns the value of the '<em><b>Property Constant</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.adele.odsConfig.OdsConfig.PropertyConstant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Constant</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Constant</em>' containment reference list.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertySetType_PropertyConstant()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PropertyConstant' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PropertyConstant> getPropertyConstant();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertySetType_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='name' namespace='##targetNamespace'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertySetType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // PropertySetType
