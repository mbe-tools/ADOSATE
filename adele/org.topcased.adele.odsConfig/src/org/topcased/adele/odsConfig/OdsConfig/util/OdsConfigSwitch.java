/**
 * <copyright>
 * </copyright>
 *
 * $Id: OdsConfigSwitch.java,v 1.1 2010-10-27 09:04:34 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.topcased.adele.odsConfig.OdsConfig.AND;
import org.topcased.adele.odsConfig.OdsConfig.AadlBoolean;
import org.topcased.adele.odsConfig.OdsConfig.AadlString;
import org.topcased.adele.odsConfig.OdsConfig.ClassifierType;
import org.topcased.adele.odsConfig.OdsConfig.Condition;
import org.topcased.adele.odsConfig.OdsConfig.DataField;
import org.topcased.adele.odsConfig.OdsConfig.DataFile;
import org.topcased.adele.odsConfig.OdsConfig.DataRecovering;
import org.topcased.adele.odsConfig.OdsConfig.DocumentRoot;
import org.topcased.adele.odsConfig.OdsConfig.EnumerationType;
import org.topcased.adele.odsConfig.OdsConfig.IntegerRange;
import org.topcased.adele.odsConfig.OdsConfig.InternalProcedure;
import org.topcased.adele.odsConfig.OdsConfig.Label;
import org.topcased.adele.odsConfig.OdsConfig.LoopCondition;
import org.topcased.adele.odsConfig.OdsConfig.NOT;
import org.topcased.adele.odsConfig.OdsConfig.NumberType;
import org.topcased.adele.odsConfig.OdsConfig.ODSection;
import org.topcased.adele.odsConfig.OdsConfig.ODSystem;
import org.topcased.adele.odsConfig.OdsConfig.ODSystemsType;
import org.topcased.adele.odsConfig.OdsConfig.OR;
import org.topcased.adele.odsConfig.OdsConfig.ObjectCondition;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;
import org.topcased.adele.odsConfig.OdsConfig.PrimitivTypesType;
import org.topcased.adele.odsConfig.OdsConfig.PropertyConstant;
import org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition;
import org.topcased.adele.odsConfig.OdsConfig.PropertySetType;
import org.topcased.adele.odsConfig.OdsConfig.PropertyType;
import org.topcased.adele.odsConfig.OdsConfig.RangeType;
import org.topcased.adele.odsConfig.OdsConfig.RealRange;
import org.topcased.adele.odsConfig.OdsConfig.RecordFieldType;
import org.topcased.adele.odsConfig.OdsConfig.RecordType;
import org.topcased.adele.odsConfig.OdsConfig.ReferenceType;
import org.topcased.adele.odsConfig.OdsConfig.Slash;
import org.topcased.adele.odsConfig.OdsConfig.Text;
import org.topcased.adele.odsConfig.OdsConfig.Type;
import org.topcased.adele.odsConfig.OdsConfig.Unit;
import org.topcased.adele.odsConfig.OdsConfig.UnitsType;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage
 * @generated
 */
public class OdsConfigSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OdsConfigPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OdsConfigSwitch() {
		if (modelPackage == null) {
			modelPackage = OdsConfigPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case OdsConfigPackage.AADL_BOOLEAN: {
				AadlBoolean aadlBoolean = (AadlBoolean)theEObject;
				T result = caseAadlBoolean(aadlBoolean);
				if (result == null) result = casePrimitivTypesType(aadlBoolean);
				if (result == null) result = caseType(aadlBoolean);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.AADL_STRING: {
				AadlString aadlString = (AadlString)theEObject;
				T result = caseAadlString(aadlString);
				if (result == null) result = casePrimitivTypesType(aadlString);
				if (result == null) result = caseType(aadlString);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.AND: {
				AND and = (AND)theEObject;
				T result = caseAND(and);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.CLASSIFIER_TYPE: {
				ClassifierType classifierType = (ClassifierType)theEObject;
				T result = caseClassifierType(classifierType);
				if (result == null) result = casePrimitivTypesType(classifierType);
				if (result == null) result = caseType(classifierType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.CONDITION: {
				Condition condition = (Condition)theEObject;
				T result = caseCondition(condition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.DATA_FIELD: {
				DataField dataField = (DataField)theEObject;
				T result = caseDataField(dataField);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.DATA_FILE: {
				DataFile dataFile = (DataFile)theEObject;
				T result = caseDataFile(dataFile);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.DATA_RECOVERING: {
				DataRecovering dataRecovering = (DataRecovering)theEObject;
				T result = caseDataRecovering(dataRecovering);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.DOCUMENT_ROOT: {
				DocumentRoot documentRoot = (DocumentRoot)theEObject;
				T result = caseDocumentRoot(documentRoot);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.ENUMERATION_TYPE: {
				EnumerationType enumerationType = (EnumerationType)theEObject;
				T result = caseEnumerationType(enumerationType);
				if (result == null) result = casePrimitivTypesType(enumerationType);
				if (result == null) result = caseType(enumerationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.INTEGER_RANGE: {
				IntegerRange integerRange = (IntegerRange)theEObject;
				T result = caseIntegerRange(integerRange);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.INTERNAL_PROCEDURE: {
				InternalProcedure internalProcedure = (InternalProcedure)theEObject;
				T result = caseInternalProcedure(internalProcedure);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.LABEL: {
				Label label = (Label)theEObject;
				T result = caseLabel(label);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.LOOP_CONDITION: {
				LoopCondition loopCondition = (LoopCondition)theEObject;
				T result = caseLoopCondition(loopCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.NOT: {
				NOT not = (NOT)theEObject;
				T result = caseNOT(not);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.NUMBER_TYPE: {
				NumberType numberType = (NumberType)theEObject;
				T result = caseNumberType(numberType);
				if (result == null) result = casePrimitivTypesType(numberType);
				if (result == null) result = caseType(numberType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.OBJECT_CONDITION: {
				ObjectCondition objectCondition = (ObjectCondition)theEObject;
				T result = caseObjectCondition(objectCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.OD_SECTION: {
				ODSection odSection = (ODSection)theEObject;
				T result = caseODSection(odSection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.OD_SYSTEM: {
				ODSystem odSystem = (ODSystem)theEObject;
				T result = caseODSystem(odSystem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.OD_SYSTEMS_TYPE: {
				ODSystemsType odSystemsType = (ODSystemsType)theEObject;
				T result = caseODSystemsType(odSystemsType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.OR: {
				OR or = (OR)theEObject;
				T result = caseOR(or);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.PRIMITIV_TYPES_TYPE: {
				PrimitivTypesType primitivTypesType = (PrimitivTypesType)theEObject;
				T result = casePrimitivTypesType(primitivTypesType);
				if (result == null) result = caseType(primitivTypesType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.PROPERTY_CONSTANT: {
				PropertyConstant propertyConstant = (PropertyConstant)theEObject;
				T result = casePropertyConstant(propertyConstant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.PROPERTY_DEFINITION: {
				PropertyDefinition propertyDefinition = (PropertyDefinition)theEObject;
				T result = casePropertyDefinition(propertyDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.PROPERTY_SET_TYPE: {
				PropertySetType propertySetType = (PropertySetType)theEObject;
				T result = casePropertySetType(propertySetType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.PROPERTY_TYPE: {
				PropertyType propertyType = (PropertyType)theEObject;
				T result = casePropertyType(propertyType);
				if (result == null) result = caseType(propertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.RANGE_TYPE: {
				RangeType rangeType = (RangeType)theEObject;
				T result = caseRangeType(rangeType);
				if (result == null) result = casePrimitivTypesType(rangeType);
				if (result == null) result = caseType(rangeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.REAL_RANGE: {
				RealRange realRange = (RealRange)theEObject;
				T result = caseRealRange(realRange);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.RECORD_FIELD_TYPE: {
				RecordFieldType recordFieldType = (RecordFieldType)theEObject;
				T result = caseRecordFieldType(recordFieldType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.RECORD_TYPE: {
				RecordType recordType = (RecordType)theEObject;
				T result = caseRecordType(recordType);
				if (result == null) result = casePrimitivTypesType(recordType);
				if (result == null) result = caseType(recordType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.REFERENCE_TYPE: {
				ReferenceType referenceType = (ReferenceType)theEObject;
				T result = caseReferenceType(referenceType);
				if (result == null) result = casePrimitivTypesType(referenceType);
				if (result == null) result = caseType(referenceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.SLASH: {
				Slash slash = (Slash)theEObject;
				T result = caseSlash(slash);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.TEXT: {
				Text text = (Text)theEObject;
				T result = caseText(text);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.UNIT: {
				Unit unit = (Unit)theEObject;
				T result = caseUnit(unit);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.UNITS_TYPE: {
				UnitsType unitsType = (UnitsType)theEObject;
				T result = caseUnitsType(unitsType);
				if (result == null) result = casePrimitivTypesType(unitsType);
				if (result == null) result = caseType(unitsType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdsConfigPackage.TYPE: {
				Type type = (Type)theEObject;
				T result = caseType(type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aadl Boolean</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aadl Boolean</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAadlBoolean(AadlBoolean object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aadl String</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aadl String</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAadlString(AadlString object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AND</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AND</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAND(AND object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Classifier Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Classifier Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClassifierType(ClassifierType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCondition(Condition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataField(DataField object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data File</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data File</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataFile(DataFile object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Recovering</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Recovering</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataRecovering(DataRecovering object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDocumentRoot(DocumentRoot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enumeration Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enumeration Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumerationType(EnumerationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Integer Range</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer Range</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntegerRange(IntegerRange object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Internal Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Internal Procedure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInternalProcedure(InternalProcedure object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Label</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Label</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLabel(Label object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Loop Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Loop Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLoopCondition(LoopCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>NOT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>NOT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNOT(NOT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Number Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Number Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNumberType(NumberType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObjectCondition(ObjectCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>OD Section</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>OD Section</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseODSection(ODSection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>OD System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>OD System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseODSystem(ODSystem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>OD Systems Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>OD Systems Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseODSystemsType(ODSystemsType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>OR</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>OR</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOR(OR object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Primitiv Types Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Primitiv Types Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePrimitivTypesType(PrimitivTypesType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePropertyConstant(PropertyConstant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePropertyDefinition(PropertyDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property Set Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property Set Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePropertySetType(PropertySetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePropertyType(PropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Range Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Range Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRangeType(RangeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Real Range</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Real Range</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRealRange(RealRange object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Record Field Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record Field Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecordFieldType(RecordFieldType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Record Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecordType(RecordType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferenceType(ReferenceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Slash</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Slash</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSlash(Slash object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Text</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Text</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseText(Text object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unit</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unit</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnit(Unit object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Units Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Units Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnitsType(UnitsType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseType(Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //OdsConfigSwitch
