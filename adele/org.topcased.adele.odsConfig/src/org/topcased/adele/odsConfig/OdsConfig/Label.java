/**
 * <copyright>
 * </copyright>
 *
 * $Id: Label.java,v 1.1 2010-10-27 09:04:22 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Label</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.Label#getValue <em>Value</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.Label#getFormat <em>Format</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.Label#getProcedure <em>Procedure</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.Label#getSubject <em>Subject</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getLabel()
 * @model extendedMetaData="name='Label' kind='simple'"
 * @generated
 */
public interface Label extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getLabel_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.Label#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Format</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Format</em>' attribute.
	 * @see #setFormat(String)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getLabel_Format()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='format' namespace='##targetNamespace'"
	 * @generated
	 */
	String getFormat();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.Label#getFormat <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Format</em>' attribute.
	 * @see #getFormat()
	 * @generated
	 */
	void setFormat(String value);

	/**
	 * Returns the value of the '<em><b>Procedure</b></em>' attribute.
	 * The literals are from the enumeration {@link org.topcased.adele.odsConfig.OdsConfig.Procedure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Procedure
	 * @see #isSetProcedure()
	 * @see #unsetProcedure()
	 * @see #setProcedure(Procedure)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getLabel_Procedure()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='procedure' namespace='##targetNamespace'"
	 * @generated
	 */
	Procedure getProcedure();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.Label#getProcedure <em>Procedure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Procedure
	 * @see #isSetProcedure()
	 * @see #unsetProcedure()
	 * @see #getProcedure()
	 * @generated
	 */
	void setProcedure(Procedure value);

	/**
	 * Unsets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.Label#getProcedure <em>Procedure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetProcedure()
	 * @see #getProcedure()
	 * @see #setProcedure(Procedure)
	 * @generated
	 */
	void unsetProcedure();

	/**
	 * Returns whether the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.Label#getProcedure <em>Procedure</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Procedure</em>' attribute is set.
	 * @see #unsetProcedure()
	 * @see #getProcedure()
	 * @see #setProcedure(Procedure)
	 * @generated
	 */
	boolean isSetProcedure();

	/**
	 * Returns the value of the '<em><b>Subject</b></em>' attribute.
	 * The default value is <code>"context"</code>.
	 * The literals are from the enumeration {@link org.topcased.adele.odsConfig.OdsConfig.SubjectType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subject</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subject</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.SubjectType
	 * @see #isSetSubject()
	 * @see #unsetSubject()
	 * @see #setSubject(SubjectType)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getLabel_Subject()
	 * @model default="context" unsettable="true"
	 *        extendedMetaData="kind='attribute' name='subject' namespace='##targetNamespace'"
	 * @generated
	 */
	SubjectType getSubject();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.Label#getSubject <em>Subject</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subject</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.SubjectType
	 * @see #isSetSubject()
	 * @see #unsetSubject()
	 * @see #getSubject()
	 * @generated
	 */
	void setSubject(SubjectType value);

	/**
	 * Unsets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.Label#getSubject <em>Subject</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSubject()
	 * @see #getSubject()
	 * @see #setSubject(SubjectType)
	 * @generated
	 */
	void unsetSubject();

	/**
	 * Returns whether the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.Label#getSubject <em>Subject</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Subject</em>' attribute is set.
	 * @see #unsetSubject()
	 * @see #getSubject()
	 * @see #setSubject(SubjectType)
	 * @generated
	 */
	boolean isSetSubject();

} // Label
