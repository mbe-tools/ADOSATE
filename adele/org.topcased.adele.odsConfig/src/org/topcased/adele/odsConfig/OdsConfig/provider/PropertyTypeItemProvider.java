/**
 * <copyright>
 * </copyright>
 *
 * $Id: PropertyTypeItemProvider.java,v 1.2 2011-04-19 14:12:04 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigFactory;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;
import org.topcased.adele.odsConfig.OdsConfig.PropertyType;

/**
 * This is the item provider adapter for a {@link org.topcased.adele.odsConfig.OdsConfig.PropertyType} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class PropertyTypeItemProvider
	extends TypeItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyTypeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_PropertyType_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_PropertyType_name_feature", "_UI_PropertyType_type"),
				 OdsConfigPackage.Literals.PROPERTY_TYPE__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(OdsConfigPackage.Literals.PROPERTY_TYPE__RECORD_TYPE);
			childrenFeatures.add(OdsConfigPackage.Literals.PROPERTY_TYPE__CLASSIFIER_TYPE);
			childrenFeatures.add(OdsConfigPackage.Literals.PROPERTY_TYPE__REFERENCE_TYPE);
			childrenFeatures.add(OdsConfigPackage.Literals.PROPERTY_TYPE__ENUMERATION_TYPE);
			childrenFeatures.add(OdsConfigPackage.Literals.PROPERTY_TYPE__AADL_BOOLEAN);
			childrenFeatures.add(OdsConfigPackage.Literals.PROPERTY_TYPE__AADL_STRING);
			childrenFeatures.add(OdsConfigPackage.Literals.PROPERTY_TYPE__UNITS_TYPE);
			childrenFeatures.add(OdsConfigPackage.Literals.PROPERTY_TYPE__RANGE_TYPE);
			childrenFeatures.add(OdsConfigPackage.Literals.PROPERTY_TYPE__NUMBER_TYPE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns PropertyType.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/PropertyType"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((PropertyType)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_PropertyType_type") :
			getString("_UI_PropertyType_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(PropertyType.class)) {
			case OdsConfigPackage.PROPERTY_TYPE__NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case OdsConfigPackage.PROPERTY_TYPE__RECORD_TYPE:
			case OdsConfigPackage.PROPERTY_TYPE__CLASSIFIER_TYPE:
			case OdsConfigPackage.PROPERTY_TYPE__REFERENCE_TYPE:
			case OdsConfigPackage.PROPERTY_TYPE__ENUMERATION_TYPE:
			case OdsConfigPackage.PROPERTY_TYPE__AADL_BOOLEAN:
			case OdsConfigPackage.PROPERTY_TYPE__AADL_STRING:
			case OdsConfigPackage.PROPERTY_TYPE__UNITS_TYPE:
			case OdsConfigPackage.PROPERTY_TYPE__RANGE_TYPE:
			case OdsConfigPackage.PROPERTY_TYPE__NUMBER_TYPE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(OdsConfigPackage.Literals.PROPERTY_TYPE__RECORD_TYPE,
				 OdsConfigFactory.eINSTANCE.createRecordType()));

		newChildDescriptors.add
			(createChildParameter
				(OdsConfigPackage.Literals.PROPERTY_TYPE__CLASSIFIER_TYPE,
				 OdsConfigFactory.eINSTANCE.createClassifierType()));

		newChildDescriptors.add
			(createChildParameter
				(OdsConfigPackage.Literals.PROPERTY_TYPE__REFERENCE_TYPE,
				 OdsConfigFactory.eINSTANCE.createReferenceType()));

		newChildDescriptors.add
			(createChildParameter
				(OdsConfigPackage.Literals.PROPERTY_TYPE__ENUMERATION_TYPE,
				 OdsConfigFactory.eINSTANCE.createEnumerationType()));

		newChildDescriptors.add
			(createChildParameter
				(OdsConfigPackage.Literals.PROPERTY_TYPE__AADL_BOOLEAN,
				 OdsConfigFactory.eINSTANCE.createAadlBoolean()));

		newChildDescriptors.add
			(createChildParameter
				(OdsConfigPackage.Literals.PROPERTY_TYPE__AADL_STRING,
				 OdsConfigFactory.eINSTANCE.createAadlString()));

		newChildDescriptors.add
			(createChildParameter
				(OdsConfigPackage.Literals.PROPERTY_TYPE__UNITS_TYPE,
				 OdsConfigFactory.eINSTANCE.createUnitsType()));

		newChildDescriptors.add
			(createChildParameter
				(OdsConfigPackage.Literals.PROPERTY_TYPE__RANGE_TYPE,
				 OdsConfigFactory.eINSTANCE.createRangeType()));

		newChildDescriptors.add
			(createChildParameter
				(OdsConfigPackage.Literals.PROPERTY_TYPE__NUMBER_TYPE,
				 OdsConfigFactory.eINSTANCE.createNumberType()));
	}

}
