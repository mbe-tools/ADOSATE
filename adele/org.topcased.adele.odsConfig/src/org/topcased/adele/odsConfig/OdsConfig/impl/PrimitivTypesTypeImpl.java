/**
 * <copyright>
 * </copyright>
 *
 * $Id: PrimitivTypesTypeImpl.java,v 1.2 2011-04-19 14:09:33 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import org.eclipse.emf.ecore.EClass;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;
import org.topcased.adele.odsConfig.OdsConfig.PrimitivTypesType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Primitiv Types Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public abstract class PrimitivTypesTypeImpl extends TypeImpl implements PrimitivTypesType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PrimitivTypesTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdsConfigPackage.Literals.PRIMITIV_TYPES_TYPE;
	}

} //PrimitivTypesTypeImpl
