/**
 * <copyright>
 * </copyright>
 *
 * $Id: OdsConfigPackage.java,v 1.1 2010-10-27 09:04:24 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigFactory
 * @model kind="package"
 * @generated
 */
public interface OdsConfigPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "OdsConfig";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/resource/org.topcased.adele.odsConfig/model/odsConfig.xsd";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "OdsConfig";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OdsConfigPackage eINSTANCE = org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.TypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.TypeImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getType()
	 * @generated
	 */
	int TYPE = 35;

	/**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.PrimitivTypesTypeImpl <em>Primitiv Types Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.PrimitivTypesTypeImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getPrimitivTypesType()
	 * @generated
	 */
	int PRIMITIV_TYPES_TYPE = 21;

	/**
	 * The number of structural features of the '<em>Primitiv Types Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIV_TYPES_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.AadlBooleanImpl <em>Aadl Boolean</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.AadlBooleanImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getAadlBoolean()
	 * @generated
	 */
	int AADL_BOOLEAN = 0;

	/**
	 * The number of structural features of the '<em>Aadl Boolean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_BOOLEAN_FEATURE_COUNT = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.AadlStringImpl <em>Aadl String</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.AadlStringImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getAadlString()
	 * @generated
	 */
	int AADL_STRING = 1;

	/**
	 * The number of structural features of the '<em>Aadl String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_STRING_FEATURE_COUNT = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ANDImpl <em>AND</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ANDImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getAND()
	 * @generated
	 */
	int AND = 2;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND__MIXED = 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND__CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Condition Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND__CONDITION_GROUP = 2;

	/**
	 * The feature id for the '<em><b>AND</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND__AND = 3;

	/**
	 * The feature id for the '<em><b>OR</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND__OR = 4;

	/**
	 * The feature id for the '<em><b>NOT</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND__NOT = 5;

	/**
	 * The number of structural features of the '<em>AND</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ClassifierTypeImpl <em>Classifier Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ClassifierTypeImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getClassifierType()
	 * @generated
	 */
	int CLASSIFIER_TYPE = 3;

	/**
	 * The number of structural features of the '<em>Classifier Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_TYPE_FEATURE_COUNT = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ConditionImpl <em>Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ConditionImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getCondition()
	 * @generated
	 */
	int CONDITION = 4;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__VALUE = 0;

	/**
	 * The number of structural features of the '<em>Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.DataFieldImpl <em>Data Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.DataFieldImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getDataField()
	 * @generated
	 */
	int DATA_FIELD = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FIELD__NAME = 0;

	/**
	 * The number of structural features of the '<em>Data Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FIELD_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.DataFileImpl <em>Data File</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.DataFileImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getDataFile()
	 * @generated
	 */
	int DATA_FILE = 6;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FILE__MIXED = 0;

	/**
	 * The feature id for the '<em><b>Data File Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FILE__DATA_FILE_GROUP = 1;

	/**
	 * The feature id for the '<em><b>Slash</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FILE__SLASH = 2;

	/**
	 * The feature id for the '<em><b>Text</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FILE__TEXT = 3;

	/**
	 * The feature id for the '<em><b>Internal Procedure</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FILE__INTERNAL_PROCEDURE = 4;

	/**
	 * The number of structural features of the '<em>Data File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FILE_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.DataRecoveringImpl <em>Data Recovering</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.DataRecoveringImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getDataRecovering()
	 * @generated
	 */
	int DATA_RECOVERING = 7;

	/**
	 * The feature id for the '<em><b>Internal Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RECOVERING__INTERNAL_PROCEDURE = 0;

	/**
	 * The feature id for the '<em><b>Data File</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RECOVERING__DATA_FILE = 1;

	/**
	 * The feature id for the '<em><b>Data Field</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RECOVERING__DATA_FIELD = 2;

	/**
	 * The number of structural features of the '<em>Data Recovering</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RECOVERING_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.DocumentRootImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 8;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>OD Systems</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OD_SYSTEMS = 3;

	/**
	 * The feature id for the '<em><b>Property Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__PROPERTY_SET = 4;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.EnumerationTypeImpl <em>Enumeration Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.EnumerationTypeImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getEnumerationType()
	 * @generated
	 */
	int ENUMERATION_TYPE = 9;

	/**
	 * The feature id for the '<em><b>Item</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_TYPE__ITEM = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Enumeration Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_TYPE_FEATURE_COUNT = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.IntegerRangeImpl <em>Integer Range</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.IntegerRangeImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getIntegerRange()
	 * @generated
	 */
	int INTEGER_RANGE = 10;

	/**
	 * The feature id for the '<em><b>Integer Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_RANGE__INTEGER_UPPER_BOUND = 0;

	/**
	 * The feature id for the '<em><b>Integer Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_RANGE__INTEGER_LOWER_BOUND = 1;

	/**
	 * The feature id for the '<em><b>Constant Upper Bound</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_RANGE__CONSTANT_UPPER_BOUND = 2;

	/**
	 * The feature id for the '<em><b>Constant Lower Bound</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_RANGE__CONSTANT_LOWER_BOUND = 3;

	/**
	 * The number of structural features of the '<em>Integer Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_RANGE_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.InternalProcedureImpl <em>Internal Procedure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.InternalProcedureImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getInternalProcedure()
	 * @generated
	 */
	int INTERNAL_PROCEDURE = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_PROCEDURE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Subject</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_PROCEDURE__SUBJECT = 1;

	/**
	 * The number of structural features of the '<em>Internal Procedure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_PROCEDURE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.LabelImpl <em>Label</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.LabelImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getLabel()
	 * @generated
	 */
	int LABEL = 12;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__FORMAT = 1;

	/**
	 * The feature id for the '<em><b>Procedure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__PROCEDURE = 2;

	/**
	 * The feature id for the '<em><b>Subject</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__SUBJECT = 3;

	/**
	 * The number of structural features of the '<em>Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.LoopConditionImpl <em>Loop Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.LoopConditionImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getLoopCondition()
	 * @generated
	 */
	int LOOP_CONDITION = 13;

	/**
	 * The feature id for the '<em><b>OD Section</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_CONDITION__OD_SECTION = 0;

	/**
	 * The feature id for the '<em><b>Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_CONDITION__MODE = 1;

	/**
	 * The feature id for the '<em><b>Procedure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_CONDITION__PROCEDURE = 2;

	/**
	 * The number of structural features of the '<em>Loop Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_CONDITION_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.NOTImpl <em>NOT</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.NOTImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getNOT()
	 * @generated
	 */
	int NOT = 14;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT__CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Condition Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT__CONDITION_GROUP = 2;

	/**
	 * The feature id for the '<em><b>AND</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT__AND = 3;

	/**
	 * The feature id for the '<em><b>OR</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT__OR = 4;

	/**
	 * The feature id for the '<em><b>NOT</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT__NOT = 5;

	/**
	 * The number of structural features of the '<em>NOT</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.NumberTypeImpl <em>Number Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.NumberTypeImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getNumberType()
	 * @generated
	 */
	int NUMBER_TYPE = 15;

	/**
	 * The feature id for the '<em><b>Integer Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_TYPE__INTEGER_RANGE = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Real Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_TYPE__REAL_RANGE = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_TYPE__TYPE = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Units</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_TYPE__UNITS = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_TYPE__UNIT = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Number Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_TYPE_FEATURE_COUNT = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ObjectConditionImpl <em>Object Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ObjectConditionImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getObjectCondition()
	 * @generated
	 */
	int OBJECT_CONDITION = 16;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONDITION__MIXED = 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONDITION__CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Condition Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONDITION__CONDITION_GROUP = 2;

	/**
	 * The feature id for the '<em><b>AND</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONDITION__AND = 3;

	/**
	 * The feature id for the '<em><b>OR</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONDITION__OR = 4;

	/**
	 * The feature id for the '<em><b>NOT</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONDITION__NOT = 5;

	/**
	 * The number of structural features of the '<em>Object Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONDITION_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSectionImpl <em>OD Section</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ODSectionImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getODSection()
	 * @generated
	 */
	int OD_SECTION = 17;

	/**
	 * The feature id for the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SECTION__LABEL = 0;

	/**
	 * The feature id for the '<em><b>Object Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SECTION__OBJECT_CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Data Recovering</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SECTION__DATA_RECOVERING = 2;

	/**
	 * The feature id for the '<em><b>OD Section</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SECTION__OD_SECTION = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SECTION__ID = 4;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SECTION__TYPE = 5;

	/**
	 * The number of structural features of the '<em>OD Section</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SECTION_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSystemImpl <em>OD System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ODSystemImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getODSystem()
	 * @generated
	 */
	int OD_SYSTEM = 18;

	/**
	 * The feature id for the '<em><b>OD Section</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SYSTEM__OD_SECTION = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SYSTEM__ID = 1;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SYSTEM__LABEL = 2;

	/**
	 * The feature id for the '<em><b>Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SYSTEM__MODE = 3;

	/**
	 * The feature id for the '<em><b>Style Sheet</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SYSTEM__STYLE_SHEET = 4;

	/**
	 * The feature id for the '<em><b>Data Recovering</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SYSTEM__DATA_RECOVERING = 5;

	/**
	 * The number of structural features of the '<em>OD System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SYSTEM_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSystemsTypeImpl <em>OD Systems Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ODSystemsTypeImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getODSystemsType()
	 * @generated
	 */
	int OD_SYSTEMS_TYPE = 19;

	/**
	 * The feature id for the '<em><b>OD System</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SYSTEMS_TYPE__OD_SYSTEM = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SYSTEMS_TYPE__ID = 1;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SYSTEMS_TYPE__LABEL = 2;

	/**
	 * The feature id for the '<em><b>Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SYSTEMS_TYPE__MODE = 3;

	/**
	 * The number of structural features of the '<em>OD Systems Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OD_SYSTEMS_TYPE_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ORImpl <em>OR</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ORImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getOR()
	 * @generated
	 */
	int OR = 20;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR__MIXED = 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR__CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Condition Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR__CONDITION_GROUP = 2;

	/**
	 * The feature id for the '<em><b>AND</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR__AND = 3;

	/**
	 * The feature id for the '<em><b>OR</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR__OR = 4;

	/**
	 * The feature id for the '<em><b>NOT</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR__NOT = 5;

	/**
	 * The number of structural features of the '<em>OR</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyConstantImpl <em>Property Constant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.PropertyConstantImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getPropertyConstant()
	 * @generated
	 */
	int PROPERTY_CONSTANT = 22;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_CONSTANT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Values</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_CONSTANT__VALUES = 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_CONSTANT__TYPE = 2;

	/**
	 * The number of structural features of the '<em>Property Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_CONSTANT_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyDefinitionImpl <em>Property Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.PropertyDefinitionImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getPropertyDefinition()
	 * @generated
	 */
	int PROPERTY_DEFINITION = 23;

	/**
	 * The feature id for the '<em><b>Default</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_DEFINITION__DEFAULT = 0;

	/**
	 * The feature id for the '<em><b>Is Multi Valued</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_DEFINITION__IS_MULTI_VALUED = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_DEFINITION__NAME = 2;

	/**
	 * The feature id for the '<em><b>Property Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_DEFINITION__PROPERTY_CONDITION = 3;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_DEFINITION__TYPE = 4;

	/**
	 * The number of structural features of the '<em>Property Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_DEFINITION_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertySetTypeImpl <em>Property Set Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.PropertySetTypeImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getPropertySetType()
	 * @generated
	 */
	int PROPERTY_SET_TYPE = 24;

	/**
	 * The feature id for the '<em><b>Property Type</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SET_TYPE__PROPERTY_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Property Definition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SET_TYPE__PROPERTY_DEFINITION = 1;

	/**
	 * The feature id for the '<em><b>Property Constant</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SET_TYPE__PROPERTY_CONSTANT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SET_TYPE__NAME = 3;

	/**
	 * The number of structural features of the '<em>Property Set Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SET_TYPE_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyTypeImpl <em>Property Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.PropertyTypeImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getPropertyType()
	 * @generated
	 */
	int PROPERTY_TYPE = 25;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE__NAME = TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Record Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE__RECORD_TYPE = TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Classifier Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE__CLASSIFIER_TYPE = TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Reference Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE__REFERENCE_TYPE = TYPE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Enumeration Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE__ENUMERATION_TYPE = TYPE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Aadl Boolean</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE__AADL_BOOLEAN = TYPE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Aadl String</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE__AADL_STRING = TYPE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Units Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE__UNITS_TYPE = TYPE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Range Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE__RANGE_TYPE = TYPE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Number Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE__NUMBER_TYPE = TYPE_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>Property Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 10;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.RangeTypeImpl <em>Range Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.RangeTypeImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getRangeType()
	 * @generated
	 */
	int RANGE_TYPE = 26;

	/**
	 * The feature id for the '<em><b>Number Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_TYPE__NUMBER_TYPE = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Range Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_TYPE_FEATURE_COUNT = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.RealRangeImpl <em>Real Range</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.RealRangeImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getRealRange()
	 * @generated
	 */
	int REAL_RANGE = 27;

	/**
	 * The feature id for the '<em><b>Real Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_RANGE__REAL_UPPER_BOUND = 0;

	/**
	 * The feature id for the '<em><b>Real Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_RANGE__REAL_LOWER_BOUND = 1;

	/**
	 * The feature id for the '<em><b>Constant Lower Bound</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_RANGE__CONSTANT_LOWER_BOUND = 2;

	/**
	 * The feature id for the '<em><b>Constant Upper Bound</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_RANGE__CONSTANT_UPPER_BOUND = 3;

	/**
	 * The number of structural features of the '<em>Real Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_RANGE_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.RecordFieldTypeImpl <em>Record Field Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.RecordFieldTypeImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getRecordFieldType()
	 * @generated
	 */
	int RECORD_FIELD_TYPE = 28;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_FIELD_TYPE__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_FIELD_TYPE__NAME = 1;

	/**
	 * The number of structural features of the '<em>Record Field Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_FIELD_TYPE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.RecordTypeImpl <em>Record Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.RecordTypeImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getRecordType()
	 * @generated
	 */
	int RECORD_TYPE = 29;

	/**
	 * The feature id for the '<em><b>Record Field</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_TYPE__RECORD_FIELD = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Record Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_TYPE_FEATURE_COUNT = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ReferenceTypeImpl <em>Reference Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ReferenceTypeImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getReferenceType()
	 * @generated
	 */
	int REFERENCE_TYPE = 30;

	/**
	 * The number of structural features of the '<em>Reference Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_TYPE_FEATURE_COUNT = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.SlashImpl <em>Slash</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.SlashImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getSlash()
	 * @generated
	 */
	int SLASH = 31;

	/**
	 * The number of structural features of the '<em>Slash</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLASH_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.TextImpl <em>Text</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.TextImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getText()
	 * @generated
	 */
	int TEXT = 32;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT__VALUE = 0;

	/**
	 * The number of structural features of the '<em>Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.UnitImpl <em>Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.UnitImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getUnit()
	 * @generated
	 */
	int UNIT = 33;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__UNIT = 0;

	/**
	 * The feature id for the '<em><b>Multiplier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__MULTIPLIER = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__NAME = 2;

	/**
	 * The number of structural features of the '<em>Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.UnitsTypeImpl <em>Units Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.UnitsTypeImpl
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getUnitsType()
	 * @generated
	 */
	int UNITS_TYPE = 34;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITS_TYPE__UNIT = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Units Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITS_TYPE_FEATURE_COUNT = PRIMITIV_TYPES_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.ConditionValue <em>Condition Value</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.ConditionValue
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getConditionValue()
	 * @generated
	 */
	int CONDITION_VALUE = 36;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.LoopMode <em>Loop Mode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.LoopMode
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getLoopMode()
	 * @generated
	 */
	int LOOP_MODE = 37;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.Mode <em>Mode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.Mode
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getMode()
	 * @generated
	 */
	int MODE = 38;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.NumericType <em>Numeric Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.NumericType
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getNumericType()
	 * @generated
	 */
	int NUMERIC_TYPE = 39;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.Procedure <em>Procedure</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.Procedure
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getProcedure()
	 * @generated
	 */
	int PROCEDURE = 40;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.SubjectType <em>Subject Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.SubjectType
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getSubjectType()
	 * @generated
	 */
	int SUBJECT_TYPE = 41;

	/**
	 * The meta object id for the '{@link org.topcased.adele.odsConfig.OdsConfig.SubjectType1 <em>Subject Type1</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.topcased.adele.odsConfig.OdsConfig.SubjectType1
	 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getSubjectType1()
	 * @generated
	 */
	int SUBJECT_TYPE1 = 42;


	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.AadlBoolean <em>Aadl Boolean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aadl Boolean</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.AadlBoolean
	 * @generated
	 */
	EClass getAadlBoolean();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.AadlString <em>Aadl String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aadl String</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.AadlString
	 * @generated
	 */
	EClass getAadlString();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.AND <em>AND</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AND</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.AND
	 * @generated
	 */
	EClass getAND();

	/**
	 * Returns the meta object for the attribute list '{@link org.topcased.adele.odsConfig.OdsConfig.AND#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.AND#getMixed()
	 * @see #getAND()
	 * @generated
	 */
	EAttribute getAND_Mixed();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.AND#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Condition</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.AND#getCondition()
	 * @see #getAND()
	 * @generated
	 */
	EReference getAND_Condition();

	/**
	 * Returns the meta object for the attribute list '{@link org.topcased.adele.odsConfig.OdsConfig.AND#getConditionGroup <em>Condition Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Condition Group</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.AND#getConditionGroup()
	 * @see #getAND()
	 * @generated
	 */
	EAttribute getAND_ConditionGroup();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.AND#getAND <em>AND</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>AND</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.AND#getAND()
	 * @see #getAND()
	 * @generated
	 */
	EReference getAND_AND();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.AND#getOR <em>OR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>OR</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.AND#getOR()
	 * @see #getAND()
	 * @generated
	 */
	EReference getAND_OR();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.AND#getNOT <em>NOT</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>NOT</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.AND#getNOT()
	 * @see #getAND()
	 * @generated
	 */
	EReference getAND_NOT();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.ClassifierType <em>Classifier Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Classifier Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ClassifierType
	 * @generated
	 */
	EClass getClassifierType();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.Condition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Condition
	 * @generated
	 */
	EClass getCondition();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.Condition#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Condition#getValue()
	 * @see #getCondition()
	 * @generated
	 */
	EAttribute getCondition_Value();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.DataField <em>Data Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Field</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DataField
	 * @generated
	 */
	EClass getDataField();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.DataField#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DataField#getName()
	 * @see #getDataField()
	 * @generated
	 */
	EAttribute getDataField_Name();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.DataFile <em>Data File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data File</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DataFile
	 * @generated
	 */
	EClass getDataFile();

	/**
	 * Returns the meta object for the attribute list '{@link org.topcased.adele.odsConfig.OdsConfig.DataFile#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DataFile#getMixed()
	 * @see #getDataFile()
	 * @generated
	 */
	EAttribute getDataFile_Mixed();

	/**
	 * Returns the meta object for the attribute list '{@link org.topcased.adele.odsConfig.OdsConfig.DataFile#getDataFileGroup <em>Data File Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Data File Group</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DataFile#getDataFileGroup()
	 * @see #getDataFile()
	 * @generated
	 */
	EAttribute getDataFile_DataFileGroup();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.DataFile#getSlash <em>Slash</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Slash</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DataFile#getSlash()
	 * @see #getDataFile()
	 * @generated
	 */
	EReference getDataFile_Slash();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.DataFile#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Text</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DataFile#getText()
	 * @see #getDataFile()
	 * @generated
	 */
	EReference getDataFile_Text();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.DataFile#getInternalProcedure <em>Internal Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Internal Procedure</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DataFile#getInternalProcedure()
	 * @see #getDataFile()
	 * @generated
	 */
	EReference getDataFile_InternalProcedure();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.DataRecovering <em>Data Recovering</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Recovering</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DataRecovering
	 * @generated
	 */
	EClass getDataRecovering();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.DataRecovering#getInternalProcedure <em>Internal Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Internal Procedure</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DataRecovering#getInternalProcedure()
	 * @see #getDataRecovering()
	 * @generated
	 */
	EReference getDataRecovering_InternalProcedure();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.DataRecovering#getDataFile <em>Data File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data File</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DataRecovering#getDataFile()
	 * @see #getDataRecovering()
	 * @generated
	 */
	EReference getDataRecovering_DataFile();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.DataRecovering#getDataField <em>Data Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Field</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DataRecovering#getDataField()
	 * @see #getDataRecovering()
	 * @generated
	 */
	EReference getDataRecovering_DataField();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link org.topcased.adele.odsConfig.OdsConfig.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link org.topcased.adele.odsConfig.OdsConfig.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link org.topcased.adele.odsConfig.OdsConfig.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.DocumentRoot#getODSystems <em>OD Systems</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>OD Systems</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DocumentRoot#getODSystems()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ODSystems();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.DocumentRoot#getPropertySet <em>Property Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Property Set</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.DocumentRoot#getPropertySet()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_PropertySet();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.EnumerationType <em>Enumeration Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enumeration Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.EnumerationType
	 * @generated
	 */
	EClass getEnumerationType();

	/**
	 * Returns the meta object for the attribute list '{@link org.topcased.adele.odsConfig.OdsConfig.EnumerationType#getItem <em>Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Item</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.EnumerationType#getItem()
	 * @see #getEnumerationType()
	 * @generated
	 */
	EAttribute getEnumerationType_Item();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.IntegerRange <em>Integer Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Range</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.IntegerRange
	 * @generated
	 */
	EClass getIntegerRange();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.IntegerRange#getIntegerUpperBound <em>Integer Upper Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integer Upper Bound</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.IntegerRange#getIntegerUpperBound()
	 * @see #getIntegerRange()
	 * @generated
	 */
	EAttribute getIntegerRange_IntegerUpperBound();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.IntegerRange#getIntegerLowerBound <em>Integer Lower Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integer Lower Bound</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.IntegerRange#getIntegerLowerBound()
	 * @see #getIntegerRange()
	 * @generated
	 */
	EAttribute getIntegerRange_IntegerLowerBound();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.adele.odsConfig.OdsConfig.IntegerRange#getConstantUpperBound <em>Constant Upper Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constant Upper Bound</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.IntegerRange#getConstantUpperBound()
	 * @see #getIntegerRange()
	 * @generated
	 */
	EReference getIntegerRange_ConstantUpperBound();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.adele.odsConfig.OdsConfig.IntegerRange#getConstantLowerBound <em>Constant Lower Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constant Lower Bound</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.IntegerRange#getConstantLowerBound()
	 * @see #getIntegerRange()
	 * @generated
	 */
	EReference getIntegerRange_ConstantLowerBound();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.InternalProcedure <em>Internal Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Internal Procedure</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.InternalProcedure
	 * @generated
	 */
	EClass getInternalProcedure();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.InternalProcedure#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.InternalProcedure#getName()
	 * @see #getInternalProcedure()
	 * @generated
	 */
	EAttribute getInternalProcedure_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.InternalProcedure#getSubject <em>Subject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Subject</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.InternalProcedure#getSubject()
	 * @see #getInternalProcedure()
	 * @generated
	 */
	EAttribute getInternalProcedure_Subject();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.Label <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Label</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Label
	 * @generated
	 */
	EClass getLabel();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.Label#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Label#getValue()
	 * @see #getLabel()
	 * @generated
	 */
	EAttribute getLabel_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.Label#getFormat <em>Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Format</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Label#getFormat()
	 * @see #getLabel()
	 * @generated
	 */
	EAttribute getLabel_Format();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.Label#getProcedure <em>Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Procedure</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Label#getProcedure()
	 * @see #getLabel()
	 * @generated
	 */
	EAttribute getLabel_Procedure();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.Label#getSubject <em>Subject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Subject</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Label#getSubject()
	 * @see #getLabel()
	 * @generated
	 */
	EAttribute getLabel_Subject();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.LoopCondition <em>Loop Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Loop Condition</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.LoopCondition
	 * @generated
	 */
	EClass getLoopCondition();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.LoopCondition#getODSection <em>OD Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>OD Section</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.LoopCondition#getODSection()
	 * @see #getLoopCondition()
	 * @generated
	 */
	EReference getLoopCondition_ODSection();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.LoopCondition#getMode <em>Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mode</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.LoopCondition#getMode()
	 * @see #getLoopCondition()
	 * @generated
	 */
	EAttribute getLoopCondition_Mode();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.LoopCondition#getProcedure <em>Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Procedure</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.LoopCondition#getProcedure()
	 * @see #getLoopCondition()
	 * @generated
	 */
	EAttribute getLoopCondition_Procedure();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.NOT <em>NOT</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NOT</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.NOT
	 * @generated
	 */
	EClass getNOT();

	/**
	 * Returns the meta object for the attribute list '{@link org.topcased.adele.odsConfig.OdsConfig.NOT#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.NOT#getMixed()
	 * @see #getNOT()
	 * @generated
	 */
	EAttribute getNOT_Mixed();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.NOT#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Condition</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.NOT#getCondition()
	 * @see #getNOT()
	 * @generated
	 */
	EReference getNOT_Condition();

	/**
	 * Returns the meta object for the attribute list '{@link org.topcased.adele.odsConfig.OdsConfig.NOT#getConditionGroup <em>Condition Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Condition Group</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.NOT#getConditionGroup()
	 * @see #getNOT()
	 * @generated
	 */
	EAttribute getNOT_ConditionGroup();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.NOT#getAND <em>AND</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>AND</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.NOT#getAND()
	 * @see #getNOT()
	 * @generated
	 */
	EReference getNOT_AND();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.NOT#getOR <em>OR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>OR</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.NOT#getOR()
	 * @see #getNOT()
	 * @generated
	 */
	EReference getNOT_OR();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.NOT#getNOT <em>NOT</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>NOT</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.NOT#getNOT()
	 * @see #getNOT()
	 * @generated
	 */
	EReference getNOT_NOT();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.NumberType <em>Number Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Number Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.NumberType
	 * @generated
	 */
	EClass getNumberType();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.NumberType#getIntegerRange <em>Integer Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Integer Range</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.NumberType#getIntegerRange()
	 * @see #getNumberType()
	 * @generated
	 */
	EReference getNumberType_IntegerRange();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.NumberType#getRealRange <em>Real Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Real Range</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.NumberType#getRealRange()
	 * @see #getNumberType()
	 * @generated
	 */
	EReference getNumberType_RealRange();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.NumberType#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.NumberType#getType()
	 * @see #getNumberType()
	 * @generated
	 */
	EAttribute getNumberType_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.NumberType#getUnits <em>Units</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Units</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.NumberType#getUnits()
	 * @see #getNumberType()
	 * @generated
	 */
	EReference getNumberType_Units();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.adele.odsConfig.OdsConfig.NumberType#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Unit</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.NumberType#getUnit()
	 * @see #getNumberType()
	 * @generated
	 */
	EReference getNumberType_Unit();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.ObjectCondition <em>Object Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Condition</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ObjectCondition
	 * @generated
	 */
	EClass getObjectCondition();

	/**
	 * Returns the meta object for the attribute list '{@link org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getMixed()
	 * @see #getObjectCondition()
	 * @generated
	 */
	EAttribute getObjectCondition_Mixed();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getCondition()
	 * @see #getObjectCondition()
	 * @generated
	 */
	EReference getObjectCondition_Condition();

	/**
	 * Returns the meta object for the attribute list '{@link org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getConditionGroup <em>Condition Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Condition Group</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getConditionGroup()
	 * @see #getObjectCondition()
	 * @generated
	 */
	EAttribute getObjectCondition_ConditionGroup();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getAND <em>AND</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>AND</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getAND()
	 * @see #getObjectCondition()
	 * @generated
	 */
	EReference getObjectCondition_AND();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getOR <em>OR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>OR</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getOR()
	 * @see #getObjectCondition()
	 * @generated
	 */
	EReference getObjectCondition_OR();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getNOT <em>NOT</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>NOT</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getNOT()
	 * @see #getObjectCondition()
	 * @generated
	 */
	EReference getObjectCondition_NOT();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.ODSection <em>OD Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OD Section</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSection
	 * @generated
	 */
	EClass getODSection();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Label</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSection#getLabel()
	 * @see #getODSection()
	 * @generated
	 */
	EReference getODSection_Label();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getObjectCondition <em>Object Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object Condition</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSection#getObjectCondition()
	 * @see #getODSection()
	 * @generated
	 */
	EReference getODSection_ObjectCondition();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getDataRecovering <em>Data Recovering</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Recovering</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSection#getDataRecovering()
	 * @see #getODSection()
	 * @generated
	 */
	EReference getODSection_DataRecovering();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getODSection <em>OD Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>OD Section</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSection#getODSection()
	 * @see #getODSection()
	 * @generated
	 */
	EReference getODSection_ODSection();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSection#getId()
	 * @see #getODSection()
	 * @generated
	 */
	EAttribute getODSection_Id();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSection#getType()
	 * @see #getODSection()
	 * @generated
	 */
	EReference getODSection_Type();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem <em>OD System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OD System</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSystem
	 * @generated
	 */
	EClass getODSystem();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getODSection <em>OD Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>OD Section</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSystem#getODSection()
	 * @see #getODSystem()
	 * @generated
	 */
	EReference getODSystem_ODSection();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSystem#getId()
	 * @see #getODSystem()
	 * @generated
	 */
	EAttribute getODSystem_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSystem#getLabel()
	 * @see #getODSystem()
	 * @generated
	 */
	EAttribute getODSystem_Label();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getMode <em>Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mode</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSystem#getMode()
	 * @see #getODSystem()
	 * @generated
	 */
	EAttribute getODSystem_Mode();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getStyleSheet <em>Style Sheet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Style Sheet</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSystem#getStyleSheet()
	 * @see #getODSystem()
	 * @generated
	 */
	EAttribute getODSystem_StyleSheet();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getDataRecovering <em>Data Recovering</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Recovering</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSystem#getDataRecovering()
	 * @see #getODSystem()
	 * @generated
	 */
	EReference getODSystem_DataRecovering();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystemsType <em>OD Systems Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OD Systems Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSystemsType
	 * @generated
	 */
	EClass getODSystemsType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getODSystem <em>OD System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>OD System</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getODSystem()
	 * @see #getODSystemsType()
	 * @generated
	 */
	EReference getODSystemsType_ODSystem();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getId()
	 * @see #getODSystemsType()
	 * @generated
	 */
	EAttribute getODSystemsType_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getLabel()
	 * @see #getODSystemsType()
	 * @generated
	 */
	EAttribute getODSystemsType_Label();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getMode <em>Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mode</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getMode()
	 * @see #getODSystemsType()
	 * @generated
	 */
	EAttribute getODSystemsType_Mode();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.OR <em>OR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OR</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OR
	 * @generated
	 */
	EClass getOR();

	/**
	 * Returns the meta object for the attribute list '{@link org.topcased.adele.odsConfig.OdsConfig.OR#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OR#getMixed()
	 * @see #getOR()
	 * @generated
	 */
	EAttribute getOR_Mixed();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.OR#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Condition</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OR#getCondition()
	 * @see #getOR()
	 * @generated
	 */
	EReference getOR_Condition();

	/**
	 * Returns the meta object for the attribute list '{@link org.topcased.adele.odsConfig.OdsConfig.OR#getConditionGroup <em>Condition Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Condition Group</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OR#getConditionGroup()
	 * @see #getOR()
	 * @generated
	 */
	EAttribute getOR_ConditionGroup();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.OR#getAND <em>AND</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>AND</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OR#getAND()
	 * @see #getOR()
	 * @generated
	 */
	EReference getOR_AND();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.OR#getOR <em>OR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>OR</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OR#getOR()
	 * @see #getOR()
	 * @generated
	 */
	EReference getOR_OR();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.OR#getNOT <em>NOT</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>NOT</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OR#getNOT()
	 * @see #getOR()
	 * @generated
	 */
	EReference getOR_NOT();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.PrimitivTypesType <em>Primitiv Types Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Primitiv Types Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PrimitivTypesType
	 * @generated
	 */
	EClass getPrimitivTypesType();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyConstant <em>Property Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property Constant</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyConstant
	 * @generated
	 */
	EClass getPropertyConstant();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyConstant#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyConstant#getName()
	 * @see #getPropertyConstant()
	 * @generated
	 */
	EAttribute getPropertyConstant_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyConstant#getValues <em>Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Values</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyConstant#getValues()
	 * @see #getPropertyConstant()
	 * @generated
	 */
	EAttribute getPropertyConstant_Values();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyConstant#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyConstant#getType()
	 * @see #getPropertyConstant()
	 * @generated
	 */
	EReference getPropertyConstant_Type();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition <em>Property Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property Definition</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition
	 * @generated
	 */
	EClass getPropertyDefinition();

	/**
	 * Returns the meta object for the attribute list '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#getDefault <em>Default</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Default</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#getDefault()
	 * @see #getPropertyDefinition()
	 * @generated
	 */
	EAttribute getPropertyDefinition_Default();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#isIsMultiValued <em>Is Multi Valued</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Multi Valued</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#isIsMultiValued()
	 * @see #getPropertyDefinition()
	 * @generated
	 */
	EAttribute getPropertyDefinition_IsMultiValued();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#getName()
	 * @see #getPropertyDefinition()
	 * @generated
	 */
	EAttribute getPropertyDefinition_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#getPropertyCondition <em>Property Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Property Condition</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#getPropertyCondition()
	 * @see #getPropertyDefinition()
	 * @generated
	 */
	EReference getPropertyDefinition_PropertyCondition();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#getType()
	 * @see #getPropertyDefinition()
	 * @generated
	 */
	EReference getPropertyDefinition_Type();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.PropertySetType <em>Property Set Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property Set Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertySetType
	 * @generated
	 */
	EClass getPropertySetType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.PropertySetType#getPropertyType <em>Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Property Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertySetType#getPropertyType()
	 * @see #getPropertySetType()
	 * @generated
	 */
	EReference getPropertySetType_PropertyType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.PropertySetType#getPropertyDefinition <em>Property Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Property Definition</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertySetType#getPropertyDefinition()
	 * @see #getPropertySetType()
	 * @generated
	 */
	EReference getPropertySetType_PropertyDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.PropertySetType#getPropertyConstant <em>Property Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Property Constant</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertySetType#getPropertyConstant()
	 * @see #getPropertySetType()
	 * @generated
	 */
	EReference getPropertySetType_PropertyConstant();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.PropertySetType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertySetType#getName()
	 * @see #getPropertySetType()
	 * @generated
	 */
	EAttribute getPropertySetType_Name();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType <em>Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyType
	 * @generated
	 */
	EClass getPropertyType();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyType#getName()
	 * @see #getPropertyType()
	 * @generated
	 */
	EAttribute getPropertyType_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getRecordType <em>Record Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Record Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyType#getRecordType()
	 * @see #getPropertyType()
	 * @generated
	 */
	EReference getPropertyType_RecordType();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getClassifierType <em>Classifier Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Classifier Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyType#getClassifierType()
	 * @see #getPropertyType()
	 * @generated
	 */
	EReference getPropertyType_ClassifierType();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getReferenceType <em>Reference Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reference Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyType#getReferenceType()
	 * @see #getPropertyType()
	 * @generated
	 */
	EReference getPropertyType_ReferenceType();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getEnumerationType <em>Enumeration Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Enumeration Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyType#getEnumerationType()
	 * @see #getPropertyType()
	 * @generated
	 */
	EReference getPropertyType_EnumerationType();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getAadlBoolean <em>Aadl Boolean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Aadl Boolean</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyType#getAadlBoolean()
	 * @see #getPropertyType()
	 * @generated
	 */
	EReference getPropertyType_AadlBoolean();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getAadlString <em>Aadl String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Aadl String</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyType#getAadlString()
	 * @see #getPropertyType()
	 * @generated
	 */
	EReference getPropertyType_AadlString();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getUnitsType <em>Units Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Units Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyType#getUnitsType()
	 * @see #getPropertyType()
	 * @generated
	 */
	EReference getPropertyType_UnitsType();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getRangeType <em>Range Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Range Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyType#getRangeType()
	 * @see #getPropertyType()
	 * @generated
	 */
	EReference getPropertyType_RangeType();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getNumberType <em>Number Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Number Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.PropertyType#getNumberType()
	 * @see #getPropertyType()
	 * @generated
	 */
	EReference getPropertyType_NumberType();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.RangeType <em>Range Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Range Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.RangeType
	 * @generated
	 */
	EClass getRangeType();

	/**
	 * Returns the meta object for the containment reference '{@link org.topcased.adele.odsConfig.OdsConfig.RangeType#getNumberType <em>Number Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Number Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.RangeType#getNumberType()
	 * @see #getRangeType()
	 * @generated
	 */
	EReference getRangeType_NumberType();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.RealRange <em>Real Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Real Range</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.RealRange
	 * @generated
	 */
	EClass getRealRange();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.RealRange#getRealUpperBound <em>Real Upper Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Real Upper Bound</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.RealRange#getRealUpperBound()
	 * @see #getRealRange()
	 * @generated
	 */
	EAttribute getRealRange_RealUpperBound();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.RealRange#getRealLowerBound <em>Real Lower Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Real Lower Bound</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.RealRange#getRealLowerBound()
	 * @see #getRealRange()
	 * @generated
	 */
	EAttribute getRealRange_RealLowerBound();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.adele.odsConfig.OdsConfig.RealRange#getConstantLowerBound <em>Constant Lower Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constant Lower Bound</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.RealRange#getConstantLowerBound()
	 * @see #getRealRange()
	 * @generated
	 */
	EReference getRealRange_ConstantLowerBound();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.adele.odsConfig.OdsConfig.RealRange#getConstantUpperBound <em>Constant Upper Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constant Upper Bound</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.RealRange#getConstantUpperBound()
	 * @see #getRealRange()
	 * @generated
	 */
	EReference getRealRange_ConstantUpperBound();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.RecordFieldType <em>Record Field Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Record Field Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.RecordFieldType
	 * @generated
	 */
	EClass getRecordFieldType();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.adele.odsConfig.OdsConfig.RecordFieldType#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.RecordFieldType#getType()
	 * @see #getRecordFieldType()
	 * @generated
	 */
	EReference getRecordFieldType_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.RecordFieldType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.RecordFieldType#getName()
	 * @see #getRecordFieldType()
	 * @generated
	 */
	EAttribute getRecordFieldType_Name();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.RecordType <em>Record Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Record Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.RecordType
	 * @generated
	 */
	EClass getRecordType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.RecordType#getRecordField <em>Record Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Record Field</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.RecordType#getRecordField()
	 * @see #getRecordType()
	 * @generated
	 */
	EReference getRecordType_RecordField();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.ReferenceType <em>Reference Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ReferenceType
	 * @generated
	 */
	EClass getReferenceType();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.Slash <em>Slash</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Slash</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Slash
	 * @generated
	 */
	EClass getSlash();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.Text <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Text</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Text
	 * @generated
	 */
	EClass getText();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.Text#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Text#getValue()
	 * @see #getText()
	 * @generated
	 */
	EAttribute getText_Value();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.Unit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unit</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Unit
	 * @generated
	 */
	EClass getUnit();

	/**
	 * Returns the meta object for the reference '{@link org.topcased.adele.odsConfig.OdsConfig.Unit#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Unit</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Unit#getUnit()
	 * @see #getUnit()
	 * @generated
	 */
	EReference getUnit_Unit();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.Unit#getMultiplier <em>Multiplier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Multiplier</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Unit#getMultiplier()
	 * @see #getUnit()
	 * @generated
	 */
	EAttribute getUnit_Multiplier();

	/**
	 * Returns the meta object for the attribute '{@link org.topcased.adele.odsConfig.OdsConfig.Unit#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Unit#getName()
	 * @see #getUnit()
	 * @generated
	 */
	EAttribute getUnit_Name();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.UnitsType <em>Units Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Units Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.UnitsType
	 * @generated
	 */
	EClass getUnitsType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.topcased.adele.odsConfig.OdsConfig.UnitsType#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Unit</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.UnitsType#getUnit()
	 * @see #getUnitsType()
	 * @generated
	 */
	EReference getUnitsType_Unit();

	/**
	 * Returns the meta object for class '{@link org.topcased.adele.odsConfig.OdsConfig.Type <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Type
	 * @generated
	 */
	EClass getType();

	/**
	 * Returns the meta object for enum '{@link org.topcased.adele.odsConfig.OdsConfig.ConditionValue <em>Condition Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Condition Value</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ConditionValue
	 * @generated
	 */
	EEnum getConditionValue();

	/**
	 * Returns the meta object for enum '{@link org.topcased.adele.odsConfig.OdsConfig.LoopMode <em>Loop Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Loop Mode</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.LoopMode
	 * @generated
	 */
	EEnum getLoopMode();

	/**
	 * Returns the meta object for enum '{@link org.topcased.adele.odsConfig.OdsConfig.Mode <em>Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Mode</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Mode
	 * @generated
	 */
	EEnum getMode();

	/**
	 * Returns the meta object for enum '{@link org.topcased.adele.odsConfig.OdsConfig.NumericType <em>Numeric Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Numeric Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.NumericType
	 * @generated
	 */
	EEnum getNumericType();

	/**
	 * Returns the meta object for enum '{@link org.topcased.adele.odsConfig.OdsConfig.Procedure <em>Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Procedure</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Procedure
	 * @generated
	 */
	EEnum getProcedure();

	/**
	 * Returns the meta object for enum '{@link org.topcased.adele.odsConfig.OdsConfig.SubjectType <em>Subject Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Subject Type</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.SubjectType
	 * @generated
	 */
	EEnum getSubjectType();

	/**
	 * Returns the meta object for enum '{@link org.topcased.adele.odsConfig.OdsConfig.SubjectType1 <em>Subject Type1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Subject Type1</em>'.
	 * @see org.topcased.adele.odsConfig.OdsConfig.SubjectType1
	 * @generated
	 */
	EEnum getSubjectType1();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OdsConfigFactory getOdsConfigFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.AadlBooleanImpl <em>Aadl Boolean</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.AadlBooleanImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getAadlBoolean()
		 * @generated
		 */
		EClass AADL_BOOLEAN = eINSTANCE.getAadlBoolean();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.AadlStringImpl <em>Aadl String</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.AadlStringImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getAadlString()
		 * @generated
		 */
		EClass AADL_STRING = eINSTANCE.getAadlString();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ANDImpl <em>AND</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ANDImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getAND()
		 * @generated
		 */
		EClass AND = eINSTANCE.getAND();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AND__MIXED = eINSTANCE.getAND_Mixed();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AND__CONDITION = eINSTANCE.getAND_Condition();

		/**
		 * The meta object literal for the '<em><b>Condition Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AND__CONDITION_GROUP = eINSTANCE.getAND_ConditionGroup();

		/**
		 * The meta object literal for the '<em><b>AND</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AND__AND = eINSTANCE.getAND_AND();

		/**
		 * The meta object literal for the '<em><b>OR</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AND__OR = eINSTANCE.getAND_OR();

		/**
		 * The meta object literal for the '<em><b>NOT</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AND__NOT = eINSTANCE.getAND_NOT();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ClassifierTypeImpl <em>Classifier Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ClassifierTypeImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getClassifierType()
		 * @generated
		 */
		EClass CLASSIFIER_TYPE = eINSTANCE.getClassifierType();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ConditionImpl <em>Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ConditionImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getCondition()
		 * @generated
		 */
		EClass CONDITION = eINSTANCE.getCondition();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION__VALUE = eINSTANCE.getCondition_Value();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.DataFieldImpl <em>Data Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.DataFieldImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getDataField()
		 * @generated
		 */
		EClass DATA_FIELD = eINSTANCE.getDataField();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_FIELD__NAME = eINSTANCE.getDataField_Name();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.DataFileImpl <em>Data File</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.DataFileImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getDataFile()
		 * @generated
		 */
		EClass DATA_FILE = eINSTANCE.getDataFile();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_FILE__MIXED = eINSTANCE.getDataFile_Mixed();

		/**
		 * The meta object literal for the '<em><b>Data File Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_FILE__DATA_FILE_GROUP = eINSTANCE.getDataFile_DataFileGroup();

		/**
		 * The meta object literal for the '<em><b>Slash</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_FILE__SLASH = eINSTANCE.getDataFile_Slash();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_FILE__TEXT = eINSTANCE.getDataFile_Text();

		/**
		 * The meta object literal for the '<em><b>Internal Procedure</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_FILE__INTERNAL_PROCEDURE = eINSTANCE.getDataFile_InternalProcedure();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.DataRecoveringImpl <em>Data Recovering</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.DataRecoveringImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getDataRecovering()
		 * @generated
		 */
		EClass DATA_RECOVERING = eINSTANCE.getDataRecovering();

		/**
		 * The meta object literal for the '<em><b>Internal Procedure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_RECOVERING__INTERNAL_PROCEDURE = eINSTANCE.getDataRecovering_InternalProcedure();

		/**
		 * The meta object literal for the '<em><b>Data File</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_RECOVERING__DATA_FILE = eINSTANCE.getDataRecovering_DataFile();

		/**
		 * The meta object literal for the '<em><b>Data Field</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_RECOVERING__DATA_FIELD = eINSTANCE.getDataRecovering_DataField();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.DocumentRootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.DocumentRootImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getDocumentRoot()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>OD Systems</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__OD_SYSTEMS = eINSTANCE.getDocumentRoot_ODSystems();

		/**
		 * The meta object literal for the '<em><b>Property Set</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__PROPERTY_SET = eINSTANCE.getDocumentRoot_PropertySet();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.EnumerationTypeImpl <em>Enumeration Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.EnumerationTypeImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getEnumerationType()
		 * @generated
		 */
		EClass ENUMERATION_TYPE = eINSTANCE.getEnumerationType();

		/**
		 * The meta object literal for the '<em><b>Item</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENUMERATION_TYPE__ITEM = eINSTANCE.getEnumerationType_Item();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.IntegerRangeImpl <em>Integer Range</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.IntegerRangeImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getIntegerRange()
		 * @generated
		 */
		EClass INTEGER_RANGE = eINSTANCE.getIntegerRange();

		/**
		 * The meta object literal for the '<em><b>Integer Upper Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_RANGE__INTEGER_UPPER_BOUND = eINSTANCE.getIntegerRange_IntegerUpperBound();

		/**
		 * The meta object literal for the '<em><b>Integer Lower Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_RANGE__INTEGER_LOWER_BOUND = eINSTANCE.getIntegerRange_IntegerLowerBound();

		/**
		 * The meta object literal for the '<em><b>Constant Upper Bound</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTEGER_RANGE__CONSTANT_UPPER_BOUND = eINSTANCE.getIntegerRange_ConstantUpperBound();

		/**
		 * The meta object literal for the '<em><b>Constant Lower Bound</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTEGER_RANGE__CONSTANT_LOWER_BOUND = eINSTANCE.getIntegerRange_ConstantLowerBound();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.InternalProcedureImpl <em>Internal Procedure</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.InternalProcedureImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getInternalProcedure()
		 * @generated
		 */
		EClass INTERNAL_PROCEDURE = eINSTANCE.getInternalProcedure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERNAL_PROCEDURE__NAME = eINSTANCE.getInternalProcedure_Name();

		/**
		 * The meta object literal for the '<em><b>Subject</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERNAL_PROCEDURE__SUBJECT = eINSTANCE.getInternalProcedure_Subject();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.LabelImpl <em>Label</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.LabelImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getLabel()
		 * @generated
		 */
		EClass LABEL = eINSTANCE.getLabel();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LABEL__VALUE = eINSTANCE.getLabel_Value();

		/**
		 * The meta object literal for the '<em><b>Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LABEL__FORMAT = eINSTANCE.getLabel_Format();

		/**
		 * The meta object literal for the '<em><b>Procedure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LABEL__PROCEDURE = eINSTANCE.getLabel_Procedure();

		/**
		 * The meta object literal for the '<em><b>Subject</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LABEL__SUBJECT = eINSTANCE.getLabel_Subject();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.LoopConditionImpl <em>Loop Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.LoopConditionImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getLoopCondition()
		 * @generated
		 */
		EClass LOOP_CONDITION = eINSTANCE.getLoopCondition();

		/**
		 * The meta object literal for the '<em><b>OD Section</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOOP_CONDITION__OD_SECTION = eINSTANCE.getLoopCondition_ODSection();

		/**
		 * The meta object literal for the '<em><b>Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOOP_CONDITION__MODE = eINSTANCE.getLoopCondition_Mode();

		/**
		 * The meta object literal for the '<em><b>Procedure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOOP_CONDITION__PROCEDURE = eINSTANCE.getLoopCondition_Procedure();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.NOTImpl <em>NOT</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.NOTImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getNOT()
		 * @generated
		 */
		EClass NOT = eINSTANCE.getNOT();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOT__MIXED = eINSTANCE.getNOT_Mixed();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NOT__CONDITION = eINSTANCE.getNOT_Condition();

		/**
		 * The meta object literal for the '<em><b>Condition Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOT__CONDITION_GROUP = eINSTANCE.getNOT_ConditionGroup();

		/**
		 * The meta object literal for the '<em><b>AND</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NOT__AND = eINSTANCE.getNOT_AND();

		/**
		 * The meta object literal for the '<em><b>OR</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NOT__OR = eINSTANCE.getNOT_OR();

		/**
		 * The meta object literal for the '<em><b>NOT</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NOT__NOT = eINSTANCE.getNOT_NOT();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.NumberTypeImpl <em>Number Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.NumberTypeImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getNumberType()
		 * @generated
		 */
		EClass NUMBER_TYPE = eINSTANCE.getNumberType();

		/**
		 * The meta object literal for the '<em><b>Integer Range</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NUMBER_TYPE__INTEGER_RANGE = eINSTANCE.getNumberType_IntegerRange();

		/**
		 * The meta object literal for the '<em><b>Real Range</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NUMBER_TYPE__REAL_RANGE = eINSTANCE.getNumberType_RealRange();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NUMBER_TYPE__TYPE = eINSTANCE.getNumberType_Type();

		/**
		 * The meta object literal for the '<em><b>Units</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NUMBER_TYPE__UNITS = eINSTANCE.getNumberType_Units();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NUMBER_TYPE__UNIT = eINSTANCE.getNumberType_Unit();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ObjectConditionImpl <em>Object Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ObjectConditionImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getObjectCondition()
		 * @generated
		 */
		EClass OBJECT_CONDITION = eINSTANCE.getObjectCondition();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBJECT_CONDITION__MIXED = eINSTANCE.getObjectCondition_Mixed();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_CONDITION__CONDITION = eINSTANCE.getObjectCondition_Condition();

		/**
		 * The meta object literal for the '<em><b>Condition Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBJECT_CONDITION__CONDITION_GROUP = eINSTANCE.getObjectCondition_ConditionGroup();

		/**
		 * The meta object literal for the '<em><b>AND</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_CONDITION__AND = eINSTANCE.getObjectCondition_AND();

		/**
		 * The meta object literal for the '<em><b>OR</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_CONDITION__OR = eINSTANCE.getObjectCondition_OR();

		/**
		 * The meta object literal for the '<em><b>NOT</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_CONDITION__NOT = eINSTANCE.getObjectCondition_NOT();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSectionImpl <em>OD Section</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ODSectionImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getODSection()
		 * @generated
		 */
		EClass OD_SECTION = eINSTANCE.getODSection();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OD_SECTION__LABEL = eINSTANCE.getODSection_Label();

		/**
		 * The meta object literal for the '<em><b>Object Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OD_SECTION__OBJECT_CONDITION = eINSTANCE.getODSection_ObjectCondition();

		/**
		 * The meta object literal for the '<em><b>Data Recovering</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OD_SECTION__DATA_RECOVERING = eINSTANCE.getODSection_DataRecovering();

		/**
		 * The meta object literal for the '<em><b>OD Section</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OD_SECTION__OD_SECTION = eINSTANCE.getODSection_ODSection();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OD_SECTION__ID = eINSTANCE.getODSection_Id();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OD_SECTION__TYPE = eINSTANCE.getODSection_Type();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSystemImpl <em>OD System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ODSystemImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getODSystem()
		 * @generated
		 */
		EClass OD_SYSTEM = eINSTANCE.getODSystem();

		/**
		 * The meta object literal for the '<em><b>OD Section</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OD_SYSTEM__OD_SECTION = eINSTANCE.getODSystem_ODSection();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OD_SYSTEM__ID = eINSTANCE.getODSystem_Id();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OD_SYSTEM__LABEL = eINSTANCE.getODSystem_Label();

		/**
		 * The meta object literal for the '<em><b>Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OD_SYSTEM__MODE = eINSTANCE.getODSystem_Mode();

		/**
		 * The meta object literal for the '<em><b>Style Sheet</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OD_SYSTEM__STYLE_SHEET = eINSTANCE.getODSystem_StyleSheet();

		/**
		 * The meta object literal for the '<em><b>Data Recovering</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OD_SYSTEM__DATA_RECOVERING = eINSTANCE.getODSystem_DataRecovering();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSystemsTypeImpl <em>OD Systems Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ODSystemsTypeImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getODSystemsType()
		 * @generated
		 */
		EClass OD_SYSTEMS_TYPE = eINSTANCE.getODSystemsType();

		/**
		 * The meta object literal for the '<em><b>OD System</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OD_SYSTEMS_TYPE__OD_SYSTEM = eINSTANCE.getODSystemsType_ODSystem();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OD_SYSTEMS_TYPE__ID = eINSTANCE.getODSystemsType_Id();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OD_SYSTEMS_TYPE__LABEL = eINSTANCE.getODSystemsType_Label();

		/**
		 * The meta object literal for the '<em><b>Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OD_SYSTEMS_TYPE__MODE = eINSTANCE.getODSystemsType_Mode();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ORImpl <em>OR</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ORImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getOR()
		 * @generated
		 */
		EClass OR = eINSTANCE.getOR();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OR__MIXED = eINSTANCE.getOR_Mixed();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OR__CONDITION = eINSTANCE.getOR_Condition();

		/**
		 * The meta object literal for the '<em><b>Condition Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OR__CONDITION_GROUP = eINSTANCE.getOR_ConditionGroup();

		/**
		 * The meta object literal for the '<em><b>AND</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OR__AND = eINSTANCE.getOR_AND();

		/**
		 * The meta object literal for the '<em><b>OR</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OR__OR = eINSTANCE.getOR_OR();

		/**
		 * The meta object literal for the '<em><b>NOT</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OR__NOT = eINSTANCE.getOR_NOT();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.PrimitivTypesTypeImpl <em>Primitiv Types Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.PrimitivTypesTypeImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getPrimitivTypesType()
		 * @generated
		 */
		EClass PRIMITIV_TYPES_TYPE = eINSTANCE.getPrimitivTypesType();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyConstantImpl <em>Property Constant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.PropertyConstantImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getPropertyConstant()
		 * @generated
		 */
		EClass PROPERTY_CONSTANT = eINSTANCE.getPropertyConstant();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_CONSTANT__NAME = eINSTANCE.getPropertyConstant_Name();

		/**
		 * The meta object literal for the '<em><b>Values</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_CONSTANT__VALUES = eINSTANCE.getPropertyConstant_Values();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_CONSTANT__TYPE = eINSTANCE.getPropertyConstant_Type();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyDefinitionImpl <em>Property Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.PropertyDefinitionImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getPropertyDefinition()
		 * @generated
		 */
		EClass PROPERTY_DEFINITION = eINSTANCE.getPropertyDefinition();

		/**
		 * The meta object literal for the '<em><b>Default</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_DEFINITION__DEFAULT = eINSTANCE.getPropertyDefinition_Default();

		/**
		 * The meta object literal for the '<em><b>Is Multi Valued</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_DEFINITION__IS_MULTI_VALUED = eINSTANCE.getPropertyDefinition_IsMultiValued();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_DEFINITION__NAME = eINSTANCE.getPropertyDefinition_Name();

		/**
		 * The meta object literal for the '<em><b>Property Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_DEFINITION__PROPERTY_CONDITION = eINSTANCE.getPropertyDefinition_PropertyCondition();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_DEFINITION__TYPE = eINSTANCE.getPropertyDefinition_Type();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertySetTypeImpl <em>Property Set Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.PropertySetTypeImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getPropertySetType()
		 * @generated
		 */
		EClass PROPERTY_SET_TYPE = eINSTANCE.getPropertySetType();

		/**
		 * The meta object literal for the '<em><b>Property Type</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_SET_TYPE__PROPERTY_TYPE = eINSTANCE.getPropertySetType_PropertyType();

		/**
		 * The meta object literal for the '<em><b>Property Definition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_SET_TYPE__PROPERTY_DEFINITION = eINSTANCE.getPropertySetType_PropertyDefinition();

		/**
		 * The meta object literal for the '<em><b>Property Constant</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_SET_TYPE__PROPERTY_CONSTANT = eINSTANCE.getPropertySetType_PropertyConstant();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_SET_TYPE__NAME = eINSTANCE.getPropertySetType_Name();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyTypeImpl <em>Property Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.PropertyTypeImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getPropertyType()
		 * @generated
		 */
		EClass PROPERTY_TYPE = eINSTANCE.getPropertyType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_TYPE__NAME = eINSTANCE.getPropertyType_Name();

		/**
		 * The meta object literal for the '<em><b>Record Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_TYPE__RECORD_TYPE = eINSTANCE.getPropertyType_RecordType();

		/**
		 * The meta object literal for the '<em><b>Classifier Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_TYPE__CLASSIFIER_TYPE = eINSTANCE.getPropertyType_ClassifierType();

		/**
		 * The meta object literal for the '<em><b>Reference Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_TYPE__REFERENCE_TYPE = eINSTANCE.getPropertyType_ReferenceType();

		/**
		 * The meta object literal for the '<em><b>Enumeration Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_TYPE__ENUMERATION_TYPE = eINSTANCE.getPropertyType_EnumerationType();

		/**
		 * The meta object literal for the '<em><b>Aadl Boolean</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_TYPE__AADL_BOOLEAN = eINSTANCE.getPropertyType_AadlBoolean();

		/**
		 * The meta object literal for the '<em><b>Aadl String</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_TYPE__AADL_STRING = eINSTANCE.getPropertyType_AadlString();

		/**
		 * The meta object literal for the '<em><b>Units Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_TYPE__UNITS_TYPE = eINSTANCE.getPropertyType_UnitsType();

		/**
		 * The meta object literal for the '<em><b>Range Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_TYPE__RANGE_TYPE = eINSTANCE.getPropertyType_RangeType();

		/**
		 * The meta object literal for the '<em><b>Number Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_TYPE__NUMBER_TYPE = eINSTANCE.getPropertyType_NumberType();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.RangeTypeImpl <em>Range Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.RangeTypeImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getRangeType()
		 * @generated
		 */
		EClass RANGE_TYPE = eINSTANCE.getRangeType();

		/**
		 * The meta object literal for the '<em><b>Number Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RANGE_TYPE__NUMBER_TYPE = eINSTANCE.getRangeType_NumberType();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.RealRangeImpl <em>Real Range</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.RealRangeImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getRealRange()
		 * @generated
		 */
		EClass REAL_RANGE = eINSTANCE.getRealRange();

		/**
		 * The meta object literal for the '<em><b>Real Upper Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REAL_RANGE__REAL_UPPER_BOUND = eINSTANCE.getRealRange_RealUpperBound();

		/**
		 * The meta object literal for the '<em><b>Real Lower Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REAL_RANGE__REAL_LOWER_BOUND = eINSTANCE.getRealRange_RealLowerBound();

		/**
		 * The meta object literal for the '<em><b>Constant Lower Bound</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REAL_RANGE__CONSTANT_LOWER_BOUND = eINSTANCE.getRealRange_ConstantLowerBound();

		/**
		 * The meta object literal for the '<em><b>Constant Upper Bound</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REAL_RANGE__CONSTANT_UPPER_BOUND = eINSTANCE.getRealRange_ConstantUpperBound();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.RecordFieldTypeImpl <em>Record Field Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.RecordFieldTypeImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getRecordFieldType()
		 * @generated
		 */
		EClass RECORD_FIELD_TYPE = eINSTANCE.getRecordFieldType();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECORD_FIELD_TYPE__TYPE = eINSTANCE.getRecordFieldType_Type();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RECORD_FIELD_TYPE__NAME = eINSTANCE.getRecordFieldType_Name();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.RecordTypeImpl <em>Record Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.RecordTypeImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getRecordType()
		 * @generated
		 */
		EClass RECORD_TYPE = eINSTANCE.getRecordType();

		/**
		 * The meta object literal for the '<em><b>Record Field</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECORD_TYPE__RECORD_FIELD = eINSTANCE.getRecordType_RecordField();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.ReferenceTypeImpl <em>Reference Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.ReferenceTypeImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getReferenceType()
		 * @generated
		 */
		EClass REFERENCE_TYPE = eINSTANCE.getReferenceType();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.SlashImpl <em>Slash</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.SlashImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getSlash()
		 * @generated
		 */
		EClass SLASH = eINSTANCE.getSlash();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.TextImpl <em>Text</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.TextImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getText()
		 * @generated
		 */
		EClass TEXT = eINSTANCE.getText();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEXT__VALUE = eINSTANCE.getText_Value();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.UnitImpl <em>Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.UnitImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getUnit()
		 * @generated
		 */
		EClass UNIT = eINSTANCE.getUnit();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIT__UNIT = eINSTANCE.getUnit_Unit();

		/**
		 * The meta object literal for the '<em><b>Multiplier</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNIT__MULTIPLIER = eINSTANCE.getUnit_Multiplier();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNIT__NAME = eINSTANCE.getUnit_Name();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.UnitsTypeImpl <em>Units Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.UnitsTypeImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getUnitsType()
		 * @generated
		 */
		EClass UNITS_TYPE = eINSTANCE.getUnitsType();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNITS_TYPE__UNIT = eINSTANCE.getUnitsType_Unit();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.impl.TypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.TypeImpl
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getType()
		 * @generated
		 */
		EClass TYPE = eINSTANCE.getType();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.ConditionValue <em>Condition Value</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.ConditionValue
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getConditionValue()
		 * @generated
		 */
		EEnum CONDITION_VALUE = eINSTANCE.getConditionValue();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.LoopMode <em>Loop Mode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.LoopMode
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getLoopMode()
		 * @generated
		 */
		EEnum LOOP_MODE = eINSTANCE.getLoopMode();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.Mode <em>Mode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.Mode
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getMode()
		 * @generated
		 */
		EEnum MODE = eINSTANCE.getMode();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.NumericType <em>Numeric Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.NumericType
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getNumericType()
		 * @generated
		 */
		EEnum NUMERIC_TYPE = eINSTANCE.getNumericType();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.Procedure <em>Procedure</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.Procedure
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getProcedure()
		 * @generated
		 */
		EEnum PROCEDURE = eINSTANCE.getProcedure();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.SubjectType <em>Subject Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.SubjectType
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getSubjectType()
		 * @generated
		 */
		EEnum SUBJECT_TYPE = eINSTANCE.getSubjectType();

		/**
		 * The meta object literal for the '{@link org.topcased.adele.odsConfig.OdsConfig.SubjectType1 <em>Subject Type1</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.topcased.adele.odsConfig.OdsConfig.SubjectType1
		 * @see org.topcased.adele.odsConfig.OdsConfig.impl.OdsConfigPackageImpl#getSubjectType1()
		 * @generated
		 */
		EEnum SUBJECT_TYPE1 = eINSTANCE.getSubjectType1();

	}

} //OdsConfigPackage
