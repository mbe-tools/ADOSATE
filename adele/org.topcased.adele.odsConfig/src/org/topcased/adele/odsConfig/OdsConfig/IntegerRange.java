/**
 * <copyright>
 * </copyright>
 *
 * $Id: IntegerRange.java,v 1.1 2010-10-27 09:04:25 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Range</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.IntegerRange#getIntegerUpperBound <em>Integer Upper Bound</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.IntegerRange#getIntegerLowerBound <em>Integer Lower Bound</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.IntegerRange#getConstantUpperBound <em>Constant Upper Bound</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.IntegerRange#getConstantLowerBound <em>Constant Lower Bound</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getIntegerRange()
 * @model extendedMetaData="name='integerRange' kind='elementOnly'"
 * @generated
 */
public interface IntegerRange extends EObject {
	/**
	 * Returns the value of the '<em><b>Integer Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Upper Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Upper Bound</em>' attribute.
	 * @see #setIntegerUpperBound(int)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getIntegerRange_IntegerUpperBound()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='element' name='integerUpperBound' namespace='##targetNamespace'"
	 * @generated
	 */
	int getIntegerUpperBound();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.IntegerRange#getIntegerUpperBound <em>Integer Upper Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Upper Bound</em>' attribute.
	 * @see #getIntegerUpperBound()
	 * @generated
	 */
	void setIntegerUpperBound(int value);

	/**
	 * Returns the value of the '<em><b>Integer Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Lower Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Lower Bound</em>' attribute.
	 * @see #setIntegerLowerBound(int)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getIntegerRange_IntegerLowerBound()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='element' name='integerLowerBound' namespace='##targetNamespace'"
	 * @generated
	 */
	int getIntegerLowerBound();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.IntegerRange#getIntegerLowerBound <em>Integer Lower Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Lower Bound</em>' attribute.
	 * @see #getIntegerLowerBound()
	 * @generated
	 */
	void setIntegerLowerBound(int value);

	/**
	 * Returns the value of the '<em><b>Constant Upper Bound</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant Upper Bound</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant Upper Bound</em>' reference.
	 * @see #setConstantUpperBound(PropertyConstant)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getIntegerRange_ConstantUpperBound()
	 * @model
	 * @generated
	 */
	PropertyConstant getConstantUpperBound();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.IntegerRange#getConstantUpperBound <em>Constant Upper Bound</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant Upper Bound</em>' reference.
	 * @see #getConstantUpperBound()
	 * @generated
	 */
	void setConstantUpperBound(PropertyConstant value);

	/**
	 * Returns the value of the '<em><b>Constant Lower Bound</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant Lower Bound</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant Lower Bound</em>' reference.
	 * @see #setConstantLowerBound(PropertyConstant)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getIntegerRange_ConstantLowerBound()
	 * @model
	 * @generated
	 */
	PropertyConstant getConstantLowerBound();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.IntegerRange#getConstantLowerBound <em>Constant Lower Bound</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant Lower Bound</em>' reference.
	 * @see #getConstantLowerBound()
	 * @generated
	 */
	void setConstantLowerBound(PropertyConstant value);

} // IntegerRange
