/**
 * <copyright>
 * </copyright>
 *
 * $Id: Condition.java,v 1.1 2010-10-27 09:04:24 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.Condition#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getCondition()
 * @model extendedMetaData="name='Condition' kind='empty'"
 * @generated
 */
public interface Condition extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * The literals are from the enumeration {@link org.topcased.adele.odsConfig.OdsConfig.ConditionValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ConditionValue
	 * @see #isSetValue()
	 * @see #unsetValue()
	 * @see #setValue(ConditionValue)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getCondition_Value()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='value' namespace='##targetNamespace'"
	 * @generated
	 */
	ConditionValue getValue();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.Condition#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.ConditionValue
	 * @see #isSetValue()
	 * @see #unsetValue()
	 * @see #getValue()
	 * @generated
	 */
	void setValue(ConditionValue value);

	/**
	 * Unsets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.Condition#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetValue()
	 * @see #getValue()
	 * @see #setValue(ConditionValue)
	 * @generated
	 */
	void unsetValue();

	/**
	 * Returns whether the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.Condition#getValue <em>Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Value</em>' attribute is set.
	 * @see #unsetValue()
	 * @see #getValue()
	 * @see #setValue(ConditionValue)
	 * @generated
	 */
	boolean isSetValue();

} // Condition
