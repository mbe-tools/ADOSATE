/**
 * <copyright>
 * </copyright>
 *
 * $Id: ODSectionImpl.java,v 1.2 2011-04-19 14:09:34 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.topcased.adele.odsConfig.OdsConfig.DataRecovering;
import org.topcased.adele.odsConfig.OdsConfig.Label;
import org.topcased.adele.odsConfig.OdsConfig.ODSection;
import org.topcased.adele.odsConfig.OdsConfig.ObjectCondition;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;
import org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OD Section</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSectionImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSectionImpl#getObjectCondition <em>Object Condition</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSectionImpl#getDataRecovering <em>Data Recovering</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSectionImpl#getODSection <em>OD Section</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSectionImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.ODSectionImpl#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ODSectionImpl extends EObjectImpl implements ODSection {
	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected Label label;

	/**
	 * The cached value of the '{@link #getObjectCondition() <em>Object Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectCondition()
	 * @generated
	 * @ordered
	 */
	protected ObjectCondition objectCondition;

	/**
	 * The cached value of the '{@link #getDataRecovering() <em>Data Recovering</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataRecovering()
	 * @generated
	 * @ordered
	 */
	protected DataRecovering dataRecovering;

	/**
	 * The cached value of the '{@link #getODSection() <em>OD Section</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getODSection()
	 * @generated
	 * @ordered
	 */
	protected EList<ODSection> oDSection;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected PropertyDefinition type;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ODSectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdsConfigPackage.Literals.OD_SECTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Label getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLabel(Label newLabel, NotificationChain msgs) {
		Label oldLabel = label;
		label = newLabel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.OD_SECTION__LABEL, oldLabel, newLabel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(Label newLabel) {
		if (newLabel != label) {
			NotificationChain msgs = null;
			if (label != null)
				msgs = ((InternalEObject)label).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.OD_SECTION__LABEL, null, msgs);
			if (newLabel != null)
				msgs = ((InternalEObject)newLabel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.OD_SECTION__LABEL, null, msgs);
			msgs = basicSetLabel(newLabel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.OD_SECTION__LABEL, newLabel, newLabel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectCondition getObjectCondition() {
		return objectCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectCondition(ObjectCondition newObjectCondition, NotificationChain msgs) {
		ObjectCondition oldObjectCondition = objectCondition;
		objectCondition = newObjectCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.OD_SECTION__OBJECT_CONDITION, oldObjectCondition, newObjectCondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectCondition(ObjectCondition newObjectCondition) {
		if (newObjectCondition != objectCondition) {
			NotificationChain msgs = null;
			if (objectCondition != null)
				msgs = ((InternalEObject)objectCondition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.OD_SECTION__OBJECT_CONDITION, null, msgs);
			if (newObjectCondition != null)
				msgs = ((InternalEObject)newObjectCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.OD_SECTION__OBJECT_CONDITION, null, msgs);
			msgs = basicSetObjectCondition(newObjectCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.OD_SECTION__OBJECT_CONDITION, newObjectCondition, newObjectCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataRecovering getDataRecovering() {
		return dataRecovering;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataRecovering(DataRecovering newDataRecovering, NotificationChain msgs) {
		DataRecovering oldDataRecovering = dataRecovering;
		dataRecovering = newDataRecovering;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.OD_SECTION__DATA_RECOVERING, oldDataRecovering, newDataRecovering);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataRecovering(DataRecovering newDataRecovering) {
		if (newDataRecovering != dataRecovering) {
			NotificationChain msgs = null;
			if (dataRecovering != null)
				msgs = ((InternalEObject)dataRecovering).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.OD_SECTION__DATA_RECOVERING, null, msgs);
			if (newDataRecovering != null)
				msgs = ((InternalEObject)newDataRecovering).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.OD_SECTION__DATA_RECOVERING, null, msgs);
			msgs = basicSetDataRecovering(newDataRecovering, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.OD_SECTION__DATA_RECOVERING, newDataRecovering, newDataRecovering));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ODSection> getODSection() {
		if (oDSection == null) {
			oDSection = new EObjectContainmentEList<ODSection>(ODSection.class, this, OdsConfigPackage.OD_SECTION__OD_SECTION);
		}
		return oDSection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.OD_SECTION__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyDefinition getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (PropertyDefinition)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdsConfigPackage.OD_SECTION__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyDefinition basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(PropertyDefinition newType) {
		PropertyDefinition oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.OD_SECTION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdsConfigPackage.OD_SECTION__LABEL:
				return basicSetLabel(null, msgs);
			case OdsConfigPackage.OD_SECTION__OBJECT_CONDITION:
				return basicSetObjectCondition(null, msgs);
			case OdsConfigPackage.OD_SECTION__DATA_RECOVERING:
				return basicSetDataRecovering(null, msgs);
			case OdsConfigPackage.OD_SECTION__OD_SECTION:
				return ((InternalEList<?>)getODSection()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdsConfigPackage.OD_SECTION__LABEL:
				return getLabel();
			case OdsConfigPackage.OD_SECTION__OBJECT_CONDITION:
				return getObjectCondition();
			case OdsConfigPackage.OD_SECTION__DATA_RECOVERING:
				return getDataRecovering();
			case OdsConfigPackage.OD_SECTION__OD_SECTION:
				return getODSection();
			case OdsConfigPackage.OD_SECTION__ID:
				return getId();
			case OdsConfigPackage.OD_SECTION__TYPE:
				if (resolve) return getType();
				return basicGetType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdsConfigPackage.OD_SECTION__LABEL:
				setLabel((Label)newValue);
				return;
			case OdsConfigPackage.OD_SECTION__OBJECT_CONDITION:
				setObjectCondition((ObjectCondition)newValue);
				return;
			case OdsConfigPackage.OD_SECTION__DATA_RECOVERING:
				setDataRecovering((DataRecovering)newValue);
				return;
			case OdsConfigPackage.OD_SECTION__OD_SECTION:
				getODSection().clear();
				getODSection().addAll((Collection<? extends ODSection>)newValue);
				return;
			case OdsConfigPackage.OD_SECTION__ID:
				setId((String)newValue);
				return;
			case OdsConfigPackage.OD_SECTION__TYPE:
				setType((PropertyDefinition)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.OD_SECTION__LABEL:
				setLabel((Label)null);
				return;
			case OdsConfigPackage.OD_SECTION__OBJECT_CONDITION:
				setObjectCondition((ObjectCondition)null);
				return;
			case OdsConfigPackage.OD_SECTION__DATA_RECOVERING:
				setDataRecovering((DataRecovering)null);
				return;
			case OdsConfigPackage.OD_SECTION__OD_SECTION:
				getODSection().clear();
				return;
			case OdsConfigPackage.OD_SECTION__ID:
				setId(ID_EDEFAULT);
				return;
			case OdsConfigPackage.OD_SECTION__TYPE:
				setType((PropertyDefinition)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.OD_SECTION__LABEL:
				return label != null;
			case OdsConfigPackage.OD_SECTION__OBJECT_CONDITION:
				return objectCondition != null;
			case OdsConfigPackage.OD_SECTION__DATA_RECOVERING:
				return dataRecovering != null;
			case OdsConfigPackage.OD_SECTION__OD_SECTION:
				return oDSection != null && !oDSection.isEmpty();
			case OdsConfigPackage.OD_SECTION__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case OdsConfigPackage.OD_SECTION__TYPE:
				return type != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}

} //ODSectionImpl
