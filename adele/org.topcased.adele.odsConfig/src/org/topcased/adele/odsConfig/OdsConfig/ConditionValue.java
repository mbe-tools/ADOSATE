/**
 * <copyright>
 * </copyright>
 *
 * $Id: ConditionValue.java,v 1.2 2011-04-19 14:13:52 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Condition Value</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getConditionValue()
 * @model extendedMetaData="name='ConditionValue'"
 * @generated
 */
public enum ConditionValue implements Enumerator {
	/**
	 * The '<em><b>Is Abstract</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_ABSTRACT_VALUE
	 * @generated
	 * @ordered
	 */
	IS_ABSTRACT(0, "isAbstract", "isAbstract"),

	/**
	 * The '<em><b>Is ADELE Component</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_ADELE_COMPONENT_VALUE
	 * @generated
	 * @ordered
	 */
	IS_ADELE_COMPONENT(1, "isADELEComponent", "isADELE_Component"),

	/**
	 * The '<em><b>Is Bus</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_BUS_VALUE
	 * @generated
	 * @ordered
	 */
	IS_BUS(2, "isBus", "isBus"),

	/**
	 * The '<em><b>Is Classifier</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_CLASSIFIER_VALUE
	 * @generated
	 * @ordered
	 */
	IS_CLASSIFIER(3, "isClassifier", "isClassifier"),

	/**
	 * The '<em><b>Is Implementation</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_IMPLEMENTATION_VALUE
	 * @generated
	 * @ordered
	 */
	IS_IMPLEMENTATION(4, "isImplementation", "isImplementation"), /**
	 * The '<em><b>Is Data</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_DATA_VALUE
	 * @generated
	 * @ordered
	 */
	IS_DATA(5, "isData", "isData"),

	/**
	 * The '<em><b>Is Device</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_DEVICE_VALUE
	 * @generated
	 * @ordered
	 */
	IS_DEVICE(6, "isDevice", "isDevice"),

	/**
	 * The '<em><b>Is Memory</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_MEMORY_VALUE
	 * @generated
	 * @ordered
	 */
	IS_MEMORY(7, "isMemory", "isMemory"),

	/**
	 * The '<em><b>Is Package</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_PACKAGE_VALUE
	 * @generated
	 * @ordered
	 */
	IS_PACKAGE(8, "isPackage", "isPackage"),

	/**
	 * The '<em><b>Is Process</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_PROCESS_VALUE
	 * @generated
	 * @ordered
	 */
	IS_PROCESS(9, "isProcess", "isProcess"),

	/**
	 * The '<em><b>Is Processor</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_PROCESSOR_VALUE
	 * @generated
	 * @ordered
	 */
	IS_PROCESSOR(10, "isProcessor", "isProcessor"),

	/**
	 * The '<em><b>Is Prototype</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_PROTOTYPE_VALUE
	 * @generated
	 * @ordered
	 */
	IS_PROTOTYPE(11, "isPrototype", "isPrototype"),

	/**
	 * The '<em><b>Is Instance</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_INSTANCE_VALUE
	 * @generated
	 * @ordered
	 */
	IS_INSTANCE(12, "isInstance", "isInstance"), /**
	 * The '<em><b>Is Subprogram</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_SUBPROGRAM_VALUE
	 * @generated
	 * @ordered
	 */
	IS_SUBPROGRAM(13, "isSubprogram", "isSubprogram"),

	/**
	 * The '<em><b>Is System</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_SYSTEM_VALUE
	 * @generated
	 * @ordered
	 */
	IS_SYSTEM(14, "isSystem", "isSystem"),

	/**
	 * The '<em><b>Is Thread</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_THREAD_VALUE
	 * @generated
	 * @ordered
	 */
	IS_THREAD(15, "isThread", "isThread"),

	/**
	 * The '<em><b>Is Thread Group</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_THREAD_GROUP_VALUE
	 * @generated
	 * @ordered
	 */
	IS_THREAD_GROUP(16, "isThreadGroup", "isThreadGroup"),

	/**
	 * The '<em><b>Is Virtual Bus</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_VIRTUAL_BUS_VALUE
	 * @generated
	 * @ordered
	 */
	IS_VIRTUAL_BUS(17, "isVirtualBus", "isVirtualBus"),

	/**
	 * The '<em><b>Is Virtual Processor</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_VIRTUAL_PROCESSOR_VALUE
	 * @generated
	 * @ordered
	 */
	IS_VIRTUAL_PROCESSOR(18, "isVirtualProcessor", "isVirtualProcessor"),

	/**
	 * The '<em><b>Is Abstract Feature</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_ABSTRACT_FEATURE_VALUE
	 * @generated
	 * @ordered
	 */
	IS_ABSTRACT_FEATURE(19, "isAbstractFeature", "isAbstractFeature"),

	/**
	 * The '<em><b>Is Bus Access</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_BUS_ACCESS_VALUE
	 * @generated
	 * @ordered
	 */
	IS_BUS_ACCESS(20, "isBusAccess", "isBusAccess"),

	/**
	 * The '<em><b>Is Data Access</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_DATA_ACCESS_VALUE
	 * @generated
	 * @ordered
	 */
	IS_DATA_ACCESS(21, "isDataAccess", "isDataAccess"),

	/**
	 * The '<em><b>Is Data Port</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_DATA_PORT_VALUE
	 * @generated
	 * @ordered
	 */
	IS_DATA_PORT(22, "isDataPort", "isDataPort"),

	/**
	 * The '<em><b>Is Event Data Port</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_EVENT_DATA_PORT_VALUE
	 * @generated
	 * @ordered
	 */
	IS_EVENT_DATA_PORT(23, "isEventDataPort", "isEventDataPort"),

	/**
	 * The '<em><b>Is Feature</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_FEATURE_VALUE
	 * @generated
	 * @ordered
	 */
	IS_FEATURE(24, "isFeature", "isFeature"),

	/**
	 * The '<em><b>Is Feature Group</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_FEATURE_GROUP_VALUE
	 * @generated
	 * @ordered
	 */
	IS_FEATURE_GROUP(25, "isFeatureGroup", "isFeatureGroup"),

	/**
	 * The '<em><b>Is Parameter</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_PARAMETER_VALUE
	 * @generated
	 * @ordered
	 */
	IS_PARAMETER(26, "isParameter", "isParameter"),

	/**
	 * The '<em><b>Is Port</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_PORT_VALUE
	 * @generated
	 * @ordered
	 */
	IS_PORT(27, "isPort", "isPort"),

	/**
	 * The '<em><b>Is Provides Subprogram Access</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_PROVIDES_SUBPROGRAM_ACCESS_VALUE
	 * @generated
	 * @ordered
	 */
	IS_PROVIDES_SUBPROGRAM_ACCESS(28, "isProvidesSubprogramAccess", "isProvidesSubprogramAccess"),

	/**
	 * The '<em><b>Is Subprogram Access</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_SUBPROGRAM_ACCESS_VALUE
	 * @generated
	 * @ordered
	 */
	IS_SUBPROGRAM_ACCESS(29, "isSubprogramAccess", "isSubprogramAccess"),

	/**
	 * The '<em><b>Is Connection</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_CONNECTION_VALUE
	 * @generated
	 * @ordered
	 */
	IS_CONNECTION(30, "isConnection", "isConnection"),

	/**
	 * The '<em><b>Is Event Port</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_EVENT_PORT_VALUE
	 * @generated
	 * @ordered
	 */
	IS_EVENT_PORT(31, "isEventPort", "isEventPort"),

	/**
	 * The '<em><b>Is Flow</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_FLOW_VALUE
	 * @generated
	 * @ordered
	 */
	IS_FLOW(32, "isFlow", "isFlow"),

	/**
	 * The '<em><b>Is Mode Transition</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_MODE_TRANSITION_VALUE
	 * @generated
	 * @ordered
	 */
	IS_MODE_TRANSITION(33, "isModeTransition", "isModeTransition"),

	/**
	 * The '<em><b>Is Port Connection</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_PORT_CONNECTION_VALUE
	 * @generated
	 * @ordered
	 */
	IS_PORT_CONNECTION(34, "isPortConnection", "isPortConnection"),

	/**
	 * The '<em><b>EEnum Literal0</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EENUM_LITERAL0_VALUE
	 * @generated
	 * @ordered
	 */
	EENUM_LITERAL0(35, "EEnumLiteral0", "EEnumLiteral0"), /**
	 * The '<em><b>Is BA Variable</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_BA_VARIABLE_VALUE
	 * @generated
	 * @ordered
	 */
	IS_BA_VARIABLE(36, "isBAVariable", "isBAVariable"), /**
	 * The '<em><b>Is BA State</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_BA_STATE_VALUE
	 * @generated
	 * @ordered
	 */
	IS_BA_STATE(37, "isBAState", "isBAState"), /**
	 * The '<em><b>Is BA Transition</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_BA_TRANSITION_VALUE
	 * @generated
	 * @ordered
	 */
	IS_BA_TRANSITION(38, "isBATransition", "isBATransition"), /**
	 * The '<em><b>Is Type</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_TYPE_VALUE
	 * @generated
	 * @ordered
	 */
	IS_TYPE(39, "isType", "isType"), /**
	 * The '<em><b>Is Subcomponent Refinement</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_SUBCOMPONENT_REFINEMENT_VALUE
	 * @generated
	 * @ordered
	 */
	IS_SUBCOMPONENT_REFINEMENT(40, "isSubcomponentRefinement", "isSubcomponentRefinement"), /**
	 * The '<em><b>Is Refinement</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_REFINEMENT_VALUE
	 * @generated
	 * @ordered
	 */
	IS_REFINEMENT(41, "isRefinement", "isRefinement");

	/**
	 * The '<em><b>Is Abstract</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Abstract</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_ABSTRACT
	 * @model name="isAbstract"
	 * @generated
	 * @ordered
	 */
	public static final int IS_ABSTRACT_VALUE = 0;

	/**
	 * The '<em><b>Is ADELE Component</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is ADELE Component</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_ADELE_COMPONENT
	 * @model name="isADELEComponent" literal="isADELE_Component"
	 * @generated
	 * @ordered
	 */
	public static final int IS_ADELE_COMPONENT_VALUE = 1;

	/**
	 * The '<em><b>Is Bus</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Bus</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_BUS
	 * @model name="isBus"
	 * @generated
	 * @ordered
	 */
	public static final int IS_BUS_VALUE = 2;

	/**
	 * The '<em><b>Is Classifier</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Classifier</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_CLASSIFIER
	 * @model name="isClassifier"
	 * @generated
	 * @ordered
	 */
	public static final int IS_CLASSIFIER_VALUE = 3;

	/**
	 * The '<em><b>Is Implementation</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Implementation</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_IMPLEMENTATION
	 * @model name="isImplementation"
	 * @generated
	 * @ordered
	 */
	public static final int IS_IMPLEMENTATION_VALUE = 4;

	/**
	 * The '<em><b>Is Data</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Data</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_DATA
	 * @model name="isData"
	 * @generated
	 * @ordered
	 */
	public static final int IS_DATA_VALUE = 5;

	/**
	 * The '<em><b>Is Device</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Device</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_DEVICE
	 * @model name="isDevice"
	 * @generated
	 * @ordered
	 */
	public static final int IS_DEVICE_VALUE = 6;

	/**
	 * The '<em><b>Is Memory</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Memory</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_MEMORY
	 * @model name="isMemory"
	 * @generated
	 * @ordered
	 */
	public static final int IS_MEMORY_VALUE = 7;

	/**
	 * The '<em><b>Is Package</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Package</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_PACKAGE
	 * @model name="isPackage"
	 * @generated
	 * @ordered
	 */
	public static final int IS_PACKAGE_VALUE = 8;

	/**
	 * The '<em><b>Is Process</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Process</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_PROCESS
	 * @model name="isProcess"
	 * @generated
	 * @ordered
	 */
	public static final int IS_PROCESS_VALUE = 9;

	/**
	 * The '<em><b>Is Processor</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Processor</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_PROCESSOR
	 * @model name="isProcessor"
	 * @generated
	 * @ordered
	 */
	public static final int IS_PROCESSOR_VALUE = 10;

	/**
	 * The '<em><b>Is Prototype</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Prototype</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_PROTOTYPE
	 * @model name="isPrototype"
	 * @generated
	 * @ordered
	 */
	public static final int IS_PROTOTYPE_VALUE = 11;

	/**
	 * The '<em><b>Is Instance</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Instance</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_INSTANCE
	 * @model name="isInstance"
	 * @generated
	 * @ordered
	 */
	public static final int IS_INSTANCE_VALUE = 12;

	/**
	 * The '<em><b>Is Subprogram</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Subprogram</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_SUBPROGRAM
	 * @model name="isSubprogram"
	 * @generated
	 * @ordered
	 */
	public static final int IS_SUBPROGRAM_VALUE = 13;

	/**
	 * The '<em><b>Is System</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is System</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_SYSTEM
	 * @model name="isSystem"
	 * @generated
	 * @ordered
	 */
	public static final int IS_SYSTEM_VALUE = 14;

	/**
	 * The '<em><b>Is Thread</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Thread</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_THREAD
	 * @model name="isThread"
	 * @generated
	 * @ordered
	 */
	public static final int IS_THREAD_VALUE = 15;

	/**
	 * The '<em><b>Is Thread Group</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Thread Group</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_THREAD_GROUP
	 * @model name="isThreadGroup"
	 * @generated
	 * @ordered
	 */
	public static final int IS_THREAD_GROUP_VALUE = 16;

	/**
	 * The '<em><b>Is Virtual Bus</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Virtual Bus</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_VIRTUAL_BUS
	 * @model name="isVirtualBus"
	 * @generated
	 * @ordered
	 */
	public static final int IS_VIRTUAL_BUS_VALUE = 17;

	/**
	 * The '<em><b>Is Virtual Processor</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Virtual Processor</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_VIRTUAL_PROCESSOR
	 * @model name="isVirtualProcessor"
	 * @generated
	 * @ordered
	 */
	public static final int IS_VIRTUAL_PROCESSOR_VALUE = 18;

	/**
	 * The '<em><b>Is Abstract Feature</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Abstract Feature</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_ABSTRACT_FEATURE
	 * @model name="isAbstractFeature"
	 * @generated
	 * @ordered
	 */
	public static final int IS_ABSTRACT_FEATURE_VALUE = 19;

	/**
	 * The '<em><b>Is Bus Access</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Bus Access</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_BUS_ACCESS
	 * @model name="isBusAccess"
	 * @generated
	 * @ordered
	 */
	public static final int IS_BUS_ACCESS_VALUE = 20;

	/**
	 * The '<em><b>Is Data Access</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Data Access</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_DATA_ACCESS
	 * @model name="isDataAccess"
	 * @generated
	 * @ordered
	 */
	public static final int IS_DATA_ACCESS_VALUE = 21;

	/**
	 * The '<em><b>Is Data Port</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Data Port</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_DATA_PORT
	 * @model name="isDataPort"
	 * @generated
	 * @ordered
	 */
	public static final int IS_DATA_PORT_VALUE = 22;

	/**
	 * The '<em><b>Is Event Data Port</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Event Data Port</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_EVENT_DATA_PORT
	 * @model name="isEventDataPort"
	 * @generated
	 * @ordered
	 */
	public static final int IS_EVENT_DATA_PORT_VALUE = 23;

	/**
	 * The '<em><b>Is Feature</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Feature</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_FEATURE
	 * @model name="isFeature"
	 * @generated
	 * @ordered
	 */
	public static final int IS_FEATURE_VALUE = 24;

	/**
	 * The '<em><b>Is Feature Group</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Feature Group</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_FEATURE_GROUP
	 * @model name="isFeatureGroup"
	 * @generated
	 * @ordered
	 */
	public static final int IS_FEATURE_GROUP_VALUE = 25;

	/**
	 * The '<em><b>Is Parameter</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Parameter</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_PARAMETER
	 * @model name="isParameter"
	 * @generated
	 * @ordered
	 */
	public static final int IS_PARAMETER_VALUE = 26;

	/**
	 * The '<em><b>Is Port</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Port</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_PORT
	 * @model name="isPort"
	 * @generated
	 * @ordered
	 */
	public static final int IS_PORT_VALUE = 27;

	/**
	 * The '<em><b>Is Provides Subprogram Access</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Provides Subprogram Access</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_PROVIDES_SUBPROGRAM_ACCESS
	 * @model name="isProvidesSubprogramAccess"
	 * @generated
	 * @ordered
	 */
	public static final int IS_PROVIDES_SUBPROGRAM_ACCESS_VALUE = 28;

	/**
	 * The '<em><b>Is Subprogram Access</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Subprogram Access</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_SUBPROGRAM_ACCESS
	 * @model name="isSubprogramAccess"
	 * @generated
	 * @ordered
	 */
	public static final int IS_SUBPROGRAM_ACCESS_VALUE = 29;

	/**
	 * The '<em><b>Is Connection</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Connection</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_CONNECTION
	 * @model name="isConnection"
	 * @generated
	 * @ordered
	 */
	public static final int IS_CONNECTION_VALUE = 30;

	/**
	 * The '<em><b>Is Event Port</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Event Port</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_EVENT_PORT
	 * @model name="isEventPort"
	 * @generated
	 * @ordered
	 */
	public static final int IS_EVENT_PORT_VALUE = 31;

	/**
	 * The '<em><b>Is Flow</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Flow</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_FLOW
	 * @model name="isFlow"
	 * @generated
	 * @ordered
	 */
	public static final int IS_FLOW_VALUE = 32;

	/**
	 * The '<em><b>Is Mode Transition</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Mode Transition</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_MODE_TRANSITION
	 * @model name="isModeTransition"
	 * @generated
	 * @ordered
	 */
	public static final int IS_MODE_TRANSITION_VALUE = 33;

	/**
	 * The '<em><b>Is Port Connection</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Port Connection</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_PORT_CONNECTION
	 * @model name="isPortConnection"
	 * @generated
	 * @ordered
	 */
	public static final int IS_PORT_CONNECTION_VALUE = 34;

	/**
	 * The '<em><b>EEnum Literal0</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EEnum Literal0</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EENUM_LITERAL0
	 * @model name="EEnumLiteral0"
	 * @generated
	 * @ordered
	 */
	public static final int EENUM_LITERAL0_VALUE = 35;

	/**
	 * The '<em><b>Is BA Variable</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is BA Variable</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_BA_VARIABLE
	 * @model name="isBAVariable"
	 * @generated
	 * @ordered
	 */
	public static final int IS_BA_VARIABLE_VALUE = 36;

	/**
	 * The '<em><b>Is BA State</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is BA State</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_BA_STATE
	 * @model name="isBAState"
	 * @generated
	 * @ordered
	 */
	public static final int IS_BA_STATE_VALUE = 37;

	/**
	 * The '<em><b>Is BA Transition</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is BA Transition</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_BA_TRANSITION
	 * @model name="isBATransition"
	 * @generated
	 * @ordered
	 */
	public static final int IS_BA_TRANSITION_VALUE = 38;

	/**
	 * The '<em><b>Is Type</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Type</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_TYPE
	 * @model name="isType"
	 * @generated
	 * @ordered
	 */
	public static final int IS_TYPE_VALUE = 39;

	/**
	 * The '<em><b>Is Subcomponent Refinement</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Subcomponent Refinement</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_SUBCOMPONENT_REFINEMENT
	 * @model name="isSubcomponentRefinement"
	 * @generated
	 * @ordered
	 */
	public static final int IS_SUBCOMPONENT_REFINEMENT_VALUE = 40;

	/**
	 * The '<em><b>Is Refinement</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Is Refinement</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_REFINEMENT
	 * @model name="isRefinement"
	 * @generated
	 * @ordered
	 */
	public static final int IS_REFINEMENT_VALUE = 41;

	/**
	 * An array of all the '<em><b>Condition Value</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ConditionValue[] VALUES_ARRAY =
		new ConditionValue[] {
			IS_ABSTRACT,
			IS_ADELE_COMPONENT,
			IS_BUS,
			IS_CLASSIFIER,
			IS_IMPLEMENTATION,
			IS_DATA,
			IS_DEVICE,
			IS_MEMORY,
			IS_PACKAGE,
			IS_PROCESS,
			IS_PROCESSOR,
			IS_PROTOTYPE,
			IS_INSTANCE,
			IS_SUBPROGRAM,
			IS_SYSTEM,
			IS_THREAD,
			IS_THREAD_GROUP,
			IS_VIRTUAL_BUS,
			IS_VIRTUAL_PROCESSOR,
			IS_ABSTRACT_FEATURE,
			IS_BUS_ACCESS,
			IS_DATA_ACCESS,
			IS_DATA_PORT,
			IS_EVENT_DATA_PORT,
			IS_FEATURE,
			IS_FEATURE_GROUP,
			IS_PARAMETER,
			IS_PORT,
			IS_PROVIDES_SUBPROGRAM_ACCESS,
			IS_SUBPROGRAM_ACCESS,
			IS_CONNECTION,
			IS_EVENT_PORT,
			IS_FLOW,
			IS_MODE_TRANSITION,
			IS_PORT_CONNECTION,
			EENUM_LITERAL0,
			IS_BA_VARIABLE,
			IS_BA_STATE,
			IS_BA_TRANSITION,
			IS_TYPE,
			IS_SUBCOMPONENT_REFINEMENT,
			IS_REFINEMENT,
		};

	/**
	 * A public read-only list of all the '<em><b>Condition Value</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ConditionValue> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Condition Value</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ConditionValue get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ConditionValue result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Condition Value</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ConditionValue getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ConditionValue result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Condition Value</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ConditionValue get(int value) {
		switch (value) {
			case IS_ABSTRACT_VALUE: return IS_ABSTRACT;
			case IS_ADELE_COMPONENT_VALUE: return IS_ADELE_COMPONENT;
			case IS_BUS_VALUE: return IS_BUS;
			case IS_CLASSIFIER_VALUE: return IS_CLASSIFIER;
			case IS_IMPLEMENTATION_VALUE: return IS_IMPLEMENTATION;
			case IS_DATA_VALUE: return IS_DATA;
			case IS_DEVICE_VALUE: return IS_DEVICE;
			case IS_MEMORY_VALUE: return IS_MEMORY;
			case IS_PACKAGE_VALUE: return IS_PACKAGE;
			case IS_PROCESS_VALUE: return IS_PROCESS;
			case IS_PROCESSOR_VALUE: return IS_PROCESSOR;
			case IS_PROTOTYPE_VALUE: return IS_PROTOTYPE;
			case IS_INSTANCE_VALUE: return IS_INSTANCE;
			case IS_SUBPROGRAM_VALUE: return IS_SUBPROGRAM;
			case IS_SYSTEM_VALUE: return IS_SYSTEM;
			case IS_THREAD_VALUE: return IS_THREAD;
			case IS_THREAD_GROUP_VALUE: return IS_THREAD_GROUP;
			case IS_VIRTUAL_BUS_VALUE: return IS_VIRTUAL_BUS;
			case IS_VIRTUAL_PROCESSOR_VALUE: return IS_VIRTUAL_PROCESSOR;
			case IS_ABSTRACT_FEATURE_VALUE: return IS_ABSTRACT_FEATURE;
			case IS_BUS_ACCESS_VALUE: return IS_BUS_ACCESS;
			case IS_DATA_ACCESS_VALUE: return IS_DATA_ACCESS;
			case IS_DATA_PORT_VALUE: return IS_DATA_PORT;
			case IS_EVENT_DATA_PORT_VALUE: return IS_EVENT_DATA_PORT;
			case IS_FEATURE_VALUE: return IS_FEATURE;
			case IS_FEATURE_GROUP_VALUE: return IS_FEATURE_GROUP;
			case IS_PARAMETER_VALUE: return IS_PARAMETER;
			case IS_PORT_VALUE: return IS_PORT;
			case IS_PROVIDES_SUBPROGRAM_ACCESS_VALUE: return IS_PROVIDES_SUBPROGRAM_ACCESS;
			case IS_SUBPROGRAM_ACCESS_VALUE: return IS_SUBPROGRAM_ACCESS;
			case IS_CONNECTION_VALUE: return IS_CONNECTION;
			case IS_EVENT_PORT_VALUE: return IS_EVENT_PORT;
			case IS_FLOW_VALUE: return IS_FLOW;
			case IS_MODE_TRANSITION_VALUE: return IS_MODE_TRANSITION;
			case IS_PORT_CONNECTION_VALUE: return IS_PORT_CONNECTION;
			case EENUM_LITERAL0_VALUE: return EENUM_LITERAL0;
			case IS_BA_VARIABLE_VALUE: return IS_BA_VARIABLE;
			case IS_BA_STATE_VALUE: return IS_BA_STATE;
			case IS_BA_TRANSITION_VALUE: return IS_BA_TRANSITION;
			case IS_TYPE_VALUE: return IS_TYPE;
			case IS_SUBCOMPONENT_REFINEMENT_VALUE: return IS_SUBCOMPONENT_REFINEMENT;
			case IS_REFINEMENT_VALUE: return IS_REFINEMENT;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ConditionValue(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ConditionValue
