/**
 * <copyright>
 * </copyright>
 *
 * $Id: AadlString.java,v 1.1 2010-10-27 09:04:21 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aadl String</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getAadlString()
 * @model extendedMetaData="name='AadlString' kind='empty'"
 * @generated
 */
public interface AadlString extends PrimitivTypesType {
} // AadlString
