/**
 * <copyright>
 * </copyright>
 *
 * $Id: RangeTypeImpl.java,v 1.2 2011-04-19 14:09:35 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.topcased.adele.odsConfig.OdsConfig.NumberType;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;
import org.topcased.adele.odsConfig.OdsConfig.RangeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Range Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.RangeTypeImpl#getNumberType <em>Number Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RangeTypeImpl extends PrimitivTypesTypeImpl implements RangeType {
	/**
	 * The cached value of the '{@link #getNumberType() <em>Number Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberType()
	 * @generated
	 * @ordered
	 */
	protected NumberType numberType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RangeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdsConfigPackage.Literals.RANGE_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NumberType getNumberType() {
		return numberType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNumberType(NumberType newNumberType, NotificationChain msgs) {
		NumberType oldNumberType = numberType;
		numberType = newNumberType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.RANGE_TYPE__NUMBER_TYPE, oldNumberType, newNumberType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumberType(NumberType newNumberType) {
		if (newNumberType != numberType) {
			NotificationChain msgs = null;
			if (numberType != null)
				msgs = ((InternalEObject)numberType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.RANGE_TYPE__NUMBER_TYPE, null, msgs);
			if (newNumberType != null)
				msgs = ((InternalEObject)newNumberType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.RANGE_TYPE__NUMBER_TYPE, null, msgs);
			msgs = basicSetNumberType(newNumberType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.RANGE_TYPE__NUMBER_TYPE, newNumberType, newNumberType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdsConfigPackage.RANGE_TYPE__NUMBER_TYPE:
				return basicSetNumberType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdsConfigPackage.RANGE_TYPE__NUMBER_TYPE:
				return getNumberType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdsConfigPackage.RANGE_TYPE__NUMBER_TYPE:
				setNumberType((NumberType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.RANGE_TYPE__NUMBER_TYPE:
				setNumberType((NumberType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.RANGE_TYPE__NUMBER_TYPE:
				return numberType != null;
		}
		return super.eIsSet(featureID);
	}

} //RangeTypeImpl
