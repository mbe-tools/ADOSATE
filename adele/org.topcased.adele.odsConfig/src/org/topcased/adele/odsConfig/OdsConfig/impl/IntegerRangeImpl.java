/**
 * <copyright>
 * </copyright>
 *
 * $Id: IntegerRangeImpl.java,v 1.2 2011-04-19 14:09:34 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.topcased.adele.odsConfig.OdsConfig.IntegerRange;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;
import org.topcased.adele.odsConfig.OdsConfig.PropertyConstant;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Integer Range</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.IntegerRangeImpl#getIntegerUpperBound <em>Integer Upper Bound</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.IntegerRangeImpl#getIntegerLowerBound <em>Integer Lower Bound</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.IntegerRangeImpl#getConstantUpperBound <em>Constant Upper Bound</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.IntegerRangeImpl#getConstantLowerBound <em>Constant Lower Bound</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IntegerRangeImpl extends EObjectImpl implements IntegerRange {
	/**
	 * The default value of the '{@link #getIntegerUpperBound() <em>Integer Upper Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerUpperBound()
	 * @generated
	 * @ordered
	 */
	protected static final int INTEGER_UPPER_BOUND_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getIntegerUpperBound() <em>Integer Upper Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerUpperBound()
	 * @generated
	 * @ordered
	 */
	protected int integerUpperBound = INTEGER_UPPER_BOUND_EDEFAULT;

	/**
	 * The default value of the '{@link #getIntegerLowerBound() <em>Integer Lower Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerLowerBound()
	 * @generated
	 * @ordered
	 */
	protected static final int INTEGER_LOWER_BOUND_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getIntegerLowerBound() <em>Integer Lower Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerLowerBound()
	 * @generated
	 * @ordered
	 */
	protected int integerLowerBound = INTEGER_LOWER_BOUND_EDEFAULT;

	/**
	 * The cached value of the '{@link #getConstantUpperBound() <em>Constant Upper Bound</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstantUpperBound()
	 * @generated
	 * @ordered
	 */
	protected PropertyConstant constantUpperBound;

	/**
	 * The cached value of the '{@link #getConstantLowerBound() <em>Constant Lower Bound</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstantLowerBound()
	 * @generated
	 * @ordered
	 */
	protected PropertyConstant constantLowerBound;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntegerRangeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdsConfigPackage.Literals.INTEGER_RANGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIntegerUpperBound() {
		return integerUpperBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegerUpperBound(int newIntegerUpperBound) {
		int oldIntegerUpperBound = integerUpperBound;
		integerUpperBound = newIntegerUpperBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.INTEGER_RANGE__INTEGER_UPPER_BOUND, oldIntegerUpperBound, integerUpperBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIntegerLowerBound() {
		return integerLowerBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegerLowerBound(int newIntegerLowerBound) {
		int oldIntegerLowerBound = integerLowerBound;
		integerLowerBound = newIntegerLowerBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.INTEGER_RANGE__INTEGER_LOWER_BOUND, oldIntegerLowerBound, integerLowerBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyConstant getConstantUpperBound() {
		if (constantUpperBound != null && constantUpperBound.eIsProxy()) {
			InternalEObject oldConstantUpperBound = (InternalEObject)constantUpperBound;
			constantUpperBound = (PropertyConstant)eResolveProxy(oldConstantUpperBound);
			if (constantUpperBound != oldConstantUpperBound) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdsConfigPackage.INTEGER_RANGE__CONSTANT_UPPER_BOUND, oldConstantUpperBound, constantUpperBound));
			}
		}
		return constantUpperBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyConstant basicGetConstantUpperBound() {
		return constantUpperBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstantUpperBound(PropertyConstant newConstantUpperBound) {
		PropertyConstant oldConstantUpperBound = constantUpperBound;
		constantUpperBound = newConstantUpperBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.INTEGER_RANGE__CONSTANT_UPPER_BOUND, oldConstantUpperBound, constantUpperBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyConstant getConstantLowerBound() {
		if (constantLowerBound != null && constantLowerBound.eIsProxy()) {
			InternalEObject oldConstantLowerBound = (InternalEObject)constantLowerBound;
			constantLowerBound = (PropertyConstant)eResolveProxy(oldConstantLowerBound);
			if (constantLowerBound != oldConstantLowerBound) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdsConfigPackage.INTEGER_RANGE__CONSTANT_LOWER_BOUND, oldConstantLowerBound, constantLowerBound));
			}
		}
		return constantLowerBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyConstant basicGetConstantLowerBound() {
		return constantLowerBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstantLowerBound(PropertyConstant newConstantLowerBound) {
		PropertyConstant oldConstantLowerBound = constantLowerBound;
		constantLowerBound = newConstantLowerBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.INTEGER_RANGE__CONSTANT_LOWER_BOUND, oldConstantLowerBound, constantLowerBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdsConfigPackage.INTEGER_RANGE__INTEGER_UPPER_BOUND:
				return getIntegerUpperBound();
			case OdsConfigPackage.INTEGER_RANGE__INTEGER_LOWER_BOUND:
				return getIntegerLowerBound();
			case OdsConfigPackage.INTEGER_RANGE__CONSTANT_UPPER_BOUND:
				if (resolve) return getConstantUpperBound();
				return basicGetConstantUpperBound();
			case OdsConfigPackage.INTEGER_RANGE__CONSTANT_LOWER_BOUND:
				if (resolve) return getConstantLowerBound();
				return basicGetConstantLowerBound();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdsConfigPackage.INTEGER_RANGE__INTEGER_UPPER_BOUND:
				setIntegerUpperBound((Integer)newValue);
				return;
			case OdsConfigPackage.INTEGER_RANGE__INTEGER_LOWER_BOUND:
				setIntegerLowerBound((Integer)newValue);
				return;
			case OdsConfigPackage.INTEGER_RANGE__CONSTANT_UPPER_BOUND:
				setConstantUpperBound((PropertyConstant)newValue);
				return;
			case OdsConfigPackage.INTEGER_RANGE__CONSTANT_LOWER_BOUND:
				setConstantLowerBound((PropertyConstant)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.INTEGER_RANGE__INTEGER_UPPER_BOUND:
				setIntegerUpperBound(INTEGER_UPPER_BOUND_EDEFAULT);
				return;
			case OdsConfigPackage.INTEGER_RANGE__INTEGER_LOWER_BOUND:
				setIntegerLowerBound(INTEGER_LOWER_BOUND_EDEFAULT);
				return;
			case OdsConfigPackage.INTEGER_RANGE__CONSTANT_UPPER_BOUND:
				setConstantUpperBound((PropertyConstant)null);
				return;
			case OdsConfigPackage.INTEGER_RANGE__CONSTANT_LOWER_BOUND:
				setConstantLowerBound((PropertyConstant)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.INTEGER_RANGE__INTEGER_UPPER_BOUND:
				return integerUpperBound != INTEGER_UPPER_BOUND_EDEFAULT;
			case OdsConfigPackage.INTEGER_RANGE__INTEGER_LOWER_BOUND:
				return integerLowerBound != INTEGER_LOWER_BOUND_EDEFAULT;
			case OdsConfigPackage.INTEGER_RANGE__CONSTANT_UPPER_BOUND:
				return constantUpperBound != null;
			case OdsConfigPackage.INTEGER_RANGE__CONSTANT_LOWER_BOUND:
				return constantLowerBound != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (integerUpperBound: ");
		result.append(integerUpperBound);
		result.append(", integerLowerBound: ");
		result.append(integerLowerBound);
		result.append(')');
		return result.toString();
	}

} //IntegerRangeImpl
