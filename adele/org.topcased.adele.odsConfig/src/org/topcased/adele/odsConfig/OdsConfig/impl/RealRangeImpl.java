/**
 * <copyright>
 * </copyright>
 *
 * $Id: RealRangeImpl.java,v 1.2 2011-04-19 14:09:35 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;
import org.topcased.adele.odsConfig.OdsConfig.PropertyConstant;
import org.topcased.adele.odsConfig.OdsConfig.RealRange;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Real Range</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.RealRangeImpl#getRealUpperBound <em>Real Upper Bound</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.RealRangeImpl#getRealLowerBound <em>Real Lower Bound</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.RealRangeImpl#getConstantLowerBound <em>Constant Lower Bound</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.RealRangeImpl#getConstantUpperBound <em>Constant Upper Bound</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RealRangeImpl extends EObjectImpl implements RealRange {
	/**
	 * The default value of the '{@link #getRealUpperBound() <em>Real Upper Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRealUpperBound()
	 * @generated
	 * @ordered
	 */
	protected static final float REAL_UPPER_BOUND_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getRealUpperBound() <em>Real Upper Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRealUpperBound()
	 * @generated
	 * @ordered
	 */
	protected float realUpperBound = REAL_UPPER_BOUND_EDEFAULT;

	/**
	 * The default value of the '{@link #getRealLowerBound() <em>Real Lower Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRealLowerBound()
	 * @generated
	 * @ordered
	 */
	protected static final float REAL_LOWER_BOUND_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getRealLowerBound() <em>Real Lower Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRealLowerBound()
	 * @generated
	 * @ordered
	 */
	protected float realLowerBound = REAL_LOWER_BOUND_EDEFAULT;

	/**
	 * The cached value of the '{@link #getConstantLowerBound() <em>Constant Lower Bound</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstantLowerBound()
	 * @generated
	 * @ordered
	 */
	protected PropertyConstant constantLowerBound;

	/**
	 * The cached value of the '{@link #getConstantUpperBound() <em>Constant Upper Bound</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstantUpperBound()
	 * @generated
	 * @ordered
	 */
	protected PropertyConstant constantUpperBound;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RealRangeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdsConfigPackage.Literals.REAL_RANGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getRealUpperBound() {
		return realUpperBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRealUpperBound(float newRealUpperBound) {
		float oldRealUpperBound = realUpperBound;
		realUpperBound = newRealUpperBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.REAL_RANGE__REAL_UPPER_BOUND, oldRealUpperBound, realUpperBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getRealLowerBound() {
		return realLowerBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRealLowerBound(float newRealLowerBound) {
		float oldRealLowerBound = realLowerBound;
		realLowerBound = newRealLowerBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.REAL_RANGE__REAL_LOWER_BOUND, oldRealLowerBound, realLowerBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyConstant getConstantLowerBound() {
		if (constantLowerBound != null && constantLowerBound.eIsProxy()) {
			InternalEObject oldConstantLowerBound = (InternalEObject)constantLowerBound;
			constantLowerBound = (PropertyConstant)eResolveProxy(oldConstantLowerBound);
			if (constantLowerBound != oldConstantLowerBound) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdsConfigPackage.REAL_RANGE__CONSTANT_LOWER_BOUND, oldConstantLowerBound, constantLowerBound));
			}
		}
		return constantLowerBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyConstant basicGetConstantLowerBound() {
		return constantLowerBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstantLowerBound(PropertyConstant newConstantLowerBound) {
		PropertyConstant oldConstantLowerBound = constantLowerBound;
		constantLowerBound = newConstantLowerBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.REAL_RANGE__CONSTANT_LOWER_BOUND, oldConstantLowerBound, constantLowerBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyConstant getConstantUpperBound() {
		if (constantUpperBound != null && constantUpperBound.eIsProxy()) {
			InternalEObject oldConstantUpperBound = (InternalEObject)constantUpperBound;
			constantUpperBound = (PropertyConstant)eResolveProxy(oldConstantUpperBound);
			if (constantUpperBound != oldConstantUpperBound) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdsConfigPackage.REAL_RANGE__CONSTANT_UPPER_BOUND, oldConstantUpperBound, constantUpperBound));
			}
		}
		return constantUpperBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyConstant basicGetConstantUpperBound() {
		return constantUpperBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstantUpperBound(PropertyConstant newConstantUpperBound) {
		PropertyConstant oldConstantUpperBound = constantUpperBound;
		constantUpperBound = newConstantUpperBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.REAL_RANGE__CONSTANT_UPPER_BOUND, oldConstantUpperBound, constantUpperBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdsConfigPackage.REAL_RANGE__REAL_UPPER_BOUND:
				return getRealUpperBound();
			case OdsConfigPackage.REAL_RANGE__REAL_LOWER_BOUND:
				return getRealLowerBound();
			case OdsConfigPackage.REAL_RANGE__CONSTANT_LOWER_BOUND:
				if (resolve) return getConstantLowerBound();
				return basicGetConstantLowerBound();
			case OdsConfigPackage.REAL_RANGE__CONSTANT_UPPER_BOUND:
				if (resolve) return getConstantUpperBound();
				return basicGetConstantUpperBound();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdsConfigPackage.REAL_RANGE__REAL_UPPER_BOUND:
				setRealUpperBound((Float)newValue);
				return;
			case OdsConfigPackage.REAL_RANGE__REAL_LOWER_BOUND:
				setRealLowerBound((Float)newValue);
				return;
			case OdsConfigPackage.REAL_RANGE__CONSTANT_LOWER_BOUND:
				setConstantLowerBound((PropertyConstant)newValue);
				return;
			case OdsConfigPackage.REAL_RANGE__CONSTANT_UPPER_BOUND:
				setConstantUpperBound((PropertyConstant)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.REAL_RANGE__REAL_UPPER_BOUND:
				setRealUpperBound(REAL_UPPER_BOUND_EDEFAULT);
				return;
			case OdsConfigPackage.REAL_RANGE__REAL_LOWER_BOUND:
				setRealLowerBound(REAL_LOWER_BOUND_EDEFAULT);
				return;
			case OdsConfigPackage.REAL_RANGE__CONSTANT_LOWER_BOUND:
				setConstantLowerBound((PropertyConstant)null);
				return;
			case OdsConfigPackage.REAL_RANGE__CONSTANT_UPPER_BOUND:
				setConstantUpperBound((PropertyConstant)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.REAL_RANGE__REAL_UPPER_BOUND:
				return realUpperBound != REAL_UPPER_BOUND_EDEFAULT;
			case OdsConfigPackage.REAL_RANGE__REAL_LOWER_BOUND:
				return realLowerBound != REAL_LOWER_BOUND_EDEFAULT;
			case OdsConfigPackage.REAL_RANGE__CONSTANT_LOWER_BOUND:
				return constantLowerBound != null;
			case OdsConfigPackage.REAL_RANGE__CONSTANT_UPPER_BOUND:
				return constantUpperBound != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (realUpperBound: ");
		result.append(realUpperBound);
		result.append(", realLowerBound: ");
		result.append(realLowerBound);
		result.append(')');
		return result.toString();
	}

} //RealRangeImpl
