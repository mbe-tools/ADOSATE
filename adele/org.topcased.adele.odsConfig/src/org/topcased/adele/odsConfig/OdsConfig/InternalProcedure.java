/**
 * <copyright>
 * </copyright>
 *
 * $Id: InternalProcedure.java,v 1.1 2010-10-27 09:04:24 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Internal Procedure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.InternalProcedure#getName <em>Name</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.InternalProcedure#getSubject <em>Subject</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getInternalProcedure()
 * @model extendedMetaData="name='InternalProcedure' kind='empty'"
 * @generated
 */
public interface InternalProcedure extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * The literals are from the enumeration {@link org.topcased.adele.odsConfig.OdsConfig.Procedure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Procedure
	 * @see #isSetName()
	 * @see #unsetName()
	 * @see #setName(Procedure)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getInternalProcedure_Name()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='name' namespace='##targetNamespace'"
	 * @generated
	 */
	Procedure getName();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.InternalProcedure#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Procedure
	 * @see #isSetName()
	 * @see #unsetName()
	 * @see #getName()
	 * @generated
	 */
	void setName(Procedure value);

	/**
	 * Unsets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.InternalProcedure#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetName()
	 * @see #getName()
	 * @see #setName(Procedure)
	 * @generated
	 */
	void unsetName();

	/**
	 * Returns whether the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.InternalProcedure#getName <em>Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Name</em>' attribute is set.
	 * @see #unsetName()
	 * @see #getName()
	 * @see #setName(Procedure)
	 * @generated
	 */
	boolean isSetName();

	/**
	 * Returns the value of the '<em><b>Subject</b></em>' attribute.
	 * The default value is <code>"context"</code>.
	 * The literals are from the enumeration {@link org.topcased.adele.odsConfig.OdsConfig.SubjectType1}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subject</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subject</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.SubjectType1
	 * @see #isSetSubject()
	 * @see #unsetSubject()
	 * @see #setSubject(SubjectType1)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getInternalProcedure_Subject()
	 * @model default="context" unsettable="true"
	 *        extendedMetaData="kind='attribute' name='subject' namespace='##targetNamespace'"
	 * @generated
	 */
	SubjectType1 getSubject();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.InternalProcedure#getSubject <em>Subject</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subject</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.SubjectType1
	 * @see #isSetSubject()
	 * @see #unsetSubject()
	 * @see #getSubject()
	 * @generated
	 */
	void setSubject(SubjectType1 value);

	/**
	 * Unsets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.InternalProcedure#getSubject <em>Subject</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSubject()
	 * @see #getSubject()
	 * @see #setSubject(SubjectType1)
	 * @generated
	 */
	void unsetSubject();

	/**
	 * Returns whether the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.InternalProcedure#getSubject <em>Subject</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Subject</em>' attribute is set.
	 * @see #unsetSubject()
	 * @see #getSubject()
	 * @see #setSubject(SubjectType1)
	 * @generated
	 */
	boolean isSetSubject();

} // InternalProcedure
