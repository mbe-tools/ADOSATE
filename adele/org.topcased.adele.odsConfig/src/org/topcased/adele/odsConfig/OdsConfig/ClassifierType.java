/**
 * <copyright>
 * </copyright>
 *
 * $Id: ClassifierType.java,v 1.1 2010-10-27 09:04:22 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Classifier Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getClassifierType()
 * @model extendedMetaData="name='ClassifierType' kind='empty'"
 * @generated
 */
public interface ClassifierType extends PrimitivTypesType {
} // ClassifierType
