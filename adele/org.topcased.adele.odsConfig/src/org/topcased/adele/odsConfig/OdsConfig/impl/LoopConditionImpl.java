/**
 * <copyright>
 * </copyright>
 *
 * $Id: LoopConditionImpl.java,v 1.2 2011-04-19 14:09:33 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.topcased.adele.odsConfig.OdsConfig.LoopCondition;
import org.topcased.adele.odsConfig.OdsConfig.LoopMode;
import org.topcased.adele.odsConfig.OdsConfig.ODSection;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;
import org.topcased.adele.odsConfig.OdsConfig.Procedure;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Loop Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.LoopConditionImpl#getODSection <em>OD Section</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.LoopConditionImpl#getMode <em>Mode</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.LoopConditionImpl#getProcedure <em>Procedure</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LoopConditionImpl extends EObjectImpl implements LoopCondition {
	/**
	 * The cached value of the '{@link #getODSection() <em>OD Section</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getODSection()
	 * @generated
	 * @ordered
	 */
	protected EList<ODSection> oDSection;

	/**
	 * The default value of the '{@link #getMode() <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMode()
	 * @generated
	 * @ordered
	 */
	protected static final LoopMode MODE_EDEFAULT = LoopMode.FLAT;

	/**
	 * The cached value of the '{@link #getMode() <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMode()
	 * @generated
	 * @ordered
	 */
	protected LoopMode mode = MODE_EDEFAULT;

	/**
	 * This is true if the Mode attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean modeESet;

	/**
	 * The default value of the '{@link #getProcedure() <em>Procedure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcedure()
	 * @generated
	 * @ordered
	 */
	protected static final Procedure PROCEDURE_EDEFAULT = Procedure.GET_CURRENT_PROJECT_DIRECTORY;

	/**
	 * The cached value of the '{@link #getProcedure() <em>Procedure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcedure()
	 * @generated
	 * @ordered
	 */
	protected Procedure procedure = PROCEDURE_EDEFAULT;

	/**
	 * This is true if the Procedure attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean procedureESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LoopConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdsConfigPackage.Literals.LOOP_CONDITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ODSection> getODSection() {
		if (oDSection == null) {
			oDSection = new EObjectContainmentEList<ODSection>(ODSection.class, this, OdsConfigPackage.LOOP_CONDITION__OD_SECTION);
		}
		return oDSection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoopMode getMode() {
		return mode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMode(LoopMode newMode) {
		LoopMode oldMode = mode;
		mode = newMode == null ? MODE_EDEFAULT : newMode;
		boolean oldModeESet = modeESet;
		modeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.LOOP_CONDITION__MODE, oldMode, mode, !oldModeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMode() {
		LoopMode oldMode = mode;
		boolean oldModeESet = modeESet;
		mode = MODE_EDEFAULT;
		modeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdsConfigPackage.LOOP_CONDITION__MODE, oldMode, MODE_EDEFAULT, oldModeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMode() {
		return modeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure getProcedure() {
		return procedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcedure(Procedure newProcedure) {
		Procedure oldProcedure = procedure;
		procedure = newProcedure == null ? PROCEDURE_EDEFAULT : newProcedure;
		boolean oldProcedureESet = procedureESet;
		procedureESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.LOOP_CONDITION__PROCEDURE, oldProcedure, procedure, !oldProcedureESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetProcedure() {
		Procedure oldProcedure = procedure;
		boolean oldProcedureESet = procedureESet;
		procedure = PROCEDURE_EDEFAULT;
		procedureESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdsConfigPackage.LOOP_CONDITION__PROCEDURE, oldProcedure, PROCEDURE_EDEFAULT, oldProcedureESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetProcedure() {
		return procedureESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdsConfigPackage.LOOP_CONDITION__OD_SECTION:
				return ((InternalEList<?>)getODSection()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdsConfigPackage.LOOP_CONDITION__OD_SECTION:
				return getODSection();
			case OdsConfigPackage.LOOP_CONDITION__MODE:
				return getMode();
			case OdsConfigPackage.LOOP_CONDITION__PROCEDURE:
				return getProcedure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdsConfigPackage.LOOP_CONDITION__OD_SECTION:
				getODSection().clear();
				getODSection().addAll((Collection<? extends ODSection>)newValue);
				return;
			case OdsConfigPackage.LOOP_CONDITION__MODE:
				setMode((LoopMode)newValue);
				return;
			case OdsConfigPackage.LOOP_CONDITION__PROCEDURE:
				setProcedure((Procedure)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.LOOP_CONDITION__OD_SECTION:
				getODSection().clear();
				return;
			case OdsConfigPackage.LOOP_CONDITION__MODE:
				unsetMode();
				return;
			case OdsConfigPackage.LOOP_CONDITION__PROCEDURE:
				unsetProcedure();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.LOOP_CONDITION__OD_SECTION:
				return oDSection != null && !oDSection.isEmpty();
			case OdsConfigPackage.LOOP_CONDITION__MODE:
				return isSetMode();
			case OdsConfigPackage.LOOP_CONDITION__PROCEDURE:
				return isSetProcedure();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mode: ");
		if (modeESet) result.append(mode); else result.append("<unset>");
		result.append(", procedure: ");
		if (procedureESet) result.append(procedure); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //LoopConditionImpl
