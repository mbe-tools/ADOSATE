/**
 * <copyright>
 * </copyright>
 *
 * $Id: NumericType.java,v 1.1 2010-10-27 09:04:24 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Numeric Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getNumericType()
 * @model extendedMetaData="name='NumericType'"
 * @generated
 */
public enum NumericType implements Enumerator {
	/**
	 * The '<em><b>Aadl Integer</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AADL_INTEGER_VALUE
	 * @generated
	 * @ordered
	 */
	AADL_INTEGER(0, "aadlInteger", "aadlInteger"),

	/**
	 * The '<em><b>Aadl Real</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AADL_REAL_VALUE
	 * @generated
	 * @ordered
	 */
	AADL_REAL(1, "aadlReal", "aadlReal");

	/**
	 * The '<em><b>Aadl Integer</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Aadl Integer</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AADL_INTEGER
	 * @model name="aadlInteger"
	 * @generated
	 * @ordered
	 */
	public static final int AADL_INTEGER_VALUE = 0;

	/**
	 * The '<em><b>Aadl Real</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Aadl Real</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AADL_REAL
	 * @model name="aadlReal"
	 * @generated
	 * @ordered
	 */
	public static final int AADL_REAL_VALUE = 1;

	/**
	 * An array of all the '<em><b>Numeric Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final NumericType[] VALUES_ARRAY =
		new NumericType[] {
			AADL_INTEGER,
			AADL_REAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Numeric Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<NumericType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Numeric Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static NumericType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			NumericType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Numeric Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static NumericType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			NumericType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Numeric Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static NumericType get(int value) {
		switch (value) {
			case AADL_INTEGER_VALUE: return AADL_INTEGER;
			case AADL_REAL_VALUE: return AADL_REAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private NumericType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //NumericType
