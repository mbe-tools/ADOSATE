/**
 * <copyright>
 * </copyright>
 *
 * $Id: RealRange.java,v 1.1 2010-10-27 09:04:21 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Real Range</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.RealRange#getRealUpperBound <em>Real Upper Bound</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.RealRange#getRealLowerBound <em>Real Lower Bound</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.RealRange#getConstantLowerBound <em>Constant Lower Bound</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.RealRange#getConstantUpperBound <em>Constant Upper Bound</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getRealRange()
 * @model extendedMetaData="name='realRange' kind='elementOnly'"
 * @generated
 */
public interface RealRange extends EObject {
	/**
	 * Returns the value of the '<em><b>Real Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Real Upper Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Real Upper Bound</em>' attribute.
	 * @see #setRealUpperBound(float)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getRealRange_RealUpperBound()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Float"
	 *        extendedMetaData="kind='element' name='realUpperBound' namespace='##targetNamespace'"
	 * @generated
	 */
	float getRealUpperBound();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.RealRange#getRealUpperBound <em>Real Upper Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Real Upper Bound</em>' attribute.
	 * @see #getRealUpperBound()
	 * @generated
	 */
	void setRealUpperBound(float value);

	/**
	 * Returns the value of the '<em><b>Real Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Real Lower Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Real Lower Bound</em>' attribute.
	 * @see #setRealLowerBound(float)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getRealRange_RealLowerBound()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Float"
	 *        extendedMetaData="kind='element' name='realLowerBound' namespace='##targetNamespace'"
	 * @generated
	 */
	float getRealLowerBound();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.RealRange#getRealLowerBound <em>Real Lower Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Real Lower Bound</em>' attribute.
	 * @see #getRealLowerBound()
	 * @generated
	 */
	void setRealLowerBound(float value);

	/**
	 * Returns the value of the '<em><b>Constant Lower Bound</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant Lower Bound</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant Lower Bound</em>' reference.
	 * @see #setConstantLowerBound(PropertyConstant)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getRealRange_ConstantLowerBound()
	 * @model
	 * @generated
	 */
	PropertyConstant getConstantLowerBound();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.RealRange#getConstantLowerBound <em>Constant Lower Bound</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant Lower Bound</em>' reference.
	 * @see #getConstantLowerBound()
	 * @generated
	 */
	void setConstantLowerBound(PropertyConstant value);

	/**
	 * Returns the value of the '<em><b>Constant Upper Bound</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant Upper Bound</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant Upper Bound</em>' reference.
	 * @see #setConstantUpperBound(PropertyConstant)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getRealRange_ConstantUpperBound()
	 * @model
	 * @generated
	 */
	PropertyConstant getConstantUpperBound();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.RealRange#getConstantUpperBound <em>Constant Upper Bound</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant Upper Bound</em>' reference.
	 * @see #getConstantUpperBound()
	 * @generated
	 */
	void setConstantUpperBound(PropertyConstant value);

} // RealRange
