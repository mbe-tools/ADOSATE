/**
 * <copyright>
 * </copyright>
 *
 * $Id: RecordFieldType.java,v 1.1 2010-10-27 09:04:24 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Record Field Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.RecordFieldType#getType <em>Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.RecordFieldType#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getRecordFieldType()
 * @model extendedMetaData="name='RecordField_._type' kind='elementOnly'"
 * @generated
 */
public interface RecordFieldType extends EObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Type)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getRecordFieldType_Type()
	 * @model required="true"
	 * @generated
	 */
	Type getType();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.RecordFieldType#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Type value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getRecordFieldType_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.RecordFieldType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // RecordFieldType
