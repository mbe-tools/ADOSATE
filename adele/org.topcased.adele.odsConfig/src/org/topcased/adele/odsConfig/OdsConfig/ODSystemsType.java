/**
 * <copyright>
 * </copyright>
 *
 * $Id: ODSystemsType.java,v 1.2 2011-04-19 14:13:30 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OD Systems Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getODSystem <em>OD System</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getId <em>Id</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getLabel <em>Label</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getMode <em>Mode</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSystemsType()
 * @model extendedMetaData="name='ODSystems_._type' kind='elementOnly'"
 * @generated
 */
public interface ODSystemsType extends EObject {
	/**
	 * Returns the value of the '<em><b>OD System</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.adele.odsConfig.OdsConfig.ODSystem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>OD System</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OD System</em>' containment reference list.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSystemsType_ODSystem()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ODSystem' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODSystem> getODSystem();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSystemsType_Id()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='id' namespace='##targetNamespace'"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSystemsType_Label()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='label' namespace='##targetNamespace'"
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Mode</b></em>' attribute.
	 * The default value is <code>"HIERARCHICAL"</code>.
	 * The literals are from the enumeration {@link org.topcased.adele.odsConfig.OdsConfig.Mode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mode</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Mode
	 * @see #isSetMode()
	 * @see #unsetMode()
	 * @see #setMode(Mode)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSystemsType_Mode()
	 * @model default="HIERARCHICAL" unsettable="true"
	 *        extendedMetaData="kind='attribute' name='mode' namespace='##targetNamespace'"
	 * @generated
	 */
	Mode getMode();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getMode <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mode</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Mode
	 * @see #isSetMode()
	 * @see #unsetMode()
	 * @see #getMode()
	 * @generated
	 */
	void setMode(Mode value);

	/**
	 * Unsets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getMode <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMode()
	 * @see #getMode()
	 * @see #setMode(Mode)
	 * @generated
	 */
	void unsetMode();

	/**
	 * Returns whether the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystemsType#getMode <em>Mode</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Mode</em>' attribute is set.
	 * @see #unsetMode()
	 * @see #getMode()
	 * @see #setMode(Mode)
	 * @generated
	 */
	boolean isSetMode();

} // ODSystemsType
