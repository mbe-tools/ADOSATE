/**
 * <copyright>
 * </copyright>
 *
 * $Id: OdsConfigFactoryImpl.java,v 1.2 2011-04-19 14:09:34 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.topcased.adele.odsConfig.OdsConfig.AND;
import org.topcased.adele.odsConfig.OdsConfig.AadlBoolean;
import org.topcased.adele.odsConfig.OdsConfig.AadlString;
import org.topcased.adele.odsConfig.OdsConfig.ClassifierType;
import org.topcased.adele.odsConfig.OdsConfig.Condition;
import org.topcased.adele.odsConfig.OdsConfig.ConditionValue;
import org.topcased.adele.odsConfig.OdsConfig.DataField;
import org.topcased.adele.odsConfig.OdsConfig.DataFile;
import org.topcased.adele.odsConfig.OdsConfig.DataRecovering;
import org.topcased.adele.odsConfig.OdsConfig.DocumentRoot;
import org.topcased.adele.odsConfig.OdsConfig.EnumerationType;
import org.topcased.adele.odsConfig.OdsConfig.IntegerRange;
import org.topcased.adele.odsConfig.OdsConfig.InternalProcedure;
import org.topcased.adele.odsConfig.OdsConfig.Label;
import org.topcased.adele.odsConfig.OdsConfig.LoopCondition;
import org.topcased.adele.odsConfig.OdsConfig.LoopMode;
import org.topcased.adele.odsConfig.OdsConfig.Mode;
import org.topcased.adele.odsConfig.OdsConfig.NOT;
import org.topcased.adele.odsConfig.OdsConfig.NumberType;
import org.topcased.adele.odsConfig.OdsConfig.NumericType;
import org.topcased.adele.odsConfig.OdsConfig.ODSection;
import org.topcased.adele.odsConfig.OdsConfig.ODSystem;
import org.topcased.adele.odsConfig.OdsConfig.ODSystemsType;
import org.topcased.adele.odsConfig.OdsConfig.OR;
import org.topcased.adele.odsConfig.OdsConfig.ObjectCondition;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigFactory;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;
import org.topcased.adele.odsConfig.OdsConfig.Procedure;
import org.topcased.adele.odsConfig.OdsConfig.PropertyConstant;
import org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition;
import org.topcased.adele.odsConfig.OdsConfig.PropertySetType;
import org.topcased.adele.odsConfig.OdsConfig.PropertyType;
import org.topcased.adele.odsConfig.OdsConfig.RangeType;
import org.topcased.adele.odsConfig.OdsConfig.RealRange;
import org.topcased.adele.odsConfig.OdsConfig.RecordFieldType;
import org.topcased.adele.odsConfig.OdsConfig.RecordType;
import org.topcased.adele.odsConfig.OdsConfig.ReferenceType;
import org.topcased.adele.odsConfig.OdsConfig.Slash;
import org.topcased.adele.odsConfig.OdsConfig.SubjectType;
import org.topcased.adele.odsConfig.OdsConfig.SubjectType1;
import org.topcased.adele.odsConfig.OdsConfig.Text;
import org.topcased.adele.odsConfig.OdsConfig.Unit;
import org.topcased.adele.odsConfig.OdsConfig.UnitsType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OdsConfigFactoryImpl extends EFactoryImpl implements OdsConfigFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OdsConfigFactory init() {
		try {
			OdsConfigFactory theOdsConfigFactory = (OdsConfigFactory)EPackage.Registry.INSTANCE.getEFactory(OdsConfigPackage.eNS_URI);
			if (theOdsConfigFactory != null) {
				return theOdsConfigFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OdsConfigFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OdsConfigFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OdsConfigPackage.AADL_BOOLEAN: return createAadlBoolean();
			case OdsConfigPackage.AADL_STRING: return createAadlString();
			case OdsConfigPackage.AND: return createAND();
			case OdsConfigPackage.CLASSIFIER_TYPE: return createClassifierType();
			case OdsConfigPackage.CONDITION: return createCondition();
			case OdsConfigPackage.DATA_FIELD: return createDataField();
			case OdsConfigPackage.DATA_FILE: return createDataFile();
			case OdsConfigPackage.DATA_RECOVERING: return createDataRecovering();
			case OdsConfigPackage.DOCUMENT_ROOT: return createDocumentRoot();
			case OdsConfigPackage.ENUMERATION_TYPE: return createEnumerationType();
			case OdsConfigPackage.INTEGER_RANGE: return createIntegerRange();
			case OdsConfigPackage.INTERNAL_PROCEDURE: return createInternalProcedure();
			case OdsConfigPackage.LABEL: return createLabel();
			case OdsConfigPackage.LOOP_CONDITION: return createLoopCondition();
			case OdsConfigPackage.NOT: return createNOT();
			case OdsConfigPackage.NUMBER_TYPE: return createNumberType();
			case OdsConfigPackage.OBJECT_CONDITION: return createObjectCondition();
			case OdsConfigPackage.OD_SECTION: return createODSection();
			case OdsConfigPackage.OD_SYSTEM: return createODSystem();
			case OdsConfigPackage.OD_SYSTEMS_TYPE: return createODSystemsType();
			case OdsConfigPackage.OR: return createOR();
			case OdsConfigPackage.PROPERTY_CONSTANT: return createPropertyConstant();
			case OdsConfigPackage.PROPERTY_DEFINITION: return createPropertyDefinition();
			case OdsConfigPackage.PROPERTY_SET_TYPE: return createPropertySetType();
			case OdsConfigPackage.PROPERTY_TYPE: return createPropertyType();
			case OdsConfigPackage.RANGE_TYPE: return createRangeType();
			case OdsConfigPackage.REAL_RANGE: return createRealRange();
			case OdsConfigPackage.RECORD_FIELD_TYPE: return createRecordFieldType();
			case OdsConfigPackage.RECORD_TYPE: return createRecordType();
			case OdsConfigPackage.REFERENCE_TYPE: return createReferenceType();
			case OdsConfigPackage.SLASH: return createSlash();
			case OdsConfigPackage.TEXT: return createText();
			case OdsConfigPackage.UNIT: return createUnit();
			case OdsConfigPackage.UNITS_TYPE: return createUnitsType();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case OdsConfigPackage.CONDITION_VALUE:
				return createConditionValueFromString(eDataType, initialValue);
			case OdsConfigPackage.LOOP_MODE:
				return createLoopModeFromString(eDataType, initialValue);
			case OdsConfigPackage.MODE:
				return createModeFromString(eDataType, initialValue);
			case OdsConfigPackage.NUMERIC_TYPE:
				return createNumericTypeFromString(eDataType, initialValue);
			case OdsConfigPackage.PROCEDURE:
				return createProcedureFromString(eDataType, initialValue);
			case OdsConfigPackage.SUBJECT_TYPE:
				return createSubjectTypeFromString(eDataType, initialValue);
			case OdsConfigPackage.SUBJECT_TYPE1:
				return createSubjectType1FromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case OdsConfigPackage.CONDITION_VALUE:
				return convertConditionValueToString(eDataType, instanceValue);
			case OdsConfigPackage.LOOP_MODE:
				return convertLoopModeToString(eDataType, instanceValue);
			case OdsConfigPackage.MODE:
				return convertModeToString(eDataType, instanceValue);
			case OdsConfigPackage.NUMERIC_TYPE:
				return convertNumericTypeToString(eDataType, instanceValue);
			case OdsConfigPackage.PROCEDURE:
				return convertProcedureToString(eDataType, instanceValue);
			case OdsConfigPackage.SUBJECT_TYPE:
				return convertSubjectTypeToString(eDataType, instanceValue);
			case OdsConfigPackage.SUBJECT_TYPE1:
				return convertSubjectType1ToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AadlBoolean createAadlBoolean() {
		AadlBooleanImpl aadlBoolean = new AadlBooleanImpl();
		return aadlBoolean;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AadlString createAadlString() {
		AadlStringImpl aadlString = new AadlStringImpl();
		return aadlString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AND createAND() {
		ANDImpl and = new ANDImpl();
		return and;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifierType createClassifierType() {
		ClassifierTypeImpl classifierType = new ClassifierTypeImpl();
		return classifierType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Condition createCondition() {
		ConditionImpl condition = new ConditionImpl();
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataField createDataField() {
		DataFieldImpl dataField = new DataFieldImpl();
		return dataField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFile createDataFile() {
		DataFileImpl dataFile = new DataFileImpl();
		return dataFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataRecovering createDataRecovering() {
		DataRecoveringImpl dataRecovering = new DataRecoveringImpl();
		return dataRecovering;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DocumentRoot createDocumentRoot() {
		DocumentRootImpl documentRoot = new DocumentRootImpl();
		return documentRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationType createEnumerationType() {
		EnumerationTypeImpl enumerationType = new EnumerationTypeImpl();
		return enumerationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerRange createIntegerRange() {
		IntegerRangeImpl integerRange = new IntegerRangeImpl();
		return integerRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalProcedure createInternalProcedure() {
		InternalProcedureImpl internalProcedure = new InternalProcedureImpl();
		return internalProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Label createLabel() {
		LabelImpl label = new LabelImpl();
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoopCondition createLoopCondition() {
		LoopConditionImpl loopCondition = new LoopConditionImpl();
		return loopCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NOT createNOT() {
		NOTImpl not = new NOTImpl();
		return not;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NumberType createNumberType() {
		NumberTypeImpl numberType = new NumberTypeImpl();
		return numberType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectCondition createObjectCondition() {
		ObjectConditionImpl objectCondition = new ObjectConditionImpl();
		return objectCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ODSection createODSection() {
		ODSectionImpl odSection = new ODSectionImpl();
		return odSection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ODSystem createODSystem() {
		ODSystemImpl odSystem = new ODSystemImpl();
		return odSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ODSystemsType createODSystemsType() {
		ODSystemsTypeImpl odSystemsType = new ODSystemsTypeImpl();
		return odSystemsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OR createOR() {
		ORImpl or = new ORImpl();
		return or;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyConstant createPropertyConstant() {
		PropertyConstantImpl propertyConstant = new PropertyConstantImpl();
		return propertyConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyDefinition createPropertyDefinition() {
		PropertyDefinitionImpl propertyDefinition = new PropertyDefinitionImpl();
		return propertyDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertySetType createPropertySetType() {
		PropertySetTypeImpl propertySetType = new PropertySetTypeImpl();
		return propertySetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyType createPropertyType() {
		PropertyTypeImpl propertyType = new PropertyTypeImpl();
		return propertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeType createRangeType() {
		RangeTypeImpl rangeType = new RangeTypeImpl();
		return rangeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RealRange createRealRange() {
		RealRangeImpl realRange = new RealRangeImpl();
		return realRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordFieldType createRecordFieldType() {
		RecordFieldTypeImpl recordFieldType = new RecordFieldTypeImpl();
		return recordFieldType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordType createRecordType() {
		RecordTypeImpl recordType = new RecordTypeImpl();
		return recordType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceType createReferenceType() {
		ReferenceTypeImpl referenceType = new ReferenceTypeImpl();
		return referenceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Slash createSlash() {
		SlashImpl slash = new SlashImpl();
		return slash;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text createText() {
		TextImpl text = new TextImpl();
		return text;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Unit createUnit() {
		UnitImpl unit = new UnitImpl();
		return unit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnitsType createUnitsType() {
		UnitsTypeImpl unitsType = new UnitsTypeImpl();
		return unitsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionValue createConditionValueFromString(EDataType eDataType, String initialValue) {
		ConditionValue result = ConditionValue.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertConditionValueToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoopMode createLoopModeFromString(EDataType eDataType, String initialValue) {
		LoopMode result = LoopMode.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLoopModeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mode createModeFromString(EDataType eDataType, String initialValue) {
		Mode result = Mode.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertModeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NumericType createNumericTypeFromString(EDataType eDataType, String initialValue) {
		NumericType result = NumericType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNumericTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure createProcedureFromString(EDataType eDataType, String initialValue) {
		Procedure result = Procedure.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertProcedureToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubjectType createSubjectTypeFromString(EDataType eDataType, String initialValue) {
		SubjectType result = SubjectType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSubjectTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubjectType1 createSubjectType1FromString(EDataType eDataType, String initialValue) {
		SubjectType1 result = SubjectType1.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSubjectType1ToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OdsConfigPackage getOdsConfigPackage() {
		return (OdsConfigPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OdsConfigPackage getPackage() {
		return OdsConfigPackage.eINSTANCE;
	}

} //OdsConfigFactoryImpl
