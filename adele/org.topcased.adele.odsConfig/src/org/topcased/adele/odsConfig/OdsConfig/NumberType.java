/**
 * <copyright>
 * </copyright>
 *
 * $Id: NumberType.java,v 1.1 2010-10-27 09:04:22 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Number Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.NumberType#getIntegerRange <em>Integer Range</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.NumberType#getRealRange <em>Real Range</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.NumberType#getType <em>Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.NumberType#getUnits <em>Units</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.NumberType#getUnit <em>Unit</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getNumberType()
 * @model extendedMetaData="name='NumberType' kind='elementOnly'"
 * @generated
 */
public interface NumberType extends PrimitivTypesType {
	/**
	 * Returns the value of the '<em><b>Integer Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Range</em>' containment reference.
	 * @see #setIntegerRange(IntegerRange)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getNumberType_IntegerRange()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='integerRange' namespace='##targetNamespace'"
	 * @generated
	 */
	IntegerRange getIntegerRange();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.NumberType#getIntegerRange <em>Integer Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Range</em>' containment reference.
	 * @see #getIntegerRange()
	 * @generated
	 */
	void setIntegerRange(IntegerRange value);

	/**
	 * Returns the value of the '<em><b>Real Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Real Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Real Range</em>' containment reference.
	 * @see #setRealRange(RealRange)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getNumberType_RealRange()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='realRange' namespace='##targetNamespace'"
	 * @generated
	 */
	RealRange getRealRange();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.NumberType#getRealRange <em>Real Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Real Range</em>' containment reference.
	 * @see #getRealRange()
	 * @generated
	 */
	void setRealRange(RealRange value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.topcased.adele.odsConfig.OdsConfig.NumericType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.NumericType
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #setType(NumericType)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getNumberType_Type()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
	 * @generated
	 */
	NumericType getType();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.NumberType#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.NumericType
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #getType()
	 * @generated
	 */
	void setType(NumericType value);

	/**
	 * Unsets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.NumberType#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetType()
	 * @see #getType()
	 * @see #setType(NumericType)
	 * @generated
	 */
	void unsetType();

	/**
	 * Returns whether the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.NumberType#getType <em>Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Type</em>' attribute is set.
	 * @see #unsetType()
	 * @see #getType()
	 * @see #setType(NumericType)
	 * @generated
	 */
	boolean isSetType();

	/**
	 * Returns the value of the '<em><b>Units</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.adele.odsConfig.OdsConfig.Unit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Units</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Units</em>' containment reference list.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getNumberType_Units()
	 * @model containment="true"
	 * @generated
	 */
	EList<Unit> getUnits();

	/**
	 * Returns the value of the '<em><b>Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit</em>' reference.
	 * @see #setUnit(PropertyType)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getNumberType_Unit()
	 * @model
	 * @generated
	 */
	PropertyType getUnit();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.NumberType#getUnit <em>Unit</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit</em>' reference.
	 * @see #getUnit()
	 * @generated
	 */
	void setUnit(PropertyType value);

} // NumberType
