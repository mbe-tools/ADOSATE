/**
 * <copyright>
 * </copyright>
 *
 * $Id: AadlBoolean.java,v 1.1 2010-10-27 09:04:24 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aadl Boolean</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getAadlBoolean()
 * @model extendedMetaData="name='AadlBoolean' kind='empty'"
 * @generated
 */
public interface AadlBoolean extends PrimitivTypesType {
} // AadlBoolean
