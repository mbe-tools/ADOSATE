/**
 * <copyright>
 * </copyright>
 *
 * $Id: PropertyType.java,v 1.1 2010-10-27 09:04:24 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getName <em>Name</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getRecordType <em>Record Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getClassifierType <em>Classifier Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getReferenceType <em>Reference Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getEnumerationType <em>Enumeration Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getAadlBoolean <em>Aadl Boolean</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getAadlString <em>Aadl String</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getUnitsType <em>Units Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getRangeType <em>Range Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getNumberType <em>Number Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyType()
 * @model extendedMetaData="name='PropertyType' kind='elementOnly'"
 * @generated
 */
public interface PropertyType extends Type {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyType_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='name' namespace='##targetNamespace'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Record Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Type</em>' containment reference.
	 * @see #setRecordType(RecordType)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyType_RecordType()
	 * @model containment="true"
	 * @generated
	 */
	RecordType getRecordType();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getRecordType <em>Record Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Type</em>' containment reference.
	 * @see #getRecordType()
	 * @generated
	 */
	void setRecordType(RecordType value);

	/**
	 * Returns the value of the '<em><b>Classifier Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classifier Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classifier Type</em>' containment reference.
	 * @see #setClassifierType(ClassifierType)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyType_ClassifierType()
	 * @model containment="true"
	 * @generated
	 */
	ClassifierType getClassifierType();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getClassifierType <em>Classifier Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Classifier Type</em>' containment reference.
	 * @see #getClassifierType()
	 * @generated
	 */
	void setClassifierType(ClassifierType value);

	/**
	 * Returns the value of the '<em><b>Reference Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Type</em>' containment reference.
	 * @see #setReferenceType(ReferenceType)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyType_ReferenceType()
	 * @model containment="true"
	 * @generated
	 */
	ReferenceType getReferenceType();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getReferenceType <em>Reference Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference Type</em>' containment reference.
	 * @see #getReferenceType()
	 * @generated
	 */
	void setReferenceType(ReferenceType value);

	/**
	 * Returns the value of the '<em><b>Enumeration Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Type</em>' containment reference.
	 * @see #setEnumerationType(EnumerationType)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyType_EnumerationType()
	 * @model containment="true"
	 * @generated
	 */
	EnumerationType getEnumerationType();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getEnumerationType <em>Enumeration Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enumeration Type</em>' containment reference.
	 * @see #getEnumerationType()
	 * @generated
	 */
	void setEnumerationType(EnumerationType value);

	/**
	 * Returns the value of the '<em><b>Aadl Boolean</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aadl Boolean</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aadl Boolean</em>' containment reference.
	 * @see #setAadlBoolean(AadlBoolean)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyType_AadlBoolean()
	 * @model containment="true"
	 * @generated
	 */
	AadlBoolean getAadlBoolean();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getAadlBoolean <em>Aadl Boolean</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aadl Boolean</em>' containment reference.
	 * @see #getAadlBoolean()
	 * @generated
	 */
	void setAadlBoolean(AadlBoolean value);

	/**
	 * Returns the value of the '<em><b>Aadl String</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aadl String</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aadl String</em>' containment reference.
	 * @see #setAadlString(AadlString)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyType_AadlString()
	 * @model containment="true"
	 * @generated
	 */
	AadlString getAadlString();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getAadlString <em>Aadl String</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aadl String</em>' containment reference.
	 * @see #getAadlString()
	 * @generated
	 */
	void setAadlString(AadlString value);

	/**
	 * Returns the value of the '<em><b>Units Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Units Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Units Type</em>' containment reference.
	 * @see #setUnitsType(UnitsType)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyType_UnitsType()
	 * @model containment="true"
	 * @generated
	 */
	UnitsType getUnitsType();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getUnitsType <em>Units Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Units Type</em>' containment reference.
	 * @see #getUnitsType()
	 * @generated
	 */
	void setUnitsType(UnitsType value);

	/**
	 * Returns the value of the '<em><b>Range Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range Type</em>' containment reference.
	 * @see #setRangeType(RangeType)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyType_RangeType()
	 * @model containment="true"
	 * @generated
	 */
	RangeType getRangeType();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getRangeType <em>Range Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range Type</em>' containment reference.
	 * @see #getRangeType()
	 * @generated
	 */
	void setRangeType(RangeType value);

	/**
	 * Returns the value of the '<em><b>Number Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Type</em>' containment reference.
	 * @see #setNumberType(NumberType)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyType_NumberType()
	 * @model containment="true"
	 * @generated
	 */
	NumberType getNumberType();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyType#getNumberType <em>Number Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Type</em>' containment reference.
	 * @see #getNumberType()
	 * @generated
	 */
	void setNumberType(NumberType value);

} // PropertyType
