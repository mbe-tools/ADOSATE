/**
 * <copyright>
 * </copyright>
 *
 * $Id: Text.java,v 1.1 2010-10-27 09:04:25 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Text</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.Text#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getText()
 * @model extendedMetaData="name='Text' kind='empty'"
 * @generated
 */
public interface Text extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getText_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='value' namespace='##targetNamespace'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.Text#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

} // Text
