/**
 * <copyright>
 * </copyright>
 *
 * $Id: OdsConfigPackageImpl.java,v 1.2 2011-04-19 14:09:35 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;
import org.topcased.adele.odsConfig.OdsConfig.AadlBoolean;
import org.topcased.adele.odsConfig.OdsConfig.AadlString;
import org.topcased.adele.odsConfig.OdsConfig.ClassifierType;
import org.topcased.adele.odsConfig.OdsConfig.Condition;
import org.topcased.adele.odsConfig.OdsConfig.ConditionValue;
import org.topcased.adele.odsConfig.OdsConfig.DataField;
import org.topcased.adele.odsConfig.OdsConfig.DataFile;
import org.topcased.adele.odsConfig.OdsConfig.DataRecovering;
import org.topcased.adele.odsConfig.OdsConfig.DocumentRoot;
import org.topcased.adele.odsConfig.OdsConfig.EnumerationType;
import org.topcased.adele.odsConfig.OdsConfig.IntegerRange;
import org.topcased.adele.odsConfig.OdsConfig.InternalProcedure;
import org.topcased.adele.odsConfig.OdsConfig.Label;
import org.topcased.adele.odsConfig.OdsConfig.LoopCondition;
import org.topcased.adele.odsConfig.OdsConfig.LoopMode;
import org.topcased.adele.odsConfig.OdsConfig.Mode;
import org.topcased.adele.odsConfig.OdsConfig.NumberType;
import org.topcased.adele.odsConfig.OdsConfig.NumericType;
import org.topcased.adele.odsConfig.OdsConfig.ODSection;
import org.topcased.adele.odsConfig.OdsConfig.ODSystem;
import org.topcased.adele.odsConfig.OdsConfig.ODSystemsType;
import org.topcased.adele.odsConfig.OdsConfig.ObjectCondition;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigFactory;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;
import org.topcased.adele.odsConfig.OdsConfig.PrimitivTypesType;
import org.topcased.adele.odsConfig.OdsConfig.Procedure;
import org.topcased.adele.odsConfig.OdsConfig.PropertyConstant;
import org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition;
import org.topcased.adele.odsConfig.OdsConfig.PropertySetType;
import org.topcased.adele.odsConfig.OdsConfig.PropertyType;
import org.topcased.adele.odsConfig.OdsConfig.RangeType;
import org.topcased.adele.odsConfig.OdsConfig.RealRange;
import org.topcased.adele.odsConfig.OdsConfig.RecordFieldType;
import org.topcased.adele.odsConfig.OdsConfig.RecordType;
import org.topcased.adele.odsConfig.OdsConfig.ReferenceType;
import org.topcased.adele.odsConfig.OdsConfig.Slash;
import org.topcased.adele.odsConfig.OdsConfig.SubjectType;
import org.topcased.adele.odsConfig.OdsConfig.SubjectType1;
import org.topcased.adele.odsConfig.OdsConfig.Text;
import org.topcased.adele.odsConfig.OdsConfig.Type;
import org.topcased.adele.odsConfig.OdsConfig.Unit;
import org.topcased.adele.odsConfig.OdsConfig.UnitsType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OdsConfigPackageImpl extends EPackageImpl implements OdsConfigPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aadlBooleanEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aadlStringEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass andEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass classifierTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataFieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataFileEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataRecoveringEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass documentRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enumerationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass integerRangeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass internalProcedureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass labelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass loopConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass notEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass numberTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass objectConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass odSectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass odSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass odSystemsTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass orEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass primitivTypesTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyConstantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertySetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rangeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass realRangeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass recordFieldTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass recordTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referenceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass slashEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass textEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unitsTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum conditionValueEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum loopModeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum modeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum numericTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum procedureEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum subjectTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum subjectType1EEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OdsConfigPackageImpl() {
		super(eNS_URI, OdsConfigFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link OdsConfigPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OdsConfigPackage init() {
		if (isInited) return (OdsConfigPackage)EPackage.Registry.INSTANCE.getEPackage(OdsConfigPackage.eNS_URI);

		// Obtain or create and register package
		OdsConfigPackageImpl theOdsConfigPackage = (OdsConfigPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof OdsConfigPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new OdsConfigPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theOdsConfigPackage.createPackageContents();

		// Initialize created meta-data
		theOdsConfigPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOdsConfigPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OdsConfigPackage.eNS_URI, theOdsConfigPackage);
		return theOdsConfigPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAadlBoolean() {
		return aadlBooleanEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAadlString() {
		return aadlStringEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAND() {
		return andEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAND_Mixed() {
		return (EAttribute)andEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAND_Condition() {
		return (EReference)andEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAND_ConditionGroup() {
		return (EAttribute)andEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAND_AND() {
		return (EReference)andEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAND_OR() {
		return (EReference)andEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAND_NOT() {
		return (EReference)andEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClassifierType() {
		return classifierTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCondition() {
		return conditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCondition_Value() {
		return (EAttribute)conditionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataField() {
		return dataFieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataField_Name() {
		return (EAttribute)dataFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataFile() {
		return dataFileEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataFile_Mixed() {
		return (EAttribute)dataFileEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataFile_DataFileGroup() {
		return (EAttribute)dataFileEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataFile_Slash() {
		return (EReference)dataFileEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataFile_Text() {
		return (EReference)dataFileEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataFile_InternalProcedure() {
		return (EReference)dataFileEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataRecovering() {
		return dataRecoveringEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataRecovering_InternalProcedure() {
		return (EReference)dataRecoveringEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataRecovering_DataFile() {
		return (EReference)dataRecoveringEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataRecovering_DataField() {
		return (EReference)dataRecoveringEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDocumentRoot() {
		return documentRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocumentRoot_Mixed() {
		return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XMLNSPrefixMap() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XSISchemaLocation() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ODSystems() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_PropertySet() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnumerationType() {
		return enumerationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnumerationType_Item() {
		return (EAttribute)enumerationTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntegerRange() {
		return integerRangeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntegerRange_IntegerUpperBound() {
		return (EAttribute)integerRangeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntegerRange_IntegerLowerBound() {
		return (EAttribute)integerRangeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIntegerRange_ConstantUpperBound() {
		return (EReference)integerRangeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIntegerRange_ConstantLowerBound() {
		return (EReference)integerRangeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInternalProcedure() {
		return internalProcedureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInternalProcedure_Name() {
		return (EAttribute)internalProcedureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInternalProcedure_Subject() {
		return (EAttribute)internalProcedureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLabel() {
		return labelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLabel_Value() {
		return (EAttribute)labelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLabel_Format() {
		return (EAttribute)labelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLabel_Procedure() {
		return (EAttribute)labelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLabel_Subject() {
		return (EAttribute)labelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLoopCondition() {
		return loopConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLoopCondition_ODSection() {
		return (EReference)loopConditionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLoopCondition_Mode() {
		return (EAttribute)loopConditionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLoopCondition_Procedure() {
		return (EAttribute)loopConditionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNOT() {
		return notEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNOT_Mixed() {
		return (EAttribute)notEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNOT_Condition() {
		return (EReference)notEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNOT_ConditionGroup() {
		return (EAttribute)notEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNOT_AND() {
		return (EReference)notEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNOT_OR() {
		return (EReference)notEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNOT_NOT() {
		return (EReference)notEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNumberType() {
		return numberTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNumberType_IntegerRange() {
		return (EReference)numberTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNumberType_RealRange() {
		return (EReference)numberTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNumberType_Type() {
		return (EAttribute)numberTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNumberType_Units() {
		return (EReference)numberTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNumberType_Unit() {
		return (EReference)numberTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObjectCondition() {
		return objectConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getObjectCondition_Mixed() {
		return (EAttribute)objectConditionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectCondition_Condition() {
		return (EReference)objectConditionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getObjectCondition_ConditionGroup() {
		return (EAttribute)objectConditionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectCondition_AND() {
		return (EReference)objectConditionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectCondition_OR() {
		return (EReference)objectConditionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectCondition_NOT() {
		return (EReference)objectConditionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getODSection() {
		return odSectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getODSection_Label() {
		return (EReference)odSectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getODSection_ObjectCondition() {
		return (EReference)odSectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getODSection_DataRecovering() {
		return (EReference)odSectionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getODSection_ODSection() {
		return (EReference)odSectionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getODSection_Id() {
		return (EAttribute)odSectionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getODSection_Type() {
		return (EReference)odSectionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getODSystem() {
		return odSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getODSystem_ODSection() {
		return (EReference)odSystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getODSystem_Id() {
		return (EAttribute)odSystemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getODSystem_Label() {
		return (EAttribute)odSystemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getODSystem_Mode() {
		return (EAttribute)odSystemEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getODSystem_StyleSheet() {
		return (EAttribute)odSystemEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getODSystem_DataRecovering() {
		return (EReference)odSystemEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getODSystemsType() {
		return odSystemsTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getODSystemsType_ODSystem() {
		return (EReference)odSystemsTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getODSystemsType_Id() {
		return (EAttribute)odSystemsTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getODSystemsType_Label() {
		return (EAttribute)odSystemsTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getODSystemsType_Mode() {
		return (EAttribute)odSystemsTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOR() {
		return orEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOR_Mixed() {
		return (EAttribute)orEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOR_Condition() {
		return (EReference)orEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOR_ConditionGroup() {
		return (EAttribute)orEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOR_AND() {
		return (EReference)orEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOR_OR() {
		return (EReference)orEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOR_NOT() {
		return (EReference)orEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPrimitivTypesType() {
		return primitivTypesTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPropertyConstant() {
		return propertyConstantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertyConstant_Name() {
		return (EAttribute)propertyConstantEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertyConstant_Values() {
		return (EAttribute)propertyConstantEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyConstant_Type() {
		return (EReference)propertyConstantEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPropertyDefinition() {
		return propertyDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertyDefinition_Default() {
		return (EAttribute)propertyDefinitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertyDefinition_IsMultiValued() {
		return (EAttribute)propertyDefinitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertyDefinition_Name() {
		return (EAttribute)propertyDefinitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyDefinition_PropertyCondition() {
		return (EReference)propertyDefinitionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyDefinition_Type() {
		return (EReference)propertyDefinitionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPropertySetType() {
		return propertySetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertySetType_PropertyType() {
		return (EReference)propertySetTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertySetType_PropertyDefinition() {
		return (EReference)propertySetTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertySetType_PropertyConstant() {
		return (EReference)propertySetTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertySetType_Name() {
		return (EAttribute)propertySetTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPropertyType() {
		return propertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertyType_Name() {
		return (EAttribute)propertyTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyType_RecordType() {
		return (EReference)propertyTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyType_ClassifierType() {
		return (EReference)propertyTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyType_ReferenceType() {
		return (EReference)propertyTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyType_EnumerationType() {
		return (EReference)propertyTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyType_AadlBoolean() {
		return (EReference)propertyTypeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyType_AadlString() {
		return (EReference)propertyTypeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyType_UnitsType() {
		return (EReference)propertyTypeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyType_RangeType() {
		return (EReference)propertyTypeEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyType_NumberType() {
		return (EReference)propertyTypeEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRangeType() {
		return rangeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRangeType_NumberType() {
		return (EReference)rangeTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRealRange() {
		return realRangeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRealRange_RealUpperBound() {
		return (EAttribute)realRangeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRealRange_RealLowerBound() {
		return (EAttribute)realRangeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRealRange_ConstantLowerBound() {
		return (EReference)realRangeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRealRange_ConstantUpperBound() {
		return (EReference)realRangeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRecordFieldType() {
		return recordFieldTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRecordFieldType_Type() {
		return (EReference)recordFieldTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRecordFieldType_Name() {
		return (EAttribute)recordFieldTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRecordType() {
		return recordTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRecordType_RecordField() {
		return (EReference)recordTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferenceType() {
		return referenceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSlash() {
		return slashEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getText() {
		return textEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getText_Value() {
		return (EAttribute)textEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnit() {
		return unitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUnit_Unit() {
		return (EReference)unitEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnit_Multiplier() {
		return (EAttribute)unitEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnit_Name() {
		return (EAttribute)unitEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnitsType() {
		return unitsTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUnitsType_Unit() {
		return (EReference)unitsTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getType() {
		return typeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getConditionValue() {
		return conditionValueEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLoopMode() {
		return loopModeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMode() {
		return modeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getNumericType() {
		return numericTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getProcedure() {
		return procedureEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSubjectType() {
		return subjectTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSubjectType1() {
		return subjectType1EEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OdsConfigFactory getOdsConfigFactory() {
		return (OdsConfigFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		aadlBooleanEClass = createEClass(AADL_BOOLEAN);

		aadlStringEClass = createEClass(AADL_STRING);

		andEClass = createEClass(AND);
		createEAttribute(andEClass, AND__MIXED);
		createEReference(andEClass, AND__CONDITION);
		createEAttribute(andEClass, AND__CONDITION_GROUP);
		createEReference(andEClass, AND__AND);
		createEReference(andEClass, AND__OR);
		createEReference(andEClass, AND__NOT);

		classifierTypeEClass = createEClass(CLASSIFIER_TYPE);

		conditionEClass = createEClass(CONDITION);
		createEAttribute(conditionEClass, CONDITION__VALUE);

		dataFieldEClass = createEClass(DATA_FIELD);
		createEAttribute(dataFieldEClass, DATA_FIELD__NAME);

		dataFileEClass = createEClass(DATA_FILE);
		createEAttribute(dataFileEClass, DATA_FILE__MIXED);
		createEAttribute(dataFileEClass, DATA_FILE__DATA_FILE_GROUP);
		createEReference(dataFileEClass, DATA_FILE__SLASH);
		createEReference(dataFileEClass, DATA_FILE__TEXT);
		createEReference(dataFileEClass, DATA_FILE__INTERNAL_PROCEDURE);

		dataRecoveringEClass = createEClass(DATA_RECOVERING);
		createEReference(dataRecoveringEClass, DATA_RECOVERING__INTERNAL_PROCEDURE);
		createEReference(dataRecoveringEClass, DATA_RECOVERING__DATA_FILE);
		createEReference(dataRecoveringEClass, DATA_RECOVERING__DATA_FIELD);

		documentRootEClass = createEClass(DOCUMENT_ROOT);
		createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		createEReference(documentRootEClass, DOCUMENT_ROOT__OD_SYSTEMS);
		createEReference(documentRootEClass, DOCUMENT_ROOT__PROPERTY_SET);

		enumerationTypeEClass = createEClass(ENUMERATION_TYPE);
		createEAttribute(enumerationTypeEClass, ENUMERATION_TYPE__ITEM);

		integerRangeEClass = createEClass(INTEGER_RANGE);
		createEAttribute(integerRangeEClass, INTEGER_RANGE__INTEGER_UPPER_BOUND);
		createEAttribute(integerRangeEClass, INTEGER_RANGE__INTEGER_LOWER_BOUND);
		createEReference(integerRangeEClass, INTEGER_RANGE__CONSTANT_UPPER_BOUND);
		createEReference(integerRangeEClass, INTEGER_RANGE__CONSTANT_LOWER_BOUND);

		internalProcedureEClass = createEClass(INTERNAL_PROCEDURE);
		createEAttribute(internalProcedureEClass, INTERNAL_PROCEDURE__NAME);
		createEAttribute(internalProcedureEClass, INTERNAL_PROCEDURE__SUBJECT);

		labelEClass = createEClass(LABEL);
		createEAttribute(labelEClass, LABEL__VALUE);
		createEAttribute(labelEClass, LABEL__FORMAT);
		createEAttribute(labelEClass, LABEL__PROCEDURE);
		createEAttribute(labelEClass, LABEL__SUBJECT);

		loopConditionEClass = createEClass(LOOP_CONDITION);
		createEReference(loopConditionEClass, LOOP_CONDITION__OD_SECTION);
		createEAttribute(loopConditionEClass, LOOP_CONDITION__MODE);
		createEAttribute(loopConditionEClass, LOOP_CONDITION__PROCEDURE);

		notEClass = createEClass(NOT);
		createEAttribute(notEClass, NOT__MIXED);
		createEReference(notEClass, NOT__CONDITION);
		createEAttribute(notEClass, NOT__CONDITION_GROUP);
		createEReference(notEClass, NOT__AND);
		createEReference(notEClass, NOT__OR);
		createEReference(notEClass, NOT__NOT);

		numberTypeEClass = createEClass(NUMBER_TYPE);
		createEReference(numberTypeEClass, NUMBER_TYPE__INTEGER_RANGE);
		createEReference(numberTypeEClass, NUMBER_TYPE__REAL_RANGE);
		createEAttribute(numberTypeEClass, NUMBER_TYPE__TYPE);
		createEReference(numberTypeEClass, NUMBER_TYPE__UNITS);
		createEReference(numberTypeEClass, NUMBER_TYPE__UNIT);

		objectConditionEClass = createEClass(OBJECT_CONDITION);
		createEAttribute(objectConditionEClass, OBJECT_CONDITION__MIXED);
		createEReference(objectConditionEClass, OBJECT_CONDITION__CONDITION);
		createEAttribute(objectConditionEClass, OBJECT_CONDITION__CONDITION_GROUP);
		createEReference(objectConditionEClass, OBJECT_CONDITION__AND);
		createEReference(objectConditionEClass, OBJECT_CONDITION__OR);
		createEReference(objectConditionEClass, OBJECT_CONDITION__NOT);

		odSectionEClass = createEClass(OD_SECTION);
		createEReference(odSectionEClass, OD_SECTION__LABEL);
		createEReference(odSectionEClass, OD_SECTION__OBJECT_CONDITION);
		createEReference(odSectionEClass, OD_SECTION__DATA_RECOVERING);
		createEReference(odSectionEClass, OD_SECTION__OD_SECTION);
		createEAttribute(odSectionEClass, OD_SECTION__ID);
		createEReference(odSectionEClass, OD_SECTION__TYPE);

		odSystemEClass = createEClass(OD_SYSTEM);
		createEReference(odSystemEClass, OD_SYSTEM__OD_SECTION);
		createEAttribute(odSystemEClass, OD_SYSTEM__ID);
		createEAttribute(odSystemEClass, OD_SYSTEM__LABEL);
		createEAttribute(odSystemEClass, OD_SYSTEM__MODE);
		createEAttribute(odSystemEClass, OD_SYSTEM__STYLE_SHEET);
		createEReference(odSystemEClass, OD_SYSTEM__DATA_RECOVERING);

		odSystemsTypeEClass = createEClass(OD_SYSTEMS_TYPE);
		createEReference(odSystemsTypeEClass, OD_SYSTEMS_TYPE__OD_SYSTEM);
		createEAttribute(odSystemsTypeEClass, OD_SYSTEMS_TYPE__ID);
		createEAttribute(odSystemsTypeEClass, OD_SYSTEMS_TYPE__LABEL);
		createEAttribute(odSystemsTypeEClass, OD_SYSTEMS_TYPE__MODE);

		orEClass = createEClass(OR);
		createEAttribute(orEClass, OR__MIXED);
		createEReference(orEClass, OR__CONDITION);
		createEAttribute(orEClass, OR__CONDITION_GROUP);
		createEReference(orEClass, OR__AND);
		createEReference(orEClass, OR__OR);
		createEReference(orEClass, OR__NOT);

		primitivTypesTypeEClass = createEClass(PRIMITIV_TYPES_TYPE);

		propertyConstantEClass = createEClass(PROPERTY_CONSTANT);
		createEAttribute(propertyConstantEClass, PROPERTY_CONSTANT__NAME);
		createEAttribute(propertyConstantEClass, PROPERTY_CONSTANT__VALUES);
		createEReference(propertyConstantEClass, PROPERTY_CONSTANT__TYPE);

		propertyDefinitionEClass = createEClass(PROPERTY_DEFINITION);
		createEAttribute(propertyDefinitionEClass, PROPERTY_DEFINITION__DEFAULT);
		createEAttribute(propertyDefinitionEClass, PROPERTY_DEFINITION__IS_MULTI_VALUED);
		createEAttribute(propertyDefinitionEClass, PROPERTY_DEFINITION__NAME);
		createEReference(propertyDefinitionEClass, PROPERTY_DEFINITION__PROPERTY_CONDITION);
		createEReference(propertyDefinitionEClass, PROPERTY_DEFINITION__TYPE);

		propertySetTypeEClass = createEClass(PROPERTY_SET_TYPE);
		createEReference(propertySetTypeEClass, PROPERTY_SET_TYPE__PROPERTY_TYPE);
		createEReference(propertySetTypeEClass, PROPERTY_SET_TYPE__PROPERTY_DEFINITION);
		createEReference(propertySetTypeEClass, PROPERTY_SET_TYPE__PROPERTY_CONSTANT);
		createEAttribute(propertySetTypeEClass, PROPERTY_SET_TYPE__NAME);

		propertyTypeEClass = createEClass(PROPERTY_TYPE);
		createEAttribute(propertyTypeEClass, PROPERTY_TYPE__NAME);
		createEReference(propertyTypeEClass, PROPERTY_TYPE__RECORD_TYPE);
		createEReference(propertyTypeEClass, PROPERTY_TYPE__CLASSIFIER_TYPE);
		createEReference(propertyTypeEClass, PROPERTY_TYPE__REFERENCE_TYPE);
		createEReference(propertyTypeEClass, PROPERTY_TYPE__ENUMERATION_TYPE);
		createEReference(propertyTypeEClass, PROPERTY_TYPE__AADL_BOOLEAN);
		createEReference(propertyTypeEClass, PROPERTY_TYPE__AADL_STRING);
		createEReference(propertyTypeEClass, PROPERTY_TYPE__UNITS_TYPE);
		createEReference(propertyTypeEClass, PROPERTY_TYPE__RANGE_TYPE);
		createEReference(propertyTypeEClass, PROPERTY_TYPE__NUMBER_TYPE);

		rangeTypeEClass = createEClass(RANGE_TYPE);
		createEReference(rangeTypeEClass, RANGE_TYPE__NUMBER_TYPE);

		realRangeEClass = createEClass(REAL_RANGE);
		createEAttribute(realRangeEClass, REAL_RANGE__REAL_UPPER_BOUND);
		createEAttribute(realRangeEClass, REAL_RANGE__REAL_LOWER_BOUND);
		createEReference(realRangeEClass, REAL_RANGE__CONSTANT_LOWER_BOUND);
		createEReference(realRangeEClass, REAL_RANGE__CONSTANT_UPPER_BOUND);

		recordFieldTypeEClass = createEClass(RECORD_FIELD_TYPE);
		createEReference(recordFieldTypeEClass, RECORD_FIELD_TYPE__TYPE);
		createEAttribute(recordFieldTypeEClass, RECORD_FIELD_TYPE__NAME);

		recordTypeEClass = createEClass(RECORD_TYPE);
		createEReference(recordTypeEClass, RECORD_TYPE__RECORD_FIELD);

		referenceTypeEClass = createEClass(REFERENCE_TYPE);

		slashEClass = createEClass(SLASH);

		textEClass = createEClass(TEXT);
		createEAttribute(textEClass, TEXT__VALUE);

		unitEClass = createEClass(UNIT);
		createEReference(unitEClass, UNIT__UNIT);
		createEAttribute(unitEClass, UNIT__MULTIPLIER);
		createEAttribute(unitEClass, UNIT__NAME);

		unitsTypeEClass = createEClass(UNITS_TYPE);
		createEReference(unitsTypeEClass, UNITS_TYPE__UNIT);

		typeEClass = createEClass(TYPE);

		// Create enums
		conditionValueEEnum = createEEnum(CONDITION_VALUE);
		loopModeEEnum = createEEnum(LOOP_MODE);
		modeEEnum = createEEnum(MODE);
		numericTypeEEnum = createEEnum(NUMERIC_TYPE);
		procedureEEnum = createEEnum(PROCEDURE);
		subjectTypeEEnum = createEEnum(SUBJECT_TYPE);
		subjectType1EEnum = createEEnum(SUBJECT_TYPE1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		aadlBooleanEClass.getESuperTypes().add(this.getPrimitivTypesType());
		aadlStringEClass.getESuperTypes().add(this.getPrimitivTypesType());
		classifierTypeEClass.getESuperTypes().add(this.getPrimitivTypesType());
		enumerationTypeEClass.getESuperTypes().add(this.getPrimitivTypesType());
		numberTypeEClass.getESuperTypes().add(this.getPrimitivTypesType());
		primitivTypesTypeEClass.getESuperTypes().add(this.getType());
		propertyTypeEClass.getESuperTypes().add(this.getType());
		rangeTypeEClass.getESuperTypes().add(this.getPrimitivTypesType());
		recordTypeEClass.getESuperTypes().add(this.getPrimitivTypesType());
		referenceTypeEClass.getESuperTypes().add(this.getPrimitivTypesType());
		unitsTypeEClass.getESuperTypes().add(this.getPrimitivTypesType());

		// Initialize classes and features; add operations and parameters
		initEClass(aadlBooleanEClass, AadlBoolean.class, "AadlBoolean", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(aadlStringEClass, AadlString.class, "AadlString", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(andEClass, org.topcased.adele.odsConfig.OdsConfig.AND.class, "AND", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAND_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.AND.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAND_Condition(), this.getCondition(), null, "condition", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.AND.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAND_ConditionGroup(), ecorePackage.getEFeatureMapEntry(), "conditionGroup", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.AND.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAND_AND(), this.getAND(), null, "aND", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.AND.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAND_OR(), this.getOR(), null, "oR", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.AND.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAND_NOT(), this.getNOT(), null, "nOT", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.AND.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(classifierTypeEClass, ClassifierType.class, "ClassifierType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(conditionEClass, Condition.class, "Condition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCondition_Value(), this.getConditionValue(), "value", null, 0, 1, Condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataFieldEClass, DataField.class, "DataField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDataField_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, DataField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataFileEClass, DataFile.class, "DataFile", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDataFile_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, DataFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataFile_DataFileGroup(), ecorePackage.getEFeatureMapEntry(), "dataFileGroup", null, 0, -1, DataFile.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDataFile_Slash(), this.getSlash(), null, "slash", null, 0, -1, DataFile.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDataFile_Text(), this.getText(), null, "text", null, 0, -1, DataFile.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDataFile_InternalProcedure(), this.getInternalProcedure(), null, "internalProcedure", null, 0, -1, DataFile.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(dataRecoveringEClass, DataRecovering.class, "DataRecovering", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataRecovering_InternalProcedure(), this.getInternalProcedure(), null, "internalProcedure", null, 0, 1, DataRecovering.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataRecovering_DataFile(), this.getDataFile(), null, "dataFile", null, 0, 1, DataRecovering.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataRecovering_DataField(), this.getDataField(), null, "dataField", null, 0, 1, DataRecovering.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ODSystems(), this.getODSystemsType(), null, "oDSystems", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_PropertySet(), this.getPropertySetType(), null, "propertySet", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(enumerationTypeEClass, EnumerationType.class, "EnumerationType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEnumerationType_Item(), theXMLTypePackage.getString(), "item", null, 1, -1, EnumerationType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(integerRangeEClass, IntegerRange.class, "IntegerRange", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntegerRange_IntegerUpperBound(), theXMLTypePackage.getInt(), "integerUpperBound", null, 0, 1, IntegerRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIntegerRange_IntegerLowerBound(), theXMLTypePackage.getInt(), "integerLowerBound", null, 0, 1, IntegerRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIntegerRange_ConstantUpperBound(), this.getPropertyConstant(), null, "constantUpperBound", null, 0, 1, IntegerRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIntegerRange_ConstantLowerBound(), this.getPropertyConstant(), null, "constantLowerBound", null, 0, 1, IntegerRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(internalProcedureEClass, InternalProcedure.class, "InternalProcedure", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInternalProcedure_Name(), this.getProcedure(), "name", null, 1, 1, InternalProcedure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInternalProcedure_Subject(), this.getSubjectType1(), "subject", "context", 0, 1, InternalProcedure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(labelEClass, Label.class, "Label", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLabel_Value(), theXMLTypePackage.getString(), "value", null, 0, 1, Label.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLabel_Format(), theXMLTypePackage.getString(), "format", null, 0, 1, Label.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLabel_Procedure(), this.getProcedure(), "procedure", null, 0, 1, Label.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLabel_Subject(), this.getSubjectType(), "subject", "context", 0, 1, Label.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(loopConditionEClass, LoopCondition.class, "LoopCondition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLoopCondition_ODSection(), this.getODSection(), null, "oDSection", null, 1, -1, LoopCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLoopCondition_Mode(), this.getLoopMode(), "mode", "flat", 0, 1, LoopCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLoopCondition_Procedure(), this.getProcedure(), "procedure", null, 1, 1, LoopCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(notEClass, org.topcased.adele.odsConfig.OdsConfig.NOT.class, "NOT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNOT_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.NOT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNOT_Condition(), this.getCondition(), null, "condition", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.NOT.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getNOT_ConditionGroup(), ecorePackage.getEFeatureMapEntry(), "conditionGroup", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.NOT.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getNOT_AND(), this.getAND(), null, "aND", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.NOT.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getNOT_OR(), this.getOR(), null, "oR", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.NOT.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getNOT_NOT(), this.getNOT(), null, "nOT", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.NOT.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(numberTypeEClass, NumberType.class, "NumberType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNumberType_IntegerRange(), this.getIntegerRange(), null, "integerRange", null, 0, 1, NumberType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNumberType_RealRange(), this.getRealRange(), null, "realRange", null, 0, 1, NumberType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNumberType_Type(), this.getNumericType(), "type", null, 1, 1, NumberType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNumberType_Units(), this.getUnit(), null, "units", null, 0, -1, NumberType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNumberType_Unit(), this.getPropertyType(), null, "unit", null, 0, 1, NumberType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(objectConditionEClass, ObjectCondition.class, "ObjectCondition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getObjectCondition_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, ObjectCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObjectCondition_Condition(), this.getCondition(), null, "condition", null, 0, 1, ObjectCondition.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getObjectCondition_ConditionGroup(), ecorePackage.getEFeatureMapEntry(), "conditionGroup", null, 0, -1, ObjectCondition.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getObjectCondition_AND(), this.getAND(), null, "aND", null, 0, -1, ObjectCondition.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getObjectCondition_OR(), this.getOR(), null, "oR", null, 0, -1, ObjectCondition.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getObjectCondition_NOT(), this.getNOT(), null, "nOT", null, 0, -1, ObjectCondition.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(odSectionEClass, ODSection.class, "ODSection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getODSection_Label(), this.getLabel(), null, "label", null, 0, 1, ODSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getODSection_ObjectCondition(), this.getObjectCondition(), null, "objectCondition", null, 0, 1, ODSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getODSection_DataRecovering(), this.getDataRecovering(), null, "dataRecovering", null, 0, 1, ODSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getODSection_ODSection(), this.getODSection(), null, "oDSection", null, 0, -1, ODSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getODSection_Id(), theXMLTypePackage.getString(), "id", null, 1, 1, ODSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getODSection_Type(), this.getPropertyDefinition(), null, "type", null, 0, 1, ODSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(odSystemEClass, ODSystem.class, "ODSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getODSystem_ODSection(), this.getODSection(), null, "oDSection", null, 1, -1, ODSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getODSystem_Id(), theXMLTypePackage.getString(), "id", null, 1, 1, ODSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getODSystem_Label(), theXMLTypePackage.getString(), "label", null, 1, 1, ODSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getODSystem_Mode(), this.getMode(), "mode", "HIERARCHICAL", 0, 1, ODSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getODSystem_StyleSheet(), theXMLTypePackage.getString(), "styleSheet", null, 0, 1, ODSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getODSystem_DataRecovering(), this.getDataRecovering(), null, "dataRecovering", null, 0, 1, ODSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(odSystemsTypeEClass, ODSystemsType.class, "ODSystemsType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getODSystemsType_ODSystem(), this.getODSystem(), null, "oDSystem", null, 1, -1, ODSystemsType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getODSystemsType_Id(), theXMLTypePackage.getString(), "id", null, 1, 1, ODSystemsType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getODSystemsType_Label(), theXMLTypePackage.getString(), "label", null, 1, 1, ODSystemsType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getODSystemsType_Mode(), this.getMode(), "mode", "HIERARCHICAL", 0, 1, ODSystemsType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(orEClass, org.topcased.adele.odsConfig.OdsConfig.OR.class, "OR", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOR_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.OR.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOR_Condition(), this.getCondition(), null, "condition", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.OR.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getOR_ConditionGroup(), ecorePackage.getEFeatureMapEntry(), "conditionGroup", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.OR.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getOR_AND(), this.getAND(), null, "aND", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.OR.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getOR_OR(), this.getOR(), null, "oR", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.OR.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getOR_NOT(), this.getNOT(), null, "nOT", null, 0, -1, org.topcased.adele.odsConfig.OdsConfig.OR.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(primitivTypesTypeEClass, PrimitivTypesType.class, "PrimitivTypesType", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(propertyConstantEClass, PropertyConstant.class, "PropertyConstant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPropertyConstant_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, PropertyConstant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPropertyConstant_Values(), ecorePackage.getEString(), "values", null, 1, 1, PropertyConstant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyConstant_Type(), this.getType(), null, "type", null, 1, 1, PropertyConstant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertyDefinitionEClass, PropertyDefinition.class, "PropertyDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPropertyDefinition_Default(), ecorePackage.getEString(), "default", null, 0, -1, PropertyDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPropertyDefinition_IsMultiValued(), theXMLTypePackage.getBoolean(), "isMultiValued", "false", 0, 1, PropertyDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPropertyDefinition_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, PropertyDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyDefinition_PropertyCondition(), this.getObjectCondition(), null, "propertyCondition", null, 1, 1, PropertyDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyDefinition_Type(), this.getType(), null, "type", null, 1, 1, PropertyDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertySetTypeEClass, PropertySetType.class, "PropertySetType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPropertySetType_PropertyType(), this.getPropertyType(), null, "propertyType", null, 0, -1, PropertySetType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertySetType_PropertyDefinition(), this.getPropertyDefinition(), null, "propertyDefinition", null, 0, -1, PropertySetType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertySetType_PropertyConstant(), this.getPropertyConstant(), null, "propertyConstant", null, 0, -1, PropertySetType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPropertySetType_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, PropertySetType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertyTypeEClass, PropertyType.class, "PropertyType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPropertyType_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, PropertyType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyType_RecordType(), this.getRecordType(), null, "recordType", null, 0, 1, PropertyType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyType_ClassifierType(), this.getClassifierType(), null, "classifierType", null, 0, 1, PropertyType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyType_ReferenceType(), this.getReferenceType(), null, "referenceType", null, 0, 1, PropertyType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyType_EnumerationType(), this.getEnumerationType(), null, "enumerationType", null, 0, 1, PropertyType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyType_AadlBoolean(), this.getAadlBoolean(), null, "aadlBoolean", null, 0, 1, PropertyType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyType_AadlString(), this.getAadlString(), null, "aadlString", null, 0, 1, PropertyType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyType_UnitsType(), this.getUnitsType(), null, "unitsType", null, 0, 1, PropertyType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyType_RangeType(), this.getRangeType(), null, "rangeType", null, 0, 1, PropertyType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyType_NumberType(), this.getNumberType(), null, "numberType", null, 0, 1, PropertyType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rangeTypeEClass, RangeType.class, "RangeType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRangeType_NumberType(), this.getNumberType(), null, "numberType", null, 1, 1, RangeType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(realRangeEClass, RealRange.class, "RealRange", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRealRange_RealUpperBound(), theXMLTypePackage.getFloat(), "realUpperBound", null, 0, 1, RealRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRealRange_RealLowerBound(), theXMLTypePackage.getFloat(), "realLowerBound", null, 0, 1, RealRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRealRange_ConstantLowerBound(), this.getPropertyConstant(), null, "constantLowerBound", null, 0, 1, RealRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRealRange_ConstantUpperBound(), this.getPropertyConstant(), null, "constantUpperBound", null, 0, 1, RealRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(recordFieldTypeEClass, RecordFieldType.class, "RecordFieldType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRecordFieldType_Type(), this.getType(), null, "type", null, 1, 1, RecordFieldType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRecordFieldType_Name(), ecorePackage.getEString(), "name", null, 1, 1, RecordFieldType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(recordTypeEClass, RecordType.class, "RecordType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRecordType_RecordField(), this.getRecordFieldType(), null, "recordField", null, 1, -1, RecordType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referenceTypeEClass, ReferenceType.class, "ReferenceType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(slashEClass, Slash.class, "Slash", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(textEClass, Text.class, "Text", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getText_Value(), theXMLTypePackage.getString(), "value", null, 0, 1, Text.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unitEClass, Unit.class, "Unit", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUnit_Unit(), this.getUnit(), null, "unit", null, 0, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUnit_Multiplier(), theXMLTypePackage.getString(), "multiplier", null, 0, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUnit_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unitsTypeEClass, UnitsType.class, "UnitsType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUnitsType_Unit(), this.getUnit(), null, "unit", null, 1, -1, UnitsType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(typeEClass, Type.class, "Type", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(conditionValueEEnum, ConditionValue.class, "ConditionValue");
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_ABSTRACT);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_ADELE_COMPONENT);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_BUS);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_CLASSIFIER);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_IMPLEMENTATION);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_DATA);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_DEVICE);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_MEMORY);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_PACKAGE);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_PROCESS);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_PROCESSOR);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_PROTOTYPE);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_INSTANCE);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_SUBPROGRAM);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_SYSTEM);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_THREAD);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_THREAD_GROUP);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_VIRTUAL_BUS);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_VIRTUAL_PROCESSOR);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_ABSTRACT_FEATURE);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_BUS_ACCESS);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_DATA_ACCESS);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_DATA_PORT);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_EVENT_DATA_PORT);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_FEATURE);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_FEATURE_GROUP);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_PARAMETER);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_PORT);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_PROVIDES_SUBPROGRAM_ACCESS);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_SUBPROGRAM_ACCESS);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_CONNECTION);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_EVENT_PORT);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_FLOW);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_MODE_TRANSITION);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_PORT_CONNECTION);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.EENUM_LITERAL0);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_BA_VARIABLE);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_BA_STATE);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_BA_TRANSITION);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_TYPE);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_SUBCOMPONENT_REFINEMENT);
		addEEnumLiteral(conditionValueEEnum, ConditionValue.IS_REFINEMENT);

		initEEnum(loopModeEEnum, LoopMode.class, "LoopMode");
		addEEnumLiteral(loopModeEEnum, LoopMode.DEEP);
		addEEnumLiteral(loopModeEEnum, LoopMode.FLAT);

		initEEnum(modeEEnum, Mode.class, "Mode");
		addEEnumLiteral(modeEEnum, Mode.HIERARCHICAL);
		addEEnumLiteral(modeEEnum, Mode.SEQUENTIAL);

		initEEnum(numericTypeEEnum, NumericType.class, "NumericType");
		addEEnumLiteral(numericTypeEEnum, NumericType.AADL_INTEGER);
		addEEnumLiteral(numericTypeEEnum, NumericType.AADL_REAL);

		initEEnum(procedureEEnum, Procedure.class, "Procedure");
		addEEnumLiteral(procedureEEnum, Procedure.GET_CURRENT_PROJECT_DIRECTORY);
		addEEnumLiteral(procedureEEnum, Procedure.GET_CURRENT_WORKSPACE_DIRECTORY);
		addEEnumLiteral(procedureEEnum, Procedure.GET_CURRENT_DESIGN_DIRECTORY);
		addEEnumLiteral(procedureEEnum, Procedure.GET_CURRENT_OBJECT_DIRECTORY);
		addEEnumLiteral(procedureEEnum, Procedure.GET_CURRENT_OBJECT_LOCATION);
		addEEnumLiteral(procedureEEnum, Procedure.GET_CURRENT_PROJECT_NAME);
		addEEnumLiteral(procedureEEnum, Procedure.GET_CURRENT_DESIGN_NAME);
		addEEnumLiteral(procedureEEnum, Procedure.GET_CURRENT_OBJECT_NAME);
		addEEnumLiteral(procedureEEnum, Procedure.GET_CURRENT_OBJECT_ID);

		initEEnum(subjectTypeEEnum, SubjectType.class, "SubjectType");
		addEEnumLiteral(subjectTypeEEnum, SubjectType.CONTEXT);
		addEEnumLiteral(subjectTypeEEnum, SubjectType.CURRENT_OBJECT);

		initEEnum(subjectType1EEnum, SubjectType1.class, "SubjectType1");
		addEEnumLiteral(subjectType1EEnum, SubjectType1.CONTEXT);
		addEEnumLiteral(subjectType1EEnum, SubjectType1.CURRENT_OBJECT);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";		
		addAnnotation
		  (aadlBooleanEClass, 
		   source, 
		   new String[] {
			 "name", "AadlBoolean",
			 "kind", "empty"
		   });		
		addAnnotation
		  (aadlStringEClass, 
		   source, 
		   new String[] {
			 "name", "AadlString",
			 "kind", "empty"
		   });		
		addAnnotation
		  (andEClass, 
		   source, 
		   new String[] {
			 "name", "AND",
			 "kind", "mixed"
		   });		
		addAnnotation
		  (getAND_Mixed(), 
		   source, 
		   new String[] {
			 "kind", "elementWildcard",
			 "name", ":mixed"
		   });		
		addAnnotation
		  (getAND_Condition(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Condition",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getAND_ConditionGroup(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "ConditionGroup:2"
		   });		
		addAnnotation
		  (getAND_AND(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "AND",
			 "namespace", "##targetNamespace",
			 "group", "ConditionGroup:2"
		   });		
		addAnnotation
		  (getAND_OR(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "OR",
			 "namespace", "##targetNamespace",
			 "group", "ConditionGroup:2"
		   });		
		addAnnotation
		  (getAND_NOT(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "NOT",
			 "namespace", "##targetNamespace",
			 "group", "ConditionGroup:2"
		   });		
		addAnnotation
		  (classifierTypeEClass, 
		   source, 
		   new String[] {
			 "name", "ClassifierType",
			 "kind", "empty"
		   });		
		addAnnotation
		  (conditionEClass, 
		   source, 
		   new String[] {
			 "name", "Condition",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getCondition_Value(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "value",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (conditionValueEEnum, 
		   source, 
		   new String[] {
			 "name", "ConditionValue"
		   });		
		addAnnotation
		  (dataFieldEClass, 
		   source, 
		   new String[] {
			 "name", "DataField",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getDataField_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "name",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (dataFileEClass, 
		   source, 
		   new String[] {
			 "name", "DataFile",
			 "kind", "mixed"
		   });		
		addAnnotation
		  (getDataFile_Mixed(), 
		   source, 
		   new String[] {
			 "kind", "elementWildcard",
			 "name", ":mixed"
		   });		
		addAnnotation
		  (getDataFile_DataFileGroup(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "DataFileGroup:1"
		   });		
		addAnnotation
		  (getDataFile_Slash(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Slash",
			 "namespace", "##targetNamespace",
			 "group", "DataFileGroup:1"
		   });		
		addAnnotation
		  (getDataFile_Text(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Text",
			 "namespace", "##targetNamespace",
			 "group", "DataFileGroup:1"
		   });		
		addAnnotation
		  (getDataFile_InternalProcedure(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "InternalProcedure",
			 "namespace", "##targetNamespace",
			 "group", "DataFileGroup:1"
		   });		
		addAnnotation
		  (dataRecoveringEClass, 
		   source, 
		   new String[] {
			 "name", "DataRecovering",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getDataRecovering_InternalProcedure(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "InternalProcedure",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDataRecovering_DataFile(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "DataFile",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDataRecovering_DataField(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "DataField",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (documentRootEClass, 
		   source, 
		   new String[] {
			 "name", "",
			 "kind", "mixed"
		   });		
		addAnnotation
		  (getDocumentRoot_Mixed(), 
		   source, 
		   new String[] {
			 "kind", "elementWildcard",
			 "name", ":mixed"
		   });		
		addAnnotation
		  (getDocumentRoot_XMLNSPrefixMap(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "xmlns:prefix"
		   });		
		addAnnotation
		  (getDocumentRoot_XSISchemaLocation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "xsi:schemaLocation"
		   });		
		addAnnotation
		  (getDocumentRoot_ODSystems(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ODSystems",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_PropertySet(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "PropertySet",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (enumerationTypeEClass, 
		   source, 
		   new String[] {
			 "name", "EnumerationType",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getEnumerationType_Item(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "item",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (integerRangeEClass, 
		   source, 
		   new String[] {
			 "name", "integerRange",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getIntegerRange_IntegerUpperBound(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "integerUpperBound",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getIntegerRange_IntegerLowerBound(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "integerLowerBound",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (internalProcedureEClass, 
		   source, 
		   new String[] {
			 "name", "InternalProcedure",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getInternalProcedure_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "name",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getInternalProcedure_Subject(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "subject",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (labelEClass, 
		   source, 
		   new String[] {
			 "name", "Label",
			 "kind", "simple"
		   });		
		addAnnotation
		  (getLabel_Value(), 
		   source, 
		   new String[] {
			 "name", ":0",
			 "kind", "simple"
		   });		
		addAnnotation
		  (getLabel_Format(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "format",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getLabel_Procedure(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "procedure",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getLabel_Subject(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "subject",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (loopConditionEClass, 
		   source, 
		   new String[] {
			 "name", "LoopCondition",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getLoopCondition_ODSection(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ODSection",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getLoopCondition_Mode(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "mode",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getLoopCondition_Procedure(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "procedure",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (loopModeEEnum, 
		   source, 
		   new String[] {
			 "name", "mode_._type"
		   });		
		addAnnotation
		  (modeEEnum, 
		   source, 
		   new String[] {
			 "name", "mode_._1_._type"
		   });		
		addAnnotation
		  (notEClass, 
		   source, 
		   new String[] {
			 "name", "NOT",
			 "kind", "mixed"
		   });		
		addAnnotation
		  (getNOT_Mixed(), 
		   source, 
		   new String[] {
			 "kind", "elementWildcard",
			 "name", ":mixed"
		   });		
		addAnnotation
		  (getNOT_Condition(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Condition",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getNOT_ConditionGroup(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "ConditionGroup:2"
		   });		
		addAnnotation
		  (getNOT_AND(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "AND",
			 "namespace", "##targetNamespace",
			 "group", "ConditionGroup:2"
		   });		
		addAnnotation
		  (getNOT_OR(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "OR",
			 "namespace", "##targetNamespace",
			 "group", "ConditionGroup:2"
		   });		
		addAnnotation
		  (getNOT_NOT(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "NOT",
			 "namespace", "##targetNamespace",
			 "group", "ConditionGroup:2"
		   });		
		addAnnotation
		  (numberTypeEClass, 
		   source, 
		   new String[] {
			 "name", "NumberType",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getNumberType_IntegerRange(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "integerRange",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getNumberType_RealRange(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "realRange",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getNumberType_Type(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "type",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (numericTypeEEnum, 
		   source, 
		   new String[] {
			 "name", "NumericType"
		   });		
		addAnnotation
		  (objectConditionEClass, 
		   source, 
		   new String[] {
			 "name", "ObjectCondition",
			 "kind", "mixed"
		   });		
		addAnnotation
		  (getObjectCondition_Mixed(), 
		   source, 
		   new String[] {
			 "kind", "elementWildcard",
			 "name", ":mixed"
		   });		
		addAnnotation
		  (getObjectCondition_Condition(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Condition",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getObjectCondition_ConditionGroup(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "ConditionGroup:2"
		   });		
		addAnnotation
		  (getObjectCondition_AND(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "AND",
			 "namespace", "##targetNamespace",
			 "group", "ConditionGroup:2"
		   });		
		addAnnotation
		  (getObjectCondition_OR(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "OR",
			 "namespace", "##targetNamespace",
			 "group", "ConditionGroup:2"
		   });		
		addAnnotation
		  (getObjectCondition_NOT(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "NOT",
			 "namespace", "##targetNamespace",
			 "group", "ConditionGroup:2"
		   });		
		addAnnotation
		  (odSectionEClass, 
		   source, 
		   new String[] {
			 "name", "ODSection",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getODSection_Label(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Label",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getODSection_ObjectCondition(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ObjectCondition",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getODSection_DataRecovering(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "DataRecovering",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getODSection_ODSection(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ODSection",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getODSection_Id(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (odSystemEClass, 
		   source, 
		   new String[] {
			 "name", "ODSystem",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getODSystem_ODSection(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ODSection",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getODSystem_Id(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getODSystem_Label(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "label",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getODSystem_Mode(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "mode",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getODSystem_StyleSheet(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "styleSheet",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (odSystemsTypeEClass, 
		   source, 
		   new String[] {
			 "name", "ODSystems_._type",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getODSystemsType_ODSystem(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ODSystem",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getODSystemsType_Id(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getODSystemsType_Label(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "label",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getODSystemsType_Mode(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "mode",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (orEClass, 
		   source, 
		   new String[] {
			 "name", "OR",
			 "kind", "mixed"
		   });		
		addAnnotation
		  (getOR_Mixed(), 
		   source, 
		   new String[] {
			 "kind", "elementWildcard",
			 "name", ":mixed"
		   });		
		addAnnotation
		  (getOR_Condition(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Condition",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getOR_ConditionGroup(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "ConditionGroup:2"
		   });		
		addAnnotation
		  (getOR_AND(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "AND",
			 "namespace", "##targetNamespace",
			 "group", "ConditionGroup:2"
		   });		
		addAnnotation
		  (getOR_OR(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "OR",
			 "namespace", "##targetNamespace",
			 "group", "ConditionGroup:2"
		   });		
		addAnnotation
		  (getOR_NOT(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "NOT",
			 "namespace", "##targetNamespace",
			 "group", "ConditionGroup:2"
		   });		
		addAnnotation
		  (primitivTypesTypeEClass, 
		   source, 
		   new String[] {
			 "name", "PrimitivTypes_._type",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (procedureEEnum, 
		   source, 
		   new String[] {
			 "name", "Procedure"
		   });		
		addAnnotation
		  (propertyConstantEClass, 
		   source, 
		   new String[] {
			 "name", "PropertyConstant",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getPropertyConstant_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "name",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getPropertyConstant_Values(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "values",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (propertyDefinitionEClass, 
		   source, 
		   new String[] {
			 "name", "PropertyDefinition",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getPropertyDefinition_Default(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "default",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getPropertyDefinition_IsMultiValued(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "isMultiValued",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getPropertyDefinition_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "name",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (propertySetTypeEClass, 
		   source, 
		   new String[] {
			 "name", "PropertySet_._type",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getPropertySetType_PropertyType(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "PropertyType",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getPropertySetType_PropertyDefinition(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "PropertyDefinition",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getPropertySetType_PropertyConstant(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "PropertyConstant",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getPropertySetType_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "name",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (propertyTypeEClass, 
		   source, 
		   new String[] {
			 "name", "PropertyType",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getPropertyType_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "name",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (rangeTypeEClass, 
		   source, 
		   new String[] {
			 "name", "RangeType",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getRangeType_NumberType(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "NumberType",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (realRangeEClass, 
		   source, 
		   new String[] {
			 "name", "realRange",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getRealRange_RealUpperBound(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "realUpperBound",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getRealRange_RealLowerBound(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "realLowerBound",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (recordFieldTypeEClass, 
		   source, 
		   new String[] {
			 "name", "RecordField_._type",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (recordTypeEClass, 
		   source, 
		   new String[] {
			 "name", "RecordType",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getRecordType_RecordField(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "RecordField",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (referenceTypeEClass, 
		   source, 
		   new String[] {
			 "name", "ReferenceType",
			 "kind", "empty"
		   });		
		addAnnotation
		  (slashEClass, 
		   source, 
		   new String[] {
			 "name", "Slash",
			 "kind", "empty"
		   });		
		addAnnotation
		  (subjectTypeEEnum, 
		   source, 
		   new String[] {
			 "name", "subject_._type"
		   });		
		addAnnotation
		  (subjectType1EEnum, 
		   source, 
		   new String[] {
			 "name", "subject_._1_._type"
		   });		
		addAnnotation
		  (textEClass, 
		   source, 
		   new String[] {
			 "name", "Text",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getText_Value(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "value",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (unitEClass, 
		   source, 
		   new String[] {
			 "name", "Unit",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getUnit_Unit(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "unit",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getUnit_Multiplier(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "multiplier",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getUnit_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "name",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (unitsTypeEClass, 
		   source, 
		   new String[] {
			 "name", "UnitsType",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getUnitsType_Unit(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "unit",
			 "namespace", "##targetNamespace"
		   });
	}

} //OdsConfigPackageImpl
