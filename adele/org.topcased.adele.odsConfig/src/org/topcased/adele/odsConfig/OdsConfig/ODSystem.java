/**
 * <copyright>
 * </copyright>
 *
 * $Id: ODSystem.java,v 1.2 2011-04-19 14:13:30 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OD System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getODSection <em>OD Section</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getId <em>Id</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getLabel <em>Label</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getMode <em>Mode</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getStyleSheet <em>Style Sheet</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getDataRecovering <em>Data Recovering</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSystem()
 * @model extendedMetaData="name='ODSystem' kind='elementOnly'"
 * @generated
 */
public interface ODSystem extends EObject {
	/**
	 * Returns the value of the '<em><b>OD Section</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.adele.odsConfig.OdsConfig.ODSection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>OD Section</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OD Section</em>' containment reference list.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSystem_ODSection()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ODSection' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODSection> getODSection();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSystem_Id()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='id' namespace='##targetNamespace'"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSystem_Label()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='label' namespace='##targetNamespace'"
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Mode</b></em>' attribute.
	 * The default value is <code>"HIERARCHICAL"</code>.
	 * The literals are from the enumeration {@link org.topcased.adele.odsConfig.OdsConfig.Mode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mode</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Mode
	 * @see #isSetMode()
	 * @see #unsetMode()
	 * @see #setMode(Mode)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSystem_Mode()
	 * @model default="HIERARCHICAL" unsettable="true"
	 *        extendedMetaData="kind='attribute' name='mode' namespace='##targetNamespace'"
	 * @generated
	 */
	Mode getMode();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getMode <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mode</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Mode
	 * @see #isSetMode()
	 * @see #unsetMode()
	 * @see #getMode()
	 * @generated
	 */
	void setMode(Mode value);

	/**
	 * Unsets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getMode <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMode()
	 * @see #getMode()
	 * @see #setMode(Mode)
	 * @generated
	 */
	void unsetMode();

	/**
	 * Returns whether the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getMode <em>Mode</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Mode</em>' attribute is set.
	 * @see #unsetMode()
	 * @see #getMode()
	 * @see #setMode(Mode)
	 * @generated
	 */
	boolean isSetMode();

	/**
	 * Returns the value of the '<em><b>Style Sheet</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Style Sheet</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Style Sheet</em>' attribute.
	 * @see #setStyleSheet(String)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSystem_StyleSheet()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='styleSheet' namespace='##targetNamespace'"
	 * @generated
	 */
	String getStyleSheet();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getStyleSheet <em>Style Sheet</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Style Sheet</em>' attribute.
	 * @see #getStyleSheet()
	 * @generated
	 */
	void setStyleSheet(String value);

	/**
	 * Returns the value of the '<em><b>Data Recovering</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Recovering</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Recovering</em>' containment reference.
	 * @see #setDataRecovering(DataRecovering)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSystem_DataRecovering()
	 * @model containment="true"
	 * @generated
	 */
	DataRecovering getDataRecovering();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSystem#getDataRecovering <em>Data Recovering</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Recovering</em>' containment reference.
	 * @see #getDataRecovering()
	 * @generated
	 */
	void setDataRecovering(DataRecovering value);

} // ODSystem
