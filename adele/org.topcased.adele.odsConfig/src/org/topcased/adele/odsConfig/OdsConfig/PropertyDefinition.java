/**
 * <copyright>
 * </copyright>
 *
 * $Id: PropertyDefinition.java,v 1.2 2011-04-19 14:13:30 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#getDefault <em>Default</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#isIsMultiValued <em>Is Multi Valued</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#getName <em>Name</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#getPropertyCondition <em>Property Condition</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyDefinition()
 * @model extendedMetaData="name='PropertyDefinition' kind='elementOnly'"
 * @generated
 */
public interface PropertyDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>Default</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default</em>' attribute list.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyDefinition_Default()
	 * @model extendedMetaData="kind='attribute' name='default' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<String> getDefault();

	/**
	 * Returns the value of the '<em><b>Is Multi Valued</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Multi Valued</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Multi Valued</em>' attribute.
	 * @see #isSetIsMultiValued()
	 * @see #unsetIsMultiValued()
	 * @see #setIsMultiValued(boolean)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyDefinition_IsMultiValued()
	 * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='isMultiValued' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isIsMultiValued();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#isIsMultiValued <em>Is Multi Valued</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Multi Valued</em>' attribute.
	 * @see #isSetIsMultiValued()
	 * @see #unsetIsMultiValued()
	 * @see #isIsMultiValued()
	 * @generated
	 */
	void setIsMultiValued(boolean value);

	/**
	 * Unsets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#isIsMultiValued <em>Is Multi Valued</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIsMultiValued()
	 * @see #isIsMultiValued()
	 * @see #setIsMultiValued(boolean)
	 * @generated
	 */
	void unsetIsMultiValued();

	/**
	 * Returns whether the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#isIsMultiValued <em>Is Multi Valued</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Is Multi Valued</em>' attribute is set.
	 * @see #unsetIsMultiValued()
	 * @see #isIsMultiValued()
	 * @see #setIsMultiValued(boolean)
	 * @generated
	 */
	boolean isSetIsMultiValued();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyDefinition_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='name' namespace='##targetNamespace'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Property Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Condition</em>' containment reference.
	 * @see #setPropertyCondition(ObjectCondition)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyDefinition_PropertyCondition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ObjectCondition getPropertyCondition();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#getPropertyCondition <em>Property Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Condition</em>' containment reference.
	 * @see #getPropertyCondition()
	 * @generated
	 */
	void setPropertyCondition(ObjectCondition value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Type)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getPropertyDefinition_Type()
	 * @model required="true"
	 * @generated
	 */
	Type getType();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Type value);

} // PropertyDefinition
