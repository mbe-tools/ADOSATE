/**
 * <copyright>
 * </copyright>
 *
 * $Id: LoopCondition.java,v 1.2 2011-04-19 14:13:30 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Loop Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.LoopCondition#getODSection <em>OD Section</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.LoopCondition#getMode <em>Mode</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.LoopCondition#getProcedure <em>Procedure</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getLoopCondition()
 * @model extendedMetaData="name='LoopCondition' kind='elementOnly'"
 * @generated
 */
public interface LoopCondition extends EObject {
	/**
	 * Returns the value of the '<em><b>OD Section</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.adele.odsConfig.OdsConfig.ODSection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>OD Section</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OD Section</em>' containment reference list.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getLoopCondition_ODSection()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ODSection' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODSection> getODSection();

	/**
	 * Returns the value of the '<em><b>Mode</b></em>' attribute.
	 * The default value is <code>"flat"</code>.
	 * The literals are from the enumeration {@link org.topcased.adele.odsConfig.OdsConfig.LoopMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mode</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.LoopMode
	 * @see #isSetMode()
	 * @see #unsetMode()
	 * @see #setMode(LoopMode)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getLoopCondition_Mode()
	 * @model default="flat" unsettable="true"
	 *        extendedMetaData="kind='attribute' name='mode' namespace='##targetNamespace'"
	 * @generated
	 */
	LoopMode getMode();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.LoopCondition#getMode <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mode</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.LoopMode
	 * @see #isSetMode()
	 * @see #unsetMode()
	 * @see #getMode()
	 * @generated
	 */
	void setMode(LoopMode value);

	/**
	 * Unsets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.LoopCondition#getMode <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMode()
	 * @see #getMode()
	 * @see #setMode(LoopMode)
	 * @generated
	 */
	void unsetMode();

	/**
	 * Returns whether the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.LoopCondition#getMode <em>Mode</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Mode</em>' attribute is set.
	 * @see #unsetMode()
	 * @see #getMode()
	 * @see #setMode(LoopMode)
	 * @generated
	 */
	boolean isSetMode();

	/**
	 * Returns the value of the '<em><b>Procedure</b></em>' attribute.
	 * The literals are from the enumeration {@link org.topcased.adele.odsConfig.OdsConfig.Procedure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Procedure
	 * @see #isSetProcedure()
	 * @see #unsetProcedure()
	 * @see #setProcedure(Procedure)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getLoopCondition_Procedure()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='procedure' namespace='##targetNamespace'"
	 * @generated
	 */
	Procedure getProcedure();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.LoopCondition#getProcedure <em>Procedure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure</em>' attribute.
	 * @see org.topcased.adele.odsConfig.OdsConfig.Procedure
	 * @see #isSetProcedure()
	 * @see #unsetProcedure()
	 * @see #getProcedure()
	 * @generated
	 */
	void setProcedure(Procedure value);

	/**
	 * Unsets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.LoopCondition#getProcedure <em>Procedure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetProcedure()
	 * @see #getProcedure()
	 * @see #setProcedure(Procedure)
	 * @generated
	 */
	void unsetProcedure();

	/**
	 * Returns whether the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.LoopCondition#getProcedure <em>Procedure</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Procedure</em>' attribute is set.
	 * @see #unsetProcedure()
	 * @see #getProcedure()
	 * @see #setProcedure(Procedure)
	 * @generated
	 */
	boolean isSetProcedure();

} // LoopCondition
