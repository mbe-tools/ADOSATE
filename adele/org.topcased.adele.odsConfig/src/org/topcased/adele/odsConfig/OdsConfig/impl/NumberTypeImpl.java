/**
 * <copyright>
 * </copyright>
 *
 * $Id: NumberTypeImpl.java,v 1.2 2011-04-19 14:09:33 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.topcased.adele.odsConfig.OdsConfig.IntegerRange;
import org.topcased.adele.odsConfig.OdsConfig.NumberType;
import org.topcased.adele.odsConfig.OdsConfig.NumericType;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;
import org.topcased.adele.odsConfig.OdsConfig.PropertyType;
import org.topcased.adele.odsConfig.OdsConfig.RealRange;
import org.topcased.adele.odsConfig.OdsConfig.Unit;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Number Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.NumberTypeImpl#getIntegerRange <em>Integer Range</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.NumberTypeImpl#getRealRange <em>Real Range</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.NumberTypeImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.NumberTypeImpl#getUnits <em>Units</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.NumberTypeImpl#getUnit <em>Unit</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NumberTypeImpl extends PrimitivTypesTypeImpl implements NumberType {
	/**
	 * The cached value of the '{@link #getIntegerRange() <em>Integer Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerRange()
	 * @generated
	 * @ordered
	 */
	protected IntegerRange integerRange;

	/**
	 * The cached value of the '{@link #getRealRange() <em>Real Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRealRange()
	 * @generated
	 * @ordered
	 */
	protected RealRange realRange;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final NumericType TYPE_EDEFAULT = NumericType.AADL_INTEGER;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected NumericType type = TYPE_EDEFAULT;

	/**
	 * This is true if the Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean typeESet;

	/**
	 * The cached value of the '{@link #getUnits() <em>Units</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnits()
	 * @generated
	 * @ordered
	 */
	protected EList<Unit> units;

	/**
	 * The cached value of the '{@link #getUnit() <em>Unit</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnit()
	 * @generated
	 * @ordered
	 */
	protected PropertyType unit;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NumberTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdsConfigPackage.Literals.NUMBER_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerRange getIntegerRange() {
		return integerRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIntegerRange(IntegerRange newIntegerRange, NotificationChain msgs) {
		IntegerRange oldIntegerRange = integerRange;
		integerRange = newIntegerRange;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.NUMBER_TYPE__INTEGER_RANGE, oldIntegerRange, newIntegerRange);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegerRange(IntegerRange newIntegerRange) {
		if (newIntegerRange != integerRange) {
			NotificationChain msgs = null;
			if (integerRange != null)
				msgs = ((InternalEObject)integerRange).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.NUMBER_TYPE__INTEGER_RANGE, null, msgs);
			if (newIntegerRange != null)
				msgs = ((InternalEObject)newIntegerRange).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.NUMBER_TYPE__INTEGER_RANGE, null, msgs);
			msgs = basicSetIntegerRange(newIntegerRange, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.NUMBER_TYPE__INTEGER_RANGE, newIntegerRange, newIntegerRange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RealRange getRealRange() {
		return realRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRealRange(RealRange newRealRange, NotificationChain msgs) {
		RealRange oldRealRange = realRange;
		realRange = newRealRange;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.NUMBER_TYPE__REAL_RANGE, oldRealRange, newRealRange);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRealRange(RealRange newRealRange) {
		if (newRealRange != realRange) {
			NotificationChain msgs = null;
			if (realRange != null)
				msgs = ((InternalEObject)realRange).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.NUMBER_TYPE__REAL_RANGE, null, msgs);
			if (newRealRange != null)
				msgs = ((InternalEObject)newRealRange).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.NUMBER_TYPE__REAL_RANGE, null, msgs);
			msgs = basicSetRealRange(newRealRange, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.NUMBER_TYPE__REAL_RANGE, newRealRange, newRealRange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NumericType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(NumericType newType) {
		NumericType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		boolean oldTypeESet = typeESet;
		typeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.NUMBER_TYPE__TYPE, oldType, type, !oldTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetType() {
		NumericType oldType = type;
		boolean oldTypeESet = typeESet;
		type = TYPE_EDEFAULT;
		typeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdsConfigPackage.NUMBER_TYPE__TYPE, oldType, TYPE_EDEFAULT, oldTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetType() {
		return typeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Unit> getUnits() {
		if (units == null) {
			units = new EObjectContainmentEList<Unit>(Unit.class, this, OdsConfigPackage.NUMBER_TYPE__UNITS);
		}
		return units;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyType getUnit() {
		if (unit != null && unit.eIsProxy()) {
			InternalEObject oldUnit = (InternalEObject)unit;
			unit = (PropertyType)eResolveProxy(oldUnit);
			if (unit != oldUnit) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OdsConfigPackage.NUMBER_TYPE__UNIT, oldUnit, unit));
			}
		}
		return unit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyType basicGetUnit() {
		return unit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnit(PropertyType newUnit) {
		PropertyType oldUnit = unit;
		unit = newUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.NUMBER_TYPE__UNIT, oldUnit, unit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdsConfigPackage.NUMBER_TYPE__INTEGER_RANGE:
				return basicSetIntegerRange(null, msgs);
			case OdsConfigPackage.NUMBER_TYPE__REAL_RANGE:
				return basicSetRealRange(null, msgs);
			case OdsConfigPackage.NUMBER_TYPE__UNITS:
				return ((InternalEList<?>)getUnits()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdsConfigPackage.NUMBER_TYPE__INTEGER_RANGE:
				return getIntegerRange();
			case OdsConfigPackage.NUMBER_TYPE__REAL_RANGE:
				return getRealRange();
			case OdsConfigPackage.NUMBER_TYPE__TYPE:
				return getType();
			case OdsConfigPackage.NUMBER_TYPE__UNITS:
				return getUnits();
			case OdsConfigPackage.NUMBER_TYPE__UNIT:
				if (resolve) return getUnit();
				return basicGetUnit();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdsConfigPackage.NUMBER_TYPE__INTEGER_RANGE:
				setIntegerRange((IntegerRange)newValue);
				return;
			case OdsConfigPackage.NUMBER_TYPE__REAL_RANGE:
				setRealRange((RealRange)newValue);
				return;
			case OdsConfigPackage.NUMBER_TYPE__TYPE:
				setType((NumericType)newValue);
				return;
			case OdsConfigPackage.NUMBER_TYPE__UNITS:
				getUnits().clear();
				getUnits().addAll((Collection<? extends Unit>)newValue);
				return;
			case OdsConfigPackage.NUMBER_TYPE__UNIT:
				setUnit((PropertyType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.NUMBER_TYPE__INTEGER_RANGE:
				setIntegerRange((IntegerRange)null);
				return;
			case OdsConfigPackage.NUMBER_TYPE__REAL_RANGE:
				setRealRange((RealRange)null);
				return;
			case OdsConfigPackage.NUMBER_TYPE__TYPE:
				unsetType();
				return;
			case OdsConfigPackage.NUMBER_TYPE__UNITS:
				getUnits().clear();
				return;
			case OdsConfigPackage.NUMBER_TYPE__UNIT:
				setUnit((PropertyType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.NUMBER_TYPE__INTEGER_RANGE:
				return integerRange != null;
			case OdsConfigPackage.NUMBER_TYPE__REAL_RANGE:
				return realRange != null;
			case OdsConfigPackage.NUMBER_TYPE__TYPE:
				return isSetType();
			case OdsConfigPackage.NUMBER_TYPE__UNITS:
				return units != null && !units.isEmpty();
			case OdsConfigPackage.NUMBER_TYPE__UNIT:
				return unit != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (type: ");
		if (typeESet) result.append(type); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //NumberTypeImpl
