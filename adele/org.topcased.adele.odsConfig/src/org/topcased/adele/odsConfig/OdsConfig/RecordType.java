/**
 * <copyright>
 * </copyright>
 *
 * $Id: RecordType.java,v 1.1 2010-10-27 09:04:25 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Record Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.RecordType#getRecordField <em>Record Field</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getRecordType()
 * @model extendedMetaData="name='RecordType' kind='elementOnly'"
 * @generated
 */
public interface RecordType extends PrimitivTypesType {
	/**
	 * Returns the value of the '<em><b>Record Field</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.adele.odsConfig.OdsConfig.RecordFieldType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Field</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Field</em>' containment reference list.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getRecordType_RecordField()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='RecordField' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<RecordFieldType> getRecordField();

} // RecordType
