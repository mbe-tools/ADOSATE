/**
 * <copyright>
 * </copyright>
 *
 * $Id: PropertySetTypeImpl.java,v 1.2 2011-04-19 14:09:33 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;
import org.topcased.adele.odsConfig.OdsConfig.PropertyConstant;
import org.topcased.adele.odsConfig.OdsConfig.PropertyDefinition;
import org.topcased.adele.odsConfig.OdsConfig.PropertySetType;
import org.topcased.adele.odsConfig.OdsConfig.PropertyType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Property Set Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertySetTypeImpl#getPropertyType <em>Property Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertySetTypeImpl#getPropertyDefinition <em>Property Definition</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertySetTypeImpl#getPropertyConstant <em>Property Constant</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertySetTypeImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PropertySetTypeImpl extends EObjectImpl implements PropertySetType {
	/**
	 * The cached value of the '{@link #getPropertyType() <em>Property Type</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyType()
	 * @generated
	 * @ordered
	 */
	protected EList<PropertyType> propertyType;

	/**
	 * The cached value of the '{@link #getPropertyDefinition() <em>Property Definition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyDefinition()
	 * @generated
	 * @ordered
	 */
	protected EList<PropertyDefinition> propertyDefinition;

	/**
	 * The cached value of the '{@link #getPropertyConstant() <em>Property Constant</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyConstant()
	 * @generated
	 * @ordered
	 */
	protected EList<PropertyConstant> propertyConstant;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PropertySetTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdsConfigPackage.Literals.PROPERTY_SET_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PropertyType> getPropertyType() {
		if (propertyType == null) {
			propertyType = new EObjectContainmentEList<PropertyType>(PropertyType.class, this, OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_TYPE);
		}
		return propertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PropertyDefinition> getPropertyDefinition() {
		if (propertyDefinition == null) {
			propertyDefinition = new EObjectContainmentEList<PropertyDefinition>(PropertyDefinition.class, this, OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_DEFINITION);
		}
		return propertyDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PropertyConstant> getPropertyConstant() {
		if (propertyConstant == null) {
			propertyConstant = new EObjectContainmentEList<PropertyConstant>(PropertyConstant.class, this, OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_CONSTANT);
		}
		return propertyConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_SET_TYPE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_TYPE:
				return ((InternalEList<?>)getPropertyType()).basicRemove(otherEnd, msgs);
			case OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_DEFINITION:
				return ((InternalEList<?>)getPropertyDefinition()).basicRemove(otherEnd, msgs);
			case OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_CONSTANT:
				return ((InternalEList<?>)getPropertyConstant()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_TYPE:
				return getPropertyType();
			case OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_DEFINITION:
				return getPropertyDefinition();
			case OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_CONSTANT:
				return getPropertyConstant();
			case OdsConfigPackage.PROPERTY_SET_TYPE__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_TYPE:
				getPropertyType().clear();
				getPropertyType().addAll((Collection<? extends PropertyType>)newValue);
				return;
			case OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_DEFINITION:
				getPropertyDefinition().clear();
				getPropertyDefinition().addAll((Collection<? extends PropertyDefinition>)newValue);
				return;
			case OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_CONSTANT:
				getPropertyConstant().clear();
				getPropertyConstant().addAll((Collection<? extends PropertyConstant>)newValue);
				return;
			case OdsConfigPackage.PROPERTY_SET_TYPE__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_TYPE:
				getPropertyType().clear();
				return;
			case OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_DEFINITION:
				getPropertyDefinition().clear();
				return;
			case OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_CONSTANT:
				getPropertyConstant().clear();
				return;
			case OdsConfigPackage.PROPERTY_SET_TYPE__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_TYPE:
				return propertyType != null && !propertyType.isEmpty();
			case OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_DEFINITION:
				return propertyDefinition != null && !propertyDefinition.isEmpty();
			case OdsConfigPackage.PROPERTY_SET_TYPE__PROPERTY_CONSTANT:
				return propertyConstant != null && !propertyConstant.isEmpty();
			case OdsConfigPackage.PROPERTY_SET_TYPE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //PropertySetTypeImpl
