/**
 * <copyright>
 * </copyright>
 *
 * $Id: SlashImpl.java,v 1.2 2011-04-19 14:09:35 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;
import org.topcased.adele.odsConfig.OdsConfig.Slash;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Slash</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class SlashImpl extends EObjectImpl implements Slash {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SlashImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdsConfigPackage.Literals.SLASH;
	}

} //SlashImpl
