/**
 * <copyright>
 * </copyright>
 *
 * $Id: ObjectCondition.java,v 1.2 2011-04-19 14:13:30 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getMixed <em>Mixed</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getCondition <em>Condition</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getConditionGroup <em>Condition Group</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getAND <em>AND</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getOR <em>OR</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getNOT <em>NOT</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getObjectCondition()
 * @model extendedMetaData="name='ObjectCondition' kind='mixed'"
 * @generated
 */
public interface ObjectCondition extends EObject {
	/**
	 * Returns the value of the '<em><b>Mixed</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mixed</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mixed</em>' attribute list.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getObjectCondition_Mixed()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' name=':mixed'"
	 * @generated
	 */
	FeatureMap getMixed();

	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #setCondition(Condition)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getObjectCondition_Condition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Condition' namespace='##targetNamespace'"
	 * @generated
	 */
	Condition getCondition();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ObjectCondition#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(Condition value);

	/**
	 * Returns the value of the '<em><b>Condition Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition Group</em>' attribute list.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getObjectCondition_ConditionGroup()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='group' name='ConditionGroup:2'"
	 * @generated
	 */
	FeatureMap getConditionGroup();

	/**
	 * Returns the value of the '<em><b>AND</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.adele.odsConfig.OdsConfig.AND}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AND</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AND</em>' containment reference list.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getObjectCondition_AND()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AND' namespace='##targetNamespace' group='ConditionGroup:2'"
	 * @generated
	 */
	EList<AND> getAND();

	/**
	 * Returns the value of the '<em><b>OR</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.adele.odsConfig.OdsConfig.OR}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>OR</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OR</em>' containment reference list.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getObjectCondition_OR()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='OR' namespace='##targetNamespace' group='ConditionGroup:2'"
	 * @generated
	 */
	EList<OR> getOR();

	/**
	 * Returns the value of the '<em><b>NOT</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.adele.odsConfig.OdsConfig.NOT}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>NOT</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NOT</em>' containment reference list.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getObjectCondition_NOT()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='NOT' namespace='##targetNamespace' group='ConditionGroup:2'"
	 * @generated
	 */
	EList<NOT> getNOT();

} // ObjectCondition
