/**
 * <copyright>
 * </copyright>
 *
 * $Id: PropertyTypeImpl.java,v 1.2 2011-04-19 14:09:34 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.topcased.adele.odsConfig.OdsConfig.AadlBoolean;
import org.topcased.adele.odsConfig.OdsConfig.AadlString;
import org.topcased.adele.odsConfig.OdsConfig.ClassifierType;
import org.topcased.adele.odsConfig.OdsConfig.EnumerationType;
import org.topcased.adele.odsConfig.OdsConfig.NumberType;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;
import org.topcased.adele.odsConfig.OdsConfig.PropertyType;
import org.topcased.adele.odsConfig.OdsConfig.RangeType;
import org.topcased.adele.odsConfig.OdsConfig.RecordType;
import org.topcased.adele.odsConfig.OdsConfig.ReferenceType;
import org.topcased.adele.odsConfig.OdsConfig.UnitsType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Property Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyTypeImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyTypeImpl#getRecordType <em>Record Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyTypeImpl#getClassifierType <em>Classifier Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyTypeImpl#getReferenceType <em>Reference Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyTypeImpl#getEnumerationType <em>Enumeration Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyTypeImpl#getAadlBoolean <em>Aadl Boolean</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyTypeImpl#getAadlString <em>Aadl String</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyTypeImpl#getUnitsType <em>Units Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyTypeImpl#getRangeType <em>Range Type</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.PropertyTypeImpl#getNumberType <em>Number Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PropertyTypeImpl extends TypeImpl implements PropertyType {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRecordType() <em>Record Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecordType()
	 * @generated
	 * @ordered
	 */
	protected RecordType recordType;

	/**
	 * The cached value of the '{@link #getClassifierType() <em>Classifier Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifierType()
	 * @generated
	 * @ordered
	 */
	protected ClassifierType classifierType;

	/**
	 * The cached value of the '{@link #getReferenceType() <em>Reference Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceType()
	 * @generated
	 * @ordered
	 */
	protected ReferenceType referenceType;

	/**
	 * The cached value of the '{@link #getEnumerationType() <em>Enumeration Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnumerationType()
	 * @generated
	 * @ordered
	 */
	protected EnumerationType enumerationType;

	/**
	 * The cached value of the '{@link #getAadlBoolean() <em>Aadl Boolean</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAadlBoolean()
	 * @generated
	 * @ordered
	 */
	protected AadlBoolean aadlBoolean;

	/**
	 * The cached value of the '{@link #getAadlString() <em>Aadl String</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAadlString()
	 * @generated
	 * @ordered
	 */
	protected AadlString aadlString;

	/**
	 * The cached value of the '{@link #getUnitsType() <em>Units Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitsType()
	 * @generated
	 * @ordered
	 */
	protected UnitsType unitsType;

	/**
	 * The cached value of the '{@link #getRangeType() <em>Range Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRangeType()
	 * @generated
	 * @ordered
	 */
	protected RangeType rangeType;

	/**
	 * The cached value of the '{@link #getNumberType() <em>Number Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberType()
	 * @generated
	 * @ordered
	 */
	protected NumberType numberType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PropertyTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdsConfigPackage.Literals.PROPERTY_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordType getRecordType() {
		return recordType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRecordType(RecordType newRecordType, NotificationChain msgs) {
		RecordType oldRecordType = recordType;
		recordType = newRecordType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__RECORD_TYPE, oldRecordType, newRecordType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecordType(RecordType newRecordType) {
		if (newRecordType != recordType) {
			NotificationChain msgs = null;
			if (recordType != null)
				msgs = ((InternalEObject)recordType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__RECORD_TYPE, null, msgs);
			if (newRecordType != null)
				msgs = ((InternalEObject)newRecordType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__RECORD_TYPE, null, msgs);
			msgs = basicSetRecordType(newRecordType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__RECORD_TYPE, newRecordType, newRecordType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifierType getClassifierType() {
		return classifierType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClassifierType(ClassifierType newClassifierType, NotificationChain msgs) {
		ClassifierType oldClassifierType = classifierType;
		classifierType = newClassifierType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__CLASSIFIER_TYPE, oldClassifierType, newClassifierType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassifierType(ClassifierType newClassifierType) {
		if (newClassifierType != classifierType) {
			NotificationChain msgs = null;
			if (classifierType != null)
				msgs = ((InternalEObject)classifierType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__CLASSIFIER_TYPE, null, msgs);
			if (newClassifierType != null)
				msgs = ((InternalEObject)newClassifierType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__CLASSIFIER_TYPE, null, msgs);
			msgs = basicSetClassifierType(newClassifierType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__CLASSIFIER_TYPE, newClassifierType, newClassifierType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceType getReferenceType() {
		return referenceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReferenceType(ReferenceType newReferenceType, NotificationChain msgs) {
		ReferenceType oldReferenceType = referenceType;
		referenceType = newReferenceType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__REFERENCE_TYPE, oldReferenceType, newReferenceType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenceType(ReferenceType newReferenceType) {
		if (newReferenceType != referenceType) {
			NotificationChain msgs = null;
			if (referenceType != null)
				msgs = ((InternalEObject)referenceType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__REFERENCE_TYPE, null, msgs);
			if (newReferenceType != null)
				msgs = ((InternalEObject)newReferenceType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__REFERENCE_TYPE, null, msgs);
			msgs = basicSetReferenceType(newReferenceType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__REFERENCE_TYPE, newReferenceType, newReferenceType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationType getEnumerationType() {
		return enumerationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnumerationType(EnumerationType newEnumerationType, NotificationChain msgs) {
		EnumerationType oldEnumerationType = enumerationType;
		enumerationType = newEnumerationType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__ENUMERATION_TYPE, oldEnumerationType, newEnumerationType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnumerationType(EnumerationType newEnumerationType) {
		if (newEnumerationType != enumerationType) {
			NotificationChain msgs = null;
			if (enumerationType != null)
				msgs = ((InternalEObject)enumerationType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__ENUMERATION_TYPE, null, msgs);
			if (newEnumerationType != null)
				msgs = ((InternalEObject)newEnumerationType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__ENUMERATION_TYPE, null, msgs);
			msgs = basicSetEnumerationType(newEnumerationType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__ENUMERATION_TYPE, newEnumerationType, newEnumerationType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AadlBoolean getAadlBoolean() {
		return aadlBoolean;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAadlBoolean(AadlBoolean newAadlBoolean, NotificationChain msgs) {
		AadlBoolean oldAadlBoolean = aadlBoolean;
		aadlBoolean = newAadlBoolean;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__AADL_BOOLEAN, oldAadlBoolean, newAadlBoolean);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAadlBoolean(AadlBoolean newAadlBoolean) {
		if (newAadlBoolean != aadlBoolean) {
			NotificationChain msgs = null;
			if (aadlBoolean != null)
				msgs = ((InternalEObject)aadlBoolean).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__AADL_BOOLEAN, null, msgs);
			if (newAadlBoolean != null)
				msgs = ((InternalEObject)newAadlBoolean).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__AADL_BOOLEAN, null, msgs);
			msgs = basicSetAadlBoolean(newAadlBoolean, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__AADL_BOOLEAN, newAadlBoolean, newAadlBoolean));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AadlString getAadlString() {
		return aadlString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAadlString(AadlString newAadlString, NotificationChain msgs) {
		AadlString oldAadlString = aadlString;
		aadlString = newAadlString;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__AADL_STRING, oldAadlString, newAadlString);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAadlString(AadlString newAadlString) {
		if (newAadlString != aadlString) {
			NotificationChain msgs = null;
			if (aadlString != null)
				msgs = ((InternalEObject)aadlString).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__AADL_STRING, null, msgs);
			if (newAadlString != null)
				msgs = ((InternalEObject)newAadlString).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__AADL_STRING, null, msgs);
			msgs = basicSetAadlString(newAadlString, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__AADL_STRING, newAadlString, newAadlString));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnitsType getUnitsType() {
		return unitsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnitsType(UnitsType newUnitsType, NotificationChain msgs) {
		UnitsType oldUnitsType = unitsType;
		unitsType = newUnitsType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__UNITS_TYPE, oldUnitsType, newUnitsType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitsType(UnitsType newUnitsType) {
		if (newUnitsType != unitsType) {
			NotificationChain msgs = null;
			if (unitsType != null)
				msgs = ((InternalEObject)unitsType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__UNITS_TYPE, null, msgs);
			if (newUnitsType != null)
				msgs = ((InternalEObject)newUnitsType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__UNITS_TYPE, null, msgs);
			msgs = basicSetUnitsType(newUnitsType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__UNITS_TYPE, newUnitsType, newUnitsType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeType getRangeType() {
		return rangeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRangeType(RangeType newRangeType, NotificationChain msgs) {
		RangeType oldRangeType = rangeType;
		rangeType = newRangeType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__RANGE_TYPE, oldRangeType, newRangeType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRangeType(RangeType newRangeType) {
		if (newRangeType != rangeType) {
			NotificationChain msgs = null;
			if (rangeType != null)
				msgs = ((InternalEObject)rangeType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__RANGE_TYPE, null, msgs);
			if (newRangeType != null)
				msgs = ((InternalEObject)newRangeType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__RANGE_TYPE, null, msgs);
			msgs = basicSetRangeType(newRangeType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__RANGE_TYPE, newRangeType, newRangeType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NumberType getNumberType() {
		return numberType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNumberType(NumberType newNumberType, NotificationChain msgs) {
		NumberType oldNumberType = numberType;
		numberType = newNumberType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__NUMBER_TYPE, oldNumberType, newNumberType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumberType(NumberType newNumberType) {
		if (newNumberType != numberType) {
			NotificationChain msgs = null;
			if (numberType != null)
				msgs = ((InternalEObject)numberType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__NUMBER_TYPE, null, msgs);
			if (newNumberType != null)
				msgs = ((InternalEObject)newNumberType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.PROPERTY_TYPE__NUMBER_TYPE, null, msgs);
			msgs = basicSetNumberType(newNumberType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.PROPERTY_TYPE__NUMBER_TYPE, newNumberType, newNumberType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdsConfigPackage.PROPERTY_TYPE__RECORD_TYPE:
				return basicSetRecordType(null, msgs);
			case OdsConfigPackage.PROPERTY_TYPE__CLASSIFIER_TYPE:
				return basicSetClassifierType(null, msgs);
			case OdsConfigPackage.PROPERTY_TYPE__REFERENCE_TYPE:
				return basicSetReferenceType(null, msgs);
			case OdsConfigPackage.PROPERTY_TYPE__ENUMERATION_TYPE:
				return basicSetEnumerationType(null, msgs);
			case OdsConfigPackage.PROPERTY_TYPE__AADL_BOOLEAN:
				return basicSetAadlBoolean(null, msgs);
			case OdsConfigPackage.PROPERTY_TYPE__AADL_STRING:
				return basicSetAadlString(null, msgs);
			case OdsConfigPackage.PROPERTY_TYPE__UNITS_TYPE:
				return basicSetUnitsType(null, msgs);
			case OdsConfigPackage.PROPERTY_TYPE__RANGE_TYPE:
				return basicSetRangeType(null, msgs);
			case OdsConfigPackage.PROPERTY_TYPE__NUMBER_TYPE:
				return basicSetNumberType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdsConfigPackage.PROPERTY_TYPE__NAME:
				return getName();
			case OdsConfigPackage.PROPERTY_TYPE__RECORD_TYPE:
				return getRecordType();
			case OdsConfigPackage.PROPERTY_TYPE__CLASSIFIER_TYPE:
				return getClassifierType();
			case OdsConfigPackage.PROPERTY_TYPE__REFERENCE_TYPE:
				return getReferenceType();
			case OdsConfigPackage.PROPERTY_TYPE__ENUMERATION_TYPE:
				return getEnumerationType();
			case OdsConfigPackage.PROPERTY_TYPE__AADL_BOOLEAN:
				return getAadlBoolean();
			case OdsConfigPackage.PROPERTY_TYPE__AADL_STRING:
				return getAadlString();
			case OdsConfigPackage.PROPERTY_TYPE__UNITS_TYPE:
				return getUnitsType();
			case OdsConfigPackage.PROPERTY_TYPE__RANGE_TYPE:
				return getRangeType();
			case OdsConfigPackage.PROPERTY_TYPE__NUMBER_TYPE:
				return getNumberType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdsConfigPackage.PROPERTY_TYPE__NAME:
				setName((String)newValue);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__RECORD_TYPE:
				setRecordType((RecordType)newValue);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__CLASSIFIER_TYPE:
				setClassifierType((ClassifierType)newValue);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__REFERENCE_TYPE:
				setReferenceType((ReferenceType)newValue);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__ENUMERATION_TYPE:
				setEnumerationType((EnumerationType)newValue);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__AADL_BOOLEAN:
				setAadlBoolean((AadlBoolean)newValue);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__AADL_STRING:
				setAadlString((AadlString)newValue);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__UNITS_TYPE:
				setUnitsType((UnitsType)newValue);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__RANGE_TYPE:
				setRangeType((RangeType)newValue);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__NUMBER_TYPE:
				setNumberType((NumberType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.PROPERTY_TYPE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__RECORD_TYPE:
				setRecordType((RecordType)null);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__CLASSIFIER_TYPE:
				setClassifierType((ClassifierType)null);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__REFERENCE_TYPE:
				setReferenceType((ReferenceType)null);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__ENUMERATION_TYPE:
				setEnumerationType((EnumerationType)null);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__AADL_BOOLEAN:
				setAadlBoolean((AadlBoolean)null);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__AADL_STRING:
				setAadlString((AadlString)null);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__UNITS_TYPE:
				setUnitsType((UnitsType)null);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__RANGE_TYPE:
				setRangeType((RangeType)null);
				return;
			case OdsConfigPackage.PROPERTY_TYPE__NUMBER_TYPE:
				setNumberType((NumberType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.PROPERTY_TYPE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case OdsConfigPackage.PROPERTY_TYPE__RECORD_TYPE:
				return recordType != null;
			case OdsConfigPackage.PROPERTY_TYPE__CLASSIFIER_TYPE:
				return classifierType != null;
			case OdsConfigPackage.PROPERTY_TYPE__REFERENCE_TYPE:
				return referenceType != null;
			case OdsConfigPackage.PROPERTY_TYPE__ENUMERATION_TYPE:
				return enumerationType != null;
			case OdsConfigPackage.PROPERTY_TYPE__AADL_BOOLEAN:
				return aadlBoolean != null;
			case OdsConfigPackage.PROPERTY_TYPE__AADL_STRING:
				return aadlString != null;
			case OdsConfigPackage.PROPERTY_TYPE__UNITS_TYPE:
				return unitsType != null;
			case OdsConfigPackage.PROPERTY_TYPE__RANGE_TYPE:
				return rangeType != null;
			case OdsConfigPackage.PROPERTY_TYPE__NUMBER_TYPE:
				return numberType != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //PropertyTypeImpl
