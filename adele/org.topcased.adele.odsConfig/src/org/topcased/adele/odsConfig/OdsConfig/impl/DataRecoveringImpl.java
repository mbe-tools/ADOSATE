/**
 * <copyright>
 * </copyright>
 *
 * $Id: DataRecoveringImpl.java,v 1.2 2011-04-19 14:09:35 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.topcased.adele.odsConfig.OdsConfig.DataField;
import org.topcased.adele.odsConfig.OdsConfig.DataFile;
import org.topcased.adele.odsConfig.OdsConfig.DataRecovering;
import org.topcased.adele.odsConfig.OdsConfig.InternalProcedure;
import org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Recovering</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.DataRecoveringImpl#getInternalProcedure <em>Internal Procedure</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.DataRecoveringImpl#getDataFile <em>Data File</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.impl.DataRecoveringImpl#getDataField <em>Data Field</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DataRecoveringImpl extends EObjectImpl implements DataRecovering {
	/**
	 * The cached value of the '{@link #getInternalProcedure() <em>Internal Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalProcedure()
	 * @generated
	 * @ordered
	 */
	protected InternalProcedure internalProcedure;

	/**
	 * The cached value of the '{@link #getDataFile() <em>Data File</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataFile()
	 * @generated
	 * @ordered
	 */
	protected DataFile dataFile;

	/**
	 * The cached value of the '{@link #getDataField() <em>Data Field</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataField()
	 * @generated
	 * @ordered
	 */
	protected DataField dataField;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataRecoveringImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdsConfigPackage.Literals.DATA_RECOVERING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalProcedure getInternalProcedure() {
		return internalProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInternalProcedure(InternalProcedure newInternalProcedure, NotificationChain msgs) {
		InternalProcedure oldInternalProcedure = internalProcedure;
		internalProcedure = newInternalProcedure;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.DATA_RECOVERING__INTERNAL_PROCEDURE, oldInternalProcedure, newInternalProcedure);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInternalProcedure(InternalProcedure newInternalProcedure) {
		if (newInternalProcedure != internalProcedure) {
			NotificationChain msgs = null;
			if (internalProcedure != null)
				msgs = ((InternalEObject)internalProcedure).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.DATA_RECOVERING__INTERNAL_PROCEDURE, null, msgs);
			if (newInternalProcedure != null)
				msgs = ((InternalEObject)newInternalProcedure).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.DATA_RECOVERING__INTERNAL_PROCEDURE, null, msgs);
			msgs = basicSetInternalProcedure(newInternalProcedure, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.DATA_RECOVERING__INTERNAL_PROCEDURE, newInternalProcedure, newInternalProcedure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFile getDataFile() {
		return dataFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataFile(DataFile newDataFile, NotificationChain msgs) {
		DataFile oldDataFile = dataFile;
		dataFile = newDataFile;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.DATA_RECOVERING__DATA_FILE, oldDataFile, newDataFile);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataFile(DataFile newDataFile) {
		if (newDataFile != dataFile) {
			NotificationChain msgs = null;
			if (dataFile != null)
				msgs = ((InternalEObject)dataFile).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.DATA_RECOVERING__DATA_FILE, null, msgs);
			if (newDataFile != null)
				msgs = ((InternalEObject)newDataFile).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.DATA_RECOVERING__DATA_FILE, null, msgs);
			msgs = basicSetDataFile(newDataFile, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.DATA_RECOVERING__DATA_FILE, newDataFile, newDataFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataField getDataField() {
		return dataField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataField(DataField newDataField, NotificationChain msgs) {
		DataField oldDataField = dataField;
		dataField = newDataField;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdsConfigPackage.DATA_RECOVERING__DATA_FIELD, oldDataField, newDataField);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataField(DataField newDataField) {
		if (newDataField != dataField) {
			NotificationChain msgs = null;
			if (dataField != null)
				msgs = ((InternalEObject)dataField).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.DATA_RECOVERING__DATA_FIELD, null, msgs);
			if (newDataField != null)
				msgs = ((InternalEObject)newDataField).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdsConfigPackage.DATA_RECOVERING__DATA_FIELD, null, msgs);
			msgs = basicSetDataField(newDataField, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdsConfigPackage.DATA_RECOVERING__DATA_FIELD, newDataField, newDataField));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdsConfigPackage.DATA_RECOVERING__INTERNAL_PROCEDURE:
				return basicSetInternalProcedure(null, msgs);
			case OdsConfigPackage.DATA_RECOVERING__DATA_FILE:
				return basicSetDataFile(null, msgs);
			case OdsConfigPackage.DATA_RECOVERING__DATA_FIELD:
				return basicSetDataField(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdsConfigPackage.DATA_RECOVERING__INTERNAL_PROCEDURE:
				return getInternalProcedure();
			case OdsConfigPackage.DATA_RECOVERING__DATA_FILE:
				return getDataFile();
			case OdsConfigPackage.DATA_RECOVERING__DATA_FIELD:
				return getDataField();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdsConfigPackage.DATA_RECOVERING__INTERNAL_PROCEDURE:
				setInternalProcedure((InternalProcedure)newValue);
				return;
			case OdsConfigPackage.DATA_RECOVERING__DATA_FILE:
				setDataFile((DataFile)newValue);
				return;
			case OdsConfigPackage.DATA_RECOVERING__DATA_FIELD:
				setDataField((DataField)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.DATA_RECOVERING__INTERNAL_PROCEDURE:
				setInternalProcedure((InternalProcedure)null);
				return;
			case OdsConfigPackage.DATA_RECOVERING__DATA_FILE:
				setDataFile((DataFile)null);
				return;
			case OdsConfigPackage.DATA_RECOVERING__DATA_FIELD:
				setDataField((DataField)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdsConfigPackage.DATA_RECOVERING__INTERNAL_PROCEDURE:
				return internalProcedure != null;
			case OdsConfigPackage.DATA_RECOVERING__DATA_FILE:
				return dataFile != null;
			case OdsConfigPackage.DATA_RECOVERING__DATA_FIELD:
				return dataField != null;
		}
		return super.eIsSet(featureID);
	}

} //DataRecoveringImpl
