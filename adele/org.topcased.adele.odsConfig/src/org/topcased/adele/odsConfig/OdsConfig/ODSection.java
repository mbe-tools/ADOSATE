/**
 * <copyright>
 * </copyright>
 *
 * $Id: ODSection.java,v 1.2 2011-04-19 14:13:29 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OD Section</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getLabel <em>Label</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getObjectCondition <em>Object Condition</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getDataRecovering <em>Data Recovering</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getODSection <em>OD Section</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getId <em>Id</em>}</li>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSection()
 * @model extendedMetaData="name='ODSection' kind='elementOnly'"
 * @generated
 */
public interface ODSection extends EObject {
	/**
	 * Returns the value of the '<em><b>Label</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' containment reference.
	 * @see #setLabel(Label)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSection_Label()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Label' namespace='##targetNamespace'"
	 * @generated
	 */
	Label getLabel();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getLabel <em>Label</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' containment reference.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(Label value);

	/**
	 * Returns the value of the '<em><b>Object Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Condition</em>' containment reference.
	 * @see #setObjectCondition(ObjectCondition)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSection_ObjectCondition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ObjectCondition' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectCondition getObjectCondition();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getObjectCondition <em>Object Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Condition</em>' containment reference.
	 * @see #getObjectCondition()
	 * @generated
	 */
	void setObjectCondition(ObjectCondition value);

	/**
	 * Returns the value of the '<em><b>Data Recovering</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Recovering</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Recovering</em>' containment reference.
	 * @see #setDataRecovering(DataRecovering)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSection_DataRecovering()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DataRecovering' namespace='##targetNamespace'"
	 * @generated
	 */
	DataRecovering getDataRecovering();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getDataRecovering <em>Data Recovering</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Recovering</em>' containment reference.
	 * @see #getDataRecovering()
	 * @generated
	 */
	void setDataRecovering(DataRecovering value);

	/**
	 * Returns the value of the '<em><b>OD Section</b></em>' containment reference list.
	 * The list contents are of type {@link org.topcased.adele.odsConfig.OdsConfig.ODSection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>OD Section</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OD Section</em>' containment reference list.
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSection_ODSection()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ODSection' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODSection> getODSection();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSection_Id()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='id' namespace='##targetNamespace'"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(PropertyDefinition)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getODSection_Type()
	 * @model
	 * @generated
	 */
	PropertyDefinition getType();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.ODSection#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(PropertyDefinition value);

} // ODSection
