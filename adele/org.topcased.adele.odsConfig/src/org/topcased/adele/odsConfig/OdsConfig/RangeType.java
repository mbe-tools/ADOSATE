/**
 * <copyright>
 * </copyright>
 *
 * $Id: RangeType.java,v 1.1 2010-10-27 09:04:25 aschach Exp $
 */
package org.topcased.adele.odsConfig.OdsConfig;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Range Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.odsConfig.OdsConfig.RangeType#getNumberType <em>Number Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getRangeType()
 * @model extendedMetaData="name='RangeType' kind='elementOnly'"
 * @generated
 */
public interface RangeType extends PrimitivTypesType {
	/**
	 * Returns the value of the '<em><b>Number Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Type</em>' containment reference.
	 * @see #setNumberType(NumberType)
	 * @see org.topcased.adele.odsConfig.OdsConfig.OdsConfigPackage#getRangeType_NumberType()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='NumberType' namespace='##targetNamespace'"
	 * @generated
	 */
	NumberType getNumberType();

	/**
	 * Sets the value of the '{@link org.topcased.adele.odsConfig.OdsConfig.RangeType#getNumberType <em>Number Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Type</em>' containment reference.
	 * @see #getNumberType()
	 * @generated
	 */
	void setNumberType(NumberType value);

} // RangeType
