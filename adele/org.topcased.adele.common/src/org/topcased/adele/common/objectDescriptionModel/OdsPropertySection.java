package org.topcased.adele.common.objectDescriptionModel;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.topcased.adele.common.properties.AdelePropertyPanel;
import org.topcased.adele.common.properties.AdelePropertySection;

public class OdsPropertySection extends AdelePropertySection {
	
	@Override
	public AdelePropertyPanel createPropertyPanel(	final Composite p_parent,
													final IActionBars p_actionBars ) {
		final OdsView odsPanel = new OdsView( p_parent );
		
		return odsPanel;
	}
    
    protected boolean addControlListener() {
    	return false;
    }
}
