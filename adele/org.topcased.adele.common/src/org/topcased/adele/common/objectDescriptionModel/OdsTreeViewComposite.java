/**
 * 
 */
package org.topcased.adele.common.objectDescriptionModel;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.views.contentoutline.ContentOutline;
import org.topcased.adele.common.utils.ADELEModeler;
import org.topcased.adele.common.utils.edit.AdeleCommonDiagramEditPart;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.adele.model.KernelSpices.SKFeature;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.adele.model.KernelSpices.SKRelation;
import org.topcased.adele.model.ObjectDescriptionModel.SKODSection;
import org.topcased.modeler.edit.EMFGraphEdgeEditPart;
import org.topcased.modeler.edit.EMFGraphNodeEditPart;

/**
 * @author Arnaud
 *
 */
public class OdsTreeViewComposite extends Composite {

	protected TreeViewer tv;
	private SKHierarchicalObject currentSelection;
	public String treeSelectionId;
	protected TreeItem selection;
	protected String compositeId;
	//private boolean isOutlineReadOnly = false;
	
	/**
	 * @param parent
	 * @param style
	 */
	public OdsTreeViewComposite(Composite parent, int tabNumber, String odsystemName, int style) {
		super(parent, style);
		compositeId = odsystemName;
		setLayout(new GridLayout(1, false));
		
		// Create the tree viewer to display the file tree
	    tv = new TreeViewer(this);
	    tv.getTree().setLayoutData(new GridData(GridData.FILL_BOTH));
	    tv.setContentProvider(new OdsTreeViewContentProvider(odsystemName, tabNumber));
	    tv.setLabelProvider(new OdsTreeViewLabelProvider());
	}

	public void selectionChanged(IWorkbenchPart part, ISelection selection, boolean refresh) {
		if (selection instanceof IStructuredSelection){
			if (part instanceof ADELEModeler) {
				Object first = ((IStructuredSelection) selection).getFirstElement();
				((OdsTreeViewContentProvider)tv.getContentProvider()).setCurrentEditor((ADELEModeler) part);
				if (isSKObjectSelected(first)){
					tv.setInput(currentSelection);
					tv.expandAll();
					if (treeSelectionId != null) {
						setSelection(tv.getTree().getItems(), refresh);
					}
				}
			}
			else if (part instanceof ContentOutline) {
				Object first = ((IStructuredSelection) selection).getFirstElement();
				
//				if (!isOutlineReadOnly) {
					// DB Fixing class cast exception with Aadl2OutlinePage
//					final IContentOutlinePage outlinePage = (IContentOutlinePage) part.getAdapter( IContentOutlinePage.class );
					
//					if ( outlinePage instanceof DiagramsOutlinePage ) {
//						SashForm sash = (SashForm) ((DiagramsOutlinePage) outlinePage ).getControl();
//						if (sash!=null) {
//							((ModelNavigator)sash.getChildren()[1]).getTreeViewer().getTree().setMenu(null);
							
							// DB Enable load resource
							//isOutlineReadOnly = true;
//						}
//					}
//				}
				
				List<ADELEModeler> editors = new ArrayList<ADELEModeler>();
				IWorkbenchWindow[] iwwindows = PlatformUI.getWorkbench().getWorkbenchWindows();
				for (int i=0; i<iwwindows.length; i++){
					IWorkbenchPage[] iwpages = iwwindows[i].getPages();		
					for (int j=0; j<iwpages.length; j++){
						IEditorReference[] eds = iwpages[j].getEditorReferences();
						
						for ( final IEditorReference editroRef : eds ) {
							final IEditorPart editor = editroRef.getEditor( false );
							
							if ( editor instanceof ADELEModeler ) {
								editors.add( (ADELEModeler) editor );
							}
						}
					}
				}
				
				if (isSKObjectSelected(first) && !editors.isEmpty()){
						tv.setInput(currentSelection);
						tv.expandAll();
						if (treeSelectionId != null) {
							setSelection(tv.getTree().getItems(), refresh);
						}
				}
				
				if (editors.isEmpty())
					tv.setInput(null);
			}
			else
				tv.setInput(null);
		}
	}
	
	protected boolean isSKObjectSelected(Object selection) {

		if (selection instanceof SKComponent || selection instanceof SKFeature
				|| selection instanceof SKRelation) {
			currentSelection = (SKHierarchicalObject) selection;
			return true;
		} else if (selection instanceof EMFGraphNodeEditPart
				&& ((EMFGraphNodeEditPart) selection).getEObject() instanceof SKHierarchicalObject) {
			currentSelection = (SKHierarchicalObject) ((EMFGraphNodeEditPart) selection)
					.getEObject();
			return true;
		} else if (selection instanceof EMFGraphEdgeEditPart
				&& ((EMFGraphEdgeEditPart) selection).getEObject() instanceof SKHierarchicalObject) {
			currentSelection = (SKHierarchicalObject) ((EMFGraphEdgeEditPart) selection)
					.getEObject();
			return true;
		} else if (selection instanceof AdeleCommonDiagramEditPart
				&& ((AdeleCommonDiagramEditPart) selection).getEObject() instanceof SKHierarchicalObject) {
			currentSelection = (SKHierarchicalObject) ((AdeleCommonDiagramEditPart) selection).getEObject();
			return true;
		} else
			return false;
	}
	
	protected void setSelection(TreeItem[] elements, boolean refresh) {
		selection = getTreeItemById(elements);
		if (selection != null){
			tv.getTree().setSelection(selection);
		}
		if (refresh){
			Event e = new Event();
			e.item = selection;
			tv.getTree().notifyListeners(SWT.Selection, e);
		}
	}

	protected TreeItem getTreeItemById(TreeItem[] elements){
		String id;
		TreeItem res = null;
		TreeItem tmp = null;
		for (int i=0;i<elements.length;i++){
			if (elements[i].getData() instanceof SKODSection)
				id = ((SKODSection) elements[i].getData()).getId();
			else 
				id = elements[i].getText();
			if (id.trim() == treeSelectionId.trim()){
				return elements[i];
			}
		}
		for (int i=0;i<elements.length;i++){
			tmp = getTreeItemById(elements[i].getItems());
			if (tmp != null) {
				res = tmp;
				break;
			}
		}
		return res;
	}
	
	public TreeViewer getTreeViewer(){
		return tv;
	}
	
	public String getCompositeId(){
		return compositeId;
	}

}
