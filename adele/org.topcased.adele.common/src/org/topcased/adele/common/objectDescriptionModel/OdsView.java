/**
 * 
 */
package org.topcased.adele.common.objectDescriptionModel;

import java.util.HashMap;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.views.contentoutline.ContentOutline;
import org.topcased.adele.common.properties.AdelePropertyPanel;
import org.topcased.adele.common.utils.ADELEModeler;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.adele.model.ObjectDescriptionModel.SKODData;
import org.topcased.adele.model.ObjectDescriptionModel.SKODSection;
import org.topcased.adele.model.ObjectDescriptionModel.SKODSystem;
import org.topcased.adele.model.ObjectDescriptionModel.impl.SKODSFactoryException;

/**
 * @author Arnaud
 *
 */
public class OdsView extends AdelePropertyPanel {

	protected ScrolledComposite odsFormScrolledComposite;
	protected Composite odsFormContainer;
	protected Composite odsTreeContainer;
	protected SKODSection currentSelectedSection;
	protected ADELEModeler currentEditor;
	protected TabFolder tabFolder;
	protected EList<String> odsystemsId = new BasicEList<String>(0);
	protected boolean dirty = false;
	
	protected HashMap<String, Object> specificAttributs = new HashMap<String, Object>();

	public OdsView( final Composite p_parent ) {
		super( p_parent );
		
		createControls();
	}

	private void createControls() {
		GridLayout layout = new GridLayout();
	    layout.numColumns = 3;
	    layout.makeColumnsEqualWidth = true;
	    setLayout(layout);
		
		final IEditorPart firstOpenedEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();

		if (firstOpenedEditor != null && firstOpenedEditor instanceof ADELEModeler){
			currentEditor = (ADELEModeler) firstOpenedEditor;
		}
		
		createTabItems();
		createFormItems();
	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		if ( part instanceof ADELEModeler || part instanceof ContentOutline	|| part instanceof OdsView ) {
			try{
				final Control selectedCtrl = tabFolder.getSelection()[0].getControl();
				
				for ( final TabItem item : tabFolder.getItems() ) {
					final OdsTreeViewComposite tvComp = (OdsTreeViewComposite) item.getControl();
					final boolean refresh = tvComp == selectedCtrl;
					tvComp.selectionChanged(part, selection, refresh );
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
			
			if (part instanceof ADELEModeler){
				if (!((ADELEModeler)part).equals(currentEditor)){
					eraseOdsView();
					currentEditor = (ADELEModeler)part;
				}
			}
		}
		else {
			eraseOdsView();
			TabItem[] tmp = tabFolder.getItems();
			
			for (int i = 0; i < tmp.length; i++) {
				((OdsTreeViewComposite) tmp[i].getControl()).selectionChanged(part, selection, false);
			}
			
			((OdsTreeViewComposite) tabFolder.getSelection()[0].getControl()).selectionChanged(part, selection, true);
		}
	}
	
	protected Text getText(String labelName){
		Control[] list = odsFormContainer.getChildren();
		for (int i=0; i<list.length; i++){
			if (list[i] instanceof Text && list[i].getData("id").equals(labelName))
				return (Text) list[i];
		}
		return null;
	}
	
	public Text getActiveText(){
		Control[] list = odsFormContainer.getChildren();
		for (int i=0; i<list.length; i++){
			if (list[i] instanceof Text && list[i].isFocusControl())
				return (Text) list[i];
		}
		return null;
	}

	protected void setOdsView (EList<SKODSection> names){
		eraseOdsView();
		addField(names.get(0), null);
		for (int i=1;i<names.size();i++){
			addField(names.get(i), odsFormContainer.getChildren()[2*i-1]);
		}
	}

	protected void setOdsView(SKODSection name){
		eraseOdsView();
		if (currentEditor.manageODSection(name.getId())) {
			currentEditor.setOdsView(this,name);
		}
		else
			addField(name, null);
	}

	protected void addField (SKODSection section, Control control){
		addStringField(section.getData(), section);
	}
	
//	protected void addField (SKODSection section, Control control){
//		SKODData data = section.getData();
//		Node type = (Node) data.getNode().getUserData("type");
//		
//		if (type != null) {
//
//			String href = type.getAttributes().getNamedItem("href").getNodeValue();		
//			IPreferenceStore store = AdeleCommonPlugin.getDefault().getPreferenceStore();
//			String databasePath = store.getString(AdeleCommonPreferenceConstants.ADELE_DATABASE_PATH);
//			String databaseFolder = databasePath.substring(0, databasePath.lastIndexOf(File.separator)+1);
//			
//			URI fileURI = URI.createFileURI(databaseFolder + href.substring(0, href.indexOf("@")-3) );
//			ResourceSet resourceSet = new ResourceSetImpl();
//			Resource resource = resourceSet.getResource(fileURI, true);
//			
//			PropertyTypeImpl pt = (PropertyTypeImpl) resource.getEObject(href.substring(href.indexOf("/")));
//			
//			
//				addUntypedField (data, section);
//
//			
//		} else {
//			addUntypedField (data, section);
//		}
//	}
//
//	protected void addUntypedField (SKODData data, SKODSection section){
//		addStringField(data, section);
//	}
	
	protected void addStringField (SKODData data, SKODSection section){
		new Label(odsFormContainer, SWT.NONE).setText(section.getLabel());
		Text txt = new Text(odsFormContainer, SWT.BORDER);
		txt.setData("id", section.getId());
		txt.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		if (data != null)
			txt.setText(data.getValue());
		/*SKProperty prop = currentSelectedHierarchicalObject.getProperty(section.getId());
		if ( prop != null){
			txt.setText(prop.getValue().toString());
		}*/

		txt.addKeyListener(new InnerKeyAdapter(this));
	}
	
	protected void eraseOdsView (){
		if ( odsFormContainer != null ) {
			odsFormContainer.dispose();
		}
		
		odsFormContainer = new Composite(odsFormScrolledComposite, SWT.NONE);
		odsFormContainer.setLayout(new GridLayout(2,false));
		odsFormScrolledComposite.setContent(odsFormContainer);
	}
	
	protected void eraseOdsTree(){
		if ( tabFolder != null ) {
			tabFolder.dispose();
		}
		
		tabFolder = new TabFolder( odsTreeContainer, SWT.NONE);
//		while (odsTreeContainer.getChildren().length>1){
//			odsTreeContainer.getChildren()[0].dispose();
//		}
	}
	
	protected SKODSection getSection(String id){
		return getSection(currentSelectedSection, id);
	}
		
	protected SKODSection getSection(SKODSection section, String id){
		if (section.getId().equals(id))
			return section;
		else
			return getChildSection(section, id);
	}
	
	protected SKODSection getChildSection(SKODSection section, String id){
		@SuppressWarnings({ "rawtypes", "unchecked" })
		EList<SKODSection> children = (EList) section.getChildren();
		if (children.size()>0){
			for (int i=0;i<children.size();i++){
				if (children.get(i).getId().equals(id))
					return children.get(i);
			}
			for (int i=0;i<children.size();i++){
				SKODSection tmp = getChildSection(children.get(i), id);
				if (tmp!=null)
					return tmp;
			}
			return null;
		}
		else
			return null;
	}
	
	protected EList<SKODSection> getTerminal(SKODSection section){
		EList<SKODSection> result = new BasicEList<SKODSection>();
		@SuppressWarnings({ "rawtypes", "unchecked" })
		EList<SKODSection> childrenSections = (EList) section.getChildren();
		if (childrenSections.size()<1)
			result.add(section);
		else {
			for (int i=0;i<childrenSections.size();i++){
				result.addAll(getTerminal(childrenSections.get(i)));
			}
		}
		return result;
	}
	
	public String getOdsSectionValue (String sectionName) {
		return getOdsSectionValue(sectionName, currentEditor.getSelectedObject());
	}
		
	public String getOdsSectionValue (String sectionName, SKHierarchicalObject object) {
		
		try {
			SKODSystem odSystem=null;
			SKODSection foundSection;
			for (String odSystemId  : odsystemsId) {
				odSystem = currentEditor.odStructure.getODSystem(odSystemId, object);
				if (odSystem!=null) {
					for (SKODSection section : odSystem.getSections()) {
						foundSection = getSection(section, sectionName);
						if (foundSection!=null)
							return foundSection.getData().getValue();
					}
				}
			}
		} catch (SKODSFactoryException e) {
			return  "";
		}
		
		return "";
	}
	
	protected void createFormItems(){
		
		if ( getChildren().length==2)
			getChildren()[0].dispose();
		
		Composite composite = new Composite(this, SWT.NONE);
		GridData data = new GridData(GridData.FILL_BOTH);
	    data.horizontalSpan = 2;
	    composite.setLayoutData(data);
	    GridLayout layout = new GridLayout();
	    layout.numColumns = 1;
	    composite.setLayout(layout);

	    data = new GridData(GridData.FILL_BOTH);
	    odsFormScrolledComposite = new ScrolledComposite(composite, SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.BORDER);
	    odsFormScrolledComposite.setLayoutData(data);
	    odsFormScrolledComposite.setLayout(new GridLayout());
	}
	
	protected void createTabItems() {
		if ( getChildren().length==2)
			getChildren()[0].dispose();
		
		EList<String> odsystemsLabel = new BasicEList<String>(0);
		odsystemsId = new BasicEList<String>(0);
		if (currentEditor!= null && currentEditor.odStructure!=null) {
			odsystemsId = currentEditor.odStructure.getODSystemsId();
			odsystemsLabel = currentEditor.odStructure.getODSystemsLabel();
		}
		else {
			odsystemsId.add("");
			odsystemsLabel.add("");
		}
		
		GridData data = new GridData(GridData.FILL_BOTH);
	    data.widthHint = 100;
	    odsTreeContainer = new Composite( this, SWT.NONE);
	    odsTreeContainer.setLayoutData(data);
	    odsTreeContainer.setLayout(new FillLayout());
		tabFolder = new TabFolder(odsTreeContainer, SWT.NONE);

		for (int i = 0; i < odsystemsId.size(); i++) {
			TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
			tabItem.setText(odsystemsLabel.get(i));
			OdsTreeViewComposite tmp = new OdsTreeViewComposite(tabFolder, i, odsystemsId
					.get(i), SWT.NONE);
			tmp.tv.addSelectionChangedListener(new InnerSelectionChangedListener(this));
			tabItem.setControl(tmp);
		}
		
	}
	
	public void refresh(){
		if (currentSelectedSection == null){
			eraseOdsView();
			return;
		}
		if (currentSelectedSection.isTerminal()) {
			setOdsView(currentSelectedSection);
		} else {
			setOdsView(getTerminal(currentSelectedSection));
		}
//		Point containerSize = odsFormContainer.computeSize(SWT.DEFAULT,
//				SWT.DEFAULT);
//		Point panelSize = odsFormScrolledComposite.getSize();
//		odsFormContainer.setSize(Math
//				.max(panelSize.x - 5, containerSize.x), Math.max(
//				panelSize.y - 5, containerSize.y));
	}
	
	public void rebuild() {
		eraseOdsTree();
		eraseOdsView();

		EList<String> odsystemsLabel = new BasicEList<String>(0);
		odsystemsId = new BasicEList<String>(0);
		if (currentEditor!= null && currentEditor.odStructure!=null) {
			odsystemsId = currentEditor.odStructure.getODSystemsId();
			odsystemsLabel = currentEditor.odStructure.getODSystemsLabel();
		}
		else {
			odsystemsId.add("");
			odsystemsLabel.add("");
		}
		
		for (int i = 0; i < odsystemsId.size(); i++) {
			TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
			tabItem.setText(odsystemsLabel.get(i));
			OdsTreeViewComposite tmp = new OdsTreeViewComposite(tabFolder, i, odsystemsId
					.get(i), SWT.NONE);
			tmp.tv.addSelectionChangedListener(new InnerSelectionChangedListener(this));
			tabItem.setControl(tmp);
		}
		

		//Point containerSize = tabFolder.computeSize( SWT.DEFAULT, SWT.DEFAULT );
		//Point panelSize = odsTreeContainer.getSize();
		
		//tabFolder.setSize( containerSize.x, containerSize.y );
	}
	
	public String getSelectedTabItemId(){
		return odsystemsId.get(tabFolder.getSelectionIndex());
	}
	
	public boolean isDirty(){
		return dirty;
	}
	
	public void setDirty(boolean flag){
		dirty=flag;
		currentEditor.refreshDirty();
	}
	
	public Object getSpecificAttribut(String name) {
		return specificAttributs.get(name);
	}
	
	public TabFolder getTabFolder () {
		return tabFolder;
	}
	
	public void setSpecificAttribut(String name, Object value) {
		specificAttributs.put(name, value);
	}
	
	public Composite getOdsFormContainer () {
		return odsFormContainer;
	}
	
	public ADELEModeler getCurrentEditor () {
		return currentEditor;
	}
	
	protected class InnerSelectionChangedListener implements ISelectionChangedListener{

		OdsView parent;
		
		public InnerSelectionChangedListener(OdsView _parent){
			parent=_parent;
		}
		
		public void selectionChanged(SelectionChangedEvent arg0) {
			ISelection sel = arg0.getSelection();
			if (sel instanceof IStructuredSelection){
				Object tmp = ((IStructuredSelection)sel).getFirstElement();
				
				if (tmp instanceof SKODSection) {
					currentSelectedSection = (SKODSection)tmp;
					String currentTabItemId = parent.getSelectedTabItemId();
					OdsTreeViewComposite tmpTree = (OdsTreeViewComposite) parent.tabFolder.getSelection()[0].getControl();
					if (currentTabItemId == tmpTree.getCompositeId())
						tmpTree.treeSelectionId = ((SKODSection)tmp).getId();
				}
				if (tmp==null)
					eraseOdsView();
				else {
					parent.refresh();
				}

				Point containerSize = odsFormContainer.computeSize( SWT.DEFAULT, SWT.DEFAULT );
				Point panelSize = odsFormScrolledComposite.getSize();
				odsFormContainer.setSize( 	Math.max( panelSize.x - 5, containerSize.x ),
											Math.max( panelSize.y - 5, containerSize.y ) );
			}
		}
	}
	
	protected class InnerKeyAdapter extends KeyAdapter {
		
		OdsView parent;
		
		public InnerKeyAdapter(OdsView _parent){
			parent=_parent;
		}
		
		public void keyReleased(KeyEvent e) {
			try {
				SKODSection section = parent.getSection((String) parent.getActiveText().getData("id"));
				section.setValueData(parent.getActiveText().getText());
				parent.setDirty(true);
			}
			catch(SKODSFactoryException msg){
				System.err.println(msg.getMessage());
			}
		}
	}
}
