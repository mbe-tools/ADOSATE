/**
 * 
 */
package org.topcased.adele.common.objectDescriptionModel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.topcased.adele.common.utils.ADELEModeler;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.adele.model.ObjectDescriptionModel.SKODSection;

/**
 * @author Arnaud
 *
 */
public class OdsTreeViewContentProvider implements ITreeContentProvider {

	protected SKHierarchicalObject[] root = {null};
	protected String odsystemName;
	private ADELEModeler editor;
	private int tabfolderNumber;
	
	public OdsTreeViewContentProvider(String name, int tabNumber) {
		odsystemName = name;
		tabfolderNumber = tabNumber;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
	 */
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof SKODSection)
			return ((SKODSection) parentElement).getChildren().toArray();
		else if (parentElement instanceof SKHierarchicalObject){
			try{
				EList<SKODSection> tmp = editor.getODSystem(tabfolderNumber).getSections();
				Object[] result = new Object[tmp.size()];
				for (int i=0; i<tmp.size(); i++)
					result[i]=tmp.get(i);
				return result;
			}
			catch (NullPointerException e){
				return null;
			}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
	 */
	public Object getParent(Object element) {
		//return ((SKObjectImpl) element).eContainer();
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
	 */
	public boolean hasChildren(Object element) {
		Object[] obj = getChildren(element);

	    // Return whether the parent has children
	    return obj == null ? false : obj.length > 0;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	public Object[] getElements(Object inputElement) {
		if (root[0]!=null) {
			Object[] tmp = root.clone();
			root[0] = null;
			return tmp;
		}
		else {
			return getChildren(inputElement);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	public void dispose() {

	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		if (newInput!=null)
			root[0] = (SKHierarchicalObject) newInput;
		else if (newInput == null)
			root[0] = null;
	}
	
	public void setCurrentEditor(ADELEModeler _editor){
		editor = _editor;
	}

}
