/**
 * 
 */
package org.topcased.adele.common.objectDescriptionModel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.LabelProviderChangedEvent;
import org.eclipse.swt.graphics.Image;
import org.topcased.adele.model.KernelSpices.SKObject;
import org.topcased.adele.model.ObjectDescriptionModel.SKODSection;

/**
 * @author Arnaud
 *
 */
public class OdsTreeViewLabelProvider implements ILabelProvider {
	// The listeners
	  private List<ILabelProviderListener> listeners;

	  // Images for tree nodes
	  private Image file;

	  private Image dir;

	  // Label provider state: preserve case of file names/directories
	  boolean preserveCase;

	  public OdsTreeViewLabelProvider() {
		    // Create the list to hold the listeners
		    listeners = new ArrayList<ILabelProviderListener>();

		    // Create the images
		    try {
		      file = new Image(null, new FileInputStream("images/file.gif"));
		      dir = new Image(null, new FileInputStream("images/directory.gif"));
		    } catch (FileNotFoundException e) {
		      // Swallow it; we'll do without images
		    }
		  }

		  /**
		   * Sets the preserve case attribute
		   * 
		   * @param preserveCase
		   *            the preserve case attribute
		   */
		  public void setPreserveCase(boolean preserveCase) {
		    this.preserveCase = preserveCase;

		    // Since this attribute affects how the labels are computed,
		    // notify all the listeners of the change.
		    LabelProviderChangedEvent event = new LabelProviderChangedEvent(this);
		    for (int i = 0, n = listeners.size(); i < n; i++) {
		      ILabelProviderListener ilpl = (ILabelProviderListener) listeners
		          .get(i);
		      ilpl.labelProviderChanged(event);
		    }
		  }
	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ILabelProvider#getImage(java.lang.Object)
	 */
	public Image getImage(Object element) {
		return file;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ILabelProvider#getText(java.lang.Object)
	 */
	public String getText(Object element) {
		if (element instanceof SKODSection)
			return ((SKODSection)element).getLabel();
		else if (element instanceof SKObject)
			return ((SKObject)element).getName();
		else 
			return (String)element;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IBaseLabelProvider#addListener(org.eclipse.jface.viewers.ILabelProviderListener)
	 */
	public void addListener(ILabelProviderListener listener) {
		listeners.add(listener);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IBaseLabelProvider#dispose()
	 */
	public void dispose() {
	    // Dispose the images
	    if (dir != null)
	      dir.dispose();
	    if (file != null)
	      file.dispose();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IBaseLabelProvider#isLabelProperty(java.lang.Object, java.lang.String)
	 */
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IBaseLabelProvider#removeListener(org.eclipse.jface.viewers.ILabelProviderListener)
	 */
	public void removeListener(ILabelProviderListener listener) {
		listeners.remove(listener);
	}

}
