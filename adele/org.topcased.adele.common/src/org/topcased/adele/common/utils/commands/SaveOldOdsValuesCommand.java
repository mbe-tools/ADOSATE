/**
 * 
 */
package org.topcased.adele.common.utils.commands;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.topcased.adele.common.utils.ADELEModeler;
import org.topcased.adele.model.KernelSpices.impl.SKHierarchicalObjectImpl;
import org.topcased.adele.model.ObjectDescriptionModel.impl.SKODSFactoryException;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.editor.ModelerGraphicalViewer;
import org.topcased.modeler.utils.Utils;

/**
 * @author Arnaud
 *
 */
public class SaveOldOdsValuesCommand extends Command {

	protected EditPart part;
	
	/**
	 * 
	 */
	public SaveOldOdsValuesCommand() {
		super();
	}

	/**
	 * @param _part
	 */
	public SaveOldOdsValuesCommand(EditPart _part) {
		super();
		part=_part;
	}
	
	@Override
	public boolean canExecute() {
		return true;
	}
	
	@Override
	public boolean canUndo() {
		return true;
	}
	
	@Override
	public void undo() {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void execute() {
		redo();
	}
	
	@Override
	public void redo() {
		ADELEModeler editor = (ADELEModeler) ((ModelerGraphicalViewer) part.getParent().getViewer()).getModelerEditor();
		saveOdsValues((SKHierarchicalObjectImpl) Utils.getElement((GraphElement)part.getModel()), editor);
	}
	
	protected void saveOdsValues(SKHierarchicalObjectImpl model, ADELEModeler editor){
		
		List<String> odsIds = editor.odStructure.getODSystemsId();
		List<Object> odsList = new BasicEList<Object>(odsIds.size());
		for (int i=0; i<odsIds.size(); i++){
			try {
				odsList.add(i, editor.odStructure.getODSystem(odsIds.get(i), model));
			} catch (SKODSFactoryException e) {
				e.printStackTrace();
			}
		}
		model.setOldOds(odsList);
		
		EList<?> children = model.getChildren();
		if (children.size()>0){
			for (int i=0; i<children.size(); i++)
				saveOdsValues((SKHierarchicalObjectImpl)children.get(i), editor);
		}
	}

}
