/**
 * 
 */
package org.topcased.adele.common.utils.commands;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Tool;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.topcased.adele.common.utils.actions.ChangeActiveDiagramAction;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.adele.model.KernelSpices.impl.SKHierarchicalObjectImpl;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.diagrams.model.util.DiagramsUtils;
import org.topcased.modeler.edit.DiagramEditPart;
import org.topcased.modeler.edit.GraphNodeEditPart;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.editor.ModelerGraphicalViewer;
import org.topcased.modeler.utils.Utils;

/**
 * @author Arnaud
 *
 */
public class SetUpFromOneLevelCommand extends Command {

	protected DiagramEditPart diag;
	protected EditPart part;
	protected SKComponent element;
	
	public SetUpFromOneLevelCommand(EditPart _part) {
		super("Set element up from one level");
		part=_part;
		element = (SKComponent) Utils.getElement((GraphElement)part.getModel());
	}
	
	@Override
	public void execute() {
		redo();
	}
	
	@Override
	public void redo() {
		
		Iterator<?> sel;
		
		try{		
			sel = ((IStructuredSelection)part.getViewer().getSelection()).iterator();
		}
		catch (Exception e){
			return;
		}
		
		Modeler editor = ((ModelerGraphicalViewer) part.getParent().getViewer()).getModelerEditor();
		BasicEList<EModelElement> node = new BasicEList<EModelElement>();
		
		try {
			GraphNodeEditPart nodeEP;
			while (sel.hasNext()){
				nodeEP = (GraphNodeEditPart)sel.next();
				node.add((EModelElement) nodeEP.getModel());
				
				SaveOldOdsValuesCommand cmd = new SaveOldOdsValuesCommand(nodeEP);
				if (cmd.canExecute())
					cmd.execute();
				
			}
		}
		catch (Exception e){ 
			return;
		}

		SKHierarchicalObjectImpl compo = (SKHierarchicalObjectImpl)((SKHierarchicalObjectImpl)Utils.getElement(editor.getActiveDiagram())).getParent();
		compo.setNewChildUp(node);
		
		DiagramEditPart diag = (DiagramEditPart)part.getParent();
		
		Tool activeTool = diag.getViewer().getEditDomain().getActiveTool();

		// deactivate the active tool
		diag.getViewer().getEditDomain().setActiveTool(null);
		diag.getViewer().getEditDomain().getPaletteViewer().setActiveTool(null);

		// The action to execute if a the active diagram must change
		IAction action = null;
		action = changeDiagramAction(diag.getEObject().eContainer(),((ModelerGraphicalViewer) diag.getViewer()).getModelerEditor());

		if (action != null) {
			action.run();
		} else {
			// restore the active tool
			diag.getViewer().getEditDomain().setActiveTool(activeTool);
		}
	}

	protected IAction changeDiagramAction(EObject modelObject, Modeler editor) {

		IAction action = null;
		
		// We search if a diagram exists.

		List<Diagram> existingDiagramList = DiagramsUtils.findAllExistingDiagram(editor
				.getDiagrams(), modelObject);
		if (existingDiagramList != null && existingDiagramList.size() > 0) {
			Diagram selectedDiagram = (Diagram) ((Diagram[]) existingDiagramList
					.toArray(new Diagram[existingDiagramList.size()]))[0];
			if (selectedDiagram != null) {
				action = new ChangeActiveDiagramAction(editor, selectedDiagram, modelObject);
			}
		}

		return action;
	}
	
	@Override
	public boolean canExecute() {
		boolean valideName = true;
		try {
			EList<SKHierarchicalObject> brothers = element.getParent().getParent().getChildren();
			String name = element.getName();
			for (int i=0;i<brothers.size();i++){
				if (((SKComponent)brothers.get(i)).getName().equals(name) &&
						!((SKComponent)brothers.get(i)).equals(element))
				valideName = false;
			}
		}
		catch (Exception e) {
			valideName = false;
		}
		return valideName;
	}
}
