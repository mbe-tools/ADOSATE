package org.topcased.adele.common.utils.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.topcased.adele.model.ADELE.util.ADELEModelUtils;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Components.Package;
import org.topcased.adele.model.ADELE_Features.Access;
import org.topcased.adele.model.ADELE_Features.AccessDirection;
import org.topcased.adele.model.ADELE_Features.DirectedFeature;
import org.topcased.adele.model.ADELE_Features.Feature;
import org.topcased.adele.model.ADELE_Features.FeatureGroup;
import org.topcased.adele.model.ADELE_Features.PortDirection;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.adele.model.KernelSpices.SKFeature;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.modeler.ModelerPropertyConstants;
import org.topcased.modeler.commands.CreateGraphNodeCommand;
import org.topcased.modeler.commands.DeleteGraphElementCommand;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.DiagramElement;
import org.topcased.modeler.di.model.EMFSemanticModelBridge;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.di.model.Property;
import org.topcased.modeler.di.model.util.DIUtils;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.utils.Utils;

/**
 * This command is used to refresh the graphical representation of any subcomponent
 * of a Component instantiation.
 * 
 */
public class RefreshFeaturesCommand extends AbstractRefreshCommand {

	/**
     * The Constructor
     * 
     * @param modeler the modeler
     * @param component the component model object
     */
    public RefreshFeaturesCommand(	final Modeler p_modeler,
									final SKComponent p_component )
    throws CoreException {
    	super( p_modeler, p_component );
    }
    
    public RefreshFeaturesCommand(	final Diagram p_diagram,
									final SKComponent p_component )
    throws CoreException {
        super( p_diagram, p_component );
    }
    
    private Collection<SKFeature> getAllOriginalFeatures( final SKComponent p_parent ) {
    	final Collection<SKFeature> features = new ArrayList<SKFeature>();

    	if ( p_parent instanceof Component ) {
        	final Component origComp = ( (Component) p_parent ).getOriginalComponent();
        	features.addAll( origComp.getAllRealFeatures() );
    	}
    	else if ( p_parent instanceof FeatureGroup ) {
    		final FeatureGroup parentFeatGrp = (FeatureGroup) p_parent;
    		final Feature origParentFeature = parentFeatGrp.getOriginalFeature();
    		
    		if ( !( origParentFeature instanceof FeatureGroup ) ) {
    			features.addAll( parentFeatGrp.getFeatures() );
    		}
    		else {
    			features.addAll( ( (FeatureGroup) origParentFeature ).getAllRealFeatures() );
    		}
    	}
    	else {
    		features.addAll( p_parent.getFeatures() );
    	}
    	
    	return features;
    }

    private Collection<SKFeature> getDisplayableFeatures( final SKComponent p_parent ) {
    	final Collection<SKFeature> children = new ArrayList<SKFeature>();
    	
		if ( 	!( p_parent instanceof Component ) ||
				!( (Component) p_parent ).isImplementation() || !( getRootComponent() instanceof Package ) ) {
   			children.addAll( p_parent.getFeatures() );
		}
    	
    	return children;
    }
    
    private void mergeFeatures( final SKComponent p_parent,
    							final Collection<SKFeature> p_originalFeatures ) {
    	final Iterator<SKFeature> existFeatIt = p_parent.getFeatures().iterator();
    	final FeatureGroup parentFeatGroup = p_parent instanceof FeatureGroup ? (FeatureGroup) p_parent : null;
    	
    	while ( existFeatIt.hasNext() ) {
    		final SKFeature existFeat = existFeatIt.next();
    		final SKFeature origFeat = ADELEModelUtils.findElementByName( existFeat, p_originalFeatures );
    		
    		if ( origFeat == null ) {
    			removeConnections( existFeat );
            	existFeatIt.remove();
    		}
    	}

    	for ( final SKFeature originalFeat : p_originalFeatures ) {
    		final SKFeature existingFeat = ADELEModelUtils.findElementByName( originalFeat, p_parent.getFeatures() );
    		final boolean inverse;
    		
        	// If from an inverse feature group, inverse the direction
    		if ( parentFeatGroup != null && parentFeatGroup.isInverse( (Feature) originalFeat ) ) {
    			inverse = true;
    		}
    		else {
    			inverse =  false;
    		}
        	
    		if ( existingFeat == null || existingFeat.eClass() != originalFeat.eClass() ) {
    			if ( existingFeat != null ) {
        			p_parent.getFeatures().remove( existingFeat );
    			}
    			
    			final SKFeature clonedFeat = cloneFeature( originalFeat );
    			
    			if ( inverse ) {
    				inverseFeatureDirection( (Feature) clonedFeat );
    			}

    			p_parent.getFeatures().add( clonedFeat );
    		}
    		else {
    			if ( inverse ) {
    				inverseFeatureDirection( (Feature) existingFeat );
    			}
    			
        		mergeFeature( originalFeat, existingFeat );
    		}
    	}
    }
    
    private void inverseFeatureDirection( final Feature p_feature ) {
    	if ( p_feature instanceof DirectedFeature ) {
    		inverseFeatureDirection( (DirectedFeature) p_feature );
    	}
    	else if ( p_feature instanceof Access ) {
    		inverseFeatureDirection( (Access) p_feature );
    	}
    }
    
    private void inverseFeatureDirection( final DirectedFeature p_feature ) {
    	if ( p_feature.getDirection() != null ) {
    		switch ( p_feature.getDirection() ) {
				case IN_LITERAL:
					p_feature.setDirection( PortDirection.OUT_LITERAL );
					break;
	
				case OUT_LITERAL:
					p_feature.setDirection( PortDirection.IN_LITERAL );
					break;
				default:
					break;
			}
    	}
    }
    
    private void inverseFeatureDirection( final Access p_feature ) {
    	if ( p_feature.getDirection() != null ) {
    		switch ( p_feature.getDirection() ) {
				case PROVIDED_LITERAL:
					p_feature.setDirection( AccessDirection.REQUIRED_LITERAL );
					break;
	
				case REQUIRED_LITERAL:
					p_feature.setDirection( AccessDirection.PROVIDED_LITERAL );
					break;
				default:
					break;
			}
    	}
    }
    
    private SKFeature cloneFeature( final SKFeature p_feature ) {
    	final SKFeature clonedFeature = EcoreUtil.copy( p_feature );
    	//clonedFeature.setId( EcoreUtil.generateUUID() );

    	// This is to ensure child features of feature group are set with unique IDs.
//		if ( clonedFeature instanceof SKComponent ) {
//			for ( final SKFeature clonedSubFeature : ( (SKComponent) clonedFeature ).getFeatures() ) {
//				clonedSubFeature.setId( EcoreUtil.generateUUID() );
//			}
//		}

    	return clonedFeature;
    }
    
    private void mergeFeature( 	final SKFeature p_sourceFeature,
    							final SKFeature p_targetFeature ) {
    	if ( p_sourceFeature instanceof Feature && p_targetFeature instanceof Feature ) {
    		final Feature sourceFeat = (Feature) p_sourceFeature;
    		final Feature targetFeat = (Feature) p_targetFeature;
    		targetFeat.setClassifier( sourceFeat.getClassifier() );
    		targetFeat.setRefinedFeature( sourceFeat.getRefinedFeature() );
    	}	
    }

    /**
     * Refresh the features of a given GraphNode. Refresh all the GraphNodes
     * linked with the model object associated with the GraphNode.
     * 
     * @param componentNode
     */
    @Override
    protected void createRefreshCommands() {
    	final SKComponent rootComponent = getRootComponent();
		mergeFeatures( rootComponent, getAllOriginalFeatures( rootComponent ) );
		
		// Ensure the features of subcomponents are also duplicated so that later on connections can be refresehd properly
		for ( final SKHierarchicalObject child : rootComponent.getChildren() ) {
			if ( child instanceof SKComponent ) {
				final SKComponent childComp = (SKComponent) child;
				mergeFeatures( childComp, getAllOriginalFeatures( childComp ) );
			}
		}
		
		final Collection<SKFeature> features = rootComponent.getFeatures();
		final GraphNode currentNode = getGraphicalRepresentation( rootComponent, getActiveDiagram() );
		
		if ( currentNode != null ) {
	    	// Delete the graph nodes that have been removed
			for ( final DiagramElement children : currentNode.getContained() ) {
	            if ( children instanceof GraphNode ) {
	            	final GraphNode node = (GraphNode) children;
	            	if ( node.getSemanticModel() instanceof EMFSemanticModelBridge ) {
		                final EObject obj = Utils.getElement( node );
		                
		                // DB: Delete graphical elements whose semantic element is a proxy (does not exist anymore)
		                if ( 	obj == null || obj.eIsProxy() || 
		                		( isManaged( obj ) && !features.contains( obj ) ) ) {
		                    add( new DeleteGraphElementCommand( node, true ) );
		                }
	            	}
	            }
			}
			
			int offSet = 5;
			
			// Create graph node for all components that do not have a graphical representation
	    	final Collection<SKFeature> displayableFeatures = getDisplayableFeatures( rootComponent );

	    	for ( final SKFeature feature : features ) {
	        	if ( displayableFeatures.contains( feature ) ) {
	        		offSet = createCreateGraphNodeCommand( /*rootComponent,*/ feature, currentNode, offSet );
	        	}
			}
		}
    }
    
    private boolean isManaged( final EObject p_object ) {
    	if ( p_object instanceof SKFeature ) {
    		if ( p_object instanceof FeatureGroup ) {
    			return !( (FeatureGroup) p_object ).isType();
    		}
    		
    		return true;
    	}
    	
    	return false;
    }
    
    private int createCreateGraphNodeCommand( 	final SKFeature p_children,
    											final GraphNode p_parentNode,
    											int pi_offSet ) {
	    	GraphNode node = getGraphicalRepresentation( p_children, p_parentNode );
	    	
			if ( node == null ) {
				node = createGraphElement( p_children );
		
				final Object[] graphProps = determineGraphicalProperties( p_children, pi_offSet );
				final Dimension size = (Dimension) graphProps[ 0 ];
				final Point position = (Point) graphProps[ 1 ];
    			final Integer attachment = (Integer) graphProps[ 2 ];
				
				final CreateGraphNodeCommand cmd = new CreateGraphNodeCommand( 	getEditDomain(),
																				node,
																				p_parentNode,
																				position,
																				size,
																				attachment );
				add( cmd );
				pi_offSet = pi_offSet + 5;
			}
		
		return pi_offSet;
    }
    
    private Object[] determineGraphicalProperties( 	final SKFeature p_feature,
    												final int offSet ) {
    	Dimension size = null;
    	Point position = null;
    	Integer attachment = null;
    	
    	SKFeature origFeat = null;
    	
    	if ( p_feature instanceof Feature ) {
    		final Feature feat = (Feature) p_feature;
    		origFeat = feat.getOriginalFeature();
    		
    		if ( origFeat == null ) {
    			origFeat = feat.getRefinedFeature();
    		}
    	}
    	else if ( p_feature instanceof FeatureGroup ) {
    		final FeatureGroup featGroup = (FeatureGroup) p_feature;
    		
    		assert !featGroup.isType();
    		
    		origFeat = featGroup.getRefinedFeature();
    			
			if ( origFeat == null ) {
				origFeat = (SKFeature) featGroup.getClassifier();
			}
    	}
    	
		if ( origFeat != null ) {
			// Search for the graphical properties of the inherited subcomponent
			final GraphNode refinedGraphNode = searchExistingGraphNode( origFeat );
			
			if ( refinedGraphNode != null ) {
				size = new Dimension( refinedGraphNode.getSize() );
				position = new Point( refinedGraphNode.getPosition() );
				final Property attachmentProp = DIUtils.getProperty( refinedGraphNode, ModelerPropertyConstants.PORT_POSITION );
				
				if ( attachmentProp != null ) {
					attachment = Integer.valueOf( attachmentProp.getValue() );
				}
			}
		}

// Return a null size for features whose size is adjusted automatically.
//		if ( size == null ) {
//    		size = new Dimension( 50, 50 );
//    	}

    	if ( position == null ) {
    		position = new Point( offSet, offSet );
    	}
    	
    	if ( attachment == null ) {
    		attachment = PositionConstants.LEFT;
    	}
    	
    	return new Object[] { size, position, attachment } ;
    }

	protected SKComponent getRootComponent() {
		return (SKComponent) super.getRootComponent();
	}
    
    @Override
	protected GraphNode createGraphElement( final EObject p_semElement ) {
    	return (GraphNode) super.createGraphElement( p_semElement );
    }
}
