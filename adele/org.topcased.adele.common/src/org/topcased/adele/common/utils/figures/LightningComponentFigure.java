/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies), Mathieu Garcia (Anyware Technologies),
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/
package org.topcased.adele.common.utils.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * A Figure that represent a lightning.
 * 
 * Creation : 20 janv. 2006
 * 
 * @author <a href="mailto:jacques.lescot@anyware-tech.com">Jacques LESCOT</a>
 */
public class LightningComponentFigure extends Figure
{
    /**
     * @see org.eclipse.draw2d.Figure#paintFigure(org.eclipse.draw2d.Graphics)
     */
    protected void paintFigure(Graphics graphics)
    {
        int figWidth = bounds.width;
        int figHeight = bounds.height;

        graphics.drawArc(new Rectangle(bounds.getTopLeft().translate(0, -figHeight / 2), new Dimension(figWidth * 4 / 5, figHeight - figHeight % 2)), 180, 90);
        graphics.drawArc(new Rectangle(bounds.getTopLeft().translate(0, figHeight / 2), new Dimension(figWidth * 4 / 5, figHeight + figHeight % 2)), 0, 90);
        graphics.drawLine(bounds.getTopLeft().translate(figWidth * 4 / 5, figHeight), bounds.getTopLeft().translate(figWidth, figHeight * 4 / 5));
        graphics.drawLine(bounds.getTopLeft().translate(figWidth * 4 / 5, figHeight), bounds.getTopLeft().translate(figWidth / 2, figHeight - (figWidth * 1 / 5)));
    }

}
