package org.topcased.adele.common.utils;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.eclipse.emf.common.util.EList;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.part.FileEditorInput;
import org.topcased.adele.common.objectDescriptionModel.OdsView;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.adele.model.KernelSpices.SKObject;
import org.topcased.adele.model.KernelSpices.impl.SKHierarchicalObjectImpl;
import org.topcased.adele.model.ObjectDescriptionModel.SKODData;
import org.topcased.adele.model.ObjectDescriptionModel.SKODSection;
import org.topcased.adele.model.ObjectDescriptionModel.SKODSystem;
import org.topcased.adele.model.ObjectDescriptionModel.impl.SKODSFactoryException;
import org.topcased.adele.model.ObjectDescriptionModel.impl.SKODSFactoryImpl;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.utils.Utils;

public abstract class ADELEModeler extends Modeler {

	public static final String PACKAGE_EDITOR_KIND = "package";
	public static final String SYSTEM_EDITOR_KIND = "system";
	
	public SKODSFactoryImpl odStructure = null;
	protected SKODSystem[] odSystemArray;
	protected SKHierarchicalObject currentSelectedObject;
	protected boolean dirty = false;
	protected String editorKind = null;

	protected ADELEModeler() {
		super();
		
		setOutlineReadOnly( true );
	}

	public void saveMovedElements() {
		if (currentSelectedObject == null) {
			currentSelectedObject = (SKHierarchicalObject) Utils
					.getElement((GraphElement) getActiveDiagram());
		}
		SKHierarchicalObjectImpl root = (SKHierarchicalObjectImpl) getRootSKHierarchicalObject(currentSelectedObject);
		Iterator<SKHierarchicalObject> movedElementIterator = getMovedElements(root).iterator();
		SKHierarchicalObject currentModel;
		Vector<Object> odsList = new Vector<Object>(odSystemArray.length * 2);
		while (movedElementIterator.hasNext()) {
			currentModel = movedElementIterator.next();
			for (int i = 0; i < odSystemArray.length; i++) {
				try {
					odsList.add(odStructure.getODSystem(odStructure
							.getODSystemsId().get(i), currentModel));
					odsList.add(currentModel);
				} catch (SKODSFactoryException e) {
					e.printStackTrace();
				}
			}
		}
		Iterator<Object> odsIt = odsList.iterator();
		SKODSystem currentOds;
		int i = 0;
		while (odsIt.hasNext()) {
			try {
				currentOds = (SKODSystem) odsIt.next();
				currentModel = (SKHierarchicalObject) odsIt.next();
				SKODSystem oldOds = (SKODSystem) ((SKHierarchicalObjectImpl) currentModel)
						.getOldOds().get(i);
				String behaviorFileName = oldOds.getDomDataKeys().get("Behavior");
				File behaviorFile=null; 
				if (behaviorFileName!=null)
					behaviorFile = new File(behaviorFileName);
				EList<SKODSection> tmp = currentOds.getSections();
				EList<SKODSection> tmpOld = oldOds.getSections();
				if (tmp != null && tmpOld != null)
					copyDataValues(tmp, tmpOld);

				currentOds.rebuildDomData(odStructure, currentModel);
				removeOdsFiles(oldOds);
				currentOds.save(true);
				if (i == ((SKHierarchicalObjectImpl) currentModel).getOldOds()
						.size() - 1) {
					((SKHierarchicalObjectImpl) currentModel).setOldOds(null);
					i = 0;
				} else
					i++;
				if (behaviorFileName!=null)
					behaviorFile.renameTo(new File(currentOds.getDomDataKeys().get(
						"Behavior")));
			} catch (SKODSFactoryException e) {
				e.printStackTrace();
			} catch (TransformerConfigurationException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (TransformerException e) {
				e.printStackTrace();
			}
		}
	}

	protected Vector<SKHierarchicalObject> getRenamedElements(
			SKHierarchicalObject element) {
		Vector<SKHierarchicalObject> res = new Vector<SKHierarchicalObject>();
		if (element.isRenamed())
			res.add(element);
		Iterator<SKHierarchicalObject> it = element.getChildren().iterator();
		while (it.hasNext()) {
			res.addAll(getRenamedElements(it.next()));
		}
		return res;
	}

	protected Vector<SKHierarchicalObject> getMovedElements(SKHierarchicalObject element) {
		Vector<SKHierarchicalObject> res = new Vector<SKHierarchicalObject>();
		
		if ( ( (SKHierarchicalObjectImpl) element ).getOldOds() != null)
			res.add(element);
		Iterator<SKHierarchicalObject> it = element.getChildren().iterator();
		while (it.hasNext()) {
			res.addAll(getMovedElements(it.next()));
		}
		return res;
	}

	protected Vector<Object> getChildrenOds(Vector<Object> odsList,
			SKHierarchicalObject parent) {
		Iterator<SKHierarchicalObject> children = parent.getChildren().iterator();
		SKHierarchicalObject child;
		while (children.hasNext()) {
			child = children.next();
			child.setParent(parent);
			for (int i = 0; i < odSystemArray.length; i++) {
				try {
					odsList.add(odStructure.getODSystem(odStructure
							.getODSystemsId().get(i), child));
					odsList.add(child);
				} catch (SKODSFactoryException e) {
					e.printStackTrace();
				}
			}
			odsList = getChildrenOds(odsList, child);
		}
		return odsList;
	}

	protected void removeOdsFiles(SKODSystem ods) {
		final Map<String, String> domDataKeys = ods.getDomDataKeys();
		Iterator<String> filesIt = domDataKeys.keySet().iterator();
		File currentFile, parentDirectory;
		while (filesIt.hasNext()) {
			String fileIt = filesIt.next();
			if (!fileIt.equalsIgnoreCase("Behavior")) {
				currentFile = new File(domDataKeys.get(fileIt));
				parentDirectory = currentFile.getParentFile();
				currentFile.delete();
				removeParentDirectories(parentDirectory);
			}
		}
	}

	protected void removeParentDirectories(File currentDirectory) {
		File parentDirectory = currentDirectory.getParentFile();
		if (currentDirectory.delete())
			removeParentDirectories(parentDirectory);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void copyDataValues(EList<SKODSection> newSections,
			EList<SKODSection> oldSections) throws SKODSFactoryException {
		for (int j = 0; j < newSections.size(); j++) {
			SKODData data = oldSections.get(j).getData();
			if (data != null)
				newSections.get(j).setValueData(data.getValue());
			EList<SKODSection> tmp = null;
			EList<SKODSection> tmpOld = null;
			try {
				tmp = (EList) newSections.get(j).getChildren();
				tmpOld = (EList) oldSections.get(j).getChildren();
			} 
			catch (Exception e) {
			}
			if (tmp != null && tmpOld != null && tmp.size() > 0
					&& tmpOld.size() > 0)
				copyDataValues(tmp, tmpOld);
		}
	}

	public SKHierarchicalObject getSelectedObject() {
		return currentSelectedObject;
	}

	public void dirtify() {
		dirty = true;
		refreshDirty();
	}

	public void refreshDirty() {
		firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	public SKODSystem getODSystem(int index) {
		return odSystemArray[index];
	}

	protected void saveODSystems() {
		for (int i = 0; i < odSystemArray.length; i++) {
			try {
				if (odSystemArray[i].isModified())
					odSystemArray[i].save();
			} catch (TransformerConfigurationException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (TransformerException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
			}
		}
	}

	protected SKHierarchicalObject getRootSKHierarchicalObject(
			SKHierarchicalObject current) {
		if (current.getLevel() == 0)
			return current;
		else
			return getRootSKHierarchicalObject((SKHierarchicalObject) current
					.eContainer());
	}

//	public MixedEditDomain getMixedEditDomain() {
//		return (MixedEditDomain) getEditDomain();
//	}

	// DB: Use setActiveDiagram() from super class, which is the same implementation. 
//	public void refreshDiagram() {
//		getGraphicalViewer().setContents(getActiveDiagram());
//	}

	public String getEditorKind() {
		return editorKind;
	}
	
	public boolean isSystemKind() {
		return SYSTEM_EDITOR_KIND.equals( getEditorKind() ); 
	}

	public abstract String getEditorKind( SKObject compo);

	public abstract void setEditorKind( SKObject compo);

	public void setPublicEditor(FileEditorInput f) {
		setInput(f);
	}
	
	//public abstract void refreshComponents();
	
	public abstract boolean manageODSection(String name);
	
	public abstract void setOdsView(OdsView ods, SKODSection name);

	@Override
	protected void setIsReadOnly(boolean newReadOnly, boolean printUserMessages) {
		// DB: Bypass read only mechanism.
	}
//    
//	@Override
//    protected CompoundCommand createSynchronizeDiagramWithSemanticModelCommand() {
//		final CompoundCommand command = super.createSynchronizeDiagramWithSemanticModelCommand();
//
//		for ( final Command childrenCommand : createGraphNodeCommand( getActiveDiagram() ) ) {
//			command.add( childrenCommand );
//		}
//
//		return command;
//	}
//	
//	private List<Command> createGraphNodeCommand( final GraphNode p_parentGraphNode ) {
//		final List<Command> commands = new ArrayList<Command>();
//		final EObject parentSemanticElement = Utils.getElement( p_parentGraphNode );
//		
//		if ( parentSemanticElement instanceof SKHierarchicalObject ) {
//			for ( final SKHierarchicalObject children : ( (SKHierarchicalObject) parentSemanticElement ).getChildren() ) {
//				if ( Utils.getGraphElement( p_parentGraphNode, children, false ) == null ) {
//		            final DiagramEditPart target = (DiagramEditPart) getGraphicalViewer().getEditPartRegistry().get( getActiveDiagram() );
//		            
//		            if ( target != null ) {
//			            final CreateRequest request = new CreateRequest();
//			            final DropFactory factory = new DropFactory( this );
//			            factory.setTransferredObjects( Collections.singletonList( children ) );
//			            request.setFactory( factory );
//			            commands.add( target.getCommand( request ) );
//		            }
//				}
//			}
//		}
//		
//		return commands;
//	}
	
//	protected void createCreateGraphEdgeCommand( final GraphNode p_graphNode ) {
//		final EObject parentSemanticElement = Utils.getElement( p_graphNode );
//    	final List<SKRelation> connections = new BasicEList<SKRelation>();
//    	final Method[] methods = parentSemanticElement.getClass().getMethods();
//
//		for ( final Method method : methods ) {
//			final int methodModifier = method.getModifiers();
//			
//			if ( 	Modifier.isPublic( methodModifier ) && !Modifier.isStatic( methodModifier )  && 
//					method.getName().endsWith( "Connection" ) ) {
//				try {
//					connections.addAll( (EList<SKRelation>) method.invoke( parentSemanticElement, (Object []) null ));
//				}
//				catch (IllegalArgumentException e) {
//					e.printStackTrace();
//				} 
//				catch (IllegalAccessException e) {
//					e.printStackTrace();
//				}
//				catch (InvocationTargetException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//    	
//    	for ( final SKRelation relation : connections ) {
//    		// retrieve the connection to display
//
//    		// check if the element is already present
//    		if (Utils.getGraphElement(modeler.getActiveDiagram(), relation, true) == null)
//    		{
//    			GraphEdge edge = (GraphEdge) creationUtils.createGraphElement(relation);
//
//    			List<?> nodeSrc = Utils.getGraphElements(compNode, relation.getObjects().get(0),true);
//    			List<?> nodetrg = Utils.getGraphElements(compNode, relation.getObjects().get(1),true);
//
//
//    			if (nodeSrc.size()>0 && nodetrg.size()>0){
//    				CreateGraphEdgeCommand cmd = new CreateGraphEdgeCommand(
//    						(DefaultEditDomain) modeler
//    						.getAdapter(DefaultEditDomain.class), edge,
//    						(GraphNode)nodeSrc.get(0), true);
//    				cmd.setTarget((GraphNode)nodetrg.get(0));
//    				add(cmd);
//    			}
//    		}
//    	}
//    }
}
