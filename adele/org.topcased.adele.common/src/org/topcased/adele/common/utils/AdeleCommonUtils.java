package org.topcased.adele.common.utils;

import java.util.Iterator;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.topcased.adele.model.ADELE_Features.FeatureGroup;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.adele.model.KernelSpices.SKFeature;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.adele.model.KernelSpices.SKObject;
import org.topcased.adele.model.KernelSpices.SKRelation;
import org.topcased.modeler.di.model.GraphConnector;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.diagrams.model.util.DiagramsUtils;
import org.topcased.modeler.utils.Utils;
import org.topcased.adele.model.ADELE_Components.Package;

import fr.labsticc.gmm.merge.service.GmmMergeUtil;

public class AdeleCommonUtils {
	
	public static final String ADELE_EXT = "adele";
	public static final String ADELE_DI_EXT = ADELE_EXT + DiagramsUtils.DIAGRAM_EXT_SUFFIX;
	
	public static boolean isAdeleResource( final Resource p_res ) {
		if ( p_res == null ) {
			return false;
		}
		
		return isAdeleResource( p_res.getURI() );
	}
	
	public static boolean isAdeleResource( final URI p_resUri ) {
		if ( p_resUri == null || p_resUri.fileExtension() == null ) {
			return false;
		}
		
		return ADELE_EXT.equals( p_resUri.fileExtension() );
	}

	public static boolean isAdeleResource( final IResource p_res ) {
		if ( p_res == null || p_res.getFileExtension() == null ) {
			return false;
		}
		
		return ADELE_EXT.equals( p_res.getFileExtension() );
	}

	public static boolean isAdeleDiagramResource( final IResource p_res ) {
		return ADELE_DI_EXT.equals( p_res.getFileExtension() );
	}

	public static void setParent(GraphNode obj){
		SKHierarchicalObject parent = (SKHierarchicalObject) Utils.getElement((GraphElement)obj.eContainer());
		SKHierarchicalObject child = (SKHierarchicalObject) Utils.getElement(obj);
		
		if ( parent != null ) {
			if ( parent.isComponent() ) {
				if ( child.isComponent() || ( parent instanceof Package && child instanceof FeatureGroup ) ) {
					child.setParent(parent);
				}
				else if ( child.isFeature() ) {
					((SKFeature)child).setComponent((SKComponent)parent);
				}
			}								// DB: Features of feature groups are contained bty the features property.
			else if (parent.isFeature() && child.isFeature() && !( parent instanceof FeatureGroup ) ) {
				child.setParent(parent);
				((SKFeature)child).setComponent(((SKFeature)parent).getComponent());
			}
			else if (parent.isRelation() && child.isRelation()) {
				child.setParent(parent);
			}
		}
	}
	
	public static void setParent(GraphEdge obj){
		SKHierarchicalObject parent = (SKHierarchicalObject) Utils.getElement((GraphElement)obj.eContainer());
		SKRelation relation = (SKRelation) Utils.getElement((GraphElement)obj);
		EList<GraphConnector> tmp = obj.getAnchor();
		final EList<SKObject> objects = relation.getObjects();// DB: Should never do this!! new BasicInternalEList<SKObject>(SKObjectImpl.class);
		objects.add((SKObject)Utils.getElement(tmp.get(0).getGraphElement()));
		objects.add((SKObject)Utils.getElement(tmp.get(1).getGraphElement()));
		//relation.setObjects(objects);
		
		if (parent!=null && parent.isRelation() ) {
			relation.setParent(parent);
				// DB: Made this attribute derived
				//relation.setLevel(parent.getLevel()+1);
			//}
		}
	}
	
	public static boolean equals( 	final EObject p_objLeft,
									final EObject p_objRight )
	throws CoreException {
		return GmmMergeUtil.getMergeService().equals(p_objLeft, p_objRight, false, 1.0d, 0.0d );
	}
	
	public static SKComponent getRoot(SKComponent compo){
		while (compo.getParent()!=null){
			compo=(SKComponent)compo.getParent();
		}
		return compo;
	}
	
	public static EList<SKHierarchicalObject> cloneSKComponentList (EList<SKHierarchicalObject> original) {
		EList<SKHierarchicalObject> res = new BasicEList<SKHierarchicalObject>();
		Iterator<SKHierarchicalObject> it = original.iterator();
		while (it.hasNext())
			res.add(it.next());
		return res;
	}
}
