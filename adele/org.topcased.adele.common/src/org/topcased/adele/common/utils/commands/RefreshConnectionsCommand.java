package org.topcased.adele.common.utils.commands;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.topcased.adele.model.ADELE.util.ADELEModelUtils;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Features.Feature;
import org.topcased.adele.model.ADELE_Relations.AbstractFeatureConnection;
import org.topcased.adele.model.ADELE_Relations.BusAccessConnection;
import org.topcased.adele.model.ADELE_Relations.DataAccessConnection;
import org.topcased.adele.model.ADELE_Relations.DataPortConnection;
import org.topcased.adele.model.ADELE_Relations.EventDataPortConnection;
import org.topcased.adele.model.ADELE_Relations.EventPortConnection;
import org.topcased.adele.model.ADELE_Relations.FeatureGroupConnection;
import org.topcased.adele.model.ADELE_Relations.ParameterConnection;
import org.topcased.adele.model.ADELE_Relations.Relation;
import org.topcased.adele.model.ADELE_Relations.SubprogramAccessConnection;
import org.topcased.adele.model.ADELE_Relations.SubprogramGroupAccessConnection;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.adele.model.KernelSpices.SKObject;
import org.topcased.adele.model.KernelSpices.SKRelation;
import org.topcased.modeler.commands.CreateGraphEdgeCommand;
import org.topcased.modeler.commands.DeleteGraphElementCommand;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.DiagramElement;
import org.topcased.modeler.di.model.EMFSemanticModelBridge;
import org.topcased.modeler.di.model.GraphEdge;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.utils.Utils;

/**
 * This command is used to refresh the graphical representation of any subcomponent
 * of a Component instantiation.
 * 
 */
public class RefreshConnectionsCommand extends AbstractRefreshCommand {

    public RefreshConnectionsCommand(	final Diagram p_diagram,
    									final SKHierarchicalObject p_component ) 
    throws CoreException {
    	super( p_diagram, p_component );
    }

	/**
     * The Constructor
     * 
     * @param modeler the modeler
     * @param component the component model object
     */
    public RefreshConnectionsCommand(	final Modeler p_modeler,
    									final SKHierarchicalObject p_object ) 
    throws CoreException {
        super( p_modeler, p_object );
    }
    
    private void mergeRelation( final Relation p_relationSrc,
    							final Relation p_relationTarget ) {
    	if ( p_relationSrc != p_relationTarget ) {
	    	p_relationTarget.getObjects().clear();
	    	final Component componentImpl = (Component) p_relationTarget.eContainer();
	    	
	    	for ( final SKObject connEnd : p_relationSrc.getObjects() ) {
	    		p_relationTarget.getObjects().add( findClonedEnd( connEnd, componentImpl ) );
	    	}
    	}
    }
    
    private Relation cloneRelation( final Relation p_relation,
    								final Component p_componentImpl ) {
    	final Relation clonedRelation = EcoreUtil.copy( p_relation );
    	clonedRelation.getObjects().clear();

    	for ( final SKObject connEnd : p_relation.getObjects() ) {
    		clonedRelation.getObjects().add( findClonedEnd( connEnd, p_componentImpl ) );
    	}

    	return clonedRelation;
    }
    
    private SKObject findClonedEnd( final SKObject p_connEnd,
    								final Component p_componentImpl ) {
    	if ( p_connEnd instanceof Feature ) {
    		final SKComponent featComp = ( (Feature) p_connEnd ).getComponent();
    		
    		if ( featComp instanceof Component ) {
    			final Component comp = (Component) featComp;
    			
    			if ( comp.isImplementation() ) {
        			return ADELEModelUtils.findElementByName( p_connEnd, p_componentImpl.getFeatures() );
    			}
    			
    			// Find the subcomponent first
    			final SKHierarchicalObject subcompo = ADELEModelUtils.findElementByName( featComp, p_componentImpl.getChildren() );
    			
    			if ( subcompo instanceof SKComponent ) {
        			return  ADELEModelUtils.findElementByName( p_connEnd, ( (SKComponent) subcompo ).getFeatures() );
    			}
    		}
    		else if ( featComp instanceof SKComponent ) { // Such as FeatureGroup
    			return  ADELEModelUtils.findElementByName( p_connEnd, ( (SKComponent) featComp ).getFeatures() );
    		}
    	}
    	else if ( p_connEnd instanceof Component ) {
    		// Subcomponent access
			return ADELEModelUtils.findElementByName( p_connEnd, p_componentImpl.getChildren() );
    	}
    	
    	throw new IllegalArgumentException( "Unknown type of connection end." );
    }
    
    private void mergeRelation( final Relation p_relation,
    							final Component p_componentImpl ) {
    	final Relation relTarget = ADELEModelUtils.findElementByName( p_relation, p_componentImpl.getAllRelations() );
    	
    	if ( relTarget == null ) {
    		final Relation clonedRelation = cloneRelation( p_relation, p_componentImpl );

    		if ( clonedRelation instanceof DataAccessConnection ) {
    			p_componentImpl.getDataAccessConnection().add( (DataAccessConnection) clonedRelation );
    		}
    		else if ( clonedRelation instanceof BusAccessConnection ) {
    			p_componentImpl.getBusAccessConnection().add( (BusAccessConnection) clonedRelation );
    		}
    		else if ( clonedRelation instanceof DataPortConnection ) {
    			p_componentImpl.getDataPortConnection().add( (DataPortConnection) clonedRelation );
    		}
    		else if ( clonedRelation instanceof EventDataPortConnection ) {
    			p_componentImpl.getEventDataPortConnection().add( (EventDataPortConnection) clonedRelation );
    		}
    		else if ( clonedRelation instanceof EventPortConnection ) {
    			p_componentImpl.getEventPortConnection().add( (EventPortConnection) clonedRelation );
    		}
    		else if ( clonedRelation instanceof FeatureGroupConnection ) {
    			p_componentImpl.getFeatureGroupConnection().add( (FeatureGroupConnection) clonedRelation );
    		}
    		else if ( clonedRelation instanceof ParameterConnection ) {
    			p_componentImpl.getParameterConnection().add( (ParameterConnection) clonedRelation );
    		}
    		else if ( clonedRelation instanceof SubprogramAccessConnection ) {
    			p_componentImpl.getSubprogramAccessConnection().add( (SubprogramAccessConnection) clonedRelation );
    		}
    		else if ( clonedRelation instanceof SubprogramGroupAccessConnection ) {
    			p_componentImpl.getSubprogramGroupAccessConnection().add( (SubprogramGroupAccessConnection) clonedRelation );
    		}
    		else if ( clonedRelation instanceof AbstractFeatureConnection ) {
    			p_componentImpl.getAbstractFeatureConnection().add( (AbstractFeatureConnection) clonedRelation );
    		}
    		else {
    			throw new IllegalArgumentException( "Unknown type of connection." );
    		}
    	}
    	else {
    		mergeRelation( p_relation, relTarget );
    	}
    }

    /**
     * Refresh the Subcomponents of a given GraphNode. Refresh all the GraphNodes
     * linked with the model object associated with the GraphNode.
     * 
     * @param componentNode
     */
    protected void createRefreshCommands() {
    	if ( getRootComponent() instanceof Component ) {
    		final Component adeleCompo = (Component) getRootComponent();
    		
    		for ( final Relation origRelation : adeleCompo.getAllOriginalRelations() ) {
    			mergeRelation( origRelation, adeleCompo );
    		}
    		
    		final GraphNode currentNode = getGraphicalRepresentation( adeleCompo, getActiveDiagram() );
    	
	    	// Delete the connections that have been removed
			for ( final DiagramElement children : currentNode.getContained() ) {
	            if ( children instanceof GraphEdge ) {
	            	final GraphEdge edge = (GraphEdge) children;
	            	if ( edge.getSemanticModel() instanceof EMFSemanticModelBridge ) {
		                final EObject obj = Utils.getElement( edge );
		                
		                // DB: Delete graphical elements whose semantic element is a proxy (does not exist anymore)
		                if ( 	obj == null || obj.eIsProxy() ||
		                		( obj instanceof SKRelation && !adeleCompo.getAllRelations().contains( obj ) ) ) {
		                    add( new DeleteGraphElementCommand( edge, true ) );
		                }
		                else if ( obj instanceof SKRelation ) {
		        			// Recreate connections for all connections that do not have source and targets.
			                final EObject sourceConnectedElem = Utils.getElement( Utils.getSource( edge ) );
			                final EObject targetConnectedElem = Utils.getElement( Utils.getTarget( edge ) );
		                	final SKRelation relation = (SKRelation) obj;
		                	
		                	if ( !relation.getObjects().contains( sourceConnectedElem ) || !relation.getObjects().contains( targetConnectedElem ) ) {
			                    add( new DeleteGraphElementCommand( edge, true ) );
								createGraphConnection( currentNode, relation );
			                }
		                }
	            	}
	            }
			}
			
			// Create connections for all connections that do not have a graphical representation
			for ( final Relation connection : adeleCompo.getAllRelations() ) {
				if ( !hasGraphicalRepresentation( connection, currentNode ) ) {
					createGraphConnection( currentNode, connection );
				}
			}
    	}
    }
    
    private void createGraphConnection( final GraphNode p_parentGraphNode,
    									final SKRelation p_relation ) {
		final GraphNode nodeSource = (GraphNode) Utils.getGraphElement( p_parentGraphNode, p_relation.getObjects().get( 0 ), true );
		final GraphNode nodeTarget = (GraphNode) Utils.getGraphElement( p_parentGraphNode, p_relation.getObjects().get( 1 ), true );

		if ( nodeSource != null && nodeTarget != null ) {
			final GraphEdge newEdge = (GraphEdge) createGraphElement( p_relation );
			final CreateGraphEdgeCommand cmd = new CreateGraphEdgeCommand( 	getEditDomain(), 
												    						newEdge,
																			nodeSource, 
																			true );
			cmd.setTarget( nodeTarget );
            add( cmd );
		}
    }

    private boolean hasGraphicalRepresentation( final Relation p_relation,
    											final GraphNode p_componentNode ) {
    	return Utils.getGraphElement( p_componentNode, p_relation, true ) != null;
    }
}
