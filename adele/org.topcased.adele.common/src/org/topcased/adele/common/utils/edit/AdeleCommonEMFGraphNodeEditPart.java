package org.topcased.adele.common.utils.edit;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.Request;
import org.eclipse.gef.Tool;
import org.eclipse.jface.action.IAction;
import org.topcased.adele.common.utils.actions.ChangeActiveDiagramAction;
import org.topcased.adele.common.utils.actions.CreateDiagramAction;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.diagrams.model.util.DiagramsUtils;
import org.topcased.modeler.edit.EMFGraphNodeEditPart;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.editor.ModelerGraphicalViewer;
import org.topcased.modeler.extensions.DiagramDescriptor;
import org.topcased.modeler.extensions.DiagramsManager;

public class AdeleCommonEMFGraphNodeEditPart extends EMFGraphNodeEditPart {

	public AdeleCommonEMFGraphNodeEditPart(GraphNode obj) {
		super(obj);
	}

	@Override
	public void performRequest(Request request){
		if (request.getType().equals(REQ_OPEN)){

			EObject modelObject=getEObject();
			EditPartViewer viewer=getViewer();

			if (viewer instanceof ModelerGraphicalViewer  )
			{

				// save the active tool
				Tool activeTool = viewer.getEditDomain().getActiveTool();

				// deactivate the active tool
				viewer.getEditDomain().setActiveTool(null);
				viewer.getEditDomain().getPaletteViewer().setActiveTool(null);

				// The action to execute if a the active diagram must change
				IAction action = createChangeDiagramAction(viewer, modelObject);

				if (action != null)
				{
					action.run();
				}
				else
				{
					// restore the active tool
					viewer.getEditDomain().setActiveTool(activeTool);
				}
			}
		}
		else
			super.performRequest(request);
	}

    protected IAction createChangeDiagramAction(EditPartViewer viewer, EObject modelObject)
    {
        Modeler editor = ((ModelerGraphicalViewer) viewer).getModelerEditor();

        IAction action = null;
        // We search if a diagram already exists and let the user choose if at
        // least two exist.
        List<Diagram> existingDiagramList = DiagramsUtils.findAllExistingDiagram(editor.getDiagrams(), modelObject);
        if (existingDiagramList != null && existingDiagramList.size() > 0)
        {
            Diagram selectedDiagram = existingDiagramList.get(0);
            if (selectedDiagram != null)
            {
                action = new ChangeActiveDiagramAction(editor, selectedDiagram, modelObject);
            }
        }
        else
            action = createNewDiagram(editor, modelObject);

        return action;
    }
	
    protected IAction createNewDiagram(Modeler editor, EObject modelObject)
    {
        List<DiagramDescriptor> diagramDescriptorsList = new ArrayList<DiagramDescriptor>();

        // Retrieve the DiagramDescriptors that can be created into this EObject
        DiagramDescriptor[] diagramDescriptors = DiagramsManager.getInstance().getDiagrams();
        for (int i = 0; i < diagramDescriptors.length; i++)
        {
            if (diagramDescriptors[i].canCreateDiagramOn(modelObject, editor.getId()))
            {
                diagramDescriptorsList.add(diagramDescriptors[i]);
            }
        }

        // at least one Diagram is available
        if (diagramDescriptorsList.size() == 1)
        {
            return new CreateDiagramAction(editor, modelObject, (DiagramDescriptor) diagramDescriptorsList.get(0));
        }
        return null;
    }
//    
//    @Override
//	protected IFigure createFigure() {
//    	final IFigure figure = super.createFigure();
//    	
//    	final EObject modelObject = getEObject();
//    	
//    	if ( 	modelObject instanceof SKHierarchicalObject &&
//    			!ADELEModelUtils.isOriginalElement( (SKHierarchicalObject) modelObject ) ) {
//    		Display display = Display.getCurrent();
//    		
//    		if (display == null) {
//    			display = Display.getDefault();
//    		}
//        	
//    		//figure.setForegroundColor( display.getSystemColor( SWT.COLOR_GRAY ) );
//    	}
//    	
//    	return figure;
//    }
}
