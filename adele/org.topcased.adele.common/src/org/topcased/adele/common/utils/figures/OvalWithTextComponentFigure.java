/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE. All rights reserved. This program and the
 * accompanying materials are made available under the terms of the Eclipse
 * Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: David Sciamma (Anyware Technologies), Mathieu Garcia (Anyware
 * Technologies), Jacques Lescot (Anyware Technologies) - initial API and
 * implementation
 ******************************************************************************/
package org.topcased.adele.common.utils.figures;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.topcased.draw2d.figures.Label;

/**
 * A figure that represent a text with an Oval around as borders
 * 
 * Creation : 20 janv. 2006
 * 
 * @author <a href="mailto:jacques.lescot@anyware-tech.com">Jacques LESCOT</a>
 */
public class OvalWithTextComponentFigure extends Figure
{
    private static final int HORIZONTAL_MARGIN = 2;

    private static final int TEXT_HEIGHT = 8;

    private Label label;

    /**
     * Constructor
     */
    public OvalWithTextComponentFigure()
    {
        setLayoutManager(new BorderLayout());

        label = new Label();
        add(label, BorderLayout.CENTER);
    }

    /**
     * Set the text to display inside de Oval
     * 
     * @param newText the newText
     */
    public void setText(String newText)
    {
        label.setText(newText);
    }

    /**
     * @see org.eclipse.draw2d.Figure#paintBorder(org.eclipse.draw2d.Graphics)
     */
    protected void paintBorder(Graphics graphics)
    {
        super.paintBorder(graphics);

        graphics.pushState();
        graphics.drawOval(new Rectangle(bounds.getLeft().x, bounds.getTop().y, bounds.width - 1, bounds.height - 1));
        graphics.popState();
    }

    /**
     * @see org.eclipse.draw2d.Figure#paintFigure(org.eclipse.draw2d.Graphics)
     */
    protected void paintFigure(Graphics graphics)
    {
        super.paintFigure(graphics);

        graphics.pushState();
        graphics.fillOval(new Rectangle(bounds.getLeft().x, bounds.getTop().y, bounds.width, bounds.height));

        FontData miniFontData = label.getFont().getFontData()[0];
        miniFontData.setHeight(TEXT_HEIGHT);
        Font miniFont = null;
        if (miniFontData != null)
        {
            FontData[] fontDataList = new FontData[1];
            fontDataList[0] = miniFontData;
            JFaceResources.getFontRegistry().put(StringConverter.asString(miniFontData), fontDataList);
            miniFont = JFaceResources.getFontRegistry().get(StringConverter.asString(miniFontData));
        }
        label.setFont(miniFont);

        graphics.popState();
    }

    /**
     * Override the method to provide space between the text and the border
     * 
     * @see org.eclipse.draw2d.Figure#getPreferredSize(int, int)
     */
    public Dimension getPreferredSize(int wHint, int hHint)
    {
        Dimension optimalDim = super.getPreferredSize(wHint, hHint);
        if (wHint == -1)
        {
            optimalDim.width = optimalDim.width + HORIZONTAL_MARGIN * 2;
        }
        return optimalDim;
    }
}
