/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/

package org.topcased.adele.common.utils.figures;

import org.eclipse.draw2d.Figure;

/**
 * A Figure common to all the Component Figure in AADL
 * 
 * Creation : 10 nov. 2005
 * 
 * @author <a href="mailto:jacques.lescot@anyware-tech.com">Jacques LESCOT</a>
 */
public class ComponentFigure extends Figure
{
    /** The width used to draw the border of the figure */
    private int lineWidth = 1;

    /**
     * Get the lineWidth that must be used
     * 
     * @return the width of the line
     */
    public int getLineWidth()
    {
        return lineWidth;
    }

    /**
     * Set the new lineWidth
     * 
     * @param newLineWidth the new width
     */
    public void setLineWidth(int newLineWidth)
    {
        this.lineWidth = newLineWidth;
    }
}
