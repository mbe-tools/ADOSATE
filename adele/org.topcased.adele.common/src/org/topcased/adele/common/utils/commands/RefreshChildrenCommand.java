package org.topcased.adele.common.utils.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Translatable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.topcased.adele.model.ADELE.util.ADELEModelUtils;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Components.Package;
import org.topcased.adele.model.ADELE_Features.FeatureGroup;
import org.topcased.adele.model.KernelSpices.SKComponent;
import org.topcased.adele.model.KernelSpices.SKFeature;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.modeler.commands.CreateGraphNodeCommand;
import org.topcased.modeler.commands.DeleteGraphElementCommand;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.DiagramElement;
import org.topcased.modeler.di.model.EMFSemanticModelBridge;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.utils.Utils;

/**
 * This command is used to refresh the graphical representation of any subcomponent
 * of a Component instantiation.
 * 
 */
public class RefreshChildrenCommand extends AbstractRefreshCommand {

	/**
     * The Constructor
     * 
     * @param modeler the modeler
     * @param component the component model object
     */
    public RefreshChildrenCommand(	final Modeler p_modeler,
    								final SKHierarchicalObject p_component ) 
    throws CoreException {
        super( p_modeler, p_component );
    }

    public RefreshChildrenCommand(	final Diagram p_diagram,
    								final SKHierarchicalObject p_component ) 
    throws CoreException {
    	super( p_diagram, p_component );
    }
   
    private Collection<SKHierarchicalObject> getAllOriginalChildren( final SKHierarchicalObject p_parent ) {
    	final Collection<SKHierarchicalObject> children = new ArrayList<SKHierarchicalObject>();

    	if ( p_parent instanceof Component ) {
        	final Component origComp = ( (Component) p_parent ).getOriginalComponent();
        	children.addAll( origComp.getAllRealChildrenComponents() );
    	}
    	else {
    		children.addAll( p_parent.getChildren() );
    	}
    	
    	return children;
    }

    private Collection<SKHierarchicalObject> getDisplayableChildren( final SKHierarchicalObject p_parent ) {
    	final Collection<SKHierarchicalObject> children = new ArrayList<SKHierarchicalObject>();
    	
		if ( 	!( p_parent instanceof Component ) ||
				!( (Component) p_parent ).isImplementation() || !( getRootComponent() instanceof Package ) ) {
   			children.addAll( p_parent.getChildren() );

   			if ( !activeOnRealElements && p_parent instanceof Component ) {
   				children.removeAll( ( (Component) p_parent ).getAllRealChildrenComponents() );
			}
		}
    	
    	return children;
    }
    
    private void removeConnections( final SKHierarchicalObject p_object ) {
    	if ( p_object instanceof Component ) {
    		final Component compo = (Component) p_object;
    		
    		if ( compo.isSubcomponent() ) {
    			for ( final SKFeature feature : compo.getFeatures() ) {
    				removeConnections( feature );
    			}
    		}
    	}
    }
    
    private void mergeChildren( final SKHierarchicalObject p_parent,
    							final Collection<SKHierarchicalObject> p_originalChildren ) {
    	final Iterator<SKHierarchicalObject> existChildrenIt = p_parent.getChildren().iterator();
    	
    	while ( existChildrenIt.hasNext() ) {
    		final SKHierarchicalObject existChildren = existChildrenIt.next();
    		final SKHierarchicalObject origChild = ADELEModelUtils.findElementByName( existChildren, p_originalChildren );
    		
    		if ( origChild == null ) {
    			removeConnections( existChildren );
    			existChildrenIt.remove();
    		}
    	}

    	for ( final SKHierarchicalObject originalChildren : p_originalChildren ) {
    		final SKHierarchicalObject existingChildren = ADELEModelUtils.findElementByName( originalChildren, p_parent.getChildren() );
    		
    		if ( existingChildren == null ) {
    			p_parent.getChildren().add( cloneChildren( originalChildren ) );
    		}
    		else {	
    			mergeChildren( originalChildren, existingChildren );
    		}
    	}
    }
    
    private SKHierarchicalObject cloneChildren( final SKHierarchicalObject p_children ) {
    	final SKHierarchicalObject clone = EcoreUtil.copy( p_children );
//    	clone.setId( EcoreUtil.generateUUID() );
    	
    	if ( clone instanceof Component ) {
    		final Component clonedCompo = (Component) clone;
    		
//    		for ( final SKFeature feature : clonedCompo.getFeatures() ) {
//    			feature.setId( EcoreUtil.generateUUID() );
//    		}
    		
    		clonedCompo.setFeaturesLock( true );
    		clonedCompo.setSubcomponentsLock( true );
    		
    		// Important: must use the original children because isSubcomponent is derived and depends on containment hierarchy.
    		if ( ( (Component) p_children ).isSubcomponent() ) {
    			clonedCompo.setSubcomponentRefinement( true );
    		}
    	}
    	
    	return clone;
    }
    
    private void mergeChildren( final SKHierarchicalObject p_sourceChildren,
    							final SKHierarchicalObject p_targetChildren) {
    	if ( p_sourceChildren instanceof Component && p_targetChildren instanceof Component ) {
    		final Component sourceCompo = (Component) p_sourceChildren;
    		final Component targetCompo = (Component) p_targetChildren;
			
   			targetCompo.setType( sourceCompo.getType() );
			targetCompo.setImplementation( sourceCompo.getImplementation() );
			targetCompo.setImplementationName( sourceCompo.getImplementationName() );
    	}	
    }
    
    private boolean isManaged( final EObject p_object ) {
    	if ( p_object instanceof SKComponent ) {
    		if ( p_object instanceof FeatureGroup ) {
    			return ( (FeatureGroup) p_object ).isType();
    		}
    		
    		return true;
    	}
    	
    	return false;
    }

    /**
     * Refresh the Subcomponents of a given GraphNode. Refresh all the GraphNodes
     * linked with the model object associated with the GraphNode.
     * 
     * @param componentNode
     */
    @Override
    protected void createRefreshCommands() {
    	final SKHierarchicalObject rootComponent = getRootComponent();
		mergeChildren( rootComponent, getAllOriginalChildren( rootComponent ) );
		final Collection<SKHierarchicalObject> components = rootComponent.getChildren();
		final GraphNode currentNode = getGraphicalRepresentation( rootComponent, getActiveDiagram() );
	
    	// Delete the graph nodes that have been removed
		for ( final DiagramElement children : currentNode.getContained() ) {
            if ( children instanceof GraphNode ) {
            	final GraphNode node = (GraphNode) children;

            	// Avoid deleting notes
            	if ( node.getSemanticModel() instanceof EMFSemanticModelBridge ) {
                    final EObject obj = Utils.getElement( node );
                    
                    // Delete graphical elements whose semantic element is a proxy (does not exist anymore)
                    if ( obj == null || obj.eIsProxy() || ( isManaged( obj ) && !components.contains( obj ) ) ) {
                        add( new DeleteGraphElementCommand( node, true ) );
                    }
            	}
            }
		}
		
		int offSet = 5;
		
		// Create graph node for all components that do not have a graphical representation
		for ( final SKHierarchicalObject component : components ) {
			offSet = createCreateGraphNodeCommand( rootComponent, component, currentNode, offSet );
		}
    }
    
    private int createCreateGraphNodeCommand( 	final SKHierarchicalObject p_parent,
    											final SKHierarchicalObject p_children,
    											final GraphNode p_parentNode,
    											int pi_offSet ) {
    	final Collection<SKHierarchicalObject> displayableChildren = getDisplayableChildren( p_parent );
    	
    	if ( displayableChildren.contains( p_children ) ) {
	    	GraphNode node = getGraphicalRepresentation( p_children, p_parentNode );
	    	
			if ( node == null ) {
				node = createGraphElement( p_children );
		
				final Translatable[] graphProps = determineGraphicalProperties( p_children, pi_offSet );
				final Dimension size = (Dimension) graphProps[ 0 ];
				final Point position = (Point) graphProps[ 1 ];
				
				final CreateGraphNodeCommand cmd = new CreateGraphNodeCommand( 	getEditDomain(),
																				node,
																				p_parentNode,
																				position,
																				size,
																				PositionConstants.NONE );
				add( cmd );
				pi_offSet = pi_offSet + 10;
			}
    	}
		
		return pi_offSet;
    }
    
    private Translatable[] determineGraphicalProperties( 	final SKHierarchicalObject p_hierObj,
    														final int offSet ) {
    	Dimension size = null;
    	Point position = null;
    	SKComponent origComp = null;
    	
    	if ( p_hierObj instanceof Component ) {
    		final Component compo = (Component) p_hierObj;
    		origComp = compo.getOriginalComponent();
    		
    		if ( origComp == null ) {
    			origComp = compo.getRefines();
    		}
    	}
    	else if ( p_hierObj instanceof FeatureGroup ) {
    		final FeatureGroup featGroup = (FeatureGroup) p_hierObj;
    		
    		if ( featGroup.isType() ) {
    			origComp = (SKComponent) featGroup.getRefinedFeature();
    			
    			if ( origComp == null ) {
    				origComp = featGroup.getInverseFeature();
    			}
    		}
    	}
    	
		if ( origComp != null ) {
			// Search for the graphical properties of the inherited subcomponent
			final GraphNode refinedGraphNode = searchExistingGraphNode( origComp );
			
			if ( refinedGraphNode != null ) {
				size = new Dimension( refinedGraphNode.getSize() );
				position = new Point( refinedGraphNode.getPosition() );
			}
		}

		if ( size == null ) {
    		size = new Dimension( 50, 50 );
    	}

    	if ( position == null ) {
    		position = new Point( offSet, offSet );
    	}
    	
    	return new Translatable[] { size, position } ;
    }
    
    @Override
	protected GraphNode createGraphElement( final EObject p_semElement ) {
    	return (GraphNode) super.createGraphElement( p_semElement );
    }
}
