/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies), Mathieu Garcia (Anyware Technologies),
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/
package org.topcased.adele.common.utils.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.XYLayout;
import org.topcased.draw2d.figures.EditableLabel;
import org.topcased.draw2d.figures.IContainerFigure;
import org.topcased.draw2d.figures.ILabel;
import org.topcased.draw2d.figures.ILabelFigure;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @author jako
 * @generated
 */
public class ComponentContainerFigure extends Figure implements ILabelFigure, IContainerFigure
{
    private ILabel header;

    private IFigure contentPane;

    /**
     * Constructor
     */
    public ComponentContainerFigure()
    {
        GridLayout gridLayout = new GridLayout();
        gridLayout.verticalSpacing = 0;
        gridLayout.horizontalSpacing = 0;
        gridLayout.marginWidth = 0;
        gridLayout.marginHeight = 0;
        setLayoutManager(gridLayout);

        header = createHeader();
        add(header, new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_CENTER));

        contentPane = createContainer();
        add(contentPane, new GridData(GridData.FILL_BOTH));
    }

    /**
     * Creates the header figure
     * 
     * @return the header figure
     */
    protected ILabel createHeader()
    {
        EditableLabel label = new EditableLabel();
        label.setAlignment(PositionConstants.LEFT);

        return label;
    }

    /**
     * Creates the container figure
     * 
     * @return the container figure
     */
    protected IFigure createContainer()
    {
        Figure container = new Figure();
        container.setLayoutManager(new XYLayout());
        container.setOpaque(true);
        container.setBorder(new LineBorder(1));
        return container;
    }

    /**
     * @see org.topcased.draw2d.figures.IContainerFigure#getContentPane()
     */
    public IFigure getContentPane()
    {
        return contentPane;
    }

    /**
     * @see org.topcased.draw2d.figures.ILabelFigure#getLabel()
     */
    public ILabel getLabel()
    {
        return header;
    }
}
