package org.topcased.adele.common.utils.commands;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.commands.CompoundCommand;
import org.topcased.adele.model.ADELE_Components.Component;
import org.topcased.adele.model.ADELE_Relations.AbstractFeatureConnection;
import org.topcased.adele.model.ADELE_Relations.BusAccessConnection;
import org.topcased.adele.model.ADELE_Relations.DataAccessConnection;
import org.topcased.adele.model.ADELE_Relations.DataPortConnection;
import org.topcased.adele.model.ADELE_Relations.EventDataPortConnection;
import org.topcased.adele.model.ADELE_Relations.EventPortConnection;
import org.topcased.adele.model.ADELE_Relations.FeatureGroupConnection;
import org.topcased.adele.model.ADELE_Relations.ParameterConnection;
import org.topcased.adele.model.ADELE_Relations.Relation;
import org.topcased.adele.model.ADELE_Relations.SubprogramAccessConnection;
import org.topcased.adele.model.ADELE_Relations.SubprogramGroupAccessConnection;
import org.topcased.adele.model.KernelSpices.SKFeature;
import org.topcased.adele.model.KernelSpices.SKHierarchicalObject;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.GraphElement;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.diagrams.model.Diagrams;
import org.topcased.modeler.diagrams.model.util.DiagramsUtils;
import org.topcased.modeler.editor.ICreationUtils;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.extensions.DiagramsManager;
import org.topcased.modeler.utils.Utils;

/**
 * This command is used to refresh the graphical representation of any subcomponent
 * of a Component instantiation.
 * 
 */
public abstract class AbstractRefreshCommand extends CompoundCommand {
	
	public static boolean activeOnRealElements = true;
    
	/** The modeler */
    //private final Modeler modeler;

	/** The CreationUtils factory to use for creating GraphElements */
    private final ICreationUtils creationUtils;
    
    private final SKHierarchicalObject rootComponent;
    
    private final Diagram activeDiagram;
    
    private final EditDomain editDomain;


	/**
     * The Constructor
     * 
     * @param modeler the modeler
     * @param component the component model object
     */
    protected AbstractRefreshCommand(	final Modeler p_modeler,
										final SKHierarchicalObject p_component )
    throws CoreException {
    	this( 	(EditDomain) p_modeler.getAdapter( EditDomain.class ),
				p_modeler.getActiveDiagram(),
				p_component );
    }

    protected AbstractRefreshCommand(	final Diagram p_diagram,
										final SKHierarchicalObject p_component ) 
	throws CoreException {
    	this( null, p_diagram, p_component );
    }

    private AbstractRefreshCommand(	final EditDomain p_editDomain,
    								final Diagram p_diagram,
    								final SKHierarchicalObject p_component ) 
    throws CoreException {
        super( "Refresh Command" );
        
        //modeler = p_modeler;
        creationUtils = DiagramsManager.getInstance().getConfiguration( p_diagram ).getCreationUtils();//modeler.getActiveConfiguration().getCreationUtils();
        editDomain = p_editDomain;
        rootComponent = p_component;
        activeDiagram = p_diagram;//modeler.getActiveDiagram();
        createRefreshCommands();
    }

    /**
     * Refresh the Subcomponents of a given GraphNode. Refresh all the GraphNodes
     * linked with the model object associated with the GraphNode.
     * 
     * @param componentNode
     */
    protected abstract void createRefreshCommands();
    
    protected GraphNode searchExistingGraphNode( final EObject p_element ) {
    	final Diagram childDiag = getChildDiagramForSemanticElement( p_element );
    	
    	if ( childDiag != null ) {
    		final GraphElement graphElement = Utils.getGraphElement( childDiag, p_element );
    		
    		if ( graphElement instanceof GraphNode ) {
    			return (GraphNode) graphElement;
    		}
    	}
		
    	final Diagrams rootDiag = getDiagramsForSemanticElement( p_element );
		
		if ( rootDiag != null ) {
			final GraphElement[] graphElements = Utils.getGraphElements( rootDiag, p_element );
			
			if ( graphElements != null ) {
				for ( final GraphElement graphEleme : graphElements ) {
					if ( graphEleme instanceof GraphNode ) {
						return (GraphNode) graphEleme;
					}
				}
			}
		}
		
		return null;
    }

    private Diagram getChildDiagramForSemanticElement( final EObject p_element ) {
		final List<Diagram> existingDiagramList = DiagramsUtils.findAllExistingDiagram( (Diagrams) EcoreUtil.getRootContainer( activeDiagram ), p_element );
		
		if ( !existingDiagramList.isEmpty() ) {
			return existingDiagramList.get( 0 );
		}
		
		return null;
    }
    
    private Diagrams getDiagramsForSemanticElement( final EObject p_element ) {
		if ( p_element == null || p_element.eResource() == null ) {
			return null;
		}
		
    	final URI diagUri = URI.createPlatformResourceURI( p_element.eResource().getURI().toPlatformString(true) +"di", true );
    	final ResourceSet resSet = activeDiagram.eResource().getResourceSet();//modeler.getResourceSet();
    	
    	if ( resSet.getURIConverter().exists( diagUri, null ) ) {
	    	final Resource diagRes = resSet.getResource( diagUri, true );
	    	
	    	if ( diagRes.getContents().isEmpty() ) {
	    		return null;
	    	}
	    	
	    	return (Diagrams) diagRes.getContents().get( 0 );
    	}
    	
    	return null;
    }
    
    protected GraphNode getGraphicalRepresentation( final SKHierarchicalObject p_element,
    												final GraphNode p_componentNode ) {
   		if ( Utils.getElement( p_componentNode ) == p_element ) {
   			return p_componentNode;
    	}

   		final GraphElement graphElem = Utils.getGraphElement( p_componentNode, p_element, true );
    	
    	if ( graphElem instanceof GraphNode ) {
        	return (GraphNode) graphElem;
    	}
    	
    	return null;
    }
//
//    protected Modeler getModeler() {
//		return modeler;
//	}

	protected GraphElement createGraphElement( final EObject p_semElement ) {
		return creationUtils.createGraphElement( p_semElement );
	}

	protected SKHierarchicalObject getRootComponent() {
		return rootComponent;
	}

	protected Diagram getActiveDiagram() {
		return activeDiagram;
	}
	
	protected EditDomain getEditDomain() {
		return editDomain;//(EditDomain) modeler.getAdapter( EditDomain.class );
	}
    
	protected void removeConnections( final SKFeature p_feature ) {
        // DB: Also delete the connections. This was not handled in Adele 1
		final Component compo = (Component) p_feature.getComponent();
		
		if ( compo.isSubcomponent() ) {
			final Component implem = (Component) compo.getParent();
    		final Iterator<Relation> relationsIt = implem.getAllRelations().iterator();
    		
        	while ( relationsIt.hasNext() ) {
        		final Relation conn = relationsIt.next();
        		
        		for ( final Object end : conn.getObjects() ) {
        			if ( end == p_feature ) {
        	    		if ( conn instanceof DataAccessConnection ) {
        	    			implem.getDataAccessConnection().remove( (DataAccessConnection) conn );
        	    		}
        	    		else if ( conn instanceof BusAccessConnection ) {
        	    			implem.getBusAccessConnection().remove( (BusAccessConnection) conn );
        	    		}
        	    		else if ( conn instanceof DataPortConnection ) {
        	    			implem.getDataPortConnection().remove( (DataPortConnection) conn );
        	    		}
        	    		else if ( conn instanceof EventDataPortConnection ) {
        	    			implem.getEventDataPortConnection().remove( (EventDataPortConnection) conn );
        	    		}
        	    		else if ( conn instanceof EventPortConnection ) {
        	    			implem.getEventPortConnection().remove( (EventPortConnection) conn );
        	    		}
        	    		else if ( conn instanceof FeatureGroupConnection ) {
        	    			implem.getFeatureGroupConnection().remove( (FeatureGroupConnection) conn );
        	    		}
        	    		else if ( conn instanceof ParameterConnection ) {
        	    			implem.getParameterConnection().remove( (ParameterConnection) conn );
        	    		}
        	    		else if ( conn instanceof SubprogramAccessConnection ) {
        	    			implem.getSubprogramAccessConnection().remove( (SubprogramAccessConnection) conn );
        	    		}
        	    		else if ( conn instanceof SubprogramGroupAccessConnection ) {
        	    			implem.getSubprogramGroupAccessConnection().remove( (SubprogramGroupAccessConnection) conn );
        	    		}
        	    		else if ( conn instanceof AbstractFeatureConnection ) {
        	    			implem.getAbstractFeatureConnection().remove( (AbstractFeatureConnection) conn );
        	    		}
        	    		else {
        	    			throw new IllegalArgumentException( "Unknown type of connection." );
        	    		}

        	    		break;
        			}
        		}
            }
		}
    }
}
