/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies), Mathieu Garcia (Anyware Technologies),
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/
package org.topcased.adele.common.utils.actions;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.topcased.adele.common.utils.edit.AdeleCommonDiagramEditPart;
import org.topcased.modeler.commands.CreateDiagramCommand;
import org.topcased.modeler.commands.CreateDiagramsCommand;
import org.topcased.modeler.edit.DiagramsRootEditPart;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.extensions.DiagramDescriptor;

/**
 * Create a new Diagram linked with a model object. <br>
 * 
 * @author jako
 */
public class CreateDiagramAction extends Action
{

    protected DiagramDescriptor diagramDescriptor;

    protected EObject object;

    protected Modeler editor;

    protected boolean initializeContent;

    /**
     * Constructor
     * 
     * @param ed the Modeler object
     * @param obj the model object associated with the diagram
     * @param diagram the DiagramDescriptor associated with the diagram to create
     */
    public CreateDiagramAction(Modeler ed, EObject obj, DiagramDescriptor diagram)
    {
        this(ed, obj, diagram, false);
    }

    /**
     * Constructor
     * 
     * @param ed the Modeler object
     * @param obj the model object associated with the diagram
     * @param diagram the DiagramDescriptor associated with the diagram to create
     * @param initialize if the children of the model objects must be imported graphically in the diagram
     */
    public CreateDiagramAction(Modeler ed, EObject obj, DiagramDescriptor diagram, boolean initialize)
    {
        super(diagram.getName());
        this.editor = ed;
        this.object = obj;
        this.diagramDescriptor = diagram;
        this.initializeContent = initialize;

        ImageDescriptor icon = this.diagramDescriptor.getIconDescriptor();
        if (icon != null)
        {
            setImageDescriptor(icon);
        }
    }

    /**
     * Execute the Action
     * 
     * @see org.eclipse.jface.action.IAction#run()
     */
    public void run() {
    	final DiagramsRootEditPart tmp = (DiagramsRootEditPart) editor.getRootEditPart();
    	final AdeleCommonDiagramEditPart diagramEditPart = (AdeleCommonDiagramEditPart) tmp.getContents();
//    	final MouseListener mouseListener = diagramEditPart.getMouseListener();
//    	
//    	// DB: Sometimes the listener is null and trying to remove it throws an exception
//    	if ( mouseListener != null ) {
//    		tmp.getFigure().removeMouseListener( mouseListener );
//    	}
    	
    	diagramEditPart.removeMouseListener();
        // The CreateDiagramsCommand should be first executed before the CreateDiagramCommand : indeed, the Diagrams
        // element should be first created when it does not exist yet.
        ((CommandStack) editor.getAdapter(CommandStack.class)).execute(new CreateDiagramsCommand(editor, object));
        ((CommandStack) editor.getAdapter(CommandStack.class)).execute(new CreateDiagramCommand(editor, object, diagramDescriptor, initializeContent));
    }
}
