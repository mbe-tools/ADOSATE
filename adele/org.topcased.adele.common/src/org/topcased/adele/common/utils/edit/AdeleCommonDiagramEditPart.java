package org.topcased.adele.common.utils.edit;

import java.util.EventObject;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.draw2d.Cursors;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseListener;
import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.command.CommandStackListener;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Tool;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.action.IAction;
import org.topcased.adele.common.utils.ADELEModeler;
import org.topcased.adele.common.utils.actions.ChangeActiveDiagramAction;
import org.topcased.adele.model.KernelSpices.impl.SKHierarchicalObjectImpl;
import org.topcased.modeler.commands.AdvancedCommandStack;
import org.topcased.modeler.commands.CreateGraphNodeCommand;
import org.topcased.modeler.di.model.Diagram;
import org.topcased.modeler.di.model.GraphNode;
import org.topcased.modeler.diagrams.model.util.DiagramsUtils;
import org.topcased.modeler.edit.DiagramEditPart;
import org.topcased.modeler.edit.DiagramsRootEditPart;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.editor.ModelerGraphicalViewer;

public abstract class AdeleCommonDiagramEditPart extends DiagramEditPart {
	
	protected CommandStackListener stackListener;
	protected CommandStack stack;
	protected InnerMouseListener mouseListener;

	/**
	 * The Constructor
	 *
	 * @param model the root model element
	 */
	public AdeleCommonDiagramEditPart( final Diagram model ) {
		super( model );
	}

	@Override
	public boolean isSelectable() {
		return false;
	}

	protected IAction changeDiagramAction(EObject modelObject, EditPart part) {
		Modeler editor = ((ModelerGraphicalViewer) part.getViewer())
				.getModelerEditor();

		IAction action = null;
		// We search if a parent diagram exists.

		List<Diagram> existingDiagramList;
		existingDiagramList= DiagramsUtils.findAllExistingDiagram(editor
					.getDiagrams(), modelObject.eContainer());
		if (existingDiagramList != null && existingDiagramList.size() > 0) {
			Diagram selectedDiagram = (Diagram) ((Diagram[]) existingDiagramList
					.toArray(new Diagram[existingDiagramList.size()]))[0];
			if (selectedDiagram != null) {
				action = new ChangeActiveDiagramAction(editor, selectedDiagram, modelObject);
			}
		}

		return action;
	}
	
	protected abstract void refreshDiagramFeatures()
	throws CoreException;
	
	protected void addNewChild(){
		final EObject eObject = getEObject();
		
		if ( !eObject.eIsProxy() ) {
			EList<EModelElement> nodes = ( (SKHierarchicalObjectImpl) eObject ).getNewChildDown();
			
			if ( nodes != null ) {
				final Iterator<EModelElement> nodeIt = nodes.iterator();
				
				while (nodeIt.hasNext()){
					GraphNode node = (GraphNode) nodeIt.next();
					Command cmd = new CreateGraphNodeCommand(getViewer().getEditDomain(),node,(GraphNode)getModel(),
							node.getPosition(), node.getSize(), null,false);
					cmd.execute();
				}
	
				stack = ((ModelerGraphicalViewer) getViewer()).getModelerEditor().getEditingDomain().getCommandStack();
				stackListener = new InnerChildDownCommandStackListener(this);
				stack.addCommandStackListener(stackListener);
	
				((SKHierarchicalObjectImpl) eObject ).setNewChildDown(null);
				
			}
	
			nodes = ((SKHierarchicalObjectImpl) eObject ).getNewChildUp();
			if (nodes != null) {
				Iterator<EModelElement> nodeIt=nodes.iterator();
				while (nodeIt.hasNext()){
					GraphNode node = (GraphNode) nodeIt.next();
					Command cmd = new CreateGraphNodeCommand(getViewer().getEditDomain(),node,(GraphNode)getModel(),
							node.getPosition(), node.getSize(), null,false);
					cmd.execute();
				}
	
				stack = ((ModelerGraphicalViewer) getViewer()).getModelerEditor().getEditingDomain().getCommandStack();
				stackListener = new InnerChildUpCommandStackListener(this);
				stack.addCommandStackListener(stackListener);
	
				((SKHierarchicalObjectImpl) eObject ).setNewChildUp(null);
				
			}
		}
	}
	
	protected abstract void refreshDiagramSubcomponents()
	throws CoreException;
	
	protected abstract void refreshDiagramConnections()
	throws CoreException;
	
	@Override
	protected IFigure createBodyFigure() {
		try {
			refreshDiagramSubcomponents();
			
			addNewChild();
			
			refreshDiagramFeatures();
			
			// DB: Added to also refresh the connections when the semantic model is changed outside the editor
			refreshDiagramConnections();
		}
		catch( final CoreException p_ex ) {
			p_ex.printStackTrace();
		}

		return null;
	}
	
	public void removeInnerStackListener(){
		stack.removeCommandStackListener(stackListener);
	}
	
	public void removeMouseListener() {
		if ( mouseListener != null ) {
			final EditPart parent = getParent();
			
			if ( parent instanceof DiagramsRootEditPart ) {
				final IFigure figure = ( (DiagramsRootEditPart) parent ).getFigure();
				
				if ( figure != null ) {
					figure.removeMouseListener( mouseListener );
				}
			}
			
			mouseListener = null;
		}
	}
	
	@Override
	public void setParent(EditPart parent) {
		removeMouseListener();
		
		super.setParent(parent);
		
		if ( parent!=null ) {
			mouseListener = new InnerMouseListener( parent );
			final IFigure figure = ( (DiagramsRootEditPart) parent ).getFigure();
			figure.addMouseListener( mouseListener );
		}
	}
	
	public MouseListener getMouseListener(){
		return mouseListener;
	}
	
	protected class InnerMouseListener implements MouseListener{

		protected EditPart parent;
		
		public InnerMouseListener(EditPart _parent){
			parent=_parent;
		}
		
		public void mouseDoubleClicked(MouseEvent arg0) {
			if (getParent()==null)
				setParent(parent);
			try { 
				setSelected(SELECTED_PRIMARY);
			} catch (Exception evt ) {
			}
			Tool activeTool = getViewer().getEditDomain().getActiveTool();

			// deactivate the active tool
			getViewer().getEditDomain().setActiveTool(null);
			getViewer().getEditDomain().getPaletteViewer().setActiveTool(null);

			// The action to execute if the active diagram must change
			IAction action = changeDiagramAction(getEObject(), parent);

			if (action != null) {
				action.run();
			} else {
				// restore the active tool
				getViewer().getEditDomain().setActiveTool(activeTool);
			}
			arg0.consume();
		}

		public void mousePressed(MouseEvent arg0) {
			if (!((Figure) getFigure().getChildren().get(1)).containsPoint(arg0.getLocation()))
				arg0.consume();
		}

		public void mouseReleased(MouseEvent arg0) {
		}
		
	}
	
	protected class InnerChildUpCommandStackListener implements CommandStackListener{

		protected AdeleCommonDiagramEditPart obj;
		
		public InnerChildUpCommandStackListener(AdeleCommonDiagramEditPart _obj) {
			obj=_obj;
		}
		
		public void commandStackChanged(EventObject arg0) {
			Command cmd = ((AdvancedCommandStack)arg0.getSource()).getUndoCommand();
			if (cmd.getLabel().equals("Create Diagram")) {
				if (obj.getViewer()!=null) {
					obj.getViewer().setCursor(Cursors.WAIT);
					((ADELEModeler)((ModelerGraphicalViewer) getViewer()).getModelerEditor()).saveMovedElements();
					obj.getViewer().setCursor(Cursors.ARROW);
				} else {
					((ADELEModeler)((ModelerGraphicalViewer) getViewer()).getModelerEditor()).saveMovedElements();
				}
				try{
					obj.removeInnerStackListener();
				}
				catch (Exception e){
					System.err.println("Oups: "+e.getMessage());
				}
			}
			
		}
	}
	
	protected class InnerChildDownCommandStackListener implements CommandStackListener{

		protected AdeleCommonDiagramEditPart obj;
		
		public InnerChildDownCommandStackListener(AdeleCommonDiagramEditPart _obj) {
			obj=_obj;
		}
		
		public void commandStackChanged(EventObject arg0) {
			Command cmd = ((AdvancedCommandStack)arg0.getSource()).getUndoCommand();
			if (cmd.getLabel().equals("Node replacement")) {
				cmd.undo();
				obj.getViewer().setCursor(Cursors.WAIT);
				((ADELEModeler)((ModelerGraphicalViewer) getViewer()).getModelerEditor()).saveMovedElements();
				obj.getViewer().setCursor(Cursors.ARROW);
				try{
					obj.removeInnerStackListener();
				}
				catch (Exception e){
					System.err.println("Oups: "+e.getMessage());
				}
			}
		}
	}
	
	protected class InnerConnectionRefreshCommandStackListener implements CommandStackListener{

		protected AdeleCommonDiagramEditPart obj;
		
		public InnerConnectionRefreshCommandStackListener(AdeleCommonDiagramEditPart _obj) {
			obj=_obj;
		}
		
		public void commandStackChanged(EventObject arg0) {
			Command cmd = ((AdvancedCommandStack)arg0.getSource()).getUndoCommand();
			// DB: Avoid null pointer exceptions
			if ( cmd != null && "Create Diagram".equals( cmd.getLabel() ) ) {
//			if (cmd.getLabel().equals("Create Diagram")) {
				if ( getViewer() != null && ((ModelerGraphicalViewer) getViewer()).getModelerEditor() != null ) {
					((ModelerGraphicalViewer) getViewer()).getModelerEditor().refreshActiveDiagram();
				}
				
//				try{
					obj.removeInnerStackListener();
//				}
//				catch (Exception e){
//					System.err.println("Oups: "+e.getMessage());
//				}
			}
			
		}
	}
}