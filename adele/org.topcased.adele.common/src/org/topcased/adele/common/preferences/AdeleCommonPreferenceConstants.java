package org.topcased.adele.common.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class AdeleCommonPreferenceConstants {

	public static final String ADELE_DATABASE_PATH = "databasePreference";

	public static final String ADELE_LMP_PATH = "LMPPreference";

	public static final String ADELE_AADLKW_CASE = "AADLKWCase";

	public static final String ADELE_AADLKW_DEFAULT_CASE = "Upper";

	public static final String[][] ADELE_AADLKW_CASES = {{"Upper","Upper"},{"Lower","Lower"},{"AsIs","AsIs"} };

	public static final String ADELE_AADLID_CASE = "AADLIDCase";

	public static final String ADELE_AADLID_DEFAULT_CASE = "Upper";

	public static final String[][] ADELE_AADLID_CASES = {{"Lower","Lower"},{"Upper","Upper"},{"AsIs","AsIs"} };
	
}