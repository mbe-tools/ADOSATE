package org.topcased.adele.common.properties;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.SubActionBars;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

public abstract class AdelePropertySection extends AbstractPropertySection {
	
	private AdelePropertyPanel propertyPanel = null;

	private SubActionBars subActionBars = null;

    protected abstract AdelePropertyPanel createPropertyPanel( 	Composite p_parent,
    															IActionBars p_actionBars );
	
    @Override
	public void createControls(	final Composite p_parent,
								final TabbedPropertySheetPage p_tabbedPropertySheetPage ) {
		super.createControls( p_parent, p_tabbedPropertySheetPage );
		
		final IActionBars actionBars = p_tabbedPropertySheetPage.getSite().getActionBars();
        subActionBars = new SubActionBars( actionBars );
        propertyPanel = createPropertyPanel( p_parent, subActionBars );
        
        if ( addControlListener() ) {
			propertyPanel.addControlListener( new ControlAdapter() {
	
	        	@Override
	        	public void controlResized( ControlEvent e ) {
					p_tabbedPropertySheetPage.resizeScrolledComposite();
				}
			} );
        }
	}
    
    protected boolean addControlListener() {
    	return true;
    }

	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ISection#dispose()
	 */
	@Override
	public void dispose() {
		super.dispose();

		if ( propertyPanel != null ) {
			propertyPanel.dispose();
			propertyPanel = null;
		}
	}

    /**
     * @see org.eclipse.ui.views.properties.tabbed.AbstractPropertySection#aboutToBeHidden()
     */
    @Override
    public void aboutToBeHidden() {
        super.aboutToBeHidden();
        
        if ( subActionBars != null ) {
	        subActionBars.deactivate();
	        subActionBars.updateActionBars();
        }
    }

    /**
     * @see org.eclipse.ui.views.properties.tabbed.AbstractPropertySection#aboutToBeShown()
     */
    @Override
    public void aboutToBeShown() {
        super.aboutToBeShown();
        
        if ( subActionBars != null ) {
	        subActionBars.activate();
	        subActionBars.updateActionBars();
        }
    }

	@Override
	public void setInput(	final IWorkbenchPart p_part,
							final ISelection p_selection ) {
		super.setInput( p_part, p_selection );
		
		if ( propertyPanel != null ) {
			propertyPanel.setInput( p_part, p_selection );
		}
	}

	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ISection#refresh()
	 */
	@Override
	public void refresh() {
		super.refresh();
		
		if ( propertyPanel != null ) {
			propertyPanel.refresh();
		}
	}
	
	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ISection#shouldUseExtraSpace()
	 */
	@Override
	public boolean shouldUseExtraSpace() {
		return true;
	}

	public AdelePropertyPanel getPropertyPanel() {
		return propertyPanel;
	}
}
