package org.topcased.adele.common.properties;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbenchPart;

public abstract class AdelePropertyPanel extends Composite {

	protected AdelePropertyPanel(	final Composite p_parent ) {
		super( p_parent, SWT.NULL );

		setLayout( new FillLayout() );
	}
	
	public abstract void setInput(	IWorkbenchPart p_part,
									ISelection p_selection );
	
	public abstract void refresh();
}
