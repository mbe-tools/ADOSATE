package org.topcased.adele.admin.builder;
// This class is not used at the moment. Fixing bug 2322
///*****************************************************************************
// * Copyright (c) 2009 Ellidiss Technologies
// *
// *    
// * All rights reserved. This program and the accompanying materials
// * are made available under the terms of the Eclipse Public License v1.0
// * which accompanies this distribution, and is available at
// * http://www.eclipse.org/legal/epl-v10.html
// *
// * Contributors:
// * 	Arnaud Schach, arnaud.schach@ellidiss.com
// *****************************************************************************/
//
//package org.topcased.adele.admin.builder;
//
//import java.util.Iterator;
//
//import org.eclipse.core.resources.IProject;
//import org.eclipse.core.resources.IProjectDescription;
//import org.eclipse.core.resources.IResource;
//import org.eclipse.core.runtime.CoreException;
//import org.eclipse.core.runtime.IAdaptable;
//import org.eclipse.jface.action.IAction;
//import org.eclipse.jface.viewers.ISelection;
//import org.eclipse.jface.viewers.IStructuredSelection;
//import org.eclipse.ui.IObjectActionDelegate;
//import org.eclipse.ui.IWorkbenchPart;
//
//public class RemoveADELEConfigNatureAction implements IObjectActionDelegate {
//
//	private ISelection selection;
//
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
//	 */
//	public void run(IAction action) {
//		if (selection instanceof IStructuredSelection) {
//			for (Iterator<?> it = ((IStructuredSelection) selection).iterator(); it
//					.hasNext();) {
//				Object element = it.next();
//				IProject project = null;
//				if (element instanceof IProject) {
//					project = (IProject) element;
//				} else if (element instanceof IAdaptable) {
//					project = (IProject) ((IAdaptable) element)
//							.getAdapter(IProject.class);
//				}
//				if (project != null) {
//					removeNature(project);
//					try
//			        {
//						project.refreshLocal(IResource.DEPTH_ONE, null);
//			        } catch (CoreException e2)
//			        {
//			            e2.printStackTrace();
//			        }
//				}
//			}
//		}
//	}
//
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction,
//	 *      org.eclipse.jface.viewers.ISelection)
//	 */
//	public void selectionChanged(IAction action, ISelection selection) {
//		this.selection = selection;
//	}
//
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see org.eclipse.ui.IObjectActionDelegate#setActivePart(org.eclipse.jface.action.IAction,
//	 *      org.eclipse.ui.IWorkbenchPart)
//	 */
//	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
//	}
//
//	/**
//	 * Toggles sample nature on a project
//	 * 
//	 * @param project
//	 *            to have sample nature added or removed
//	 */
//	public void removeNature(IProject project) {
//		try {
//			IProjectDescription description = project.getDescription();
//			String[] natures = description.getNatureIds();
//
//            IProject[] projects = project.getReferencedProjects();
//            for (int i=0;i<projects.length;i++){
//            	if(projects[i].getNature(ADELEConfigNature.NATURE_ID)!=null)
//            		System.err.println(projects[i].getName());
//            }
//            
//			for (int i = 0; i < natures.length; ++i) {
//				if (ADELEConfigNature.NATURE_ID.equals(natures[i])) {
//					// Remove the nature
//					String[] newNatures = new String[natures.length - 1];
//					System.arraycopy(natures, 0, newNatures, 0, i);
//					System.arraycopy(natures, i + 1, newNatures, i,
//							natures.length - i - 1);
//					description.setNatureIds(newNatures);
//					project.setDescription(description, null);
//					return;
//				}
//			}
//		} catch (CoreException e) {
//			System.err.println(e.getMessage());
//		}
//	}
//
//}
