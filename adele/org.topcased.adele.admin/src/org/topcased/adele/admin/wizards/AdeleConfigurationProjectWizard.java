package org.topcased.adele.admin.wizards;
// This class is not used at the moment. Fixing bug 2322
///*****************************************************************************
// * Copyright (c) 2009 Ellidiss Technologies
// *
// *    
// * All rights reserved. This program and the accompanying materials
// * are made available under the terms of the Eclipse Public License v1.0
// * which accompanies this distribution, and is available at
// * http://www.eclipse.org/legal/epl-v10.html
// *
// * Contributors:
// * 	Arnaud Schach, arnaud.schach@ellidiss.com
// *****************************************************************************/
//
//package org.topcased.adele.admin.wizards;
//
//import java.lang.reflect.InvocationTargetException;
//import java.text.MessageFormat;
//
//import org.eclipse.core.resources.IProject;
//import org.eclipse.core.resources.IProjectDescription;
//import org.eclipse.core.resources.IResourceStatus;
//import org.eclipse.core.resources.IWorkspace;
//import org.eclipse.core.resources.ResourcesPlugin;
//import org.eclipse.core.runtime.CoreException;
//import org.eclipse.core.runtime.IConfigurationElement;
//import org.eclipse.core.runtime.IExecutableExtension;
//import org.eclipse.core.runtime.IPath;
//import org.eclipse.core.runtime.IProgressMonitor;
//import org.eclipse.core.runtime.OperationCanceledException;
//import org.eclipse.core.runtime.SubProgressMonitor;
//import org.eclipse.jface.dialogs.ErrorDialog;
//import org.eclipse.jface.dialogs.IDialogSettings;
//import org.eclipse.jface.dialogs.MessageDialog;
//import org.eclipse.jface.viewers.IStructuredSelection;
//import org.eclipse.jface.wizard.WizardPage;
//import org.eclipse.swt.SWT;
//import org.eclipse.swt.layout.GridData;
//import org.eclipse.swt.layout.GridLayout;
//import org.eclipse.swt.widgets.Combo;
//import org.eclipse.swt.widgets.Composite;
//import org.eclipse.swt.widgets.Label;
//import org.eclipse.ui.IWorkbench;
//import org.eclipse.ui.actions.WorkspaceModifyOperation;
//import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;
//import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;
//import org.eclipse.ui.wizards.newresource.BasicNewResourceWizard;
//import org.topcased.adele.admin.ADELEAdministrationPlugin;
//import org.topcased.adele.admin.builder.AddADELEConfigNatureAction;
//
///**
// * This is a simple wizard for creating a new ADELE project.
// */
//public class AdeleConfigurationProjectWizard extends BasicNewResourceWizard implements
//        IExecutableExtension
//{
//
//    /**
//     */
//    public class WizardNewAdeleConfigurationProjectCreationPage extends
//            WizardNewProjectCreationPage
//    {
//        /**
//         * Create the project creation page
//         */
//        public WizardNewAdeleConfigurationProjectCreationPage(String pageId)
//        {
//            super(pageId);
//        }
//
//        /**
//         * The framework calls this to see if the project is correct.
//         */
//        protected boolean validatePage()
//        {
//            if (getProjectName().indexOf(' ') != -1)
//            {
//                setErrorMessage("The space is an invalid character in project name "
//                        + getProjectName() + '.');
//                return false;
//            } else
//                return super.validatePage();
//        }
//
//    }
//    
//    public class WizardAdeleConfigPage extends WizardPage{
//
//		protected WizardAdeleConfigPage(String pageName) {
//			super(pageName);
//		    setPageComplete(true);
//		}
//
//		public void createControl(Composite parent) {
//			Composite composite = new Composite(parent, SWT.NONE);
//		    composite.setLayout(new GridLayout(2, false));
//		    new Label(composite, SWT.NULL).setText("List of local  configuration: ");
//		    Combo combo = new Combo(composite, SWT.READ_ONLY | SWT.BORDER);
//		    IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
//		    for (int i=0;i<projects.length;i++){
//		    	combo.add(projects[i].getName());
//		    }
//		    combo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
//		    setControl(composite);
//		}
//    	
//    }
//    /**
//     * The config element which declares this wizard.
//     */
//    private IConfigurationElement configElement;
//
//    //cache of newly-created project
//    private IProject newProject;
//
//    /**
//     * This is the project creation page.
//     */
//    protected WizardNewAdeleConfigurationProjectCreationPage newProjectCreationPage;
//
//    /**
//     * This is the config definition page.
//     */
//    protected WizardAdeleConfigPage newConfigPage;
//
//    
//    protected IWorkbench workbench;
//
//    /**
//     * Creates a wizard for creating a new project resource in the workspace.
//     */
//    public AdeleConfigurationProjectWizard()
//    {
//        IDialogSettings workbenchSettings = ADELEAdministrationPlugin.getDefault()
//                .getDialogSettings();
//        IDialogSettings section = workbenchSettings
//                .getSection("BasicNewProjectResourceWizard");
//        if (section == null)
//            section = workbenchSettings
//                    .addNewSection("BasicNewProjectResourceWizard");
//        setDialogSettings(section);
//    }
//
//    /*
//     * (non-Javadoc) Method declared on IWizard.
//     */
//    public void addPages()
//    {
//        super.addPages();
//
//        newProjectCreationPage = new WizardNewAdeleConfigurationProjectCreationPage(
//                "basicNewProjectPage");
//        newProjectCreationPage.setTitle("ADELE Project"); 
//        newProjectCreationPage
//                .setDescription("Create a new ADELE project resource."); 
//        this.addPage(newProjectCreationPage);
//    }
//
//    /**
//     * Creates a new project resource with the selected name.
//     * <p>
//     * In normal usage, this method is invoked after the user has pressed Finish
//     * on the wizard; the enablement of the Finish button implies that all
//     * controls on the pages currently contain valid values.
//     * </p>
//     * <p>
//     * Note that this wizard caches the new project once it has been
//     * successfully created; subsequent invocations of this method will answer
//     * the same project resource without attempting to create it again.
//     * </p>
//     * 
//     * @return the created project resource, or <code>null</code> if the
//     *         project was not created
//     */
//    private IProject createNewProject()
//    {
//        if (newProject != null)
//            return newProject;
//
//        // get a project handle
//        final IProject newProjectHandle = newProjectCreationPage
//                .getProjectHandle();
//
//        // get a project descriptor
//        IPath newPath = null;
//        if (!newProjectCreationPage.useDefaults())
//            newPath = newProjectCreationPage.getLocationPath();
//
//        IWorkspace workspace = ResourcesPlugin.getWorkspace();
//        final IProjectDescription description = workspace
//                .newProjectDescription(newProjectHandle.getName());
//        description.setLocation(newPath);
//
//        // create the new project operation
//        WorkspaceModifyOperation op = new WorkspaceModifyOperation()
//        {
//            protected void execute(IProgressMonitor monitor)
//                    throws CoreException
//            {
//                createProject(description, newProjectHandle, monitor);
//            }
//        };
//
//        // run the new project creation operation
//        try
//        {
//            getContainer().run(true, true, op);
//        } catch (InterruptedException e)
//        {
//            return null;
//        } catch (InvocationTargetException e)
//        {
//            // ie.- one of the steps resulted in a core exception
//            Throwable t = e.getTargetException();
//            if (t instanceof CoreException)
//            {
//            	if (((CoreException) t).getStatus().getCode() == IResourceStatus.CASE_VARIANT_EXISTS)
//            	{
//            		MessageDialog
//            		.openError(
//            				getShell(),
//            				"Creation Problems", 
//            				MessageFormat
//            				.format(
//            						"The underlying file system is case insensitive. There is an existing project which conflicts with ''{0}''.", new Object[] { newProjectHandle.getName() })
//            		);
//            	} else
//            	{
//            		ErrorDialog.openError(getShell(), "Creation Problems", 
//            				null, // no special message
//            				((CoreException) t).getStatus());
//            	}
//            } else
//            {  MessageDialog
//                        .openError(
//                                getShell(),
//                                "Creation Problems", 
//                                MessageFormat
//                                        .format(
//                                                "Internal error: {0}", new Object[] { t.getMessage() })); 
//            }
//            return null;
//        }
//
//        newProject = newProjectHandle;
//
//        return newProject;
//    }
//
//    /**
//     * Creates a project resource given the project handle and description.
//     * 
//     * @param description
//     *            the project description to create a project resource for
//     * @param projectHandle
//     *            the project handle to create a project resource for
//     * @param monitor
//     *            the progress monitor to show visual progress with
//     * 
//     * @exception CoreException
//     *                if the operation fails
//     * @exception OperationCanceledException
//     *                if the operation is canceled
//     */
//    void createProject(IProjectDescription description, IProject projectHandle,
//            IProgressMonitor monitor) throws CoreException,
//            OperationCanceledException
//    {
//        try
//        {
//            monitor.beginTask("", 2000);
//
//            projectHandle.create(description, new SubProgressMonitor(monitor,
//                    1000));
//
//            if (monitor.isCanceled())
//                throw new OperationCanceledException();
//
//            projectHandle.open(new SubProgressMonitor(monitor, 1000));
//
//        } finally
//        {
//            monitor.done();
//        }
//    }
//
//    /**
//     * Returns the newly created project.
//     * 
//     * @return the created project, or <code>null</code> if project not
//     *         created
//     */
//    public IProject getNewProject()
//    {
//        return newProject;
//    }
//
//    /**
//     * This just records the information. <!-- begin-user-doc --> <!--
//     * end-user-doc -->
//     * 
//     * @generated
//     */
//    public void init(IWorkbench workbench, IStructuredSelection selection)
//    {
//        super.init(workbench, selection);
//        this.workbench = workbench;
//        setWindowTitle("New");
//    }
//
//    /**
//     * Do the work after everything is specified. <!-- begin-user-doc --> <!--
//     * end-user-doc -->
//     * 
//     * @generated NOT
//     */
//    public boolean performFinish()
//    {
//        createNewProject();
//        if (newProject == null)
//            return false;
//        updatePerspective();
//        selectAndReveal(newProject);
//        final IProject p = getNewProject();
//        AddADELEConfigNatureAction.addNature(p);
//        try
//        {
//            p.refreshLocal(1, null);
//            
//        } catch (CoreException e2)
//        {
//            e2.printStackTrace();
//        }
//        return true;
//    }
//
//    /**
//     * Stores the configuration element for the wizard. The config element will
//     * be used in <code>performFinish</code> to set the result perspective.
//     */
//    public void setInitializationData(IConfigurationElement cfig,
//            String propertyName, Object data)
//    {
//        configElement = cfig;
//    }
//
//    /**
//     * Updates the perspective for the active page within the window.
//     */
//    protected void updatePerspective()
//    {
//        BasicNewProjectResourceWizard.updatePerspective(configElement);
//    }
//}
