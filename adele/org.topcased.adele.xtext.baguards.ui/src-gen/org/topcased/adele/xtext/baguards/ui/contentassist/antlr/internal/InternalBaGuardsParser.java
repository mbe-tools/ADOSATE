package org.topcased.adele.xtext.baguards.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.topcased.adele.xtext.baguards.services.BaGuardsGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalBaGuardsParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING_LITERAL", "RULE_DIGIT", "RULE_EXTENDED_DIGIT", "RULE_INT", "RULE_ID", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'::'", "'.'", "'_'", "'E'", "'+'", "'E -'", "'#'", "'frozen'", "'or'", "'and'", "','", "'otherwise'", "'timeout'", "'?'", "'count'", "'fresh'", "'xor'", "'='", "'!='", "'<'", "'<='", "'>'", "'>='", "'-'", "'*'", "'/'", "'mod'", "'rem'", "'**'", "'abs'", "'not'", "'true'", "'false'", "'ps'", "'ns'", "'us'", "'ms'", "'sec'", "'min'", "'hr'", "'on dispatch'", "'stop'"
    };
    public static final int RULE_ID=8;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=13;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int EOF=-1;
    public static final int T__55=55;
    public static final int T__19=19;
    public static final int T__16=16;
    public static final int T__51=51;
    public static final int T__15=15;
    public static final int T__52=52;
    public static final int T__18=18;
    public static final int T__53=53;
    public static final int RULE_STRING_LITERAL=4;
    public static final int T__54=54;
    public static final int T__17=17;
    public static final int T__14=14;
    public static final int RULE_INT=7;
    public static final int T__50=50;
    public static final int RULE_EXTENDED_DIGIT=6;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RULE_SL_COMMENT=11;
    public static final int RULE_ML_COMMENT=10;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_STRING=9;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int RULE_WS=12;
    public static final int RULE_DIGIT=5;

    // delegates
    // delegators


        public InternalBaGuardsParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalBaGuardsParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalBaGuardsParser.tokenNames; }
    public String getGrammarFileName() { return "../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g"; }


     
     	private BaGuardsGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(BaGuardsGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleGuard"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:60:1: entryRuleGuard : ruleGuard EOF ;
    public final void entryRuleGuard() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:61:1: ( ruleGuard EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:62:1: ruleGuard EOF
            {
             before(grammarAccess.getGuardRule()); 
            pushFollow(FOLLOW_ruleGuard_in_entryRuleGuard61);
            ruleGuard();

            state._fsp--;

             after(grammarAccess.getGuardRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGuard68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGuard"


    // $ANTLR start "ruleGuard"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:69:1: ruleGuard : ( ( rule__Guard__Alternatives ) ) ;
    public final void ruleGuard() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:73:2: ( ( ( rule__Guard__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:74:1: ( ( rule__Guard__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:74:1: ( ( rule__Guard__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:75:1: ( rule__Guard__Alternatives )
            {
             before(grammarAccess.getGuardAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:76:1: ( rule__Guard__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:76:2: rule__Guard__Alternatives
            {
            pushFollow(FOLLOW_rule__Guard__Alternatives_in_ruleGuard94);
            rule__Guard__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getGuardAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGuard"


    // $ANTLR start "entryRuleexecute_condition"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:88:1: entryRuleexecute_condition : ruleexecute_condition EOF ;
    public final void entryRuleexecute_condition() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:89:1: ( ruleexecute_condition EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:90:1: ruleexecute_condition EOF
            {
             before(grammarAccess.getExecute_conditionRule()); 
            pushFollow(FOLLOW_ruleexecute_condition_in_entryRuleexecute_condition121);
            ruleexecute_condition();

            state._fsp--;

             after(grammarAccess.getExecute_conditionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleexecute_condition128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleexecute_condition"


    // $ANTLR start "ruleexecute_condition"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:97:1: ruleexecute_condition : ( ( rule__Execute_condition__Alternatives ) ) ;
    public final void ruleexecute_condition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:101:2: ( ( ( rule__Execute_condition__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:102:1: ( ( rule__Execute_condition__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:102:1: ( ( rule__Execute_condition__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:103:1: ( rule__Execute_condition__Alternatives )
            {
             before(grammarAccess.getExecute_conditionAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:104:1: ( rule__Execute_condition__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:104:2: rule__Execute_condition__Alternatives
            {
            pushFollow(FOLLOW_rule__Execute_condition__Alternatives_in_ruleexecute_condition154);
            rule__Execute_condition__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getExecute_conditionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleexecute_condition"


    // $ANTLR start "entryRulebehavior_action_block_timeout_catch"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:116:1: entryRulebehavior_action_block_timeout_catch : rulebehavior_action_block_timeout_catch EOF ;
    public final void entryRulebehavior_action_block_timeout_catch() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:117:1: ( rulebehavior_action_block_timeout_catch EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:118:1: rulebehavior_action_block_timeout_catch EOF
            {
             before(grammarAccess.getBehavior_action_block_timeout_catchRule()); 
            pushFollow(FOLLOW_rulebehavior_action_block_timeout_catch_in_entryRulebehavior_action_block_timeout_catch181);
            rulebehavior_action_block_timeout_catch();

            state._fsp--;

             after(grammarAccess.getBehavior_action_block_timeout_catchRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulebehavior_action_block_timeout_catch188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulebehavior_action_block_timeout_catch"


    // $ANTLR start "rulebehavior_action_block_timeout_catch"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:125:1: rulebehavior_action_block_timeout_catch : ( ( rule__Behavior_action_block_timeout_catch__BabnameAssignment ) ) ;
    public final void rulebehavior_action_block_timeout_catch() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:129:2: ( ( ( rule__Behavior_action_block_timeout_catch__BabnameAssignment ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:130:1: ( ( rule__Behavior_action_block_timeout_catch__BabnameAssignment ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:130:1: ( ( rule__Behavior_action_block_timeout_catch__BabnameAssignment ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:131:1: ( rule__Behavior_action_block_timeout_catch__BabnameAssignment )
            {
             before(grammarAccess.getBehavior_action_block_timeout_catchAccess().getBabnameAssignment()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:132:1: ( rule__Behavior_action_block_timeout_catch__BabnameAssignment )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:132:2: rule__Behavior_action_block_timeout_catch__BabnameAssignment
            {
            pushFollow(FOLLOW_rule__Behavior_action_block_timeout_catch__BabnameAssignment_in_rulebehavior_action_block_timeout_catch214);
            rule__Behavior_action_block_timeout_catch__BabnameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getBehavior_action_block_timeout_catchAccess().getBabnameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulebehavior_action_block_timeout_catch"


    // $ANTLR start "entryRulevalue_expression"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:144:1: entryRulevalue_expression : rulevalue_expression EOF ;
    public final void entryRulevalue_expression() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:145:1: ( rulevalue_expression EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:146:1: rulevalue_expression EOF
            {
             before(grammarAccess.getValue_expressionRule()); 
            pushFollow(FOLLOW_rulevalue_expression_in_entryRulevalue_expression241);
            rulevalue_expression();

            state._fsp--;

             after(grammarAccess.getValue_expressionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulevalue_expression248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulevalue_expression"


    // $ANTLR start "rulevalue_expression"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:153:1: rulevalue_expression : ( ( rule__Value_expression__Group__0 ) ) ;
    public final void rulevalue_expression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:157:2: ( ( ( rule__Value_expression__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:158:1: ( ( rule__Value_expression__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:158:1: ( ( rule__Value_expression__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:159:1: ( rule__Value_expression__Group__0 )
            {
             before(grammarAccess.getValue_expressionAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:160:1: ( rule__Value_expression__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:160:2: rule__Value_expression__Group__0
            {
            pushFollow(FOLLOW_rule__Value_expression__Group__0_in_rulevalue_expression274);
            rule__Value_expression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getValue_expressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulevalue_expression"


    // $ANTLR start "entryRulerelation"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:172:1: entryRulerelation : rulerelation EOF ;
    public final void entryRulerelation() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:173:1: ( rulerelation EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:174:1: rulerelation EOF
            {
             before(grammarAccess.getRelationRule()); 
            pushFollow(FOLLOW_rulerelation_in_entryRulerelation301);
            rulerelation();

            state._fsp--;

             after(grammarAccess.getRelationRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulerelation308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulerelation"


    // $ANTLR start "rulerelation"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:181:1: rulerelation : ( ( rule__Relation__Group__0 ) ) ;
    public final void rulerelation() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:185:2: ( ( ( rule__Relation__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:186:1: ( ( rule__Relation__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:186:1: ( ( rule__Relation__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:187:1: ( rule__Relation__Group__0 )
            {
             before(grammarAccess.getRelationAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:188:1: ( rule__Relation__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:188:2: rule__Relation__Group__0
            {
            pushFollow(FOLLOW_rule__Relation__Group__0_in_rulerelation334);
            rule__Relation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRelationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulerelation"


    // $ANTLR start "entryRulesimple_expression"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:200:1: entryRulesimple_expression : rulesimple_expression EOF ;
    public final void entryRulesimple_expression() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:201:1: ( rulesimple_expression EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:202:1: rulesimple_expression EOF
            {
             before(grammarAccess.getSimple_expressionRule()); 
            pushFollow(FOLLOW_rulesimple_expression_in_entryRulesimple_expression361);
            rulesimple_expression();

            state._fsp--;

             after(grammarAccess.getSimple_expressionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulesimple_expression368); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulesimple_expression"


    // $ANTLR start "rulesimple_expression"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:209:1: rulesimple_expression : ( ( rule__Simple_expression__Group__0 ) ) ;
    public final void rulesimple_expression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:213:2: ( ( ( rule__Simple_expression__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:214:1: ( ( rule__Simple_expression__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:214:1: ( ( rule__Simple_expression__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:215:1: ( rule__Simple_expression__Group__0 )
            {
             before(grammarAccess.getSimple_expressionAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:216:1: ( rule__Simple_expression__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:216:2: rule__Simple_expression__Group__0
            {
            pushFollow(FOLLOW_rule__Simple_expression__Group__0_in_rulesimple_expression394);
            rule__Simple_expression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSimple_expressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulesimple_expression"


    // $ANTLR start "entryRuleterm"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:228:1: entryRuleterm : ruleterm EOF ;
    public final void entryRuleterm() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:229:1: ( ruleterm EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:230:1: ruleterm EOF
            {
             before(grammarAccess.getTermRule()); 
            pushFollow(FOLLOW_ruleterm_in_entryRuleterm421);
            ruleterm();

            state._fsp--;

             after(grammarAccess.getTermRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleterm428); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleterm"


    // $ANTLR start "ruleterm"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:237:1: ruleterm : ( ( rule__Term__Group__0 ) ) ;
    public final void ruleterm() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:241:2: ( ( ( rule__Term__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:242:1: ( ( rule__Term__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:242:1: ( ( rule__Term__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:243:1: ( rule__Term__Group__0 )
            {
             before(grammarAccess.getTermAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:244:1: ( rule__Term__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:244:2: rule__Term__Group__0
            {
            pushFollow(FOLLOW_rule__Term__Group__0_in_ruleterm454);
            rule__Term__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTermAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleterm"


    // $ANTLR start "entryRulefactor"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:256:1: entryRulefactor : rulefactor EOF ;
    public final void entryRulefactor() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:257:1: ( rulefactor EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:258:1: rulefactor EOF
            {
             before(grammarAccess.getFactorRule()); 
            pushFollow(FOLLOW_rulefactor_in_entryRulefactor481);
            rulefactor();

            state._fsp--;

             after(grammarAccess.getFactorRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulefactor488); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulefactor"


    // $ANTLR start "rulefactor"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:265:1: rulefactor : ( ( rule__Factor__Alternatives ) ) ;
    public final void rulefactor() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:269:2: ( ( ( rule__Factor__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:270:1: ( ( rule__Factor__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:270:1: ( ( rule__Factor__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:271:1: ( rule__Factor__Alternatives )
            {
             before(grammarAccess.getFactorAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:272:1: ( rule__Factor__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:272:2: rule__Factor__Alternatives
            {
            pushFollow(FOLLOW_rule__Factor__Alternatives_in_rulefactor514);
            rule__Factor__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFactorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulefactor"


    // $ANTLR start "entryRulevalue"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:284:1: entryRulevalue : rulevalue EOF ;
    public final void entryRulevalue() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:285:1: ( rulevalue EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:286:1: rulevalue EOF
            {
             before(grammarAccess.getValueRule()); 
            pushFollow(FOLLOW_rulevalue_in_entryRulevalue541);
            rulevalue();

            state._fsp--;

             after(grammarAccess.getValueRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulevalue548); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulevalue"


    // $ANTLR start "rulevalue"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:293:1: rulevalue : ( ( rule__Value__Alternatives ) ) ;
    public final void rulevalue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:297:2: ( ( ( rule__Value__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:298:1: ( ( rule__Value__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:298:1: ( ( rule__Value__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:299:1: ( rule__Value__Alternatives )
            {
             before(grammarAccess.getValueAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:300:1: ( rule__Value__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:300:2: rule__Value__Alternatives
            {
            pushFollow(FOLLOW_rule__Value__Alternatives_in_rulevalue574);
            rule__Value__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getValueAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulevalue"


    // $ANTLR start "entryRulevalue_variable"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:312:1: entryRulevalue_variable : rulevalue_variable EOF ;
    public final void entryRulevalue_variable() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:313:1: ( rulevalue_variable EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:314:1: rulevalue_variable EOF
            {
             before(grammarAccess.getValue_variableRule()); 
            pushFollow(FOLLOW_rulevalue_variable_in_entryRulevalue_variable601);
            rulevalue_variable();

            state._fsp--;

             after(grammarAccess.getValue_variableRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulevalue_variable608); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulevalue_variable"


    // $ANTLR start "rulevalue_variable"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:321:1: rulevalue_variable : ( ( rule__Value_variable__Group__0 ) ) ;
    public final void rulevalue_variable() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:325:2: ( ( ( rule__Value_variable__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:326:1: ( ( rule__Value_variable__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:326:1: ( ( rule__Value_variable__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:327:1: ( rule__Value_variable__Group__0 )
            {
             before(grammarAccess.getValue_variableAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:328:1: ( rule__Value_variable__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:328:2: rule__Value_variable__Group__0
            {
            pushFollow(FOLLOW_rule__Value_variable__Group__0_in_rulevalue_variable634);
            rule__Value_variable__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getValue_variableAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulevalue_variable"


    // $ANTLR start "entryRulevalue_constant"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:340:1: entryRulevalue_constant : rulevalue_constant EOF ;
    public final void entryRulevalue_constant() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:341:1: ( rulevalue_constant EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:342:1: rulevalue_constant EOF
            {
             before(grammarAccess.getValue_constantRule()); 
            pushFollow(FOLLOW_rulevalue_constant_in_entryRulevalue_constant661);
            rulevalue_constant();

            state._fsp--;

             after(grammarAccess.getValue_constantRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulevalue_constant668); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulevalue_constant"


    // $ANTLR start "rulevalue_constant"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:349:1: rulevalue_constant : ( ( rule__Value_constant__Alternatives ) ) ;
    public final void rulevalue_constant() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:353:2: ( ( ( rule__Value_constant__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:354:1: ( ( rule__Value_constant__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:354:1: ( ( rule__Value_constant__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:355:1: ( rule__Value_constant__Alternatives )
            {
             before(grammarAccess.getValue_constantAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:356:1: ( rule__Value_constant__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:356:2: rule__Value_constant__Alternatives
            {
            pushFollow(FOLLOW_rule__Value_constant__Alternatives_in_rulevalue_constant694);
            rule__Value_constant__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getValue_constantAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulevalue_constant"


    // $ANTLR start "entryRulelogical_operator"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:368:1: entryRulelogical_operator : rulelogical_operator EOF ;
    public final void entryRulelogical_operator() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:369:1: ( rulelogical_operator EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:370:1: rulelogical_operator EOF
            {
             before(grammarAccess.getLogical_operatorRule()); 
            pushFollow(FOLLOW_rulelogical_operator_in_entryRulelogical_operator721);
            rulelogical_operator();

            state._fsp--;

             after(grammarAccess.getLogical_operatorRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulelogical_operator728); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulelogical_operator"


    // $ANTLR start "rulelogical_operator"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:377:1: rulelogical_operator : ( ( rule__Logical_operator__Alternatives ) ) ;
    public final void rulelogical_operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:381:2: ( ( ( rule__Logical_operator__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:382:1: ( ( rule__Logical_operator__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:382:1: ( ( rule__Logical_operator__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:383:1: ( rule__Logical_operator__Alternatives )
            {
             before(grammarAccess.getLogical_operatorAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:384:1: ( rule__Logical_operator__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:384:2: rule__Logical_operator__Alternatives
            {
            pushFollow(FOLLOW_rule__Logical_operator__Alternatives_in_rulelogical_operator754);
            rule__Logical_operator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getLogical_operatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulelogical_operator"


    // $ANTLR start "entryRulerelational_operator"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:396:1: entryRulerelational_operator : rulerelational_operator EOF ;
    public final void entryRulerelational_operator() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:397:1: ( rulerelational_operator EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:398:1: rulerelational_operator EOF
            {
             before(grammarAccess.getRelational_operatorRule()); 
            pushFollow(FOLLOW_rulerelational_operator_in_entryRulerelational_operator781);
            rulerelational_operator();

            state._fsp--;

             after(grammarAccess.getRelational_operatorRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulerelational_operator788); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulerelational_operator"


    // $ANTLR start "rulerelational_operator"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:405:1: rulerelational_operator : ( ( rule__Relational_operator__Alternatives ) ) ;
    public final void rulerelational_operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:409:2: ( ( ( rule__Relational_operator__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:410:1: ( ( rule__Relational_operator__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:410:1: ( ( rule__Relational_operator__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:411:1: ( rule__Relational_operator__Alternatives )
            {
             before(grammarAccess.getRelational_operatorAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:412:1: ( rule__Relational_operator__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:412:2: rule__Relational_operator__Alternatives
            {
            pushFollow(FOLLOW_rule__Relational_operator__Alternatives_in_rulerelational_operator814);
            rule__Relational_operator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getRelational_operatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulerelational_operator"


    // $ANTLR start "entryRulebinary_adding_operator"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:424:1: entryRulebinary_adding_operator : rulebinary_adding_operator EOF ;
    public final void entryRulebinary_adding_operator() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:425:1: ( rulebinary_adding_operator EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:426:1: rulebinary_adding_operator EOF
            {
             before(grammarAccess.getBinary_adding_operatorRule()); 
            pushFollow(FOLLOW_rulebinary_adding_operator_in_entryRulebinary_adding_operator841);
            rulebinary_adding_operator();

            state._fsp--;

             after(grammarAccess.getBinary_adding_operatorRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulebinary_adding_operator848); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulebinary_adding_operator"


    // $ANTLR start "rulebinary_adding_operator"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:433:1: rulebinary_adding_operator : ( ( rule__Binary_adding_operator__Alternatives ) ) ;
    public final void rulebinary_adding_operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:437:2: ( ( ( rule__Binary_adding_operator__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:438:1: ( ( rule__Binary_adding_operator__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:438:1: ( ( rule__Binary_adding_operator__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:439:1: ( rule__Binary_adding_operator__Alternatives )
            {
             before(grammarAccess.getBinary_adding_operatorAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:440:1: ( rule__Binary_adding_operator__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:440:2: rule__Binary_adding_operator__Alternatives
            {
            pushFollow(FOLLOW_rule__Binary_adding_operator__Alternatives_in_rulebinary_adding_operator874);
            rule__Binary_adding_operator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBinary_adding_operatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulebinary_adding_operator"


    // $ANTLR start "entryRuleunary_adding_operator"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:452:1: entryRuleunary_adding_operator : ruleunary_adding_operator EOF ;
    public final void entryRuleunary_adding_operator() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:453:1: ( ruleunary_adding_operator EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:454:1: ruleunary_adding_operator EOF
            {
             before(grammarAccess.getUnary_adding_operatorRule()); 
            pushFollow(FOLLOW_ruleunary_adding_operator_in_entryRuleunary_adding_operator901);
            ruleunary_adding_operator();

            state._fsp--;

             after(grammarAccess.getUnary_adding_operatorRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleunary_adding_operator908); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleunary_adding_operator"


    // $ANTLR start "ruleunary_adding_operator"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:461:1: ruleunary_adding_operator : ( ( rule__Unary_adding_operator__Alternatives ) ) ;
    public final void ruleunary_adding_operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:465:2: ( ( ( rule__Unary_adding_operator__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:466:1: ( ( rule__Unary_adding_operator__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:466:1: ( ( rule__Unary_adding_operator__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:467:1: ( rule__Unary_adding_operator__Alternatives )
            {
             before(grammarAccess.getUnary_adding_operatorAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:468:1: ( rule__Unary_adding_operator__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:468:2: rule__Unary_adding_operator__Alternatives
            {
            pushFollow(FOLLOW_rule__Unary_adding_operator__Alternatives_in_ruleunary_adding_operator934);
            rule__Unary_adding_operator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getUnary_adding_operatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleunary_adding_operator"


    // $ANTLR start "entryRulemultiplying_operator"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:480:1: entryRulemultiplying_operator : rulemultiplying_operator EOF ;
    public final void entryRulemultiplying_operator() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:481:1: ( rulemultiplying_operator EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:482:1: rulemultiplying_operator EOF
            {
             before(grammarAccess.getMultiplying_operatorRule()); 
            pushFollow(FOLLOW_rulemultiplying_operator_in_entryRulemultiplying_operator961);
            rulemultiplying_operator();

            state._fsp--;

             after(grammarAccess.getMultiplying_operatorRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulemultiplying_operator968); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulemultiplying_operator"


    // $ANTLR start "rulemultiplying_operator"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:489:1: rulemultiplying_operator : ( ( rule__Multiplying_operator__Alternatives ) ) ;
    public final void rulemultiplying_operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:493:2: ( ( ( rule__Multiplying_operator__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:494:1: ( ( rule__Multiplying_operator__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:494:1: ( ( rule__Multiplying_operator__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:495:1: ( rule__Multiplying_operator__Alternatives )
            {
             before(grammarAccess.getMultiplying_operatorAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:496:1: ( rule__Multiplying_operator__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:496:2: rule__Multiplying_operator__Alternatives
            {
            pushFollow(FOLLOW_rule__Multiplying_operator__Alternatives_in_rulemultiplying_operator994);
            rule__Multiplying_operator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getMultiplying_operatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulemultiplying_operator"


    // $ANTLR start "entryRulebinary_numeric_operator"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:508:1: entryRulebinary_numeric_operator : rulebinary_numeric_operator EOF ;
    public final void entryRulebinary_numeric_operator() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:509:1: ( rulebinary_numeric_operator EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:510:1: rulebinary_numeric_operator EOF
            {
             before(grammarAccess.getBinary_numeric_operatorRule()); 
            pushFollow(FOLLOW_rulebinary_numeric_operator_in_entryRulebinary_numeric_operator1021);
            rulebinary_numeric_operator();

            state._fsp--;

             after(grammarAccess.getBinary_numeric_operatorRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulebinary_numeric_operator1028); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulebinary_numeric_operator"


    // $ANTLR start "rulebinary_numeric_operator"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:517:1: rulebinary_numeric_operator : ( ( rule__Binary_numeric_operator__BnonameAssignment ) ) ;
    public final void rulebinary_numeric_operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:521:2: ( ( ( rule__Binary_numeric_operator__BnonameAssignment ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:522:1: ( ( rule__Binary_numeric_operator__BnonameAssignment ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:522:1: ( ( rule__Binary_numeric_operator__BnonameAssignment ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:523:1: ( rule__Binary_numeric_operator__BnonameAssignment )
            {
             before(grammarAccess.getBinary_numeric_operatorAccess().getBnonameAssignment()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:524:1: ( rule__Binary_numeric_operator__BnonameAssignment )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:524:2: rule__Binary_numeric_operator__BnonameAssignment
            {
            pushFollow(FOLLOW_rule__Binary_numeric_operator__BnonameAssignment_in_rulebinary_numeric_operator1054);
            rule__Binary_numeric_operator__BnonameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getBinary_numeric_operatorAccess().getBnonameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulebinary_numeric_operator"


    // $ANTLR start "entryRuleunary_numeric_operator"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:536:1: entryRuleunary_numeric_operator : ruleunary_numeric_operator EOF ;
    public final void entryRuleunary_numeric_operator() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:537:1: ( ruleunary_numeric_operator EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:538:1: ruleunary_numeric_operator EOF
            {
             before(grammarAccess.getUnary_numeric_operatorRule()); 
            pushFollow(FOLLOW_ruleunary_numeric_operator_in_entryRuleunary_numeric_operator1081);
            ruleunary_numeric_operator();

            state._fsp--;

             after(grammarAccess.getUnary_numeric_operatorRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleunary_numeric_operator1088); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleunary_numeric_operator"


    // $ANTLR start "ruleunary_numeric_operator"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:545:1: ruleunary_numeric_operator : ( ( rule__Unary_numeric_operator__UnonameAssignment ) ) ;
    public final void ruleunary_numeric_operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:549:2: ( ( ( rule__Unary_numeric_operator__UnonameAssignment ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:550:1: ( ( rule__Unary_numeric_operator__UnonameAssignment ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:550:1: ( ( rule__Unary_numeric_operator__UnonameAssignment ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:551:1: ( rule__Unary_numeric_operator__UnonameAssignment )
            {
             before(grammarAccess.getUnary_numeric_operatorAccess().getUnonameAssignment()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:552:1: ( rule__Unary_numeric_operator__UnonameAssignment )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:552:2: rule__Unary_numeric_operator__UnonameAssignment
            {
            pushFollow(FOLLOW_rule__Unary_numeric_operator__UnonameAssignment_in_ruleunary_numeric_operator1114);
            rule__Unary_numeric_operator__UnonameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getUnary_numeric_operatorAccess().getUnonameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleunary_numeric_operator"


    // $ANTLR start "entryRuleunary_boolean_operator"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:564:1: entryRuleunary_boolean_operator : ruleunary_boolean_operator EOF ;
    public final void entryRuleunary_boolean_operator() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:565:1: ( ruleunary_boolean_operator EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:566:1: ruleunary_boolean_operator EOF
            {
             before(grammarAccess.getUnary_boolean_operatorRule()); 
            pushFollow(FOLLOW_ruleunary_boolean_operator_in_entryRuleunary_boolean_operator1141);
            ruleunary_boolean_operator();

            state._fsp--;

             after(grammarAccess.getUnary_boolean_operatorRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleunary_boolean_operator1148); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleunary_boolean_operator"


    // $ANTLR start "ruleunary_boolean_operator"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:573:1: ruleunary_boolean_operator : ( ( rule__Unary_boolean_operator__UbonameAssignment ) ) ;
    public final void ruleunary_boolean_operator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:577:2: ( ( ( rule__Unary_boolean_operator__UbonameAssignment ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:578:1: ( ( rule__Unary_boolean_operator__UbonameAssignment ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:578:1: ( ( rule__Unary_boolean_operator__UbonameAssignment ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:579:1: ( rule__Unary_boolean_operator__UbonameAssignment )
            {
             before(grammarAccess.getUnary_boolean_operatorAccess().getUbonameAssignment()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:580:1: ( rule__Unary_boolean_operator__UbonameAssignment )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:580:2: rule__Unary_boolean_operator__UbonameAssignment
            {
            pushFollow(FOLLOW_rule__Unary_boolean_operator__UbonameAssignment_in_ruleunary_boolean_operator1174);
            rule__Unary_boolean_operator__UbonameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getUnary_boolean_operatorAccess().getUbonameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleunary_boolean_operator"


    // $ANTLR start "entryRuleboolean_literal"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:592:1: entryRuleboolean_literal : ruleboolean_literal EOF ;
    public final void entryRuleboolean_literal() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:593:1: ( ruleboolean_literal EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:594:1: ruleboolean_literal EOF
            {
             before(grammarAccess.getBoolean_literalRule()); 
            pushFollow(FOLLOW_ruleboolean_literal_in_entryRuleboolean_literal1201);
            ruleboolean_literal();

            state._fsp--;

             after(grammarAccess.getBoolean_literalRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleboolean_literal1208); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleboolean_literal"


    // $ANTLR start "ruleboolean_literal"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:601:1: ruleboolean_literal : ( ( rule__Boolean_literal__Alternatives ) ) ;
    public final void ruleboolean_literal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:605:2: ( ( ( rule__Boolean_literal__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:606:1: ( ( rule__Boolean_literal__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:606:1: ( ( rule__Boolean_literal__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:607:1: ( rule__Boolean_literal__Alternatives )
            {
             before(grammarAccess.getBoolean_literalAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:608:1: ( rule__Boolean_literal__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:608:2: rule__Boolean_literal__Alternatives
            {
            pushFollow(FOLLOW_rule__Boolean_literal__Alternatives_in_ruleboolean_literal1234);
            rule__Boolean_literal__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBoolean_literalAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleboolean_literal"


    // $ANTLR start "entryRuleinteger_value"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:620:1: entryRuleinteger_value : ruleinteger_value EOF ;
    public final void entryRuleinteger_value() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:621:1: ( ruleinteger_value EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:622:1: ruleinteger_value EOF
            {
             before(grammarAccess.getInteger_valueRule()); 
            pushFollow(FOLLOW_ruleinteger_value_in_entryRuleinteger_value1261);
            ruleinteger_value();

            state._fsp--;

             after(grammarAccess.getInteger_valueRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleinteger_value1268); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleinteger_value"


    // $ANTLR start "ruleinteger_value"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:629:1: ruleinteger_value : ( ( rule__Integer_value__IvnameAssignment ) ) ;
    public final void ruleinteger_value() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:633:2: ( ( ( rule__Integer_value__IvnameAssignment ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:634:1: ( ( rule__Integer_value__IvnameAssignment ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:634:1: ( ( rule__Integer_value__IvnameAssignment ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:635:1: ( rule__Integer_value__IvnameAssignment )
            {
             before(grammarAccess.getInteger_valueAccess().getIvnameAssignment()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:636:1: ( rule__Integer_value__IvnameAssignment )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:636:2: rule__Integer_value__IvnameAssignment
            {
            pushFollow(FOLLOW_rule__Integer_value__IvnameAssignment_in_ruleinteger_value1294);
            rule__Integer_value__IvnameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getInteger_valueAccess().getIvnameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleinteger_value"


    // $ANTLR start "entryRulebehavior_time"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:648:1: entryRulebehavior_time : rulebehavior_time EOF ;
    public final void entryRulebehavior_time() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:649:1: ( rulebehavior_time EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:650:1: rulebehavior_time EOF
            {
             before(grammarAccess.getBehavior_timeRule()); 
            pushFollow(FOLLOW_rulebehavior_time_in_entryRulebehavior_time1321);
            rulebehavior_time();

            state._fsp--;

             after(grammarAccess.getBehavior_timeRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulebehavior_time1328); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulebehavior_time"


    // $ANTLR start "rulebehavior_time"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:657:1: rulebehavior_time : ( ( rule__Behavior_time__Group__0 ) ) ;
    public final void rulebehavior_time() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:661:2: ( ( ( rule__Behavior_time__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:662:1: ( ( rule__Behavior_time__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:662:1: ( ( rule__Behavior_time__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:663:1: ( rule__Behavior_time__Group__0 )
            {
             before(grammarAccess.getBehavior_timeAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:664:1: ( rule__Behavior_time__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:664:2: rule__Behavior_time__Group__0
            {
            pushFollow(FOLLOW_rule__Behavior_time__Group__0_in_rulebehavior_time1354);
            rule__Behavior_time__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBehavior_timeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulebehavior_time"


    // $ANTLR start "entryRuleunit_identifier"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:676:1: entryRuleunit_identifier : ruleunit_identifier EOF ;
    public final void entryRuleunit_identifier() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:677:1: ( ruleunit_identifier EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:678:1: ruleunit_identifier EOF
            {
             before(grammarAccess.getUnit_identifierRule()); 
            pushFollow(FOLLOW_ruleunit_identifier_in_entryRuleunit_identifier1381);
            ruleunit_identifier();

            state._fsp--;

             after(grammarAccess.getUnit_identifierRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleunit_identifier1388); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleunit_identifier"


    // $ANTLR start "ruleunit_identifier"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:685:1: ruleunit_identifier : ( ( rule__Unit_identifier__Alternatives ) ) ;
    public final void ruleunit_identifier() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:689:2: ( ( ( rule__Unit_identifier__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:690:1: ( ( rule__Unit_identifier__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:690:1: ( ( rule__Unit_identifier__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:691:1: ( rule__Unit_identifier__Alternatives )
            {
             before(grammarAccess.getUnit_identifierAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:692:1: ( rule__Unit_identifier__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:692:2: rule__Unit_identifier__Alternatives
            {
            pushFollow(FOLLOW_rule__Unit_identifier__Alternatives_in_ruleunit_identifier1414);
            rule__Unit_identifier__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getUnit_identifierAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleunit_identifier"


    // $ANTLR start "entryRulenumeric_literal"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:704:1: entryRulenumeric_literal : rulenumeric_literal EOF ;
    public final void entryRulenumeric_literal() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:705:1: ( rulenumeric_literal EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:706:1: rulenumeric_literal EOF
            {
             before(grammarAccess.getNumeric_literalRule()); 
            pushFollow(FOLLOW_rulenumeric_literal_in_entryRulenumeric_literal1441);
            rulenumeric_literal();

            state._fsp--;

             after(grammarAccess.getNumeric_literalRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulenumeric_literal1448); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulenumeric_literal"


    // $ANTLR start "rulenumeric_literal"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:713:1: rulenumeric_literal : ( ( rule__Numeric_literal__Alternatives ) ) ;
    public final void rulenumeric_literal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:717:2: ( ( ( rule__Numeric_literal__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:718:1: ( ( rule__Numeric_literal__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:718:1: ( ( rule__Numeric_literal__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:719:1: ( rule__Numeric_literal__Alternatives )
            {
             before(grammarAccess.getNumeric_literalAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:720:1: ( rule__Numeric_literal__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:720:2: rule__Numeric_literal__Alternatives
            {
            pushFollow(FOLLOW_rule__Numeric_literal__Alternatives_in_rulenumeric_literal1474);
            rule__Numeric_literal__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getNumeric_literalAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulenumeric_literal"


    // $ANTLR start "entryRuleinteger_literal"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:732:1: entryRuleinteger_literal : ruleinteger_literal EOF ;
    public final void entryRuleinteger_literal() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:733:1: ( ruleinteger_literal EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:734:1: ruleinteger_literal EOF
            {
             before(grammarAccess.getInteger_literalRule()); 
            pushFollow(FOLLOW_ruleinteger_literal_in_entryRuleinteger_literal1501);
            ruleinteger_literal();

            state._fsp--;

             after(grammarAccess.getInteger_literalRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleinteger_literal1508); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleinteger_literal"


    // $ANTLR start "ruleinteger_literal"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:741:1: ruleinteger_literal : ( ( rule__Integer_literal__Alternatives ) ) ;
    public final void ruleinteger_literal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:745:2: ( ( ( rule__Integer_literal__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:746:1: ( ( rule__Integer_literal__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:746:1: ( ( rule__Integer_literal__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:747:1: ( rule__Integer_literal__Alternatives )
            {
             before(grammarAccess.getInteger_literalAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:748:1: ( rule__Integer_literal__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:748:2: rule__Integer_literal__Alternatives
            {
            pushFollow(FOLLOW_rule__Integer_literal__Alternatives_in_ruleinteger_literal1534);
            rule__Integer_literal__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getInteger_literalAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleinteger_literal"


    // $ANTLR start "entryRulereal_literal"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:760:1: entryRulereal_literal : rulereal_literal EOF ;
    public final void entryRulereal_literal() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:761:1: ( rulereal_literal EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:762:1: rulereal_literal EOF
            {
             before(grammarAccess.getReal_literalRule()); 
            pushFollow(FOLLOW_rulereal_literal_in_entryRulereal_literal1561);
            rulereal_literal();

            state._fsp--;

             after(grammarAccess.getReal_literalRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulereal_literal1568); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulereal_literal"


    // $ANTLR start "rulereal_literal"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:769:1: rulereal_literal : ( ( rule__Real_literal__RlnameAssignment ) ) ;
    public final void rulereal_literal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:773:2: ( ( ( rule__Real_literal__RlnameAssignment ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:774:1: ( ( rule__Real_literal__RlnameAssignment ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:774:1: ( ( rule__Real_literal__RlnameAssignment ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:775:1: ( rule__Real_literal__RlnameAssignment )
            {
             before(grammarAccess.getReal_literalAccess().getRlnameAssignment()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:776:1: ( rule__Real_literal__RlnameAssignment )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:776:2: rule__Real_literal__RlnameAssignment
            {
            pushFollow(FOLLOW_rule__Real_literal__RlnameAssignment_in_rulereal_literal1594);
            rule__Real_literal__RlnameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getReal_literalAccess().getRlnameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulereal_literal"


    // $ANTLR start "entryRuledecimal_integer_literal"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:788:1: entryRuledecimal_integer_literal : ruledecimal_integer_literal EOF ;
    public final void entryRuledecimal_integer_literal() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:789:1: ( ruledecimal_integer_literal EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:790:1: ruledecimal_integer_literal EOF
            {
             before(grammarAccess.getDecimal_integer_literalRule()); 
            pushFollow(FOLLOW_ruledecimal_integer_literal_in_entryRuledecimal_integer_literal1621);
            ruledecimal_integer_literal();

            state._fsp--;

             after(grammarAccess.getDecimal_integer_literalRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuledecimal_integer_literal1628); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuledecimal_integer_literal"


    // $ANTLR start "ruledecimal_integer_literal"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:797:1: ruledecimal_integer_literal : ( ( rule__Decimal_integer_literal__Group__0 ) ) ;
    public final void ruledecimal_integer_literal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:801:2: ( ( ( rule__Decimal_integer_literal__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:802:1: ( ( rule__Decimal_integer_literal__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:802:1: ( ( rule__Decimal_integer_literal__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:803:1: ( rule__Decimal_integer_literal__Group__0 )
            {
             before(grammarAccess.getDecimal_integer_literalAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:804:1: ( rule__Decimal_integer_literal__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:804:2: rule__Decimal_integer_literal__Group__0
            {
            pushFollow(FOLLOW_rule__Decimal_integer_literal__Group__0_in_ruledecimal_integer_literal1654);
            rule__Decimal_integer_literal__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDecimal_integer_literalAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruledecimal_integer_literal"


    // $ANTLR start "entryRuledecimal_real_literal"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:816:1: entryRuledecimal_real_literal : ruledecimal_real_literal EOF ;
    public final void entryRuledecimal_real_literal() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:817:1: ( ruledecimal_real_literal EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:818:1: ruledecimal_real_literal EOF
            {
             before(grammarAccess.getDecimal_real_literalRule()); 
            pushFollow(FOLLOW_ruledecimal_real_literal_in_entryRuledecimal_real_literal1681);
            ruledecimal_real_literal();

            state._fsp--;

             after(grammarAccess.getDecimal_real_literalRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuledecimal_real_literal1688); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuledecimal_real_literal"


    // $ANTLR start "ruledecimal_real_literal"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:825:1: ruledecimal_real_literal : ( ( rule__Decimal_real_literal__Group__0 ) ) ;
    public final void ruledecimal_real_literal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:829:2: ( ( ( rule__Decimal_real_literal__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:830:1: ( ( rule__Decimal_real_literal__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:830:1: ( ( rule__Decimal_real_literal__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:831:1: ( rule__Decimal_real_literal__Group__0 )
            {
             before(grammarAccess.getDecimal_real_literalAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:832:1: ( rule__Decimal_real_literal__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:832:2: rule__Decimal_real_literal__Group__0
            {
            pushFollow(FOLLOW_rule__Decimal_real_literal__Group__0_in_ruledecimal_real_literal1714);
            rule__Decimal_real_literal__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDecimal_real_literalAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruledecimal_real_literal"


    // $ANTLR start "entryRulenumeral"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:844:1: entryRulenumeral : rulenumeral EOF ;
    public final void entryRulenumeral() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:845:1: ( rulenumeral EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:846:1: rulenumeral EOF
            {
             before(grammarAccess.getNumeralRule()); 
            pushFollow(FOLLOW_rulenumeral_in_entryRulenumeral1741);
            rulenumeral();

            state._fsp--;

             after(grammarAccess.getNumeralRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulenumeral1748); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulenumeral"


    // $ANTLR start "rulenumeral"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:853:1: rulenumeral : ( ( rule__Numeral__Group__0 ) ) ;
    public final void rulenumeral() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:857:2: ( ( ( rule__Numeral__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:858:1: ( ( rule__Numeral__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:858:1: ( ( rule__Numeral__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:859:1: ( rule__Numeral__Group__0 )
            {
             before(grammarAccess.getNumeralAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:860:1: ( rule__Numeral__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:860:2: rule__Numeral__Group__0
            {
            pushFollow(FOLLOW_rule__Numeral__Group__0_in_rulenumeral1774);
            rule__Numeral__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNumeralAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulenumeral"


    // $ANTLR start "entryRuleexponent"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:872:1: entryRuleexponent : ruleexponent EOF ;
    public final void entryRuleexponent() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:873:1: ( ruleexponent EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:874:1: ruleexponent EOF
            {
             before(grammarAccess.getExponentRule()); 
            pushFollow(FOLLOW_ruleexponent_in_entryRuleexponent1801);
            ruleexponent();

            state._fsp--;

             after(grammarAccess.getExponentRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleexponent1808); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleexponent"


    // $ANTLR start "ruleexponent"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:881:1: ruleexponent : ( ( rule__Exponent__Alternatives ) ) ;
    public final void ruleexponent() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:885:2: ( ( ( rule__Exponent__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:886:1: ( ( rule__Exponent__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:886:1: ( ( rule__Exponent__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:887:1: ( rule__Exponent__Alternatives )
            {
             before(grammarAccess.getExponentAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:888:1: ( rule__Exponent__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:888:2: rule__Exponent__Alternatives
            {
            pushFollow(FOLLOW_rule__Exponent__Alternatives_in_ruleexponent1834);
            rule__Exponent__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getExponentAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleexponent"


    // $ANTLR start "entryRulepositive_exponent"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:900:1: entryRulepositive_exponent : rulepositive_exponent EOF ;
    public final void entryRulepositive_exponent() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:901:1: ( rulepositive_exponent EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:902:1: rulepositive_exponent EOF
            {
             before(grammarAccess.getPositive_exponentRule()); 
            pushFollow(FOLLOW_rulepositive_exponent_in_entryRulepositive_exponent1861);
            rulepositive_exponent();

            state._fsp--;

             after(grammarAccess.getPositive_exponentRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulepositive_exponent1868); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulepositive_exponent"


    // $ANTLR start "rulepositive_exponent"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:909:1: rulepositive_exponent : ( ( rule__Positive_exponent__Group__0 ) ) ;
    public final void rulepositive_exponent() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:913:2: ( ( ( rule__Positive_exponent__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:914:1: ( ( rule__Positive_exponent__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:914:1: ( ( rule__Positive_exponent__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:915:1: ( rule__Positive_exponent__Group__0 )
            {
             before(grammarAccess.getPositive_exponentAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:916:1: ( rule__Positive_exponent__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:916:2: rule__Positive_exponent__Group__0
            {
            pushFollow(FOLLOW_rule__Positive_exponent__Group__0_in_rulepositive_exponent1894);
            rule__Positive_exponent__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPositive_exponentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulepositive_exponent"


    // $ANTLR start "entryRulebased_integer_literal"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:928:1: entryRulebased_integer_literal : rulebased_integer_literal EOF ;
    public final void entryRulebased_integer_literal() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:929:1: ( rulebased_integer_literal EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:930:1: rulebased_integer_literal EOF
            {
             before(grammarAccess.getBased_integer_literalRule()); 
            pushFollow(FOLLOW_rulebased_integer_literal_in_entryRulebased_integer_literal1921);
            rulebased_integer_literal();

            state._fsp--;

             after(grammarAccess.getBased_integer_literalRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulebased_integer_literal1928); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulebased_integer_literal"


    // $ANTLR start "rulebased_integer_literal"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:937:1: rulebased_integer_literal : ( ( rule__Based_integer_literal__Group__0 ) ) ;
    public final void rulebased_integer_literal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:941:2: ( ( ( rule__Based_integer_literal__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:942:1: ( ( rule__Based_integer_literal__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:942:1: ( ( rule__Based_integer_literal__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:943:1: ( rule__Based_integer_literal__Group__0 )
            {
             before(grammarAccess.getBased_integer_literalAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:944:1: ( rule__Based_integer_literal__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:944:2: rule__Based_integer_literal__Group__0
            {
            pushFollow(FOLLOW_rule__Based_integer_literal__Group__0_in_rulebased_integer_literal1954);
            rule__Based_integer_literal__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBased_integer_literalAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulebased_integer_literal"


    // $ANTLR start "entryRulebase"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:956:1: entryRulebase : rulebase EOF ;
    public final void entryRulebase() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:957:1: ( rulebase EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:958:1: rulebase EOF
            {
             before(grammarAccess.getBaseRule()); 
            pushFollow(FOLLOW_rulebase_in_entryRulebase1981);
            rulebase();

            state._fsp--;

             after(grammarAccess.getBaseRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulebase1988); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulebase"


    // $ANTLR start "rulebase"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:965:1: rulebase : ( ( rule__Base__Group__0 ) ) ;
    public final void rulebase() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:969:2: ( ( ( rule__Base__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:970:1: ( ( rule__Base__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:970:1: ( ( rule__Base__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:971:1: ( rule__Base__Group__0 )
            {
             before(grammarAccess.getBaseAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:972:1: ( rule__Base__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:972:2: rule__Base__Group__0
            {
            pushFollow(FOLLOW_rule__Base__Group__0_in_rulebase2014);
            rule__Base__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBaseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulebase"


    // $ANTLR start "entryRulebased_numeral"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:984:1: entryRulebased_numeral : rulebased_numeral EOF ;
    public final void entryRulebased_numeral() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:985:1: ( rulebased_numeral EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:986:1: rulebased_numeral EOF
            {
             before(grammarAccess.getBased_numeralRule()); 
            pushFollow(FOLLOW_rulebased_numeral_in_entryRulebased_numeral2041);
            rulebased_numeral();

            state._fsp--;

             after(grammarAccess.getBased_numeralRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulebased_numeral2048); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulebased_numeral"


    // $ANTLR start "rulebased_numeral"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:993:1: rulebased_numeral : ( ( rule__Based_numeral__Group__0 ) ) ;
    public final void rulebased_numeral() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:997:2: ( ( ( rule__Based_numeral__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:998:1: ( ( rule__Based_numeral__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:998:1: ( ( rule__Based_numeral__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:999:1: ( rule__Based_numeral__Group__0 )
            {
             before(grammarAccess.getBased_numeralAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1000:1: ( rule__Based_numeral__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1000:2: rule__Based_numeral__Group__0
            {
            pushFollow(FOLLOW_rule__Based_numeral__Group__0_in_rulebased_numeral2074);
            rule__Based_numeral__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBased_numeralAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulebased_numeral"


    // $ANTLR start "entryRuleinteger"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1012:1: entryRuleinteger : ruleinteger EOF ;
    public final void entryRuleinteger() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1013:1: ( ruleinteger EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1014:1: ruleinteger EOF
            {
             before(grammarAccess.getIntegerRule()); 
            pushFollow(FOLLOW_ruleinteger_in_entryRuleinteger2101);
            ruleinteger();

            state._fsp--;

             after(grammarAccess.getIntegerRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleinteger2108); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleinteger"


    // $ANTLR start "ruleinteger"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1021:1: ruleinteger : ( ( rule__Integer__Alternatives ) ) ;
    public final void ruleinteger() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1025:2: ( ( ( rule__Integer__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1026:1: ( ( rule__Integer__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1026:1: ( ( rule__Integer__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1027:1: ( rule__Integer__Alternatives )
            {
             before(grammarAccess.getIntegerAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1028:1: ( rule__Integer__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1028:2: rule__Integer__Alternatives
            {
            pushFollow(FOLLOW_rule__Integer__Alternatives_in_ruleinteger2134);
            rule__Integer__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getIntegerAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleinteger"


    // $ANTLR start "entryRuledispatch_condition"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1040:1: entryRuledispatch_condition : ruledispatch_condition EOF ;
    public final void entryRuledispatch_condition() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1041:1: ( ruledispatch_condition EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1042:1: ruledispatch_condition EOF
            {
             before(grammarAccess.getDispatch_conditionRule()); 
            pushFollow(FOLLOW_ruledispatch_condition_in_entryRuledispatch_condition2161);
            ruledispatch_condition();

            state._fsp--;

             after(grammarAccess.getDispatch_conditionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuledispatch_condition2168); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuledispatch_condition"


    // $ANTLR start "ruledispatch_condition"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1049:1: ruledispatch_condition : ( ( rule__Dispatch_condition__Group__0 ) ) ;
    public final void ruledispatch_condition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1053:2: ( ( ( rule__Dispatch_condition__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1054:1: ( ( rule__Dispatch_condition__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1054:1: ( ( rule__Dispatch_condition__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1055:1: ( rule__Dispatch_condition__Group__0 )
            {
             before(grammarAccess.getDispatch_conditionAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1056:1: ( rule__Dispatch_condition__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1056:2: rule__Dispatch_condition__Group__0
            {
            pushFollow(FOLLOW_rule__Dispatch_condition__Group__0_in_ruledispatch_condition2194);
            rule__Dispatch_condition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDispatch_conditionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruledispatch_condition"


    // $ANTLR start "entryRuledispatch_trigger_condition"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1068:1: entryRuledispatch_trigger_condition : ruledispatch_trigger_condition EOF ;
    public final void entryRuledispatch_trigger_condition() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1069:1: ( ruledispatch_trigger_condition EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1070:1: ruledispatch_trigger_condition EOF
            {
             before(grammarAccess.getDispatch_trigger_conditionRule()); 
            pushFollow(FOLLOW_ruledispatch_trigger_condition_in_entryRuledispatch_trigger_condition2221);
            ruledispatch_trigger_condition();

            state._fsp--;

             after(grammarAccess.getDispatch_trigger_conditionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuledispatch_trigger_condition2228); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuledispatch_trigger_condition"


    // $ANTLR start "ruledispatch_trigger_condition"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1077:1: ruledispatch_trigger_condition : ( ( rule__Dispatch_trigger_condition__Alternatives ) ) ;
    public final void ruledispatch_trigger_condition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1081:2: ( ( ( rule__Dispatch_trigger_condition__Alternatives ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1082:1: ( ( rule__Dispatch_trigger_condition__Alternatives ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1082:1: ( ( rule__Dispatch_trigger_condition__Alternatives ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1083:1: ( rule__Dispatch_trigger_condition__Alternatives )
            {
             before(grammarAccess.getDispatch_trigger_conditionAccess().getAlternatives()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1084:1: ( rule__Dispatch_trigger_condition__Alternatives )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1084:2: rule__Dispatch_trigger_condition__Alternatives
            {
            pushFollow(FOLLOW_rule__Dispatch_trigger_condition__Alternatives_in_ruledispatch_trigger_condition2254);
            rule__Dispatch_trigger_condition__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDispatch_trigger_conditionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruledispatch_trigger_condition"


    // $ANTLR start "entryRuledispatch_trigger_logical_expression"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1096:1: entryRuledispatch_trigger_logical_expression : ruledispatch_trigger_logical_expression EOF ;
    public final void entryRuledispatch_trigger_logical_expression() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1097:1: ( ruledispatch_trigger_logical_expression EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1098:1: ruledispatch_trigger_logical_expression EOF
            {
             before(grammarAccess.getDispatch_trigger_logical_expressionRule()); 
            pushFollow(FOLLOW_ruledispatch_trigger_logical_expression_in_entryRuledispatch_trigger_logical_expression2281);
            ruledispatch_trigger_logical_expression();

            state._fsp--;

             after(grammarAccess.getDispatch_trigger_logical_expressionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuledispatch_trigger_logical_expression2288); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuledispatch_trigger_logical_expression"


    // $ANTLR start "ruledispatch_trigger_logical_expression"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1105:1: ruledispatch_trigger_logical_expression : ( ( rule__Dispatch_trigger_logical_expression__Group__0 ) ) ;
    public final void ruledispatch_trigger_logical_expression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1109:2: ( ( ( rule__Dispatch_trigger_logical_expression__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1110:1: ( ( rule__Dispatch_trigger_logical_expression__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1110:1: ( ( rule__Dispatch_trigger_logical_expression__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1111:1: ( rule__Dispatch_trigger_logical_expression__Group__0 )
            {
             before(grammarAccess.getDispatch_trigger_logical_expressionAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1112:1: ( rule__Dispatch_trigger_logical_expression__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1112:2: rule__Dispatch_trigger_logical_expression__Group__0
            {
            pushFollow(FOLLOW_rule__Dispatch_trigger_logical_expression__Group__0_in_ruledispatch_trigger_logical_expression2314);
            rule__Dispatch_trigger_logical_expression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDispatch_trigger_logical_expressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruledispatch_trigger_logical_expression"


    // $ANTLR start "entryRuledispatch_conjunction"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1124:1: entryRuledispatch_conjunction : ruledispatch_conjunction EOF ;
    public final void entryRuledispatch_conjunction() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1125:1: ( ruledispatch_conjunction EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1126:1: ruledispatch_conjunction EOF
            {
             before(grammarAccess.getDispatch_conjunctionRule()); 
            pushFollow(FOLLOW_ruledispatch_conjunction_in_entryRuledispatch_conjunction2341);
            ruledispatch_conjunction();

            state._fsp--;

             after(grammarAccess.getDispatch_conjunctionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuledispatch_conjunction2348); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuledispatch_conjunction"


    // $ANTLR start "ruledispatch_conjunction"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1133:1: ruledispatch_conjunction : ( ( rule__Dispatch_conjunction__Group__0 ) ) ;
    public final void ruledispatch_conjunction() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1137:2: ( ( ( rule__Dispatch_conjunction__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1138:1: ( ( rule__Dispatch_conjunction__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1138:1: ( ( rule__Dispatch_conjunction__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1139:1: ( rule__Dispatch_conjunction__Group__0 )
            {
             before(grammarAccess.getDispatch_conjunctionAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1140:1: ( rule__Dispatch_conjunction__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1140:2: rule__Dispatch_conjunction__Group__0
            {
            pushFollow(FOLLOW_rule__Dispatch_conjunction__Group__0_in_ruledispatch_conjunction2374);
            rule__Dispatch_conjunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDispatch_conjunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruledispatch_conjunction"


    // $ANTLR start "entryRulecompletion_relative_timeout_condition_and_catch"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1152:1: entryRulecompletion_relative_timeout_condition_and_catch : rulecompletion_relative_timeout_condition_and_catch EOF ;
    public final void entryRulecompletion_relative_timeout_condition_and_catch() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1153:1: ( rulecompletion_relative_timeout_condition_and_catch EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1154:1: rulecompletion_relative_timeout_condition_and_catch EOF
            {
             before(grammarAccess.getCompletion_relative_timeout_condition_and_catchRule()); 
            pushFollow(FOLLOW_rulecompletion_relative_timeout_condition_and_catch_in_entryRulecompletion_relative_timeout_condition_and_catch2401);
            rulecompletion_relative_timeout_condition_and_catch();

            state._fsp--;

             after(grammarAccess.getCompletion_relative_timeout_condition_and_catchRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulecompletion_relative_timeout_condition_and_catch2408); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulecompletion_relative_timeout_condition_and_catch"


    // $ANTLR start "rulecompletion_relative_timeout_condition_and_catch"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1161:1: rulecompletion_relative_timeout_condition_and_catch : ( ( rule__Completion_relative_timeout_condition_and_catch__Group__0 ) ) ;
    public final void rulecompletion_relative_timeout_condition_and_catch() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1165:2: ( ( ( rule__Completion_relative_timeout_condition_and_catch__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1166:1: ( ( rule__Completion_relative_timeout_condition_and_catch__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1166:1: ( ( rule__Completion_relative_timeout_condition_and_catch__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1167:1: ( rule__Completion_relative_timeout_condition_and_catch__Group__0 )
            {
             before(grammarAccess.getCompletion_relative_timeout_condition_and_catchAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1168:1: ( rule__Completion_relative_timeout_condition_and_catch__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1168:2: rule__Completion_relative_timeout_condition_and_catch__Group__0
            {
            pushFollow(FOLLOW_rule__Completion_relative_timeout_condition_and_catch__Group__0_in_rulecompletion_relative_timeout_condition_and_catch2434);
            rule__Completion_relative_timeout_condition_and_catch__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCompletion_relative_timeout_condition_and_catchAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulecompletion_relative_timeout_condition_and_catch"


    // $ANTLR start "entryRuledispatch_trigger"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1180:1: entryRuledispatch_trigger : ruledispatch_trigger EOF ;
    public final void entryRuledispatch_trigger() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1181:1: ( ruledispatch_trigger EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1182:1: ruledispatch_trigger EOF
            {
             before(grammarAccess.getDispatch_triggerRule()); 
            pushFollow(FOLLOW_ruledispatch_trigger_in_entryRuledispatch_trigger2461);
            ruledispatch_trigger();

            state._fsp--;

             after(grammarAccess.getDispatch_triggerRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuledispatch_trigger2468); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuledispatch_trigger"


    // $ANTLR start "ruledispatch_trigger"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1189:1: ruledispatch_trigger : ( ( rule__Dispatch_trigger__DtnameAssignment ) ) ;
    public final void ruledispatch_trigger() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1193:2: ( ( ( rule__Dispatch_trigger__DtnameAssignment ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1194:1: ( ( rule__Dispatch_trigger__DtnameAssignment ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1194:1: ( ( rule__Dispatch_trigger__DtnameAssignment ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1195:1: ( rule__Dispatch_trigger__DtnameAssignment )
            {
             before(grammarAccess.getDispatch_triggerAccess().getDtnameAssignment()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1196:1: ( rule__Dispatch_trigger__DtnameAssignment )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1196:2: rule__Dispatch_trigger__DtnameAssignment
            {
            pushFollow(FOLLOW_rule__Dispatch_trigger__DtnameAssignment_in_ruledispatch_trigger2494);
            rule__Dispatch_trigger__DtnameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getDispatch_triggerAccess().getDtnameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruledispatch_trigger"


    // $ANTLR start "entryRulefrozen_ports"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1208:1: entryRulefrozen_ports : rulefrozen_ports EOF ;
    public final void entryRulefrozen_ports() throws RecognitionException {
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1209:1: ( rulefrozen_ports EOF )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1210:1: rulefrozen_ports EOF
            {
             before(grammarAccess.getFrozen_portsRule()); 
            pushFollow(FOLLOW_rulefrozen_ports_in_entryRulefrozen_ports2521);
            rulefrozen_ports();

            state._fsp--;

             after(grammarAccess.getFrozen_portsRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulefrozen_ports2528); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulefrozen_ports"


    // $ANTLR start "rulefrozen_ports"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1217:1: rulefrozen_ports : ( ( rule__Frozen_ports__Group__0 ) ) ;
    public final void rulefrozen_ports() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1221:2: ( ( ( rule__Frozen_ports__Group__0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1222:1: ( ( rule__Frozen_ports__Group__0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1222:1: ( ( rule__Frozen_ports__Group__0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1223:1: ( rule__Frozen_ports__Group__0 )
            {
             before(grammarAccess.getFrozen_portsAccess().getGroup()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1224:1: ( rule__Frozen_ports__Group__0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1224:2: rule__Frozen_ports__Group__0
            {
            pushFollow(FOLLOW_rule__Frozen_ports__Group__0_in_rulefrozen_ports2554);
            rule__Frozen_ports__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFrozen_portsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulefrozen_ports"


    // $ANTLR start "rule__Guard__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1236:1: rule__Guard__Alternatives : ( ( ( rule__Guard__DispatchAssignment_0 ) ) | ( ( rule__Guard__ExecuteAssignment_1 ) ) );
    public final void rule__Guard__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1240:1: ( ( ( rule__Guard__DispatchAssignment_0 ) ) | ( ( rule__Guard__ExecuteAssignment_1 ) ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==54) ) {
                alt1=1;
            }
            else if ( ((LA1_0>=RULE_STRING_LITERAL && LA1_0<=RULE_DIGIT)||LA1_0==RULE_INT||LA1_0==18||(LA1_0>=25 && LA1_0<=26)||LA1_0==37||(LA1_0>=43 && LA1_0<=46)) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1241:1: ( ( rule__Guard__DispatchAssignment_0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1241:1: ( ( rule__Guard__DispatchAssignment_0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1242:1: ( rule__Guard__DispatchAssignment_0 )
                    {
                     before(grammarAccess.getGuardAccess().getDispatchAssignment_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1243:1: ( rule__Guard__DispatchAssignment_0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1243:2: rule__Guard__DispatchAssignment_0
                    {
                    pushFollow(FOLLOW_rule__Guard__DispatchAssignment_0_in_rule__Guard__Alternatives2590);
                    rule__Guard__DispatchAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getGuardAccess().getDispatchAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1247:6: ( ( rule__Guard__ExecuteAssignment_1 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1247:6: ( ( rule__Guard__ExecuteAssignment_1 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1248:1: ( rule__Guard__ExecuteAssignment_1 )
                    {
                     before(grammarAccess.getGuardAccess().getExecuteAssignment_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1249:1: ( rule__Guard__ExecuteAssignment_1 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1249:2: rule__Guard__ExecuteAssignment_1
                    {
                    pushFollow(FOLLOW_rule__Guard__ExecuteAssignment_1_in_rule__Guard__Alternatives2608);
                    rule__Guard__ExecuteAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getGuardAccess().getExecuteAssignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guard__Alternatives"


    // $ANTLR start "rule__Execute_condition__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1258:1: rule__Execute_condition__Alternatives : ( ( ( rule__Execute_condition__Ecname1Assignment_0 ) ) | ( ( rule__Execute_condition__Ecname2Assignment_1 ) ) | ( ( rule__Execute_condition__Ecname3Assignment_2 ) ) );
    public final void rule__Execute_condition__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1262:1: ( ( ( rule__Execute_condition__Ecname1Assignment_0 ) ) | ( ( rule__Execute_condition__Ecname2Assignment_1 ) ) | ( ( rule__Execute_condition__Ecname3Assignment_2 ) ) )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 25:
                {
                alt2=1;
                }
                break;
            case 26:
                {
                alt2=2;
                }
                break;
            case RULE_STRING_LITERAL:
            case RULE_DIGIT:
            case RULE_INT:
            case 18:
            case 37:
            case 43:
            case 44:
            case 45:
            case 46:
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1263:1: ( ( rule__Execute_condition__Ecname1Assignment_0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1263:1: ( ( rule__Execute_condition__Ecname1Assignment_0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1264:1: ( rule__Execute_condition__Ecname1Assignment_0 )
                    {
                     before(grammarAccess.getExecute_conditionAccess().getEcname1Assignment_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1265:1: ( rule__Execute_condition__Ecname1Assignment_0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1265:2: rule__Execute_condition__Ecname1Assignment_0
                    {
                    pushFollow(FOLLOW_rule__Execute_condition__Ecname1Assignment_0_in_rule__Execute_condition__Alternatives2641);
                    rule__Execute_condition__Ecname1Assignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExecute_conditionAccess().getEcname1Assignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1269:6: ( ( rule__Execute_condition__Ecname2Assignment_1 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1269:6: ( ( rule__Execute_condition__Ecname2Assignment_1 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1270:1: ( rule__Execute_condition__Ecname2Assignment_1 )
                    {
                     before(grammarAccess.getExecute_conditionAccess().getEcname2Assignment_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1271:1: ( rule__Execute_condition__Ecname2Assignment_1 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1271:2: rule__Execute_condition__Ecname2Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Execute_condition__Ecname2Assignment_1_in_rule__Execute_condition__Alternatives2659);
                    rule__Execute_condition__Ecname2Assignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getExecute_conditionAccess().getEcname2Assignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1275:6: ( ( rule__Execute_condition__Ecname3Assignment_2 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1275:6: ( ( rule__Execute_condition__Ecname3Assignment_2 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1276:1: ( rule__Execute_condition__Ecname3Assignment_2 )
                    {
                     before(grammarAccess.getExecute_conditionAccess().getEcname3Assignment_2()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1277:1: ( rule__Execute_condition__Ecname3Assignment_2 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1277:2: rule__Execute_condition__Ecname3Assignment_2
                    {
                    pushFollow(FOLLOW_rule__Execute_condition__Ecname3Assignment_2_in_rule__Execute_condition__Alternatives2677);
                    rule__Execute_condition__Ecname3Assignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getExecute_conditionAccess().getEcname3Assignment_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Execute_condition__Alternatives"


    // $ANTLR start "rule__Factor__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1286:1: rule__Factor__Alternatives : ( ( ( rule__Factor__Group_0__0 ) ) | ( ( rule__Factor__Group_1__0 ) ) | ( ( rule__Factor__Group_2__0 ) ) );
    public final void rule__Factor__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1290:1: ( ( ( rule__Factor__Group_0__0 ) ) | ( ( rule__Factor__Group_1__0 ) ) | ( ( rule__Factor__Group_2__0 ) ) )
            int alt3=3;
            switch ( input.LA(1) ) {
            case RULE_STRING_LITERAL:
            case RULE_DIGIT:
            case RULE_INT:
            case 45:
            case 46:
                {
                alt3=1;
                }
                break;
            case 43:
                {
                alt3=2;
                }
                break;
            case 44:
                {
                alt3=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1291:1: ( ( rule__Factor__Group_0__0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1291:1: ( ( rule__Factor__Group_0__0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1292:1: ( rule__Factor__Group_0__0 )
                    {
                     before(grammarAccess.getFactorAccess().getGroup_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1293:1: ( rule__Factor__Group_0__0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1293:2: rule__Factor__Group_0__0
                    {
                    pushFollow(FOLLOW_rule__Factor__Group_0__0_in_rule__Factor__Alternatives2710);
                    rule__Factor__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getFactorAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1297:6: ( ( rule__Factor__Group_1__0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1297:6: ( ( rule__Factor__Group_1__0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1298:1: ( rule__Factor__Group_1__0 )
                    {
                     before(grammarAccess.getFactorAccess().getGroup_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1299:1: ( rule__Factor__Group_1__0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1299:2: rule__Factor__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__Factor__Group_1__0_in_rule__Factor__Alternatives2728);
                    rule__Factor__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getFactorAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1303:6: ( ( rule__Factor__Group_2__0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1303:6: ( ( rule__Factor__Group_2__0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1304:1: ( rule__Factor__Group_2__0 )
                    {
                     before(grammarAccess.getFactorAccess().getGroup_2()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1305:1: ( rule__Factor__Group_2__0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1305:2: rule__Factor__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__Factor__Group_2__0_in_rule__Factor__Alternatives2746);
                    rule__Factor__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getFactorAccess().getGroup_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Alternatives"


    // $ANTLR start "rule__Value__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1314:1: rule__Value__Alternatives : ( ( ( rule__Value__Vname1Assignment_0 ) ) | ( ( rule__Value__Vname2Assignment_1 ) ) );
    public final void rule__Value__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1318:1: ( ( ( rule__Value__Vname1Assignment_0 ) ) | ( ( rule__Value__Vname2Assignment_1 ) ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_STRING_LITERAL) ) {
                int LA4_1 = input.LA(2);

                if ( (LA4_1==14) ) {
                    alt4=2;
                }
                else if ( (LA4_1==EOF||LA4_1==18||(LA4_1>=22 && LA4_1<=23)||(LA4_1>=27 && LA4_1<=42)) ) {
                    alt4=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA4_0==RULE_DIGIT||LA4_0==RULE_INT||(LA4_0>=45 && LA4_0<=46)) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1319:1: ( ( rule__Value__Vname1Assignment_0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1319:1: ( ( rule__Value__Vname1Assignment_0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1320:1: ( rule__Value__Vname1Assignment_0 )
                    {
                     before(grammarAccess.getValueAccess().getVname1Assignment_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1321:1: ( rule__Value__Vname1Assignment_0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1321:2: rule__Value__Vname1Assignment_0
                    {
                    pushFollow(FOLLOW_rule__Value__Vname1Assignment_0_in_rule__Value__Alternatives2779);
                    rule__Value__Vname1Assignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getValueAccess().getVname1Assignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1325:6: ( ( rule__Value__Vname2Assignment_1 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1325:6: ( ( rule__Value__Vname2Assignment_1 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1326:1: ( rule__Value__Vname2Assignment_1 )
                    {
                     before(grammarAccess.getValueAccess().getVname2Assignment_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1327:1: ( rule__Value__Vname2Assignment_1 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1327:2: rule__Value__Vname2Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Value__Vname2Assignment_1_in_rule__Value__Alternatives2797);
                    rule__Value__Vname2Assignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getValueAccess().getVname2Assignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Alternatives"


    // $ANTLR start "rule__Value_variable__Alternatives_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1336:1: rule__Value_variable__Alternatives_1 : ( ( ( rule__Value_variable__Vvname1Assignment_1_0 ) ) | ( ( rule__Value_variable__Vvname2Assignment_1_1 ) ) | ( ( rule__Value_variable__Vvname3Assignment_1_2 ) ) );
    public final void rule__Value_variable__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1340:1: ( ( ( rule__Value_variable__Vvname1Assignment_1_0 ) ) | ( ( rule__Value_variable__Vvname2Assignment_1_1 ) ) | ( ( rule__Value_variable__Vvname3Assignment_1_2 ) ) )
            int alt5=3;
            switch ( input.LA(1) ) {
            case 27:
                {
                alt5=1;
                }
                break;
            case 28:
                {
                alt5=2;
                }
                break;
            case 29:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1341:1: ( ( rule__Value_variable__Vvname1Assignment_1_0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1341:1: ( ( rule__Value_variable__Vvname1Assignment_1_0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1342:1: ( rule__Value_variable__Vvname1Assignment_1_0 )
                    {
                     before(grammarAccess.getValue_variableAccess().getVvname1Assignment_1_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1343:1: ( rule__Value_variable__Vvname1Assignment_1_0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1343:2: rule__Value_variable__Vvname1Assignment_1_0
                    {
                    pushFollow(FOLLOW_rule__Value_variable__Vvname1Assignment_1_0_in_rule__Value_variable__Alternatives_12830);
                    rule__Value_variable__Vvname1Assignment_1_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getValue_variableAccess().getVvname1Assignment_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1347:6: ( ( rule__Value_variable__Vvname2Assignment_1_1 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1347:6: ( ( rule__Value_variable__Vvname2Assignment_1_1 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1348:1: ( rule__Value_variable__Vvname2Assignment_1_1 )
                    {
                     before(grammarAccess.getValue_variableAccess().getVvname2Assignment_1_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1349:1: ( rule__Value_variable__Vvname2Assignment_1_1 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1349:2: rule__Value_variable__Vvname2Assignment_1_1
                    {
                    pushFollow(FOLLOW_rule__Value_variable__Vvname2Assignment_1_1_in_rule__Value_variable__Alternatives_12848);
                    rule__Value_variable__Vvname2Assignment_1_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getValue_variableAccess().getVvname2Assignment_1_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1353:6: ( ( rule__Value_variable__Vvname3Assignment_1_2 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1353:6: ( ( rule__Value_variable__Vvname3Assignment_1_2 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1354:1: ( rule__Value_variable__Vvname3Assignment_1_2 )
                    {
                     before(grammarAccess.getValue_variableAccess().getVvname3Assignment_1_2()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1355:1: ( rule__Value_variable__Vvname3Assignment_1_2 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1355:2: rule__Value_variable__Vvname3Assignment_1_2
                    {
                    pushFollow(FOLLOW_rule__Value_variable__Vvname3Assignment_1_2_in_rule__Value_variable__Alternatives_12866);
                    rule__Value_variable__Vvname3Assignment_1_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getValue_variableAccess().getVvname3Assignment_1_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_variable__Alternatives_1"


    // $ANTLR start "rule__Value_constant__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1364:1: rule__Value_constant__Alternatives : ( ( ( rule__Value_constant__Vcname1Assignment_0 ) ) | ( ( rule__Value_constant__Vcname2Assignment_1 ) ) | ( ( rule__Value_constant__Group_2__0 ) ) );
    public final void rule__Value_constant__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1368:1: ( ( ( rule__Value_constant__Vcname1Assignment_0 ) ) | ( ( rule__Value_constant__Vcname2Assignment_1 ) ) | ( ( rule__Value_constant__Group_2__0 ) ) )
            int alt6=3;
            switch ( input.LA(1) ) {
            case 45:
            case 46:
                {
                alt6=1;
                }
                break;
            case RULE_DIGIT:
            case RULE_INT:
                {
                alt6=2;
                }
                break;
            case RULE_STRING_LITERAL:
                {
                alt6=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1369:1: ( ( rule__Value_constant__Vcname1Assignment_0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1369:1: ( ( rule__Value_constant__Vcname1Assignment_0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1370:1: ( rule__Value_constant__Vcname1Assignment_0 )
                    {
                     before(grammarAccess.getValue_constantAccess().getVcname1Assignment_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1371:1: ( rule__Value_constant__Vcname1Assignment_0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1371:2: rule__Value_constant__Vcname1Assignment_0
                    {
                    pushFollow(FOLLOW_rule__Value_constant__Vcname1Assignment_0_in_rule__Value_constant__Alternatives2899);
                    rule__Value_constant__Vcname1Assignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getValue_constantAccess().getVcname1Assignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1375:6: ( ( rule__Value_constant__Vcname2Assignment_1 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1375:6: ( ( rule__Value_constant__Vcname2Assignment_1 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1376:1: ( rule__Value_constant__Vcname2Assignment_1 )
                    {
                     before(grammarAccess.getValue_constantAccess().getVcname2Assignment_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1377:1: ( rule__Value_constant__Vcname2Assignment_1 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1377:2: rule__Value_constant__Vcname2Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Value_constant__Vcname2Assignment_1_in_rule__Value_constant__Alternatives2917);
                    rule__Value_constant__Vcname2Assignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getValue_constantAccess().getVcname2Assignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1381:6: ( ( rule__Value_constant__Group_2__0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1381:6: ( ( rule__Value_constant__Group_2__0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1382:1: ( rule__Value_constant__Group_2__0 )
                    {
                     before(grammarAccess.getValue_constantAccess().getGroup_2()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1383:1: ( rule__Value_constant__Group_2__0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1383:2: rule__Value_constant__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__Value_constant__Group_2__0_in_rule__Value_constant__Alternatives2935);
                    rule__Value_constant__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getValue_constantAccess().getGroup_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_constant__Alternatives"


    // $ANTLR start "rule__Logical_operator__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1392:1: rule__Logical_operator__Alternatives : ( ( ( rule__Logical_operator__Loname1Assignment_0 ) ) | ( ( rule__Logical_operator__Loname2Assignment_1 ) ) | ( ( rule__Logical_operator__Loname3Assignment_2 ) ) );
    public final void rule__Logical_operator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1396:1: ( ( ( rule__Logical_operator__Loname1Assignment_0 ) ) | ( ( rule__Logical_operator__Loname2Assignment_1 ) ) | ( ( rule__Logical_operator__Loname3Assignment_2 ) ) )
            int alt7=3;
            switch ( input.LA(1) ) {
            case 23:
                {
                alt7=1;
                }
                break;
            case 22:
                {
                alt7=2;
                }
                break;
            case 30:
                {
                alt7=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1397:1: ( ( rule__Logical_operator__Loname1Assignment_0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1397:1: ( ( rule__Logical_operator__Loname1Assignment_0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1398:1: ( rule__Logical_operator__Loname1Assignment_0 )
                    {
                     before(grammarAccess.getLogical_operatorAccess().getLoname1Assignment_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1399:1: ( rule__Logical_operator__Loname1Assignment_0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1399:2: rule__Logical_operator__Loname1Assignment_0
                    {
                    pushFollow(FOLLOW_rule__Logical_operator__Loname1Assignment_0_in_rule__Logical_operator__Alternatives2968);
                    rule__Logical_operator__Loname1Assignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogical_operatorAccess().getLoname1Assignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1403:6: ( ( rule__Logical_operator__Loname2Assignment_1 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1403:6: ( ( rule__Logical_operator__Loname2Assignment_1 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1404:1: ( rule__Logical_operator__Loname2Assignment_1 )
                    {
                     before(grammarAccess.getLogical_operatorAccess().getLoname2Assignment_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1405:1: ( rule__Logical_operator__Loname2Assignment_1 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1405:2: rule__Logical_operator__Loname2Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Logical_operator__Loname2Assignment_1_in_rule__Logical_operator__Alternatives2986);
                    rule__Logical_operator__Loname2Assignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogical_operatorAccess().getLoname2Assignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1409:6: ( ( rule__Logical_operator__Loname3Assignment_2 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1409:6: ( ( rule__Logical_operator__Loname3Assignment_2 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1410:1: ( rule__Logical_operator__Loname3Assignment_2 )
                    {
                     before(grammarAccess.getLogical_operatorAccess().getLoname3Assignment_2()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1411:1: ( rule__Logical_operator__Loname3Assignment_2 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1411:2: rule__Logical_operator__Loname3Assignment_2
                    {
                    pushFollow(FOLLOW_rule__Logical_operator__Loname3Assignment_2_in_rule__Logical_operator__Alternatives3004);
                    rule__Logical_operator__Loname3Assignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getLogical_operatorAccess().getLoname3Assignment_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Logical_operator__Alternatives"


    // $ANTLR start "rule__Relational_operator__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1420:1: rule__Relational_operator__Alternatives : ( ( ( rule__Relational_operator__Roname1Assignment_0 ) ) | ( ( rule__Relational_operator__Roname2Assignment_1 ) ) | ( ( rule__Relational_operator__Roname3Assignment_2 ) ) | ( ( rule__Relational_operator__Roname4Assignment_3 ) ) | ( ( rule__Relational_operator__Roname5Assignment_4 ) ) | ( ( rule__Relational_operator__Roname6Assignment_5 ) ) );
    public final void rule__Relational_operator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1424:1: ( ( ( rule__Relational_operator__Roname1Assignment_0 ) ) | ( ( rule__Relational_operator__Roname2Assignment_1 ) ) | ( ( rule__Relational_operator__Roname3Assignment_2 ) ) | ( ( rule__Relational_operator__Roname4Assignment_3 ) ) | ( ( rule__Relational_operator__Roname5Assignment_4 ) ) | ( ( rule__Relational_operator__Roname6Assignment_5 ) ) )
            int alt8=6;
            switch ( input.LA(1) ) {
            case 31:
                {
                alt8=1;
                }
                break;
            case 32:
                {
                alt8=2;
                }
                break;
            case 33:
                {
                alt8=3;
                }
                break;
            case 34:
                {
                alt8=4;
                }
                break;
            case 35:
                {
                alt8=5;
                }
                break;
            case 36:
                {
                alt8=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1425:1: ( ( rule__Relational_operator__Roname1Assignment_0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1425:1: ( ( rule__Relational_operator__Roname1Assignment_0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1426:1: ( rule__Relational_operator__Roname1Assignment_0 )
                    {
                     before(grammarAccess.getRelational_operatorAccess().getRoname1Assignment_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1427:1: ( rule__Relational_operator__Roname1Assignment_0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1427:2: rule__Relational_operator__Roname1Assignment_0
                    {
                    pushFollow(FOLLOW_rule__Relational_operator__Roname1Assignment_0_in_rule__Relational_operator__Alternatives3037);
                    rule__Relational_operator__Roname1Assignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelational_operatorAccess().getRoname1Assignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1431:6: ( ( rule__Relational_operator__Roname2Assignment_1 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1431:6: ( ( rule__Relational_operator__Roname2Assignment_1 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1432:1: ( rule__Relational_operator__Roname2Assignment_1 )
                    {
                     before(grammarAccess.getRelational_operatorAccess().getRoname2Assignment_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1433:1: ( rule__Relational_operator__Roname2Assignment_1 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1433:2: rule__Relational_operator__Roname2Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Relational_operator__Roname2Assignment_1_in_rule__Relational_operator__Alternatives3055);
                    rule__Relational_operator__Roname2Assignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelational_operatorAccess().getRoname2Assignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1437:6: ( ( rule__Relational_operator__Roname3Assignment_2 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1437:6: ( ( rule__Relational_operator__Roname3Assignment_2 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1438:1: ( rule__Relational_operator__Roname3Assignment_2 )
                    {
                     before(grammarAccess.getRelational_operatorAccess().getRoname3Assignment_2()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1439:1: ( rule__Relational_operator__Roname3Assignment_2 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1439:2: rule__Relational_operator__Roname3Assignment_2
                    {
                    pushFollow(FOLLOW_rule__Relational_operator__Roname3Assignment_2_in_rule__Relational_operator__Alternatives3073);
                    rule__Relational_operator__Roname3Assignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelational_operatorAccess().getRoname3Assignment_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1443:6: ( ( rule__Relational_operator__Roname4Assignment_3 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1443:6: ( ( rule__Relational_operator__Roname4Assignment_3 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1444:1: ( rule__Relational_operator__Roname4Assignment_3 )
                    {
                     before(grammarAccess.getRelational_operatorAccess().getRoname4Assignment_3()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1445:1: ( rule__Relational_operator__Roname4Assignment_3 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1445:2: rule__Relational_operator__Roname4Assignment_3
                    {
                    pushFollow(FOLLOW_rule__Relational_operator__Roname4Assignment_3_in_rule__Relational_operator__Alternatives3091);
                    rule__Relational_operator__Roname4Assignment_3();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelational_operatorAccess().getRoname4Assignment_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1449:6: ( ( rule__Relational_operator__Roname5Assignment_4 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1449:6: ( ( rule__Relational_operator__Roname5Assignment_4 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1450:1: ( rule__Relational_operator__Roname5Assignment_4 )
                    {
                     before(grammarAccess.getRelational_operatorAccess().getRoname5Assignment_4()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1451:1: ( rule__Relational_operator__Roname5Assignment_4 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1451:2: rule__Relational_operator__Roname5Assignment_4
                    {
                    pushFollow(FOLLOW_rule__Relational_operator__Roname5Assignment_4_in_rule__Relational_operator__Alternatives3109);
                    rule__Relational_operator__Roname5Assignment_4();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelational_operatorAccess().getRoname5Assignment_4()); 

                    }


                    }
                    break;
                case 6 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1455:6: ( ( rule__Relational_operator__Roname6Assignment_5 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1455:6: ( ( rule__Relational_operator__Roname6Assignment_5 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1456:1: ( rule__Relational_operator__Roname6Assignment_5 )
                    {
                     before(grammarAccess.getRelational_operatorAccess().getRoname6Assignment_5()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1457:1: ( rule__Relational_operator__Roname6Assignment_5 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1457:2: rule__Relational_operator__Roname6Assignment_5
                    {
                    pushFollow(FOLLOW_rule__Relational_operator__Roname6Assignment_5_in_rule__Relational_operator__Alternatives3127);
                    rule__Relational_operator__Roname6Assignment_5();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelational_operatorAccess().getRoname6Assignment_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational_operator__Alternatives"


    // $ANTLR start "rule__Binary_adding_operator__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1466:1: rule__Binary_adding_operator__Alternatives : ( ( ( rule__Binary_adding_operator__Baoname1Assignment_0 ) ) | ( ( rule__Binary_adding_operator__Baoname2Assignment_1 ) ) );
    public final void rule__Binary_adding_operator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1470:1: ( ( ( rule__Binary_adding_operator__Baoname1Assignment_0 ) ) | ( ( rule__Binary_adding_operator__Baoname2Assignment_1 ) ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==18) ) {
                alt9=1;
            }
            else if ( (LA9_0==37) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1471:1: ( ( rule__Binary_adding_operator__Baoname1Assignment_0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1471:1: ( ( rule__Binary_adding_operator__Baoname1Assignment_0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1472:1: ( rule__Binary_adding_operator__Baoname1Assignment_0 )
                    {
                     before(grammarAccess.getBinary_adding_operatorAccess().getBaoname1Assignment_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1473:1: ( rule__Binary_adding_operator__Baoname1Assignment_0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1473:2: rule__Binary_adding_operator__Baoname1Assignment_0
                    {
                    pushFollow(FOLLOW_rule__Binary_adding_operator__Baoname1Assignment_0_in_rule__Binary_adding_operator__Alternatives3160);
                    rule__Binary_adding_operator__Baoname1Assignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBinary_adding_operatorAccess().getBaoname1Assignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1477:6: ( ( rule__Binary_adding_operator__Baoname2Assignment_1 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1477:6: ( ( rule__Binary_adding_operator__Baoname2Assignment_1 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1478:1: ( rule__Binary_adding_operator__Baoname2Assignment_1 )
                    {
                     before(grammarAccess.getBinary_adding_operatorAccess().getBaoname2Assignment_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1479:1: ( rule__Binary_adding_operator__Baoname2Assignment_1 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1479:2: rule__Binary_adding_operator__Baoname2Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Binary_adding_operator__Baoname2Assignment_1_in_rule__Binary_adding_operator__Alternatives3178);
                    rule__Binary_adding_operator__Baoname2Assignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getBinary_adding_operatorAccess().getBaoname2Assignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary_adding_operator__Alternatives"


    // $ANTLR start "rule__Unary_adding_operator__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1488:1: rule__Unary_adding_operator__Alternatives : ( ( ( rule__Unary_adding_operator__Uaoname1Assignment_0 ) ) | ( ( rule__Unary_adding_operator__Uaoname2Assignment_1 ) ) );
    public final void rule__Unary_adding_operator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1492:1: ( ( ( rule__Unary_adding_operator__Uaoname1Assignment_0 ) ) | ( ( rule__Unary_adding_operator__Uaoname2Assignment_1 ) ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==18) ) {
                alt10=1;
            }
            else if ( (LA10_0==37) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1493:1: ( ( rule__Unary_adding_operator__Uaoname1Assignment_0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1493:1: ( ( rule__Unary_adding_operator__Uaoname1Assignment_0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1494:1: ( rule__Unary_adding_operator__Uaoname1Assignment_0 )
                    {
                     before(grammarAccess.getUnary_adding_operatorAccess().getUaoname1Assignment_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1495:1: ( rule__Unary_adding_operator__Uaoname1Assignment_0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1495:2: rule__Unary_adding_operator__Uaoname1Assignment_0
                    {
                    pushFollow(FOLLOW_rule__Unary_adding_operator__Uaoname1Assignment_0_in_rule__Unary_adding_operator__Alternatives3211);
                    rule__Unary_adding_operator__Uaoname1Assignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getUnary_adding_operatorAccess().getUaoname1Assignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1499:6: ( ( rule__Unary_adding_operator__Uaoname2Assignment_1 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1499:6: ( ( rule__Unary_adding_operator__Uaoname2Assignment_1 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1500:1: ( rule__Unary_adding_operator__Uaoname2Assignment_1 )
                    {
                     before(grammarAccess.getUnary_adding_operatorAccess().getUaoname2Assignment_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1501:1: ( rule__Unary_adding_operator__Uaoname2Assignment_1 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1501:2: rule__Unary_adding_operator__Uaoname2Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Unary_adding_operator__Uaoname2Assignment_1_in_rule__Unary_adding_operator__Alternatives3229);
                    rule__Unary_adding_operator__Uaoname2Assignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getUnary_adding_operatorAccess().getUaoname2Assignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary_adding_operator__Alternatives"


    // $ANTLR start "rule__Multiplying_operator__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1510:1: rule__Multiplying_operator__Alternatives : ( ( ( rule__Multiplying_operator__Moname1Assignment_0 ) ) | ( ( rule__Multiplying_operator__Moname2Assignment_1 ) ) | ( ( rule__Multiplying_operator__Moname3Assignment_2 ) ) | ( ( rule__Multiplying_operator__Moname4Assignment_3 ) ) );
    public final void rule__Multiplying_operator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1514:1: ( ( ( rule__Multiplying_operator__Moname1Assignment_0 ) ) | ( ( rule__Multiplying_operator__Moname2Assignment_1 ) ) | ( ( rule__Multiplying_operator__Moname3Assignment_2 ) ) | ( ( rule__Multiplying_operator__Moname4Assignment_3 ) ) )
            int alt11=4;
            switch ( input.LA(1) ) {
            case 38:
                {
                alt11=1;
                }
                break;
            case 39:
                {
                alt11=2;
                }
                break;
            case 40:
                {
                alt11=3;
                }
                break;
            case 41:
                {
                alt11=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1515:1: ( ( rule__Multiplying_operator__Moname1Assignment_0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1515:1: ( ( rule__Multiplying_operator__Moname1Assignment_0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1516:1: ( rule__Multiplying_operator__Moname1Assignment_0 )
                    {
                     before(grammarAccess.getMultiplying_operatorAccess().getMoname1Assignment_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1517:1: ( rule__Multiplying_operator__Moname1Assignment_0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1517:2: rule__Multiplying_operator__Moname1Assignment_0
                    {
                    pushFollow(FOLLOW_rule__Multiplying_operator__Moname1Assignment_0_in_rule__Multiplying_operator__Alternatives3262);
                    rule__Multiplying_operator__Moname1Assignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getMultiplying_operatorAccess().getMoname1Assignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1521:6: ( ( rule__Multiplying_operator__Moname2Assignment_1 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1521:6: ( ( rule__Multiplying_operator__Moname2Assignment_1 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1522:1: ( rule__Multiplying_operator__Moname2Assignment_1 )
                    {
                     before(grammarAccess.getMultiplying_operatorAccess().getMoname2Assignment_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1523:1: ( rule__Multiplying_operator__Moname2Assignment_1 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1523:2: rule__Multiplying_operator__Moname2Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Multiplying_operator__Moname2Assignment_1_in_rule__Multiplying_operator__Alternatives3280);
                    rule__Multiplying_operator__Moname2Assignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getMultiplying_operatorAccess().getMoname2Assignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1527:6: ( ( rule__Multiplying_operator__Moname3Assignment_2 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1527:6: ( ( rule__Multiplying_operator__Moname3Assignment_2 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1528:1: ( rule__Multiplying_operator__Moname3Assignment_2 )
                    {
                     before(grammarAccess.getMultiplying_operatorAccess().getMoname3Assignment_2()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1529:1: ( rule__Multiplying_operator__Moname3Assignment_2 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1529:2: rule__Multiplying_operator__Moname3Assignment_2
                    {
                    pushFollow(FOLLOW_rule__Multiplying_operator__Moname3Assignment_2_in_rule__Multiplying_operator__Alternatives3298);
                    rule__Multiplying_operator__Moname3Assignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getMultiplying_operatorAccess().getMoname3Assignment_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1533:6: ( ( rule__Multiplying_operator__Moname4Assignment_3 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1533:6: ( ( rule__Multiplying_operator__Moname4Assignment_3 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1534:1: ( rule__Multiplying_operator__Moname4Assignment_3 )
                    {
                     before(grammarAccess.getMultiplying_operatorAccess().getMoname4Assignment_3()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1535:1: ( rule__Multiplying_operator__Moname4Assignment_3 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1535:2: rule__Multiplying_operator__Moname4Assignment_3
                    {
                    pushFollow(FOLLOW_rule__Multiplying_operator__Moname4Assignment_3_in_rule__Multiplying_operator__Alternatives3316);
                    rule__Multiplying_operator__Moname4Assignment_3();

                    state._fsp--;


                    }

                     after(grammarAccess.getMultiplying_operatorAccess().getMoname4Assignment_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplying_operator__Alternatives"


    // $ANTLR start "rule__Boolean_literal__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1544:1: rule__Boolean_literal__Alternatives : ( ( ( rule__Boolean_literal__Blname1Assignment_0 ) ) | ( ( rule__Boolean_literal__Blname1Assignment_1 ) ) );
    public final void rule__Boolean_literal__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1548:1: ( ( ( rule__Boolean_literal__Blname1Assignment_0 ) ) | ( ( rule__Boolean_literal__Blname1Assignment_1 ) ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==45) ) {
                alt12=1;
            }
            else if ( (LA12_0==46) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1549:1: ( ( rule__Boolean_literal__Blname1Assignment_0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1549:1: ( ( rule__Boolean_literal__Blname1Assignment_0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1550:1: ( rule__Boolean_literal__Blname1Assignment_0 )
                    {
                     before(grammarAccess.getBoolean_literalAccess().getBlname1Assignment_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1551:1: ( rule__Boolean_literal__Blname1Assignment_0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1551:2: rule__Boolean_literal__Blname1Assignment_0
                    {
                    pushFollow(FOLLOW_rule__Boolean_literal__Blname1Assignment_0_in_rule__Boolean_literal__Alternatives3349);
                    rule__Boolean_literal__Blname1Assignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBoolean_literalAccess().getBlname1Assignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1555:6: ( ( rule__Boolean_literal__Blname1Assignment_1 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1555:6: ( ( rule__Boolean_literal__Blname1Assignment_1 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1556:1: ( rule__Boolean_literal__Blname1Assignment_1 )
                    {
                     before(grammarAccess.getBoolean_literalAccess().getBlname1Assignment_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1557:1: ( rule__Boolean_literal__Blname1Assignment_1 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1557:2: rule__Boolean_literal__Blname1Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Boolean_literal__Blname1Assignment_1_in_rule__Boolean_literal__Alternatives3367);
                    rule__Boolean_literal__Blname1Assignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getBoolean_literalAccess().getBlname1Assignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Boolean_literal__Alternatives"


    // $ANTLR start "rule__Unit_identifier__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1566:1: rule__Unit_identifier__Alternatives : ( ( ( rule__Unit_identifier__Uiname1Assignment_0 ) ) | ( ( rule__Unit_identifier__Uiname2Assignment_1 ) ) | ( ( rule__Unit_identifier__Uiname3Assignment_2 ) ) | ( ( rule__Unit_identifier__Uiname4Assignment_3 ) ) | ( ( rule__Unit_identifier__Uiname5Assignment_4 ) ) | ( ( rule__Unit_identifier__Uiname6Assignment_5 ) ) | ( ( rule__Unit_identifier__Uiname7Assignment_6 ) ) );
    public final void rule__Unit_identifier__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1570:1: ( ( ( rule__Unit_identifier__Uiname1Assignment_0 ) ) | ( ( rule__Unit_identifier__Uiname2Assignment_1 ) ) | ( ( rule__Unit_identifier__Uiname3Assignment_2 ) ) | ( ( rule__Unit_identifier__Uiname4Assignment_3 ) ) | ( ( rule__Unit_identifier__Uiname5Assignment_4 ) ) | ( ( rule__Unit_identifier__Uiname6Assignment_5 ) ) | ( ( rule__Unit_identifier__Uiname7Assignment_6 ) ) )
            int alt13=7;
            switch ( input.LA(1) ) {
            case 47:
                {
                alt13=1;
                }
                break;
            case 48:
                {
                alt13=2;
                }
                break;
            case 49:
                {
                alt13=3;
                }
                break;
            case 50:
                {
                alt13=4;
                }
                break;
            case 51:
                {
                alt13=5;
                }
                break;
            case 52:
                {
                alt13=6;
                }
                break;
            case 53:
                {
                alt13=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1571:1: ( ( rule__Unit_identifier__Uiname1Assignment_0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1571:1: ( ( rule__Unit_identifier__Uiname1Assignment_0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1572:1: ( rule__Unit_identifier__Uiname1Assignment_0 )
                    {
                     before(grammarAccess.getUnit_identifierAccess().getUiname1Assignment_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1573:1: ( rule__Unit_identifier__Uiname1Assignment_0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1573:2: rule__Unit_identifier__Uiname1Assignment_0
                    {
                    pushFollow(FOLLOW_rule__Unit_identifier__Uiname1Assignment_0_in_rule__Unit_identifier__Alternatives3400);
                    rule__Unit_identifier__Uiname1Assignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getUnit_identifierAccess().getUiname1Assignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1577:6: ( ( rule__Unit_identifier__Uiname2Assignment_1 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1577:6: ( ( rule__Unit_identifier__Uiname2Assignment_1 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1578:1: ( rule__Unit_identifier__Uiname2Assignment_1 )
                    {
                     before(grammarAccess.getUnit_identifierAccess().getUiname2Assignment_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1579:1: ( rule__Unit_identifier__Uiname2Assignment_1 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1579:2: rule__Unit_identifier__Uiname2Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Unit_identifier__Uiname2Assignment_1_in_rule__Unit_identifier__Alternatives3418);
                    rule__Unit_identifier__Uiname2Assignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getUnit_identifierAccess().getUiname2Assignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1583:6: ( ( rule__Unit_identifier__Uiname3Assignment_2 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1583:6: ( ( rule__Unit_identifier__Uiname3Assignment_2 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1584:1: ( rule__Unit_identifier__Uiname3Assignment_2 )
                    {
                     before(grammarAccess.getUnit_identifierAccess().getUiname3Assignment_2()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1585:1: ( rule__Unit_identifier__Uiname3Assignment_2 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1585:2: rule__Unit_identifier__Uiname3Assignment_2
                    {
                    pushFollow(FOLLOW_rule__Unit_identifier__Uiname3Assignment_2_in_rule__Unit_identifier__Alternatives3436);
                    rule__Unit_identifier__Uiname3Assignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getUnit_identifierAccess().getUiname3Assignment_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1589:6: ( ( rule__Unit_identifier__Uiname4Assignment_3 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1589:6: ( ( rule__Unit_identifier__Uiname4Assignment_3 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1590:1: ( rule__Unit_identifier__Uiname4Assignment_3 )
                    {
                     before(grammarAccess.getUnit_identifierAccess().getUiname4Assignment_3()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1591:1: ( rule__Unit_identifier__Uiname4Assignment_3 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1591:2: rule__Unit_identifier__Uiname4Assignment_3
                    {
                    pushFollow(FOLLOW_rule__Unit_identifier__Uiname4Assignment_3_in_rule__Unit_identifier__Alternatives3454);
                    rule__Unit_identifier__Uiname4Assignment_3();

                    state._fsp--;


                    }

                     after(grammarAccess.getUnit_identifierAccess().getUiname4Assignment_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1595:6: ( ( rule__Unit_identifier__Uiname5Assignment_4 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1595:6: ( ( rule__Unit_identifier__Uiname5Assignment_4 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1596:1: ( rule__Unit_identifier__Uiname5Assignment_4 )
                    {
                     before(grammarAccess.getUnit_identifierAccess().getUiname5Assignment_4()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1597:1: ( rule__Unit_identifier__Uiname5Assignment_4 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1597:2: rule__Unit_identifier__Uiname5Assignment_4
                    {
                    pushFollow(FOLLOW_rule__Unit_identifier__Uiname5Assignment_4_in_rule__Unit_identifier__Alternatives3472);
                    rule__Unit_identifier__Uiname5Assignment_4();

                    state._fsp--;


                    }

                     after(grammarAccess.getUnit_identifierAccess().getUiname5Assignment_4()); 

                    }


                    }
                    break;
                case 6 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1601:6: ( ( rule__Unit_identifier__Uiname6Assignment_5 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1601:6: ( ( rule__Unit_identifier__Uiname6Assignment_5 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1602:1: ( rule__Unit_identifier__Uiname6Assignment_5 )
                    {
                     before(grammarAccess.getUnit_identifierAccess().getUiname6Assignment_5()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1603:1: ( rule__Unit_identifier__Uiname6Assignment_5 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1603:2: rule__Unit_identifier__Uiname6Assignment_5
                    {
                    pushFollow(FOLLOW_rule__Unit_identifier__Uiname6Assignment_5_in_rule__Unit_identifier__Alternatives3490);
                    rule__Unit_identifier__Uiname6Assignment_5();

                    state._fsp--;


                    }

                     after(grammarAccess.getUnit_identifierAccess().getUiname6Assignment_5()); 

                    }


                    }
                    break;
                case 7 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1607:6: ( ( rule__Unit_identifier__Uiname7Assignment_6 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1607:6: ( ( rule__Unit_identifier__Uiname7Assignment_6 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1608:1: ( rule__Unit_identifier__Uiname7Assignment_6 )
                    {
                     before(grammarAccess.getUnit_identifierAccess().getUiname7Assignment_6()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1609:1: ( rule__Unit_identifier__Uiname7Assignment_6 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1609:2: rule__Unit_identifier__Uiname7Assignment_6
                    {
                    pushFollow(FOLLOW_rule__Unit_identifier__Uiname7Assignment_6_in_rule__Unit_identifier__Alternatives3508);
                    rule__Unit_identifier__Uiname7Assignment_6();

                    state._fsp--;


                    }

                     after(grammarAccess.getUnit_identifierAccess().getUiname7Assignment_6()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unit_identifier__Alternatives"


    // $ANTLR start "rule__Numeric_literal__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1618:1: rule__Numeric_literal__Alternatives : ( ( ( rule__Numeric_literal__Nlname1Assignment_0 ) ) | ( ( rule__Numeric_literal__Nlname2Assignment_1 ) ) );
    public final void rule__Numeric_literal__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1622:1: ( ( ( rule__Numeric_literal__Nlname1Assignment_0 ) ) | ( ( rule__Numeric_literal__Nlname2Assignment_1 ) ) )
            int alt14=2;
            alt14 = dfa14.predict(input);
            switch (alt14) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1623:1: ( ( rule__Numeric_literal__Nlname1Assignment_0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1623:1: ( ( rule__Numeric_literal__Nlname1Assignment_0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1624:1: ( rule__Numeric_literal__Nlname1Assignment_0 )
                    {
                     before(grammarAccess.getNumeric_literalAccess().getNlname1Assignment_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1625:1: ( rule__Numeric_literal__Nlname1Assignment_0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1625:2: rule__Numeric_literal__Nlname1Assignment_0
                    {
                    pushFollow(FOLLOW_rule__Numeric_literal__Nlname1Assignment_0_in_rule__Numeric_literal__Alternatives3541);
                    rule__Numeric_literal__Nlname1Assignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getNumeric_literalAccess().getNlname1Assignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1629:6: ( ( rule__Numeric_literal__Nlname2Assignment_1 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1629:6: ( ( rule__Numeric_literal__Nlname2Assignment_1 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1630:1: ( rule__Numeric_literal__Nlname2Assignment_1 )
                    {
                     before(grammarAccess.getNumeric_literalAccess().getNlname2Assignment_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1631:1: ( rule__Numeric_literal__Nlname2Assignment_1 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1631:2: rule__Numeric_literal__Nlname2Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Numeric_literal__Nlname2Assignment_1_in_rule__Numeric_literal__Alternatives3559);
                    rule__Numeric_literal__Nlname2Assignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getNumeric_literalAccess().getNlname2Assignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Numeric_literal__Alternatives"


    // $ANTLR start "rule__Integer_literal__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1640:1: rule__Integer_literal__Alternatives : ( ( ( rule__Integer_literal__Ilname1Assignment_0 ) ) | ( ( rule__Integer_literal__Ilname2Assignment_1 ) ) );
    public final void rule__Integer_literal__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1644:1: ( ( ( rule__Integer_literal__Ilname1Assignment_0 ) ) | ( ( rule__Integer_literal__Ilname2Assignment_1 ) ) )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==RULE_INT) ) {
                alt15=1;
            }
            else if ( (LA15_0==RULE_DIGIT) ) {
                switch ( input.LA(2) ) {
                case EOF:
                case RULE_INT:
                case 16:
                case 17:
                case 18:
                case 22:
                case 23:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 47:
                case 48:
                case 49:
                case 50:
                case 51:
                case 52:
                case 53:
                    {
                    alt15=1;
                    }
                    break;
                case RULE_DIGIT:
                    {
                    int LA15_3 = input.LA(3);

                    if ( (LA15_3==EOF||LA15_3==RULE_DIGIT||LA15_3==RULE_INT||(LA15_3>=16 && LA15_3<=18)||(LA15_3>=22 && LA15_3<=23)||(LA15_3>=30 && LA15_3<=42)||(LA15_3>=47 && LA15_3<=53)) ) {
                        alt15=1;
                    }
                    else if ( (LA15_3==20) ) {
                        alt15=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 15, 3, input);

                        throw nvae;
                    }
                    }
                    break;
                case 20:
                    {
                    alt15=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 15, 2, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1645:1: ( ( rule__Integer_literal__Ilname1Assignment_0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1645:1: ( ( rule__Integer_literal__Ilname1Assignment_0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1646:1: ( rule__Integer_literal__Ilname1Assignment_0 )
                    {
                     before(grammarAccess.getInteger_literalAccess().getIlname1Assignment_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1647:1: ( rule__Integer_literal__Ilname1Assignment_0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1647:2: rule__Integer_literal__Ilname1Assignment_0
                    {
                    pushFollow(FOLLOW_rule__Integer_literal__Ilname1Assignment_0_in_rule__Integer_literal__Alternatives3592);
                    rule__Integer_literal__Ilname1Assignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getInteger_literalAccess().getIlname1Assignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1651:6: ( ( rule__Integer_literal__Ilname2Assignment_1 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1651:6: ( ( rule__Integer_literal__Ilname2Assignment_1 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1652:1: ( rule__Integer_literal__Ilname2Assignment_1 )
                    {
                     before(grammarAccess.getInteger_literalAccess().getIlname2Assignment_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1653:1: ( rule__Integer_literal__Ilname2Assignment_1 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1653:2: rule__Integer_literal__Ilname2Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Integer_literal__Ilname2Assignment_1_in_rule__Integer_literal__Alternatives3610);
                    rule__Integer_literal__Ilname2Assignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getInteger_literalAccess().getIlname2Assignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Integer_literal__Alternatives"


    // $ANTLR start "rule__Exponent__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1662:1: rule__Exponent__Alternatives : ( ( ( rule__Exponent__Group_0__0 ) ) | ( ( rule__Exponent__Group_1__0 ) ) );
    public final void rule__Exponent__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1666:1: ( ( ( rule__Exponent__Group_0__0 ) ) | ( ( rule__Exponent__Group_1__0 ) ) )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==17) ) {
                alt16=1;
            }
            else if ( (LA16_0==19) ) {
                alt16=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1667:1: ( ( rule__Exponent__Group_0__0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1667:1: ( ( rule__Exponent__Group_0__0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1668:1: ( rule__Exponent__Group_0__0 )
                    {
                     before(grammarAccess.getExponentAccess().getGroup_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1669:1: ( rule__Exponent__Group_0__0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1669:2: rule__Exponent__Group_0__0
                    {
                    pushFollow(FOLLOW_rule__Exponent__Group_0__0_in_rule__Exponent__Alternatives3643);
                    rule__Exponent__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExponentAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1673:6: ( ( rule__Exponent__Group_1__0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1673:6: ( ( rule__Exponent__Group_1__0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1674:1: ( rule__Exponent__Group_1__0 )
                    {
                     before(grammarAccess.getExponentAccess().getGroup_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1675:1: ( rule__Exponent__Group_1__0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1675:2: rule__Exponent__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__Exponent__Group_1__0_in_rule__Exponent__Alternatives3661);
                    rule__Exponent__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExponentAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponent__Alternatives"


    // $ANTLR start "rule__Integer__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1684:1: rule__Integer__Alternatives : ( ( ( rule__Integer__Value1Assignment_0 ) ) | ( ( rule__Integer__Value2Assignment_1 ) ) );
    public final void rule__Integer__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1688:1: ( ( ( rule__Integer__Value1Assignment_0 ) ) | ( ( rule__Integer__Value2Assignment_1 ) ) )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_INT) ) {
                alt17=1;
            }
            else if ( (LA17_0==RULE_DIGIT) ) {
                alt17=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1689:1: ( ( rule__Integer__Value1Assignment_0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1689:1: ( ( rule__Integer__Value1Assignment_0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1690:1: ( rule__Integer__Value1Assignment_0 )
                    {
                     before(grammarAccess.getIntegerAccess().getValue1Assignment_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1691:1: ( rule__Integer__Value1Assignment_0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1691:2: rule__Integer__Value1Assignment_0
                    {
                    pushFollow(FOLLOW_rule__Integer__Value1Assignment_0_in_rule__Integer__Alternatives3694);
                    rule__Integer__Value1Assignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getIntegerAccess().getValue1Assignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1695:6: ( ( rule__Integer__Value2Assignment_1 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1695:6: ( ( rule__Integer__Value2Assignment_1 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1696:1: ( rule__Integer__Value2Assignment_1 )
                    {
                     before(grammarAccess.getIntegerAccess().getValue2Assignment_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1697:1: ( rule__Integer__Value2Assignment_1 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1697:2: rule__Integer__Value2Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Integer__Value2Assignment_1_in_rule__Integer__Alternatives3712);
                    rule__Integer__Value2Assignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getIntegerAccess().getValue2Assignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Integer__Alternatives"


    // $ANTLR start "rule__Dispatch_trigger_condition__Alternatives"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1706:1: rule__Dispatch_trigger_condition__Alternatives : ( ( ( rule__Dispatch_trigger_condition__Dtcname1Assignment_0 ) ) | ( ( rule__Dispatch_trigger_condition__Dtcname4Assignment_1 ) ) | ( ( rule__Dispatch_trigger_condition__Dtcname2Assignment_2 ) ) );
    public final void rule__Dispatch_trigger_condition__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1710:1: ( ( ( rule__Dispatch_trigger_condition__Dtcname1Assignment_0 ) ) | ( ( rule__Dispatch_trigger_condition__Dtcname4Assignment_1 ) ) | ( ( rule__Dispatch_trigger_condition__Dtcname2Assignment_2 ) ) )
            int alt18=3;
            switch ( input.LA(1) ) {
            case RULE_STRING_LITERAL:
                {
                alt18=1;
                }
                break;
            case 55:
                {
                alt18=2;
                }
                break;
            case 26:
                {
                alt18=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }

            switch (alt18) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1711:1: ( ( rule__Dispatch_trigger_condition__Dtcname1Assignment_0 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1711:1: ( ( rule__Dispatch_trigger_condition__Dtcname1Assignment_0 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1712:1: ( rule__Dispatch_trigger_condition__Dtcname1Assignment_0 )
                    {
                     before(grammarAccess.getDispatch_trigger_conditionAccess().getDtcname1Assignment_0()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1713:1: ( rule__Dispatch_trigger_condition__Dtcname1Assignment_0 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1713:2: rule__Dispatch_trigger_condition__Dtcname1Assignment_0
                    {
                    pushFollow(FOLLOW_rule__Dispatch_trigger_condition__Dtcname1Assignment_0_in_rule__Dispatch_trigger_condition__Alternatives3745);
                    rule__Dispatch_trigger_condition__Dtcname1Assignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getDispatch_trigger_conditionAccess().getDtcname1Assignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1717:6: ( ( rule__Dispatch_trigger_condition__Dtcname4Assignment_1 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1717:6: ( ( rule__Dispatch_trigger_condition__Dtcname4Assignment_1 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1718:1: ( rule__Dispatch_trigger_condition__Dtcname4Assignment_1 )
                    {
                     before(grammarAccess.getDispatch_trigger_conditionAccess().getDtcname4Assignment_1()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1719:1: ( rule__Dispatch_trigger_condition__Dtcname4Assignment_1 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1719:2: rule__Dispatch_trigger_condition__Dtcname4Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Dispatch_trigger_condition__Dtcname4Assignment_1_in_rule__Dispatch_trigger_condition__Alternatives3763);
                    rule__Dispatch_trigger_condition__Dtcname4Assignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getDispatch_trigger_conditionAccess().getDtcname4Assignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1723:6: ( ( rule__Dispatch_trigger_condition__Dtcname2Assignment_2 ) )
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1723:6: ( ( rule__Dispatch_trigger_condition__Dtcname2Assignment_2 ) )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1724:1: ( rule__Dispatch_trigger_condition__Dtcname2Assignment_2 )
                    {
                     before(grammarAccess.getDispatch_trigger_conditionAccess().getDtcname2Assignment_2()); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1725:1: ( rule__Dispatch_trigger_condition__Dtcname2Assignment_2 )
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1725:2: rule__Dispatch_trigger_condition__Dtcname2Assignment_2
                    {
                    pushFollow(FOLLOW_rule__Dispatch_trigger_condition__Dtcname2Assignment_2_in_rule__Dispatch_trigger_condition__Alternatives3781);
                    rule__Dispatch_trigger_condition__Dtcname2Assignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getDispatch_trigger_conditionAccess().getDtcname2Assignment_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_trigger_condition__Alternatives"


    // $ANTLR start "rule__Value_expression__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1736:1: rule__Value_expression__Group__0 : rule__Value_expression__Group__0__Impl rule__Value_expression__Group__1 ;
    public final void rule__Value_expression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1740:1: ( rule__Value_expression__Group__0__Impl rule__Value_expression__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1741:2: rule__Value_expression__Group__0__Impl rule__Value_expression__Group__1
            {
            pushFollow(FOLLOW_rule__Value_expression__Group__0__Impl_in_rule__Value_expression__Group__03812);
            rule__Value_expression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Value_expression__Group__1_in_rule__Value_expression__Group__03815);
            rule__Value_expression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_expression__Group__0"


    // $ANTLR start "rule__Value_expression__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1748:1: rule__Value_expression__Group__0__Impl : ( ( rule__Value_expression__Vename1Assignment_0 ) ) ;
    public final void rule__Value_expression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1752:1: ( ( ( rule__Value_expression__Vename1Assignment_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1753:1: ( ( rule__Value_expression__Vename1Assignment_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1753:1: ( ( rule__Value_expression__Vename1Assignment_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1754:1: ( rule__Value_expression__Vename1Assignment_0 )
            {
             before(grammarAccess.getValue_expressionAccess().getVename1Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1755:1: ( rule__Value_expression__Vename1Assignment_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1755:2: rule__Value_expression__Vename1Assignment_0
            {
            pushFollow(FOLLOW_rule__Value_expression__Vename1Assignment_0_in_rule__Value_expression__Group__0__Impl3842);
            rule__Value_expression__Vename1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getValue_expressionAccess().getVename1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_expression__Group__0__Impl"


    // $ANTLR start "rule__Value_expression__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1765:1: rule__Value_expression__Group__1 : rule__Value_expression__Group__1__Impl ;
    public final void rule__Value_expression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1769:1: ( rule__Value_expression__Group__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1770:2: rule__Value_expression__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Value_expression__Group__1__Impl_in_rule__Value_expression__Group__13872);
            rule__Value_expression__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_expression__Group__1"


    // $ANTLR start "rule__Value_expression__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1776:1: rule__Value_expression__Group__1__Impl : ( ( rule__Value_expression__Group_1__0 )* ) ;
    public final void rule__Value_expression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1780:1: ( ( ( rule__Value_expression__Group_1__0 )* ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1781:1: ( ( rule__Value_expression__Group_1__0 )* )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1781:1: ( ( rule__Value_expression__Group_1__0 )* )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1782:1: ( rule__Value_expression__Group_1__0 )*
            {
             before(grammarAccess.getValue_expressionAccess().getGroup_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1783:1: ( rule__Value_expression__Group_1__0 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( ((LA19_0>=22 && LA19_0<=23)||LA19_0==30) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1783:2: rule__Value_expression__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__Value_expression__Group_1__0_in_rule__Value_expression__Group__1__Impl3899);
            	    rule__Value_expression__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

             after(grammarAccess.getValue_expressionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_expression__Group__1__Impl"


    // $ANTLR start "rule__Value_expression__Group_1__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1797:1: rule__Value_expression__Group_1__0 : rule__Value_expression__Group_1__0__Impl rule__Value_expression__Group_1__1 ;
    public final void rule__Value_expression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1801:1: ( rule__Value_expression__Group_1__0__Impl rule__Value_expression__Group_1__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1802:2: rule__Value_expression__Group_1__0__Impl rule__Value_expression__Group_1__1
            {
            pushFollow(FOLLOW_rule__Value_expression__Group_1__0__Impl_in_rule__Value_expression__Group_1__03934);
            rule__Value_expression__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Value_expression__Group_1__1_in_rule__Value_expression__Group_1__03937);
            rule__Value_expression__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_expression__Group_1__0"


    // $ANTLR start "rule__Value_expression__Group_1__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1809:1: rule__Value_expression__Group_1__0__Impl : ( ( rule__Value_expression__Vename3Assignment_1_0 ) ) ;
    public final void rule__Value_expression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1813:1: ( ( ( rule__Value_expression__Vename3Assignment_1_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1814:1: ( ( rule__Value_expression__Vename3Assignment_1_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1814:1: ( ( rule__Value_expression__Vename3Assignment_1_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1815:1: ( rule__Value_expression__Vename3Assignment_1_0 )
            {
             before(grammarAccess.getValue_expressionAccess().getVename3Assignment_1_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1816:1: ( rule__Value_expression__Vename3Assignment_1_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1816:2: rule__Value_expression__Vename3Assignment_1_0
            {
            pushFollow(FOLLOW_rule__Value_expression__Vename3Assignment_1_0_in_rule__Value_expression__Group_1__0__Impl3964);
            rule__Value_expression__Vename3Assignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getValue_expressionAccess().getVename3Assignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_expression__Group_1__0__Impl"


    // $ANTLR start "rule__Value_expression__Group_1__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1826:1: rule__Value_expression__Group_1__1 : rule__Value_expression__Group_1__1__Impl ;
    public final void rule__Value_expression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1830:1: ( rule__Value_expression__Group_1__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1831:2: rule__Value_expression__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Value_expression__Group_1__1__Impl_in_rule__Value_expression__Group_1__13994);
            rule__Value_expression__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_expression__Group_1__1"


    // $ANTLR start "rule__Value_expression__Group_1__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1837:1: rule__Value_expression__Group_1__1__Impl : ( ( rule__Value_expression__Vename2Assignment_1_1 ) ) ;
    public final void rule__Value_expression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1841:1: ( ( ( rule__Value_expression__Vename2Assignment_1_1 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1842:1: ( ( rule__Value_expression__Vename2Assignment_1_1 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1842:1: ( ( rule__Value_expression__Vename2Assignment_1_1 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1843:1: ( rule__Value_expression__Vename2Assignment_1_1 )
            {
             before(grammarAccess.getValue_expressionAccess().getVename2Assignment_1_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1844:1: ( rule__Value_expression__Vename2Assignment_1_1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1844:2: rule__Value_expression__Vename2Assignment_1_1
            {
            pushFollow(FOLLOW_rule__Value_expression__Vename2Assignment_1_1_in_rule__Value_expression__Group_1__1__Impl4021);
            rule__Value_expression__Vename2Assignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getValue_expressionAccess().getVename2Assignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_expression__Group_1__1__Impl"


    // $ANTLR start "rule__Relation__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1858:1: rule__Relation__Group__0 : rule__Relation__Group__0__Impl rule__Relation__Group__1 ;
    public final void rule__Relation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1862:1: ( rule__Relation__Group__0__Impl rule__Relation__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1863:2: rule__Relation__Group__0__Impl rule__Relation__Group__1
            {
            pushFollow(FOLLOW_rule__Relation__Group__0__Impl_in_rule__Relation__Group__04055);
            rule__Relation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Relation__Group__1_in_rule__Relation__Group__04058);
            rule__Relation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group__0"


    // $ANTLR start "rule__Relation__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1870:1: rule__Relation__Group__0__Impl : ( ( rule__Relation__Rname1Assignment_0 ) ) ;
    public final void rule__Relation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1874:1: ( ( ( rule__Relation__Rname1Assignment_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1875:1: ( ( rule__Relation__Rname1Assignment_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1875:1: ( ( rule__Relation__Rname1Assignment_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1876:1: ( rule__Relation__Rname1Assignment_0 )
            {
             before(grammarAccess.getRelationAccess().getRname1Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1877:1: ( rule__Relation__Rname1Assignment_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1877:2: rule__Relation__Rname1Assignment_0
            {
            pushFollow(FOLLOW_rule__Relation__Rname1Assignment_0_in_rule__Relation__Group__0__Impl4085);
            rule__Relation__Rname1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getRelationAccess().getRname1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group__0__Impl"


    // $ANTLR start "rule__Relation__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1887:1: rule__Relation__Group__1 : rule__Relation__Group__1__Impl ;
    public final void rule__Relation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1891:1: ( rule__Relation__Group__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1892:2: rule__Relation__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Relation__Group__1__Impl_in_rule__Relation__Group__14115);
            rule__Relation__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group__1"


    // $ANTLR start "rule__Relation__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1898:1: rule__Relation__Group__1__Impl : ( ( rule__Relation__Group_1__0 )? ) ;
    public final void rule__Relation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1902:1: ( ( ( rule__Relation__Group_1__0 )? ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1903:1: ( ( rule__Relation__Group_1__0 )? )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1903:1: ( ( rule__Relation__Group_1__0 )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1904:1: ( rule__Relation__Group_1__0 )?
            {
             before(grammarAccess.getRelationAccess().getGroup_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1905:1: ( rule__Relation__Group_1__0 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( ((LA20_0>=31 && LA20_0<=36)) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1905:2: rule__Relation__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__Relation__Group_1__0_in_rule__Relation__Group__1__Impl4142);
                    rule__Relation__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRelationAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group__1__Impl"


    // $ANTLR start "rule__Relation__Group_1__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1919:1: rule__Relation__Group_1__0 : rule__Relation__Group_1__0__Impl rule__Relation__Group_1__1 ;
    public final void rule__Relation__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1923:1: ( rule__Relation__Group_1__0__Impl rule__Relation__Group_1__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1924:2: rule__Relation__Group_1__0__Impl rule__Relation__Group_1__1
            {
            pushFollow(FOLLOW_rule__Relation__Group_1__0__Impl_in_rule__Relation__Group_1__04177);
            rule__Relation__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Relation__Group_1__1_in_rule__Relation__Group_1__04180);
            rule__Relation__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1__0"


    // $ANTLR start "rule__Relation__Group_1__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1931:1: rule__Relation__Group_1__0__Impl : ( ( rule__Relation__Rname3Assignment_1_0 ) ) ;
    public final void rule__Relation__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1935:1: ( ( ( rule__Relation__Rname3Assignment_1_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1936:1: ( ( rule__Relation__Rname3Assignment_1_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1936:1: ( ( rule__Relation__Rname3Assignment_1_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1937:1: ( rule__Relation__Rname3Assignment_1_0 )
            {
             before(grammarAccess.getRelationAccess().getRname3Assignment_1_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1938:1: ( rule__Relation__Rname3Assignment_1_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1938:2: rule__Relation__Rname3Assignment_1_0
            {
            pushFollow(FOLLOW_rule__Relation__Rname3Assignment_1_0_in_rule__Relation__Group_1__0__Impl4207);
            rule__Relation__Rname3Assignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getRelationAccess().getRname3Assignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1__0__Impl"


    // $ANTLR start "rule__Relation__Group_1__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1948:1: rule__Relation__Group_1__1 : rule__Relation__Group_1__1__Impl ;
    public final void rule__Relation__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1952:1: ( rule__Relation__Group_1__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1953:2: rule__Relation__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Relation__Group_1__1__Impl_in_rule__Relation__Group_1__14237);
            rule__Relation__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1__1"


    // $ANTLR start "rule__Relation__Group_1__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1959:1: rule__Relation__Group_1__1__Impl : ( ( rule__Relation__Rname2Assignment_1_1 ) ) ;
    public final void rule__Relation__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1963:1: ( ( ( rule__Relation__Rname2Assignment_1_1 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1964:1: ( ( rule__Relation__Rname2Assignment_1_1 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1964:1: ( ( rule__Relation__Rname2Assignment_1_1 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1965:1: ( rule__Relation__Rname2Assignment_1_1 )
            {
             before(grammarAccess.getRelationAccess().getRname2Assignment_1_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1966:1: ( rule__Relation__Rname2Assignment_1_1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1966:2: rule__Relation__Rname2Assignment_1_1
            {
            pushFollow(FOLLOW_rule__Relation__Rname2Assignment_1_1_in_rule__Relation__Group_1__1__Impl4264);
            rule__Relation__Rname2Assignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getRelationAccess().getRname2Assignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1__1__Impl"


    // $ANTLR start "rule__Simple_expression__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1980:1: rule__Simple_expression__Group__0 : rule__Simple_expression__Group__0__Impl rule__Simple_expression__Group__1 ;
    public final void rule__Simple_expression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1984:1: ( rule__Simple_expression__Group__0__Impl rule__Simple_expression__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1985:2: rule__Simple_expression__Group__0__Impl rule__Simple_expression__Group__1
            {
            pushFollow(FOLLOW_rule__Simple_expression__Group__0__Impl_in_rule__Simple_expression__Group__04298);
            rule__Simple_expression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Simple_expression__Group__1_in_rule__Simple_expression__Group__04301);
            rule__Simple_expression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Simple_expression__Group__0"


    // $ANTLR start "rule__Simple_expression__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1992:1: rule__Simple_expression__Group__0__Impl : ( ( rule__Simple_expression__Sename2Assignment_0 )? ) ;
    public final void rule__Simple_expression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1996:1: ( ( ( rule__Simple_expression__Sename2Assignment_0 )? ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1997:1: ( ( rule__Simple_expression__Sename2Assignment_0 )? )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1997:1: ( ( rule__Simple_expression__Sename2Assignment_0 )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1998:1: ( rule__Simple_expression__Sename2Assignment_0 )?
            {
             before(grammarAccess.getSimple_expressionAccess().getSename2Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1999:1: ( rule__Simple_expression__Sename2Assignment_0 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==18||LA21_0==37) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1999:2: rule__Simple_expression__Sename2Assignment_0
                    {
                    pushFollow(FOLLOW_rule__Simple_expression__Sename2Assignment_0_in_rule__Simple_expression__Group__0__Impl4328);
                    rule__Simple_expression__Sename2Assignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimple_expressionAccess().getSename2Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Simple_expression__Group__0__Impl"


    // $ANTLR start "rule__Simple_expression__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2009:1: rule__Simple_expression__Group__1 : rule__Simple_expression__Group__1__Impl rule__Simple_expression__Group__2 ;
    public final void rule__Simple_expression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2013:1: ( rule__Simple_expression__Group__1__Impl rule__Simple_expression__Group__2 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2014:2: rule__Simple_expression__Group__1__Impl rule__Simple_expression__Group__2
            {
            pushFollow(FOLLOW_rule__Simple_expression__Group__1__Impl_in_rule__Simple_expression__Group__14359);
            rule__Simple_expression__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Simple_expression__Group__2_in_rule__Simple_expression__Group__14362);
            rule__Simple_expression__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Simple_expression__Group__1"


    // $ANTLR start "rule__Simple_expression__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2021:1: rule__Simple_expression__Group__1__Impl : ( ( rule__Simple_expression__Sename1Assignment_1 ) ) ;
    public final void rule__Simple_expression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2025:1: ( ( ( rule__Simple_expression__Sename1Assignment_1 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2026:1: ( ( rule__Simple_expression__Sename1Assignment_1 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2026:1: ( ( rule__Simple_expression__Sename1Assignment_1 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2027:1: ( rule__Simple_expression__Sename1Assignment_1 )
            {
             before(grammarAccess.getSimple_expressionAccess().getSename1Assignment_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2028:1: ( rule__Simple_expression__Sename1Assignment_1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2028:2: rule__Simple_expression__Sename1Assignment_1
            {
            pushFollow(FOLLOW_rule__Simple_expression__Sename1Assignment_1_in_rule__Simple_expression__Group__1__Impl4389);
            rule__Simple_expression__Sename1Assignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSimple_expressionAccess().getSename1Assignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Simple_expression__Group__1__Impl"


    // $ANTLR start "rule__Simple_expression__Group__2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2038:1: rule__Simple_expression__Group__2 : rule__Simple_expression__Group__2__Impl ;
    public final void rule__Simple_expression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2042:1: ( rule__Simple_expression__Group__2__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2043:2: rule__Simple_expression__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Simple_expression__Group__2__Impl_in_rule__Simple_expression__Group__24419);
            rule__Simple_expression__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Simple_expression__Group__2"


    // $ANTLR start "rule__Simple_expression__Group__2__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2049:1: rule__Simple_expression__Group__2__Impl : ( ( rule__Simple_expression__Group_2__0 )* ) ;
    public final void rule__Simple_expression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2053:1: ( ( ( rule__Simple_expression__Group_2__0 )* ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2054:1: ( ( rule__Simple_expression__Group_2__0 )* )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2054:1: ( ( rule__Simple_expression__Group_2__0 )* )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2055:1: ( rule__Simple_expression__Group_2__0 )*
            {
             before(grammarAccess.getSimple_expressionAccess().getGroup_2()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2056:1: ( rule__Simple_expression__Group_2__0 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==18||LA22_0==37) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2056:2: rule__Simple_expression__Group_2__0
            	    {
            	    pushFollow(FOLLOW_rule__Simple_expression__Group_2__0_in_rule__Simple_expression__Group__2__Impl4446);
            	    rule__Simple_expression__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

             after(grammarAccess.getSimple_expressionAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Simple_expression__Group__2__Impl"


    // $ANTLR start "rule__Simple_expression__Group_2__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2072:1: rule__Simple_expression__Group_2__0 : rule__Simple_expression__Group_2__0__Impl rule__Simple_expression__Group_2__1 ;
    public final void rule__Simple_expression__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2076:1: ( rule__Simple_expression__Group_2__0__Impl rule__Simple_expression__Group_2__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2077:2: rule__Simple_expression__Group_2__0__Impl rule__Simple_expression__Group_2__1
            {
            pushFollow(FOLLOW_rule__Simple_expression__Group_2__0__Impl_in_rule__Simple_expression__Group_2__04483);
            rule__Simple_expression__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Simple_expression__Group_2__1_in_rule__Simple_expression__Group_2__04486);
            rule__Simple_expression__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Simple_expression__Group_2__0"


    // $ANTLR start "rule__Simple_expression__Group_2__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2084:1: rule__Simple_expression__Group_2__0__Impl : ( ( rule__Simple_expression__Sename3Assignment_2_0 ) ) ;
    public final void rule__Simple_expression__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2088:1: ( ( ( rule__Simple_expression__Sename3Assignment_2_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2089:1: ( ( rule__Simple_expression__Sename3Assignment_2_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2089:1: ( ( rule__Simple_expression__Sename3Assignment_2_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2090:1: ( rule__Simple_expression__Sename3Assignment_2_0 )
            {
             before(grammarAccess.getSimple_expressionAccess().getSename3Assignment_2_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2091:1: ( rule__Simple_expression__Sename3Assignment_2_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2091:2: rule__Simple_expression__Sename3Assignment_2_0
            {
            pushFollow(FOLLOW_rule__Simple_expression__Sename3Assignment_2_0_in_rule__Simple_expression__Group_2__0__Impl4513);
            rule__Simple_expression__Sename3Assignment_2_0();

            state._fsp--;


            }

             after(grammarAccess.getSimple_expressionAccess().getSename3Assignment_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Simple_expression__Group_2__0__Impl"


    // $ANTLR start "rule__Simple_expression__Group_2__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2101:1: rule__Simple_expression__Group_2__1 : rule__Simple_expression__Group_2__1__Impl ;
    public final void rule__Simple_expression__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2105:1: ( rule__Simple_expression__Group_2__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2106:2: rule__Simple_expression__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__Simple_expression__Group_2__1__Impl_in_rule__Simple_expression__Group_2__14543);
            rule__Simple_expression__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Simple_expression__Group_2__1"


    // $ANTLR start "rule__Simple_expression__Group_2__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2112:1: rule__Simple_expression__Group_2__1__Impl : ( ( rule__Simple_expression__Sename4Assignment_2_1 ) ) ;
    public final void rule__Simple_expression__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2116:1: ( ( ( rule__Simple_expression__Sename4Assignment_2_1 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2117:1: ( ( rule__Simple_expression__Sename4Assignment_2_1 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2117:1: ( ( rule__Simple_expression__Sename4Assignment_2_1 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2118:1: ( rule__Simple_expression__Sename4Assignment_2_1 )
            {
             before(grammarAccess.getSimple_expressionAccess().getSename4Assignment_2_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2119:1: ( rule__Simple_expression__Sename4Assignment_2_1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2119:2: rule__Simple_expression__Sename4Assignment_2_1
            {
            pushFollow(FOLLOW_rule__Simple_expression__Sename4Assignment_2_1_in_rule__Simple_expression__Group_2__1__Impl4570);
            rule__Simple_expression__Sename4Assignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getSimple_expressionAccess().getSename4Assignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Simple_expression__Group_2__1__Impl"


    // $ANTLR start "rule__Term__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2133:1: rule__Term__Group__0 : rule__Term__Group__0__Impl rule__Term__Group__1 ;
    public final void rule__Term__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2137:1: ( rule__Term__Group__0__Impl rule__Term__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2138:2: rule__Term__Group__0__Impl rule__Term__Group__1
            {
            pushFollow(FOLLOW_rule__Term__Group__0__Impl_in_rule__Term__Group__04604);
            rule__Term__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Term__Group__1_in_rule__Term__Group__04607);
            rule__Term__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group__0"


    // $ANTLR start "rule__Term__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2145:1: rule__Term__Group__0__Impl : ( ( rule__Term__Tname1Assignment_0 ) ) ;
    public final void rule__Term__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2149:1: ( ( ( rule__Term__Tname1Assignment_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2150:1: ( ( rule__Term__Tname1Assignment_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2150:1: ( ( rule__Term__Tname1Assignment_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2151:1: ( rule__Term__Tname1Assignment_0 )
            {
             before(grammarAccess.getTermAccess().getTname1Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2152:1: ( rule__Term__Tname1Assignment_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2152:2: rule__Term__Tname1Assignment_0
            {
            pushFollow(FOLLOW_rule__Term__Tname1Assignment_0_in_rule__Term__Group__0__Impl4634);
            rule__Term__Tname1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getTermAccess().getTname1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group__0__Impl"


    // $ANTLR start "rule__Term__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2162:1: rule__Term__Group__1 : rule__Term__Group__1__Impl ;
    public final void rule__Term__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2166:1: ( rule__Term__Group__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2167:2: rule__Term__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Term__Group__1__Impl_in_rule__Term__Group__14664);
            rule__Term__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group__1"


    // $ANTLR start "rule__Term__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2173:1: rule__Term__Group__1__Impl : ( ( rule__Term__Group_1__0 )* ) ;
    public final void rule__Term__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2177:1: ( ( ( rule__Term__Group_1__0 )* ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2178:1: ( ( rule__Term__Group_1__0 )* )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2178:1: ( ( rule__Term__Group_1__0 )* )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2179:1: ( rule__Term__Group_1__0 )*
            {
             before(grammarAccess.getTermAccess().getGroup_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2180:1: ( rule__Term__Group_1__0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( ((LA23_0>=38 && LA23_0<=41)) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2180:2: rule__Term__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__Term__Group_1__0_in_rule__Term__Group__1__Impl4691);
            	    rule__Term__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getTermAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group__1__Impl"


    // $ANTLR start "rule__Term__Group_1__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2194:1: rule__Term__Group_1__0 : rule__Term__Group_1__0__Impl rule__Term__Group_1__1 ;
    public final void rule__Term__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2198:1: ( rule__Term__Group_1__0__Impl rule__Term__Group_1__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2199:2: rule__Term__Group_1__0__Impl rule__Term__Group_1__1
            {
            pushFollow(FOLLOW_rule__Term__Group_1__0__Impl_in_rule__Term__Group_1__04726);
            rule__Term__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Term__Group_1__1_in_rule__Term__Group_1__04729);
            rule__Term__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group_1__0"


    // $ANTLR start "rule__Term__Group_1__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2206:1: rule__Term__Group_1__0__Impl : ( ( rule__Term__Tname3Assignment_1_0 ) ) ;
    public final void rule__Term__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2210:1: ( ( ( rule__Term__Tname3Assignment_1_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2211:1: ( ( rule__Term__Tname3Assignment_1_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2211:1: ( ( rule__Term__Tname3Assignment_1_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2212:1: ( rule__Term__Tname3Assignment_1_0 )
            {
             before(grammarAccess.getTermAccess().getTname3Assignment_1_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2213:1: ( rule__Term__Tname3Assignment_1_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2213:2: rule__Term__Tname3Assignment_1_0
            {
            pushFollow(FOLLOW_rule__Term__Tname3Assignment_1_0_in_rule__Term__Group_1__0__Impl4756);
            rule__Term__Tname3Assignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getTermAccess().getTname3Assignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group_1__0__Impl"


    // $ANTLR start "rule__Term__Group_1__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2223:1: rule__Term__Group_1__1 : rule__Term__Group_1__1__Impl ;
    public final void rule__Term__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2227:1: ( rule__Term__Group_1__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2228:2: rule__Term__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Term__Group_1__1__Impl_in_rule__Term__Group_1__14786);
            rule__Term__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group_1__1"


    // $ANTLR start "rule__Term__Group_1__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2234:1: rule__Term__Group_1__1__Impl : ( ( rule__Term__Tname2Assignment_1_1 ) ) ;
    public final void rule__Term__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2238:1: ( ( ( rule__Term__Tname2Assignment_1_1 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2239:1: ( ( rule__Term__Tname2Assignment_1_1 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2239:1: ( ( rule__Term__Tname2Assignment_1_1 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2240:1: ( rule__Term__Tname2Assignment_1_1 )
            {
             before(grammarAccess.getTermAccess().getTname2Assignment_1_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2241:1: ( rule__Term__Tname2Assignment_1_1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2241:2: rule__Term__Tname2Assignment_1_1
            {
            pushFollow(FOLLOW_rule__Term__Tname2Assignment_1_1_in_rule__Term__Group_1__1__Impl4813);
            rule__Term__Tname2Assignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getTermAccess().getTname2Assignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group_1__1__Impl"


    // $ANTLR start "rule__Factor__Group_0__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2255:1: rule__Factor__Group_0__0 : rule__Factor__Group_0__0__Impl rule__Factor__Group_0__1 ;
    public final void rule__Factor__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2259:1: ( rule__Factor__Group_0__0__Impl rule__Factor__Group_0__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2260:2: rule__Factor__Group_0__0__Impl rule__Factor__Group_0__1
            {
            pushFollow(FOLLOW_rule__Factor__Group_0__0__Impl_in_rule__Factor__Group_0__04847);
            rule__Factor__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Factor__Group_0__1_in_rule__Factor__Group_0__04850);
            rule__Factor__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group_0__0"


    // $ANTLR start "rule__Factor__Group_0__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2267:1: rule__Factor__Group_0__0__Impl : ( ( rule__Factor__Fname1Assignment_0_0 ) ) ;
    public final void rule__Factor__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2271:1: ( ( ( rule__Factor__Fname1Assignment_0_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2272:1: ( ( rule__Factor__Fname1Assignment_0_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2272:1: ( ( rule__Factor__Fname1Assignment_0_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2273:1: ( rule__Factor__Fname1Assignment_0_0 )
            {
             before(grammarAccess.getFactorAccess().getFname1Assignment_0_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2274:1: ( rule__Factor__Fname1Assignment_0_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2274:2: rule__Factor__Fname1Assignment_0_0
            {
            pushFollow(FOLLOW_rule__Factor__Fname1Assignment_0_0_in_rule__Factor__Group_0__0__Impl4877);
            rule__Factor__Fname1Assignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getFactorAccess().getFname1Assignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group_0__0__Impl"


    // $ANTLR start "rule__Factor__Group_0__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2284:1: rule__Factor__Group_0__1 : rule__Factor__Group_0__1__Impl ;
    public final void rule__Factor__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2288:1: ( rule__Factor__Group_0__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2289:2: rule__Factor__Group_0__1__Impl
            {
            pushFollow(FOLLOW_rule__Factor__Group_0__1__Impl_in_rule__Factor__Group_0__14907);
            rule__Factor__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group_0__1"


    // $ANTLR start "rule__Factor__Group_0__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2295:1: rule__Factor__Group_0__1__Impl : ( ( rule__Factor__Group_0_1__0 )? ) ;
    public final void rule__Factor__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2299:1: ( ( ( rule__Factor__Group_0_1__0 )? ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2300:1: ( ( rule__Factor__Group_0_1__0 )? )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2300:1: ( ( rule__Factor__Group_0_1__0 )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2301:1: ( rule__Factor__Group_0_1__0 )?
            {
             before(grammarAccess.getFactorAccess().getGroup_0_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2302:1: ( rule__Factor__Group_0_1__0 )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==42) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2302:2: rule__Factor__Group_0_1__0
                    {
                    pushFollow(FOLLOW_rule__Factor__Group_0_1__0_in_rule__Factor__Group_0__1__Impl4934);
                    rule__Factor__Group_0_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFactorAccess().getGroup_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group_0__1__Impl"


    // $ANTLR start "rule__Factor__Group_0_1__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2316:1: rule__Factor__Group_0_1__0 : rule__Factor__Group_0_1__0__Impl rule__Factor__Group_0_1__1 ;
    public final void rule__Factor__Group_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2320:1: ( rule__Factor__Group_0_1__0__Impl rule__Factor__Group_0_1__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2321:2: rule__Factor__Group_0_1__0__Impl rule__Factor__Group_0_1__1
            {
            pushFollow(FOLLOW_rule__Factor__Group_0_1__0__Impl_in_rule__Factor__Group_0_1__04969);
            rule__Factor__Group_0_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Factor__Group_0_1__1_in_rule__Factor__Group_0_1__04972);
            rule__Factor__Group_0_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group_0_1__0"


    // $ANTLR start "rule__Factor__Group_0_1__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2328:1: rule__Factor__Group_0_1__0__Impl : ( ( rule__Factor__Fname3Assignment_0_1_0 ) ) ;
    public final void rule__Factor__Group_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2332:1: ( ( ( rule__Factor__Fname3Assignment_0_1_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2333:1: ( ( rule__Factor__Fname3Assignment_0_1_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2333:1: ( ( rule__Factor__Fname3Assignment_0_1_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2334:1: ( rule__Factor__Fname3Assignment_0_1_0 )
            {
             before(grammarAccess.getFactorAccess().getFname3Assignment_0_1_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2335:1: ( rule__Factor__Fname3Assignment_0_1_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2335:2: rule__Factor__Fname3Assignment_0_1_0
            {
            pushFollow(FOLLOW_rule__Factor__Fname3Assignment_0_1_0_in_rule__Factor__Group_0_1__0__Impl4999);
            rule__Factor__Fname3Assignment_0_1_0();

            state._fsp--;


            }

             after(grammarAccess.getFactorAccess().getFname3Assignment_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group_0_1__0__Impl"


    // $ANTLR start "rule__Factor__Group_0_1__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2345:1: rule__Factor__Group_0_1__1 : rule__Factor__Group_0_1__1__Impl ;
    public final void rule__Factor__Group_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2349:1: ( rule__Factor__Group_0_1__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2350:2: rule__Factor__Group_0_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Factor__Group_0_1__1__Impl_in_rule__Factor__Group_0_1__15029);
            rule__Factor__Group_0_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group_0_1__1"


    // $ANTLR start "rule__Factor__Group_0_1__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2356:1: rule__Factor__Group_0_1__1__Impl : ( ( rule__Factor__Fname2Assignment_0_1_1 ) ) ;
    public final void rule__Factor__Group_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2360:1: ( ( ( rule__Factor__Fname2Assignment_0_1_1 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2361:1: ( ( rule__Factor__Fname2Assignment_0_1_1 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2361:1: ( ( rule__Factor__Fname2Assignment_0_1_1 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2362:1: ( rule__Factor__Fname2Assignment_0_1_1 )
            {
             before(grammarAccess.getFactorAccess().getFname2Assignment_0_1_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2363:1: ( rule__Factor__Fname2Assignment_0_1_1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2363:2: rule__Factor__Fname2Assignment_0_1_1
            {
            pushFollow(FOLLOW_rule__Factor__Fname2Assignment_0_1_1_in_rule__Factor__Group_0_1__1__Impl5056);
            rule__Factor__Fname2Assignment_0_1_1();

            state._fsp--;


            }

             after(grammarAccess.getFactorAccess().getFname2Assignment_0_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group_0_1__1__Impl"


    // $ANTLR start "rule__Factor__Group_1__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2377:1: rule__Factor__Group_1__0 : rule__Factor__Group_1__0__Impl rule__Factor__Group_1__1 ;
    public final void rule__Factor__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2381:1: ( rule__Factor__Group_1__0__Impl rule__Factor__Group_1__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2382:2: rule__Factor__Group_1__0__Impl rule__Factor__Group_1__1
            {
            pushFollow(FOLLOW_rule__Factor__Group_1__0__Impl_in_rule__Factor__Group_1__05090);
            rule__Factor__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Factor__Group_1__1_in_rule__Factor__Group_1__05093);
            rule__Factor__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group_1__0"


    // $ANTLR start "rule__Factor__Group_1__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2389:1: rule__Factor__Group_1__0__Impl : ( ( rule__Factor__Fname4Assignment_1_0 ) ) ;
    public final void rule__Factor__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2393:1: ( ( ( rule__Factor__Fname4Assignment_1_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2394:1: ( ( rule__Factor__Fname4Assignment_1_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2394:1: ( ( rule__Factor__Fname4Assignment_1_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2395:1: ( rule__Factor__Fname4Assignment_1_0 )
            {
             before(grammarAccess.getFactorAccess().getFname4Assignment_1_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2396:1: ( rule__Factor__Fname4Assignment_1_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2396:2: rule__Factor__Fname4Assignment_1_0
            {
            pushFollow(FOLLOW_rule__Factor__Fname4Assignment_1_0_in_rule__Factor__Group_1__0__Impl5120);
            rule__Factor__Fname4Assignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getFactorAccess().getFname4Assignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group_1__0__Impl"


    // $ANTLR start "rule__Factor__Group_1__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2406:1: rule__Factor__Group_1__1 : rule__Factor__Group_1__1__Impl ;
    public final void rule__Factor__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2410:1: ( rule__Factor__Group_1__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2411:2: rule__Factor__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Factor__Group_1__1__Impl_in_rule__Factor__Group_1__15150);
            rule__Factor__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group_1__1"


    // $ANTLR start "rule__Factor__Group_1__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2417:1: rule__Factor__Group_1__1__Impl : ( ( rule__Factor__Fname5Assignment_1_1 ) ) ;
    public final void rule__Factor__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2421:1: ( ( ( rule__Factor__Fname5Assignment_1_1 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2422:1: ( ( rule__Factor__Fname5Assignment_1_1 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2422:1: ( ( rule__Factor__Fname5Assignment_1_1 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2423:1: ( rule__Factor__Fname5Assignment_1_1 )
            {
             before(grammarAccess.getFactorAccess().getFname5Assignment_1_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2424:1: ( rule__Factor__Fname5Assignment_1_1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2424:2: rule__Factor__Fname5Assignment_1_1
            {
            pushFollow(FOLLOW_rule__Factor__Fname5Assignment_1_1_in_rule__Factor__Group_1__1__Impl5177);
            rule__Factor__Fname5Assignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getFactorAccess().getFname5Assignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group_1__1__Impl"


    // $ANTLR start "rule__Factor__Group_2__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2438:1: rule__Factor__Group_2__0 : rule__Factor__Group_2__0__Impl rule__Factor__Group_2__1 ;
    public final void rule__Factor__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2442:1: ( rule__Factor__Group_2__0__Impl rule__Factor__Group_2__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2443:2: rule__Factor__Group_2__0__Impl rule__Factor__Group_2__1
            {
            pushFollow(FOLLOW_rule__Factor__Group_2__0__Impl_in_rule__Factor__Group_2__05211);
            rule__Factor__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Factor__Group_2__1_in_rule__Factor__Group_2__05214);
            rule__Factor__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group_2__0"


    // $ANTLR start "rule__Factor__Group_2__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2450:1: rule__Factor__Group_2__0__Impl : ( ( rule__Factor__Fname6Assignment_2_0 ) ) ;
    public final void rule__Factor__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2454:1: ( ( ( rule__Factor__Fname6Assignment_2_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2455:1: ( ( rule__Factor__Fname6Assignment_2_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2455:1: ( ( rule__Factor__Fname6Assignment_2_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2456:1: ( rule__Factor__Fname6Assignment_2_0 )
            {
             before(grammarAccess.getFactorAccess().getFname6Assignment_2_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2457:1: ( rule__Factor__Fname6Assignment_2_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2457:2: rule__Factor__Fname6Assignment_2_0
            {
            pushFollow(FOLLOW_rule__Factor__Fname6Assignment_2_0_in_rule__Factor__Group_2__0__Impl5241);
            rule__Factor__Fname6Assignment_2_0();

            state._fsp--;


            }

             after(grammarAccess.getFactorAccess().getFname6Assignment_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group_2__0__Impl"


    // $ANTLR start "rule__Factor__Group_2__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2467:1: rule__Factor__Group_2__1 : rule__Factor__Group_2__1__Impl ;
    public final void rule__Factor__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2471:1: ( rule__Factor__Group_2__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2472:2: rule__Factor__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__Factor__Group_2__1__Impl_in_rule__Factor__Group_2__15271);
            rule__Factor__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group_2__1"


    // $ANTLR start "rule__Factor__Group_2__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2478:1: rule__Factor__Group_2__1__Impl : ( ( rule__Factor__Fname7Assignment_2_1 ) ) ;
    public final void rule__Factor__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2482:1: ( ( ( rule__Factor__Fname7Assignment_2_1 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2483:1: ( ( rule__Factor__Fname7Assignment_2_1 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2483:1: ( ( rule__Factor__Fname7Assignment_2_1 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2484:1: ( rule__Factor__Fname7Assignment_2_1 )
            {
             before(grammarAccess.getFactorAccess().getFname7Assignment_2_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2485:1: ( rule__Factor__Fname7Assignment_2_1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2485:2: rule__Factor__Fname7Assignment_2_1
            {
            pushFollow(FOLLOW_rule__Factor__Fname7Assignment_2_1_in_rule__Factor__Group_2__1__Impl5298);
            rule__Factor__Fname7Assignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getFactorAccess().getFname7Assignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group_2__1__Impl"


    // $ANTLR start "rule__Value_variable__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2499:1: rule__Value_variable__Group__0 : rule__Value_variable__Group__0__Impl rule__Value_variable__Group__1 ;
    public final void rule__Value_variable__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2503:1: ( rule__Value_variable__Group__0__Impl rule__Value_variable__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2504:2: rule__Value_variable__Group__0__Impl rule__Value_variable__Group__1
            {
            pushFollow(FOLLOW_rule__Value_variable__Group__0__Impl_in_rule__Value_variable__Group__05332);
            rule__Value_variable__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Value_variable__Group__1_in_rule__Value_variable__Group__05335);
            rule__Value_variable__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_variable__Group__0"


    // $ANTLR start "rule__Value_variable__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2511:1: rule__Value_variable__Group__0__Impl : ( ( rule__Value_variable__Vvname0Assignment_0 ) ) ;
    public final void rule__Value_variable__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2515:1: ( ( ( rule__Value_variable__Vvname0Assignment_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2516:1: ( ( rule__Value_variable__Vvname0Assignment_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2516:1: ( ( rule__Value_variable__Vvname0Assignment_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2517:1: ( rule__Value_variable__Vvname0Assignment_0 )
            {
             before(grammarAccess.getValue_variableAccess().getVvname0Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2518:1: ( rule__Value_variable__Vvname0Assignment_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2518:2: rule__Value_variable__Vvname0Assignment_0
            {
            pushFollow(FOLLOW_rule__Value_variable__Vvname0Assignment_0_in_rule__Value_variable__Group__0__Impl5362);
            rule__Value_variable__Vvname0Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getValue_variableAccess().getVvname0Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_variable__Group__0__Impl"


    // $ANTLR start "rule__Value_variable__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2528:1: rule__Value_variable__Group__1 : rule__Value_variable__Group__1__Impl ;
    public final void rule__Value_variable__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2532:1: ( rule__Value_variable__Group__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2533:2: rule__Value_variable__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Value_variable__Group__1__Impl_in_rule__Value_variable__Group__15392);
            rule__Value_variable__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_variable__Group__1"


    // $ANTLR start "rule__Value_variable__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2539:1: rule__Value_variable__Group__1__Impl : ( ( rule__Value_variable__Alternatives_1 )? ) ;
    public final void rule__Value_variable__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2543:1: ( ( ( rule__Value_variable__Alternatives_1 )? ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2544:1: ( ( rule__Value_variable__Alternatives_1 )? )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2544:1: ( ( rule__Value_variable__Alternatives_1 )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2545:1: ( rule__Value_variable__Alternatives_1 )?
            {
             before(grammarAccess.getValue_variableAccess().getAlternatives_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2546:1: ( rule__Value_variable__Alternatives_1 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( ((LA25_0>=27 && LA25_0<=29)) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2546:2: rule__Value_variable__Alternatives_1
                    {
                    pushFollow(FOLLOW_rule__Value_variable__Alternatives_1_in_rule__Value_variable__Group__1__Impl5419);
                    rule__Value_variable__Alternatives_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getValue_variableAccess().getAlternatives_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_variable__Group__1__Impl"


    // $ANTLR start "rule__Value_constant__Group_2__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2560:1: rule__Value_constant__Group_2__0 : rule__Value_constant__Group_2__0__Impl rule__Value_constant__Group_2__1 ;
    public final void rule__Value_constant__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2564:1: ( rule__Value_constant__Group_2__0__Impl rule__Value_constant__Group_2__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2565:2: rule__Value_constant__Group_2__0__Impl rule__Value_constant__Group_2__1
            {
            pushFollow(FOLLOW_rule__Value_constant__Group_2__0__Impl_in_rule__Value_constant__Group_2__05454);
            rule__Value_constant__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Value_constant__Group_2__1_in_rule__Value_constant__Group_2__05457);
            rule__Value_constant__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_constant__Group_2__0"


    // $ANTLR start "rule__Value_constant__Group_2__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2572:1: rule__Value_constant__Group_2__0__Impl : ( ( rule__Value_constant__Vcname3Assignment_2_0 ) ) ;
    public final void rule__Value_constant__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2576:1: ( ( ( rule__Value_constant__Vcname3Assignment_2_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2577:1: ( ( rule__Value_constant__Vcname3Assignment_2_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2577:1: ( ( rule__Value_constant__Vcname3Assignment_2_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2578:1: ( rule__Value_constant__Vcname3Assignment_2_0 )
            {
             before(grammarAccess.getValue_constantAccess().getVcname3Assignment_2_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2579:1: ( rule__Value_constant__Vcname3Assignment_2_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2579:2: rule__Value_constant__Vcname3Assignment_2_0
            {
            pushFollow(FOLLOW_rule__Value_constant__Vcname3Assignment_2_0_in_rule__Value_constant__Group_2__0__Impl5484);
            rule__Value_constant__Vcname3Assignment_2_0();

            state._fsp--;


            }

             after(grammarAccess.getValue_constantAccess().getVcname3Assignment_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_constant__Group_2__0__Impl"


    // $ANTLR start "rule__Value_constant__Group_2__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2589:1: rule__Value_constant__Group_2__1 : rule__Value_constant__Group_2__1__Impl rule__Value_constant__Group_2__2 ;
    public final void rule__Value_constant__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2593:1: ( rule__Value_constant__Group_2__1__Impl rule__Value_constant__Group_2__2 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2594:2: rule__Value_constant__Group_2__1__Impl rule__Value_constant__Group_2__2
            {
            pushFollow(FOLLOW_rule__Value_constant__Group_2__1__Impl_in_rule__Value_constant__Group_2__15514);
            rule__Value_constant__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Value_constant__Group_2__2_in_rule__Value_constant__Group_2__15517);
            rule__Value_constant__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_constant__Group_2__1"


    // $ANTLR start "rule__Value_constant__Group_2__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2601:1: rule__Value_constant__Group_2__1__Impl : ( '::' ) ;
    public final void rule__Value_constant__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2605:1: ( ( '::' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2606:1: ( '::' )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2606:1: ( '::' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2607:1: '::'
            {
             before(grammarAccess.getValue_constantAccess().getColonColonKeyword_2_1()); 
            match(input,14,FOLLOW_14_in_rule__Value_constant__Group_2__1__Impl5545); 
             after(grammarAccess.getValue_constantAccess().getColonColonKeyword_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_constant__Group_2__1__Impl"


    // $ANTLR start "rule__Value_constant__Group_2__2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2620:1: rule__Value_constant__Group_2__2 : rule__Value_constant__Group_2__2__Impl ;
    public final void rule__Value_constant__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2624:1: ( rule__Value_constant__Group_2__2__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2625:2: rule__Value_constant__Group_2__2__Impl
            {
            pushFollow(FOLLOW_rule__Value_constant__Group_2__2__Impl_in_rule__Value_constant__Group_2__25576);
            rule__Value_constant__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_constant__Group_2__2"


    // $ANTLR start "rule__Value_constant__Group_2__2__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2631:1: rule__Value_constant__Group_2__2__Impl : ( ( rule__Value_constant__Vcname4Assignment_2_2 ) ) ;
    public final void rule__Value_constant__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2635:1: ( ( ( rule__Value_constant__Vcname4Assignment_2_2 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2636:1: ( ( rule__Value_constant__Vcname4Assignment_2_2 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2636:1: ( ( rule__Value_constant__Vcname4Assignment_2_2 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2637:1: ( rule__Value_constant__Vcname4Assignment_2_2 )
            {
             before(grammarAccess.getValue_constantAccess().getVcname4Assignment_2_2()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2638:1: ( rule__Value_constant__Vcname4Assignment_2_2 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2638:2: rule__Value_constant__Vcname4Assignment_2_2
            {
            pushFollow(FOLLOW_rule__Value_constant__Vcname4Assignment_2_2_in_rule__Value_constant__Group_2__2__Impl5603);
            rule__Value_constant__Vcname4Assignment_2_2();

            state._fsp--;


            }

             after(grammarAccess.getValue_constantAccess().getVcname4Assignment_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_constant__Group_2__2__Impl"


    // $ANTLR start "rule__Behavior_time__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2654:1: rule__Behavior_time__Group__0 : rule__Behavior_time__Group__0__Impl rule__Behavior_time__Group__1 ;
    public final void rule__Behavior_time__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2658:1: ( rule__Behavior_time__Group__0__Impl rule__Behavior_time__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2659:2: rule__Behavior_time__Group__0__Impl rule__Behavior_time__Group__1
            {
            pushFollow(FOLLOW_rule__Behavior_time__Group__0__Impl_in_rule__Behavior_time__Group__05639);
            rule__Behavior_time__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Behavior_time__Group__1_in_rule__Behavior_time__Group__05642);
            rule__Behavior_time__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Behavior_time__Group__0"


    // $ANTLR start "rule__Behavior_time__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2666:1: rule__Behavior_time__Group__0__Impl : ( ( rule__Behavior_time__Btname1Assignment_0 ) ) ;
    public final void rule__Behavior_time__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2670:1: ( ( ( rule__Behavior_time__Btname1Assignment_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2671:1: ( ( rule__Behavior_time__Btname1Assignment_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2671:1: ( ( rule__Behavior_time__Btname1Assignment_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2672:1: ( rule__Behavior_time__Btname1Assignment_0 )
            {
             before(grammarAccess.getBehavior_timeAccess().getBtname1Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2673:1: ( rule__Behavior_time__Btname1Assignment_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2673:2: rule__Behavior_time__Btname1Assignment_0
            {
            pushFollow(FOLLOW_rule__Behavior_time__Btname1Assignment_0_in_rule__Behavior_time__Group__0__Impl5669);
            rule__Behavior_time__Btname1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getBehavior_timeAccess().getBtname1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Behavior_time__Group__0__Impl"


    // $ANTLR start "rule__Behavior_time__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2683:1: rule__Behavior_time__Group__1 : rule__Behavior_time__Group__1__Impl ;
    public final void rule__Behavior_time__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2687:1: ( rule__Behavior_time__Group__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2688:2: rule__Behavior_time__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Behavior_time__Group__1__Impl_in_rule__Behavior_time__Group__15699);
            rule__Behavior_time__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Behavior_time__Group__1"


    // $ANTLR start "rule__Behavior_time__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2694:1: rule__Behavior_time__Group__1__Impl : ( ( rule__Behavior_time__Btname2Assignment_1 ) ) ;
    public final void rule__Behavior_time__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2698:1: ( ( ( rule__Behavior_time__Btname2Assignment_1 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2699:1: ( ( rule__Behavior_time__Btname2Assignment_1 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2699:1: ( ( rule__Behavior_time__Btname2Assignment_1 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2700:1: ( rule__Behavior_time__Btname2Assignment_1 )
            {
             before(grammarAccess.getBehavior_timeAccess().getBtname2Assignment_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2701:1: ( rule__Behavior_time__Btname2Assignment_1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2701:2: rule__Behavior_time__Btname2Assignment_1
            {
            pushFollow(FOLLOW_rule__Behavior_time__Btname2Assignment_1_in_rule__Behavior_time__Group__1__Impl5726);
            rule__Behavior_time__Btname2Assignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBehavior_timeAccess().getBtname2Assignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Behavior_time__Group__1__Impl"


    // $ANTLR start "rule__Decimal_integer_literal__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2715:1: rule__Decimal_integer_literal__Group__0 : rule__Decimal_integer_literal__Group__0__Impl rule__Decimal_integer_literal__Group__1 ;
    public final void rule__Decimal_integer_literal__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2719:1: ( rule__Decimal_integer_literal__Group__0__Impl rule__Decimal_integer_literal__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2720:2: rule__Decimal_integer_literal__Group__0__Impl rule__Decimal_integer_literal__Group__1
            {
            pushFollow(FOLLOW_rule__Decimal_integer_literal__Group__0__Impl_in_rule__Decimal_integer_literal__Group__05760);
            rule__Decimal_integer_literal__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Decimal_integer_literal__Group__1_in_rule__Decimal_integer_literal__Group__05763);
            rule__Decimal_integer_literal__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_integer_literal__Group__0"


    // $ANTLR start "rule__Decimal_integer_literal__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2727:1: rule__Decimal_integer_literal__Group__0__Impl : ( ( rule__Decimal_integer_literal__Dilname1Assignment_0 ) ) ;
    public final void rule__Decimal_integer_literal__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2731:1: ( ( ( rule__Decimal_integer_literal__Dilname1Assignment_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2732:1: ( ( rule__Decimal_integer_literal__Dilname1Assignment_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2732:1: ( ( rule__Decimal_integer_literal__Dilname1Assignment_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2733:1: ( rule__Decimal_integer_literal__Dilname1Assignment_0 )
            {
             before(grammarAccess.getDecimal_integer_literalAccess().getDilname1Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2734:1: ( rule__Decimal_integer_literal__Dilname1Assignment_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2734:2: rule__Decimal_integer_literal__Dilname1Assignment_0
            {
            pushFollow(FOLLOW_rule__Decimal_integer_literal__Dilname1Assignment_0_in_rule__Decimal_integer_literal__Group__0__Impl5790);
            rule__Decimal_integer_literal__Dilname1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getDecimal_integer_literalAccess().getDilname1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_integer_literal__Group__0__Impl"


    // $ANTLR start "rule__Decimal_integer_literal__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2744:1: rule__Decimal_integer_literal__Group__1 : rule__Decimal_integer_literal__Group__1__Impl ;
    public final void rule__Decimal_integer_literal__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2748:1: ( rule__Decimal_integer_literal__Group__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2749:2: rule__Decimal_integer_literal__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Decimal_integer_literal__Group__1__Impl_in_rule__Decimal_integer_literal__Group__15820);
            rule__Decimal_integer_literal__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_integer_literal__Group__1"


    // $ANTLR start "rule__Decimal_integer_literal__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2755:1: rule__Decimal_integer_literal__Group__1__Impl : ( ( rule__Decimal_integer_literal__Dilname2Assignment_1 )? ) ;
    public final void rule__Decimal_integer_literal__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2759:1: ( ( ( rule__Decimal_integer_literal__Dilname2Assignment_1 )? ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2760:1: ( ( rule__Decimal_integer_literal__Dilname2Assignment_1 )? )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2760:1: ( ( rule__Decimal_integer_literal__Dilname2Assignment_1 )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2761:1: ( rule__Decimal_integer_literal__Dilname2Assignment_1 )?
            {
             before(grammarAccess.getDecimal_integer_literalAccess().getDilname2Assignment_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2762:1: ( rule__Decimal_integer_literal__Dilname2Assignment_1 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==17) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2762:2: rule__Decimal_integer_literal__Dilname2Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Decimal_integer_literal__Dilname2Assignment_1_in_rule__Decimal_integer_literal__Group__1__Impl5847);
                    rule__Decimal_integer_literal__Dilname2Assignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDecimal_integer_literalAccess().getDilname2Assignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_integer_literal__Group__1__Impl"


    // $ANTLR start "rule__Decimal_real_literal__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2776:1: rule__Decimal_real_literal__Group__0 : rule__Decimal_real_literal__Group__0__Impl rule__Decimal_real_literal__Group__1 ;
    public final void rule__Decimal_real_literal__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2780:1: ( rule__Decimal_real_literal__Group__0__Impl rule__Decimal_real_literal__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2781:2: rule__Decimal_real_literal__Group__0__Impl rule__Decimal_real_literal__Group__1
            {
            pushFollow(FOLLOW_rule__Decimal_real_literal__Group__0__Impl_in_rule__Decimal_real_literal__Group__05882);
            rule__Decimal_real_literal__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Decimal_real_literal__Group__1_in_rule__Decimal_real_literal__Group__05885);
            rule__Decimal_real_literal__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_real_literal__Group__0"


    // $ANTLR start "rule__Decimal_real_literal__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2788:1: rule__Decimal_real_literal__Group__0__Impl : ( ( rule__Decimal_real_literal__Drlname1Assignment_0 ) ) ;
    public final void rule__Decimal_real_literal__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2792:1: ( ( ( rule__Decimal_real_literal__Drlname1Assignment_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2793:1: ( ( rule__Decimal_real_literal__Drlname1Assignment_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2793:1: ( ( rule__Decimal_real_literal__Drlname1Assignment_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2794:1: ( rule__Decimal_real_literal__Drlname1Assignment_0 )
            {
             before(grammarAccess.getDecimal_real_literalAccess().getDrlname1Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2795:1: ( rule__Decimal_real_literal__Drlname1Assignment_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2795:2: rule__Decimal_real_literal__Drlname1Assignment_0
            {
            pushFollow(FOLLOW_rule__Decimal_real_literal__Drlname1Assignment_0_in_rule__Decimal_real_literal__Group__0__Impl5912);
            rule__Decimal_real_literal__Drlname1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getDecimal_real_literalAccess().getDrlname1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_real_literal__Group__0__Impl"


    // $ANTLR start "rule__Decimal_real_literal__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2805:1: rule__Decimal_real_literal__Group__1 : rule__Decimal_real_literal__Group__1__Impl rule__Decimal_real_literal__Group__2 ;
    public final void rule__Decimal_real_literal__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2809:1: ( rule__Decimal_real_literal__Group__1__Impl rule__Decimal_real_literal__Group__2 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2810:2: rule__Decimal_real_literal__Group__1__Impl rule__Decimal_real_literal__Group__2
            {
            pushFollow(FOLLOW_rule__Decimal_real_literal__Group__1__Impl_in_rule__Decimal_real_literal__Group__15942);
            rule__Decimal_real_literal__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Decimal_real_literal__Group__2_in_rule__Decimal_real_literal__Group__15945);
            rule__Decimal_real_literal__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_real_literal__Group__1"


    // $ANTLR start "rule__Decimal_real_literal__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2817:1: rule__Decimal_real_literal__Group__1__Impl : ( '.' ) ;
    public final void rule__Decimal_real_literal__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2821:1: ( ( '.' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2822:1: ( '.' )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2822:1: ( '.' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2823:1: '.'
            {
             before(grammarAccess.getDecimal_real_literalAccess().getFullStopKeyword_1()); 
            match(input,15,FOLLOW_15_in_rule__Decimal_real_literal__Group__1__Impl5973); 
             after(grammarAccess.getDecimal_real_literalAccess().getFullStopKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_real_literal__Group__1__Impl"


    // $ANTLR start "rule__Decimal_real_literal__Group__2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2836:1: rule__Decimal_real_literal__Group__2 : rule__Decimal_real_literal__Group__2__Impl rule__Decimal_real_literal__Group__3 ;
    public final void rule__Decimal_real_literal__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2840:1: ( rule__Decimal_real_literal__Group__2__Impl rule__Decimal_real_literal__Group__3 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2841:2: rule__Decimal_real_literal__Group__2__Impl rule__Decimal_real_literal__Group__3
            {
            pushFollow(FOLLOW_rule__Decimal_real_literal__Group__2__Impl_in_rule__Decimal_real_literal__Group__26004);
            rule__Decimal_real_literal__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Decimal_real_literal__Group__3_in_rule__Decimal_real_literal__Group__26007);
            rule__Decimal_real_literal__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_real_literal__Group__2"


    // $ANTLR start "rule__Decimal_real_literal__Group__2__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2848:1: rule__Decimal_real_literal__Group__2__Impl : ( ( rule__Decimal_real_literal__Drlname2Assignment_2 ) ) ;
    public final void rule__Decimal_real_literal__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2852:1: ( ( ( rule__Decimal_real_literal__Drlname2Assignment_2 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2853:1: ( ( rule__Decimal_real_literal__Drlname2Assignment_2 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2853:1: ( ( rule__Decimal_real_literal__Drlname2Assignment_2 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2854:1: ( rule__Decimal_real_literal__Drlname2Assignment_2 )
            {
             before(grammarAccess.getDecimal_real_literalAccess().getDrlname2Assignment_2()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2855:1: ( rule__Decimal_real_literal__Drlname2Assignment_2 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2855:2: rule__Decimal_real_literal__Drlname2Assignment_2
            {
            pushFollow(FOLLOW_rule__Decimal_real_literal__Drlname2Assignment_2_in_rule__Decimal_real_literal__Group__2__Impl6034);
            rule__Decimal_real_literal__Drlname2Assignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDecimal_real_literalAccess().getDrlname2Assignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_real_literal__Group__2__Impl"


    // $ANTLR start "rule__Decimal_real_literal__Group__3"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2865:1: rule__Decimal_real_literal__Group__3 : rule__Decimal_real_literal__Group__3__Impl ;
    public final void rule__Decimal_real_literal__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2869:1: ( rule__Decimal_real_literal__Group__3__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2870:2: rule__Decimal_real_literal__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__Decimal_real_literal__Group__3__Impl_in_rule__Decimal_real_literal__Group__36064);
            rule__Decimal_real_literal__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_real_literal__Group__3"


    // $ANTLR start "rule__Decimal_real_literal__Group__3__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2876:1: rule__Decimal_real_literal__Group__3__Impl : ( ( rule__Decimal_real_literal__Drlname3Assignment_3 )? ) ;
    public final void rule__Decimal_real_literal__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2880:1: ( ( ( rule__Decimal_real_literal__Drlname3Assignment_3 )? ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2881:1: ( ( rule__Decimal_real_literal__Drlname3Assignment_3 )? )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2881:1: ( ( rule__Decimal_real_literal__Drlname3Assignment_3 )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2882:1: ( rule__Decimal_real_literal__Drlname3Assignment_3 )?
            {
             before(grammarAccess.getDecimal_real_literalAccess().getDrlname3Assignment_3()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2883:1: ( rule__Decimal_real_literal__Drlname3Assignment_3 )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==17||LA27_0==19) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2883:2: rule__Decimal_real_literal__Drlname3Assignment_3
                    {
                    pushFollow(FOLLOW_rule__Decimal_real_literal__Drlname3Assignment_3_in_rule__Decimal_real_literal__Group__3__Impl6091);
                    rule__Decimal_real_literal__Drlname3Assignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDecimal_real_literalAccess().getDrlname3Assignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_real_literal__Group__3__Impl"


    // $ANTLR start "rule__Numeral__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2901:1: rule__Numeral__Group__0 : rule__Numeral__Group__0__Impl rule__Numeral__Group__1 ;
    public final void rule__Numeral__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2905:1: ( rule__Numeral__Group__0__Impl rule__Numeral__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2906:2: rule__Numeral__Group__0__Impl rule__Numeral__Group__1
            {
            pushFollow(FOLLOW_rule__Numeral__Group__0__Impl_in_rule__Numeral__Group__06130);
            rule__Numeral__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Numeral__Group__1_in_rule__Numeral__Group__06133);
            rule__Numeral__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Numeral__Group__0"


    // $ANTLR start "rule__Numeral__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2913:1: rule__Numeral__Group__0__Impl : ( ( rule__Numeral__Nname1Assignment_0 ) ) ;
    public final void rule__Numeral__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2917:1: ( ( ( rule__Numeral__Nname1Assignment_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2918:1: ( ( rule__Numeral__Nname1Assignment_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2918:1: ( ( rule__Numeral__Nname1Assignment_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2919:1: ( rule__Numeral__Nname1Assignment_0 )
            {
             before(grammarAccess.getNumeralAccess().getNname1Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2920:1: ( rule__Numeral__Nname1Assignment_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2920:2: rule__Numeral__Nname1Assignment_0
            {
            pushFollow(FOLLOW_rule__Numeral__Nname1Assignment_0_in_rule__Numeral__Group__0__Impl6160);
            rule__Numeral__Nname1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getNumeralAccess().getNname1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Numeral__Group__0__Impl"


    // $ANTLR start "rule__Numeral__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2930:1: rule__Numeral__Group__1 : rule__Numeral__Group__1__Impl ;
    public final void rule__Numeral__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2934:1: ( rule__Numeral__Group__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2935:2: rule__Numeral__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Numeral__Group__1__Impl_in_rule__Numeral__Group__16190);
            rule__Numeral__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Numeral__Group__1"


    // $ANTLR start "rule__Numeral__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2941:1: rule__Numeral__Group__1__Impl : ( ( rule__Numeral__Group_1__0 )* ) ;
    public final void rule__Numeral__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2945:1: ( ( ( rule__Numeral__Group_1__0 )* ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2946:1: ( ( rule__Numeral__Group_1__0 )* )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2946:1: ( ( rule__Numeral__Group_1__0 )* )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2947:1: ( rule__Numeral__Group_1__0 )*
            {
             before(grammarAccess.getNumeralAccess().getGroup_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2948:1: ( rule__Numeral__Group_1__0 )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==RULE_DIGIT||LA28_0==RULE_INT||LA28_0==16) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2948:2: rule__Numeral__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__Numeral__Group_1__0_in_rule__Numeral__Group__1__Impl6217);
            	    rule__Numeral__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

             after(grammarAccess.getNumeralAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Numeral__Group__1__Impl"


    // $ANTLR start "rule__Numeral__Group_1__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2962:1: rule__Numeral__Group_1__0 : rule__Numeral__Group_1__0__Impl rule__Numeral__Group_1__1 ;
    public final void rule__Numeral__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2966:1: ( rule__Numeral__Group_1__0__Impl rule__Numeral__Group_1__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2967:2: rule__Numeral__Group_1__0__Impl rule__Numeral__Group_1__1
            {
            pushFollow(FOLLOW_rule__Numeral__Group_1__0__Impl_in_rule__Numeral__Group_1__06252);
            rule__Numeral__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Numeral__Group_1__1_in_rule__Numeral__Group_1__06255);
            rule__Numeral__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Numeral__Group_1__0"


    // $ANTLR start "rule__Numeral__Group_1__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2974:1: rule__Numeral__Group_1__0__Impl : ( ( '_' )? ) ;
    public final void rule__Numeral__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2978:1: ( ( ( '_' )? ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2979:1: ( ( '_' )? )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2979:1: ( ( '_' )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2980:1: ( '_' )?
            {
             before(grammarAccess.getNumeralAccess().get_Keyword_1_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2981:1: ( '_' )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==16) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2982:2: '_'
                    {
                    match(input,16,FOLLOW_16_in_rule__Numeral__Group_1__0__Impl6284); 

                    }
                    break;

            }

             after(grammarAccess.getNumeralAccess().get_Keyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Numeral__Group_1__0__Impl"


    // $ANTLR start "rule__Numeral__Group_1__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2993:1: rule__Numeral__Group_1__1 : rule__Numeral__Group_1__1__Impl ;
    public final void rule__Numeral__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2997:1: ( rule__Numeral__Group_1__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:2998:2: rule__Numeral__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Numeral__Group_1__1__Impl_in_rule__Numeral__Group_1__16317);
            rule__Numeral__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Numeral__Group_1__1"


    // $ANTLR start "rule__Numeral__Group_1__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3004:1: rule__Numeral__Group_1__1__Impl : ( ( rule__Numeral__Nname2Assignment_1_1 ) ) ;
    public final void rule__Numeral__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3008:1: ( ( ( rule__Numeral__Nname2Assignment_1_1 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3009:1: ( ( rule__Numeral__Nname2Assignment_1_1 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3009:1: ( ( rule__Numeral__Nname2Assignment_1_1 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3010:1: ( rule__Numeral__Nname2Assignment_1_1 )
            {
             before(grammarAccess.getNumeralAccess().getNname2Assignment_1_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3011:1: ( rule__Numeral__Nname2Assignment_1_1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3011:2: rule__Numeral__Nname2Assignment_1_1
            {
            pushFollow(FOLLOW_rule__Numeral__Nname2Assignment_1_1_in_rule__Numeral__Group_1__1__Impl6344);
            rule__Numeral__Nname2Assignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getNumeralAccess().getNname2Assignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Numeral__Group_1__1__Impl"


    // $ANTLR start "rule__Exponent__Group_0__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3025:1: rule__Exponent__Group_0__0 : rule__Exponent__Group_0__0__Impl rule__Exponent__Group_0__1 ;
    public final void rule__Exponent__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3029:1: ( rule__Exponent__Group_0__0__Impl rule__Exponent__Group_0__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3030:2: rule__Exponent__Group_0__0__Impl rule__Exponent__Group_0__1
            {
            pushFollow(FOLLOW_rule__Exponent__Group_0__0__Impl_in_rule__Exponent__Group_0__06378);
            rule__Exponent__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Exponent__Group_0__1_in_rule__Exponent__Group_0__06381);
            rule__Exponent__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponent__Group_0__0"


    // $ANTLR start "rule__Exponent__Group_0__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3037:1: rule__Exponent__Group_0__0__Impl : ( 'E' ) ;
    public final void rule__Exponent__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3041:1: ( ( 'E' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3042:1: ( 'E' )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3042:1: ( 'E' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3043:1: 'E'
            {
             before(grammarAccess.getExponentAccess().getEKeyword_0_0()); 
            match(input,17,FOLLOW_17_in_rule__Exponent__Group_0__0__Impl6409); 
             after(grammarAccess.getExponentAccess().getEKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponent__Group_0__0__Impl"


    // $ANTLR start "rule__Exponent__Group_0__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3056:1: rule__Exponent__Group_0__1 : rule__Exponent__Group_0__1__Impl rule__Exponent__Group_0__2 ;
    public final void rule__Exponent__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3060:1: ( rule__Exponent__Group_0__1__Impl rule__Exponent__Group_0__2 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3061:2: rule__Exponent__Group_0__1__Impl rule__Exponent__Group_0__2
            {
            pushFollow(FOLLOW_rule__Exponent__Group_0__1__Impl_in_rule__Exponent__Group_0__16440);
            rule__Exponent__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Exponent__Group_0__2_in_rule__Exponent__Group_0__16443);
            rule__Exponent__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponent__Group_0__1"


    // $ANTLR start "rule__Exponent__Group_0__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3068:1: rule__Exponent__Group_0__1__Impl : ( ( '+' )? ) ;
    public final void rule__Exponent__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3072:1: ( ( ( '+' )? ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3073:1: ( ( '+' )? )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3073:1: ( ( '+' )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3074:1: ( '+' )?
            {
             before(grammarAccess.getExponentAccess().getPlusSignKeyword_0_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3075:1: ( '+' )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==18) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3076:2: '+'
                    {
                    match(input,18,FOLLOW_18_in_rule__Exponent__Group_0__1__Impl6472); 

                    }
                    break;

            }

             after(grammarAccess.getExponentAccess().getPlusSignKeyword_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponent__Group_0__1__Impl"


    // $ANTLR start "rule__Exponent__Group_0__2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3087:1: rule__Exponent__Group_0__2 : rule__Exponent__Group_0__2__Impl ;
    public final void rule__Exponent__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3091:1: ( rule__Exponent__Group_0__2__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3092:2: rule__Exponent__Group_0__2__Impl
            {
            pushFollow(FOLLOW_rule__Exponent__Group_0__2__Impl_in_rule__Exponent__Group_0__26505);
            rule__Exponent__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponent__Group_0__2"


    // $ANTLR start "rule__Exponent__Group_0__2__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3098:1: rule__Exponent__Group_0__2__Impl : ( ( rule__Exponent__Ename1Assignment_0_2 ) ) ;
    public final void rule__Exponent__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3102:1: ( ( ( rule__Exponent__Ename1Assignment_0_2 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3103:1: ( ( rule__Exponent__Ename1Assignment_0_2 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3103:1: ( ( rule__Exponent__Ename1Assignment_0_2 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3104:1: ( rule__Exponent__Ename1Assignment_0_2 )
            {
             before(grammarAccess.getExponentAccess().getEname1Assignment_0_2()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3105:1: ( rule__Exponent__Ename1Assignment_0_2 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3105:2: rule__Exponent__Ename1Assignment_0_2
            {
            pushFollow(FOLLOW_rule__Exponent__Ename1Assignment_0_2_in_rule__Exponent__Group_0__2__Impl6532);
            rule__Exponent__Ename1Assignment_0_2();

            state._fsp--;


            }

             after(grammarAccess.getExponentAccess().getEname1Assignment_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponent__Group_0__2__Impl"


    // $ANTLR start "rule__Exponent__Group_1__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3121:1: rule__Exponent__Group_1__0 : rule__Exponent__Group_1__0__Impl rule__Exponent__Group_1__1 ;
    public final void rule__Exponent__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3125:1: ( rule__Exponent__Group_1__0__Impl rule__Exponent__Group_1__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3126:2: rule__Exponent__Group_1__0__Impl rule__Exponent__Group_1__1
            {
            pushFollow(FOLLOW_rule__Exponent__Group_1__0__Impl_in_rule__Exponent__Group_1__06568);
            rule__Exponent__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Exponent__Group_1__1_in_rule__Exponent__Group_1__06571);
            rule__Exponent__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponent__Group_1__0"


    // $ANTLR start "rule__Exponent__Group_1__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3133:1: rule__Exponent__Group_1__0__Impl : ( 'E -' ) ;
    public final void rule__Exponent__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3137:1: ( ( 'E -' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3138:1: ( 'E -' )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3138:1: ( 'E -' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3139:1: 'E -'
            {
             before(grammarAccess.getExponentAccess().getEKeyword_1_0()); 
            match(input,19,FOLLOW_19_in_rule__Exponent__Group_1__0__Impl6599); 
             after(grammarAccess.getExponentAccess().getEKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponent__Group_1__0__Impl"


    // $ANTLR start "rule__Exponent__Group_1__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3152:1: rule__Exponent__Group_1__1 : rule__Exponent__Group_1__1__Impl ;
    public final void rule__Exponent__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3156:1: ( rule__Exponent__Group_1__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3157:2: rule__Exponent__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Exponent__Group_1__1__Impl_in_rule__Exponent__Group_1__16630);
            rule__Exponent__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponent__Group_1__1"


    // $ANTLR start "rule__Exponent__Group_1__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3163:1: rule__Exponent__Group_1__1__Impl : ( ( rule__Exponent__Ename2Assignment_1_1 ) ) ;
    public final void rule__Exponent__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3167:1: ( ( ( rule__Exponent__Ename2Assignment_1_1 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3168:1: ( ( rule__Exponent__Ename2Assignment_1_1 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3168:1: ( ( rule__Exponent__Ename2Assignment_1_1 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3169:1: ( rule__Exponent__Ename2Assignment_1_1 )
            {
             before(grammarAccess.getExponentAccess().getEname2Assignment_1_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3170:1: ( rule__Exponent__Ename2Assignment_1_1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3170:2: rule__Exponent__Ename2Assignment_1_1
            {
            pushFollow(FOLLOW_rule__Exponent__Ename2Assignment_1_1_in_rule__Exponent__Group_1__1__Impl6657);
            rule__Exponent__Ename2Assignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getExponentAccess().getEname2Assignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponent__Group_1__1__Impl"


    // $ANTLR start "rule__Positive_exponent__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3184:1: rule__Positive_exponent__Group__0 : rule__Positive_exponent__Group__0__Impl rule__Positive_exponent__Group__1 ;
    public final void rule__Positive_exponent__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3188:1: ( rule__Positive_exponent__Group__0__Impl rule__Positive_exponent__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3189:2: rule__Positive_exponent__Group__0__Impl rule__Positive_exponent__Group__1
            {
            pushFollow(FOLLOW_rule__Positive_exponent__Group__0__Impl_in_rule__Positive_exponent__Group__06691);
            rule__Positive_exponent__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Positive_exponent__Group__1_in_rule__Positive_exponent__Group__06694);
            rule__Positive_exponent__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Positive_exponent__Group__0"


    // $ANTLR start "rule__Positive_exponent__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3196:1: rule__Positive_exponent__Group__0__Impl : ( 'E' ) ;
    public final void rule__Positive_exponent__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3200:1: ( ( 'E' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3201:1: ( 'E' )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3201:1: ( 'E' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3202:1: 'E'
            {
             before(grammarAccess.getPositive_exponentAccess().getEKeyword_0()); 
            match(input,17,FOLLOW_17_in_rule__Positive_exponent__Group__0__Impl6722); 
             after(grammarAccess.getPositive_exponentAccess().getEKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Positive_exponent__Group__0__Impl"


    // $ANTLR start "rule__Positive_exponent__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3215:1: rule__Positive_exponent__Group__1 : rule__Positive_exponent__Group__1__Impl rule__Positive_exponent__Group__2 ;
    public final void rule__Positive_exponent__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3219:1: ( rule__Positive_exponent__Group__1__Impl rule__Positive_exponent__Group__2 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3220:2: rule__Positive_exponent__Group__1__Impl rule__Positive_exponent__Group__2
            {
            pushFollow(FOLLOW_rule__Positive_exponent__Group__1__Impl_in_rule__Positive_exponent__Group__16753);
            rule__Positive_exponent__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Positive_exponent__Group__2_in_rule__Positive_exponent__Group__16756);
            rule__Positive_exponent__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Positive_exponent__Group__1"


    // $ANTLR start "rule__Positive_exponent__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3227:1: rule__Positive_exponent__Group__1__Impl : ( ( '+' )? ) ;
    public final void rule__Positive_exponent__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3231:1: ( ( ( '+' )? ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3232:1: ( ( '+' )? )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3232:1: ( ( '+' )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3233:1: ( '+' )?
            {
             before(grammarAccess.getPositive_exponentAccess().getPlusSignKeyword_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3234:1: ( '+' )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==18) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3235:2: '+'
                    {
                    match(input,18,FOLLOW_18_in_rule__Positive_exponent__Group__1__Impl6785); 

                    }
                    break;

            }

             after(grammarAccess.getPositive_exponentAccess().getPlusSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Positive_exponent__Group__1__Impl"


    // $ANTLR start "rule__Positive_exponent__Group__2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3246:1: rule__Positive_exponent__Group__2 : rule__Positive_exponent__Group__2__Impl ;
    public final void rule__Positive_exponent__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3250:1: ( rule__Positive_exponent__Group__2__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3251:2: rule__Positive_exponent__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Positive_exponent__Group__2__Impl_in_rule__Positive_exponent__Group__26818);
            rule__Positive_exponent__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Positive_exponent__Group__2"


    // $ANTLR start "rule__Positive_exponent__Group__2__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3257:1: rule__Positive_exponent__Group__2__Impl : ( ( rule__Positive_exponent__PenameAssignment_2 ) ) ;
    public final void rule__Positive_exponent__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3261:1: ( ( ( rule__Positive_exponent__PenameAssignment_2 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3262:1: ( ( rule__Positive_exponent__PenameAssignment_2 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3262:1: ( ( rule__Positive_exponent__PenameAssignment_2 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3263:1: ( rule__Positive_exponent__PenameAssignment_2 )
            {
             before(grammarAccess.getPositive_exponentAccess().getPenameAssignment_2()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3264:1: ( rule__Positive_exponent__PenameAssignment_2 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3264:2: rule__Positive_exponent__PenameAssignment_2
            {
            pushFollow(FOLLOW_rule__Positive_exponent__PenameAssignment_2_in_rule__Positive_exponent__Group__2__Impl6845);
            rule__Positive_exponent__PenameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getPositive_exponentAccess().getPenameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Positive_exponent__Group__2__Impl"


    // $ANTLR start "rule__Based_integer_literal__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3280:1: rule__Based_integer_literal__Group__0 : rule__Based_integer_literal__Group__0__Impl rule__Based_integer_literal__Group__1 ;
    public final void rule__Based_integer_literal__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3284:1: ( rule__Based_integer_literal__Group__0__Impl rule__Based_integer_literal__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3285:2: rule__Based_integer_literal__Group__0__Impl rule__Based_integer_literal__Group__1
            {
            pushFollow(FOLLOW_rule__Based_integer_literal__Group__0__Impl_in_rule__Based_integer_literal__Group__06881);
            rule__Based_integer_literal__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Based_integer_literal__Group__1_in_rule__Based_integer_literal__Group__06884);
            rule__Based_integer_literal__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_integer_literal__Group__0"


    // $ANTLR start "rule__Based_integer_literal__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3292:1: rule__Based_integer_literal__Group__0__Impl : ( ( rule__Based_integer_literal__Bilname1Assignment_0 ) ) ;
    public final void rule__Based_integer_literal__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3296:1: ( ( ( rule__Based_integer_literal__Bilname1Assignment_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3297:1: ( ( rule__Based_integer_literal__Bilname1Assignment_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3297:1: ( ( rule__Based_integer_literal__Bilname1Assignment_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3298:1: ( rule__Based_integer_literal__Bilname1Assignment_0 )
            {
             before(grammarAccess.getBased_integer_literalAccess().getBilname1Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3299:1: ( rule__Based_integer_literal__Bilname1Assignment_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3299:2: rule__Based_integer_literal__Bilname1Assignment_0
            {
            pushFollow(FOLLOW_rule__Based_integer_literal__Bilname1Assignment_0_in_rule__Based_integer_literal__Group__0__Impl6911);
            rule__Based_integer_literal__Bilname1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getBased_integer_literalAccess().getBilname1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_integer_literal__Group__0__Impl"


    // $ANTLR start "rule__Based_integer_literal__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3309:1: rule__Based_integer_literal__Group__1 : rule__Based_integer_literal__Group__1__Impl rule__Based_integer_literal__Group__2 ;
    public final void rule__Based_integer_literal__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3313:1: ( rule__Based_integer_literal__Group__1__Impl rule__Based_integer_literal__Group__2 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3314:2: rule__Based_integer_literal__Group__1__Impl rule__Based_integer_literal__Group__2
            {
            pushFollow(FOLLOW_rule__Based_integer_literal__Group__1__Impl_in_rule__Based_integer_literal__Group__16941);
            rule__Based_integer_literal__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Based_integer_literal__Group__2_in_rule__Based_integer_literal__Group__16944);
            rule__Based_integer_literal__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_integer_literal__Group__1"


    // $ANTLR start "rule__Based_integer_literal__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3321:1: rule__Based_integer_literal__Group__1__Impl : ( '#' ) ;
    public final void rule__Based_integer_literal__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3325:1: ( ( '#' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3326:1: ( '#' )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3326:1: ( '#' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3327:1: '#'
            {
             before(grammarAccess.getBased_integer_literalAccess().getNumberSignKeyword_1()); 
            match(input,20,FOLLOW_20_in_rule__Based_integer_literal__Group__1__Impl6972); 
             after(grammarAccess.getBased_integer_literalAccess().getNumberSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_integer_literal__Group__1__Impl"


    // $ANTLR start "rule__Based_integer_literal__Group__2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3340:1: rule__Based_integer_literal__Group__2 : rule__Based_integer_literal__Group__2__Impl rule__Based_integer_literal__Group__3 ;
    public final void rule__Based_integer_literal__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3344:1: ( rule__Based_integer_literal__Group__2__Impl rule__Based_integer_literal__Group__3 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3345:2: rule__Based_integer_literal__Group__2__Impl rule__Based_integer_literal__Group__3
            {
            pushFollow(FOLLOW_rule__Based_integer_literal__Group__2__Impl_in_rule__Based_integer_literal__Group__27003);
            rule__Based_integer_literal__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Based_integer_literal__Group__3_in_rule__Based_integer_literal__Group__27006);
            rule__Based_integer_literal__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_integer_literal__Group__2"


    // $ANTLR start "rule__Based_integer_literal__Group__2__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3352:1: rule__Based_integer_literal__Group__2__Impl : ( ( rule__Based_integer_literal__Bilname2Assignment_2 ) ) ;
    public final void rule__Based_integer_literal__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3356:1: ( ( ( rule__Based_integer_literal__Bilname2Assignment_2 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3357:1: ( ( rule__Based_integer_literal__Bilname2Assignment_2 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3357:1: ( ( rule__Based_integer_literal__Bilname2Assignment_2 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3358:1: ( rule__Based_integer_literal__Bilname2Assignment_2 )
            {
             before(grammarAccess.getBased_integer_literalAccess().getBilname2Assignment_2()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3359:1: ( rule__Based_integer_literal__Bilname2Assignment_2 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3359:2: rule__Based_integer_literal__Bilname2Assignment_2
            {
            pushFollow(FOLLOW_rule__Based_integer_literal__Bilname2Assignment_2_in_rule__Based_integer_literal__Group__2__Impl7033);
            rule__Based_integer_literal__Bilname2Assignment_2();

            state._fsp--;


            }

             after(grammarAccess.getBased_integer_literalAccess().getBilname2Assignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_integer_literal__Group__2__Impl"


    // $ANTLR start "rule__Based_integer_literal__Group__3"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3369:1: rule__Based_integer_literal__Group__3 : rule__Based_integer_literal__Group__3__Impl rule__Based_integer_literal__Group__4 ;
    public final void rule__Based_integer_literal__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3373:1: ( rule__Based_integer_literal__Group__3__Impl rule__Based_integer_literal__Group__4 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3374:2: rule__Based_integer_literal__Group__3__Impl rule__Based_integer_literal__Group__4
            {
            pushFollow(FOLLOW_rule__Based_integer_literal__Group__3__Impl_in_rule__Based_integer_literal__Group__37063);
            rule__Based_integer_literal__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Based_integer_literal__Group__4_in_rule__Based_integer_literal__Group__37066);
            rule__Based_integer_literal__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_integer_literal__Group__3"


    // $ANTLR start "rule__Based_integer_literal__Group__3__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3381:1: rule__Based_integer_literal__Group__3__Impl : ( '#' ) ;
    public final void rule__Based_integer_literal__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3385:1: ( ( '#' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3386:1: ( '#' )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3386:1: ( '#' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3387:1: '#'
            {
             before(grammarAccess.getBased_integer_literalAccess().getNumberSignKeyword_3()); 
            match(input,20,FOLLOW_20_in_rule__Based_integer_literal__Group__3__Impl7094); 
             after(grammarAccess.getBased_integer_literalAccess().getNumberSignKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_integer_literal__Group__3__Impl"


    // $ANTLR start "rule__Based_integer_literal__Group__4"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3400:1: rule__Based_integer_literal__Group__4 : rule__Based_integer_literal__Group__4__Impl ;
    public final void rule__Based_integer_literal__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3404:1: ( rule__Based_integer_literal__Group__4__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3405:2: rule__Based_integer_literal__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Based_integer_literal__Group__4__Impl_in_rule__Based_integer_literal__Group__47125);
            rule__Based_integer_literal__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_integer_literal__Group__4"


    // $ANTLR start "rule__Based_integer_literal__Group__4__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3411:1: rule__Based_integer_literal__Group__4__Impl : ( ( rule__Based_integer_literal__Bilname3Assignment_4 )? ) ;
    public final void rule__Based_integer_literal__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3415:1: ( ( ( rule__Based_integer_literal__Bilname3Assignment_4 )? ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3416:1: ( ( rule__Based_integer_literal__Bilname3Assignment_4 )? )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3416:1: ( ( rule__Based_integer_literal__Bilname3Assignment_4 )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3417:1: ( rule__Based_integer_literal__Bilname3Assignment_4 )?
            {
             before(grammarAccess.getBased_integer_literalAccess().getBilname3Assignment_4()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3418:1: ( rule__Based_integer_literal__Bilname3Assignment_4 )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==17) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3418:2: rule__Based_integer_literal__Bilname3Assignment_4
                    {
                    pushFollow(FOLLOW_rule__Based_integer_literal__Bilname3Assignment_4_in_rule__Based_integer_literal__Group__4__Impl7152);
                    rule__Based_integer_literal__Bilname3Assignment_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBased_integer_literalAccess().getBilname3Assignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_integer_literal__Group__4__Impl"


    // $ANTLR start "rule__Base__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3438:1: rule__Base__Group__0 : rule__Base__Group__0__Impl rule__Base__Group__1 ;
    public final void rule__Base__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3442:1: ( rule__Base__Group__0__Impl rule__Base__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3443:2: rule__Base__Group__0__Impl rule__Base__Group__1
            {
            pushFollow(FOLLOW_rule__Base__Group__0__Impl_in_rule__Base__Group__07193);
            rule__Base__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Base__Group__1_in_rule__Base__Group__07196);
            rule__Base__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Base__Group__0"


    // $ANTLR start "rule__Base__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3450:1: rule__Base__Group__0__Impl : ( ( rule__Base__Bname1Assignment_0 ) ) ;
    public final void rule__Base__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3454:1: ( ( ( rule__Base__Bname1Assignment_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3455:1: ( ( rule__Base__Bname1Assignment_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3455:1: ( ( rule__Base__Bname1Assignment_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3456:1: ( rule__Base__Bname1Assignment_0 )
            {
             before(grammarAccess.getBaseAccess().getBname1Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3457:1: ( rule__Base__Bname1Assignment_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3457:2: rule__Base__Bname1Assignment_0
            {
            pushFollow(FOLLOW_rule__Base__Bname1Assignment_0_in_rule__Base__Group__0__Impl7223);
            rule__Base__Bname1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getBaseAccess().getBname1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Base__Group__0__Impl"


    // $ANTLR start "rule__Base__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3467:1: rule__Base__Group__1 : rule__Base__Group__1__Impl ;
    public final void rule__Base__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3471:1: ( rule__Base__Group__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3472:2: rule__Base__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Base__Group__1__Impl_in_rule__Base__Group__17253);
            rule__Base__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Base__Group__1"


    // $ANTLR start "rule__Base__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3478:1: rule__Base__Group__1__Impl : ( ( rule__Base__Bname2Assignment_1 )? ) ;
    public final void rule__Base__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3482:1: ( ( ( rule__Base__Bname2Assignment_1 )? ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3483:1: ( ( rule__Base__Bname2Assignment_1 )? )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3483:1: ( ( rule__Base__Bname2Assignment_1 )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3484:1: ( rule__Base__Bname2Assignment_1 )?
            {
             before(grammarAccess.getBaseAccess().getBname2Assignment_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3485:1: ( rule__Base__Bname2Assignment_1 )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==RULE_DIGIT) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3485:2: rule__Base__Bname2Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Base__Bname2Assignment_1_in_rule__Base__Group__1__Impl7280);
                    rule__Base__Bname2Assignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBaseAccess().getBname2Assignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Base__Group__1__Impl"


    // $ANTLR start "rule__Based_numeral__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3499:1: rule__Based_numeral__Group__0 : rule__Based_numeral__Group__0__Impl rule__Based_numeral__Group__1 ;
    public final void rule__Based_numeral__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3503:1: ( rule__Based_numeral__Group__0__Impl rule__Based_numeral__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3504:2: rule__Based_numeral__Group__0__Impl rule__Based_numeral__Group__1
            {
            pushFollow(FOLLOW_rule__Based_numeral__Group__0__Impl_in_rule__Based_numeral__Group__07315);
            rule__Based_numeral__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Based_numeral__Group__1_in_rule__Based_numeral__Group__07318);
            rule__Based_numeral__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_numeral__Group__0"


    // $ANTLR start "rule__Based_numeral__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3511:1: rule__Based_numeral__Group__0__Impl : ( ( rule__Based_numeral__Bnname1Assignment_0 ) ) ;
    public final void rule__Based_numeral__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3515:1: ( ( ( rule__Based_numeral__Bnname1Assignment_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3516:1: ( ( rule__Based_numeral__Bnname1Assignment_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3516:1: ( ( rule__Based_numeral__Bnname1Assignment_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3517:1: ( rule__Based_numeral__Bnname1Assignment_0 )
            {
             before(grammarAccess.getBased_numeralAccess().getBnname1Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3518:1: ( rule__Based_numeral__Bnname1Assignment_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3518:2: rule__Based_numeral__Bnname1Assignment_0
            {
            pushFollow(FOLLOW_rule__Based_numeral__Bnname1Assignment_0_in_rule__Based_numeral__Group__0__Impl7345);
            rule__Based_numeral__Bnname1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getBased_numeralAccess().getBnname1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_numeral__Group__0__Impl"


    // $ANTLR start "rule__Based_numeral__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3528:1: rule__Based_numeral__Group__1 : rule__Based_numeral__Group__1__Impl ;
    public final void rule__Based_numeral__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3532:1: ( rule__Based_numeral__Group__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3533:2: rule__Based_numeral__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Based_numeral__Group__1__Impl_in_rule__Based_numeral__Group__17375);
            rule__Based_numeral__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_numeral__Group__1"


    // $ANTLR start "rule__Based_numeral__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3539:1: rule__Based_numeral__Group__1__Impl : ( ( rule__Based_numeral__Group_1__0 )? ) ;
    public final void rule__Based_numeral__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3543:1: ( ( ( rule__Based_numeral__Group_1__0 )? ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3544:1: ( ( rule__Based_numeral__Group_1__0 )? )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3544:1: ( ( rule__Based_numeral__Group_1__0 )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3545:1: ( rule__Based_numeral__Group_1__0 )?
            {
             before(grammarAccess.getBased_numeralAccess().getGroup_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3546:1: ( rule__Based_numeral__Group_1__0 )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==RULE_EXTENDED_DIGIT||LA34_0==16) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3546:2: rule__Based_numeral__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__Based_numeral__Group_1__0_in_rule__Based_numeral__Group__1__Impl7402);
                    rule__Based_numeral__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBased_numeralAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_numeral__Group__1__Impl"


    // $ANTLR start "rule__Based_numeral__Group_1__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3560:1: rule__Based_numeral__Group_1__0 : rule__Based_numeral__Group_1__0__Impl rule__Based_numeral__Group_1__1 ;
    public final void rule__Based_numeral__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3564:1: ( rule__Based_numeral__Group_1__0__Impl rule__Based_numeral__Group_1__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3565:2: rule__Based_numeral__Group_1__0__Impl rule__Based_numeral__Group_1__1
            {
            pushFollow(FOLLOW_rule__Based_numeral__Group_1__0__Impl_in_rule__Based_numeral__Group_1__07437);
            rule__Based_numeral__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Based_numeral__Group_1__1_in_rule__Based_numeral__Group_1__07440);
            rule__Based_numeral__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_numeral__Group_1__0"


    // $ANTLR start "rule__Based_numeral__Group_1__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3572:1: rule__Based_numeral__Group_1__0__Impl : ( ( '_' )? ) ;
    public final void rule__Based_numeral__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3576:1: ( ( ( '_' )? ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3577:1: ( ( '_' )? )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3577:1: ( ( '_' )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3578:1: ( '_' )?
            {
             before(grammarAccess.getBased_numeralAccess().get_Keyword_1_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3579:1: ( '_' )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==16) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3580:2: '_'
                    {
                    match(input,16,FOLLOW_16_in_rule__Based_numeral__Group_1__0__Impl7469); 

                    }
                    break;

            }

             after(grammarAccess.getBased_numeralAccess().get_Keyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_numeral__Group_1__0__Impl"


    // $ANTLR start "rule__Based_numeral__Group_1__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3591:1: rule__Based_numeral__Group_1__1 : rule__Based_numeral__Group_1__1__Impl ;
    public final void rule__Based_numeral__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3595:1: ( rule__Based_numeral__Group_1__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3596:2: rule__Based_numeral__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Based_numeral__Group_1__1__Impl_in_rule__Based_numeral__Group_1__17502);
            rule__Based_numeral__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_numeral__Group_1__1"


    // $ANTLR start "rule__Based_numeral__Group_1__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3602:1: rule__Based_numeral__Group_1__1__Impl : ( ( rule__Based_numeral__Bnname2Assignment_1_1 ) ) ;
    public final void rule__Based_numeral__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3606:1: ( ( ( rule__Based_numeral__Bnname2Assignment_1_1 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3607:1: ( ( rule__Based_numeral__Bnname2Assignment_1_1 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3607:1: ( ( rule__Based_numeral__Bnname2Assignment_1_1 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3608:1: ( rule__Based_numeral__Bnname2Assignment_1_1 )
            {
             before(grammarAccess.getBased_numeralAccess().getBnname2Assignment_1_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3609:1: ( rule__Based_numeral__Bnname2Assignment_1_1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3609:2: rule__Based_numeral__Bnname2Assignment_1_1
            {
            pushFollow(FOLLOW_rule__Based_numeral__Bnname2Assignment_1_1_in_rule__Based_numeral__Group_1__1__Impl7529);
            rule__Based_numeral__Bnname2Assignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getBased_numeralAccess().getBnname2Assignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_numeral__Group_1__1__Impl"


    // $ANTLR start "rule__Dispatch_condition__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3623:1: rule__Dispatch_condition__Group__0 : rule__Dispatch_condition__Group__0__Impl rule__Dispatch_condition__Group__1 ;
    public final void rule__Dispatch_condition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3627:1: ( rule__Dispatch_condition__Group__0__Impl rule__Dispatch_condition__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3628:2: rule__Dispatch_condition__Group__0__Impl rule__Dispatch_condition__Group__1
            {
            pushFollow(FOLLOW_rule__Dispatch_condition__Group__0__Impl_in_rule__Dispatch_condition__Group__07563);
            rule__Dispatch_condition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Dispatch_condition__Group__1_in_rule__Dispatch_condition__Group__07566);
            rule__Dispatch_condition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_condition__Group__0"


    // $ANTLR start "rule__Dispatch_condition__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3635:1: rule__Dispatch_condition__Group__0__Impl : ( ( rule__Dispatch_condition__Dcname3Assignment_0 ) ) ;
    public final void rule__Dispatch_condition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3639:1: ( ( ( rule__Dispatch_condition__Dcname3Assignment_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3640:1: ( ( rule__Dispatch_condition__Dcname3Assignment_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3640:1: ( ( rule__Dispatch_condition__Dcname3Assignment_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3641:1: ( rule__Dispatch_condition__Dcname3Assignment_0 )
            {
             before(grammarAccess.getDispatch_conditionAccess().getDcname3Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3642:1: ( rule__Dispatch_condition__Dcname3Assignment_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3642:2: rule__Dispatch_condition__Dcname3Assignment_0
            {
            pushFollow(FOLLOW_rule__Dispatch_condition__Dcname3Assignment_0_in_rule__Dispatch_condition__Group__0__Impl7593);
            rule__Dispatch_condition__Dcname3Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getDispatch_conditionAccess().getDcname3Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_condition__Group__0__Impl"


    // $ANTLR start "rule__Dispatch_condition__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3652:1: rule__Dispatch_condition__Group__1 : rule__Dispatch_condition__Group__1__Impl rule__Dispatch_condition__Group__2 ;
    public final void rule__Dispatch_condition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3656:1: ( rule__Dispatch_condition__Group__1__Impl rule__Dispatch_condition__Group__2 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3657:2: rule__Dispatch_condition__Group__1__Impl rule__Dispatch_condition__Group__2
            {
            pushFollow(FOLLOW_rule__Dispatch_condition__Group__1__Impl_in_rule__Dispatch_condition__Group__17623);
            rule__Dispatch_condition__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Dispatch_condition__Group__2_in_rule__Dispatch_condition__Group__17626);
            rule__Dispatch_condition__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_condition__Group__1"


    // $ANTLR start "rule__Dispatch_condition__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3664:1: rule__Dispatch_condition__Group__1__Impl : ( ( rule__Dispatch_condition__Dcname1Assignment_1 )? ) ;
    public final void rule__Dispatch_condition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3668:1: ( ( ( rule__Dispatch_condition__Dcname1Assignment_1 )? ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3669:1: ( ( rule__Dispatch_condition__Dcname1Assignment_1 )? )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3669:1: ( ( rule__Dispatch_condition__Dcname1Assignment_1 )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3670:1: ( rule__Dispatch_condition__Dcname1Assignment_1 )?
            {
             before(grammarAccess.getDispatch_conditionAccess().getDcname1Assignment_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3671:1: ( rule__Dispatch_condition__Dcname1Assignment_1 )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==RULE_STRING_LITERAL||LA36_0==26||LA36_0==55) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3671:2: rule__Dispatch_condition__Dcname1Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Dispatch_condition__Dcname1Assignment_1_in_rule__Dispatch_condition__Group__1__Impl7653);
                    rule__Dispatch_condition__Dcname1Assignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDispatch_conditionAccess().getDcname1Assignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_condition__Group__1__Impl"


    // $ANTLR start "rule__Dispatch_condition__Group__2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3681:1: rule__Dispatch_condition__Group__2 : rule__Dispatch_condition__Group__2__Impl ;
    public final void rule__Dispatch_condition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3685:1: ( rule__Dispatch_condition__Group__2__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3686:2: rule__Dispatch_condition__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Dispatch_condition__Group__2__Impl_in_rule__Dispatch_condition__Group__27684);
            rule__Dispatch_condition__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_condition__Group__2"


    // $ANTLR start "rule__Dispatch_condition__Group__2__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3692:1: rule__Dispatch_condition__Group__2__Impl : ( ( rule__Dispatch_condition__Group_2__0 )? ) ;
    public final void rule__Dispatch_condition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3696:1: ( ( ( rule__Dispatch_condition__Group_2__0 )? ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3697:1: ( ( rule__Dispatch_condition__Group_2__0 )? )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3697:1: ( ( rule__Dispatch_condition__Group_2__0 )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3698:1: ( rule__Dispatch_condition__Group_2__0 )?
            {
             before(grammarAccess.getDispatch_conditionAccess().getGroup_2()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3699:1: ( rule__Dispatch_condition__Group_2__0 )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==21) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3699:2: rule__Dispatch_condition__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__Dispatch_condition__Group_2__0_in_rule__Dispatch_condition__Group__2__Impl7711);
                    rule__Dispatch_condition__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDispatch_conditionAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_condition__Group__2__Impl"


    // $ANTLR start "rule__Dispatch_condition__Group_2__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3715:1: rule__Dispatch_condition__Group_2__0 : rule__Dispatch_condition__Group_2__0__Impl rule__Dispatch_condition__Group_2__1 ;
    public final void rule__Dispatch_condition__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3719:1: ( rule__Dispatch_condition__Group_2__0__Impl rule__Dispatch_condition__Group_2__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3720:2: rule__Dispatch_condition__Group_2__0__Impl rule__Dispatch_condition__Group_2__1
            {
            pushFollow(FOLLOW_rule__Dispatch_condition__Group_2__0__Impl_in_rule__Dispatch_condition__Group_2__07748);
            rule__Dispatch_condition__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Dispatch_condition__Group_2__1_in_rule__Dispatch_condition__Group_2__07751);
            rule__Dispatch_condition__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_condition__Group_2__0"


    // $ANTLR start "rule__Dispatch_condition__Group_2__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3727:1: rule__Dispatch_condition__Group_2__0__Impl : ( 'frozen' ) ;
    public final void rule__Dispatch_condition__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3731:1: ( ( 'frozen' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3732:1: ( 'frozen' )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3732:1: ( 'frozen' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3733:1: 'frozen'
            {
             before(grammarAccess.getDispatch_conditionAccess().getFrozenKeyword_2_0()); 
            match(input,21,FOLLOW_21_in_rule__Dispatch_condition__Group_2__0__Impl7779); 
             after(grammarAccess.getDispatch_conditionAccess().getFrozenKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_condition__Group_2__0__Impl"


    // $ANTLR start "rule__Dispatch_condition__Group_2__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3746:1: rule__Dispatch_condition__Group_2__1 : rule__Dispatch_condition__Group_2__1__Impl ;
    public final void rule__Dispatch_condition__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3750:1: ( rule__Dispatch_condition__Group_2__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3751:2: rule__Dispatch_condition__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__Dispatch_condition__Group_2__1__Impl_in_rule__Dispatch_condition__Group_2__17810);
            rule__Dispatch_condition__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_condition__Group_2__1"


    // $ANTLR start "rule__Dispatch_condition__Group_2__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3757:1: rule__Dispatch_condition__Group_2__1__Impl : ( ( rule__Dispatch_condition__Dcname2Assignment_2_1 ) ) ;
    public final void rule__Dispatch_condition__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3761:1: ( ( ( rule__Dispatch_condition__Dcname2Assignment_2_1 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3762:1: ( ( rule__Dispatch_condition__Dcname2Assignment_2_1 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3762:1: ( ( rule__Dispatch_condition__Dcname2Assignment_2_1 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3763:1: ( rule__Dispatch_condition__Dcname2Assignment_2_1 )
            {
             before(grammarAccess.getDispatch_conditionAccess().getDcname2Assignment_2_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3764:1: ( rule__Dispatch_condition__Dcname2Assignment_2_1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3764:2: rule__Dispatch_condition__Dcname2Assignment_2_1
            {
            pushFollow(FOLLOW_rule__Dispatch_condition__Dcname2Assignment_2_1_in_rule__Dispatch_condition__Group_2__1__Impl7837);
            rule__Dispatch_condition__Dcname2Assignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getDispatch_conditionAccess().getDcname2Assignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_condition__Group_2__1__Impl"


    // $ANTLR start "rule__Dispatch_trigger_logical_expression__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3778:1: rule__Dispatch_trigger_logical_expression__Group__0 : rule__Dispatch_trigger_logical_expression__Group__0__Impl rule__Dispatch_trigger_logical_expression__Group__1 ;
    public final void rule__Dispatch_trigger_logical_expression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3782:1: ( rule__Dispatch_trigger_logical_expression__Group__0__Impl rule__Dispatch_trigger_logical_expression__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3783:2: rule__Dispatch_trigger_logical_expression__Group__0__Impl rule__Dispatch_trigger_logical_expression__Group__1
            {
            pushFollow(FOLLOW_rule__Dispatch_trigger_logical_expression__Group__0__Impl_in_rule__Dispatch_trigger_logical_expression__Group__07871);
            rule__Dispatch_trigger_logical_expression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Dispatch_trigger_logical_expression__Group__1_in_rule__Dispatch_trigger_logical_expression__Group__07874);
            rule__Dispatch_trigger_logical_expression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_trigger_logical_expression__Group__0"


    // $ANTLR start "rule__Dispatch_trigger_logical_expression__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3790:1: rule__Dispatch_trigger_logical_expression__Group__0__Impl : ( ( rule__Dispatch_trigger_logical_expression__Dtlename1Assignment_0 ) ) ;
    public final void rule__Dispatch_trigger_logical_expression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3794:1: ( ( ( rule__Dispatch_trigger_logical_expression__Dtlename1Assignment_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3795:1: ( ( rule__Dispatch_trigger_logical_expression__Dtlename1Assignment_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3795:1: ( ( rule__Dispatch_trigger_logical_expression__Dtlename1Assignment_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3796:1: ( rule__Dispatch_trigger_logical_expression__Dtlename1Assignment_0 )
            {
             before(grammarAccess.getDispatch_trigger_logical_expressionAccess().getDtlename1Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3797:1: ( rule__Dispatch_trigger_logical_expression__Dtlename1Assignment_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3797:2: rule__Dispatch_trigger_logical_expression__Dtlename1Assignment_0
            {
            pushFollow(FOLLOW_rule__Dispatch_trigger_logical_expression__Dtlename1Assignment_0_in_rule__Dispatch_trigger_logical_expression__Group__0__Impl7901);
            rule__Dispatch_trigger_logical_expression__Dtlename1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getDispatch_trigger_logical_expressionAccess().getDtlename1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_trigger_logical_expression__Group__0__Impl"


    // $ANTLR start "rule__Dispatch_trigger_logical_expression__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3807:1: rule__Dispatch_trigger_logical_expression__Group__1 : rule__Dispatch_trigger_logical_expression__Group__1__Impl ;
    public final void rule__Dispatch_trigger_logical_expression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3811:1: ( rule__Dispatch_trigger_logical_expression__Group__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3812:2: rule__Dispatch_trigger_logical_expression__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Dispatch_trigger_logical_expression__Group__1__Impl_in_rule__Dispatch_trigger_logical_expression__Group__17931);
            rule__Dispatch_trigger_logical_expression__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_trigger_logical_expression__Group__1"


    // $ANTLR start "rule__Dispatch_trigger_logical_expression__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3818:1: rule__Dispatch_trigger_logical_expression__Group__1__Impl : ( ( rule__Dispatch_trigger_logical_expression__Group_1__0 )* ) ;
    public final void rule__Dispatch_trigger_logical_expression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3822:1: ( ( ( rule__Dispatch_trigger_logical_expression__Group_1__0 )* ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3823:1: ( ( rule__Dispatch_trigger_logical_expression__Group_1__0 )* )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3823:1: ( ( rule__Dispatch_trigger_logical_expression__Group_1__0 )* )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3824:1: ( rule__Dispatch_trigger_logical_expression__Group_1__0 )*
            {
             before(grammarAccess.getDispatch_trigger_logical_expressionAccess().getGroup_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3825:1: ( rule__Dispatch_trigger_logical_expression__Group_1__0 )*
            loop38:
            do {
                int alt38=2;
                int LA38_0 = input.LA(1);

                if ( (LA38_0==22) ) {
                    alt38=1;
                }


                switch (alt38) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3825:2: rule__Dispatch_trigger_logical_expression__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__Dispatch_trigger_logical_expression__Group_1__0_in_rule__Dispatch_trigger_logical_expression__Group__1__Impl7958);
            	    rule__Dispatch_trigger_logical_expression__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop38;
                }
            } while (true);

             after(grammarAccess.getDispatch_trigger_logical_expressionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_trigger_logical_expression__Group__1__Impl"


    // $ANTLR start "rule__Dispatch_trigger_logical_expression__Group_1__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3839:1: rule__Dispatch_trigger_logical_expression__Group_1__0 : rule__Dispatch_trigger_logical_expression__Group_1__0__Impl rule__Dispatch_trigger_logical_expression__Group_1__1 ;
    public final void rule__Dispatch_trigger_logical_expression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3843:1: ( rule__Dispatch_trigger_logical_expression__Group_1__0__Impl rule__Dispatch_trigger_logical_expression__Group_1__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3844:2: rule__Dispatch_trigger_logical_expression__Group_1__0__Impl rule__Dispatch_trigger_logical_expression__Group_1__1
            {
            pushFollow(FOLLOW_rule__Dispatch_trigger_logical_expression__Group_1__0__Impl_in_rule__Dispatch_trigger_logical_expression__Group_1__07993);
            rule__Dispatch_trigger_logical_expression__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Dispatch_trigger_logical_expression__Group_1__1_in_rule__Dispatch_trigger_logical_expression__Group_1__07996);
            rule__Dispatch_trigger_logical_expression__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_trigger_logical_expression__Group_1__0"


    // $ANTLR start "rule__Dispatch_trigger_logical_expression__Group_1__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3851:1: rule__Dispatch_trigger_logical_expression__Group_1__0__Impl : ( 'or' ) ;
    public final void rule__Dispatch_trigger_logical_expression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3855:1: ( ( 'or' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3856:1: ( 'or' )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3856:1: ( 'or' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3857:1: 'or'
            {
             before(grammarAccess.getDispatch_trigger_logical_expressionAccess().getOrKeyword_1_0()); 
            match(input,22,FOLLOW_22_in_rule__Dispatch_trigger_logical_expression__Group_1__0__Impl8024); 
             after(grammarAccess.getDispatch_trigger_logical_expressionAccess().getOrKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_trigger_logical_expression__Group_1__0__Impl"


    // $ANTLR start "rule__Dispatch_trigger_logical_expression__Group_1__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3870:1: rule__Dispatch_trigger_logical_expression__Group_1__1 : rule__Dispatch_trigger_logical_expression__Group_1__1__Impl ;
    public final void rule__Dispatch_trigger_logical_expression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3874:1: ( rule__Dispatch_trigger_logical_expression__Group_1__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3875:2: rule__Dispatch_trigger_logical_expression__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Dispatch_trigger_logical_expression__Group_1__1__Impl_in_rule__Dispatch_trigger_logical_expression__Group_1__18055);
            rule__Dispatch_trigger_logical_expression__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_trigger_logical_expression__Group_1__1"


    // $ANTLR start "rule__Dispatch_trigger_logical_expression__Group_1__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3881:1: rule__Dispatch_trigger_logical_expression__Group_1__1__Impl : ( ( rule__Dispatch_trigger_logical_expression__Dtlename2Assignment_1_1 ) ) ;
    public final void rule__Dispatch_trigger_logical_expression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3885:1: ( ( ( rule__Dispatch_trigger_logical_expression__Dtlename2Assignment_1_1 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3886:1: ( ( rule__Dispatch_trigger_logical_expression__Dtlename2Assignment_1_1 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3886:1: ( ( rule__Dispatch_trigger_logical_expression__Dtlename2Assignment_1_1 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3887:1: ( rule__Dispatch_trigger_logical_expression__Dtlename2Assignment_1_1 )
            {
             before(grammarAccess.getDispatch_trigger_logical_expressionAccess().getDtlename2Assignment_1_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3888:1: ( rule__Dispatch_trigger_logical_expression__Dtlename2Assignment_1_1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3888:2: rule__Dispatch_trigger_logical_expression__Dtlename2Assignment_1_1
            {
            pushFollow(FOLLOW_rule__Dispatch_trigger_logical_expression__Dtlename2Assignment_1_1_in_rule__Dispatch_trigger_logical_expression__Group_1__1__Impl8082);
            rule__Dispatch_trigger_logical_expression__Dtlename2Assignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getDispatch_trigger_logical_expressionAccess().getDtlename2Assignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_trigger_logical_expression__Group_1__1__Impl"


    // $ANTLR start "rule__Dispatch_conjunction__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3902:1: rule__Dispatch_conjunction__Group__0 : rule__Dispatch_conjunction__Group__0__Impl rule__Dispatch_conjunction__Group__1 ;
    public final void rule__Dispatch_conjunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3906:1: ( rule__Dispatch_conjunction__Group__0__Impl rule__Dispatch_conjunction__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3907:2: rule__Dispatch_conjunction__Group__0__Impl rule__Dispatch_conjunction__Group__1
            {
            pushFollow(FOLLOW_rule__Dispatch_conjunction__Group__0__Impl_in_rule__Dispatch_conjunction__Group__08116);
            rule__Dispatch_conjunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Dispatch_conjunction__Group__1_in_rule__Dispatch_conjunction__Group__08119);
            rule__Dispatch_conjunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_conjunction__Group__0"


    // $ANTLR start "rule__Dispatch_conjunction__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3914:1: rule__Dispatch_conjunction__Group__0__Impl : ( ( rule__Dispatch_conjunction__Dcname1Assignment_0 ) ) ;
    public final void rule__Dispatch_conjunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3918:1: ( ( ( rule__Dispatch_conjunction__Dcname1Assignment_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3919:1: ( ( rule__Dispatch_conjunction__Dcname1Assignment_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3919:1: ( ( rule__Dispatch_conjunction__Dcname1Assignment_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3920:1: ( rule__Dispatch_conjunction__Dcname1Assignment_0 )
            {
             before(grammarAccess.getDispatch_conjunctionAccess().getDcname1Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3921:1: ( rule__Dispatch_conjunction__Dcname1Assignment_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3921:2: rule__Dispatch_conjunction__Dcname1Assignment_0
            {
            pushFollow(FOLLOW_rule__Dispatch_conjunction__Dcname1Assignment_0_in_rule__Dispatch_conjunction__Group__0__Impl8146);
            rule__Dispatch_conjunction__Dcname1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getDispatch_conjunctionAccess().getDcname1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_conjunction__Group__0__Impl"


    // $ANTLR start "rule__Dispatch_conjunction__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3931:1: rule__Dispatch_conjunction__Group__1 : rule__Dispatch_conjunction__Group__1__Impl ;
    public final void rule__Dispatch_conjunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3935:1: ( rule__Dispatch_conjunction__Group__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3936:2: rule__Dispatch_conjunction__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Dispatch_conjunction__Group__1__Impl_in_rule__Dispatch_conjunction__Group__18176);
            rule__Dispatch_conjunction__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_conjunction__Group__1"


    // $ANTLR start "rule__Dispatch_conjunction__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3942:1: rule__Dispatch_conjunction__Group__1__Impl : ( ( rule__Dispatch_conjunction__Group_1__0 )* ) ;
    public final void rule__Dispatch_conjunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3946:1: ( ( ( rule__Dispatch_conjunction__Group_1__0 )* ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3947:1: ( ( rule__Dispatch_conjunction__Group_1__0 )* )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3947:1: ( ( rule__Dispatch_conjunction__Group_1__0 )* )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3948:1: ( rule__Dispatch_conjunction__Group_1__0 )*
            {
             before(grammarAccess.getDispatch_conjunctionAccess().getGroup_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3949:1: ( rule__Dispatch_conjunction__Group_1__0 )*
            loop39:
            do {
                int alt39=2;
                int LA39_0 = input.LA(1);

                if ( (LA39_0==23) ) {
                    alt39=1;
                }


                switch (alt39) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3949:2: rule__Dispatch_conjunction__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__Dispatch_conjunction__Group_1__0_in_rule__Dispatch_conjunction__Group__1__Impl8203);
            	    rule__Dispatch_conjunction__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop39;
                }
            } while (true);

             after(grammarAccess.getDispatch_conjunctionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_conjunction__Group__1__Impl"


    // $ANTLR start "rule__Dispatch_conjunction__Group_1__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3963:1: rule__Dispatch_conjunction__Group_1__0 : rule__Dispatch_conjunction__Group_1__0__Impl rule__Dispatch_conjunction__Group_1__1 ;
    public final void rule__Dispatch_conjunction__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3967:1: ( rule__Dispatch_conjunction__Group_1__0__Impl rule__Dispatch_conjunction__Group_1__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3968:2: rule__Dispatch_conjunction__Group_1__0__Impl rule__Dispatch_conjunction__Group_1__1
            {
            pushFollow(FOLLOW_rule__Dispatch_conjunction__Group_1__0__Impl_in_rule__Dispatch_conjunction__Group_1__08238);
            rule__Dispatch_conjunction__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Dispatch_conjunction__Group_1__1_in_rule__Dispatch_conjunction__Group_1__08241);
            rule__Dispatch_conjunction__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_conjunction__Group_1__0"


    // $ANTLR start "rule__Dispatch_conjunction__Group_1__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3975:1: rule__Dispatch_conjunction__Group_1__0__Impl : ( 'and' ) ;
    public final void rule__Dispatch_conjunction__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3979:1: ( ( 'and' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3980:1: ( 'and' )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3980:1: ( 'and' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3981:1: 'and'
            {
             before(grammarAccess.getDispatch_conjunctionAccess().getAndKeyword_1_0()); 
            match(input,23,FOLLOW_23_in_rule__Dispatch_conjunction__Group_1__0__Impl8269); 
             after(grammarAccess.getDispatch_conjunctionAccess().getAndKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_conjunction__Group_1__0__Impl"


    // $ANTLR start "rule__Dispatch_conjunction__Group_1__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3994:1: rule__Dispatch_conjunction__Group_1__1 : rule__Dispatch_conjunction__Group_1__1__Impl ;
    public final void rule__Dispatch_conjunction__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3998:1: ( rule__Dispatch_conjunction__Group_1__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:3999:2: rule__Dispatch_conjunction__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Dispatch_conjunction__Group_1__1__Impl_in_rule__Dispatch_conjunction__Group_1__18300);
            rule__Dispatch_conjunction__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_conjunction__Group_1__1"


    // $ANTLR start "rule__Dispatch_conjunction__Group_1__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4005:1: rule__Dispatch_conjunction__Group_1__1__Impl : ( ( rule__Dispatch_conjunction__Dcname2Assignment_1_1 ) ) ;
    public final void rule__Dispatch_conjunction__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4009:1: ( ( ( rule__Dispatch_conjunction__Dcname2Assignment_1_1 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4010:1: ( ( rule__Dispatch_conjunction__Dcname2Assignment_1_1 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4010:1: ( ( rule__Dispatch_conjunction__Dcname2Assignment_1_1 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4011:1: ( rule__Dispatch_conjunction__Dcname2Assignment_1_1 )
            {
             before(grammarAccess.getDispatch_conjunctionAccess().getDcname2Assignment_1_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4012:1: ( rule__Dispatch_conjunction__Dcname2Assignment_1_1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4012:2: rule__Dispatch_conjunction__Dcname2Assignment_1_1
            {
            pushFollow(FOLLOW_rule__Dispatch_conjunction__Dcname2Assignment_1_1_in_rule__Dispatch_conjunction__Group_1__1__Impl8327);
            rule__Dispatch_conjunction__Dcname2Assignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getDispatch_conjunctionAccess().getDcname2Assignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_conjunction__Group_1__1__Impl"


    // $ANTLR start "rule__Completion_relative_timeout_condition_and_catch__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4026:1: rule__Completion_relative_timeout_condition_and_catch__Group__0 : rule__Completion_relative_timeout_condition_and_catch__Group__0__Impl rule__Completion_relative_timeout_condition_and_catch__Group__1 ;
    public final void rule__Completion_relative_timeout_condition_and_catch__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4030:1: ( rule__Completion_relative_timeout_condition_and_catch__Group__0__Impl rule__Completion_relative_timeout_condition_and_catch__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4031:2: rule__Completion_relative_timeout_condition_and_catch__Group__0__Impl rule__Completion_relative_timeout_condition_and_catch__Group__1
            {
            pushFollow(FOLLOW_rule__Completion_relative_timeout_condition_and_catch__Group__0__Impl_in_rule__Completion_relative_timeout_condition_and_catch__Group__08361);
            rule__Completion_relative_timeout_condition_and_catch__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Completion_relative_timeout_condition_and_catch__Group__1_in_rule__Completion_relative_timeout_condition_and_catch__Group__08364);
            rule__Completion_relative_timeout_condition_and_catch__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Completion_relative_timeout_condition_and_catch__Group__0"


    // $ANTLR start "rule__Completion_relative_timeout_condition_and_catch__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4038:1: rule__Completion_relative_timeout_condition_and_catch__Group__0__Impl : ( ( rule__Completion_relative_timeout_condition_and_catch__Cdtcacname1Assignment_0 ) ) ;
    public final void rule__Completion_relative_timeout_condition_and_catch__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4042:1: ( ( ( rule__Completion_relative_timeout_condition_and_catch__Cdtcacname1Assignment_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4043:1: ( ( rule__Completion_relative_timeout_condition_and_catch__Cdtcacname1Assignment_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4043:1: ( ( rule__Completion_relative_timeout_condition_and_catch__Cdtcacname1Assignment_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4044:1: ( rule__Completion_relative_timeout_condition_and_catch__Cdtcacname1Assignment_0 )
            {
             before(grammarAccess.getCompletion_relative_timeout_condition_and_catchAccess().getCdtcacname1Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4045:1: ( rule__Completion_relative_timeout_condition_and_catch__Cdtcacname1Assignment_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4045:2: rule__Completion_relative_timeout_condition_and_catch__Cdtcacname1Assignment_0
            {
            pushFollow(FOLLOW_rule__Completion_relative_timeout_condition_and_catch__Cdtcacname1Assignment_0_in_rule__Completion_relative_timeout_condition_and_catch__Group__0__Impl8391);
            rule__Completion_relative_timeout_condition_and_catch__Cdtcacname1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getCompletion_relative_timeout_condition_and_catchAccess().getCdtcacname1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Completion_relative_timeout_condition_and_catch__Group__0__Impl"


    // $ANTLR start "rule__Completion_relative_timeout_condition_and_catch__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4055:1: rule__Completion_relative_timeout_condition_and_catch__Group__1 : rule__Completion_relative_timeout_condition_and_catch__Group__1__Impl ;
    public final void rule__Completion_relative_timeout_condition_and_catch__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4059:1: ( rule__Completion_relative_timeout_condition_and_catch__Group__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4060:2: rule__Completion_relative_timeout_condition_and_catch__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Completion_relative_timeout_condition_and_catch__Group__1__Impl_in_rule__Completion_relative_timeout_condition_and_catch__Group__18421);
            rule__Completion_relative_timeout_condition_and_catch__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Completion_relative_timeout_condition_and_catch__Group__1"


    // $ANTLR start "rule__Completion_relative_timeout_condition_and_catch__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4066:1: rule__Completion_relative_timeout_condition_and_catch__Group__1__Impl : ( ( rule__Completion_relative_timeout_condition_and_catch__Cdtcacname2Assignment_1 )? ) ;
    public final void rule__Completion_relative_timeout_condition_and_catch__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4070:1: ( ( ( rule__Completion_relative_timeout_condition_and_catch__Cdtcacname2Assignment_1 )? ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4071:1: ( ( rule__Completion_relative_timeout_condition_and_catch__Cdtcacname2Assignment_1 )? )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4071:1: ( ( rule__Completion_relative_timeout_condition_and_catch__Cdtcacname2Assignment_1 )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4072:1: ( rule__Completion_relative_timeout_condition_and_catch__Cdtcacname2Assignment_1 )?
            {
             before(grammarAccess.getCompletion_relative_timeout_condition_and_catchAccess().getCdtcacname2Assignment_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4073:1: ( rule__Completion_relative_timeout_condition_and_catch__Cdtcacname2Assignment_1 )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==RULE_DIGIT||LA40_0==RULE_INT) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4073:2: rule__Completion_relative_timeout_condition_and_catch__Cdtcacname2Assignment_1
                    {
                    pushFollow(FOLLOW_rule__Completion_relative_timeout_condition_and_catch__Cdtcacname2Assignment_1_in_rule__Completion_relative_timeout_condition_and_catch__Group__1__Impl8448);
                    rule__Completion_relative_timeout_condition_and_catch__Cdtcacname2Assignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCompletion_relative_timeout_condition_and_catchAccess().getCdtcacname2Assignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Completion_relative_timeout_condition_and_catch__Group__1__Impl"


    // $ANTLR start "rule__Frozen_ports__Group__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4087:1: rule__Frozen_ports__Group__0 : rule__Frozen_ports__Group__0__Impl rule__Frozen_ports__Group__1 ;
    public final void rule__Frozen_ports__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4091:1: ( rule__Frozen_ports__Group__0__Impl rule__Frozen_ports__Group__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4092:2: rule__Frozen_ports__Group__0__Impl rule__Frozen_ports__Group__1
            {
            pushFollow(FOLLOW_rule__Frozen_ports__Group__0__Impl_in_rule__Frozen_ports__Group__08483);
            rule__Frozen_ports__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Frozen_ports__Group__1_in_rule__Frozen_ports__Group__08486);
            rule__Frozen_ports__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Frozen_ports__Group__0"


    // $ANTLR start "rule__Frozen_ports__Group__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4099:1: rule__Frozen_ports__Group__0__Impl : ( ( rule__Frozen_ports__Fpname1Assignment_0 ) ) ;
    public final void rule__Frozen_ports__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4103:1: ( ( ( rule__Frozen_ports__Fpname1Assignment_0 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4104:1: ( ( rule__Frozen_ports__Fpname1Assignment_0 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4104:1: ( ( rule__Frozen_ports__Fpname1Assignment_0 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4105:1: ( rule__Frozen_ports__Fpname1Assignment_0 )
            {
             before(grammarAccess.getFrozen_portsAccess().getFpname1Assignment_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4106:1: ( rule__Frozen_ports__Fpname1Assignment_0 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4106:2: rule__Frozen_ports__Fpname1Assignment_0
            {
            pushFollow(FOLLOW_rule__Frozen_ports__Fpname1Assignment_0_in_rule__Frozen_ports__Group__0__Impl8513);
            rule__Frozen_ports__Fpname1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getFrozen_portsAccess().getFpname1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Frozen_ports__Group__0__Impl"


    // $ANTLR start "rule__Frozen_ports__Group__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4116:1: rule__Frozen_ports__Group__1 : rule__Frozen_ports__Group__1__Impl ;
    public final void rule__Frozen_ports__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4120:1: ( rule__Frozen_ports__Group__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4121:2: rule__Frozen_ports__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Frozen_ports__Group__1__Impl_in_rule__Frozen_ports__Group__18543);
            rule__Frozen_ports__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Frozen_ports__Group__1"


    // $ANTLR start "rule__Frozen_ports__Group__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4127:1: rule__Frozen_ports__Group__1__Impl : ( ( rule__Frozen_ports__Group_1__0 )* ) ;
    public final void rule__Frozen_ports__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4131:1: ( ( ( rule__Frozen_ports__Group_1__0 )* ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4132:1: ( ( rule__Frozen_ports__Group_1__0 )* )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4132:1: ( ( rule__Frozen_ports__Group_1__0 )* )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4133:1: ( rule__Frozen_ports__Group_1__0 )*
            {
             before(grammarAccess.getFrozen_portsAccess().getGroup_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4134:1: ( rule__Frozen_ports__Group_1__0 )*
            loop41:
            do {
                int alt41=2;
                int LA41_0 = input.LA(1);

                if ( (LA41_0==24) ) {
                    alt41=1;
                }


                switch (alt41) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4134:2: rule__Frozen_ports__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__Frozen_ports__Group_1__0_in_rule__Frozen_ports__Group__1__Impl8570);
            	    rule__Frozen_ports__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop41;
                }
            } while (true);

             after(grammarAccess.getFrozen_portsAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Frozen_ports__Group__1__Impl"


    // $ANTLR start "rule__Frozen_ports__Group_1__0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4148:1: rule__Frozen_ports__Group_1__0 : rule__Frozen_ports__Group_1__0__Impl rule__Frozen_ports__Group_1__1 ;
    public final void rule__Frozen_ports__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4152:1: ( rule__Frozen_ports__Group_1__0__Impl rule__Frozen_ports__Group_1__1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4153:2: rule__Frozen_ports__Group_1__0__Impl rule__Frozen_ports__Group_1__1
            {
            pushFollow(FOLLOW_rule__Frozen_ports__Group_1__0__Impl_in_rule__Frozen_ports__Group_1__08605);
            rule__Frozen_ports__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Frozen_ports__Group_1__1_in_rule__Frozen_ports__Group_1__08608);
            rule__Frozen_ports__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Frozen_ports__Group_1__0"


    // $ANTLR start "rule__Frozen_ports__Group_1__0__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4160:1: rule__Frozen_ports__Group_1__0__Impl : ( ',' ) ;
    public final void rule__Frozen_ports__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4164:1: ( ( ',' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4165:1: ( ',' )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4165:1: ( ',' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4166:1: ','
            {
             before(grammarAccess.getFrozen_portsAccess().getCommaKeyword_1_0()); 
            match(input,24,FOLLOW_24_in_rule__Frozen_ports__Group_1__0__Impl8636); 
             after(grammarAccess.getFrozen_portsAccess().getCommaKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Frozen_ports__Group_1__0__Impl"


    // $ANTLR start "rule__Frozen_ports__Group_1__1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4179:1: rule__Frozen_ports__Group_1__1 : rule__Frozen_ports__Group_1__1__Impl ;
    public final void rule__Frozen_ports__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4183:1: ( rule__Frozen_ports__Group_1__1__Impl )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4184:2: rule__Frozen_ports__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Frozen_ports__Group_1__1__Impl_in_rule__Frozen_ports__Group_1__18667);
            rule__Frozen_ports__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Frozen_ports__Group_1__1"


    // $ANTLR start "rule__Frozen_ports__Group_1__1__Impl"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4190:1: rule__Frozen_ports__Group_1__1__Impl : ( ( rule__Frozen_ports__Fpname2Assignment_1_1 ) ) ;
    public final void rule__Frozen_ports__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4194:1: ( ( ( rule__Frozen_ports__Fpname2Assignment_1_1 ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4195:1: ( ( rule__Frozen_ports__Fpname2Assignment_1_1 ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4195:1: ( ( rule__Frozen_ports__Fpname2Assignment_1_1 ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4196:1: ( rule__Frozen_ports__Fpname2Assignment_1_1 )
            {
             before(grammarAccess.getFrozen_portsAccess().getFpname2Assignment_1_1()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4197:1: ( rule__Frozen_ports__Fpname2Assignment_1_1 )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4197:2: rule__Frozen_ports__Fpname2Assignment_1_1
            {
            pushFollow(FOLLOW_rule__Frozen_ports__Fpname2Assignment_1_1_in_rule__Frozen_ports__Group_1__1__Impl8694);
            rule__Frozen_ports__Fpname2Assignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getFrozen_portsAccess().getFpname2Assignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Frozen_ports__Group_1__1__Impl"


    // $ANTLR start "rule__Guard__DispatchAssignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4212:1: rule__Guard__DispatchAssignment_0 : ( ruledispatch_condition ) ;
    public final void rule__Guard__DispatchAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4216:1: ( ( ruledispatch_condition ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4217:1: ( ruledispatch_condition )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4217:1: ( ruledispatch_condition )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4218:1: ruledispatch_condition
            {
             before(grammarAccess.getGuardAccess().getDispatchDispatch_conditionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruledispatch_condition_in_rule__Guard__DispatchAssignment_08733);
            ruledispatch_condition();

            state._fsp--;

             after(grammarAccess.getGuardAccess().getDispatchDispatch_conditionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guard__DispatchAssignment_0"


    // $ANTLR start "rule__Guard__ExecuteAssignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4227:1: rule__Guard__ExecuteAssignment_1 : ( ruleexecute_condition ) ;
    public final void rule__Guard__ExecuteAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4231:1: ( ( ruleexecute_condition ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4232:1: ( ruleexecute_condition )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4232:1: ( ruleexecute_condition )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4233:1: ruleexecute_condition
            {
             before(grammarAccess.getGuardAccess().getExecuteExecute_conditionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleexecute_condition_in_rule__Guard__ExecuteAssignment_18764);
            ruleexecute_condition();

            state._fsp--;

             after(grammarAccess.getGuardAccess().getExecuteExecute_conditionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guard__ExecuteAssignment_1"


    // $ANTLR start "rule__Execute_condition__Ecname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4242:1: rule__Execute_condition__Ecname1Assignment_0 : ( ( 'otherwise' ) ) ;
    public final void rule__Execute_condition__Ecname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4246:1: ( ( ( 'otherwise' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4247:1: ( ( 'otherwise' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4247:1: ( ( 'otherwise' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4248:1: ( 'otherwise' )
            {
             before(grammarAccess.getExecute_conditionAccess().getEcname1OtherwiseKeyword_0_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4249:1: ( 'otherwise' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4250:1: 'otherwise'
            {
             before(grammarAccess.getExecute_conditionAccess().getEcname1OtherwiseKeyword_0_0()); 
            match(input,25,FOLLOW_25_in_rule__Execute_condition__Ecname1Assignment_08800); 
             after(grammarAccess.getExecute_conditionAccess().getEcname1OtherwiseKeyword_0_0()); 

            }

             after(grammarAccess.getExecute_conditionAccess().getEcname1OtherwiseKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Execute_condition__Ecname1Assignment_0"


    // $ANTLR start "rule__Execute_condition__Ecname2Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4265:1: rule__Execute_condition__Ecname2Assignment_1 : ( rulebehavior_action_block_timeout_catch ) ;
    public final void rule__Execute_condition__Ecname2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4269:1: ( ( rulebehavior_action_block_timeout_catch ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4270:1: ( rulebehavior_action_block_timeout_catch )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4270:1: ( rulebehavior_action_block_timeout_catch )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4271:1: rulebehavior_action_block_timeout_catch
            {
             before(grammarAccess.getExecute_conditionAccess().getEcname2Behavior_action_block_timeout_catchParserRuleCall_1_0()); 
            pushFollow(FOLLOW_rulebehavior_action_block_timeout_catch_in_rule__Execute_condition__Ecname2Assignment_18839);
            rulebehavior_action_block_timeout_catch();

            state._fsp--;

             after(grammarAccess.getExecute_conditionAccess().getEcname2Behavior_action_block_timeout_catchParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Execute_condition__Ecname2Assignment_1"


    // $ANTLR start "rule__Execute_condition__Ecname3Assignment_2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4280:1: rule__Execute_condition__Ecname3Assignment_2 : ( rulevalue_expression ) ;
    public final void rule__Execute_condition__Ecname3Assignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4284:1: ( ( rulevalue_expression ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4285:1: ( rulevalue_expression )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4285:1: ( rulevalue_expression )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4286:1: rulevalue_expression
            {
             before(grammarAccess.getExecute_conditionAccess().getEcname3Value_expressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_rulevalue_expression_in_rule__Execute_condition__Ecname3Assignment_28870);
            rulevalue_expression();

            state._fsp--;

             after(grammarAccess.getExecute_conditionAccess().getEcname3Value_expressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Execute_condition__Ecname3Assignment_2"


    // $ANTLR start "rule__Behavior_action_block_timeout_catch__BabnameAssignment"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4295:1: rule__Behavior_action_block_timeout_catch__BabnameAssignment : ( ( 'timeout' ) ) ;
    public final void rule__Behavior_action_block_timeout_catch__BabnameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4299:1: ( ( ( 'timeout' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4300:1: ( ( 'timeout' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4300:1: ( ( 'timeout' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4301:1: ( 'timeout' )
            {
             before(grammarAccess.getBehavior_action_block_timeout_catchAccess().getBabnameTimeoutKeyword_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4302:1: ( 'timeout' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4303:1: 'timeout'
            {
             before(grammarAccess.getBehavior_action_block_timeout_catchAccess().getBabnameTimeoutKeyword_0()); 
            match(input,26,FOLLOW_26_in_rule__Behavior_action_block_timeout_catch__BabnameAssignment8906); 
             after(grammarAccess.getBehavior_action_block_timeout_catchAccess().getBabnameTimeoutKeyword_0()); 

            }

             after(grammarAccess.getBehavior_action_block_timeout_catchAccess().getBabnameTimeoutKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Behavior_action_block_timeout_catch__BabnameAssignment"


    // $ANTLR start "rule__Value_expression__Vename1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4318:1: rule__Value_expression__Vename1Assignment_0 : ( rulerelation ) ;
    public final void rule__Value_expression__Vename1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4322:1: ( ( rulerelation ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4323:1: ( rulerelation )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4323:1: ( rulerelation )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4324:1: rulerelation
            {
             before(grammarAccess.getValue_expressionAccess().getVename1RelationParserRuleCall_0_0()); 
            pushFollow(FOLLOW_rulerelation_in_rule__Value_expression__Vename1Assignment_08945);
            rulerelation();

            state._fsp--;

             after(grammarAccess.getValue_expressionAccess().getVename1RelationParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_expression__Vename1Assignment_0"


    // $ANTLR start "rule__Value_expression__Vename3Assignment_1_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4333:1: rule__Value_expression__Vename3Assignment_1_0 : ( rulelogical_operator ) ;
    public final void rule__Value_expression__Vename3Assignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4337:1: ( ( rulelogical_operator ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4338:1: ( rulelogical_operator )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4338:1: ( rulelogical_operator )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4339:1: rulelogical_operator
            {
             before(grammarAccess.getValue_expressionAccess().getVename3Logical_operatorParserRuleCall_1_0_0()); 
            pushFollow(FOLLOW_rulelogical_operator_in_rule__Value_expression__Vename3Assignment_1_08976);
            rulelogical_operator();

            state._fsp--;

             after(grammarAccess.getValue_expressionAccess().getVename3Logical_operatorParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_expression__Vename3Assignment_1_0"


    // $ANTLR start "rule__Value_expression__Vename2Assignment_1_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4348:1: rule__Value_expression__Vename2Assignment_1_1 : ( rulerelation ) ;
    public final void rule__Value_expression__Vename2Assignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4352:1: ( ( rulerelation ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4353:1: ( rulerelation )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4353:1: ( rulerelation )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4354:1: rulerelation
            {
             before(grammarAccess.getValue_expressionAccess().getVename2RelationParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_rulerelation_in_rule__Value_expression__Vename2Assignment_1_19007);
            rulerelation();

            state._fsp--;

             after(grammarAccess.getValue_expressionAccess().getVename2RelationParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_expression__Vename2Assignment_1_1"


    // $ANTLR start "rule__Relation__Rname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4363:1: rule__Relation__Rname1Assignment_0 : ( rulesimple_expression ) ;
    public final void rule__Relation__Rname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4367:1: ( ( rulesimple_expression ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4368:1: ( rulesimple_expression )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4368:1: ( rulesimple_expression )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4369:1: rulesimple_expression
            {
             before(grammarAccess.getRelationAccess().getRname1Simple_expressionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_rulesimple_expression_in_rule__Relation__Rname1Assignment_09038);
            rulesimple_expression();

            state._fsp--;

             after(grammarAccess.getRelationAccess().getRname1Simple_expressionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Rname1Assignment_0"


    // $ANTLR start "rule__Relation__Rname3Assignment_1_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4378:1: rule__Relation__Rname3Assignment_1_0 : ( rulerelational_operator ) ;
    public final void rule__Relation__Rname3Assignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4382:1: ( ( rulerelational_operator ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4383:1: ( rulerelational_operator )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4383:1: ( rulerelational_operator )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4384:1: rulerelational_operator
            {
             before(grammarAccess.getRelationAccess().getRname3Relational_operatorParserRuleCall_1_0_0()); 
            pushFollow(FOLLOW_rulerelational_operator_in_rule__Relation__Rname3Assignment_1_09069);
            rulerelational_operator();

            state._fsp--;

             after(grammarAccess.getRelationAccess().getRname3Relational_operatorParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Rname3Assignment_1_0"


    // $ANTLR start "rule__Relation__Rname2Assignment_1_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4393:1: rule__Relation__Rname2Assignment_1_1 : ( rulesimple_expression ) ;
    public final void rule__Relation__Rname2Assignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4397:1: ( ( rulesimple_expression ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4398:1: ( rulesimple_expression )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4398:1: ( rulesimple_expression )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4399:1: rulesimple_expression
            {
             before(grammarAccess.getRelationAccess().getRname2Simple_expressionParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_rulesimple_expression_in_rule__Relation__Rname2Assignment_1_19100);
            rulesimple_expression();

            state._fsp--;

             after(grammarAccess.getRelationAccess().getRname2Simple_expressionParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Rname2Assignment_1_1"


    // $ANTLR start "rule__Simple_expression__Sename2Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4408:1: rule__Simple_expression__Sename2Assignment_0 : ( ruleunary_adding_operator ) ;
    public final void rule__Simple_expression__Sename2Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4412:1: ( ( ruleunary_adding_operator ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4413:1: ( ruleunary_adding_operator )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4413:1: ( ruleunary_adding_operator )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4414:1: ruleunary_adding_operator
            {
             before(grammarAccess.getSimple_expressionAccess().getSename2Unary_adding_operatorParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleunary_adding_operator_in_rule__Simple_expression__Sename2Assignment_09131);
            ruleunary_adding_operator();

            state._fsp--;

             after(grammarAccess.getSimple_expressionAccess().getSename2Unary_adding_operatorParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Simple_expression__Sename2Assignment_0"


    // $ANTLR start "rule__Simple_expression__Sename1Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4423:1: rule__Simple_expression__Sename1Assignment_1 : ( ruleterm ) ;
    public final void rule__Simple_expression__Sename1Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4427:1: ( ( ruleterm ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4428:1: ( ruleterm )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4428:1: ( ruleterm )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4429:1: ruleterm
            {
             before(grammarAccess.getSimple_expressionAccess().getSename1TermParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleterm_in_rule__Simple_expression__Sename1Assignment_19162);
            ruleterm();

            state._fsp--;

             after(grammarAccess.getSimple_expressionAccess().getSename1TermParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Simple_expression__Sename1Assignment_1"


    // $ANTLR start "rule__Simple_expression__Sename3Assignment_2_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4438:1: rule__Simple_expression__Sename3Assignment_2_0 : ( rulebinary_adding_operator ) ;
    public final void rule__Simple_expression__Sename3Assignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4442:1: ( ( rulebinary_adding_operator ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4443:1: ( rulebinary_adding_operator )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4443:1: ( rulebinary_adding_operator )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4444:1: rulebinary_adding_operator
            {
             before(grammarAccess.getSimple_expressionAccess().getSename3Binary_adding_operatorParserRuleCall_2_0_0()); 
            pushFollow(FOLLOW_rulebinary_adding_operator_in_rule__Simple_expression__Sename3Assignment_2_09193);
            rulebinary_adding_operator();

            state._fsp--;

             after(grammarAccess.getSimple_expressionAccess().getSename3Binary_adding_operatorParserRuleCall_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Simple_expression__Sename3Assignment_2_0"


    // $ANTLR start "rule__Simple_expression__Sename4Assignment_2_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4453:1: rule__Simple_expression__Sename4Assignment_2_1 : ( ruleterm ) ;
    public final void rule__Simple_expression__Sename4Assignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4457:1: ( ( ruleterm ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4458:1: ( ruleterm )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4458:1: ( ruleterm )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4459:1: ruleterm
            {
             before(grammarAccess.getSimple_expressionAccess().getSename4TermParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_ruleterm_in_rule__Simple_expression__Sename4Assignment_2_19224);
            ruleterm();

            state._fsp--;

             after(grammarAccess.getSimple_expressionAccess().getSename4TermParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Simple_expression__Sename4Assignment_2_1"


    // $ANTLR start "rule__Term__Tname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4468:1: rule__Term__Tname1Assignment_0 : ( rulefactor ) ;
    public final void rule__Term__Tname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4472:1: ( ( rulefactor ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4473:1: ( rulefactor )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4473:1: ( rulefactor )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4474:1: rulefactor
            {
             before(grammarAccess.getTermAccess().getTname1FactorParserRuleCall_0_0()); 
            pushFollow(FOLLOW_rulefactor_in_rule__Term__Tname1Assignment_09255);
            rulefactor();

            state._fsp--;

             after(grammarAccess.getTermAccess().getTname1FactorParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Tname1Assignment_0"


    // $ANTLR start "rule__Term__Tname3Assignment_1_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4483:1: rule__Term__Tname3Assignment_1_0 : ( rulemultiplying_operator ) ;
    public final void rule__Term__Tname3Assignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4487:1: ( ( rulemultiplying_operator ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4488:1: ( rulemultiplying_operator )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4488:1: ( rulemultiplying_operator )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4489:1: rulemultiplying_operator
            {
             before(grammarAccess.getTermAccess().getTname3Multiplying_operatorParserRuleCall_1_0_0()); 
            pushFollow(FOLLOW_rulemultiplying_operator_in_rule__Term__Tname3Assignment_1_09286);
            rulemultiplying_operator();

            state._fsp--;

             after(grammarAccess.getTermAccess().getTname3Multiplying_operatorParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Tname3Assignment_1_0"


    // $ANTLR start "rule__Term__Tname2Assignment_1_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4498:1: rule__Term__Tname2Assignment_1_1 : ( rulefactor ) ;
    public final void rule__Term__Tname2Assignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4502:1: ( ( rulefactor ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4503:1: ( rulefactor )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4503:1: ( rulefactor )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4504:1: rulefactor
            {
             before(grammarAccess.getTermAccess().getTname2FactorParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_rulefactor_in_rule__Term__Tname2Assignment_1_19317);
            rulefactor();

            state._fsp--;

             after(grammarAccess.getTermAccess().getTname2FactorParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Tname2Assignment_1_1"


    // $ANTLR start "rule__Factor__Fname1Assignment_0_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4513:1: rule__Factor__Fname1Assignment_0_0 : ( rulevalue ) ;
    public final void rule__Factor__Fname1Assignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4517:1: ( ( rulevalue ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4518:1: ( rulevalue )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4518:1: ( rulevalue )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4519:1: rulevalue
            {
             before(grammarAccess.getFactorAccess().getFname1ValueParserRuleCall_0_0_0()); 
            pushFollow(FOLLOW_rulevalue_in_rule__Factor__Fname1Assignment_0_09348);
            rulevalue();

            state._fsp--;

             after(grammarAccess.getFactorAccess().getFname1ValueParserRuleCall_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Fname1Assignment_0_0"


    // $ANTLR start "rule__Factor__Fname3Assignment_0_1_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4528:1: rule__Factor__Fname3Assignment_0_1_0 : ( rulebinary_numeric_operator ) ;
    public final void rule__Factor__Fname3Assignment_0_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4532:1: ( ( rulebinary_numeric_operator ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4533:1: ( rulebinary_numeric_operator )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4533:1: ( rulebinary_numeric_operator )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4534:1: rulebinary_numeric_operator
            {
             before(grammarAccess.getFactorAccess().getFname3Binary_numeric_operatorParserRuleCall_0_1_0_0()); 
            pushFollow(FOLLOW_rulebinary_numeric_operator_in_rule__Factor__Fname3Assignment_0_1_09379);
            rulebinary_numeric_operator();

            state._fsp--;

             after(grammarAccess.getFactorAccess().getFname3Binary_numeric_operatorParserRuleCall_0_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Fname3Assignment_0_1_0"


    // $ANTLR start "rule__Factor__Fname2Assignment_0_1_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4543:1: rule__Factor__Fname2Assignment_0_1_1 : ( rulevalue ) ;
    public final void rule__Factor__Fname2Assignment_0_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4547:1: ( ( rulevalue ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4548:1: ( rulevalue )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4548:1: ( rulevalue )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4549:1: rulevalue
            {
             before(grammarAccess.getFactorAccess().getFname2ValueParserRuleCall_0_1_1_0()); 
            pushFollow(FOLLOW_rulevalue_in_rule__Factor__Fname2Assignment_0_1_19410);
            rulevalue();

            state._fsp--;

             after(grammarAccess.getFactorAccess().getFname2ValueParserRuleCall_0_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Fname2Assignment_0_1_1"


    // $ANTLR start "rule__Factor__Fname4Assignment_1_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4558:1: rule__Factor__Fname4Assignment_1_0 : ( ruleunary_numeric_operator ) ;
    public final void rule__Factor__Fname4Assignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4562:1: ( ( ruleunary_numeric_operator ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4563:1: ( ruleunary_numeric_operator )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4563:1: ( ruleunary_numeric_operator )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4564:1: ruleunary_numeric_operator
            {
             before(grammarAccess.getFactorAccess().getFname4Unary_numeric_operatorParserRuleCall_1_0_0()); 
            pushFollow(FOLLOW_ruleunary_numeric_operator_in_rule__Factor__Fname4Assignment_1_09441);
            ruleunary_numeric_operator();

            state._fsp--;

             after(grammarAccess.getFactorAccess().getFname4Unary_numeric_operatorParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Fname4Assignment_1_0"


    // $ANTLR start "rule__Factor__Fname5Assignment_1_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4573:1: rule__Factor__Fname5Assignment_1_1 : ( rulevalue ) ;
    public final void rule__Factor__Fname5Assignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4577:1: ( ( rulevalue ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4578:1: ( rulevalue )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4578:1: ( rulevalue )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4579:1: rulevalue
            {
             before(grammarAccess.getFactorAccess().getFname5ValueParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_rulevalue_in_rule__Factor__Fname5Assignment_1_19472);
            rulevalue();

            state._fsp--;

             after(grammarAccess.getFactorAccess().getFname5ValueParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Fname5Assignment_1_1"


    // $ANTLR start "rule__Factor__Fname6Assignment_2_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4588:1: rule__Factor__Fname6Assignment_2_0 : ( ruleunary_boolean_operator ) ;
    public final void rule__Factor__Fname6Assignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4592:1: ( ( ruleunary_boolean_operator ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4593:1: ( ruleunary_boolean_operator )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4593:1: ( ruleunary_boolean_operator )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4594:1: ruleunary_boolean_operator
            {
             before(grammarAccess.getFactorAccess().getFname6Unary_boolean_operatorParserRuleCall_2_0_0()); 
            pushFollow(FOLLOW_ruleunary_boolean_operator_in_rule__Factor__Fname6Assignment_2_09503);
            ruleunary_boolean_operator();

            state._fsp--;

             after(grammarAccess.getFactorAccess().getFname6Unary_boolean_operatorParserRuleCall_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Fname6Assignment_2_0"


    // $ANTLR start "rule__Factor__Fname7Assignment_2_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4603:1: rule__Factor__Fname7Assignment_2_1 : ( rulevalue ) ;
    public final void rule__Factor__Fname7Assignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4607:1: ( ( rulevalue ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4608:1: ( rulevalue )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4608:1: ( rulevalue )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4609:1: rulevalue
            {
             before(grammarAccess.getFactorAccess().getFname7ValueParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_rulevalue_in_rule__Factor__Fname7Assignment_2_19534);
            rulevalue();

            state._fsp--;

             after(grammarAccess.getFactorAccess().getFname7ValueParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Fname7Assignment_2_1"


    // $ANTLR start "rule__Value__Vname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4618:1: rule__Value__Vname1Assignment_0 : ( rulevalue_variable ) ;
    public final void rule__Value__Vname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4622:1: ( ( rulevalue_variable ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4623:1: ( rulevalue_variable )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4623:1: ( rulevalue_variable )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4624:1: rulevalue_variable
            {
             before(grammarAccess.getValueAccess().getVname1Value_variableParserRuleCall_0_0()); 
            pushFollow(FOLLOW_rulevalue_variable_in_rule__Value__Vname1Assignment_09565);
            rulevalue_variable();

            state._fsp--;

             after(grammarAccess.getValueAccess().getVname1Value_variableParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Vname1Assignment_0"


    // $ANTLR start "rule__Value__Vname2Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4633:1: rule__Value__Vname2Assignment_1 : ( rulevalue_constant ) ;
    public final void rule__Value__Vname2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4637:1: ( ( rulevalue_constant ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4638:1: ( rulevalue_constant )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4638:1: ( rulevalue_constant )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4639:1: rulevalue_constant
            {
             before(grammarAccess.getValueAccess().getVname2Value_constantParserRuleCall_1_0()); 
            pushFollow(FOLLOW_rulevalue_constant_in_rule__Value__Vname2Assignment_19596);
            rulevalue_constant();

            state._fsp--;

             after(grammarAccess.getValueAccess().getVname2Value_constantParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Vname2Assignment_1"


    // $ANTLR start "rule__Value_variable__Vvname0Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4648:1: rule__Value_variable__Vvname0Assignment_0 : ( RULE_STRING_LITERAL ) ;
    public final void rule__Value_variable__Vvname0Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4652:1: ( ( RULE_STRING_LITERAL ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4653:1: ( RULE_STRING_LITERAL )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4653:1: ( RULE_STRING_LITERAL )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4654:1: RULE_STRING_LITERAL
            {
             before(grammarAccess.getValue_variableAccess().getVvname0String_literalTerminalRuleCall_0_0()); 
            match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rule__Value_variable__Vvname0Assignment_09627); 
             after(grammarAccess.getValue_variableAccess().getVvname0String_literalTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_variable__Vvname0Assignment_0"


    // $ANTLR start "rule__Value_variable__Vvname1Assignment_1_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4663:1: rule__Value_variable__Vvname1Assignment_1_0 : ( ( '?' ) ) ;
    public final void rule__Value_variable__Vvname1Assignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4667:1: ( ( ( '?' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4668:1: ( ( '?' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4668:1: ( ( '?' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4669:1: ( '?' )
            {
             before(grammarAccess.getValue_variableAccess().getVvname1QuestionMarkKeyword_1_0_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4670:1: ( '?' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4671:1: '?'
            {
             before(grammarAccess.getValue_variableAccess().getVvname1QuestionMarkKeyword_1_0_0()); 
            match(input,27,FOLLOW_27_in_rule__Value_variable__Vvname1Assignment_1_09663); 
             after(grammarAccess.getValue_variableAccess().getVvname1QuestionMarkKeyword_1_0_0()); 

            }

             after(grammarAccess.getValue_variableAccess().getVvname1QuestionMarkKeyword_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_variable__Vvname1Assignment_1_0"


    // $ANTLR start "rule__Value_variable__Vvname2Assignment_1_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4686:1: rule__Value_variable__Vvname2Assignment_1_1 : ( ( 'count' ) ) ;
    public final void rule__Value_variable__Vvname2Assignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4690:1: ( ( ( 'count' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4691:1: ( ( 'count' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4691:1: ( ( 'count' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4692:1: ( 'count' )
            {
             before(grammarAccess.getValue_variableAccess().getVvname2CountKeyword_1_1_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4693:1: ( 'count' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4694:1: 'count'
            {
             before(grammarAccess.getValue_variableAccess().getVvname2CountKeyword_1_1_0()); 
            match(input,28,FOLLOW_28_in_rule__Value_variable__Vvname2Assignment_1_19707); 
             after(grammarAccess.getValue_variableAccess().getVvname2CountKeyword_1_1_0()); 

            }

             after(grammarAccess.getValue_variableAccess().getVvname2CountKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_variable__Vvname2Assignment_1_1"


    // $ANTLR start "rule__Value_variable__Vvname3Assignment_1_2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4709:1: rule__Value_variable__Vvname3Assignment_1_2 : ( ( 'fresh' ) ) ;
    public final void rule__Value_variable__Vvname3Assignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4713:1: ( ( ( 'fresh' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4714:1: ( ( 'fresh' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4714:1: ( ( 'fresh' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4715:1: ( 'fresh' )
            {
             before(grammarAccess.getValue_variableAccess().getVvname3FreshKeyword_1_2_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4716:1: ( 'fresh' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4717:1: 'fresh'
            {
             before(grammarAccess.getValue_variableAccess().getVvname3FreshKeyword_1_2_0()); 
            match(input,29,FOLLOW_29_in_rule__Value_variable__Vvname3Assignment_1_29751); 
             after(grammarAccess.getValue_variableAccess().getVvname3FreshKeyword_1_2_0()); 

            }

             after(grammarAccess.getValue_variableAccess().getVvname3FreshKeyword_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_variable__Vvname3Assignment_1_2"


    // $ANTLR start "rule__Value_constant__Vcname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4732:1: rule__Value_constant__Vcname1Assignment_0 : ( ruleboolean_literal ) ;
    public final void rule__Value_constant__Vcname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4736:1: ( ( ruleboolean_literal ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4737:1: ( ruleboolean_literal )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4737:1: ( ruleboolean_literal )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4738:1: ruleboolean_literal
            {
             before(grammarAccess.getValue_constantAccess().getVcname1Boolean_literalParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleboolean_literal_in_rule__Value_constant__Vcname1Assignment_09790);
            ruleboolean_literal();

            state._fsp--;

             after(grammarAccess.getValue_constantAccess().getVcname1Boolean_literalParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_constant__Vcname1Assignment_0"


    // $ANTLR start "rule__Value_constant__Vcname2Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4747:1: rule__Value_constant__Vcname2Assignment_1 : ( rulenumeric_literal ) ;
    public final void rule__Value_constant__Vcname2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4751:1: ( ( rulenumeric_literal ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4752:1: ( rulenumeric_literal )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4752:1: ( rulenumeric_literal )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4753:1: rulenumeric_literal
            {
             before(grammarAccess.getValue_constantAccess().getVcname2Numeric_literalParserRuleCall_1_0()); 
            pushFollow(FOLLOW_rulenumeric_literal_in_rule__Value_constant__Vcname2Assignment_19821);
            rulenumeric_literal();

            state._fsp--;

             after(grammarAccess.getValue_constantAccess().getVcname2Numeric_literalParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_constant__Vcname2Assignment_1"


    // $ANTLR start "rule__Value_constant__Vcname3Assignment_2_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4762:1: rule__Value_constant__Vcname3Assignment_2_0 : ( RULE_STRING_LITERAL ) ;
    public final void rule__Value_constant__Vcname3Assignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4766:1: ( ( RULE_STRING_LITERAL ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4767:1: ( RULE_STRING_LITERAL )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4767:1: ( RULE_STRING_LITERAL )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4768:1: RULE_STRING_LITERAL
            {
             before(grammarAccess.getValue_constantAccess().getVcname3String_literalTerminalRuleCall_2_0_0()); 
            match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rule__Value_constant__Vcname3Assignment_2_09852); 
             after(grammarAccess.getValue_constantAccess().getVcname3String_literalTerminalRuleCall_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_constant__Vcname3Assignment_2_0"


    // $ANTLR start "rule__Value_constant__Vcname4Assignment_2_2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4777:1: rule__Value_constant__Vcname4Assignment_2_2 : ( RULE_STRING_LITERAL ) ;
    public final void rule__Value_constant__Vcname4Assignment_2_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4781:1: ( ( RULE_STRING_LITERAL ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4782:1: ( RULE_STRING_LITERAL )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4782:1: ( RULE_STRING_LITERAL )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4783:1: RULE_STRING_LITERAL
            {
             before(grammarAccess.getValue_constantAccess().getVcname4String_literalTerminalRuleCall_2_2_0()); 
            match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rule__Value_constant__Vcname4Assignment_2_29883); 
             after(grammarAccess.getValue_constantAccess().getVcname4String_literalTerminalRuleCall_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value_constant__Vcname4Assignment_2_2"


    // $ANTLR start "rule__Logical_operator__Loname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4792:1: rule__Logical_operator__Loname1Assignment_0 : ( ( 'and' ) ) ;
    public final void rule__Logical_operator__Loname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4796:1: ( ( ( 'and' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4797:1: ( ( 'and' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4797:1: ( ( 'and' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4798:1: ( 'and' )
            {
             before(grammarAccess.getLogical_operatorAccess().getLoname1AndKeyword_0_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4799:1: ( 'and' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4800:1: 'and'
            {
             before(grammarAccess.getLogical_operatorAccess().getLoname1AndKeyword_0_0()); 
            match(input,23,FOLLOW_23_in_rule__Logical_operator__Loname1Assignment_09919); 
             after(grammarAccess.getLogical_operatorAccess().getLoname1AndKeyword_0_0()); 

            }

             after(grammarAccess.getLogical_operatorAccess().getLoname1AndKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Logical_operator__Loname1Assignment_0"


    // $ANTLR start "rule__Logical_operator__Loname2Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4815:1: rule__Logical_operator__Loname2Assignment_1 : ( ( 'or' ) ) ;
    public final void rule__Logical_operator__Loname2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4819:1: ( ( ( 'or' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4820:1: ( ( 'or' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4820:1: ( ( 'or' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4821:1: ( 'or' )
            {
             before(grammarAccess.getLogical_operatorAccess().getLoname2OrKeyword_1_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4822:1: ( 'or' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4823:1: 'or'
            {
             before(grammarAccess.getLogical_operatorAccess().getLoname2OrKeyword_1_0()); 
            match(input,22,FOLLOW_22_in_rule__Logical_operator__Loname2Assignment_19963); 
             after(grammarAccess.getLogical_operatorAccess().getLoname2OrKeyword_1_0()); 

            }

             after(grammarAccess.getLogical_operatorAccess().getLoname2OrKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Logical_operator__Loname2Assignment_1"


    // $ANTLR start "rule__Logical_operator__Loname3Assignment_2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4838:1: rule__Logical_operator__Loname3Assignment_2 : ( ( 'xor' ) ) ;
    public final void rule__Logical_operator__Loname3Assignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4842:1: ( ( ( 'xor' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4843:1: ( ( 'xor' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4843:1: ( ( 'xor' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4844:1: ( 'xor' )
            {
             before(grammarAccess.getLogical_operatorAccess().getLoname3XorKeyword_2_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4845:1: ( 'xor' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4846:1: 'xor'
            {
             before(grammarAccess.getLogical_operatorAccess().getLoname3XorKeyword_2_0()); 
            match(input,30,FOLLOW_30_in_rule__Logical_operator__Loname3Assignment_210007); 
             after(grammarAccess.getLogical_operatorAccess().getLoname3XorKeyword_2_0()); 

            }

             after(grammarAccess.getLogical_operatorAccess().getLoname3XorKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Logical_operator__Loname3Assignment_2"


    // $ANTLR start "rule__Relational_operator__Roname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4861:1: rule__Relational_operator__Roname1Assignment_0 : ( ( '=' ) ) ;
    public final void rule__Relational_operator__Roname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4865:1: ( ( ( '=' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4866:1: ( ( '=' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4866:1: ( ( '=' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4867:1: ( '=' )
            {
             before(grammarAccess.getRelational_operatorAccess().getRoname1EqualsSignKeyword_0_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4868:1: ( '=' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4869:1: '='
            {
             before(grammarAccess.getRelational_operatorAccess().getRoname1EqualsSignKeyword_0_0()); 
            match(input,31,FOLLOW_31_in_rule__Relational_operator__Roname1Assignment_010051); 
             after(grammarAccess.getRelational_operatorAccess().getRoname1EqualsSignKeyword_0_0()); 

            }

             after(grammarAccess.getRelational_operatorAccess().getRoname1EqualsSignKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational_operator__Roname1Assignment_0"


    // $ANTLR start "rule__Relational_operator__Roname2Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4884:1: rule__Relational_operator__Roname2Assignment_1 : ( ( '!=' ) ) ;
    public final void rule__Relational_operator__Roname2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4888:1: ( ( ( '!=' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4889:1: ( ( '!=' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4889:1: ( ( '!=' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4890:1: ( '!=' )
            {
             before(grammarAccess.getRelational_operatorAccess().getRoname2ExclamationMarkEqualsSignKeyword_1_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4891:1: ( '!=' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4892:1: '!='
            {
             before(grammarAccess.getRelational_operatorAccess().getRoname2ExclamationMarkEqualsSignKeyword_1_0()); 
            match(input,32,FOLLOW_32_in_rule__Relational_operator__Roname2Assignment_110095); 
             after(grammarAccess.getRelational_operatorAccess().getRoname2ExclamationMarkEqualsSignKeyword_1_0()); 

            }

             after(grammarAccess.getRelational_operatorAccess().getRoname2ExclamationMarkEqualsSignKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational_operator__Roname2Assignment_1"


    // $ANTLR start "rule__Relational_operator__Roname3Assignment_2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4907:1: rule__Relational_operator__Roname3Assignment_2 : ( ( '<' ) ) ;
    public final void rule__Relational_operator__Roname3Assignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4911:1: ( ( ( '<' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4912:1: ( ( '<' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4912:1: ( ( '<' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4913:1: ( '<' )
            {
             before(grammarAccess.getRelational_operatorAccess().getRoname3LessThanSignKeyword_2_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4914:1: ( '<' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4915:1: '<'
            {
             before(grammarAccess.getRelational_operatorAccess().getRoname3LessThanSignKeyword_2_0()); 
            match(input,33,FOLLOW_33_in_rule__Relational_operator__Roname3Assignment_210139); 
             after(grammarAccess.getRelational_operatorAccess().getRoname3LessThanSignKeyword_2_0()); 

            }

             after(grammarAccess.getRelational_operatorAccess().getRoname3LessThanSignKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational_operator__Roname3Assignment_2"


    // $ANTLR start "rule__Relational_operator__Roname4Assignment_3"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4930:1: rule__Relational_operator__Roname4Assignment_3 : ( ( '<=' ) ) ;
    public final void rule__Relational_operator__Roname4Assignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4934:1: ( ( ( '<=' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4935:1: ( ( '<=' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4935:1: ( ( '<=' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4936:1: ( '<=' )
            {
             before(grammarAccess.getRelational_operatorAccess().getRoname4LessThanSignEqualsSignKeyword_3_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4937:1: ( '<=' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4938:1: '<='
            {
             before(grammarAccess.getRelational_operatorAccess().getRoname4LessThanSignEqualsSignKeyword_3_0()); 
            match(input,34,FOLLOW_34_in_rule__Relational_operator__Roname4Assignment_310183); 
             after(grammarAccess.getRelational_operatorAccess().getRoname4LessThanSignEqualsSignKeyword_3_0()); 

            }

             after(grammarAccess.getRelational_operatorAccess().getRoname4LessThanSignEqualsSignKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational_operator__Roname4Assignment_3"


    // $ANTLR start "rule__Relational_operator__Roname5Assignment_4"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4953:1: rule__Relational_operator__Roname5Assignment_4 : ( ( '>' ) ) ;
    public final void rule__Relational_operator__Roname5Assignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4957:1: ( ( ( '>' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4958:1: ( ( '>' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4958:1: ( ( '>' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4959:1: ( '>' )
            {
             before(grammarAccess.getRelational_operatorAccess().getRoname5GreaterThanSignKeyword_4_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4960:1: ( '>' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4961:1: '>'
            {
             before(grammarAccess.getRelational_operatorAccess().getRoname5GreaterThanSignKeyword_4_0()); 
            match(input,35,FOLLOW_35_in_rule__Relational_operator__Roname5Assignment_410227); 
             after(grammarAccess.getRelational_operatorAccess().getRoname5GreaterThanSignKeyword_4_0()); 

            }

             after(grammarAccess.getRelational_operatorAccess().getRoname5GreaterThanSignKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational_operator__Roname5Assignment_4"


    // $ANTLR start "rule__Relational_operator__Roname6Assignment_5"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4976:1: rule__Relational_operator__Roname6Assignment_5 : ( ( '>=' ) ) ;
    public final void rule__Relational_operator__Roname6Assignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4980:1: ( ( ( '>=' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4981:1: ( ( '>=' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4981:1: ( ( '>=' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4982:1: ( '>=' )
            {
             before(grammarAccess.getRelational_operatorAccess().getRoname6GreaterThanSignEqualsSignKeyword_5_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4983:1: ( '>=' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4984:1: '>='
            {
             before(grammarAccess.getRelational_operatorAccess().getRoname6GreaterThanSignEqualsSignKeyword_5_0()); 
            match(input,36,FOLLOW_36_in_rule__Relational_operator__Roname6Assignment_510271); 
             after(grammarAccess.getRelational_operatorAccess().getRoname6GreaterThanSignEqualsSignKeyword_5_0()); 

            }

             after(grammarAccess.getRelational_operatorAccess().getRoname6GreaterThanSignEqualsSignKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relational_operator__Roname6Assignment_5"


    // $ANTLR start "rule__Binary_adding_operator__Baoname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:4999:1: rule__Binary_adding_operator__Baoname1Assignment_0 : ( ( '+' ) ) ;
    public final void rule__Binary_adding_operator__Baoname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5003:1: ( ( ( '+' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5004:1: ( ( '+' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5004:1: ( ( '+' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5005:1: ( '+' )
            {
             before(grammarAccess.getBinary_adding_operatorAccess().getBaoname1PlusSignKeyword_0_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5006:1: ( '+' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5007:1: '+'
            {
             before(grammarAccess.getBinary_adding_operatorAccess().getBaoname1PlusSignKeyword_0_0()); 
            match(input,18,FOLLOW_18_in_rule__Binary_adding_operator__Baoname1Assignment_010315); 
             after(grammarAccess.getBinary_adding_operatorAccess().getBaoname1PlusSignKeyword_0_0()); 

            }

             after(grammarAccess.getBinary_adding_operatorAccess().getBaoname1PlusSignKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary_adding_operator__Baoname1Assignment_0"


    // $ANTLR start "rule__Binary_adding_operator__Baoname2Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5022:1: rule__Binary_adding_operator__Baoname2Assignment_1 : ( ( '-' ) ) ;
    public final void rule__Binary_adding_operator__Baoname2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5026:1: ( ( ( '-' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5027:1: ( ( '-' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5027:1: ( ( '-' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5028:1: ( '-' )
            {
             before(grammarAccess.getBinary_adding_operatorAccess().getBaoname2HyphenMinusKeyword_1_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5029:1: ( '-' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5030:1: '-'
            {
             before(grammarAccess.getBinary_adding_operatorAccess().getBaoname2HyphenMinusKeyword_1_0()); 
            match(input,37,FOLLOW_37_in_rule__Binary_adding_operator__Baoname2Assignment_110359); 
             after(grammarAccess.getBinary_adding_operatorAccess().getBaoname2HyphenMinusKeyword_1_0()); 

            }

             after(grammarAccess.getBinary_adding_operatorAccess().getBaoname2HyphenMinusKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary_adding_operator__Baoname2Assignment_1"


    // $ANTLR start "rule__Unary_adding_operator__Uaoname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5045:1: rule__Unary_adding_operator__Uaoname1Assignment_0 : ( ( '+' ) ) ;
    public final void rule__Unary_adding_operator__Uaoname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5049:1: ( ( ( '+' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5050:1: ( ( '+' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5050:1: ( ( '+' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5051:1: ( '+' )
            {
             before(grammarAccess.getUnary_adding_operatorAccess().getUaoname1PlusSignKeyword_0_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5052:1: ( '+' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5053:1: '+'
            {
             before(grammarAccess.getUnary_adding_operatorAccess().getUaoname1PlusSignKeyword_0_0()); 
            match(input,18,FOLLOW_18_in_rule__Unary_adding_operator__Uaoname1Assignment_010403); 
             after(grammarAccess.getUnary_adding_operatorAccess().getUaoname1PlusSignKeyword_0_0()); 

            }

             after(grammarAccess.getUnary_adding_operatorAccess().getUaoname1PlusSignKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary_adding_operator__Uaoname1Assignment_0"


    // $ANTLR start "rule__Unary_adding_operator__Uaoname2Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5068:1: rule__Unary_adding_operator__Uaoname2Assignment_1 : ( ( '-' ) ) ;
    public final void rule__Unary_adding_operator__Uaoname2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5072:1: ( ( ( '-' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5073:1: ( ( '-' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5073:1: ( ( '-' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5074:1: ( '-' )
            {
             before(grammarAccess.getUnary_adding_operatorAccess().getUaoname2HyphenMinusKeyword_1_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5075:1: ( '-' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5076:1: '-'
            {
             before(grammarAccess.getUnary_adding_operatorAccess().getUaoname2HyphenMinusKeyword_1_0()); 
            match(input,37,FOLLOW_37_in_rule__Unary_adding_operator__Uaoname2Assignment_110447); 
             after(grammarAccess.getUnary_adding_operatorAccess().getUaoname2HyphenMinusKeyword_1_0()); 

            }

             after(grammarAccess.getUnary_adding_operatorAccess().getUaoname2HyphenMinusKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary_adding_operator__Uaoname2Assignment_1"


    // $ANTLR start "rule__Multiplying_operator__Moname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5091:1: rule__Multiplying_operator__Moname1Assignment_0 : ( ( '*' ) ) ;
    public final void rule__Multiplying_operator__Moname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5095:1: ( ( ( '*' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5096:1: ( ( '*' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5096:1: ( ( '*' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5097:1: ( '*' )
            {
             before(grammarAccess.getMultiplying_operatorAccess().getMoname1AsteriskKeyword_0_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5098:1: ( '*' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5099:1: '*'
            {
             before(grammarAccess.getMultiplying_operatorAccess().getMoname1AsteriskKeyword_0_0()); 
            match(input,38,FOLLOW_38_in_rule__Multiplying_operator__Moname1Assignment_010491); 
             after(grammarAccess.getMultiplying_operatorAccess().getMoname1AsteriskKeyword_0_0()); 

            }

             after(grammarAccess.getMultiplying_operatorAccess().getMoname1AsteriskKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplying_operator__Moname1Assignment_0"


    // $ANTLR start "rule__Multiplying_operator__Moname2Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5114:1: rule__Multiplying_operator__Moname2Assignment_1 : ( ( '/' ) ) ;
    public final void rule__Multiplying_operator__Moname2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5118:1: ( ( ( '/' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5119:1: ( ( '/' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5119:1: ( ( '/' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5120:1: ( '/' )
            {
             before(grammarAccess.getMultiplying_operatorAccess().getMoname2SolidusKeyword_1_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5121:1: ( '/' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5122:1: '/'
            {
             before(grammarAccess.getMultiplying_operatorAccess().getMoname2SolidusKeyword_1_0()); 
            match(input,39,FOLLOW_39_in_rule__Multiplying_operator__Moname2Assignment_110535); 
             after(grammarAccess.getMultiplying_operatorAccess().getMoname2SolidusKeyword_1_0()); 

            }

             after(grammarAccess.getMultiplying_operatorAccess().getMoname2SolidusKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplying_operator__Moname2Assignment_1"


    // $ANTLR start "rule__Multiplying_operator__Moname3Assignment_2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5137:1: rule__Multiplying_operator__Moname3Assignment_2 : ( ( 'mod' ) ) ;
    public final void rule__Multiplying_operator__Moname3Assignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5141:1: ( ( ( 'mod' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5142:1: ( ( 'mod' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5142:1: ( ( 'mod' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5143:1: ( 'mod' )
            {
             before(grammarAccess.getMultiplying_operatorAccess().getMoname3ModKeyword_2_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5144:1: ( 'mod' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5145:1: 'mod'
            {
             before(grammarAccess.getMultiplying_operatorAccess().getMoname3ModKeyword_2_0()); 
            match(input,40,FOLLOW_40_in_rule__Multiplying_operator__Moname3Assignment_210579); 
             after(grammarAccess.getMultiplying_operatorAccess().getMoname3ModKeyword_2_0()); 

            }

             after(grammarAccess.getMultiplying_operatorAccess().getMoname3ModKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplying_operator__Moname3Assignment_2"


    // $ANTLR start "rule__Multiplying_operator__Moname4Assignment_3"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5160:1: rule__Multiplying_operator__Moname4Assignment_3 : ( ( 'rem' ) ) ;
    public final void rule__Multiplying_operator__Moname4Assignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5164:1: ( ( ( 'rem' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5165:1: ( ( 'rem' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5165:1: ( ( 'rem' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5166:1: ( 'rem' )
            {
             before(grammarAccess.getMultiplying_operatorAccess().getMoname4RemKeyword_3_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5167:1: ( 'rem' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5168:1: 'rem'
            {
             before(grammarAccess.getMultiplying_operatorAccess().getMoname4RemKeyword_3_0()); 
            match(input,41,FOLLOW_41_in_rule__Multiplying_operator__Moname4Assignment_310623); 
             after(grammarAccess.getMultiplying_operatorAccess().getMoname4RemKeyword_3_0()); 

            }

             after(grammarAccess.getMultiplying_operatorAccess().getMoname4RemKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Multiplying_operator__Moname4Assignment_3"


    // $ANTLR start "rule__Binary_numeric_operator__BnonameAssignment"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5183:1: rule__Binary_numeric_operator__BnonameAssignment : ( ( '**' ) ) ;
    public final void rule__Binary_numeric_operator__BnonameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5187:1: ( ( ( '**' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5188:1: ( ( '**' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5188:1: ( ( '**' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5189:1: ( '**' )
            {
             before(grammarAccess.getBinary_numeric_operatorAccess().getBnonameAsteriskAsteriskKeyword_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5190:1: ( '**' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5191:1: '**'
            {
             before(grammarAccess.getBinary_numeric_operatorAccess().getBnonameAsteriskAsteriskKeyword_0()); 
            match(input,42,FOLLOW_42_in_rule__Binary_numeric_operator__BnonameAssignment10667); 
             after(grammarAccess.getBinary_numeric_operatorAccess().getBnonameAsteriskAsteriskKeyword_0()); 

            }

             after(grammarAccess.getBinary_numeric_operatorAccess().getBnonameAsteriskAsteriskKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary_numeric_operator__BnonameAssignment"


    // $ANTLR start "rule__Unary_numeric_operator__UnonameAssignment"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5206:1: rule__Unary_numeric_operator__UnonameAssignment : ( ( 'abs' ) ) ;
    public final void rule__Unary_numeric_operator__UnonameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5210:1: ( ( ( 'abs' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5211:1: ( ( 'abs' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5211:1: ( ( 'abs' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5212:1: ( 'abs' )
            {
             before(grammarAccess.getUnary_numeric_operatorAccess().getUnonameAbsKeyword_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5213:1: ( 'abs' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5214:1: 'abs'
            {
             before(grammarAccess.getUnary_numeric_operatorAccess().getUnonameAbsKeyword_0()); 
            match(input,43,FOLLOW_43_in_rule__Unary_numeric_operator__UnonameAssignment10711); 
             after(grammarAccess.getUnary_numeric_operatorAccess().getUnonameAbsKeyword_0()); 

            }

             after(grammarAccess.getUnary_numeric_operatorAccess().getUnonameAbsKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary_numeric_operator__UnonameAssignment"


    // $ANTLR start "rule__Unary_boolean_operator__UbonameAssignment"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5229:1: rule__Unary_boolean_operator__UbonameAssignment : ( ( 'not' ) ) ;
    public final void rule__Unary_boolean_operator__UbonameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5233:1: ( ( ( 'not' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5234:1: ( ( 'not' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5234:1: ( ( 'not' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5235:1: ( 'not' )
            {
             before(grammarAccess.getUnary_boolean_operatorAccess().getUbonameNotKeyword_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5236:1: ( 'not' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5237:1: 'not'
            {
             before(grammarAccess.getUnary_boolean_operatorAccess().getUbonameNotKeyword_0()); 
            match(input,44,FOLLOW_44_in_rule__Unary_boolean_operator__UbonameAssignment10755); 
             after(grammarAccess.getUnary_boolean_operatorAccess().getUbonameNotKeyword_0()); 

            }

             after(grammarAccess.getUnary_boolean_operatorAccess().getUbonameNotKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary_boolean_operator__UbonameAssignment"


    // $ANTLR start "rule__Boolean_literal__Blname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5252:1: rule__Boolean_literal__Blname1Assignment_0 : ( ( 'true' ) ) ;
    public final void rule__Boolean_literal__Blname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5256:1: ( ( ( 'true' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5257:1: ( ( 'true' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5257:1: ( ( 'true' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5258:1: ( 'true' )
            {
             before(grammarAccess.getBoolean_literalAccess().getBlname1TrueKeyword_0_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5259:1: ( 'true' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5260:1: 'true'
            {
             before(grammarAccess.getBoolean_literalAccess().getBlname1TrueKeyword_0_0()); 
            match(input,45,FOLLOW_45_in_rule__Boolean_literal__Blname1Assignment_010799); 
             after(grammarAccess.getBoolean_literalAccess().getBlname1TrueKeyword_0_0()); 

            }

             after(grammarAccess.getBoolean_literalAccess().getBlname1TrueKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Boolean_literal__Blname1Assignment_0"


    // $ANTLR start "rule__Boolean_literal__Blname1Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5275:1: rule__Boolean_literal__Blname1Assignment_1 : ( ( 'false' ) ) ;
    public final void rule__Boolean_literal__Blname1Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5279:1: ( ( ( 'false' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5280:1: ( ( 'false' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5280:1: ( ( 'false' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5281:1: ( 'false' )
            {
             before(grammarAccess.getBoolean_literalAccess().getBlname1FalseKeyword_1_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5282:1: ( 'false' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5283:1: 'false'
            {
             before(grammarAccess.getBoolean_literalAccess().getBlname1FalseKeyword_1_0()); 
            match(input,46,FOLLOW_46_in_rule__Boolean_literal__Blname1Assignment_110843); 
             after(grammarAccess.getBoolean_literalAccess().getBlname1FalseKeyword_1_0()); 

            }

             after(grammarAccess.getBoolean_literalAccess().getBlname1FalseKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Boolean_literal__Blname1Assignment_1"


    // $ANTLR start "rule__Integer_value__IvnameAssignment"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5298:1: rule__Integer_value__IvnameAssignment : ( rulenumeric_literal ) ;
    public final void rule__Integer_value__IvnameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5302:1: ( ( rulenumeric_literal ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5303:1: ( rulenumeric_literal )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5303:1: ( rulenumeric_literal )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5304:1: rulenumeric_literal
            {
             before(grammarAccess.getInteger_valueAccess().getIvnameNumeric_literalParserRuleCall_0()); 
            pushFollow(FOLLOW_rulenumeric_literal_in_rule__Integer_value__IvnameAssignment10882);
            rulenumeric_literal();

            state._fsp--;

             after(grammarAccess.getInteger_valueAccess().getIvnameNumeric_literalParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Integer_value__IvnameAssignment"


    // $ANTLR start "rule__Behavior_time__Btname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5313:1: rule__Behavior_time__Btname1Assignment_0 : ( ruleinteger_value ) ;
    public final void rule__Behavior_time__Btname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5317:1: ( ( ruleinteger_value ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5318:1: ( ruleinteger_value )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5318:1: ( ruleinteger_value )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5319:1: ruleinteger_value
            {
             before(grammarAccess.getBehavior_timeAccess().getBtname1Integer_valueParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleinteger_value_in_rule__Behavior_time__Btname1Assignment_010913);
            ruleinteger_value();

            state._fsp--;

             after(grammarAccess.getBehavior_timeAccess().getBtname1Integer_valueParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Behavior_time__Btname1Assignment_0"


    // $ANTLR start "rule__Behavior_time__Btname2Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5328:1: rule__Behavior_time__Btname2Assignment_1 : ( ruleunit_identifier ) ;
    public final void rule__Behavior_time__Btname2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5332:1: ( ( ruleunit_identifier ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5333:1: ( ruleunit_identifier )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5333:1: ( ruleunit_identifier )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5334:1: ruleunit_identifier
            {
             before(grammarAccess.getBehavior_timeAccess().getBtname2Unit_identifierParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleunit_identifier_in_rule__Behavior_time__Btname2Assignment_110944);
            ruleunit_identifier();

            state._fsp--;

             after(grammarAccess.getBehavior_timeAccess().getBtname2Unit_identifierParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Behavior_time__Btname2Assignment_1"


    // $ANTLR start "rule__Unit_identifier__Uiname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5343:1: rule__Unit_identifier__Uiname1Assignment_0 : ( ( 'ps' ) ) ;
    public final void rule__Unit_identifier__Uiname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5347:1: ( ( ( 'ps' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5348:1: ( ( 'ps' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5348:1: ( ( 'ps' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5349:1: ( 'ps' )
            {
             before(grammarAccess.getUnit_identifierAccess().getUiname1PsKeyword_0_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5350:1: ( 'ps' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5351:1: 'ps'
            {
             before(grammarAccess.getUnit_identifierAccess().getUiname1PsKeyword_0_0()); 
            match(input,47,FOLLOW_47_in_rule__Unit_identifier__Uiname1Assignment_010980); 
             after(grammarAccess.getUnit_identifierAccess().getUiname1PsKeyword_0_0()); 

            }

             after(grammarAccess.getUnit_identifierAccess().getUiname1PsKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unit_identifier__Uiname1Assignment_0"


    // $ANTLR start "rule__Unit_identifier__Uiname2Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5366:1: rule__Unit_identifier__Uiname2Assignment_1 : ( ( 'ns' ) ) ;
    public final void rule__Unit_identifier__Uiname2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5370:1: ( ( ( 'ns' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5371:1: ( ( 'ns' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5371:1: ( ( 'ns' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5372:1: ( 'ns' )
            {
             before(grammarAccess.getUnit_identifierAccess().getUiname2NsKeyword_1_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5373:1: ( 'ns' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5374:1: 'ns'
            {
             before(grammarAccess.getUnit_identifierAccess().getUiname2NsKeyword_1_0()); 
            match(input,48,FOLLOW_48_in_rule__Unit_identifier__Uiname2Assignment_111024); 
             after(grammarAccess.getUnit_identifierAccess().getUiname2NsKeyword_1_0()); 

            }

             after(grammarAccess.getUnit_identifierAccess().getUiname2NsKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unit_identifier__Uiname2Assignment_1"


    // $ANTLR start "rule__Unit_identifier__Uiname3Assignment_2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5389:1: rule__Unit_identifier__Uiname3Assignment_2 : ( ( 'us' ) ) ;
    public final void rule__Unit_identifier__Uiname3Assignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5393:1: ( ( ( 'us' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5394:1: ( ( 'us' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5394:1: ( ( 'us' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5395:1: ( 'us' )
            {
             before(grammarAccess.getUnit_identifierAccess().getUiname3UsKeyword_2_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5396:1: ( 'us' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5397:1: 'us'
            {
             before(grammarAccess.getUnit_identifierAccess().getUiname3UsKeyword_2_0()); 
            match(input,49,FOLLOW_49_in_rule__Unit_identifier__Uiname3Assignment_211068); 
             after(grammarAccess.getUnit_identifierAccess().getUiname3UsKeyword_2_0()); 

            }

             after(grammarAccess.getUnit_identifierAccess().getUiname3UsKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unit_identifier__Uiname3Assignment_2"


    // $ANTLR start "rule__Unit_identifier__Uiname4Assignment_3"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5412:1: rule__Unit_identifier__Uiname4Assignment_3 : ( ( 'ms' ) ) ;
    public final void rule__Unit_identifier__Uiname4Assignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5416:1: ( ( ( 'ms' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5417:1: ( ( 'ms' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5417:1: ( ( 'ms' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5418:1: ( 'ms' )
            {
             before(grammarAccess.getUnit_identifierAccess().getUiname4MsKeyword_3_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5419:1: ( 'ms' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5420:1: 'ms'
            {
             before(grammarAccess.getUnit_identifierAccess().getUiname4MsKeyword_3_0()); 
            match(input,50,FOLLOW_50_in_rule__Unit_identifier__Uiname4Assignment_311112); 
             after(grammarAccess.getUnit_identifierAccess().getUiname4MsKeyword_3_0()); 

            }

             after(grammarAccess.getUnit_identifierAccess().getUiname4MsKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unit_identifier__Uiname4Assignment_3"


    // $ANTLR start "rule__Unit_identifier__Uiname5Assignment_4"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5435:1: rule__Unit_identifier__Uiname5Assignment_4 : ( ( 'sec' ) ) ;
    public final void rule__Unit_identifier__Uiname5Assignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5439:1: ( ( ( 'sec' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5440:1: ( ( 'sec' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5440:1: ( ( 'sec' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5441:1: ( 'sec' )
            {
             before(grammarAccess.getUnit_identifierAccess().getUiname5SecKeyword_4_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5442:1: ( 'sec' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5443:1: 'sec'
            {
             before(grammarAccess.getUnit_identifierAccess().getUiname5SecKeyword_4_0()); 
            match(input,51,FOLLOW_51_in_rule__Unit_identifier__Uiname5Assignment_411156); 
             after(grammarAccess.getUnit_identifierAccess().getUiname5SecKeyword_4_0()); 

            }

             after(grammarAccess.getUnit_identifierAccess().getUiname5SecKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unit_identifier__Uiname5Assignment_4"


    // $ANTLR start "rule__Unit_identifier__Uiname6Assignment_5"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5458:1: rule__Unit_identifier__Uiname6Assignment_5 : ( ( 'min' ) ) ;
    public final void rule__Unit_identifier__Uiname6Assignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5462:1: ( ( ( 'min' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5463:1: ( ( 'min' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5463:1: ( ( 'min' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5464:1: ( 'min' )
            {
             before(grammarAccess.getUnit_identifierAccess().getUiname6MinKeyword_5_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5465:1: ( 'min' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5466:1: 'min'
            {
             before(grammarAccess.getUnit_identifierAccess().getUiname6MinKeyword_5_0()); 
            match(input,52,FOLLOW_52_in_rule__Unit_identifier__Uiname6Assignment_511200); 
             after(grammarAccess.getUnit_identifierAccess().getUiname6MinKeyword_5_0()); 

            }

             after(grammarAccess.getUnit_identifierAccess().getUiname6MinKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unit_identifier__Uiname6Assignment_5"


    // $ANTLR start "rule__Unit_identifier__Uiname7Assignment_6"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5481:1: rule__Unit_identifier__Uiname7Assignment_6 : ( ( 'hr' ) ) ;
    public final void rule__Unit_identifier__Uiname7Assignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5485:1: ( ( ( 'hr' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5486:1: ( ( 'hr' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5486:1: ( ( 'hr' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5487:1: ( 'hr' )
            {
             before(grammarAccess.getUnit_identifierAccess().getUiname7HrKeyword_6_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5488:1: ( 'hr' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5489:1: 'hr'
            {
             before(grammarAccess.getUnit_identifierAccess().getUiname7HrKeyword_6_0()); 
            match(input,53,FOLLOW_53_in_rule__Unit_identifier__Uiname7Assignment_611244); 
             after(grammarAccess.getUnit_identifierAccess().getUiname7HrKeyword_6_0()); 

            }

             after(grammarAccess.getUnit_identifierAccess().getUiname7HrKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unit_identifier__Uiname7Assignment_6"


    // $ANTLR start "rule__Numeric_literal__Nlname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5504:1: rule__Numeric_literal__Nlname1Assignment_0 : ( ruleinteger_literal ) ;
    public final void rule__Numeric_literal__Nlname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5508:1: ( ( ruleinteger_literal ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5509:1: ( ruleinteger_literal )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5509:1: ( ruleinteger_literal )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5510:1: ruleinteger_literal
            {
             before(grammarAccess.getNumeric_literalAccess().getNlname1Integer_literalParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleinteger_literal_in_rule__Numeric_literal__Nlname1Assignment_011283);
            ruleinteger_literal();

            state._fsp--;

             after(grammarAccess.getNumeric_literalAccess().getNlname1Integer_literalParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Numeric_literal__Nlname1Assignment_0"


    // $ANTLR start "rule__Numeric_literal__Nlname2Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5519:1: rule__Numeric_literal__Nlname2Assignment_1 : ( rulereal_literal ) ;
    public final void rule__Numeric_literal__Nlname2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5523:1: ( ( rulereal_literal ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5524:1: ( rulereal_literal )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5524:1: ( rulereal_literal )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5525:1: rulereal_literal
            {
             before(grammarAccess.getNumeric_literalAccess().getNlname2Real_literalParserRuleCall_1_0()); 
            pushFollow(FOLLOW_rulereal_literal_in_rule__Numeric_literal__Nlname2Assignment_111314);
            rulereal_literal();

            state._fsp--;

             after(grammarAccess.getNumeric_literalAccess().getNlname2Real_literalParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Numeric_literal__Nlname2Assignment_1"


    // $ANTLR start "rule__Integer_literal__Ilname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5534:1: rule__Integer_literal__Ilname1Assignment_0 : ( ruledecimal_integer_literal ) ;
    public final void rule__Integer_literal__Ilname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5538:1: ( ( ruledecimal_integer_literal ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5539:1: ( ruledecimal_integer_literal )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5539:1: ( ruledecimal_integer_literal )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5540:1: ruledecimal_integer_literal
            {
             before(grammarAccess.getInteger_literalAccess().getIlname1Decimal_integer_literalParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruledecimal_integer_literal_in_rule__Integer_literal__Ilname1Assignment_011345);
            ruledecimal_integer_literal();

            state._fsp--;

             after(grammarAccess.getInteger_literalAccess().getIlname1Decimal_integer_literalParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Integer_literal__Ilname1Assignment_0"


    // $ANTLR start "rule__Integer_literal__Ilname2Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5549:1: rule__Integer_literal__Ilname2Assignment_1 : ( rulebased_integer_literal ) ;
    public final void rule__Integer_literal__Ilname2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5553:1: ( ( rulebased_integer_literal ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5554:1: ( rulebased_integer_literal )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5554:1: ( rulebased_integer_literal )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5555:1: rulebased_integer_literal
            {
             before(grammarAccess.getInteger_literalAccess().getIlname2Based_integer_literalParserRuleCall_1_0()); 
            pushFollow(FOLLOW_rulebased_integer_literal_in_rule__Integer_literal__Ilname2Assignment_111376);
            rulebased_integer_literal();

            state._fsp--;

             after(grammarAccess.getInteger_literalAccess().getIlname2Based_integer_literalParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Integer_literal__Ilname2Assignment_1"


    // $ANTLR start "rule__Real_literal__RlnameAssignment"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5564:1: rule__Real_literal__RlnameAssignment : ( ruledecimal_real_literal ) ;
    public final void rule__Real_literal__RlnameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5568:1: ( ( ruledecimal_real_literal ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5569:1: ( ruledecimal_real_literal )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5569:1: ( ruledecimal_real_literal )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5570:1: ruledecimal_real_literal
            {
             before(grammarAccess.getReal_literalAccess().getRlnameDecimal_real_literalParserRuleCall_0()); 
            pushFollow(FOLLOW_ruledecimal_real_literal_in_rule__Real_literal__RlnameAssignment11407);
            ruledecimal_real_literal();

            state._fsp--;

             after(grammarAccess.getReal_literalAccess().getRlnameDecimal_real_literalParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_literal__RlnameAssignment"


    // $ANTLR start "rule__Decimal_integer_literal__Dilname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5579:1: rule__Decimal_integer_literal__Dilname1Assignment_0 : ( rulenumeral ) ;
    public final void rule__Decimal_integer_literal__Dilname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5583:1: ( ( rulenumeral ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5584:1: ( rulenumeral )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5584:1: ( rulenumeral )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5585:1: rulenumeral
            {
             before(grammarAccess.getDecimal_integer_literalAccess().getDilname1NumeralParserRuleCall_0_0()); 
            pushFollow(FOLLOW_rulenumeral_in_rule__Decimal_integer_literal__Dilname1Assignment_011438);
            rulenumeral();

            state._fsp--;

             after(grammarAccess.getDecimal_integer_literalAccess().getDilname1NumeralParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_integer_literal__Dilname1Assignment_0"


    // $ANTLR start "rule__Decimal_integer_literal__Dilname2Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5594:1: rule__Decimal_integer_literal__Dilname2Assignment_1 : ( rulepositive_exponent ) ;
    public final void rule__Decimal_integer_literal__Dilname2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5598:1: ( ( rulepositive_exponent ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5599:1: ( rulepositive_exponent )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5599:1: ( rulepositive_exponent )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5600:1: rulepositive_exponent
            {
             before(grammarAccess.getDecimal_integer_literalAccess().getDilname2Positive_exponentParserRuleCall_1_0()); 
            pushFollow(FOLLOW_rulepositive_exponent_in_rule__Decimal_integer_literal__Dilname2Assignment_111469);
            rulepositive_exponent();

            state._fsp--;

             after(grammarAccess.getDecimal_integer_literalAccess().getDilname2Positive_exponentParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_integer_literal__Dilname2Assignment_1"


    // $ANTLR start "rule__Decimal_real_literal__Drlname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5609:1: rule__Decimal_real_literal__Drlname1Assignment_0 : ( rulenumeral ) ;
    public final void rule__Decimal_real_literal__Drlname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5613:1: ( ( rulenumeral ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5614:1: ( rulenumeral )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5614:1: ( rulenumeral )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5615:1: rulenumeral
            {
             before(grammarAccess.getDecimal_real_literalAccess().getDrlname1NumeralParserRuleCall_0_0()); 
            pushFollow(FOLLOW_rulenumeral_in_rule__Decimal_real_literal__Drlname1Assignment_011500);
            rulenumeral();

            state._fsp--;

             after(grammarAccess.getDecimal_real_literalAccess().getDrlname1NumeralParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_real_literal__Drlname1Assignment_0"


    // $ANTLR start "rule__Decimal_real_literal__Drlname2Assignment_2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5624:1: rule__Decimal_real_literal__Drlname2Assignment_2 : ( rulenumeral ) ;
    public final void rule__Decimal_real_literal__Drlname2Assignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5628:1: ( ( rulenumeral ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5629:1: ( rulenumeral )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5629:1: ( rulenumeral )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5630:1: rulenumeral
            {
             before(grammarAccess.getDecimal_real_literalAccess().getDrlname2NumeralParserRuleCall_2_0()); 
            pushFollow(FOLLOW_rulenumeral_in_rule__Decimal_real_literal__Drlname2Assignment_211531);
            rulenumeral();

            state._fsp--;

             after(grammarAccess.getDecimal_real_literalAccess().getDrlname2NumeralParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_real_literal__Drlname2Assignment_2"


    // $ANTLR start "rule__Decimal_real_literal__Drlname3Assignment_3"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5639:1: rule__Decimal_real_literal__Drlname3Assignment_3 : ( ruleexponent ) ;
    public final void rule__Decimal_real_literal__Drlname3Assignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5643:1: ( ( ruleexponent ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5644:1: ( ruleexponent )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5644:1: ( ruleexponent )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5645:1: ruleexponent
            {
             before(grammarAccess.getDecimal_real_literalAccess().getDrlname3ExponentParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleexponent_in_rule__Decimal_real_literal__Drlname3Assignment_311562);
            ruleexponent();

            state._fsp--;

             after(grammarAccess.getDecimal_real_literalAccess().getDrlname3ExponentParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Decimal_real_literal__Drlname3Assignment_3"


    // $ANTLR start "rule__Numeral__Nname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5654:1: rule__Numeral__Nname1Assignment_0 : ( ruleinteger ) ;
    public final void rule__Numeral__Nname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5658:1: ( ( ruleinteger ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5659:1: ( ruleinteger )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5659:1: ( ruleinteger )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5660:1: ruleinteger
            {
             before(grammarAccess.getNumeralAccess().getNname1IntegerParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleinteger_in_rule__Numeral__Nname1Assignment_011593);
            ruleinteger();

            state._fsp--;

             after(grammarAccess.getNumeralAccess().getNname1IntegerParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Numeral__Nname1Assignment_0"


    // $ANTLR start "rule__Numeral__Nname2Assignment_1_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5669:1: rule__Numeral__Nname2Assignment_1_1 : ( ruleinteger ) ;
    public final void rule__Numeral__Nname2Assignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5673:1: ( ( ruleinteger ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5674:1: ( ruleinteger )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5674:1: ( ruleinteger )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5675:1: ruleinteger
            {
             before(grammarAccess.getNumeralAccess().getNname2IntegerParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_ruleinteger_in_rule__Numeral__Nname2Assignment_1_111624);
            ruleinteger();

            state._fsp--;

             after(grammarAccess.getNumeralAccess().getNname2IntegerParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Numeral__Nname2Assignment_1_1"


    // $ANTLR start "rule__Exponent__Ename1Assignment_0_2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5684:1: rule__Exponent__Ename1Assignment_0_2 : ( rulenumeral ) ;
    public final void rule__Exponent__Ename1Assignment_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5688:1: ( ( rulenumeral ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5689:1: ( rulenumeral )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5689:1: ( rulenumeral )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5690:1: rulenumeral
            {
             before(grammarAccess.getExponentAccess().getEname1NumeralParserRuleCall_0_2_0()); 
            pushFollow(FOLLOW_rulenumeral_in_rule__Exponent__Ename1Assignment_0_211655);
            rulenumeral();

            state._fsp--;

             after(grammarAccess.getExponentAccess().getEname1NumeralParserRuleCall_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponent__Ename1Assignment_0_2"


    // $ANTLR start "rule__Exponent__Ename2Assignment_1_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5699:1: rule__Exponent__Ename2Assignment_1_1 : ( rulenumeral ) ;
    public final void rule__Exponent__Ename2Assignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5703:1: ( ( rulenumeral ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5704:1: ( rulenumeral )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5704:1: ( rulenumeral )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5705:1: rulenumeral
            {
             before(grammarAccess.getExponentAccess().getEname2NumeralParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_rulenumeral_in_rule__Exponent__Ename2Assignment_1_111686);
            rulenumeral();

            state._fsp--;

             after(grammarAccess.getExponentAccess().getEname2NumeralParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponent__Ename2Assignment_1_1"


    // $ANTLR start "rule__Positive_exponent__PenameAssignment_2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5714:1: rule__Positive_exponent__PenameAssignment_2 : ( rulenumeral ) ;
    public final void rule__Positive_exponent__PenameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5718:1: ( ( rulenumeral ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5719:1: ( rulenumeral )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5719:1: ( rulenumeral )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5720:1: rulenumeral
            {
             before(grammarAccess.getPositive_exponentAccess().getPenameNumeralParserRuleCall_2_0()); 
            pushFollow(FOLLOW_rulenumeral_in_rule__Positive_exponent__PenameAssignment_211717);
            rulenumeral();

            state._fsp--;

             after(grammarAccess.getPositive_exponentAccess().getPenameNumeralParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Positive_exponent__PenameAssignment_2"


    // $ANTLR start "rule__Based_integer_literal__Bilname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5729:1: rule__Based_integer_literal__Bilname1Assignment_0 : ( rulebase ) ;
    public final void rule__Based_integer_literal__Bilname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5733:1: ( ( rulebase ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5734:1: ( rulebase )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5734:1: ( rulebase )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5735:1: rulebase
            {
             before(grammarAccess.getBased_integer_literalAccess().getBilname1BaseParserRuleCall_0_0()); 
            pushFollow(FOLLOW_rulebase_in_rule__Based_integer_literal__Bilname1Assignment_011748);
            rulebase();

            state._fsp--;

             after(grammarAccess.getBased_integer_literalAccess().getBilname1BaseParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_integer_literal__Bilname1Assignment_0"


    // $ANTLR start "rule__Based_integer_literal__Bilname2Assignment_2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5744:1: rule__Based_integer_literal__Bilname2Assignment_2 : ( rulebased_numeral ) ;
    public final void rule__Based_integer_literal__Bilname2Assignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5748:1: ( ( rulebased_numeral ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5749:1: ( rulebased_numeral )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5749:1: ( rulebased_numeral )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5750:1: rulebased_numeral
            {
             before(grammarAccess.getBased_integer_literalAccess().getBilname2Based_numeralParserRuleCall_2_0()); 
            pushFollow(FOLLOW_rulebased_numeral_in_rule__Based_integer_literal__Bilname2Assignment_211779);
            rulebased_numeral();

            state._fsp--;

             after(grammarAccess.getBased_integer_literalAccess().getBilname2Based_numeralParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_integer_literal__Bilname2Assignment_2"


    // $ANTLR start "rule__Based_integer_literal__Bilname3Assignment_4"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5759:1: rule__Based_integer_literal__Bilname3Assignment_4 : ( rulepositive_exponent ) ;
    public final void rule__Based_integer_literal__Bilname3Assignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5763:1: ( ( rulepositive_exponent ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5764:1: ( rulepositive_exponent )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5764:1: ( rulepositive_exponent )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5765:1: rulepositive_exponent
            {
             before(grammarAccess.getBased_integer_literalAccess().getBilname3Positive_exponentParserRuleCall_4_0()); 
            pushFollow(FOLLOW_rulepositive_exponent_in_rule__Based_integer_literal__Bilname3Assignment_411810);
            rulepositive_exponent();

            state._fsp--;

             after(grammarAccess.getBased_integer_literalAccess().getBilname3Positive_exponentParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_integer_literal__Bilname3Assignment_4"


    // $ANTLR start "rule__Base__Bname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5774:1: rule__Base__Bname1Assignment_0 : ( RULE_DIGIT ) ;
    public final void rule__Base__Bname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5778:1: ( ( RULE_DIGIT ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5779:1: ( RULE_DIGIT )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5779:1: ( RULE_DIGIT )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5780:1: RULE_DIGIT
            {
             before(grammarAccess.getBaseAccess().getBname1DigitTerminalRuleCall_0_0()); 
            match(input,RULE_DIGIT,FOLLOW_RULE_DIGIT_in_rule__Base__Bname1Assignment_011841); 
             after(grammarAccess.getBaseAccess().getBname1DigitTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Base__Bname1Assignment_0"


    // $ANTLR start "rule__Base__Bname2Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5789:1: rule__Base__Bname2Assignment_1 : ( RULE_DIGIT ) ;
    public final void rule__Base__Bname2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5793:1: ( ( RULE_DIGIT ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5794:1: ( RULE_DIGIT )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5794:1: ( RULE_DIGIT )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5795:1: RULE_DIGIT
            {
             before(grammarAccess.getBaseAccess().getBname2DigitTerminalRuleCall_1_0()); 
            match(input,RULE_DIGIT,FOLLOW_RULE_DIGIT_in_rule__Base__Bname2Assignment_111872); 
             after(grammarAccess.getBaseAccess().getBname2DigitTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Base__Bname2Assignment_1"


    // $ANTLR start "rule__Based_numeral__Bnname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5804:1: rule__Based_numeral__Bnname1Assignment_0 : ( RULE_EXTENDED_DIGIT ) ;
    public final void rule__Based_numeral__Bnname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5808:1: ( ( RULE_EXTENDED_DIGIT ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5809:1: ( RULE_EXTENDED_DIGIT )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5809:1: ( RULE_EXTENDED_DIGIT )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5810:1: RULE_EXTENDED_DIGIT
            {
             before(grammarAccess.getBased_numeralAccess().getBnname1Extended_digitTerminalRuleCall_0_0()); 
            match(input,RULE_EXTENDED_DIGIT,FOLLOW_RULE_EXTENDED_DIGIT_in_rule__Based_numeral__Bnname1Assignment_011903); 
             after(grammarAccess.getBased_numeralAccess().getBnname1Extended_digitTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_numeral__Bnname1Assignment_0"


    // $ANTLR start "rule__Based_numeral__Bnname2Assignment_1_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5819:1: rule__Based_numeral__Bnname2Assignment_1_1 : ( RULE_EXTENDED_DIGIT ) ;
    public final void rule__Based_numeral__Bnname2Assignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5823:1: ( ( RULE_EXTENDED_DIGIT ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5824:1: ( RULE_EXTENDED_DIGIT )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5824:1: ( RULE_EXTENDED_DIGIT )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5825:1: RULE_EXTENDED_DIGIT
            {
             before(grammarAccess.getBased_numeralAccess().getBnname2Extended_digitTerminalRuleCall_1_1_0()); 
            match(input,RULE_EXTENDED_DIGIT,FOLLOW_RULE_EXTENDED_DIGIT_in_rule__Based_numeral__Bnname2Assignment_1_111934); 
             after(grammarAccess.getBased_numeralAccess().getBnname2Extended_digitTerminalRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Based_numeral__Bnname2Assignment_1_1"


    // $ANTLR start "rule__Integer__Value1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5834:1: rule__Integer__Value1Assignment_0 : ( RULE_INT ) ;
    public final void rule__Integer__Value1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5838:1: ( ( RULE_INT ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5839:1: ( RULE_INT )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5839:1: ( RULE_INT )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5840:1: RULE_INT
            {
             before(grammarAccess.getIntegerAccess().getValue1INTTerminalRuleCall_0_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__Integer__Value1Assignment_011965); 
             after(grammarAccess.getIntegerAccess().getValue1INTTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Integer__Value1Assignment_0"


    // $ANTLR start "rule__Integer__Value2Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5849:1: rule__Integer__Value2Assignment_1 : ( RULE_DIGIT ) ;
    public final void rule__Integer__Value2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5853:1: ( ( RULE_DIGIT ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5854:1: ( RULE_DIGIT )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5854:1: ( RULE_DIGIT )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5855:1: RULE_DIGIT
            {
             before(grammarAccess.getIntegerAccess().getValue2DigitTerminalRuleCall_1_0()); 
            match(input,RULE_DIGIT,FOLLOW_RULE_DIGIT_in_rule__Integer__Value2Assignment_111996); 
             after(grammarAccess.getIntegerAccess().getValue2DigitTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Integer__Value2Assignment_1"


    // $ANTLR start "rule__Dispatch_condition__Dcname3Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5864:1: rule__Dispatch_condition__Dcname3Assignment_0 : ( ( 'on dispatch' ) ) ;
    public final void rule__Dispatch_condition__Dcname3Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5868:1: ( ( ( 'on dispatch' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5869:1: ( ( 'on dispatch' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5869:1: ( ( 'on dispatch' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5870:1: ( 'on dispatch' )
            {
             before(grammarAccess.getDispatch_conditionAccess().getDcname3OnDispatchKeyword_0_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5871:1: ( 'on dispatch' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5872:1: 'on dispatch'
            {
             before(grammarAccess.getDispatch_conditionAccess().getDcname3OnDispatchKeyword_0_0()); 
            match(input,54,FOLLOW_54_in_rule__Dispatch_condition__Dcname3Assignment_012032); 
             after(grammarAccess.getDispatch_conditionAccess().getDcname3OnDispatchKeyword_0_0()); 

            }

             after(grammarAccess.getDispatch_conditionAccess().getDcname3OnDispatchKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_condition__Dcname3Assignment_0"


    // $ANTLR start "rule__Dispatch_condition__Dcname1Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5887:1: rule__Dispatch_condition__Dcname1Assignment_1 : ( ruledispatch_trigger_condition ) ;
    public final void rule__Dispatch_condition__Dcname1Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5891:1: ( ( ruledispatch_trigger_condition ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5892:1: ( ruledispatch_trigger_condition )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5892:1: ( ruledispatch_trigger_condition )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5893:1: ruledispatch_trigger_condition
            {
             before(grammarAccess.getDispatch_conditionAccess().getDcname1Dispatch_trigger_conditionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruledispatch_trigger_condition_in_rule__Dispatch_condition__Dcname1Assignment_112071);
            ruledispatch_trigger_condition();

            state._fsp--;

             after(grammarAccess.getDispatch_conditionAccess().getDcname1Dispatch_trigger_conditionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_condition__Dcname1Assignment_1"


    // $ANTLR start "rule__Dispatch_condition__Dcname2Assignment_2_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5902:1: rule__Dispatch_condition__Dcname2Assignment_2_1 : ( rulefrozen_ports ) ;
    public final void rule__Dispatch_condition__Dcname2Assignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5906:1: ( ( rulefrozen_ports ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5907:1: ( rulefrozen_ports )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5907:1: ( rulefrozen_ports )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5908:1: rulefrozen_ports
            {
             before(grammarAccess.getDispatch_conditionAccess().getDcname2Frozen_portsParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_rulefrozen_ports_in_rule__Dispatch_condition__Dcname2Assignment_2_112102);
            rulefrozen_ports();

            state._fsp--;

             after(grammarAccess.getDispatch_conditionAccess().getDcname2Frozen_portsParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_condition__Dcname2Assignment_2_1"


    // $ANTLR start "rule__Dispatch_trigger_condition__Dtcname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5917:1: rule__Dispatch_trigger_condition__Dtcname1Assignment_0 : ( ruledispatch_trigger_logical_expression ) ;
    public final void rule__Dispatch_trigger_condition__Dtcname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5921:1: ( ( ruledispatch_trigger_logical_expression ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5922:1: ( ruledispatch_trigger_logical_expression )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5922:1: ( ruledispatch_trigger_logical_expression )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5923:1: ruledispatch_trigger_logical_expression
            {
             before(grammarAccess.getDispatch_trigger_conditionAccess().getDtcname1Dispatch_trigger_logical_expressionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruledispatch_trigger_logical_expression_in_rule__Dispatch_trigger_condition__Dtcname1Assignment_012133);
            ruledispatch_trigger_logical_expression();

            state._fsp--;

             after(grammarAccess.getDispatch_trigger_conditionAccess().getDtcname1Dispatch_trigger_logical_expressionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_trigger_condition__Dtcname1Assignment_0"


    // $ANTLR start "rule__Dispatch_trigger_condition__Dtcname4Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5932:1: rule__Dispatch_trigger_condition__Dtcname4Assignment_1 : ( ( 'stop' ) ) ;
    public final void rule__Dispatch_trigger_condition__Dtcname4Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5936:1: ( ( ( 'stop' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5937:1: ( ( 'stop' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5937:1: ( ( 'stop' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5938:1: ( 'stop' )
            {
             before(grammarAccess.getDispatch_trigger_conditionAccess().getDtcname4StopKeyword_1_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5939:1: ( 'stop' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5940:1: 'stop'
            {
             before(grammarAccess.getDispatch_trigger_conditionAccess().getDtcname4StopKeyword_1_0()); 
            match(input,55,FOLLOW_55_in_rule__Dispatch_trigger_condition__Dtcname4Assignment_112169); 
             after(grammarAccess.getDispatch_trigger_conditionAccess().getDtcname4StopKeyword_1_0()); 

            }

             after(grammarAccess.getDispatch_trigger_conditionAccess().getDtcname4StopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_trigger_condition__Dtcname4Assignment_1"


    // $ANTLR start "rule__Dispatch_trigger_condition__Dtcname2Assignment_2"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5955:1: rule__Dispatch_trigger_condition__Dtcname2Assignment_2 : ( rulecompletion_relative_timeout_condition_and_catch ) ;
    public final void rule__Dispatch_trigger_condition__Dtcname2Assignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5959:1: ( ( rulecompletion_relative_timeout_condition_and_catch ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5960:1: ( rulecompletion_relative_timeout_condition_and_catch )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5960:1: ( rulecompletion_relative_timeout_condition_and_catch )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5961:1: rulecompletion_relative_timeout_condition_and_catch
            {
             before(grammarAccess.getDispatch_trigger_conditionAccess().getDtcname2Completion_relative_timeout_condition_and_catchParserRuleCall_2_0()); 
            pushFollow(FOLLOW_rulecompletion_relative_timeout_condition_and_catch_in_rule__Dispatch_trigger_condition__Dtcname2Assignment_212208);
            rulecompletion_relative_timeout_condition_and_catch();

            state._fsp--;

             after(grammarAccess.getDispatch_trigger_conditionAccess().getDtcname2Completion_relative_timeout_condition_and_catchParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_trigger_condition__Dtcname2Assignment_2"


    // $ANTLR start "rule__Dispatch_trigger_logical_expression__Dtlename1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5970:1: rule__Dispatch_trigger_logical_expression__Dtlename1Assignment_0 : ( ruledispatch_conjunction ) ;
    public final void rule__Dispatch_trigger_logical_expression__Dtlename1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5974:1: ( ( ruledispatch_conjunction ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5975:1: ( ruledispatch_conjunction )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5975:1: ( ruledispatch_conjunction )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5976:1: ruledispatch_conjunction
            {
             before(grammarAccess.getDispatch_trigger_logical_expressionAccess().getDtlename1Dispatch_conjunctionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruledispatch_conjunction_in_rule__Dispatch_trigger_logical_expression__Dtlename1Assignment_012239);
            ruledispatch_conjunction();

            state._fsp--;

             after(grammarAccess.getDispatch_trigger_logical_expressionAccess().getDtlename1Dispatch_conjunctionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_trigger_logical_expression__Dtlename1Assignment_0"


    // $ANTLR start "rule__Dispatch_trigger_logical_expression__Dtlename2Assignment_1_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5985:1: rule__Dispatch_trigger_logical_expression__Dtlename2Assignment_1_1 : ( ruledispatch_conjunction ) ;
    public final void rule__Dispatch_trigger_logical_expression__Dtlename2Assignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5989:1: ( ( ruledispatch_conjunction ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5990:1: ( ruledispatch_conjunction )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5990:1: ( ruledispatch_conjunction )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:5991:1: ruledispatch_conjunction
            {
             before(grammarAccess.getDispatch_trigger_logical_expressionAccess().getDtlename2Dispatch_conjunctionParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_ruledispatch_conjunction_in_rule__Dispatch_trigger_logical_expression__Dtlename2Assignment_1_112270);
            ruledispatch_conjunction();

            state._fsp--;

             after(grammarAccess.getDispatch_trigger_logical_expressionAccess().getDtlename2Dispatch_conjunctionParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_trigger_logical_expression__Dtlename2Assignment_1_1"


    // $ANTLR start "rule__Dispatch_conjunction__Dcname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6000:1: rule__Dispatch_conjunction__Dcname1Assignment_0 : ( ruledispatch_trigger ) ;
    public final void rule__Dispatch_conjunction__Dcname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6004:1: ( ( ruledispatch_trigger ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6005:1: ( ruledispatch_trigger )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6005:1: ( ruledispatch_trigger )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6006:1: ruledispatch_trigger
            {
             before(grammarAccess.getDispatch_conjunctionAccess().getDcname1Dispatch_triggerParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruledispatch_trigger_in_rule__Dispatch_conjunction__Dcname1Assignment_012301);
            ruledispatch_trigger();

            state._fsp--;

             after(grammarAccess.getDispatch_conjunctionAccess().getDcname1Dispatch_triggerParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_conjunction__Dcname1Assignment_0"


    // $ANTLR start "rule__Dispatch_conjunction__Dcname2Assignment_1_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6015:1: rule__Dispatch_conjunction__Dcname2Assignment_1_1 : ( ruledispatch_trigger ) ;
    public final void rule__Dispatch_conjunction__Dcname2Assignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6019:1: ( ( ruledispatch_trigger ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6020:1: ( ruledispatch_trigger )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6020:1: ( ruledispatch_trigger )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6021:1: ruledispatch_trigger
            {
             before(grammarAccess.getDispatch_conjunctionAccess().getDcname2Dispatch_triggerParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_ruledispatch_trigger_in_rule__Dispatch_conjunction__Dcname2Assignment_1_112332);
            ruledispatch_trigger();

            state._fsp--;

             after(grammarAccess.getDispatch_conjunctionAccess().getDcname2Dispatch_triggerParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_conjunction__Dcname2Assignment_1_1"


    // $ANTLR start "rule__Completion_relative_timeout_condition_and_catch__Cdtcacname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6030:1: rule__Completion_relative_timeout_condition_and_catch__Cdtcacname1Assignment_0 : ( ( 'timeout' ) ) ;
    public final void rule__Completion_relative_timeout_condition_and_catch__Cdtcacname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6034:1: ( ( ( 'timeout' ) ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6035:1: ( ( 'timeout' ) )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6035:1: ( ( 'timeout' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6036:1: ( 'timeout' )
            {
             before(grammarAccess.getCompletion_relative_timeout_condition_and_catchAccess().getCdtcacname1TimeoutKeyword_0_0()); 
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6037:1: ( 'timeout' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6038:1: 'timeout'
            {
             before(grammarAccess.getCompletion_relative_timeout_condition_and_catchAccess().getCdtcacname1TimeoutKeyword_0_0()); 
            match(input,26,FOLLOW_26_in_rule__Completion_relative_timeout_condition_and_catch__Cdtcacname1Assignment_012368); 
             after(grammarAccess.getCompletion_relative_timeout_condition_and_catchAccess().getCdtcacname1TimeoutKeyword_0_0()); 

            }

             after(grammarAccess.getCompletion_relative_timeout_condition_and_catchAccess().getCdtcacname1TimeoutKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Completion_relative_timeout_condition_and_catch__Cdtcacname1Assignment_0"


    // $ANTLR start "rule__Completion_relative_timeout_condition_and_catch__Cdtcacname2Assignment_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6053:1: rule__Completion_relative_timeout_condition_and_catch__Cdtcacname2Assignment_1 : ( rulebehavior_time ) ;
    public final void rule__Completion_relative_timeout_condition_and_catch__Cdtcacname2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6057:1: ( ( rulebehavior_time ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6058:1: ( rulebehavior_time )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6058:1: ( rulebehavior_time )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6059:1: rulebehavior_time
            {
             before(grammarAccess.getCompletion_relative_timeout_condition_and_catchAccess().getCdtcacname2Behavior_timeParserRuleCall_1_0()); 
            pushFollow(FOLLOW_rulebehavior_time_in_rule__Completion_relative_timeout_condition_and_catch__Cdtcacname2Assignment_112407);
            rulebehavior_time();

            state._fsp--;

             after(grammarAccess.getCompletion_relative_timeout_condition_and_catchAccess().getCdtcacname2Behavior_timeParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Completion_relative_timeout_condition_and_catch__Cdtcacname2Assignment_1"


    // $ANTLR start "rule__Dispatch_trigger__DtnameAssignment"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6068:1: rule__Dispatch_trigger__DtnameAssignment : ( RULE_STRING_LITERAL ) ;
    public final void rule__Dispatch_trigger__DtnameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6072:1: ( ( RULE_STRING_LITERAL ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6073:1: ( RULE_STRING_LITERAL )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6073:1: ( RULE_STRING_LITERAL )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6074:1: RULE_STRING_LITERAL
            {
             before(grammarAccess.getDispatch_triggerAccess().getDtnameString_literalTerminalRuleCall_0()); 
            match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rule__Dispatch_trigger__DtnameAssignment12438); 
             after(grammarAccess.getDispatch_triggerAccess().getDtnameString_literalTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dispatch_trigger__DtnameAssignment"


    // $ANTLR start "rule__Frozen_ports__Fpname1Assignment_0"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6083:1: rule__Frozen_ports__Fpname1Assignment_0 : ( RULE_STRING_LITERAL ) ;
    public final void rule__Frozen_ports__Fpname1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6087:1: ( ( RULE_STRING_LITERAL ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6088:1: ( RULE_STRING_LITERAL )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6088:1: ( RULE_STRING_LITERAL )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6089:1: RULE_STRING_LITERAL
            {
             before(grammarAccess.getFrozen_portsAccess().getFpname1String_literalTerminalRuleCall_0_0()); 
            match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rule__Frozen_ports__Fpname1Assignment_012469); 
             after(grammarAccess.getFrozen_portsAccess().getFpname1String_literalTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Frozen_ports__Fpname1Assignment_0"


    // $ANTLR start "rule__Frozen_ports__Fpname2Assignment_1_1"
    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6098:1: rule__Frozen_ports__Fpname2Assignment_1_1 : ( RULE_STRING_LITERAL ) ;
    public final void rule__Frozen_ports__Fpname2Assignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6102:1: ( ( RULE_STRING_LITERAL ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6103:1: ( RULE_STRING_LITERAL )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6103:1: ( RULE_STRING_LITERAL )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6104:1: RULE_STRING_LITERAL
            {
             before(grammarAccess.getFrozen_portsAccess().getFpname2String_literalTerminalRuleCall_1_1_0()); 
            match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rule__Frozen_ports__Fpname2Assignment_1_112500); 
             after(grammarAccess.getFrozen_portsAccess().getFpname2String_literalTerminalRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Frozen_ports__Fpname2Assignment_1_1"

    // Delegated rules


    protected DFA14 dfa14 = new DFA14(this);
    static final String DFA14_eotS =
        "\11\uffff";
    static final String DFA14_eofS =
        "\1\uffff\2\6\1\uffff\2\6\2\uffff\1\6";
    static final String DFA14_minS =
        "\6\5\2\uffff\1\5";
    static final String DFA14_maxS =
        "\1\7\2\65\1\7\2\65\2\uffff\1\65";
    static final String DFA14_acceptS =
        "\6\uffff\1\1\1\2\1\uffff";
    static final String DFA14_specialS =
        "\11\uffff}>";
    static final String[] DFA14_transitionS = {
            "\1\2\1\uffff\1\1",
            "\1\5\1\uffff\1\4\7\uffff\1\7\1\3\2\6\3\uffff\2\6\6\uffff\15"+
            "\6\4\uffff\7\6",
            "\1\10\1\uffff\1\4\7\uffff\1\7\1\3\2\6\1\uffff\1\6\1\uffff"+
            "\2\6\6\uffff\15\6\4\uffff\7\6",
            "\1\5\1\uffff\1\4",
            "\1\5\1\uffff\1\4\7\uffff\1\7\1\3\2\6\3\uffff\2\6\6\uffff\15"+
            "\6\4\uffff\7\6",
            "\1\5\1\uffff\1\4\7\uffff\1\7\1\3\2\6\3\uffff\2\6\6\uffff\15"+
            "\6\4\uffff\7\6",
            "",
            "",
            "\1\5\1\uffff\1\4\7\uffff\1\7\1\3\2\6\1\uffff\1\6\1\uffff\2"+
            "\6\6\uffff\15\6\4\uffff\7\6"
    };

    static final short[] DFA14_eot = DFA.unpackEncodedString(DFA14_eotS);
    static final short[] DFA14_eof = DFA.unpackEncodedString(DFA14_eofS);
    static final char[] DFA14_min = DFA.unpackEncodedStringToUnsignedChars(DFA14_minS);
    static final char[] DFA14_max = DFA.unpackEncodedStringToUnsignedChars(DFA14_maxS);
    static final short[] DFA14_accept = DFA.unpackEncodedString(DFA14_acceptS);
    static final short[] DFA14_special = DFA.unpackEncodedString(DFA14_specialS);
    static final short[][] DFA14_transition;

    static {
        int numStates = DFA14_transitionS.length;
        DFA14_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA14_transition[i] = DFA.unpackEncodedString(DFA14_transitionS[i]);
        }
    }

    class DFA14 extends DFA {

        public DFA14(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 14;
            this.eot = DFA14_eot;
            this.eof = DFA14_eof;
            this.min = DFA14_min;
            this.max = DFA14_max;
            this.accept = DFA14_accept;
            this.special = DFA14_special;
            this.transition = DFA14_transition;
        }
        public String getDescription() {
            return "1618:1: rule__Numeric_literal__Alternatives : ( ( ( rule__Numeric_literal__Nlname1Assignment_0 ) ) | ( ( rule__Numeric_literal__Nlname2Assignment_1 ) ) );";
        }
    }
 

    public static final BitSet FOLLOW_ruleGuard_in_entryRuleGuard61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGuard68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Guard__Alternatives_in_ruleGuard94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleexecute_condition_in_entryRuleexecute_condition121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleexecute_condition128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Execute_condition__Alternatives_in_ruleexecute_condition154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebehavior_action_block_timeout_catch_in_entryRulebehavior_action_block_timeout_catch181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebehavior_action_block_timeout_catch188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Behavior_action_block_timeout_catch__BabnameAssignment_in_rulebehavior_action_block_timeout_catch214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_expression_in_entryRulevalue_expression241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulevalue_expression248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_expression__Group__0_in_rulevalue_expression274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulerelation_in_entryRulerelation301 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulerelation308 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group__0_in_rulerelation334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulesimple_expression_in_entryRulesimple_expression361 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulesimple_expression368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Simple_expression__Group__0_in_rulesimple_expression394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleterm_in_entryRuleterm421 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleterm428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Term__Group__0_in_ruleterm454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulefactor_in_entryRulefactor481 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulefactor488 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Alternatives_in_rulefactor514 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_in_entryRulevalue541 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulevalue548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value__Alternatives_in_rulevalue574 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_variable_in_entryRulevalue_variable601 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulevalue_variable608 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_variable__Group__0_in_rulevalue_variable634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_constant_in_entryRulevalue_constant661 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulevalue_constant668 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_constant__Alternatives_in_rulevalue_constant694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulelogical_operator_in_entryRulelogical_operator721 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulelogical_operator728 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Logical_operator__Alternatives_in_rulelogical_operator754 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulerelational_operator_in_entryRulerelational_operator781 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulerelational_operator788 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relational_operator__Alternatives_in_rulerelational_operator814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebinary_adding_operator_in_entryRulebinary_adding_operator841 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebinary_adding_operator848 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Binary_adding_operator__Alternatives_in_rulebinary_adding_operator874 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunary_adding_operator_in_entryRuleunary_adding_operator901 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleunary_adding_operator908 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unary_adding_operator__Alternatives_in_ruleunary_adding_operator934 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulemultiplying_operator_in_entryRulemultiplying_operator961 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulemultiplying_operator968 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Multiplying_operator__Alternatives_in_rulemultiplying_operator994 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebinary_numeric_operator_in_entryRulebinary_numeric_operator1021 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebinary_numeric_operator1028 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Binary_numeric_operator__BnonameAssignment_in_rulebinary_numeric_operator1054 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunary_numeric_operator_in_entryRuleunary_numeric_operator1081 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleunary_numeric_operator1088 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unary_numeric_operator__UnonameAssignment_in_ruleunary_numeric_operator1114 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunary_boolean_operator_in_entryRuleunary_boolean_operator1141 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleunary_boolean_operator1148 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unary_boolean_operator__UbonameAssignment_in_ruleunary_boolean_operator1174 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleboolean_literal_in_entryRuleboolean_literal1201 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleboolean_literal1208 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Boolean_literal__Alternatives_in_ruleboolean_literal1234 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_value_in_entryRuleinteger_value1261 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleinteger_value1268 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Integer_value__IvnameAssignment_in_ruleinteger_value1294 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebehavior_time_in_entryRulebehavior_time1321 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebehavior_time1328 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Behavior_time__Group__0_in_rulebehavior_time1354 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunit_identifier_in_entryRuleunit_identifier1381 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleunit_identifier1388 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unit_identifier__Alternatives_in_ruleunit_identifier1414 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeric_literal_in_entryRulenumeric_literal1441 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulenumeric_literal1448 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Numeric_literal__Alternatives_in_rulenumeric_literal1474 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_literal_in_entryRuleinteger_literal1501 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleinteger_literal1508 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Integer_literal__Alternatives_in_ruleinteger_literal1534 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulereal_literal_in_entryRulereal_literal1561 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulereal_literal1568 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Real_literal__RlnameAssignment_in_rulereal_literal1594 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledecimal_integer_literal_in_entryRuledecimal_integer_literal1621 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledecimal_integer_literal1628 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Decimal_integer_literal__Group__0_in_ruledecimal_integer_literal1654 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledecimal_real_literal_in_entryRuledecimal_real_literal1681 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledecimal_real_literal1688 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Decimal_real_literal__Group__0_in_ruledecimal_real_literal1714 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeral_in_entryRulenumeral1741 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulenumeral1748 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Numeral__Group__0_in_rulenumeral1774 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleexponent_in_entryRuleexponent1801 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleexponent1808 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponent__Alternatives_in_ruleexponent1834 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulepositive_exponent_in_entryRulepositive_exponent1861 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulepositive_exponent1868 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Positive_exponent__Group__0_in_rulepositive_exponent1894 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebased_integer_literal_in_entryRulebased_integer_literal1921 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebased_integer_literal1928 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_integer_literal__Group__0_in_rulebased_integer_literal1954 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebase_in_entryRulebase1981 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebase1988 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Base__Group__0_in_rulebase2014 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebased_numeral_in_entryRulebased_numeral2041 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebased_numeral2048 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_numeral__Group__0_in_rulebased_numeral2074 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_in_entryRuleinteger2101 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleinteger2108 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Integer__Alternatives_in_ruleinteger2134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_condition_in_entryRuledispatch_condition2161 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledispatch_condition2168 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_condition__Group__0_in_ruledispatch_condition2194 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_trigger_condition_in_entryRuledispatch_trigger_condition2221 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledispatch_trigger_condition2228 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_trigger_condition__Alternatives_in_ruledispatch_trigger_condition2254 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_trigger_logical_expression_in_entryRuledispatch_trigger_logical_expression2281 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledispatch_trigger_logical_expression2288 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_trigger_logical_expression__Group__0_in_ruledispatch_trigger_logical_expression2314 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_conjunction_in_entryRuledispatch_conjunction2341 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledispatch_conjunction2348 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_conjunction__Group__0_in_ruledispatch_conjunction2374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulecompletion_relative_timeout_condition_and_catch_in_entryRulecompletion_relative_timeout_condition_and_catch2401 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulecompletion_relative_timeout_condition_and_catch2408 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Completion_relative_timeout_condition_and_catch__Group__0_in_rulecompletion_relative_timeout_condition_and_catch2434 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_trigger_in_entryRuledispatch_trigger2461 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledispatch_trigger2468 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_trigger__DtnameAssignment_in_ruledispatch_trigger2494 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulefrozen_ports_in_entryRulefrozen_ports2521 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulefrozen_ports2528 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Frozen_ports__Group__0_in_rulefrozen_ports2554 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Guard__DispatchAssignment_0_in_rule__Guard__Alternatives2590 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Guard__ExecuteAssignment_1_in_rule__Guard__Alternatives2608 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Execute_condition__Ecname1Assignment_0_in_rule__Execute_condition__Alternatives2641 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Execute_condition__Ecname2Assignment_1_in_rule__Execute_condition__Alternatives2659 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Execute_condition__Ecname3Assignment_2_in_rule__Execute_condition__Alternatives2677 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Group_0__0_in_rule__Factor__Alternatives2710 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Group_1__0_in_rule__Factor__Alternatives2728 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Group_2__0_in_rule__Factor__Alternatives2746 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value__Vname1Assignment_0_in_rule__Value__Alternatives2779 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value__Vname2Assignment_1_in_rule__Value__Alternatives2797 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_variable__Vvname1Assignment_1_0_in_rule__Value_variable__Alternatives_12830 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_variable__Vvname2Assignment_1_1_in_rule__Value_variable__Alternatives_12848 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_variable__Vvname3Assignment_1_2_in_rule__Value_variable__Alternatives_12866 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_constant__Vcname1Assignment_0_in_rule__Value_constant__Alternatives2899 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_constant__Vcname2Assignment_1_in_rule__Value_constant__Alternatives2917 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_constant__Group_2__0_in_rule__Value_constant__Alternatives2935 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Logical_operator__Loname1Assignment_0_in_rule__Logical_operator__Alternatives2968 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Logical_operator__Loname2Assignment_1_in_rule__Logical_operator__Alternatives2986 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Logical_operator__Loname3Assignment_2_in_rule__Logical_operator__Alternatives3004 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relational_operator__Roname1Assignment_0_in_rule__Relational_operator__Alternatives3037 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relational_operator__Roname2Assignment_1_in_rule__Relational_operator__Alternatives3055 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relational_operator__Roname3Assignment_2_in_rule__Relational_operator__Alternatives3073 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relational_operator__Roname4Assignment_3_in_rule__Relational_operator__Alternatives3091 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relational_operator__Roname5Assignment_4_in_rule__Relational_operator__Alternatives3109 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relational_operator__Roname6Assignment_5_in_rule__Relational_operator__Alternatives3127 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Binary_adding_operator__Baoname1Assignment_0_in_rule__Binary_adding_operator__Alternatives3160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Binary_adding_operator__Baoname2Assignment_1_in_rule__Binary_adding_operator__Alternatives3178 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unary_adding_operator__Uaoname1Assignment_0_in_rule__Unary_adding_operator__Alternatives3211 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unary_adding_operator__Uaoname2Assignment_1_in_rule__Unary_adding_operator__Alternatives3229 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Multiplying_operator__Moname1Assignment_0_in_rule__Multiplying_operator__Alternatives3262 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Multiplying_operator__Moname2Assignment_1_in_rule__Multiplying_operator__Alternatives3280 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Multiplying_operator__Moname3Assignment_2_in_rule__Multiplying_operator__Alternatives3298 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Multiplying_operator__Moname4Assignment_3_in_rule__Multiplying_operator__Alternatives3316 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Boolean_literal__Blname1Assignment_0_in_rule__Boolean_literal__Alternatives3349 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Boolean_literal__Blname1Assignment_1_in_rule__Boolean_literal__Alternatives3367 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unit_identifier__Uiname1Assignment_0_in_rule__Unit_identifier__Alternatives3400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unit_identifier__Uiname2Assignment_1_in_rule__Unit_identifier__Alternatives3418 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unit_identifier__Uiname3Assignment_2_in_rule__Unit_identifier__Alternatives3436 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unit_identifier__Uiname4Assignment_3_in_rule__Unit_identifier__Alternatives3454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unit_identifier__Uiname5Assignment_4_in_rule__Unit_identifier__Alternatives3472 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unit_identifier__Uiname6Assignment_5_in_rule__Unit_identifier__Alternatives3490 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unit_identifier__Uiname7Assignment_6_in_rule__Unit_identifier__Alternatives3508 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Numeric_literal__Nlname1Assignment_0_in_rule__Numeric_literal__Alternatives3541 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Numeric_literal__Nlname2Assignment_1_in_rule__Numeric_literal__Alternatives3559 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Integer_literal__Ilname1Assignment_0_in_rule__Integer_literal__Alternatives3592 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Integer_literal__Ilname2Assignment_1_in_rule__Integer_literal__Alternatives3610 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponent__Group_0__0_in_rule__Exponent__Alternatives3643 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponent__Group_1__0_in_rule__Exponent__Alternatives3661 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Integer__Value1Assignment_0_in_rule__Integer__Alternatives3694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Integer__Value2Assignment_1_in_rule__Integer__Alternatives3712 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_trigger_condition__Dtcname1Assignment_0_in_rule__Dispatch_trigger_condition__Alternatives3745 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_trigger_condition__Dtcname4Assignment_1_in_rule__Dispatch_trigger_condition__Alternatives3763 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_trigger_condition__Dtcname2Assignment_2_in_rule__Dispatch_trigger_condition__Alternatives3781 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_expression__Group__0__Impl_in_rule__Value_expression__Group__03812 = new BitSet(new long[]{0x0000000040C00000L});
    public static final BitSet FOLLOW_rule__Value_expression__Group__1_in_rule__Value_expression__Group__03815 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_expression__Vename1Assignment_0_in_rule__Value_expression__Group__0__Impl3842 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_expression__Group__1__Impl_in_rule__Value_expression__Group__13872 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_expression__Group_1__0_in_rule__Value_expression__Group__1__Impl3899 = new BitSet(new long[]{0x0000000040C00002L});
    public static final BitSet FOLLOW_rule__Value_expression__Group_1__0__Impl_in_rule__Value_expression__Group_1__03934 = new BitSet(new long[]{0x00007820000400B0L});
    public static final BitSet FOLLOW_rule__Value_expression__Group_1__1_in_rule__Value_expression__Group_1__03937 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_expression__Vename3Assignment_1_0_in_rule__Value_expression__Group_1__0__Impl3964 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_expression__Group_1__1__Impl_in_rule__Value_expression__Group_1__13994 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_expression__Vename2Assignment_1_1_in_rule__Value_expression__Group_1__1__Impl4021 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group__0__Impl_in_rule__Relation__Group__04055 = new BitSet(new long[]{0x0000001F80000000L});
    public static final BitSet FOLLOW_rule__Relation__Group__1_in_rule__Relation__Group__04058 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Rname1Assignment_0_in_rule__Relation__Group__0__Impl4085 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group__1__Impl_in_rule__Relation__Group__14115 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1__0_in_rule__Relation__Group__1__Impl4142 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1__0__Impl_in_rule__Relation__Group_1__04177 = new BitSet(new long[]{0x00007820000400B0L});
    public static final BitSet FOLLOW_rule__Relation__Group_1__1_in_rule__Relation__Group_1__04180 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Rname3Assignment_1_0_in_rule__Relation__Group_1__0__Impl4207 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1__1__Impl_in_rule__Relation__Group_1__14237 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Rname2Assignment_1_1_in_rule__Relation__Group_1__1__Impl4264 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Simple_expression__Group__0__Impl_in_rule__Simple_expression__Group__04298 = new BitSet(new long[]{0x00007820000400B0L});
    public static final BitSet FOLLOW_rule__Simple_expression__Group__1_in_rule__Simple_expression__Group__04301 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Simple_expression__Sename2Assignment_0_in_rule__Simple_expression__Group__0__Impl4328 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Simple_expression__Group__1__Impl_in_rule__Simple_expression__Group__14359 = new BitSet(new long[]{0x0000002000040000L});
    public static final BitSet FOLLOW_rule__Simple_expression__Group__2_in_rule__Simple_expression__Group__14362 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Simple_expression__Sename1Assignment_1_in_rule__Simple_expression__Group__1__Impl4389 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Simple_expression__Group__2__Impl_in_rule__Simple_expression__Group__24419 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Simple_expression__Group_2__0_in_rule__Simple_expression__Group__2__Impl4446 = new BitSet(new long[]{0x0000002000040002L});
    public static final BitSet FOLLOW_rule__Simple_expression__Group_2__0__Impl_in_rule__Simple_expression__Group_2__04483 = new BitSet(new long[]{0x00007820000400B0L});
    public static final BitSet FOLLOW_rule__Simple_expression__Group_2__1_in_rule__Simple_expression__Group_2__04486 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Simple_expression__Sename3Assignment_2_0_in_rule__Simple_expression__Group_2__0__Impl4513 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Simple_expression__Group_2__1__Impl_in_rule__Simple_expression__Group_2__14543 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Simple_expression__Sename4Assignment_2_1_in_rule__Simple_expression__Group_2__1__Impl4570 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Term__Group__0__Impl_in_rule__Term__Group__04604 = new BitSet(new long[]{0x000003C000000000L});
    public static final BitSet FOLLOW_rule__Term__Group__1_in_rule__Term__Group__04607 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Term__Tname1Assignment_0_in_rule__Term__Group__0__Impl4634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Term__Group__1__Impl_in_rule__Term__Group__14664 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Term__Group_1__0_in_rule__Term__Group__1__Impl4691 = new BitSet(new long[]{0x000003C000000002L});
    public static final BitSet FOLLOW_rule__Term__Group_1__0__Impl_in_rule__Term__Group_1__04726 = new BitSet(new long[]{0x00007820000400B0L});
    public static final BitSet FOLLOW_rule__Term__Group_1__1_in_rule__Term__Group_1__04729 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Term__Tname3Assignment_1_0_in_rule__Term__Group_1__0__Impl4756 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Term__Group_1__1__Impl_in_rule__Term__Group_1__14786 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Term__Tname2Assignment_1_1_in_rule__Term__Group_1__1__Impl4813 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Group_0__0__Impl_in_rule__Factor__Group_0__04847 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_rule__Factor__Group_0__1_in_rule__Factor__Group_0__04850 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Fname1Assignment_0_0_in_rule__Factor__Group_0__0__Impl4877 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Group_0__1__Impl_in_rule__Factor__Group_0__14907 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Group_0_1__0_in_rule__Factor__Group_0__1__Impl4934 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Group_0_1__0__Impl_in_rule__Factor__Group_0_1__04969 = new BitSet(new long[]{0x00006000000000B0L});
    public static final BitSet FOLLOW_rule__Factor__Group_0_1__1_in_rule__Factor__Group_0_1__04972 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Fname3Assignment_0_1_0_in_rule__Factor__Group_0_1__0__Impl4999 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Group_0_1__1__Impl_in_rule__Factor__Group_0_1__15029 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Fname2Assignment_0_1_1_in_rule__Factor__Group_0_1__1__Impl5056 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Group_1__0__Impl_in_rule__Factor__Group_1__05090 = new BitSet(new long[]{0x00006000000000B0L});
    public static final BitSet FOLLOW_rule__Factor__Group_1__1_in_rule__Factor__Group_1__05093 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Fname4Assignment_1_0_in_rule__Factor__Group_1__0__Impl5120 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Group_1__1__Impl_in_rule__Factor__Group_1__15150 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Fname5Assignment_1_1_in_rule__Factor__Group_1__1__Impl5177 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Group_2__0__Impl_in_rule__Factor__Group_2__05211 = new BitSet(new long[]{0x00006000000000B0L});
    public static final BitSet FOLLOW_rule__Factor__Group_2__1_in_rule__Factor__Group_2__05214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Fname6Assignment_2_0_in_rule__Factor__Group_2__0__Impl5241 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Group_2__1__Impl_in_rule__Factor__Group_2__15271 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Factor__Fname7Assignment_2_1_in_rule__Factor__Group_2__1__Impl5298 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_variable__Group__0__Impl_in_rule__Value_variable__Group__05332 = new BitSet(new long[]{0x0000000038000000L});
    public static final BitSet FOLLOW_rule__Value_variable__Group__1_in_rule__Value_variable__Group__05335 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_variable__Vvname0Assignment_0_in_rule__Value_variable__Group__0__Impl5362 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_variable__Group__1__Impl_in_rule__Value_variable__Group__15392 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_variable__Alternatives_1_in_rule__Value_variable__Group__1__Impl5419 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_constant__Group_2__0__Impl_in_rule__Value_constant__Group_2__05454 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_rule__Value_constant__Group_2__1_in_rule__Value_constant__Group_2__05457 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_constant__Vcname3Assignment_2_0_in_rule__Value_constant__Group_2__0__Impl5484 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_constant__Group_2__1__Impl_in_rule__Value_constant__Group_2__15514 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Value_constant__Group_2__2_in_rule__Value_constant__Group_2__15517 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__Value_constant__Group_2__1__Impl5545 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_constant__Group_2__2__Impl_in_rule__Value_constant__Group_2__25576 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Value_constant__Vcname4Assignment_2_2_in_rule__Value_constant__Group_2__2__Impl5603 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Behavior_time__Group__0__Impl_in_rule__Behavior_time__Group__05639 = new BitSet(new long[]{0x003F800000000000L});
    public static final BitSet FOLLOW_rule__Behavior_time__Group__1_in_rule__Behavior_time__Group__05642 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Behavior_time__Btname1Assignment_0_in_rule__Behavior_time__Group__0__Impl5669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Behavior_time__Group__1__Impl_in_rule__Behavior_time__Group__15699 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Behavior_time__Btname2Assignment_1_in_rule__Behavior_time__Group__1__Impl5726 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Decimal_integer_literal__Group__0__Impl_in_rule__Decimal_integer_literal__Group__05760 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Decimal_integer_literal__Group__1_in_rule__Decimal_integer_literal__Group__05763 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Decimal_integer_literal__Dilname1Assignment_0_in_rule__Decimal_integer_literal__Group__0__Impl5790 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Decimal_integer_literal__Group__1__Impl_in_rule__Decimal_integer_literal__Group__15820 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Decimal_integer_literal__Dilname2Assignment_1_in_rule__Decimal_integer_literal__Group__1__Impl5847 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Decimal_real_literal__Group__0__Impl_in_rule__Decimal_real_literal__Group__05882 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__Decimal_real_literal__Group__1_in_rule__Decimal_real_literal__Group__05885 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Decimal_real_literal__Drlname1Assignment_0_in_rule__Decimal_real_literal__Group__0__Impl5912 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Decimal_real_literal__Group__1__Impl_in_rule__Decimal_real_literal__Group__15942 = new BitSet(new long[]{0x00000000000000A0L});
    public static final BitSet FOLLOW_rule__Decimal_real_literal__Group__2_in_rule__Decimal_real_literal__Group__15945 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Decimal_real_literal__Group__1__Impl5973 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Decimal_real_literal__Group__2__Impl_in_rule__Decimal_real_literal__Group__26004 = new BitSet(new long[]{0x00000000000A0000L});
    public static final BitSet FOLLOW_rule__Decimal_real_literal__Group__3_in_rule__Decimal_real_literal__Group__26007 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Decimal_real_literal__Drlname2Assignment_2_in_rule__Decimal_real_literal__Group__2__Impl6034 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Decimal_real_literal__Group__3__Impl_in_rule__Decimal_real_literal__Group__36064 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Decimal_real_literal__Drlname3Assignment_3_in_rule__Decimal_real_literal__Group__3__Impl6091 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Numeral__Group__0__Impl_in_rule__Numeral__Group__06130 = new BitSet(new long[]{0x00000000000100A0L});
    public static final BitSet FOLLOW_rule__Numeral__Group__1_in_rule__Numeral__Group__06133 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Numeral__Nname1Assignment_0_in_rule__Numeral__Group__0__Impl6160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Numeral__Group__1__Impl_in_rule__Numeral__Group__16190 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Numeral__Group_1__0_in_rule__Numeral__Group__1__Impl6217 = new BitSet(new long[]{0x00000000000100A2L});
    public static final BitSet FOLLOW_rule__Numeral__Group_1__0__Impl_in_rule__Numeral__Group_1__06252 = new BitSet(new long[]{0x00000000000100A0L});
    public static final BitSet FOLLOW_rule__Numeral__Group_1__1_in_rule__Numeral__Group_1__06255 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Numeral__Group_1__0__Impl6284 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Numeral__Group_1__1__Impl_in_rule__Numeral__Group_1__16317 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Numeral__Nname2Assignment_1_1_in_rule__Numeral__Group_1__1__Impl6344 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponent__Group_0__0__Impl_in_rule__Exponent__Group_0__06378 = new BitSet(new long[]{0x00000000000400A0L});
    public static final BitSet FOLLOW_rule__Exponent__Group_0__1_in_rule__Exponent__Group_0__06381 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Exponent__Group_0__0__Impl6409 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponent__Group_0__1__Impl_in_rule__Exponent__Group_0__16440 = new BitSet(new long[]{0x00000000000400A0L});
    public static final BitSet FOLLOW_rule__Exponent__Group_0__2_in_rule__Exponent__Group_0__16443 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Exponent__Group_0__1__Impl6472 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponent__Group_0__2__Impl_in_rule__Exponent__Group_0__26505 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponent__Ename1Assignment_0_2_in_rule__Exponent__Group_0__2__Impl6532 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponent__Group_1__0__Impl_in_rule__Exponent__Group_1__06568 = new BitSet(new long[]{0x00000000000000A0L});
    public static final BitSet FOLLOW_rule__Exponent__Group_1__1_in_rule__Exponent__Group_1__06571 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__Exponent__Group_1__0__Impl6599 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponent__Group_1__1__Impl_in_rule__Exponent__Group_1__16630 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponent__Ename2Assignment_1_1_in_rule__Exponent__Group_1__1__Impl6657 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Positive_exponent__Group__0__Impl_in_rule__Positive_exponent__Group__06691 = new BitSet(new long[]{0x00000000000400A0L});
    public static final BitSet FOLLOW_rule__Positive_exponent__Group__1_in_rule__Positive_exponent__Group__06694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Positive_exponent__Group__0__Impl6722 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Positive_exponent__Group__1__Impl_in_rule__Positive_exponent__Group__16753 = new BitSet(new long[]{0x00000000000400A0L});
    public static final BitSet FOLLOW_rule__Positive_exponent__Group__2_in_rule__Positive_exponent__Group__16756 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Positive_exponent__Group__1__Impl6785 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Positive_exponent__Group__2__Impl_in_rule__Positive_exponent__Group__26818 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Positive_exponent__PenameAssignment_2_in_rule__Positive_exponent__Group__2__Impl6845 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_integer_literal__Group__0__Impl_in_rule__Based_integer_literal__Group__06881 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__Based_integer_literal__Group__1_in_rule__Based_integer_literal__Group__06884 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_integer_literal__Bilname1Assignment_0_in_rule__Based_integer_literal__Group__0__Impl6911 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_integer_literal__Group__1__Impl_in_rule__Based_integer_literal__Group__16941 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rule__Based_integer_literal__Group__2_in_rule__Based_integer_literal__Group__16944 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Based_integer_literal__Group__1__Impl6972 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_integer_literal__Group__2__Impl_in_rule__Based_integer_literal__Group__27003 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__Based_integer_literal__Group__3_in_rule__Based_integer_literal__Group__27006 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_integer_literal__Bilname2Assignment_2_in_rule__Based_integer_literal__Group__2__Impl7033 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_integer_literal__Group__3__Impl_in_rule__Based_integer_literal__Group__37063 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Based_integer_literal__Group__4_in_rule__Based_integer_literal__Group__37066 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Based_integer_literal__Group__3__Impl7094 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_integer_literal__Group__4__Impl_in_rule__Based_integer_literal__Group__47125 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_integer_literal__Bilname3Assignment_4_in_rule__Based_integer_literal__Group__4__Impl7152 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Base__Group__0__Impl_in_rule__Base__Group__07193 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Base__Group__1_in_rule__Base__Group__07196 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Base__Bname1Assignment_0_in_rule__Base__Group__0__Impl7223 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Base__Group__1__Impl_in_rule__Base__Group__17253 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Base__Bname2Assignment_1_in_rule__Base__Group__1__Impl7280 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_numeral__Group__0__Impl_in_rule__Based_numeral__Group__07315 = new BitSet(new long[]{0x0000000000010040L});
    public static final BitSet FOLLOW_rule__Based_numeral__Group__1_in_rule__Based_numeral__Group__07318 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_numeral__Bnname1Assignment_0_in_rule__Based_numeral__Group__0__Impl7345 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_numeral__Group__1__Impl_in_rule__Based_numeral__Group__17375 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_numeral__Group_1__0_in_rule__Based_numeral__Group__1__Impl7402 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_numeral__Group_1__0__Impl_in_rule__Based_numeral__Group_1__07437 = new BitSet(new long[]{0x0000000000010040L});
    public static final BitSet FOLLOW_rule__Based_numeral__Group_1__1_in_rule__Based_numeral__Group_1__07440 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Based_numeral__Group_1__0__Impl7469 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_numeral__Group_1__1__Impl_in_rule__Based_numeral__Group_1__17502 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Based_numeral__Bnname2Assignment_1_1_in_rule__Based_numeral__Group_1__1__Impl7529 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_condition__Group__0__Impl_in_rule__Dispatch_condition__Group__07563 = new BitSet(new long[]{0x0080000004200010L});
    public static final BitSet FOLLOW_rule__Dispatch_condition__Group__1_in_rule__Dispatch_condition__Group__07566 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_condition__Dcname3Assignment_0_in_rule__Dispatch_condition__Group__0__Impl7593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_condition__Group__1__Impl_in_rule__Dispatch_condition__Group__17623 = new BitSet(new long[]{0x0080000004200010L});
    public static final BitSet FOLLOW_rule__Dispatch_condition__Group__2_in_rule__Dispatch_condition__Group__17626 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_condition__Dcname1Assignment_1_in_rule__Dispatch_condition__Group__1__Impl7653 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_condition__Group__2__Impl_in_rule__Dispatch_condition__Group__27684 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_condition__Group_2__0_in_rule__Dispatch_condition__Group__2__Impl7711 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_condition__Group_2__0__Impl_in_rule__Dispatch_condition__Group_2__07748 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Dispatch_condition__Group_2__1_in_rule__Dispatch_condition__Group_2__07751 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Dispatch_condition__Group_2__0__Impl7779 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_condition__Group_2__1__Impl_in_rule__Dispatch_condition__Group_2__17810 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_condition__Dcname2Assignment_2_1_in_rule__Dispatch_condition__Group_2__1__Impl7837 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_trigger_logical_expression__Group__0__Impl_in_rule__Dispatch_trigger_logical_expression__Group__07871 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__Dispatch_trigger_logical_expression__Group__1_in_rule__Dispatch_trigger_logical_expression__Group__07874 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_trigger_logical_expression__Dtlename1Assignment_0_in_rule__Dispatch_trigger_logical_expression__Group__0__Impl7901 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_trigger_logical_expression__Group__1__Impl_in_rule__Dispatch_trigger_logical_expression__Group__17931 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_trigger_logical_expression__Group_1__0_in_rule__Dispatch_trigger_logical_expression__Group__1__Impl7958 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_rule__Dispatch_trigger_logical_expression__Group_1__0__Impl_in_rule__Dispatch_trigger_logical_expression__Group_1__07993 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Dispatch_trigger_logical_expression__Group_1__1_in_rule__Dispatch_trigger_logical_expression__Group_1__07996 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Dispatch_trigger_logical_expression__Group_1__0__Impl8024 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_trigger_logical_expression__Group_1__1__Impl_in_rule__Dispatch_trigger_logical_expression__Group_1__18055 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_trigger_logical_expression__Dtlename2Assignment_1_1_in_rule__Dispatch_trigger_logical_expression__Group_1__1__Impl8082 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_conjunction__Group__0__Impl_in_rule__Dispatch_conjunction__Group__08116 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_rule__Dispatch_conjunction__Group__1_in_rule__Dispatch_conjunction__Group__08119 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_conjunction__Dcname1Assignment_0_in_rule__Dispatch_conjunction__Group__0__Impl8146 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_conjunction__Group__1__Impl_in_rule__Dispatch_conjunction__Group__18176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_conjunction__Group_1__0_in_rule__Dispatch_conjunction__Group__1__Impl8203 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_rule__Dispatch_conjunction__Group_1__0__Impl_in_rule__Dispatch_conjunction__Group_1__08238 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Dispatch_conjunction__Group_1__1_in_rule__Dispatch_conjunction__Group_1__08241 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__Dispatch_conjunction__Group_1__0__Impl8269 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_conjunction__Group_1__1__Impl_in_rule__Dispatch_conjunction__Group_1__18300 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dispatch_conjunction__Dcname2Assignment_1_1_in_rule__Dispatch_conjunction__Group_1__1__Impl8327 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Completion_relative_timeout_condition_and_catch__Group__0__Impl_in_rule__Completion_relative_timeout_condition_and_catch__Group__08361 = new BitSet(new long[]{0x00000000000000A0L});
    public static final BitSet FOLLOW_rule__Completion_relative_timeout_condition_and_catch__Group__1_in_rule__Completion_relative_timeout_condition_and_catch__Group__08364 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Completion_relative_timeout_condition_and_catch__Cdtcacname1Assignment_0_in_rule__Completion_relative_timeout_condition_and_catch__Group__0__Impl8391 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Completion_relative_timeout_condition_and_catch__Group__1__Impl_in_rule__Completion_relative_timeout_condition_and_catch__Group__18421 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Completion_relative_timeout_condition_and_catch__Cdtcacname2Assignment_1_in_rule__Completion_relative_timeout_condition_and_catch__Group__1__Impl8448 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Frozen_ports__Group__0__Impl_in_rule__Frozen_ports__Group__08483 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_rule__Frozen_ports__Group__1_in_rule__Frozen_ports__Group__08486 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Frozen_ports__Fpname1Assignment_0_in_rule__Frozen_ports__Group__0__Impl8513 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Frozen_ports__Group__1__Impl_in_rule__Frozen_ports__Group__18543 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Frozen_ports__Group_1__0_in_rule__Frozen_ports__Group__1__Impl8570 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_rule__Frozen_ports__Group_1__0__Impl_in_rule__Frozen_ports__Group_1__08605 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Frozen_ports__Group_1__1_in_rule__Frozen_ports__Group_1__08608 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__Frozen_ports__Group_1__0__Impl8636 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Frozen_ports__Group_1__1__Impl_in_rule__Frozen_ports__Group_1__18667 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Frozen_ports__Fpname2Assignment_1_1_in_rule__Frozen_ports__Group_1__1__Impl8694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_condition_in_rule__Guard__DispatchAssignment_08733 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleexecute_condition_in_rule__Guard__ExecuteAssignment_18764 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__Execute_condition__Ecname1Assignment_08800 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebehavior_action_block_timeout_catch_in_rule__Execute_condition__Ecname2Assignment_18839 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_expression_in_rule__Execute_condition__Ecname3Assignment_28870 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rule__Behavior_action_block_timeout_catch__BabnameAssignment8906 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulerelation_in_rule__Value_expression__Vename1Assignment_08945 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulelogical_operator_in_rule__Value_expression__Vename3Assignment_1_08976 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulerelation_in_rule__Value_expression__Vename2Assignment_1_19007 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulesimple_expression_in_rule__Relation__Rname1Assignment_09038 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulerelational_operator_in_rule__Relation__Rname3Assignment_1_09069 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulesimple_expression_in_rule__Relation__Rname2Assignment_1_19100 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunary_adding_operator_in_rule__Simple_expression__Sename2Assignment_09131 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleterm_in_rule__Simple_expression__Sename1Assignment_19162 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebinary_adding_operator_in_rule__Simple_expression__Sename3Assignment_2_09193 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleterm_in_rule__Simple_expression__Sename4Assignment_2_19224 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulefactor_in_rule__Term__Tname1Assignment_09255 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulemultiplying_operator_in_rule__Term__Tname3Assignment_1_09286 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulefactor_in_rule__Term__Tname2Assignment_1_19317 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_in_rule__Factor__Fname1Assignment_0_09348 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebinary_numeric_operator_in_rule__Factor__Fname3Assignment_0_1_09379 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_in_rule__Factor__Fname2Assignment_0_1_19410 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunary_numeric_operator_in_rule__Factor__Fname4Assignment_1_09441 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_in_rule__Factor__Fname5Assignment_1_19472 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunary_boolean_operator_in_rule__Factor__Fname6Assignment_2_09503 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_in_rule__Factor__Fname7Assignment_2_19534 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_variable_in_rule__Value__Vname1Assignment_09565 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_constant_in_rule__Value__Vname2Assignment_19596 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rule__Value_variable__Vvname0Assignment_09627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__Value_variable__Vvname1Assignment_1_09663 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__Value_variable__Vvname2Assignment_1_19707 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_rule__Value_variable__Vvname3Assignment_1_29751 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleboolean_literal_in_rule__Value_constant__Vcname1Assignment_09790 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeric_literal_in_rule__Value_constant__Vcname2Assignment_19821 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rule__Value_constant__Vcname3Assignment_2_09852 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rule__Value_constant__Vcname4Assignment_2_29883 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__Logical_operator__Loname1Assignment_09919 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Logical_operator__Loname2Assignment_19963 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__Logical_operator__Loname3Assignment_210007 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__Relational_operator__Roname1Assignment_010051 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_rule__Relational_operator__Roname2Assignment_110095 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_rule__Relational_operator__Roname3Assignment_210139 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_rule__Relational_operator__Roname4Assignment_310183 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_rule__Relational_operator__Roname5Assignment_410227 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_rule__Relational_operator__Roname6Assignment_510271 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Binary_adding_operator__Baoname1Assignment_010315 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_rule__Binary_adding_operator__Baoname2Assignment_110359 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Unary_adding_operator__Uaoname1Assignment_010403 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_rule__Unary_adding_operator__Uaoname2Assignment_110447 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_rule__Multiplying_operator__Moname1Assignment_010491 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_rule__Multiplying_operator__Moname2Assignment_110535 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_rule__Multiplying_operator__Moname3Assignment_210579 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_rule__Multiplying_operator__Moname4Assignment_310623 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_rule__Binary_numeric_operator__BnonameAssignment10667 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_rule__Unary_numeric_operator__UnonameAssignment10711 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_rule__Unary_boolean_operator__UbonameAssignment10755 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_rule__Boolean_literal__Blname1Assignment_010799 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_rule__Boolean_literal__Blname1Assignment_110843 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeric_literal_in_rule__Integer_value__IvnameAssignment10882 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_value_in_rule__Behavior_time__Btname1Assignment_010913 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunit_identifier_in_rule__Behavior_time__Btname2Assignment_110944 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_rule__Unit_identifier__Uiname1Assignment_010980 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_rule__Unit_identifier__Uiname2Assignment_111024 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_rule__Unit_identifier__Uiname3Assignment_211068 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_rule__Unit_identifier__Uiname4Assignment_311112 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_51_in_rule__Unit_identifier__Uiname5Assignment_411156 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_rule__Unit_identifier__Uiname6Assignment_511200 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_53_in_rule__Unit_identifier__Uiname7Assignment_611244 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_literal_in_rule__Numeric_literal__Nlname1Assignment_011283 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulereal_literal_in_rule__Numeric_literal__Nlname2Assignment_111314 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledecimal_integer_literal_in_rule__Integer_literal__Ilname1Assignment_011345 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebased_integer_literal_in_rule__Integer_literal__Ilname2Assignment_111376 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledecimal_real_literal_in_rule__Real_literal__RlnameAssignment11407 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeral_in_rule__Decimal_integer_literal__Dilname1Assignment_011438 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulepositive_exponent_in_rule__Decimal_integer_literal__Dilname2Assignment_111469 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeral_in_rule__Decimal_real_literal__Drlname1Assignment_011500 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeral_in_rule__Decimal_real_literal__Drlname2Assignment_211531 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleexponent_in_rule__Decimal_real_literal__Drlname3Assignment_311562 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_in_rule__Numeral__Nname1Assignment_011593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_in_rule__Numeral__Nname2Assignment_1_111624 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeral_in_rule__Exponent__Ename1Assignment_0_211655 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeral_in_rule__Exponent__Ename2Assignment_1_111686 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeral_in_rule__Positive_exponent__PenameAssignment_211717 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebase_in_rule__Based_integer_literal__Bilname1Assignment_011748 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebased_numeral_in_rule__Based_integer_literal__Bilname2Assignment_211779 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulepositive_exponent_in_rule__Based_integer_literal__Bilname3Assignment_411810 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DIGIT_in_rule__Base__Bname1Assignment_011841 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DIGIT_in_rule__Base__Bname2Assignment_111872 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_EXTENDED_DIGIT_in_rule__Based_numeral__Bnname1Assignment_011903 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_EXTENDED_DIGIT_in_rule__Based_numeral__Bnname2Assignment_1_111934 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__Integer__Value1Assignment_011965 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DIGIT_in_rule__Integer__Value2Assignment_111996 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_54_in_rule__Dispatch_condition__Dcname3Assignment_012032 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_trigger_condition_in_rule__Dispatch_condition__Dcname1Assignment_112071 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulefrozen_ports_in_rule__Dispatch_condition__Dcname2Assignment_2_112102 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_trigger_logical_expression_in_rule__Dispatch_trigger_condition__Dtcname1Assignment_012133 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_55_in_rule__Dispatch_trigger_condition__Dtcname4Assignment_112169 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulecompletion_relative_timeout_condition_and_catch_in_rule__Dispatch_trigger_condition__Dtcname2Assignment_212208 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_conjunction_in_rule__Dispatch_trigger_logical_expression__Dtlename1Assignment_012239 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_conjunction_in_rule__Dispatch_trigger_logical_expression__Dtlename2Assignment_1_112270 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_trigger_in_rule__Dispatch_conjunction__Dcname1Assignment_012301 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledispatch_trigger_in_rule__Dispatch_conjunction__Dcname2Assignment_1_112332 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rule__Completion_relative_timeout_condition_and_catch__Cdtcacname1Assignment_012368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebehavior_time_in_rule__Completion_relative_timeout_condition_and_catch__Cdtcacname2Assignment_112407 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rule__Dispatch_trigger__DtnameAssignment12438 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rule__Frozen_ports__Fpname1Assignment_012469 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rule__Frozen_ports__Fpname2Assignment_1_112500 = new BitSet(new long[]{0x0000000000000002L});

}