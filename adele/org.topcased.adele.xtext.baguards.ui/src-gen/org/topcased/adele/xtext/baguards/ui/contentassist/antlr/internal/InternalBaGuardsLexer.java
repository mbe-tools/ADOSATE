package org.topcased.adele.xtext.baguards.ui.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalBaGuardsLexer extends Lexer {
    public static final int RULE_ID=8;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int T__21=21;
    public static final int RULE_ANY_OTHER=13;
    public static final int T__20=20;
    public static final int EOF=-1;
    public static final int T__55=55;
    public static final int T__19=19;
    public static final int T__51=51;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__52=52;
    public static final int T__18=18;
    public static final int T__53=53;
    public static final int RULE_STRING_LITERAL=4;
    public static final int T__54=54;
    public static final int T__17=17;
    public static final int T__14=14;
    public static final int RULE_INT=7;
    public static final int T__50=50;
    public static final int RULE_EXTENDED_DIGIT=6;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RULE_SL_COMMENT=11;
    public static final int RULE_ML_COMMENT=10;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_STRING=9;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int RULE_WS=12;
    public static final int RULE_DIGIT=5;

    // delegates
    // delegators

    public InternalBaGuardsLexer() {;} 
    public InternalBaGuardsLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalBaGuardsLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g"; }

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:11:7: ( '::' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:11:9: '::'
            {
            match("::"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:12:7: ( '.' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:12:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:13:7: ( '_' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:13:9: '_'
            {
            match('_'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:14:7: ( 'E' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:14:9: 'E'
            {
            match('E'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:15:7: ( '+' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:15:9: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:16:7: ( 'E -' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:16:9: 'E -'
            {
            match("E -"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:17:7: ( '#' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:17:9: '#'
            {
            match('#'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:18:7: ( 'frozen' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:18:9: 'frozen'
            {
            match("frozen"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:19:7: ( 'or' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:19:9: 'or'
            {
            match("or"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:20:7: ( 'and' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:20:9: 'and'
            {
            match("and"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:21:7: ( ',' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:21:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:22:7: ( 'otherwise' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:22:9: 'otherwise'
            {
            match("otherwise"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:23:7: ( 'timeout' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:23:9: 'timeout'
            {
            match("timeout"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:24:7: ( '?' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:24:9: '?'
            {
            match('?'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:25:7: ( 'count' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:25:9: 'count'
            {
            match("count"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:26:7: ( 'fresh' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:26:9: 'fresh'
            {
            match("fresh"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:27:7: ( 'xor' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:27:9: 'xor'
            {
            match("xor"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:28:7: ( '=' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:28:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:29:7: ( '!=' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:29:9: '!='
            {
            match("!="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:30:7: ( '<' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:30:9: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:31:7: ( '<=' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:31:9: '<='
            {
            match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:32:7: ( '>' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:32:9: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:33:7: ( '>=' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:33:9: '>='
            {
            match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:34:7: ( '-' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:34:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:35:7: ( '*' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:35:9: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:36:7: ( '/' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:36:9: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:37:7: ( 'mod' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:37:9: 'mod'
            {
            match("mod"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:38:7: ( 'rem' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:38:9: 'rem'
            {
            match("rem"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:39:7: ( '**' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:39:9: '**'
            {
            match("**"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "T__43"
    public final void mT__43() throws RecognitionException {
        try {
            int _type = T__43;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:40:7: ( 'abs' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:40:9: 'abs'
            {
            match("abs"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__43"

    // $ANTLR start "T__44"
    public final void mT__44() throws RecognitionException {
        try {
            int _type = T__44;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:41:7: ( 'not' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:41:9: 'not'
            {
            match("not"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__44"

    // $ANTLR start "T__45"
    public final void mT__45() throws RecognitionException {
        try {
            int _type = T__45;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:42:7: ( 'true' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:42:9: 'true'
            {
            match("true"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__45"

    // $ANTLR start "T__46"
    public final void mT__46() throws RecognitionException {
        try {
            int _type = T__46;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:43:7: ( 'false' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:43:9: 'false'
            {
            match("false"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__46"

    // $ANTLR start "T__47"
    public final void mT__47() throws RecognitionException {
        try {
            int _type = T__47;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:44:7: ( 'ps' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:44:9: 'ps'
            {
            match("ps"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__47"

    // $ANTLR start "T__48"
    public final void mT__48() throws RecognitionException {
        try {
            int _type = T__48;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:45:7: ( 'ns' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:45:9: 'ns'
            {
            match("ns"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__48"

    // $ANTLR start "T__49"
    public final void mT__49() throws RecognitionException {
        try {
            int _type = T__49;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:46:7: ( 'us' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:46:9: 'us'
            {
            match("us"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__49"

    // $ANTLR start "T__50"
    public final void mT__50() throws RecognitionException {
        try {
            int _type = T__50;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:47:7: ( 'ms' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:47:9: 'ms'
            {
            match("ms"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__50"

    // $ANTLR start "T__51"
    public final void mT__51() throws RecognitionException {
        try {
            int _type = T__51;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:48:7: ( 'sec' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:48:9: 'sec'
            {
            match("sec"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__51"

    // $ANTLR start "T__52"
    public final void mT__52() throws RecognitionException {
        try {
            int _type = T__52;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:49:7: ( 'min' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:49:9: 'min'
            {
            match("min"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__52"

    // $ANTLR start "T__53"
    public final void mT__53() throws RecognitionException {
        try {
            int _type = T__53;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:50:7: ( 'hr' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:50:9: 'hr'
            {
            match("hr"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__53"

    // $ANTLR start "T__54"
    public final void mT__54() throws RecognitionException {
        try {
            int _type = T__54;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:51:7: ( 'on dispatch' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:51:9: 'on dispatch'
            {
            match("on dispatch"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__54"

    // $ANTLR start "T__55"
    public final void mT__55() throws RecognitionException {
        try {
            int _type = T__55;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:52:7: ( 'stop' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:52:9: 'stop'
            {
            match("stop"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__55"

    // $ANTLR start "RULE_DIGIT"
    public final void mRULE_DIGIT() throws RecognitionException {
        try {
            int _type = RULE_DIGIT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6114:12: ( '0' .. '9' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6114:14: '0' .. '9'
            {
            matchRange('0','9'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DIGIT"

    // $ANTLR start "RULE_EXTENDED_DIGIT"
    public final void mRULE_EXTENDED_DIGIT() throws RecognitionException {
        try {
            int _type = RULE_EXTENDED_DIGIT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6116:21: ( ( RULE_DIGIT | 'A' .. 'F' | 'a' .. 'f' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6116:23: ( RULE_DIGIT | 'A' .. 'F' | 'a' .. 'f' )
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='F')||(input.LA(1)>='a' && input.LA(1)<='f') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_EXTENDED_DIGIT"

    // $ANTLR start "RULE_STRING_LITERAL"
    public final void mRULE_STRING_LITERAL() throws RecognitionException {
        try {
            int _type = RULE_STRING_LITERAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6118:21: ( ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '.' | RULE_INT )* )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6118:23: ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '.' | RULE_INT )*
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6118:43: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '.' | RULE_INT )*
            loop1:
            do {
                int alt1=6;
                switch ( input.LA(1) ) {
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                    {
                    alt1=1;
                    }
                    break;
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                    {
                    alt1=2;
                    }
                    break;
                case '_':
                    {
                    alt1=3;
                    }
                    break;
                case '.':
                    {
                    alt1=4;
                    }
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    {
                    alt1=5;
                    }
                    break;

                }

                switch (alt1) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6118:44: 'a' .. 'z'
            	    {
            	    matchRange('a','z'); 

            	    }
            	    break;
            	case 2 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6118:53: 'A' .. 'Z'
            	    {
            	    matchRange('A','Z'); 

            	    }
            	    break;
            	case 3 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6118:62: '_'
            	    {
            	    match('_'); 

            	    }
            	    break;
            	case 4 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6118:66: '.'
            	    {
            	    match('.'); 

            	    }
            	    break;
            	case 5 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6118:70: RULE_INT
            	    {
            	    mRULE_INT(); 

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING_LITERAL"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6120:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6120:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6120:11: ( '^' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='^') ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6120:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6120:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>='0' && LA3_0<='9')||(LA3_0>='A' && LA3_0<='Z')||LA3_0=='_'||(LA3_0>='a' && LA3_0<='z')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6122:10: ( ( '0' .. '9' )+ )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6122:12: ( '0' .. '9' )+
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6122:12: ( '0' .. '9' )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>='0' && LA4_0<='9')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6122:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6124:13: ( ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6124:15: ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6124:15: ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0=='\"') ) {
                alt7=1;
            }
            else if ( (LA7_0=='\'') ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6124:16: '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6124:20: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop5:
                    do {
                        int alt5=3;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0=='\\') ) {
                            alt5=1;
                        }
                        else if ( ((LA5_0>='\u0000' && LA5_0<='!')||(LA5_0>='#' && LA5_0<='[')||(LA5_0>=']' && LA5_0<='\uFFFF')) ) {
                            alt5=2;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6124:21: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' )
                    	    {
                    	    match('\\'); 
                    	    if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||(input.LA(1)>='t' && input.LA(1)<='u') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;
                    	case 2 :
                    	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6124:66: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6124:86: '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6124:91: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop6:
                    do {
                        int alt6=3;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0=='\\') ) {
                            alt6=1;
                        }
                        else if ( ((LA6_0>='\u0000' && LA6_0<='&')||(LA6_0>='(' && LA6_0<='[')||(LA6_0>=']' && LA6_0<='\uFFFF')) ) {
                            alt6=2;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6124:92: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' )
                    	    {
                    	    match('\\'); 
                    	    if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||(input.LA(1)>='t' && input.LA(1)<='u') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;
                    	case 2 :
                    	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6124:137: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6126:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6126:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6126:24: ( options {greedy=false; } : . )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0=='*') ) {
                    int LA8_1 = input.LA(2);

                    if ( (LA8_1=='/') ) {
                        alt8=2;
                    }
                    else if ( ((LA8_1>='\u0000' && LA8_1<='.')||(LA8_1>='0' && LA8_1<='\uFFFF')) ) {
                        alt8=1;
                    }


                }
                else if ( ((LA8_0>='\u0000' && LA8_0<=')')||(LA8_0>='+' && LA8_0<='\uFFFF')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6126:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6128:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6128:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6128:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>='\u0000' && LA9_0<='\t')||(LA9_0>='\u000B' && LA9_0<='\f')||(LA9_0>='\u000E' && LA9_0<='\uFFFF')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6128:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6128:40: ( ( '\\r' )? '\\n' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0=='\n'||LA11_0=='\r') ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6128:41: ( '\\r' )? '\\n'
                    {
                    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6128:41: ( '\\r' )?
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0=='\r') ) {
                        alt10=1;
                    }
                    switch (alt10) {
                        case 1 :
                            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6128:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6130:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6130:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6130:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt12=0;
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>='\t' && LA12_0<='\n')||LA12_0=='\r'||LA12_0==' ') ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt12 >= 1 ) break loop12;
                        EarlyExitException eee =
                            new EarlyExitException(12, input);
                        throw eee;
                }
                cnt12++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6132:16: ( . )
            // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:6132:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:8: ( T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | RULE_DIGIT | RULE_EXTENDED_DIGIT | RULE_STRING_LITERAL | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt13=52;
        alt13 = dfa13.predict(input);
        switch (alt13) {
            case 1 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:10: T__14
                {
                mT__14(); 

                }
                break;
            case 2 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:16: T__15
                {
                mT__15(); 

                }
                break;
            case 3 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:22: T__16
                {
                mT__16(); 

                }
                break;
            case 4 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:28: T__17
                {
                mT__17(); 

                }
                break;
            case 5 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:34: T__18
                {
                mT__18(); 

                }
                break;
            case 6 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:40: T__19
                {
                mT__19(); 

                }
                break;
            case 7 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:46: T__20
                {
                mT__20(); 

                }
                break;
            case 8 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:52: T__21
                {
                mT__21(); 

                }
                break;
            case 9 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:58: T__22
                {
                mT__22(); 

                }
                break;
            case 10 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:64: T__23
                {
                mT__23(); 

                }
                break;
            case 11 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:70: T__24
                {
                mT__24(); 

                }
                break;
            case 12 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:76: T__25
                {
                mT__25(); 

                }
                break;
            case 13 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:82: T__26
                {
                mT__26(); 

                }
                break;
            case 14 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:88: T__27
                {
                mT__27(); 

                }
                break;
            case 15 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:94: T__28
                {
                mT__28(); 

                }
                break;
            case 16 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:100: T__29
                {
                mT__29(); 

                }
                break;
            case 17 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:106: T__30
                {
                mT__30(); 

                }
                break;
            case 18 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:112: T__31
                {
                mT__31(); 

                }
                break;
            case 19 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:118: T__32
                {
                mT__32(); 

                }
                break;
            case 20 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:124: T__33
                {
                mT__33(); 

                }
                break;
            case 21 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:130: T__34
                {
                mT__34(); 

                }
                break;
            case 22 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:136: T__35
                {
                mT__35(); 

                }
                break;
            case 23 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:142: T__36
                {
                mT__36(); 

                }
                break;
            case 24 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:148: T__37
                {
                mT__37(); 

                }
                break;
            case 25 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:154: T__38
                {
                mT__38(); 

                }
                break;
            case 26 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:160: T__39
                {
                mT__39(); 

                }
                break;
            case 27 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:166: T__40
                {
                mT__40(); 

                }
                break;
            case 28 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:172: T__41
                {
                mT__41(); 

                }
                break;
            case 29 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:178: T__42
                {
                mT__42(); 

                }
                break;
            case 30 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:184: T__43
                {
                mT__43(); 

                }
                break;
            case 31 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:190: T__44
                {
                mT__44(); 

                }
                break;
            case 32 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:196: T__45
                {
                mT__45(); 

                }
                break;
            case 33 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:202: T__46
                {
                mT__46(); 

                }
                break;
            case 34 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:208: T__47
                {
                mT__47(); 

                }
                break;
            case 35 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:214: T__48
                {
                mT__48(); 

                }
                break;
            case 36 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:220: T__49
                {
                mT__49(); 

                }
                break;
            case 37 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:226: T__50
                {
                mT__50(); 

                }
                break;
            case 38 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:232: T__51
                {
                mT__51(); 

                }
                break;
            case 39 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:238: T__52
                {
                mT__52(); 

                }
                break;
            case 40 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:244: T__53
                {
                mT__53(); 

                }
                break;
            case 41 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:250: T__54
                {
                mT__54(); 

                }
                break;
            case 42 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:256: T__55
                {
                mT__55(); 

                }
                break;
            case 43 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:262: RULE_DIGIT
                {
                mRULE_DIGIT(); 

                }
                break;
            case 44 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:273: RULE_EXTENDED_DIGIT
                {
                mRULE_EXTENDED_DIGIT(); 

                }
                break;
            case 45 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:293: RULE_STRING_LITERAL
                {
                mRULE_STRING_LITERAL(); 

                }
                break;
            case 46 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:313: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 47 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:321: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 48 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:330: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 49 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:342: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 50 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:358: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 51 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:374: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 52 :
                // ../org.topcased.adele.xtext.baguards.ui/src-gen/org/topcased/adele/xtext/baguards/ui/contentassist/antlr/internal/InternalBaGuards.g:1:382: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA13 dfa13 = new DFA13(this);
    static final String DFA13_eotS =
        "\1\uffff\1\44\1\uffff\1\47\1\53\2\uffff\1\64\1\57\1\64\1\uffff"+
        "\1\57\1\uffff\1\64\1\57\1\uffff\1\44\1\103\1\105\1\uffff\1\110\1"+
        "\113\7\57\1\127\1\64\1\57\3\44\7\uffff\1\57\1\uffff\3\57\3\uffff"+
        "\2\57\1\uffff\1\136\4\57\1\uffff\2\57\1\uffff\2\57\14\uffff\1\57"+
        "\1\150\3\57\1\154\1\155\1\156\2\57\1\161\4\uffff\3\57\1\uffff\1"+
        "\57\1\uffff\1\166\1\167\3\57\1\173\1\174\1\uffff\1\175\1\176\1\177"+
        "\3\uffff\1\u0080\1\57\1\uffff\4\57\2\uffff\1\57\1\u0087\1\57\6\uffff"+
        "\1\u0089\1\57\1\u008b\1\u008c\2\57\1\uffff\1\u008f\1\uffff\1\u0090"+
        "\2\uffff\2\57\2\uffff\1\57\1\u0094\1\57\1\uffff\1\u0096\1\uffff";
    static final String DFA13_eofS =
        "\u0097\uffff";
    static final String DFA13_minS =
        "\1\0\1\72\1\uffff\1\60\1\40\2\uffff\1\56\1\60\1\56\1\uffff\1\60"+
        "\1\uffff\1\56\1\60\1\uffff\3\75\1\uffff\2\52\10\60\1\56\1\60\1\101"+
        "\2\0\7\uffff\1\60\1\uffff\3\60\3\uffff\2\60\1\uffff\1\56\1\60\1"+
        "\40\2\60\1\uffff\2\60\1\uffff\2\60\14\uffff\1\60\1\56\3\60\3\56"+
        "\2\60\1\56\4\uffff\3\60\1\uffff\1\60\1\uffff\2\56\3\60\2\56\1\uffff"+
        "\3\56\3\uffff\1\56\1\60\1\uffff\4\60\2\uffff\1\60\1\56\1\60\6\uffff"+
        "\1\56\1\60\2\56\2\60\1\uffff\1\56\1\uffff\1\56\2\uffff\2\60\2\uffff"+
        "\1\60\1\56\1\60\1\uffff\1\56\1\uffff";
    static final String DFA13_maxS =
        "\1\uffff\1\72\1\uffff\2\172\2\uffff\3\172\1\uffff\1\172\1\uffff"+
        "\2\172\1\uffff\3\75\1\uffff\1\52\1\57\7\172\1\71\3\172\2\uffff\7"+
        "\uffff\1\172\1\uffff\3\172\3\uffff\2\172\1\uffff\5\172\1\uffff\2"+
        "\172\1\uffff\2\172\14\uffff\13\172\4\uffff\3\172\1\uffff\1\172\1"+
        "\uffff\7\172\1\uffff\3\172\3\uffff\2\172\1\uffff\4\172\2\uffff\3"+
        "\172\6\uffff\6\172\1\uffff\1\172\1\uffff\1\172\2\uffff\2\172\2\uffff"+
        "\3\172\1\uffff\1\172\1\uffff";
    static final String DFA13_acceptS =
        "\2\uffff\1\2\2\uffff\1\5\1\7\3\uffff\1\13\1\uffff\1\16\2\uffff"+
        "\1\22\3\uffff\1\30\17\uffff\1\63\1\64\1\1\1\2\1\3\1\56\1\6\1\uffff"+
        "\1\4\3\uffff\1\55\1\5\1\7\2\uffff\1\54\5\uffff\1\13\2\uffff\1\16"+
        "\2\uffff\1\22\1\23\1\25\1\24\1\27\1\26\1\30\1\35\1\31\1\61\1\62"+
        "\1\32\13\uffff\1\53\1\57\1\60\1\63\3\uffff\1\11\1\uffff\1\51\7\uffff"+
        "\1\45\3\uffff\1\43\1\42\1\44\2\uffff\1\50\4\uffff\1\12\1\36\3\uffff"+
        "\1\21\1\33\1\47\1\34\1\37\1\46\6\uffff\1\40\1\uffff\1\52\1\uffff"+
        "\1\20\1\41\2\uffff\1\17\1\10\3\uffff\1\15\1\uffff\1\14";
    static final String DFA13_specialS =
        "\1\1\40\uffff\1\2\1\0\164\uffff}>";
    static final String[] DFA13_transitionS = {
            "\11\44\2\43\2\44\1\43\22\44\1\43\1\20\1\41\1\6\3\44\1\42\2"+
            "\44\1\24\1\5\1\12\1\23\1\2\1\25\12\35\1\1\1\44\1\21\1\17\1\22"+
            "\1\14\1\44\4\36\1\4\1\36\24\37\3\44\1\40\1\3\1\44\1\11\1\36"+
            "\1\15\2\36\1\7\1\37\1\34\4\37\1\26\1\30\1\10\1\31\1\37\1\27"+
            "\1\33\1\13\1\32\2\37\1\16\2\37\uff85\44",
            "\1\45",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\51\15\uffff\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1"+
            "\55\1\uffff\32\52",
            "",
            "",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\1\63"+
            "\20\52\1\62\10\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\15\52\1\67\3\52"+
            "\1\65\1\52\1\66\6\52",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\1\52"+
            "\1\71\13\52\1\70\14\52",
            "",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\10\52\1\73\10\52"+
            "\1\74\10\52",
            "",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\16"+
            "\52\1\76\13\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\16\52\1\77\13\52",
            "",
            "\1\101",
            "\1\102",
            "\1\104",
            "",
            "\1\107",
            "\1\111\4\uffff\1\112",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\10\52\1\116\5\52"+
            "\1\114\3\52\1\115\7\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\4\52\1\117\25\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\16\52\1\120\3\52"+
            "\1\121\7\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\22\52\1\122\7\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\22\52\1\123\7\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\4\52\1\124\16\52"+
            "\1\125\6\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\21\52\1\126\10\52",
            "\12\130",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32\52",
            "\32\50\4\uffff\1\50\1\uffff\32\50",
            "\0\131",
            "\0\131",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32\52",
            "",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32\52",
            "",
            "",
            "",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\4\52\1\134\11\52"+
            "\1\133\13\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\13\52\1\135\16\52",
            "",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\7\52\1\137\22\52",
            "\1\140\17\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\3\52\1\141\26\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\22\52\1\142\7\52",
            "",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\14\52\1\143\15\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\24\52\1\144\5\52",
            "",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\24\52\1\145\5\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\21\52\1\146\10\52",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\3\52\1\147\26\52",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\15\52\1\151\14\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\14\52\1\152\15\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\23\52\1\153\6\52",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\2\52\1\157\27\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\16\52\1\160\13\52",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "",
            "",
            "",
            "",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\31\52\1\162",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\22\52\1\163\7\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\22\52\1\164\7\52",
            "",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\4\52\1\165\25\52",
            "",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\4\52\1\170\25\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\4\52\1\171\25\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\15\52\1\172\14\52",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "",
            "",
            "",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\17\52\1\u0081\12"+
            "\52",
            "",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\4\52\1\u0082\25"+
            "\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\7\52\1\u0083\22"+
            "\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\4\52\1\u0084\25"+
            "\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\21\52\1\u0085\10"+
            "\52",
            "",
            "",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\16\52\1\u0086\13"+
            "\52",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\23\52\1\u0088\6"+
            "\52",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\15\52\1\u008a\14"+
            "\52",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\26\52\1\u008d\3"+
            "\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\24\52\1\u008e\5"+
            "\52",
            "",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "",
            "",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\10\52\1\u0091\21"+
            "\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\23\52\1\u0092\6"+
            "\52",
            "",
            "",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\22\52\1\u0093\7"+
            "\52",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            "\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\4\52\1\u0095\25"+
            "\52",
            "",
            "\1\57\1\uffff\12\56\7\uffff\32\54\4\uffff\1\55\1\uffff\32"+
            "\52",
            ""
    };

    static final short[] DFA13_eot = DFA.unpackEncodedString(DFA13_eotS);
    static final short[] DFA13_eof = DFA.unpackEncodedString(DFA13_eofS);
    static final char[] DFA13_min = DFA.unpackEncodedStringToUnsignedChars(DFA13_minS);
    static final char[] DFA13_max = DFA.unpackEncodedStringToUnsignedChars(DFA13_maxS);
    static final short[] DFA13_accept = DFA.unpackEncodedString(DFA13_acceptS);
    static final short[] DFA13_special = DFA.unpackEncodedString(DFA13_specialS);
    static final short[][] DFA13_transition;

    static {
        int numStates = DFA13_transitionS.length;
        DFA13_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA13_transition[i] = DFA.unpackEncodedString(DFA13_transitionS[i]);
        }
    }

    class DFA13 extends DFA {

        public DFA13(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 13;
            this.eot = DFA13_eot;
            this.eof = DFA13_eof;
            this.min = DFA13_min;
            this.max = DFA13_max;
            this.accept = DFA13_accept;
            this.special = DFA13_special;
            this.transition = DFA13_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | RULE_DIGIT | RULE_EXTENDED_DIGIT | RULE_STRING_LITERAL | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA13_34 = input.LA(1);

                        s = -1;
                        if ( ((LA13_34>='\u0000' && LA13_34<='\uFFFF')) ) {s = 89;}

                        else s = 36;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA13_0 = input.LA(1);

                        s = -1;
                        if ( (LA13_0==':') ) {s = 1;}

                        else if ( (LA13_0=='.') ) {s = 2;}

                        else if ( (LA13_0=='_') ) {s = 3;}

                        else if ( (LA13_0=='E') ) {s = 4;}

                        else if ( (LA13_0=='+') ) {s = 5;}

                        else if ( (LA13_0=='#') ) {s = 6;}

                        else if ( (LA13_0=='f') ) {s = 7;}

                        else if ( (LA13_0=='o') ) {s = 8;}

                        else if ( (LA13_0=='a') ) {s = 9;}

                        else if ( (LA13_0==',') ) {s = 10;}

                        else if ( (LA13_0=='t') ) {s = 11;}

                        else if ( (LA13_0=='?') ) {s = 12;}

                        else if ( (LA13_0=='c') ) {s = 13;}

                        else if ( (LA13_0=='x') ) {s = 14;}

                        else if ( (LA13_0=='=') ) {s = 15;}

                        else if ( (LA13_0=='!') ) {s = 16;}

                        else if ( (LA13_0=='<') ) {s = 17;}

                        else if ( (LA13_0=='>') ) {s = 18;}

                        else if ( (LA13_0=='-') ) {s = 19;}

                        else if ( (LA13_0=='*') ) {s = 20;}

                        else if ( (LA13_0=='/') ) {s = 21;}

                        else if ( (LA13_0=='m') ) {s = 22;}

                        else if ( (LA13_0=='r') ) {s = 23;}

                        else if ( (LA13_0=='n') ) {s = 24;}

                        else if ( (LA13_0=='p') ) {s = 25;}

                        else if ( (LA13_0=='u') ) {s = 26;}

                        else if ( (LA13_0=='s') ) {s = 27;}

                        else if ( (LA13_0=='h') ) {s = 28;}

                        else if ( ((LA13_0>='0' && LA13_0<='9')) ) {s = 29;}

                        else if ( ((LA13_0>='A' && LA13_0<='D')||LA13_0=='F'||LA13_0=='b'||(LA13_0>='d' && LA13_0<='e')) ) {s = 30;}

                        else if ( ((LA13_0>='G' && LA13_0<='Z')||LA13_0=='g'||(LA13_0>='i' && LA13_0<='l')||LA13_0=='q'||(LA13_0>='v' && LA13_0<='w')||(LA13_0>='y' && LA13_0<='z')) ) {s = 31;}

                        else if ( (LA13_0=='^') ) {s = 32;}

                        else if ( (LA13_0=='\"') ) {s = 33;}

                        else if ( (LA13_0=='\'') ) {s = 34;}

                        else if ( ((LA13_0>='\t' && LA13_0<='\n')||LA13_0=='\r'||LA13_0==' ') ) {s = 35;}

                        else if ( ((LA13_0>='\u0000' && LA13_0<='\b')||(LA13_0>='\u000B' && LA13_0<='\f')||(LA13_0>='\u000E' && LA13_0<='\u001F')||(LA13_0>='$' && LA13_0<='&')||(LA13_0>='(' && LA13_0<=')')||LA13_0==';'||LA13_0=='@'||(LA13_0>='[' && LA13_0<=']')||LA13_0=='`'||(LA13_0>='{' && LA13_0<='\uFFFF')) ) {s = 36;}

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA13_33 = input.LA(1);

                        s = -1;
                        if ( ((LA13_33>='\u0000' && LA13_33<='\uFFFF')) ) {s = 89;}

                        else s = 36;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 13, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}