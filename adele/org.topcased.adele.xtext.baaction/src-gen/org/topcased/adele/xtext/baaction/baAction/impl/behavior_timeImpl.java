/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.behavior_time;
import org.topcased.adele.xtext.baaction.baAction.integer_value;
import org.topcased.adele.xtext.baaction.baAction.unit_identifier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>behavior time</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_timeImpl#getBtname1 <em>Btname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_timeImpl#getBtname2 <em>Btname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class behavior_timeImpl extends MinimalEObjectImpl.Container implements behavior_time
{
  /**
   * The cached value of the '{@link #getBtname1() <em>Btname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBtname1()
   * @generated
   * @ordered
   */
  protected integer_value btname1;

  /**
   * The cached value of the '{@link #getBtname2() <em>Btname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBtname2()
   * @generated
   * @ordered
   */
  protected unit_identifier btname2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected behavior_timeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.BEHAVIOR_TIME;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public integer_value getBtname1()
  {
    return btname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBtname1(integer_value newBtname1, NotificationChain msgs)
  {
    integer_value oldBtname1 = btname1;
    btname1 = newBtname1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_TIME__BTNAME1, oldBtname1, newBtname1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBtname1(integer_value newBtname1)
  {
    if (newBtname1 != btname1)
    {
      NotificationChain msgs = null;
      if (btname1 != null)
        msgs = ((InternalEObject)btname1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_TIME__BTNAME1, null, msgs);
      if (newBtname1 != null)
        msgs = ((InternalEObject)newBtname1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_TIME__BTNAME1, null, msgs);
      msgs = basicSetBtname1(newBtname1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_TIME__BTNAME1, newBtname1, newBtname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public unit_identifier getBtname2()
  {
    return btname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBtname2(unit_identifier newBtname2, NotificationChain msgs)
  {
    unit_identifier oldBtname2 = btname2;
    btname2 = newBtname2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_TIME__BTNAME2, oldBtname2, newBtname2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBtname2(unit_identifier newBtname2)
  {
    if (newBtname2 != btname2)
    {
      NotificationChain msgs = null;
      if (btname2 != null)
        msgs = ((InternalEObject)btname2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_TIME__BTNAME2, null, msgs);
      if (newBtname2 != null)
        msgs = ((InternalEObject)newBtname2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_TIME__BTNAME2, null, msgs);
      msgs = basicSetBtname2(newBtname2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_TIME__BTNAME2, newBtname2, newBtname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_TIME__BTNAME1:
        return basicSetBtname1(null, msgs);
      case BaActionPackage.BEHAVIOR_TIME__BTNAME2:
        return basicSetBtname2(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_TIME__BTNAME1:
        return getBtname1();
      case BaActionPackage.BEHAVIOR_TIME__BTNAME2:
        return getBtname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_TIME__BTNAME1:
        setBtname1((integer_value)newValue);
        return;
      case BaActionPackage.BEHAVIOR_TIME__BTNAME2:
        setBtname2((unit_identifier)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_TIME__BTNAME1:
        setBtname1((integer_value)null);
        return;
      case BaActionPackage.BEHAVIOR_TIME__BTNAME2:
        setBtname2((unit_identifier)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_TIME__BTNAME1:
        return btname1 != null;
      case BaActionPackage.BEHAVIOR_TIME__BTNAME2:
        return btname2 != null;
    }
    return super.eIsSet(featureID);
  }

} //behavior_timeImpl
