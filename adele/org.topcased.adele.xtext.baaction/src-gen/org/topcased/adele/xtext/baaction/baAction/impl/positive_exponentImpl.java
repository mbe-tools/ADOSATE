/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.numeral;
import org.topcased.adele.xtext.baaction.baAction.positive_exponent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>positive exponent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.positive_exponentImpl#getPename <em>Pename</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class positive_exponentImpl extends MinimalEObjectImpl.Container implements positive_exponent
{
  /**
   * The cached value of the '{@link #getPename() <em>Pename</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPename()
   * @generated
   * @ordered
   */
  protected numeral pename;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected positive_exponentImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.POSITIVE_EXPONENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public numeral getPename()
  {
    return pename;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPename(numeral newPename, NotificationChain msgs)
  {
    numeral oldPename = pename;
    pename = newPename;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.POSITIVE_EXPONENT__PENAME, oldPename, newPename);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPename(numeral newPename)
  {
    if (newPename != pename)
    {
      NotificationChain msgs = null;
      if (pename != null)
        msgs = ((InternalEObject)pename).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.POSITIVE_EXPONENT__PENAME, null, msgs);
      if (newPename != null)
        msgs = ((InternalEObject)newPename).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.POSITIVE_EXPONENT__PENAME, null, msgs);
      msgs = basicSetPename(newPename, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.POSITIVE_EXPONENT__PENAME, newPename, newPename));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.POSITIVE_EXPONENT__PENAME:
        return basicSetPename(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.POSITIVE_EXPONENT__PENAME:
        return getPename();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.POSITIVE_EXPONENT__PENAME:
        setPename((numeral)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.POSITIVE_EXPONENT__PENAME:
        setPename((numeral)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.POSITIVE_EXPONENT__PENAME:
        return pename != null;
    }
    return super.eIsSet(featureID);
  }

} //positive_exponentImpl
