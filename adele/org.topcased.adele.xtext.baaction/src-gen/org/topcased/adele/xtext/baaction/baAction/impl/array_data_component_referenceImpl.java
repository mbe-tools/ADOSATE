/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.array_data_component_reference;
import org.topcased.adele.xtext.baaction.baAction.data_component_reference;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>array data component reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.array_data_component_referenceImpl#getAdcr1 <em>Adcr1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.array_data_component_referenceImpl#getAdcr2 <em>Adcr2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.array_data_component_referenceImpl#getAdcr3 <em>Adcr3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.array_data_component_referenceImpl#getAdcr4 <em>Adcr4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.array_data_component_referenceImpl#getAdcr5 <em>Adcr5</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class array_data_component_referenceImpl extends MinimalEObjectImpl.Container implements array_data_component_reference
{
  /**
   * The default value of the '{@link #getAdcr1() <em>Adcr1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAdcr1()
   * @generated
   * @ordered
   */
  protected static final String ADCR1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAdcr1() <em>Adcr1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAdcr1()
   * @generated
   * @ordered
   */
  protected String adcr1 = ADCR1_EDEFAULT;

  /**
   * The cached value of the '{@link #getAdcr2() <em>Adcr2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAdcr2()
   * @generated
   * @ordered
   */
  protected data_component_reference adcr2;

  /**
   * The cached value of the '{@link #getAdcr3() <em>Adcr3</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAdcr3()
   * @generated
   * @ordered
   */
  protected EList<String> adcr3;

  /**
   * The cached value of the '{@link #getAdcr4() <em>Adcr4</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAdcr4()
   * @generated
   * @ordered
   */
  protected EList<data_component_reference> adcr4;

  /**
   * The default value of the '{@link #getAdcr5() <em>Adcr5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAdcr5()
   * @generated
   * @ordered
   */
  protected static final String ADCR5_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAdcr5() <em>Adcr5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAdcr5()
   * @generated
   * @ordered
   */
  protected String adcr5 = ADCR5_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected array_data_component_referenceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.ARRAY_DATA_COMPONENT_REFERENCE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAdcr1()
  {
    return adcr1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAdcr1(String newAdcr1)
  {
    String oldAdcr1 = adcr1;
    adcr1 = newAdcr1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR1, oldAdcr1, adcr1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public data_component_reference getAdcr2()
  {
    return adcr2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAdcr2(data_component_reference newAdcr2, NotificationChain msgs)
  {
    data_component_reference oldAdcr2 = adcr2;
    adcr2 = newAdcr2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR2, oldAdcr2, newAdcr2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAdcr2(data_component_reference newAdcr2)
  {
    if (newAdcr2 != adcr2)
    {
      NotificationChain msgs = null;
      if (adcr2 != null)
        msgs = ((InternalEObject)adcr2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR2, null, msgs);
      if (newAdcr2 != null)
        msgs = ((InternalEObject)newAdcr2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR2, null, msgs);
      msgs = basicSetAdcr2(newAdcr2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR2, newAdcr2, newAdcr2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getAdcr3()
  {
    if (adcr3 == null)
    {
      adcr3 = new EDataTypeEList<String>(String.class, this, BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR3);
    }
    return adcr3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<data_component_reference> getAdcr4()
  {
    if (adcr4 == null)
    {
      adcr4 = new EObjectContainmentEList<data_component_reference>(data_component_reference.class, this, BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR4);
    }
    return adcr4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAdcr5()
  {
    return adcr5;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAdcr5(String newAdcr5)
  {
    String oldAdcr5 = adcr5;
    adcr5 = newAdcr5;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR5, oldAdcr5, adcr5));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR2:
        return basicSetAdcr2(null, msgs);
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR4:
        return ((InternalEList<?>)getAdcr4()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR1:
        return getAdcr1();
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR2:
        return getAdcr2();
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR3:
        return getAdcr3();
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR4:
        return getAdcr4();
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR5:
        return getAdcr5();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR1:
        setAdcr1((String)newValue);
        return;
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR2:
        setAdcr2((data_component_reference)newValue);
        return;
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR3:
        getAdcr3().clear();
        getAdcr3().addAll((Collection<? extends String>)newValue);
        return;
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR4:
        getAdcr4().clear();
        getAdcr4().addAll((Collection<? extends data_component_reference>)newValue);
        return;
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR5:
        setAdcr5((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR1:
        setAdcr1(ADCR1_EDEFAULT);
        return;
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR2:
        setAdcr2((data_component_reference)null);
        return;
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR3:
        getAdcr3().clear();
        return;
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR4:
        getAdcr4().clear();
        return;
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR5:
        setAdcr5(ADCR5_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR1:
        return ADCR1_EDEFAULT == null ? adcr1 != null : !ADCR1_EDEFAULT.equals(adcr1);
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR2:
        return adcr2 != null;
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR3:
        return adcr3 != null && !adcr3.isEmpty();
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR4:
        return adcr4 != null && !adcr4.isEmpty();
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE__ADCR5:
        return ADCR5_EDEFAULT == null ? adcr5 != null : !ADCR5_EDEFAULT.equals(adcr5);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (adcr1: ");
    result.append(adcr1);
    result.append(", adcr3: ");
    result.append(adcr3);
    result.append(", adcr5: ");
    result.append(adcr5);
    result.append(')');
    return result.toString();
  }

} //array_data_component_referenceImpl
