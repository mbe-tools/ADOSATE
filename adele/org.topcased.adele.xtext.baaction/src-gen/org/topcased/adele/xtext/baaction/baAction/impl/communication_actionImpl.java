/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.communication_action;
import org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>communication action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa1 <em>Ca1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa2 <em>Ca2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa3 <em>Ca3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa4 <em>Ca4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa5 <em>Ca5</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa21 <em>Ca21</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa22 <em>Ca22</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa23 <em>Ca23</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa24 <em>Ca24</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa25 <em>Ca25</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa26 <em>Ca26</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa27 <em>Ca27</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa28 <em>Ca28</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa29 <em>Ca29</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa30 <em>Ca30</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa31 <em>Ca31</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa32 <em>Ca32</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl#getCa33 <em>Ca33</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class communication_actionImpl extends MinimalEObjectImpl.Container implements communication_action
{
  /**
   * The default value of the '{@link #getCa1() <em>Ca1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa1()
   * @generated
   * @ordered
   */
  protected static final String CA1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa1() <em>Ca1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa1()
   * @generated
   * @ordered
   */
  protected String ca1 = CA1_EDEFAULT;

  /**
   * The default value of the '{@link #getCa2() <em>Ca2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa2()
   * @generated
   * @ordered
   */
  protected static final String CA2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa2() <em>Ca2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa2()
   * @generated
   * @ordered
   */
  protected String ca2 = CA2_EDEFAULT;

  /**
   * The default value of the '{@link #getCa3() <em>Ca3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa3()
   * @generated
   * @ordered
   */
  protected static final String CA3_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa3() <em>Ca3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa3()
   * @generated
   * @ordered
   */
  protected String ca3 = CA3_EDEFAULT;

  /**
   * The cached value of the '{@link #getCa4() <em>Ca4</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa4()
   * @generated
   * @ordered
   */
  protected subprogram_parameter_list ca4;

  /**
   * The default value of the '{@link #getCa5() <em>Ca5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa5()
   * @generated
   * @ordered
   */
  protected static final String CA5_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa5() <em>Ca5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa5()
   * @generated
   * @ordered
   */
  protected String ca5 = CA5_EDEFAULT;

  /**
   * The default value of the '{@link #getCa21() <em>Ca21</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa21()
   * @generated
   * @ordered
   */
  protected static final String CA21_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa21() <em>Ca21</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa21()
   * @generated
   * @ordered
   */
  protected String ca21 = CA21_EDEFAULT;

  /**
   * The default value of the '{@link #getCa22() <em>Ca22</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa22()
   * @generated
   * @ordered
   */
  protected static final String CA22_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa22() <em>Ca22</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa22()
   * @generated
   * @ordered
   */
  protected String ca22 = CA22_EDEFAULT;

  /**
   * The default value of the '{@link #getCa23() <em>Ca23</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa23()
   * @generated
   * @ordered
   */
  protected static final String CA23_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa23() <em>Ca23</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa23()
   * @generated
   * @ordered
   */
  protected String ca23 = CA23_EDEFAULT;

  /**
   * The default value of the '{@link #getCa24() <em>Ca24</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa24()
   * @generated
   * @ordered
   */
  protected static final String CA24_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa24() <em>Ca24</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa24()
   * @generated
   * @ordered
   */
  protected String ca24 = CA24_EDEFAULT;

  /**
   * The default value of the '{@link #getCa25() <em>Ca25</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa25()
   * @generated
   * @ordered
   */
  protected static final String CA25_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa25() <em>Ca25</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa25()
   * @generated
   * @ordered
   */
  protected String ca25 = CA25_EDEFAULT;

  /**
   * The default value of the '{@link #getCa26() <em>Ca26</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa26()
   * @generated
   * @ordered
   */
  protected static final String CA26_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa26() <em>Ca26</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa26()
   * @generated
   * @ordered
   */
  protected String ca26 = CA26_EDEFAULT;

  /**
   * The default value of the '{@link #getCa27() <em>Ca27</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa27()
   * @generated
   * @ordered
   */
  protected static final String CA27_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa27() <em>Ca27</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa27()
   * @generated
   * @ordered
   */
  protected String ca27 = CA27_EDEFAULT;

  /**
   * The default value of the '{@link #getCa28() <em>Ca28</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa28()
   * @generated
   * @ordered
   */
  protected static final String CA28_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa28() <em>Ca28</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa28()
   * @generated
   * @ordered
   */
  protected String ca28 = CA28_EDEFAULT;

  /**
   * The default value of the '{@link #getCa29() <em>Ca29</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa29()
   * @generated
   * @ordered
   */
  protected static final String CA29_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa29() <em>Ca29</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa29()
   * @generated
   * @ordered
   */
  protected String ca29 = CA29_EDEFAULT;

  /**
   * The default value of the '{@link #getCa30() <em>Ca30</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa30()
   * @generated
   * @ordered
   */
  protected static final String CA30_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa30() <em>Ca30</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa30()
   * @generated
   * @ordered
   */
  protected String ca30 = CA30_EDEFAULT;

  /**
   * The default value of the '{@link #getCa31() <em>Ca31</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa31()
   * @generated
   * @ordered
   */
  protected static final String CA31_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa31() <em>Ca31</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa31()
   * @generated
   * @ordered
   */
  protected String ca31 = CA31_EDEFAULT;

  /**
   * The default value of the '{@link #getCa32() <em>Ca32</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa32()
   * @generated
   * @ordered
   */
  protected static final String CA32_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa32() <em>Ca32</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa32()
   * @generated
   * @ordered
   */
  protected String ca32 = CA32_EDEFAULT;

  /**
   * The default value of the '{@link #getCa33() <em>Ca33</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa33()
   * @generated
   * @ordered
   */
  protected static final String CA33_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCa33() <em>Ca33</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCa33()
   * @generated
   * @ordered
   */
  protected String ca33 = CA33_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected communication_actionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.COMMUNICATION_ACTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa1()
  {
    return ca1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa1(String newCa1)
  {
    String oldCa1 = ca1;
    ca1 = newCa1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA1, oldCa1, ca1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa2()
  {
    return ca2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa2(String newCa2)
  {
    String oldCa2 = ca2;
    ca2 = newCa2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA2, oldCa2, ca2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa3()
  {
    return ca3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa3(String newCa3)
  {
    String oldCa3 = ca3;
    ca3 = newCa3;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA3, oldCa3, ca3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public subprogram_parameter_list getCa4()
  {
    return ca4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCa4(subprogram_parameter_list newCa4, NotificationChain msgs)
  {
    subprogram_parameter_list oldCa4 = ca4;
    ca4 = newCa4;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA4, oldCa4, newCa4);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa4(subprogram_parameter_list newCa4)
  {
    if (newCa4 != ca4)
    {
      NotificationChain msgs = null;
      if (ca4 != null)
        msgs = ((InternalEObject)ca4).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.COMMUNICATION_ACTION__CA4, null, msgs);
      if (newCa4 != null)
        msgs = ((InternalEObject)newCa4).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.COMMUNICATION_ACTION__CA4, null, msgs);
      msgs = basicSetCa4(newCa4, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA4, newCa4, newCa4));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa5()
  {
    return ca5;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa5(String newCa5)
  {
    String oldCa5 = ca5;
    ca5 = newCa5;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA5, oldCa5, ca5));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa21()
  {
    return ca21;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa21(String newCa21)
  {
    String oldCa21 = ca21;
    ca21 = newCa21;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA21, oldCa21, ca21));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa22()
  {
    return ca22;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa22(String newCa22)
  {
    String oldCa22 = ca22;
    ca22 = newCa22;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA22, oldCa22, ca22));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa23()
  {
    return ca23;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa23(String newCa23)
  {
    String oldCa23 = ca23;
    ca23 = newCa23;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA23, oldCa23, ca23));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa24()
  {
    return ca24;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa24(String newCa24)
  {
    String oldCa24 = ca24;
    ca24 = newCa24;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA24, oldCa24, ca24));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa25()
  {
    return ca25;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa25(String newCa25)
  {
    String oldCa25 = ca25;
    ca25 = newCa25;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA25, oldCa25, ca25));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa26()
  {
    return ca26;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa26(String newCa26)
  {
    String oldCa26 = ca26;
    ca26 = newCa26;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA26, oldCa26, ca26));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa27()
  {
    return ca27;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa27(String newCa27)
  {
    String oldCa27 = ca27;
    ca27 = newCa27;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA27, oldCa27, ca27));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa28()
  {
    return ca28;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa28(String newCa28)
  {
    String oldCa28 = ca28;
    ca28 = newCa28;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA28, oldCa28, ca28));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa29()
  {
    return ca29;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa29(String newCa29)
  {
    String oldCa29 = ca29;
    ca29 = newCa29;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA29, oldCa29, ca29));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa30()
  {
    return ca30;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa30(String newCa30)
  {
    String oldCa30 = ca30;
    ca30 = newCa30;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA30, oldCa30, ca30));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa31()
  {
    return ca31;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa31(String newCa31)
  {
    String oldCa31 = ca31;
    ca31 = newCa31;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA31, oldCa31, ca31));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa32()
  {
    return ca32;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa32(String newCa32)
  {
    String oldCa32 = ca32;
    ca32 = newCa32;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA32, oldCa32, ca32));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCa33()
  {
    return ca33;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCa33(String newCa33)
  {
    String oldCa33 = ca33;
    ca33 = newCa33;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.COMMUNICATION_ACTION__CA33, oldCa33, ca33));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.COMMUNICATION_ACTION__CA4:
        return basicSetCa4(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.COMMUNICATION_ACTION__CA1:
        return getCa1();
      case BaActionPackage.COMMUNICATION_ACTION__CA2:
        return getCa2();
      case BaActionPackage.COMMUNICATION_ACTION__CA3:
        return getCa3();
      case BaActionPackage.COMMUNICATION_ACTION__CA4:
        return getCa4();
      case BaActionPackage.COMMUNICATION_ACTION__CA5:
        return getCa5();
      case BaActionPackage.COMMUNICATION_ACTION__CA21:
        return getCa21();
      case BaActionPackage.COMMUNICATION_ACTION__CA22:
        return getCa22();
      case BaActionPackage.COMMUNICATION_ACTION__CA23:
        return getCa23();
      case BaActionPackage.COMMUNICATION_ACTION__CA24:
        return getCa24();
      case BaActionPackage.COMMUNICATION_ACTION__CA25:
        return getCa25();
      case BaActionPackage.COMMUNICATION_ACTION__CA26:
        return getCa26();
      case BaActionPackage.COMMUNICATION_ACTION__CA27:
        return getCa27();
      case BaActionPackage.COMMUNICATION_ACTION__CA28:
        return getCa28();
      case BaActionPackage.COMMUNICATION_ACTION__CA29:
        return getCa29();
      case BaActionPackage.COMMUNICATION_ACTION__CA30:
        return getCa30();
      case BaActionPackage.COMMUNICATION_ACTION__CA31:
        return getCa31();
      case BaActionPackage.COMMUNICATION_ACTION__CA32:
        return getCa32();
      case BaActionPackage.COMMUNICATION_ACTION__CA33:
        return getCa33();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.COMMUNICATION_ACTION__CA1:
        setCa1((String)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA2:
        setCa2((String)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA3:
        setCa3((String)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA4:
        setCa4((subprogram_parameter_list)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA5:
        setCa5((String)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA21:
        setCa21((String)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA22:
        setCa22((String)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA23:
        setCa23((String)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA24:
        setCa24((String)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA25:
        setCa25((String)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA26:
        setCa26((String)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA27:
        setCa27((String)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA28:
        setCa28((String)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA29:
        setCa29((String)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA30:
        setCa30((String)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA31:
        setCa31((String)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA32:
        setCa32((String)newValue);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA33:
        setCa33((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.COMMUNICATION_ACTION__CA1:
        setCa1(CA1_EDEFAULT);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA2:
        setCa2(CA2_EDEFAULT);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA3:
        setCa3(CA3_EDEFAULT);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA4:
        setCa4((subprogram_parameter_list)null);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA5:
        setCa5(CA5_EDEFAULT);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA21:
        setCa21(CA21_EDEFAULT);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA22:
        setCa22(CA22_EDEFAULT);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA23:
        setCa23(CA23_EDEFAULT);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA24:
        setCa24(CA24_EDEFAULT);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA25:
        setCa25(CA25_EDEFAULT);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA26:
        setCa26(CA26_EDEFAULT);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA27:
        setCa27(CA27_EDEFAULT);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA28:
        setCa28(CA28_EDEFAULT);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA29:
        setCa29(CA29_EDEFAULT);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA30:
        setCa30(CA30_EDEFAULT);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA31:
        setCa31(CA31_EDEFAULT);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA32:
        setCa32(CA32_EDEFAULT);
        return;
      case BaActionPackage.COMMUNICATION_ACTION__CA33:
        setCa33(CA33_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.COMMUNICATION_ACTION__CA1:
        return CA1_EDEFAULT == null ? ca1 != null : !CA1_EDEFAULT.equals(ca1);
      case BaActionPackage.COMMUNICATION_ACTION__CA2:
        return CA2_EDEFAULT == null ? ca2 != null : !CA2_EDEFAULT.equals(ca2);
      case BaActionPackage.COMMUNICATION_ACTION__CA3:
        return CA3_EDEFAULT == null ? ca3 != null : !CA3_EDEFAULT.equals(ca3);
      case BaActionPackage.COMMUNICATION_ACTION__CA4:
        return ca4 != null;
      case BaActionPackage.COMMUNICATION_ACTION__CA5:
        return CA5_EDEFAULT == null ? ca5 != null : !CA5_EDEFAULT.equals(ca5);
      case BaActionPackage.COMMUNICATION_ACTION__CA21:
        return CA21_EDEFAULT == null ? ca21 != null : !CA21_EDEFAULT.equals(ca21);
      case BaActionPackage.COMMUNICATION_ACTION__CA22:
        return CA22_EDEFAULT == null ? ca22 != null : !CA22_EDEFAULT.equals(ca22);
      case BaActionPackage.COMMUNICATION_ACTION__CA23:
        return CA23_EDEFAULT == null ? ca23 != null : !CA23_EDEFAULT.equals(ca23);
      case BaActionPackage.COMMUNICATION_ACTION__CA24:
        return CA24_EDEFAULT == null ? ca24 != null : !CA24_EDEFAULT.equals(ca24);
      case BaActionPackage.COMMUNICATION_ACTION__CA25:
        return CA25_EDEFAULT == null ? ca25 != null : !CA25_EDEFAULT.equals(ca25);
      case BaActionPackage.COMMUNICATION_ACTION__CA26:
        return CA26_EDEFAULT == null ? ca26 != null : !CA26_EDEFAULT.equals(ca26);
      case BaActionPackage.COMMUNICATION_ACTION__CA27:
        return CA27_EDEFAULT == null ? ca27 != null : !CA27_EDEFAULT.equals(ca27);
      case BaActionPackage.COMMUNICATION_ACTION__CA28:
        return CA28_EDEFAULT == null ? ca28 != null : !CA28_EDEFAULT.equals(ca28);
      case BaActionPackage.COMMUNICATION_ACTION__CA29:
        return CA29_EDEFAULT == null ? ca29 != null : !CA29_EDEFAULT.equals(ca29);
      case BaActionPackage.COMMUNICATION_ACTION__CA30:
        return CA30_EDEFAULT == null ? ca30 != null : !CA30_EDEFAULT.equals(ca30);
      case BaActionPackage.COMMUNICATION_ACTION__CA31:
        return CA31_EDEFAULT == null ? ca31 != null : !CA31_EDEFAULT.equals(ca31);
      case BaActionPackage.COMMUNICATION_ACTION__CA32:
        return CA32_EDEFAULT == null ? ca32 != null : !CA32_EDEFAULT.equals(ca32);
      case BaActionPackage.COMMUNICATION_ACTION__CA33:
        return CA33_EDEFAULT == null ? ca33 != null : !CA33_EDEFAULT.equals(ca33);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (ca1: ");
    result.append(ca1);
    result.append(", ca2: ");
    result.append(ca2);
    result.append(", ca3: ");
    result.append(ca3);
    result.append(", ca5: ");
    result.append(ca5);
    result.append(", ca21: ");
    result.append(ca21);
    result.append(", ca22: ");
    result.append(ca22);
    result.append(", ca23: ");
    result.append(ca23);
    result.append(", ca24: ");
    result.append(ca24);
    result.append(", ca25: ");
    result.append(ca25);
    result.append(", ca26: ");
    result.append(ca26);
    result.append(", ca27: ");
    result.append(ca27);
    result.append(", ca28: ");
    result.append(ca28);
    result.append(", ca29: ");
    result.append(ca29);
    result.append(", ca30: ");
    result.append(ca30);
    result.append(", ca31: ");
    result.append(ca31);
    result.append(", ca32: ");
    result.append(ca32);
    result.append(", ca33: ");
    result.append(ca33);
    result.append(')');
    return result.toString();
  }

} //communication_actionImpl
