/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.behavior_time;
import org.topcased.adele.xtext.baaction.baAction.timed_action;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>timed action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.timed_actionImpl#getTa1 <em>Ta1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.timed_actionImpl#getTa1bis <em>Ta1bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.timed_actionImpl#getTa2 <em>Ta2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.timed_actionImpl#getTa3 <em>Ta3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.timed_actionImpl#getTa4 <em>Ta4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.timed_actionImpl#getTa5 <em>Ta5</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class timed_actionImpl extends MinimalEObjectImpl.Container implements timed_action
{
  /**
   * The default value of the '{@link #getTa1() <em>Ta1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTa1()
   * @generated
   * @ordered
   */
  protected static final String TA1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getTa1() <em>Ta1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTa1()
   * @generated
   * @ordered
   */
  protected String ta1 = TA1_EDEFAULT;

  /**
   * The default value of the '{@link #getTa1bis() <em>Ta1bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTa1bis()
   * @generated
   * @ordered
   */
  protected static final String TA1BIS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getTa1bis() <em>Ta1bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTa1bis()
   * @generated
   * @ordered
   */
  protected String ta1bis = TA1BIS_EDEFAULT;

  /**
   * The cached value of the '{@link #getTa2() <em>Ta2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTa2()
   * @generated
   * @ordered
   */
  protected behavior_time ta2;

  /**
   * The default value of the '{@link #getTa3() <em>Ta3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTa3()
   * @generated
   * @ordered
   */
  protected static final String TA3_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getTa3() <em>Ta3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTa3()
   * @generated
   * @ordered
   */
  protected String ta3 = TA3_EDEFAULT;

  /**
   * The cached value of the '{@link #getTa4() <em>Ta4</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTa4()
   * @generated
   * @ordered
   */
  protected behavior_time ta4;

  /**
   * The default value of the '{@link #getTa5() <em>Ta5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTa5()
   * @generated
   * @ordered
   */
  protected static final String TA5_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getTa5() <em>Ta5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTa5()
   * @generated
   * @ordered
   */
  protected String ta5 = TA5_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected timed_actionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.TIMED_ACTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getTa1()
  {
    return ta1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTa1(String newTa1)
  {
    String oldTa1 = ta1;
    ta1 = newTa1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.TIMED_ACTION__TA1, oldTa1, ta1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getTa1bis()
  {
    return ta1bis;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTa1bis(String newTa1bis)
  {
    String oldTa1bis = ta1bis;
    ta1bis = newTa1bis;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.TIMED_ACTION__TA1BIS, oldTa1bis, ta1bis));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_time getTa2()
  {
    return ta2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTa2(behavior_time newTa2, NotificationChain msgs)
  {
    behavior_time oldTa2 = ta2;
    ta2 = newTa2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.TIMED_ACTION__TA2, oldTa2, newTa2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTa2(behavior_time newTa2)
  {
    if (newTa2 != ta2)
    {
      NotificationChain msgs = null;
      if (ta2 != null)
        msgs = ((InternalEObject)ta2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.TIMED_ACTION__TA2, null, msgs);
      if (newTa2 != null)
        msgs = ((InternalEObject)newTa2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.TIMED_ACTION__TA2, null, msgs);
      msgs = basicSetTa2(newTa2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.TIMED_ACTION__TA2, newTa2, newTa2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getTa3()
  {
    return ta3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTa3(String newTa3)
  {
    String oldTa3 = ta3;
    ta3 = newTa3;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.TIMED_ACTION__TA3, oldTa3, ta3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_time getTa4()
  {
    return ta4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTa4(behavior_time newTa4, NotificationChain msgs)
  {
    behavior_time oldTa4 = ta4;
    ta4 = newTa4;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.TIMED_ACTION__TA4, oldTa4, newTa4);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTa4(behavior_time newTa4)
  {
    if (newTa4 != ta4)
    {
      NotificationChain msgs = null;
      if (ta4 != null)
        msgs = ((InternalEObject)ta4).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.TIMED_ACTION__TA4, null, msgs);
      if (newTa4 != null)
        msgs = ((InternalEObject)newTa4).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.TIMED_ACTION__TA4, null, msgs);
      msgs = basicSetTa4(newTa4, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.TIMED_ACTION__TA4, newTa4, newTa4));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getTa5()
  {
    return ta5;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTa5(String newTa5)
  {
    String oldTa5 = ta5;
    ta5 = newTa5;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.TIMED_ACTION__TA5, oldTa5, ta5));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.TIMED_ACTION__TA2:
        return basicSetTa2(null, msgs);
      case BaActionPackage.TIMED_ACTION__TA4:
        return basicSetTa4(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.TIMED_ACTION__TA1:
        return getTa1();
      case BaActionPackage.TIMED_ACTION__TA1BIS:
        return getTa1bis();
      case BaActionPackage.TIMED_ACTION__TA2:
        return getTa2();
      case BaActionPackage.TIMED_ACTION__TA3:
        return getTa3();
      case BaActionPackage.TIMED_ACTION__TA4:
        return getTa4();
      case BaActionPackage.TIMED_ACTION__TA5:
        return getTa5();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.TIMED_ACTION__TA1:
        setTa1((String)newValue);
        return;
      case BaActionPackage.TIMED_ACTION__TA1BIS:
        setTa1bis((String)newValue);
        return;
      case BaActionPackage.TIMED_ACTION__TA2:
        setTa2((behavior_time)newValue);
        return;
      case BaActionPackage.TIMED_ACTION__TA3:
        setTa3((String)newValue);
        return;
      case BaActionPackage.TIMED_ACTION__TA4:
        setTa4((behavior_time)newValue);
        return;
      case BaActionPackage.TIMED_ACTION__TA5:
        setTa5((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.TIMED_ACTION__TA1:
        setTa1(TA1_EDEFAULT);
        return;
      case BaActionPackage.TIMED_ACTION__TA1BIS:
        setTa1bis(TA1BIS_EDEFAULT);
        return;
      case BaActionPackage.TIMED_ACTION__TA2:
        setTa2((behavior_time)null);
        return;
      case BaActionPackage.TIMED_ACTION__TA3:
        setTa3(TA3_EDEFAULT);
        return;
      case BaActionPackage.TIMED_ACTION__TA4:
        setTa4((behavior_time)null);
        return;
      case BaActionPackage.TIMED_ACTION__TA5:
        setTa5(TA5_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.TIMED_ACTION__TA1:
        return TA1_EDEFAULT == null ? ta1 != null : !TA1_EDEFAULT.equals(ta1);
      case BaActionPackage.TIMED_ACTION__TA1BIS:
        return TA1BIS_EDEFAULT == null ? ta1bis != null : !TA1BIS_EDEFAULT.equals(ta1bis);
      case BaActionPackage.TIMED_ACTION__TA2:
        return ta2 != null;
      case BaActionPackage.TIMED_ACTION__TA3:
        return TA3_EDEFAULT == null ? ta3 != null : !TA3_EDEFAULT.equals(ta3);
      case BaActionPackage.TIMED_ACTION__TA4:
        return ta4 != null;
      case BaActionPackage.TIMED_ACTION__TA5:
        return TA5_EDEFAULT == null ? ta5 != null : !TA5_EDEFAULT.equals(ta5);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (ta1: ");
    result.append(ta1);
    result.append(", ta1bis: ");
    result.append(ta1bis);
    result.append(", ta3: ");
    result.append(ta3);
    result.append(", ta5: ");
    result.append(ta5);
    result.append(')');
    return result.toString();
  }

} //timed_actionImpl
