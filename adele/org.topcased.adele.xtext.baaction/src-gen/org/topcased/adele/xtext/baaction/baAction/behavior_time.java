/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>behavior time</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_time#getBtname1 <em>Btname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_time#getBtname2 <em>Btname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_time()
 * @model
 * @generated
 */
public interface behavior_time extends EObject
{
  /**
   * Returns the value of the '<em><b>Btname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Btname1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Btname1</em>' containment reference.
   * @see #setBtname1(integer_value)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_time_Btname1()
   * @model containment="true"
   * @generated
   */
  integer_value getBtname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_time#getBtname1 <em>Btname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Btname1</em>' containment reference.
   * @see #getBtname1()
   * @generated
   */
  void setBtname1(integer_value value);

  /**
   * Returns the value of the '<em><b>Btname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Btname2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Btname2</em>' containment reference.
   * @see #setBtname2(unit_identifier)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_time_Btname2()
   * @model containment="true"
   * @generated
   */
  unit_identifier getBtname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_time#getBtname2 <em>Btname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Btname2</em>' containment reference.
   * @see #getBtname2()
   * @generated
   */
  void setBtname2(unit_identifier value);

} // behavior_time
