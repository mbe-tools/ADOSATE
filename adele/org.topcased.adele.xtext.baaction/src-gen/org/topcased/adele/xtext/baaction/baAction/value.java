/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.value#getVname1 <em>Vname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.value#getVname2 <em>Vname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getvalue()
 * @model
 * @generated
 */
public interface value extends EObject
{
  /**
   * Returns the value of the '<em><b>Vname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Vname1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Vname1</em>' containment reference.
   * @see #setVname1(value_variable)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getvalue_Vname1()
   * @model containment="true"
   * @generated
   */
  value_variable getVname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.value#getVname1 <em>Vname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Vname1</em>' containment reference.
   * @see #getVname1()
   * @generated
   */
  void setVname1(value_variable value);

  /**
   * Returns the value of the '<em><b>Vname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Vname2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Vname2</em>' containment reference.
   * @see #setVname2(value_constant)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getvalue_Vname2()
   * @model containment="true"
   * @generated
   */
  value_constant getVname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.value#getVname2 <em>Vname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Vname2</em>' containment reference.
   * @see #getVname2()
   * @generated
   */
  void setVname2(value_constant value);

} // value
