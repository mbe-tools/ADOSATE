/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.data_component_reference;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>data component reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.data_component_referenceImpl#getDcr1 <em>Dcr1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.data_component_referenceImpl#getDcr2 <em>Dcr2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.data_component_referenceImpl#getDcr3 <em>Dcr3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class data_component_referenceImpl extends MinimalEObjectImpl.Container implements data_component_reference
{
  /**
   * The default value of the '{@link #getDcr1() <em>Dcr1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDcr1()
   * @generated
   * @ordered
   */
  protected static final String DCR1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDcr1() <em>Dcr1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDcr1()
   * @generated
   * @ordered
   */
  protected String dcr1 = DCR1_EDEFAULT;

  /**
   * The cached value of the '{@link #getDcr2() <em>Dcr2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDcr2()
   * @generated
   * @ordered
   */
  protected EList<String> dcr2;

  /**
   * The cached value of the '{@link #getDcr3() <em>Dcr3</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDcr3()
   * @generated
   * @ordered
   */
  protected EList<String> dcr3;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected data_component_referenceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.DATA_COMPONENT_REFERENCE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDcr1()
  {
    return dcr1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDcr1(String newDcr1)
  {
    String oldDcr1 = dcr1;
    dcr1 = newDcr1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.DATA_COMPONENT_REFERENCE__DCR1, oldDcr1, dcr1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getDcr2()
  {
    if (dcr2 == null)
    {
      dcr2 = new EDataTypeEList<String>(String.class, this, BaActionPackage.DATA_COMPONENT_REFERENCE__DCR2);
    }
    return dcr2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getDcr3()
  {
    if (dcr3 == null)
    {
      dcr3 = new EDataTypeEList<String>(String.class, this, BaActionPackage.DATA_COMPONENT_REFERENCE__DCR3);
    }
    return dcr3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.DATA_COMPONENT_REFERENCE__DCR1:
        return getDcr1();
      case BaActionPackage.DATA_COMPONENT_REFERENCE__DCR2:
        return getDcr2();
      case BaActionPackage.DATA_COMPONENT_REFERENCE__DCR3:
        return getDcr3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.DATA_COMPONENT_REFERENCE__DCR1:
        setDcr1((String)newValue);
        return;
      case BaActionPackage.DATA_COMPONENT_REFERENCE__DCR2:
        getDcr2().clear();
        getDcr2().addAll((Collection<? extends String>)newValue);
        return;
      case BaActionPackage.DATA_COMPONENT_REFERENCE__DCR3:
        getDcr3().clear();
        getDcr3().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.DATA_COMPONENT_REFERENCE__DCR1:
        setDcr1(DCR1_EDEFAULT);
        return;
      case BaActionPackage.DATA_COMPONENT_REFERENCE__DCR2:
        getDcr2().clear();
        return;
      case BaActionPackage.DATA_COMPONENT_REFERENCE__DCR3:
        getDcr3().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.DATA_COMPONENT_REFERENCE__DCR1:
        return DCR1_EDEFAULT == null ? dcr1 != null : !DCR1_EDEFAULT.equals(dcr1);
      case BaActionPackage.DATA_COMPONENT_REFERENCE__DCR2:
        return dcr2 != null && !dcr2.isEmpty();
      case BaActionPackage.DATA_COMPONENT_REFERENCE__DCR3:
        return dcr3 != null && !dcr3.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (dcr1: ");
    result.append(dcr1);
    result.append(", dcr2: ");
    result.append(dcr2);
    result.append(", dcr3: ");
    result.append(dcr3);
    result.append(')');
    return result.toString();
  }

} //data_component_referenceImpl
