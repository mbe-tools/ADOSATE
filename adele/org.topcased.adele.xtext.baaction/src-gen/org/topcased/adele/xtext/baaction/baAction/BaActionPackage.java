/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionFactory
 * @model kind="package"
 * @generated
 */
public interface BaActionPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "baAction";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.topcased.org/adele/xtext/baaction/BaAction";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "baAction";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  BaActionPackage eINSTANCE = org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl.init();

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_blockImpl <em>behavior action block</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_blockImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbehavior_action_block()
   * @generated
   */
  int BEHAVIOR_ACTION_BLOCK = 0;

  /**
   * The feature id for the '<em><b>Bac1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_BLOCK__BAC1 = 0;

  /**
   * The feature id for the '<em><b>Bac2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_BLOCK__BAC2 = 1;

  /**
   * The feature id for the '<em><b>Bac3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_BLOCK__BAC3 = 2;

  /**
   * The feature id for the '<em><b>Bac4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_BLOCK__BAC4 = 3;

  /**
   * The feature id for the '<em><b>Bac5</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_BLOCK__BAC5 = 4;

  /**
   * The number of structural features of the '<em>behavior action block</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_BLOCK_FEATURE_COUNT = 5;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl <em>behavior action</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbehavior_action()
   * @generated
   */
  int BEHAVIOR_ACTION = 1;

  /**
   * The feature id for the '<em><b>Ba1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA1 = 0;

  /**
   * The feature id for the '<em><b>Ba2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA2 = 1;

  /**
   * The feature id for the '<em><b>Ba3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA3 = 2;

  /**
   * The feature id for the '<em><b>Ba3bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA3BIS = 3;

  /**
   * The feature id for the '<em><b>Ba4</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA4 = 4;

  /**
   * The feature id for the '<em><b>Ba5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA5 = 5;

  /**
   * The feature id for the '<em><b>Ba6</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA6 = 6;

  /**
   * The feature id for the '<em><b>Ba7</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA7 = 7;

  /**
   * The feature id for the '<em><b>Ba7bis</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA7BIS = 8;

  /**
   * The feature id for the '<em><b>Ba8</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA8 = 9;

  /**
   * The feature id for the '<em><b>Ba9</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA9 = 10;

  /**
   * The feature id for the '<em><b>Ba10</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA10 = 11;

  /**
   * The feature id for the '<em><b>Ba11</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA11 = 12;

  /**
   * The feature id for the '<em><b>Ba12</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA12 = 13;

  /**
   * The feature id for the '<em><b>Ba13</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA13 = 14;

  /**
   * The feature id for the '<em><b>Ba13bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA13BIS = 15;

  /**
   * The feature id for the '<em><b>Ba14</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA14 = 16;

  /**
   * The feature id for the '<em><b>Ba14bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA14BIS = 17;

  /**
   * The feature id for the '<em><b>Ba15</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA15 = 18;

  /**
   * The feature id for the '<em><b>Ba16</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA16 = 19;

  /**
   * The feature id for the '<em><b>Ba17</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA17 = 20;

  /**
   * The feature id for the '<em><b>Ba18</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA18 = 21;

  /**
   * The feature id for the '<em><b>Ba19</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA19 = 22;

  /**
   * The feature id for the '<em><b>Ba20</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA20 = 23;

  /**
   * The feature id for the '<em><b>Ba20bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA20BIS = 24;

  /**
   * The feature id for the '<em><b>Ba21</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA21 = 25;

  /**
   * The feature id for the '<em><b>Ba22</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA22 = 26;

  /**
   * The feature id for the '<em><b>Ba23</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA23 = 27;

  /**
   * The feature id for the '<em><b>Ba23bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA23BIS = 28;

  /**
   * The feature id for the '<em><b>Ba24</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA24 = 29;

  /**
   * The feature id for the '<em><b>Ba25</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA25 = 30;

  /**
   * The feature id for the '<em><b>Ba26</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA26 = 31;

  /**
   * The feature id for the '<em><b>Ba27</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA27 = 32;

  /**
   * The feature id for the '<em><b>Ba29</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA29 = 33;

  /**
   * The feature id for the '<em><b>Ba30</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA30 = 34;

  /**
   * The feature id for the '<em><b>Ba30bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA30BIS = 35;

  /**
   * The feature id for the '<em><b>Ba31</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA31 = 36;

  /**
   * The feature id for the '<em><b>Ba32</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA32 = 37;

  /**
   * The feature id for the '<em><b>Ba33</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA33 = 38;

  /**
   * The feature id for the '<em><b>Ba33bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA33BIS = 39;

  /**
   * The feature id for the '<em><b>Ba34</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA34 = 40;

  /**
   * The feature id for the '<em><b>Ba35</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA35 = 41;

  /**
   * The feature id for the '<em><b>Ba35bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA35BIS = 42;

  /**
   * The feature id for the '<em><b>Ba36</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA36 = 43;

  /**
   * The feature id for the '<em><b>Ba37</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA37 = 44;

  /**
   * The feature id for the '<em><b>Ba38</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA38 = 45;

  /**
   * The feature id for the '<em><b>Ba39</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA39 = 46;

  /**
   * The feature id for the '<em><b>Ba40</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA40 = 47;

  /**
   * The feature id for the '<em><b>Ba40bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA40BIS = 48;

  /**
   * The feature id for the '<em><b>Ba41</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA41 = 49;

  /**
   * The feature id for the '<em><b>Ba42</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION__BA42 = 50;

  /**
   * The number of structural features of the '<em>behavior action</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_FEATURE_COUNT = 51;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionsImpl <em>behavior actions</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionsImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbehavior_actions()
   * @generated
   */
  int BEHAVIOR_ACTIONS = 2;

  /**
   * The feature id for the '<em><b>Bas1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTIONS__BAS1 = 0;

  /**
   * The feature id for the '<em><b>Baseq2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTIONS__BASEQ2 = 1;

  /**
   * The feature id for the '<em><b>Baseq4</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTIONS__BASEQ4 = 2;

  /**
   * The feature id for the '<em><b>Baseq3</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTIONS__BASEQ3 = 3;

  /**
   * The number of structural features of the '<em>behavior actions</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTIONS_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_sequenceImpl <em>behavior action sequence</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_sequenceImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbehavior_action_sequence()
   * @generated
   */
  int BEHAVIOR_ACTION_SEQUENCE = 3;

  /**
   * The feature id for the '<em><b>Baseq1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_SEQUENCE__BASEQ1 = 0;

  /**
   * The feature id for the '<em><b>Baseq2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_SEQUENCE__BASEQ2 = 1;

  /**
   * The feature id for the '<em><b>Baseq3</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_SEQUENCE__BASEQ3 = 2;

  /**
   * The number of structural features of the '<em>behavior action sequence</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_SEQUENCE_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_setImpl <em>behavior action set</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_setImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbehavior_action_set()
   * @generated
   */
  int BEHAVIOR_ACTION_SET = 4;

  /**
   * The feature id for the '<em><b>Baset1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_SET__BASET1 = 0;

  /**
   * The feature id for the '<em><b>Baset2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_SET__BASET2 = 1;

  /**
   * The feature id for the '<em><b>Baset3</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_SET__BASET3 = 2;

  /**
   * The number of structural features of the '<em>behavior action set</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_ACTION_SET_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.element_valuesImpl <em>element values</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.element_valuesImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getelement_values()
   * @generated
   */
  int ELEMENT_VALUES = 5;

  /**
   * The feature id for the '<em><b>Ev1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELEMENT_VALUES__EV1 = 0;

  /**
   * The feature id for the '<em><b>Ev2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELEMENT_VALUES__EV2 = 1;

  /**
   * The feature id for the '<em><b>Ev3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELEMENT_VALUES__EV3 = 2;

  /**
   * The number of structural features of the '<em>element values</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELEMENT_VALUES_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.array_data_component_referenceImpl <em>array data component reference</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.array_data_component_referenceImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getarray_data_component_reference()
   * @generated
   */
  int ARRAY_DATA_COMPONENT_REFERENCE = 6;

  /**
   * The feature id for the '<em><b>Adcr1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_DATA_COMPONENT_REFERENCE__ADCR1 = 0;

  /**
   * The feature id for the '<em><b>Adcr2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_DATA_COMPONENT_REFERENCE__ADCR2 = 1;

  /**
   * The feature id for the '<em><b>Adcr3</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_DATA_COMPONENT_REFERENCE__ADCR3 = 2;

  /**
   * The feature id for the '<em><b>Adcr4</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_DATA_COMPONENT_REFERENCE__ADCR4 = 3;

  /**
   * The feature id for the '<em><b>Adcr5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_DATA_COMPONENT_REFERENCE__ADCR5 = 4;

  /**
   * The number of structural features of the '<em>array data component reference</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_DATA_COMPONENT_REFERENCE_FEATURE_COUNT = 5;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.basic_actionImpl <em>basic action</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.basic_actionImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbasic_action()
   * @generated
   */
  int BASIC_ACTION = 7;

  /**
   * The feature id for the '<em><b>Bact1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_ACTION__BACT1 = 0;

  /**
   * The feature id for the '<em><b>Bact2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_ACTION__BACT2 = 1;

  /**
   * The feature id for the '<em><b>Bact3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_ACTION__BACT3 = 2;

  /**
   * The number of structural features of the '<em>basic action</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_ACTION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.assignment_actionImpl <em>assignment action</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.assignment_actionImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getassignment_action()
   * @generated
   */
  int ASSIGNMENT_ACTION = 8;

  /**
   * The feature id for the '<em><b>Aa1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_ACTION__AA1 = 0;

  /**
   * The feature id for the '<em><b>Aa2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_ACTION__AA2 = 1;

  /**
   * The feature id for the '<em><b>Aa3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_ACTION__AA3 = 2;

  /**
   * The feature id for the '<em><b>Aa4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_ACTION__AA4 = 3;

  /**
   * The number of structural features of the '<em>assignment action</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_ACTION_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl <em>communication action</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getcommunication_action()
   * @generated
   */
  int COMMUNICATION_ACTION = 9;

  /**
   * The feature id for the '<em><b>Ca1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA1 = 0;

  /**
   * The feature id for the '<em><b>Ca2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA2 = 1;

  /**
   * The feature id for the '<em><b>Ca3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA3 = 2;

  /**
   * The feature id for the '<em><b>Ca4</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA4 = 3;

  /**
   * The feature id for the '<em><b>Ca5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA5 = 4;

  /**
   * The feature id for the '<em><b>Ca21</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA21 = 5;

  /**
   * The feature id for the '<em><b>Ca22</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA22 = 6;

  /**
   * The feature id for the '<em><b>Ca23</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA23 = 7;

  /**
   * The feature id for the '<em><b>Ca24</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA24 = 8;

  /**
   * The feature id for the '<em><b>Ca25</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA25 = 9;

  /**
   * The feature id for the '<em><b>Ca26</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA26 = 10;

  /**
   * The feature id for the '<em><b>Ca27</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA27 = 11;

  /**
   * The feature id for the '<em><b>Ca28</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA28 = 12;

  /**
   * The feature id for the '<em><b>Ca29</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA29 = 13;

  /**
   * The feature id for the '<em><b>Ca30</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA30 = 14;

  /**
   * The feature id for the '<em><b>Ca31</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA31 = 15;

  /**
   * The feature id for the '<em><b>Ca32</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA32 = 16;

  /**
   * The feature id for the '<em><b>Ca33</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION__CA33 = 17;

  /**
   * The number of structural features of the '<em>communication action</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMUNICATION_ACTION_FEATURE_COUNT = 18;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.timed_actionImpl <em>timed action</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.timed_actionImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#gettimed_action()
   * @generated
   */
  int TIMED_ACTION = 10;

  /**
   * The feature id for the '<em><b>Ta1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMED_ACTION__TA1 = 0;

  /**
   * The feature id for the '<em><b>Ta1bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMED_ACTION__TA1BIS = 1;

  /**
   * The feature id for the '<em><b>Ta2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMED_ACTION__TA2 = 2;

  /**
   * The feature id for the '<em><b>Ta3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMED_ACTION__TA3 = 3;

  /**
   * The feature id for the '<em><b>Ta4</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMED_ACTION__TA4 = 4;

  /**
   * The feature id for the '<em><b>Ta5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMED_ACTION__TA5 = 5;

  /**
   * The number of structural features of the '<em>timed action</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TIMED_ACTION_FEATURE_COUNT = 6;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.subprogram_parameter_listImpl <em>subprogram parameter list</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.subprogram_parameter_listImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getsubprogram_parameter_list()
   * @generated
   */
  int SUBPROGRAM_PARAMETER_LIST = 11;

  /**
   * The feature id for the '<em><b>Spl1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBPROGRAM_PARAMETER_LIST__SPL1 = 0;

  /**
   * The feature id for the '<em><b>Spl2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBPROGRAM_PARAMETER_LIST__SPL2 = 1;

  /**
   * The feature id for the '<em><b>Spl3</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBPROGRAM_PARAMETER_LIST__SPL3 = 2;

  /**
   * The number of structural features of the '<em>subprogram parameter list</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBPROGRAM_PARAMETER_LIST_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.parameter_labelImpl <em>parameter label</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.parameter_labelImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getparameter_label()
   * @generated
   */
  int PARAMETER_LABEL = 12;

  /**
   * The feature id for the '<em><b>Pl1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER_LABEL__PL1 = 0;

  /**
   * The number of structural features of the '<em>parameter label</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER_LABEL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.data_component_referenceImpl <em>data component reference</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.data_component_referenceImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getdata_component_reference()
   * @generated
   */
  int DATA_COMPONENT_REFERENCE = 13;

  /**
   * The feature id for the '<em><b>Dcr1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_COMPONENT_REFERENCE__DCR1 = 0;

  /**
   * The feature id for the '<em><b>Dcr2</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_COMPONENT_REFERENCE__DCR2 = 1;

  /**
   * The feature id for the '<em><b>Dcr3</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_COMPONENT_REFERENCE__DCR3 = 2;

  /**
   * The number of structural features of the '<em>data component reference</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_COMPONENT_REFERENCE_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.nameImpl <em>name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.nameImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getname()
   * @generated
   */
  int NAME = 14;

  /**
   * The feature id for the '<em><b>N1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAME__N1 = 0;

  /**
   * The feature id for the '<em><b>N2</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAME__N2 = 1;

  /**
   * The number of structural features of the '<em>name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAME_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.array_indexImpl <em>array index</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.array_indexImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getarray_index()
   * @generated
   */
  int ARRAY_INDEX = 15;

  /**
   * The feature id for the '<em><b>Ai1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_INDEX__AI1 = 0;

  /**
   * The feature id for the '<em><b>Ai2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_INDEX__AI2 = 1;

  /**
   * The feature id for the '<em><b>Ai3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_INDEX__AI3 = 2;

  /**
   * The number of structural features of the '<em>array index</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_INDEX_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.integer_valueImpl <em>integer value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.integer_valueImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getinteger_value()
   * @generated
   */
  int INTEGER_VALUE = 16;

  /**
   * The feature id for the '<em><b>Ivname</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_VALUE__IVNAME = 0;

  /**
   * The number of structural features of the '<em>integer value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_VALUE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.numeric_literalImpl <em>numeric literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.numeric_literalImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getnumeric_literal()
   * @generated
   */
  int NUMERIC_LITERAL = 17;

  /**
   * The feature id for the '<em><b>Nlname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERIC_LITERAL__NLNAME1 = 0;

  /**
   * The feature id for the '<em><b>Nlname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERIC_LITERAL__NLNAME2 = 1;

  /**
   * The number of structural features of the '<em>numeric literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERIC_LITERAL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.integer_literalImpl <em>integer literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.integer_literalImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getinteger_literal()
   * @generated
   */
  int INTEGER_LITERAL = 18;

  /**
   * The feature id for the '<em><b>Ilname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL__ILNAME1 = 0;

  /**
   * The feature id for the '<em><b>Ilname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL__ILNAME2 = 1;

  /**
   * The number of structural features of the '<em>integer literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.real_literalImpl <em>real literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.real_literalImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getreal_literal()
   * @generated
   */
  int REAL_LITERAL = 19;

  /**
   * The feature id for the '<em><b>Rlname</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_LITERAL__RLNAME = 0;

  /**
   * The number of structural features of the '<em>real literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REAL_LITERAL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.decimal_integer_literalImpl <em>decimal integer literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.decimal_integer_literalImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getdecimal_integer_literal()
   * @generated
   */
  int DECIMAL_INTEGER_LITERAL = 20;

  /**
   * The feature id for the '<em><b>Dilname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_INTEGER_LITERAL__DILNAME1 = 0;

  /**
   * The feature id for the '<em><b>Dilname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_INTEGER_LITERAL__DILNAME2 = 1;

  /**
   * The number of structural features of the '<em>decimal integer literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_INTEGER_LITERAL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.decimal_real_literalImpl <em>decimal real literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.decimal_real_literalImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getdecimal_real_literal()
   * @generated
   */
  int DECIMAL_REAL_LITERAL = 21;

  /**
   * The feature id for the '<em><b>Drlname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_REAL_LITERAL__DRLNAME1 = 0;

  /**
   * The feature id for the '<em><b>Drlname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_REAL_LITERAL__DRLNAME2 = 1;

  /**
   * The feature id for the '<em><b>Drlname3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_REAL_LITERAL__DRLNAME3 = 2;

  /**
   * The number of structural features of the '<em>decimal real literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_REAL_LITERAL_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.numeralImpl <em>numeral</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.numeralImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getnumeral()
   * @generated
   */
  int NUMERAL = 22;

  /**
   * The feature id for the '<em><b>Nname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERAL__NNAME1 = 0;

  /**
   * The feature id for the '<em><b>Nname2</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERAL__NNAME2 = 1;

  /**
   * The number of structural features of the '<em>numeral</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NUMERAL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.exponentImpl <em>exponent</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.exponentImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getexponent()
   * @generated
   */
  int EXPONENT = 23;

  /**
   * The feature id for the '<em><b>Ename1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPONENT__ENAME1 = 0;

  /**
   * The feature id for the '<em><b>Ename2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPONENT__ENAME2 = 1;

  /**
   * The number of structural features of the '<em>exponent</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPONENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.positive_exponentImpl <em>positive exponent</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.positive_exponentImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getpositive_exponent()
   * @generated
   */
  int POSITIVE_EXPONENT = 24;

  /**
   * The feature id for the '<em><b>Pename</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POSITIVE_EXPONENT__PENAME = 0;

  /**
   * The number of structural features of the '<em>positive exponent</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int POSITIVE_EXPONENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.based_integer_literalImpl <em>based integer literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.based_integer_literalImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbased_integer_literal()
   * @generated
   */
  int BASED_INTEGER_LITERAL = 25;

  /**
   * The feature id for the '<em><b>Bilname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASED_INTEGER_LITERAL__BILNAME1 = 0;

  /**
   * The feature id for the '<em><b>Bilname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASED_INTEGER_LITERAL__BILNAME2 = 1;

  /**
   * The feature id for the '<em><b>Bilname3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASED_INTEGER_LITERAL__BILNAME3 = 2;

  /**
   * The number of structural features of the '<em>based integer literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASED_INTEGER_LITERAL_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.baseImpl <em>base</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.baseImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbase()
   * @generated
   */
  int BASE = 26;

  /**
   * The feature id for the '<em><b>Bname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE__BNAME1 = 0;

  /**
   * The feature id for the '<em><b>Bname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE__BNAME2 = 1;

  /**
   * The number of structural features of the '<em>base</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.based_numeralImpl <em>based numeral</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.based_numeralImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbased_numeral()
   * @generated
   */
  int BASED_NUMERAL = 27;

  /**
   * The feature id for the '<em><b>Bnname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASED_NUMERAL__BNNAME1 = 0;

  /**
   * The feature id for the '<em><b>Bnname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASED_NUMERAL__BNNAME2 = 1;

  /**
   * The number of structural features of the '<em>based numeral</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASED_NUMERAL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_timeImpl <em>behavior time</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.behavior_timeImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbehavior_time()
   * @generated
   */
  int BEHAVIOR_TIME = 28;

  /**
   * The feature id for the '<em><b>Btname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_TIME__BTNAME1 = 0;

  /**
   * The feature id for the '<em><b>Btname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_TIME__BTNAME2 = 1;

  /**
   * The number of structural features of the '<em>behavior time</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BEHAVIOR_TIME_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.unit_identifierImpl <em>unit identifier</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.unit_identifierImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getunit_identifier()
   * @generated
   */
  int UNIT_IDENTIFIER = 29;

  /**
   * The feature id for the '<em><b>Uiname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNIT_IDENTIFIER__UINAME1 = 0;

  /**
   * The feature id for the '<em><b>Uiname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNIT_IDENTIFIER__UINAME2 = 1;

  /**
   * The feature id for the '<em><b>Uiname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNIT_IDENTIFIER__UINAME3 = 2;

  /**
   * The feature id for the '<em><b>Uiname4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNIT_IDENTIFIER__UINAME4 = 3;

  /**
   * The feature id for the '<em><b>Uiname5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNIT_IDENTIFIER__UINAME5 = 4;

  /**
   * The feature id for the '<em><b>Uiname6</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNIT_IDENTIFIER__UINAME6 = 5;

  /**
   * The feature id for the '<em><b>Uiname7</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNIT_IDENTIFIER__UINAME7 = 6;

  /**
   * The number of structural features of the '<em>unit identifier</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNIT_IDENTIFIER_FEATURE_COUNT = 7;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.integer_rangeImpl <em>integer range</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.integer_rangeImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getinteger_range()
   * @generated
   */
  int INTEGER_RANGE = 30;

  /**
   * The feature id for the '<em><b>Min</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_RANGE__MIN = 0;

  /**
   * The feature id for the '<em><b>Irsep</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_RANGE__IRSEP = 1;

  /**
   * The feature id for the '<em><b>Max</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_RANGE__MAX = 2;

  /**
   * The number of structural features of the '<em>integer range</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_RANGE_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.integerImpl <em>integer</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.integerImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getinteger()
   * @generated
   */
  int INTEGER = 31;

  /**
   * The feature id for the '<em><b>Value1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER__VALUE1 = 0;

  /**
   * The feature id for the '<em><b>Value2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER__VALUE2 = 1;

  /**
   * The number of structural features of the '<em>integer</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.value_expressionImpl <em>value expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.value_expressionImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getvalue_expression()
   * @generated
   */
  int VALUE_EXPRESSION = 32;

  /**
   * The feature id for the '<em><b>Vename1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_EXPRESSION__VENAME1 = 0;

  /**
   * The feature id for the '<em><b>Vename3</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_EXPRESSION__VENAME3 = 1;

  /**
   * The feature id for the '<em><b>Vename2</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_EXPRESSION__VENAME2 = 2;

  /**
   * The number of structural features of the '<em>value expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_EXPRESSION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.relationImpl <em>relation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.relationImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getrelation()
   * @generated
   */
  int RELATION = 33;

  /**
   * The feature id for the '<em><b>Rname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATION__RNAME1 = 0;

  /**
   * The feature id for the '<em><b>Rname3</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATION__RNAME3 = 1;

  /**
   * The feature id for the '<em><b>Rname2</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATION__RNAME2 = 2;

  /**
   * The number of structural features of the '<em>relation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.simple_expressionImpl <em>simple expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.simple_expressionImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getsimple_expression()
   * @generated
   */
  int SIMPLE_EXPRESSION = 34;

  /**
   * The feature id for the '<em><b>Sename2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_EXPRESSION__SENAME2 = 0;

  /**
   * The feature id for the '<em><b>Sename1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_EXPRESSION__SENAME1 = 1;

  /**
   * The feature id for the '<em><b>Sename3</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_EXPRESSION__SENAME3 = 2;

  /**
   * The feature id for the '<em><b>Sename4</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_EXPRESSION__SENAME4 = 3;

  /**
   * The number of structural features of the '<em>simple expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_EXPRESSION_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.termImpl <em>term</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.termImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getterm()
   * @generated
   */
  int TERM = 35;

  /**
   * The feature id for the '<em><b>Tname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__TNAME1 = 0;

  /**
   * The feature id for the '<em><b>Tname3</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__TNAME3 = 1;

  /**
   * The feature id for the '<em><b>Tname2</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM__TNAME2 = 2;

  /**
   * The number of structural features of the '<em>term</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.factorImpl <em>factor</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.factorImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getfactor()
   * @generated
   */
  int FACTOR = 36;

  /**
   * The feature id for the '<em><b>Fname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FACTOR__FNAME1 = 0;

  /**
   * The feature id for the '<em><b>Fname3</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FACTOR__FNAME3 = 1;

  /**
   * The feature id for the '<em><b>Fname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FACTOR__FNAME2 = 2;

  /**
   * The feature id for the '<em><b>Fname4</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FACTOR__FNAME4 = 3;

  /**
   * The feature id for the '<em><b>Fname5</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FACTOR__FNAME5 = 4;

  /**
   * The feature id for the '<em><b>Fname6</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FACTOR__FNAME6 = 5;

  /**
   * The feature id for the '<em><b>Fname7</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FACTOR__FNAME7 = 6;

  /**
   * The number of structural features of the '<em>factor</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FACTOR_FEATURE_COUNT = 7;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.valueImpl <em>value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.valueImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getvalue()
   * @generated
   */
  int VALUE = 37;

  /**
   * The feature id for the '<em><b>Vname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE__VNAME1 = 0;

  /**
   * The feature id for the '<em><b>Vname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE__VNAME2 = 1;

  /**
   * The number of structural features of the '<em>value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.value_variableImpl <em>value variable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.value_variableImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getvalue_variable()
   * @generated
   */
  int VALUE_VARIABLE = 38;

  /**
   * The feature id for the '<em><b>Vvname0</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_VARIABLE__VVNAME0 = 0;

  /**
   * The feature id for the '<em><b>Vvname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_VARIABLE__VVNAME1 = 1;

  /**
   * The feature id for the '<em><b>Vvname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_VARIABLE__VVNAME2 = 2;

  /**
   * The feature id for the '<em><b>Vvname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_VARIABLE__VVNAME3 = 3;

  /**
   * The number of structural features of the '<em>value variable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_VARIABLE_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.value_constantImpl <em>value constant</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.value_constantImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getvalue_constant()
   * @generated
   */
  int VALUE_CONSTANT = 39;

  /**
   * The feature id for the '<em><b>Vcname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_CONSTANT__VCNAME1 = 0;

  /**
   * The feature id for the '<em><b>Vcname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_CONSTANT__VCNAME2 = 1;

  /**
   * The feature id for the '<em><b>Vcname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_CONSTANT__VCNAME3 = 2;

  /**
   * The feature id for the '<em><b>Vcname4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_CONSTANT__VCNAME4 = 3;

  /**
   * The number of structural features of the '<em>value constant</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_CONSTANT_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.logical_operatorImpl <em>logical operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.logical_operatorImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getlogical_operator()
   * @generated
   */
  int LOGICAL_OPERATOR = 40;

  /**
   * The feature id for the '<em><b>Loname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGICAL_OPERATOR__LONAME1 = 0;

  /**
   * The feature id for the '<em><b>Loname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGICAL_OPERATOR__LONAME2 = 1;

  /**
   * The feature id for the '<em><b>Loname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGICAL_OPERATOR__LONAME3 = 2;

  /**
   * The number of structural features of the '<em>logical operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOGICAL_OPERATOR_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.relational_operatorImpl <em>relational operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.relational_operatorImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getrelational_operator()
   * @generated
   */
  int RELATIONAL_OPERATOR = 41;

  /**
   * The feature id for the '<em><b>Roname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_OPERATOR__RONAME1 = 0;

  /**
   * The feature id for the '<em><b>Roname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_OPERATOR__RONAME2 = 1;

  /**
   * The feature id for the '<em><b>Roname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_OPERATOR__RONAME3 = 2;

  /**
   * The feature id for the '<em><b>Roname4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_OPERATOR__RONAME4 = 3;

  /**
   * The feature id for the '<em><b>Roname5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_OPERATOR__RONAME5 = 4;

  /**
   * The feature id for the '<em><b>Roname6</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_OPERATOR__RONAME6 = 5;

  /**
   * The number of structural features of the '<em>relational operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONAL_OPERATOR_FEATURE_COUNT = 6;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.binary_adding_operatorImpl <em>binary adding operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.binary_adding_operatorImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbinary_adding_operator()
   * @generated
   */
  int BINARY_ADDING_OPERATOR = 42;

  /**
   * The feature id for the '<em><b>Baoname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BINARY_ADDING_OPERATOR__BAONAME1 = 0;

  /**
   * The feature id for the '<em><b>Baoname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BINARY_ADDING_OPERATOR__BAONAME2 = 1;

  /**
   * The number of structural features of the '<em>binary adding operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BINARY_ADDING_OPERATOR_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.unary_adding_operatorImpl <em>unary adding operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.unary_adding_operatorImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getunary_adding_operator()
   * @generated
   */
  int UNARY_ADDING_OPERATOR = 43;

  /**
   * The feature id for the '<em><b>Uaoname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_ADDING_OPERATOR__UAONAME1 = 0;

  /**
   * The feature id for the '<em><b>Uaoname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_ADDING_OPERATOR__UAONAME2 = 1;

  /**
   * The number of structural features of the '<em>unary adding operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_ADDING_OPERATOR_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.multiplying_operatorImpl <em>multiplying operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.multiplying_operatorImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getmultiplying_operator()
   * @generated
   */
  int MULTIPLYING_OPERATOR = 44;

  /**
   * The feature id for the '<em><b>Moname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLYING_OPERATOR__MONAME1 = 0;

  /**
   * The feature id for the '<em><b>Moname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLYING_OPERATOR__MONAME2 = 1;

  /**
   * The feature id for the '<em><b>Moname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLYING_OPERATOR__MONAME3 = 2;

  /**
   * The feature id for the '<em><b>Moname4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLYING_OPERATOR__MONAME4 = 3;

  /**
   * The number of structural features of the '<em>multiplying operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLYING_OPERATOR_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.binary_numeric_operatorImpl <em>binary numeric operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.binary_numeric_operatorImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbinary_numeric_operator()
   * @generated
   */
  int BINARY_NUMERIC_OPERATOR = 45;

  /**
   * The feature id for the '<em><b>Bnoname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BINARY_NUMERIC_OPERATOR__BNONAME = 0;

  /**
   * The number of structural features of the '<em>binary numeric operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BINARY_NUMERIC_OPERATOR_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.unary_numeric_operatorImpl <em>unary numeric operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.unary_numeric_operatorImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getunary_numeric_operator()
   * @generated
   */
  int UNARY_NUMERIC_OPERATOR = 46;

  /**
   * The feature id for the '<em><b>Unoname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_NUMERIC_OPERATOR__UNONAME = 0;

  /**
   * The number of structural features of the '<em>unary numeric operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_NUMERIC_OPERATOR_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.unary_boolean_operatorImpl <em>unary boolean operator</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.unary_boolean_operatorImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getunary_boolean_operator()
   * @generated
   */
  int UNARY_BOOLEAN_OPERATOR = 47;

  /**
   * The feature id for the '<em><b>Uboname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_BOOLEAN_OPERATOR__UBONAME = 0;

  /**
   * The number of structural features of the '<em>unary boolean operator</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_BOOLEAN_OPERATOR_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.boolean_literalImpl <em>boolean literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.topcased.adele.xtext.baaction.baAction.impl.boolean_literalImpl
   * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getboolean_literal()
   * @generated
   */
  int BOOLEAN_LITERAL = 48;

  /**
   * The feature id for the '<em><b>Blname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL__BLNAME1 = 0;

  /**
   * The number of structural features of the '<em>boolean literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL_FEATURE_COUNT = 1;


  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block <em>behavior action block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>behavior action block</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_block
   * @generated
   */
  EClass getbehavior_action_block();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac1 <em>Bac1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bac1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac1()
   * @see #getbehavior_action_block()
   * @generated
   */
  EAttribute getbehavior_action_block_Bac1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac2 <em>Bac2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bac2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac2()
   * @see #getbehavior_action_block()
   * @generated
   */
  EReference getbehavior_action_block_Bac2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac3 <em>Bac3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bac3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac3()
   * @see #getbehavior_action_block()
   * @generated
   */
  EAttribute getbehavior_action_block_Bac3();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac4 <em>Bac4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bac4</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac4()
   * @see #getbehavior_action_block()
   * @generated
   */
  EAttribute getbehavior_action_block_Bac4();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac5 <em>Bac5</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bac5</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac5()
   * @see #getbehavior_action_block()
   * @generated
   */
  EReference getbehavior_action_block_Bac5();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action <em>behavior action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>behavior action</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action
   * @generated
   */
  EClass getbehavior_action();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa1 <em>Ba1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ba1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa1()
   * @see #getbehavior_action()
   * @generated
   */
  EReference getbehavior_action_Ba1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa2 <em>Ba2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ba2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa2()
   * @see #getbehavior_action()
   * @generated
   */
  EReference getbehavior_action_Ba2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa3 <em>Ba3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa3()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba3();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa3bis <em>Ba3bis</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba3bis</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa3bis()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba3bis();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa4 <em>Ba4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ba4</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa4()
   * @see #getbehavior_action()
   * @generated
   */
  EReference getbehavior_action_Ba4();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa5 <em>Ba5</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba5</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa5()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba5();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa6 <em>Ba6</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ba6</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa6()
   * @see #getbehavior_action()
   * @generated
   */
  EReference getbehavior_action_Ba6();

  /**
   * Returns the meta object for the attribute list '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa7 <em>Ba7</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Ba7</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa7()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba7();

  /**
   * Returns the meta object for the attribute list '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa7bis <em>Ba7bis</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Ba7bis</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa7bis()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba7bis();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa8 <em>Ba8</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Ba8</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa8()
   * @see #getbehavior_action()
   * @generated
   */
  EReference getbehavior_action_Ba8();

  /**
   * Returns the meta object for the attribute list '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa9 <em>Ba9</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Ba9</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa9()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba9();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa10 <em>Ba10</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Ba10</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa10()
   * @see #getbehavior_action()
   * @generated
   */
  EReference getbehavior_action_Ba10();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa11 <em>Ba11</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba11</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa11()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba11();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa12 <em>Ba12</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ba12</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa12()
   * @see #getbehavior_action()
   * @generated
   */
  EReference getbehavior_action_Ba12();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa13 <em>Ba13</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba13</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa13()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba13();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa13bis <em>Ba13bis</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba13bis</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa13bis()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba13bis();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa14 <em>Ba14</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba14</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa14()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba14();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa14bis <em>Ba14bis</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba14bis</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa14bis()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba14bis();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa15 <em>Ba15</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba15</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa15()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba15();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa16 <em>Ba16</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba16</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa16()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba16();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa17 <em>Ba17</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba17</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa17()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba17();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa18 <em>Ba18</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba18</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa18()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba18();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa19 <em>Ba19</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ba19</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa19()
   * @see #getbehavior_action()
   * @generated
   */
  EReference getbehavior_action_Ba19();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa20 <em>Ba20</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba20</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa20()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba20();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa20bis <em>Ba20bis</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba20bis</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa20bis()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba20bis();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa21 <em>Ba21</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ba21</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa21()
   * @see #getbehavior_action()
   * @generated
   */
  EReference getbehavior_action_Ba21();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa22 <em>Ba22</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba22</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa22()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba22();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa23 <em>Ba23</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba23</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa23()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba23();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa23bis <em>Ba23bis</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba23bis</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa23bis()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba23bis();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa24 <em>Ba24</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba24</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa24()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba24();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa25 <em>Ba25</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba25</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa25()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba25();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa26 <em>Ba26</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba26</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa26()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba26();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa27 <em>Ba27</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba27</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa27()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba27();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa29 <em>Ba29</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ba29</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa29()
   * @see #getbehavior_action()
   * @generated
   */
  EReference getbehavior_action_Ba29();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa30 <em>Ba30</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba30</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa30()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba30();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa30bis <em>Ba30bis</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba30bis</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa30bis()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba30bis();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa31 <em>Ba31</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ba31</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa31()
   * @see #getbehavior_action()
   * @generated
   */
  EReference getbehavior_action_Ba31();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa32 <em>Ba32</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba32</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa32()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba32();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa33 <em>Ba33</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba33</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa33()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba33();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa33bis <em>Ba33bis</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba33bis</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa33bis()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba33bis();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa34 <em>Ba34</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ba34</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa34()
   * @see #getbehavior_action()
   * @generated
   */
  EReference getbehavior_action_Ba34();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa35 <em>Ba35</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba35</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa35()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba35();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa35bis <em>Ba35bis</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba35bis</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa35bis()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba35bis();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa36 <em>Ba36</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ba36</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa36()
   * @see #getbehavior_action()
   * @generated
   */
  EReference getbehavior_action_Ba36();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa37 <em>Ba37</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba37</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa37()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba37();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa38 <em>Ba38</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba38</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa38()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba38();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa39 <em>Ba39</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ba39</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa39()
   * @see #getbehavior_action()
   * @generated
   */
  EReference getbehavior_action_Ba39();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa40 <em>Ba40</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba40</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa40()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba40();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa40bis <em>Ba40bis</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba40bis</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa40bis()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba40bis();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa41 <em>Ba41</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ba41</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa41()
   * @see #getbehavior_action()
   * @generated
   */
  EReference getbehavior_action_Ba41();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa42 <em>Ba42</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ba42</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa42()
   * @see #getbehavior_action()
   * @generated
   */
  EAttribute getbehavior_action_Ba42();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.behavior_actions <em>behavior actions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>behavior actions</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_actions
   * @generated
   */
  EClass getbehavior_actions();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_actions#getBas1 <em>Bas1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bas1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_actions#getBas1()
   * @see #getbehavior_actions()
   * @generated
   */
  EReference getbehavior_actions_Bas1();

  /**
   * Returns the meta object for the attribute list '{@link org.topcased.adele.xtext.baaction.baAction.behavior_actions#getBaseq2 <em>Baseq2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Baseq2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_actions#getBaseq2()
   * @see #getbehavior_actions()
   * @generated
   */
  EAttribute getbehavior_actions_Baseq2();

  /**
   * Returns the meta object for the attribute list '{@link org.topcased.adele.xtext.baaction.baAction.behavior_actions#getBaseq4 <em>Baseq4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Baseq4</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_actions#getBaseq4()
   * @see #getbehavior_actions()
   * @generated
   */
  EAttribute getbehavior_actions_Baseq4();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.behavior_actions#getBaseq3 <em>Baseq3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Baseq3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_actions#getBaseq3()
   * @see #getbehavior_actions()
   * @generated
   */
  EReference getbehavior_actions_Baseq3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence <em>behavior action sequence</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>behavior action sequence</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence
   * @generated
   */
  EClass getbehavior_action_sequence();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence#getBaseq1 <em>Baseq1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Baseq1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence#getBaseq1()
   * @see #getbehavior_action_sequence()
   * @generated
   */
  EReference getbehavior_action_sequence_Baseq1();

  /**
   * Returns the meta object for the attribute list '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence#getBaseq2 <em>Baseq2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Baseq2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence#getBaseq2()
   * @see #getbehavior_action_sequence()
   * @generated
   */
  EAttribute getbehavior_action_sequence_Baseq2();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence#getBaseq3 <em>Baseq3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Baseq3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence#getBaseq3()
   * @see #getbehavior_action_sequence()
   * @generated
   */
  EReference getbehavior_action_sequence_Baseq3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_set <em>behavior action set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>behavior action set</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_set
   * @generated
   */
  EClass getbehavior_action_set();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_set#getBaset1 <em>Baset1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Baset1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_set#getBaset1()
   * @see #getbehavior_action_set()
   * @generated
   */
  EReference getbehavior_action_set_Baset1();

  /**
   * Returns the meta object for the attribute list '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_set#getBaset2 <em>Baset2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Baset2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_set#getBaset2()
   * @see #getbehavior_action_set()
   * @generated
   */
  EAttribute getbehavior_action_set_Baset2();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_set#getBaset3 <em>Baset3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Baset3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_set#getBaset3()
   * @see #getbehavior_action_set()
   * @generated
   */
  EReference getbehavior_action_set_Baset3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.element_values <em>element values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>element values</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.element_values
   * @generated
   */
  EClass getelement_values();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.element_values#getEv1 <em>Ev1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ev1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.element_values#getEv1()
   * @see #getelement_values()
   * @generated
   */
  EReference getelement_values_Ev1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.element_values#getEv2 <em>Ev2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ev2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.element_values#getEv2()
   * @see #getelement_values()
   * @generated
   */
  EAttribute getelement_values_Ev2();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.element_values#getEv3 <em>Ev3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ev3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.element_values#getEv3()
   * @see #getelement_values()
   * @generated
   */
  EReference getelement_values_Ev3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.array_data_component_reference <em>array data component reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>array data component reference</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.array_data_component_reference
   * @generated
   */
  EClass getarray_data_component_reference();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr1 <em>Adcr1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Adcr1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr1()
   * @see #getarray_data_component_reference()
   * @generated
   */
  EAttribute getarray_data_component_reference_Adcr1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr2 <em>Adcr2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Adcr2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr2()
   * @see #getarray_data_component_reference()
   * @generated
   */
  EReference getarray_data_component_reference_Adcr2();

  /**
   * Returns the meta object for the attribute list '{@link org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr3 <em>Adcr3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Adcr3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr3()
   * @see #getarray_data_component_reference()
   * @generated
   */
  EAttribute getarray_data_component_reference_Adcr3();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr4 <em>Adcr4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Adcr4</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr4()
   * @see #getarray_data_component_reference()
   * @generated
   */
  EReference getarray_data_component_reference_Adcr4();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr5 <em>Adcr5</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Adcr5</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr5()
   * @see #getarray_data_component_reference()
   * @generated
   */
  EAttribute getarray_data_component_reference_Adcr5();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.basic_action <em>basic action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>basic action</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.basic_action
   * @generated
   */
  EClass getbasic_action();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.basic_action#getBact1 <em>Bact1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bact1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.basic_action#getBact1()
   * @see #getbasic_action()
   * @generated
   */
  EReference getbasic_action_Bact1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.basic_action#getBact2 <em>Bact2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bact2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.basic_action#getBact2()
   * @see #getbasic_action()
   * @generated
   */
  EReference getbasic_action_Bact2();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.basic_action#getBact3 <em>Bact3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bact3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.basic_action#getBact3()
   * @see #getbasic_action()
   * @generated
   */
  EReference getbasic_action_Bact3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.assignment_action <em>assignment action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>assignment action</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.assignment_action
   * @generated
   */
  EClass getassignment_action();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.assignment_action#getAa1 <em>Aa1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Aa1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.assignment_action#getAa1()
   * @see #getassignment_action()
   * @generated
   */
  EAttribute getassignment_action_Aa1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.assignment_action#getAa2 <em>Aa2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Aa2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.assignment_action#getAa2()
   * @see #getassignment_action()
   * @generated
   */
  EAttribute getassignment_action_Aa2();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.assignment_action#getAa3 <em>Aa3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Aa3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.assignment_action#getAa3()
   * @see #getassignment_action()
   * @generated
   */
  EReference getassignment_action_Aa3();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.assignment_action#getAa4 <em>Aa4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Aa4</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.assignment_action#getAa4()
   * @see #getassignment_action()
   * @generated
   */
  EAttribute getassignment_action_Aa4();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.communication_action <em>communication action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>communication action</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action
   * @generated
   */
  EClass getcommunication_action();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa1 <em>Ca1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa1()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa2 <em>Ca2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa2()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa3 <em>Ca3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa3()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca3();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa4 <em>Ca4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ca4</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa4()
   * @see #getcommunication_action()
   * @generated
   */
  EReference getcommunication_action_Ca4();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa5 <em>Ca5</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca5</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa5()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca5();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa21 <em>Ca21</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca21</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa21()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca21();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa22 <em>Ca22</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca22</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa22()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca22();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa23 <em>Ca23</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca23</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa23()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca23();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa24 <em>Ca24</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca24</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa24()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca24();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa25 <em>Ca25</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca25</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa25()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca25();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa26 <em>Ca26</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca26</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa26()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca26();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa27 <em>Ca27</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca27</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa27()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca27();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa28 <em>Ca28</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca28</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa28()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca28();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa29 <em>Ca29</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca29</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa29()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca29();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa30 <em>Ca30</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca30</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa30()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca30();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa31 <em>Ca31</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca31</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa31()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca31();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa32 <em>Ca32</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca32</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa32()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca32();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa33 <em>Ca33</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ca33</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action#getCa33()
   * @see #getcommunication_action()
   * @generated
   */
  EAttribute getcommunication_action_Ca33();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.timed_action <em>timed action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>timed action</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.timed_action
   * @generated
   */
  EClass gettimed_action();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa1 <em>Ta1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ta1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.timed_action#getTa1()
   * @see #gettimed_action()
   * @generated
   */
  EAttribute gettimed_action_Ta1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa1bis <em>Ta1bis</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ta1bis</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.timed_action#getTa1bis()
   * @see #gettimed_action()
   * @generated
   */
  EAttribute gettimed_action_Ta1bis();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa2 <em>Ta2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ta2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.timed_action#getTa2()
   * @see #gettimed_action()
   * @generated
   */
  EReference gettimed_action_Ta2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa3 <em>Ta3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ta3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.timed_action#getTa3()
   * @see #gettimed_action()
   * @generated
   */
  EAttribute gettimed_action_Ta3();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa4 <em>Ta4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ta4</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.timed_action#getTa4()
   * @see #gettimed_action()
   * @generated
   */
  EReference gettimed_action_Ta4();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa5 <em>Ta5</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ta5</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.timed_action#getTa5()
   * @see #gettimed_action()
   * @generated
   */
  EAttribute gettimed_action_Ta5();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list <em>subprogram parameter list</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>subprogram parameter list</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list
   * @generated
   */
  EClass getsubprogram_parameter_list();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list#getSpl1 <em>Spl1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Spl1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list#getSpl1()
   * @see #getsubprogram_parameter_list()
   * @generated
   */
  EReference getsubprogram_parameter_list_Spl1();

  /**
   * Returns the meta object for the attribute list '{@link org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list#getSpl2 <em>Spl2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Spl2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list#getSpl2()
   * @see #getsubprogram_parameter_list()
   * @generated
   */
  EAttribute getsubprogram_parameter_list_Spl2();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list#getSpl3 <em>Spl3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Spl3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list#getSpl3()
   * @see #getsubprogram_parameter_list()
   * @generated
   */
  EReference getsubprogram_parameter_list_Spl3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.parameter_label <em>parameter label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>parameter label</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.parameter_label
   * @generated
   */
  EClass getparameter_label();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.parameter_label#getPl1 <em>Pl1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Pl1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.parameter_label#getPl1()
   * @see #getparameter_label()
   * @generated
   */
  EReference getparameter_label_Pl1();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.data_component_reference <em>data component reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>data component reference</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.data_component_reference
   * @generated
   */
  EClass getdata_component_reference();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.data_component_reference#getDcr1 <em>Dcr1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dcr1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.data_component_reference#getDcr1()
   * @see #getdata_component_reference()
   * @generated
   */
  EAttribute getdata_component_reference_Dcr1();

  /**
   * Returns the meta object for the attribute list '{@link org.topcased.adele.xtext.baaction.baAction.data_component_reference#getDcr2 <em>Dcr2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Dcr2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.data_component_reference#getDcr2()
   * @see #getdata_component_reference()
   * @generated
   */
  EAttribute getdata_component_reference_Dcr2();

  /**
   * Returns the meta object for the attribute list '{@link org.topcased.adele.xtext.baaction.baAction.data_component_reference#getDcr3 <em>Dcr3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Dcr3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.data_component_reference#getDcr3()
   * @see #getdata_component_reference()
   * @generated
   */
  EAttribute getdata_component_reference_Dcr3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.name <em>name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>name</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.name
   * @generated
   */
  EClass getname();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.name#getN1 <em>N1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>N1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.name#getN1()
   * @see #getname()
   * @generated
   */
  EAttribute getname_N1();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.name#getN2 <em>N2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>N2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.name#getN2()
   * @see #getname()
   * @generated
   */
  EReference getname_N2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.array_index <em>array index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>array index</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.array_index
   * @generated
   */
  EClass getarray_index();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.array_index#getAi1 <em>Ai1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ai1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.array_index#getAi1()
   * @see #getarray_index()
   * @generated
   */
  EAttribute getarray_index_Ai1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.array_index#getAi2 <em>Ai2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ai2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.array_index#getAi2()
   * @see #getarray_index()
   * @generated
   */
  EReference getarray_index_Ai2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.array_index#getAi3 <em>Ai3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Ai3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.array_index#getAi3()
   * @see #getarray_index()
   * @generated
   */
  EAttribute getarray_index_Ai3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.integer_value <em>integer value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>integer value</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.integer_value
   * @generated
   */
  EClass getinteger_value();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.integer_value#getIvname <em>Ivname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ivname</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.integer_value#getIvname()
   * @see #getinteger_value()
   * @generated
   */
  EReference getinteger_value_Ivname();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.numeric_literal <em>numeric literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>numeric literal</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.numeric_literal
   * @generated
   */
  EClass getnumeric_literal();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.numeric_literal#getNlname1 <em>Nlname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Nlname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.numeric_literal#getNlname1()
   * @see #getnumeric_literal()
   * @generated
   */
  EReference getnumeric_literal_Nlname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.numeric_literal#getNlname2 <em>Nlname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Nlname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.numeric_literal#getNlname2()
   * @see #getnumeric_literal()
   * @generated
   */
  EReference getnumeric_literal_Nlname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.integer_literal <em>integer literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>integer literal</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.integer_literal
   * @generated
   */
  EClass getinteger_literal();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.integer_literal#getIlname1 <em>Ilname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ilname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.integer_literal#getIlname1()
   * @see #getinteger_literal()
   * @generated
   */
  EReference getinteger_literal_Ilname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.integer_literal#getIlname2 <em>Ilname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ilname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.integer_literal#getIlname2()
   * @see #getinteger_literal()
   * @generated
   */
  EReference getinteger_literal_Ilname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.real_literal <em>real literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>real literal</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.real_literal
   * @generated
   */
  EClass getreal_literal();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.real_literal#getRlname <em>Rlname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Rlname</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.real_literal#getRlname()
   * @see #getreal_literal()
   * @generated
   */
  EReference getreal_literal_Rlname();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.decimal_integer_literal <em>decimal integer literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>decimal integer literal</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.decimal_integer_literal
   * @generated
   */
  EClass getdecimal_integer_literal();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.decimal_integer_literal#getDilname1 <em>Dilname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Dilname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.decimal_integer_literal#getDilname1()
   * @see #getdecimal_integer_literal()
   * @generated
   */
  EReference getdecimal_integer_literal_Dilname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.decimal_integer_literal#getDilname2 <em>Dilname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Dilname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.decimal_integer_literal#getDilname2()
   * @see #getdecimal_integer_literal()
   * @generated
   */
  EReference getdecimal_integer_literal_Dilname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.decimal_real_literal <em>decimal real literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>decimal real literal</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.decimal_real_literal
   * @generated
   */
  EClass getdecimal_real_literal();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.decimal_real_literal#getDrlname1 <em>Drlname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Drlname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.decimal_real_literal#getDrlname1()
   * @see #getdecimal_real_literal()
   * @generated
   */
  EReference getdecimal_real_literal_Drlname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.decimal_real_literal#getDrlname2 <em>Drlname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Drlname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.decimal_real_literal#getDrlname2()
   * @see #getdecimal_real_literal()
   * @generated
   */
  EReference getdecimal_real_literal_Drlname2();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.decimal_real_literal#getDrlname3 <em>Drlname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Drlname3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.decimal_real_literal#getDrlname3()
   * @see #getdecimal_real_literal()
   * @generated
   */
  EReference getdecimal_real_literal_Drlname3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.numeral <em>numeral</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>numeral</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.numeral
   * @generated
   */
  EClass getnumeral();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.numeral#getNname1 <em>Nname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Nname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.numeral#getNname1()
   * @see #getnumeral()
   * @generated
   */
  EReference getnumeral_Nname1();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.numeral#getNname2 <em>Nname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Nname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.numeral#getNname2()
   * @see #getnumeral()
   * @generated
   */
  EReference getnumeral_Nname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.exponent <em>exponent</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>exponent</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.exponent
   * @generated
   */
  EClass getexponent();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.exponent#getEname1 <em>Ename1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ename1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.exponent#getEname1()
   * @see #getexponent()
   * @generated
   */
  EReference getexponent_Ename1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.exponent#getEname2 <em>Ename2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ename2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.exponent#getEname2()
   * @see #getexponent()
   * @generated
   */
  EReference getexponent_Ename2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.positive_exponent <em>positive exponent</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>positive exponent</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.positive_exponent
   * @generated
   */
  EClass getpositive_exponent();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.positive_exponent#getPename <em>Pename</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Pename</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.positive_exponent#getPename()
   * @see #getpositive_exponent()
   * @generated
   */
  EReference getpositive_exponent_Pename();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.based_integer_literal <em>based integer literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>based integer literal</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.based_integer_literal
   * @generated
   */
  EClass getbased_integer_literal();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.based_integer_literal#getBilname1 <em>Bilname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bilname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.based_integer_literal#getBilname1()
   * @see #getbased_integer_literal()
   * @generated
   */
  EReference getbased_integer_literal_Bilname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.based_integer_literal#getBilname2 <em>Bilname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bilname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.based_integer_literal#getBilname2()
   * @see #getbased_integer_literal()
   * @generated
   */
  EReference getbased_integer_literal_Bilname2();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.based_integer_literal#getBilname3 <em>Bilname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bilname3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.based_integer_literal#getBilname3()
   * @see #getbased_integer_literal()
   * @generated
   */
  EReference getbased_integer_literal_Bilname3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.base <em>base</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>base</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.base
   * @generated
   */
  EClass getbase();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.base#getBname1 <em>Bname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.base#getBname1()
   * @see #getbase()
   * @generated
   */
  EAttribute getbase_Bname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.base#getBname2 <em>Bname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.base#getBname2()
   * @see #getbase()
   * @generated
   */
  EAttribute getbase_Bname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.based_numeral <em>based numeral</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>based numeral</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.based_numeral
   * @generated
   */
  EClass getbased_numeral();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.based_numeral#getBnname1 <em>Bnname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bnname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.based_numeral#getBnname1()
   * @see #getbased_numeral()
   * @generated
   */
  EAttribute getbased_numeral_Bnname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.based_numeral#getBnname2 <em>Bnname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bnname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.based_numeral#getBnname2()
   * @see #getbased_numeral()
   * @generated
   */
  EAttribute getbased_numeral_Bnname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.behavior_time <em>behavior time</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>behavior time</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_time
   * @generated
   */
  EClass getbehavior_time();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_time#getBtname1 <em>Btname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Btname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_time#getBtname1()
   * @see #getbehavior_time()
   * @generated
   */
  EReference getbehavior_time_Btname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.behavior_time#getBtname2 <em>Btname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Btname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_time#getBtname2()
   * @see #getbehavior_time()
   * @generated
   */
  EReference getbehavior_time_Btname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.unit_identifier <em>unit identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>unit identifier</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.unit_identifier
   * @generated
   */
  EClass getunit_identifier();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.unit_identifier#getUiname1 <em>Uiname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uiname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.unit_identifier#getUiname1()
   * @see #getunit_identifier()
   * @generated
   */
  EAttribute getunit_identifier_Uiname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.unit_identifier#getUiname2 <em>Uiname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uiname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.unit_identifier#getUiname2()
   * @see #getunit_identifier()
   * @generated
   */
  EAttribute getunit_identifier_Uiname2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.unit_identifier#getUiname3 <em>Uiname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uiname3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.unit_identifier#getUiname3()
   * @see #getunit_identifier()
   * @generated
   */
  EAttribute getunit_identifier_Uiname3();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.unit_identifier#getUiname4 <em>Uiname4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uiname4</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.unit_identifier#getUiname4()
   * @see #getunit_identifier()
   * @generated
   */
  EAttribute getunit_identifier_Uiname4();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.unit_identifier#getUiname5 <em>Uiname5</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uiname5</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.unit_identifier#getUiname5()
   * @see #getunit_identifier()
   * @generated
   */
  EAttribute getunit_identifier_Uiname5();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.unit_identifier#getUiname6 <em>Uiname6</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uiname6</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.unit_identifier#getUiname6()
   * @see #getunit_identifier()
   * @generated
   */
  EAttribute getunit_identifier_Uiname6();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.unit_identifier#getUiname7 <em>Uiname7</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uiname7</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.unit_identifier#getUiname7()
   * @see #getunit_identifier()
   * @generated
   */
  EAttribute getunit_identifier_Uiname7();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.integer_range <em>integer range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>integer range</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.integer_range
   * @generated
   */
  EClass getinteger_range();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.integer_range#getMin <em>Min</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Min</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.integer_range#getMin()
   * @see #getinteger_range()
   * @generated
   */
  EReference getinteger_range_Min();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.integer_range#getIrsep <em>Irsep</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Irsep</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.integer_range#getIrsep()
   * @see #getinteger_range()
   * @generated
   */
  EAttribute getinteger_range_Irsep();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.integer_range#getMax <em>Max</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Max</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.integer_range#getMax()
   * @see #getinteger_range()
   * @generated
   */
  EReference getinteger_range_Max();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.integer <em>integer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>integer</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.integer
   * @generated
   */
  EClass getinteger();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.integer#getValue1 <em>Value1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.integer#getValue1()
   * @see #getinteger()
   * @generated
   */
  EAttribute getinteger_Value1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.integer#getValue2 <em>Value2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.integer#getValue2()
   * @see #getinteger()
   * @generated
   */
  EAttribute getinteger_Value2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.value_expression <em>value expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>value expression</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value_expression
   * @generated
   */
  EClass getvalue_expression();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.value_expression#getVename1 <em>Vename1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Vename1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value_expression#getVename1()
   * @see #getvalue_expression()
   * @generated
   */
  EReference getvalue_expression_Vename1();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.value_expression#getVename3 <em>Vename3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Vename3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value_expression#getVename3()
   * @see #getvalue_expression()
   * @generated
   */
  EReference getvalue_expression_Vename3();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.value_expression#getVename2 <em>Vename2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Vename2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value_expression#getVename2()
   * @see #getvalue_expression()
   * @generated
   */
  EReference getvalue_expression_Vename2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.relation <em>relation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>relation</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.relation
   * @generated
   */
  EClass getrelation();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.relation#getRname1 <em>Rname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Rname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.relation#getRname1()
   * @see #getrelation()
   * @generated
   */
  EReference getrelation_Rname1();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.relation#getRname3 <em>Rname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rname3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.relation#getRname3()
   * @see #getrelation()
   * @generated
   */
  EReference getrelation_Rname3();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.relation#getRname2 <em>Rname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.relation#getRname2()
   * @see #getrelation()
   * @generated
   */
  EReference getrelation_Rname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.simple_expression <em>simple expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>simple expression</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.simple_expression
   * @generated
   */
  EClass getsimple_expression();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.simple_expression#getSename2 <em>Sename2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sename2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.simple_expression#getSename2()
   * @see #getsimple_expression()
   * @generated
   */
  EReference getsimple_expression_Sename2();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.simple_expression#getSename1 <em>Sename1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sename1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.simple_expression#getSename1()
   * @see #getsimple_expression()
   * @generated
   */
  EReference getsimple_expression_Sename1();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.simple_expression#getSename3 <em>Sename3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sename3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.simple_expression#getSename3()
   * @see #getsimple_expression()
   * @generated
   */
  EReference getsimple_expression_Sename3();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.simple_expression#getSename4 <em>Sename4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sename4</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.simple_expression#getSename4()
   * @see #getsimple_expression()
   * @generated
   */
  EReference getsimple_expression_Sename4();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.term <em>term</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>term</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.term
   * @generated
   */
  EClass getterm();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.term#getTname1 <em>Tname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Tname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.term#getTname1()
   * @see #getterm()
   * @generated
   */
  EReference getterm_Tname1();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.term#getTname3 <em>Tname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Tname3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.term#getTname3()
   * @see #getterm()
   * @generated
   */
  EReference getterm_Tname3();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.term#getTname2 <em>Tname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Tname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.term#getTname2()
   * @see #getterm()
   * @generated
   */
  EReference getterm_Tname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.factor <em>factor</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>factor</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.factor
   * @generated
   */
  EClass getfactor();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname1 <em>Fname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Fname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.factor#getFname1()
   * @see #getfactor()
   * @generated
   */
  EReference getfactor_Fname1();

  /**
   * Returns the meta object for the containment reference list '{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname3 <em>Fname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Fname3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.factor#getFname3()
   * @see #getfactor()
   * @generated
   */
  EReference getfactor_Fname3();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname2 <em>Fname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Fname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.factor#getFname2()
   * @see #getfactor()
   * @generated
   */
  EReference getfactor_Fname2();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname4 <em>Fname4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Fname4</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.factor#getFname4()
   * @see #getfactor()
   * @generated
   */
  EReference getfactor_Fname4();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname5 <em>Fname5</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Fname5</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.factor#getFname5()
   * @see #getfactor()
   * @generated
   */
  EReference getfactor_Fname5();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname6 <em>Fname6</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Fname6</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.factor#getFname6()
   * @see #getfactor()
   * @generated
   */
  EReference getfactor_Fname6();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname7 <em>Fname7</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Fname7</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.factor#getFname7()
   * @see #getfactor()
   * @generated
   */
  EReference getfactor_Fname7();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.value <em>value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>value</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value
   * @generated
   */
  EClass getvalue();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.value#getVname1 <em>Vname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Vname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value#getVname1()
   * @see #getvalue()
   * @generated
   */
  EReference getvalue_Vname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.value#getVname2 <em>Vname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Vname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value#getVname2()
   * @see #getvalue()
   * @generated
   */
  EReference getvalue_Vname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.value_variable <em>value variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>value variable</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value_variable
   * @generated
   */
  EClass getvalue_variable();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.value_variable#getVvname0 <em>Vvname0</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Vvname0</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value_variable#getVvname0()
   * @see #getvalue_variable()
   * @generated
   */
  EAttribute getvalue_variable_Vvname0();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.value_variable#getVvname1 <em>Vvname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Vvname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value_variable#getVvname1()
   * @see #getvalue_variable()
   * @generated
   */
  EAttribute getvalue_variable_Vvname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.value_variable#getVvname2 <em>Vvname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Vvname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value_variable#getVvname2()
   * @see #getvalue_variable()
   * @generated
   */
  EAttribute getvalue_variable_Vvname2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.value_variable#getVvname3 <em>Vvname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Vvname3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value_variable#getVvname3()
   * @see #getvalue_variable()
   * @generated
   */
  EAttribute getvalue_variable_Vvname3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.value_constant <em>value constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>value constant</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value_constant
   * @generated
   */
  EClass getvalue_constant();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.value_constant#getVcname1 <em>Vcname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Vcname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value_constant#getVcname1()
   * @see #getvalue_constant()
   * @generated
   */
  EReference getvalue_constant_Vcname1();

  /**
   * Returns the meta object for the containment reference '{@link org.topcased.adele.xtext.baaction.baAction.value_constant#getVcname2 <em>Vcname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Vcname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value_constant#getVcname2()
   * @see #getvalue_constant()
   * @generated
   */
  EReference getvalue_constant_Vcname2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.value_constant#getVcname3 <em>Vcname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Vcname3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value_constant#getVcname3()
   * @see #getvalue_constant()
   * @generated
   */
  EAttribute getvalue_constant_Vcname3();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.value_constant#getVcname4 <em>Vcname4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Vcname4</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.value_constant#getVcname4()
   * @see #getvalue_constant()
   * @generated
   */
  EAttribute getvalue_constant_Vcname4();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.logical_operator <em>logical operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>logical operator</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.logical_operator
   * @generated
   */
  EClass getlogical_operator();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.logical_operator#getLoname1 <em>Loname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Loname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.logical_operator#getLoname1()
   * @see #getlogical_operator()
   * @generated
   */
  EAttribute getlogical_operator_Loname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.logical_operator#getLoname2 <em>Loname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Loname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.logical_operator#getLoname2()
   * @see #getlogical_operator()
   * @generated
   */
  EAttribute getlogical_operator_Loname2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.logical_operator#getLoname3 <em>Loname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Loname3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.logical_operator#getLoname3()
   * @see #getlogical_operator()
   * @generated
   */
  EAttribute getlogical_operator_Loname3();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.relational_operator <em>relational operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>relational operator</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.relational_operator
   * @generated
   */
  EClass getrelational_operator();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.relational_operator#getRoname1 <em>Roname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Roname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.relational_operator#getRoname1()
   * @see #getrelational_operator()
   * @generated
   */
  EAttribute getrelational_operator_Roname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.relational_operator#getRoname2 <em>Roname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Roname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.relational_operator#getRoname2()
   * @see #getrelational_operator()
   * @generated
   */
  EAttribute getrelational_operator_Roname2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.relational_operator#getRoname3 <em>Roname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Roname3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.relational_operator#getRoname3()
   * @see #getrelational_operator()
   * @generated
   */
  EAttribute getrelational_operator_Roname3();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.relational_operator#getRoname4 <em>Roname4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Roname4</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.relational_operator#getRoname4()
   * @see #getrelational_operator()
   * @generated
   */
  EAttribute getrelational_operator_Roname4();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.relational_operator#getRoname5 <em>Roname5</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Roname5</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.relational_operator#getRoname5()
   * @see #getrelational_operator()
   * @generated
   */
  EAttribute getrelational_operator_Roname5();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.relational_operator#getRoname6 <em>Roname6</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Roname6</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.relational_operator#getRoname6()
   * @see #getrelational_operator()
   * @generated
   */
  EAttribute getrelational_operator_Roname6();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.binary_adding_operator <em>binary adding operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>binary adding operator</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.binary_adding_operator
   * @generated
   */
  EClass getbinary_adding_operator();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.binary_adding_operator#getBaoname1 <em>Baoname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Baoname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.binary_adding_operator#getBaoname1()
   * @see #getbinary_adding_operator()
   * @generated
   */
  EAttribute getbinary_adding_operator_Baoname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.binary_adding_operator#getBaoname2 <em>Baoname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Baoname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.binary_adding_operator#getBaoname2()
   * @see #getbinary_adding_operator()
   * @generated
   */
  EAttribute getbinary_adding_operator_Baoname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.unary_adding_operator <em>unary adding operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>unary adding operator</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.unary_adding_operator
   * @generated
   */
  EClass getunary_adding_operator();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.unary_adding_operator#getUaoname1 <em>Uaoname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uaoname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.unary_adding_operator#getUaoname1()
   * @see #getunary_adding_operator()
   * @generated
   */
  EAttribute getunary_adding_operator_Uaoname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.unary_adding_operator#getUaoname2 <em>Uaoname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uaoname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.unary_adding_operator#getUaoname2()
   * @see #getunary_adding_operator()
   * @generated
   */
  EAttribute getunary_adding_operator_Uaoname2();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.multiplying_operator <em>multiplying operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>multiplying operator</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.multiplying_operator
   * @generated
   */
  EClass getmultiplying_operator();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.multiplying_operator#getMoname1 <em>Moname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Moname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.multiplying_operator#getMoname1()
   * @see #getmultiplying_operator()
   * @generated
   */
  EAttribute getmultiplying_operator_Moname1();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.multiplying_operator#getMoname2 <em>Moname2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Moname2</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.multiplying_operator#getMoname2()
   * @see #getmultiplying_operator()
   * @generated
   */
  EAttribute getmultiplying_operator_Moname2();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.multiplying_operator#getMoname3 <em>Moname3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Moname3</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.multiplying_operator#getMoname3()
   * @see #getmultiplying_operator()
   * @generated
   */
  EAttribute getmultiplying_operator_Moname3();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.multiplying_operator#getMoname4 <em>Moname4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Moname4</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.multiplying_operator#getMoname4()
   * @see #getmultiplying_operator()
   * @generated
   */
  EAttribute getmultiplying_operator_Moname4();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.binary_numeric_operator <em>binary numeric operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>binary numeric operator</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.binary_numeric_operator
   * @generated
   */
  EClass getbinary_numeric_operator();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.binary_numeric_operator#getBnoname <em>Bnoname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bnoname</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.binary_numeric_operator#getBnoname()
   * @see #getbinary_numeric_operator()
   * @generated
   */
  EAttribute getbinary_numeric_operator_Bnoname();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.unary_numeric_operator <em>unary numeric operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>unary numeric operator</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.unary_numeric_operator
   * @generated
   */
  EClass getunary_numeric_operator();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.unary_numeric_operator#getUnoname <em>Unoname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Unoname</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.unary_numeric_operator#getUnoname()
   * @see #getunary_numeric_operator()
   * @generated
   */
  EAttribute getunary_numeric_operator_Unoname();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.unary_boolean_operator <em>unary boolean operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>unary boolean operator</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.unary_boolean_operator
   * @generated
   */
  EClass getunary_boolean_operator();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.unary_boolean_operator#getUboname <em>Uboname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Uboname</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.unary_boolean_operator#getUboname()
   * @see #getunary_boolean_operator()
   * @generated
   */
  EAttribute getunary_boolean_operator_Uboname();

  /**
   * Returns the meta object for class '{@link org.topcased.adele.xtext.baaction.baAction.boolean_literal <em>boolean literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>boolean literal</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.boolean_literal
   * @generated
   */
  EClass getboolean_literal();

  /**
   * Returns the meta object for the attribute '{@link org.topcased.adele.xtext.baaction.baAction.boolean_literal#getBlname1 <em>Blname1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Blname1</em>'.
   * @see org.topcased.adele.xtext.baaction.baAction.boolean_literal#getBlname1()
   * @see #getboolean_literal()
   * @generated
   */
  EAttribute getboolean_literal_Blname1();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  BaActionFactory getBaActionFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_blockImpl <em>behavior action block</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_blockImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbehavior_action_block()
     * @generated
     */
    EClass BEHAVIOR_ACTION_BLOCK = eINSTANCE.getbehavior_action_block();

    /**
     * The meta object literal for the '<em><b>Bac1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION_BLOCK__BAC1 = eINSTANCE.getbehavior_action_block_Bac1();

    /**
     * The meta object literal for the '<em><b>Bac2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION_BLOCK__BAC2 = eINSTANCE.getbehavior_action_block_Bac2();

    /**
     * The meta object literal for the '<em><b>Bac3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION_BLOCK__BAC3 = eINSTANCE.getbehavior_action_block_Bac3();

    /**
     * The meta object literal for the '<em><b>Bac4</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION_BLOCK__BAC4 = eINSTANCE.getbehavior_action_block_Bac4();

    /**
     * The meta object literal for the '<em><b>Bac5</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION_BLOCK__BAC5 = eINSTANCE.getbehavior_action_block_Bac5();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl <em>behavior action</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbehavior_action()
     * @generated
     */
    EClass BEHAVIOR_ACTION = eINSTANCE.getbehavior_action();

    /**
     * The meta object literal for the '<em><b>Ba1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION__BA1 = eINSTANCE.getbehavior_action_Ba1();

    /**
     * The meta object literal for the '<em><b>Ba2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION__BA2 = eINSTANCE.getbehavior_action_Ba2();

    /**
     * The meta object literal for the '<em><b>Ba3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA3 = eINSTANCE.getbehavior_action_Ba3();

    /**
     * The meta object literal for the '<em><b>Ba3bis</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA3BIS = eINSTANCE.getbehavior_action_Ba3bis();

    /**
     * The meta object literal for the '<em><b>Ba4</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION__BA4 = eINSTANCE.getbehavior_action_Ba4();

    /**
     * The meta object literal for the '<em><b>Ba5</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA5 = eINSTANCE.getbehavior_action_Ba5();

    /**
     * The meta object literal for the '<em><b>Ba6</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION__BA6 = eINSTANCE.getbehavior_action_Ba6();

    /**
     * The meta object literal for the '<em><b>Ba7</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA7 = eINSTANCE.getbehavior_action_Ba7();

    /**
     * The meta object literal for the '<em><b>Ba7bis</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA7BIS = eINSTANCE.getbehavior_action_Ba7bis();

    /**
     * The meta object literal for the '<em><b>Ba8</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION__BA8 = eINSTANCE.getbehavior_action_Ba8();

    /**
     * The meta object literal for the '<em><b>Ba9</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA9 = eINSTANCE.getbehavior_action_Ba9();

    /**
     * The meta object literal for the '<em><b>Ba10</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION__BA10 = eINSTANCE.getbehavior_action_Ba10();

    /**
     * The meta object literal for the '<em><b>Ba11</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA11 = eINSTANCE.getbehavior_action_Ba11();

    /**
     * The meta object literal for the '<em><b>Ba12</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION__BA12 = eINSTANCE.getbehavior_action_Ba12();

    /**
     * The meta object literal for the '<em><b>Ba13</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA13 = eINSTANCE.getbehavior_action_Ba13();

    /**
     * The meta object literal for the '<em><b>Ba13bis</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA13BIS = eINSTANCE.getbehavior_action_Ba13bis();

    /**
     * The meta object literal for the '<em><b>Ba14</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA14 = eINSTANCE.getbehavior_action_Ba14();

    /**
     * The meta object literal for the '<em><b>Ba14bis</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA14BIS = eINSTANCE.getbehavior_action_Ba14bis();

    /**
     * The meta object literal for the '<em><b>Ba15</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA15 = eINSTANCE.getbehavior_action_Ba15();

    /**
     * The meta object literal for the '<em><b>Ba16</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA16 = eINSTANCE.getbehavior_action_Ba16();

    /**
     * The meta object literal for the '<em><b>Ba17</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA17 = eINSTANCE.getbehavior_action_Ba17();

    /**
     * The meta object literal for the '<em><b>Ba18</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA18 = eINSTANCE.getbehavior_action_Ba18();

    /**
     * The meta object literal for the '<em><b>Ba19</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION__BA19 = eINSTANCE.getbehavior_action_Ba19();

    /**
     * The meta object literal for the '<em><b>Ba20</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA20 = eINSTANCE.getbehavior_action_Ba20();

    /**
     * The meta object literal for the '<em><b>Ba20bis</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA20BIS = eINSTANCE.getbehavior_action_Ba20bis();

    /**
     * The meta object literal for the '<em><b>Ba21</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION__BA21 = eINSTANCE.getbehavior_action_Ba21();

    /**
     * The meta object literal for the '<em><b>Ba22</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA22 = eINSTANCE.getbehavior_action_Ba22();

    /**
     * The meta object literal for the '<em><b>Ba23</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA23 = eINSTANCE.getbehavior_action_Ba23();

    /**
     * The meta object literal for the '<em><b>Ba23bis</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA23BIS = eINSTANCE.getbehavior_action_Ba23bis();

    /**
     * The meta object literal for the '<em><b>Ba24</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA24 = eINSTANCE.getbehavior_action_Ba24();

    /**
     * The meta object literal for the '<em><b>Ba25</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA25 = eINSTANCE.getbehavior_action_Ba25();

    /**
     * The meta object literal for the '<em><b>Ba26</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA26 = eINSTANCE.getbehavior_action_Ba26();

    /**
     * The meta object literal for the '<em><b>Ba27</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA27 = eINSTANCE.getbehavior_action_Ba27();

    /**
     * The meta object literal for the '<em><b>Ba29</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION__BA29 = eINSTANCE.getbehavior_action_Ba29();

    /**
     * The meta object literal for the '<em><b>Ba30</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA30 = eINSTANCE.getbehavior_action_Ba30();

    /**
     * The meta object literal for the '<em><b>Ba30bis</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA30BIS = eINSTANCE.getbehavior_action_Ba30bis();

    /**
     * The meta object literal for the '<em><b>Ba31</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION__BA31 = eINSTANCE.getbehavior_action_Ba31();

    /**
     * The meta object literal for the '<em><b>Ba32</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA32 = eINSTANCE.getbehavior_action_Ba32();

    /**
     * The meta object literal for the '<em><b>Ba33</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA33 = eINSTANCE.getbehavior_action_Ba33();

    /**
     * The meta object literal for the '<em><b>Ba33bis</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA33BIS = eINSTANCE.getbehavior_action_Ba33bis();

    /**
     * The meta object literal for the '<em><b>Ba34</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION__BA34 = eINSTANCE.getbehavior_action_Ba34();

    /**
     * The meta object literal for the '<em><b>Ba35</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA35 = eINSTANCE.getbehavior_action_Ba35();

    /**
     * The meta object literal for the '<em><b>Ba35bis</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA35BIS = eINSTANCE.getbehavior_action_Ba35bis();

    /**
     * The meta object literal for the '<em><b>Ba36</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION__BA36 = eINSTANCE.getbehavior_action_Ba36();

    /**
     * The meta object literal for the '<em><b>Ba37</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA37 = eINSTANCE.getbehavior_action_Ba37();

    /**
     * The meta object literal for the '<em><b>Ba38</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA38 = eINSTANCE.getbehavior_action_Ba38();

    /**
     * The meta object literal for the '<em><b>Ba39</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION__BA39 = eINSTANCE.getbehavior_action_Ba39();

    /**
     * The meta object literal for the '<em><b>Ba40</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA40 = eINSTANCE.getbehavior_action_Ba40();

    /**
     * The meta object literal for the '<em><b>Ba40bis</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA40BIS = eINSTANCE.getbehavior_action_Ba40bis();

    /**
     * The meta object literal for the '<em><b>Ba41</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION__BA41 = eINSTANCE.getbehavior_action_Ba41();

    /**
     * The meta object literal for the '<em><b>Ba42</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION__BA42 = eINSTANCE.getbehavior_action_Ba42();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionsImpl <em>behavior actions</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionsImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbehavior_actions()
     * @generated
     */
    EClass BEHAVIOR_ACTIONS = eINSTANCE.getbehavior_actions();

    /**
     * The meta object literal for the '<em><b>Bas1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTIONS__BAS1 = eINSTANCE.getbehavior_actions_Bas1();

    /**
     * The meta object literal for the '<em><b>Baseq2</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTIONS__BASEQ2 = eINSTANCE.getbehavior_actions_Baseq2();

    /**
     * The meta object literal for the '<em><b>Baseq4</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTIONS__BASEQ4 = eINSTANCE.getbehavior_actions_Baseq4();

    /**
     * The meta object literal for the '<em><b>Baseq3</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTIONS__BASEQ3 = eINSTANCE.getbehavior_actions_Baseq3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_sequenceImpl <em>behavior action sequence</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_sequenceImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbehavior_action_sequence()
     * @generated
     */
    EClass BEHAVIOR_ACTION_SEQUENCE = eINSTANCE.getbehavior_action_sequence();

    /**
     * The meta object literal for the '<em><b>Baseq1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION_SEQUENCE__BASEQ1 = eINSTANCE.getbehavior_action_sequence_Baseq1();

    /**
     * The meta object literal for the '<em><b>Baseq2</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION_SEQUENCE__BASEQ2 = eINSTANCE.getbehavior_action_sequence_Baseq2();

    /**
     * The meta object literal for the '<em><b>Baseq3</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION_SEQUENCE__BASEQ3 = eINSTANCE.getbehavior_action_sequence_Baseq3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_setImpl <em>behavior action set</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_setImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbehavior_action_set()
     * @generated
     */
    EClass BEHAVIOR_ACTION_SET = eINSTANCE.getbehavior_action_set();

    /**
     * The meta object literal for the '<em><b>Baset1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION_SET__BASET1 = eINSTANCE.getbehavior_action_set_Baset1();

    /**
     * The meta object literal for the '<em><b>Baset2</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BEHAVIOR_ACTION_SET__BASET2 = eINSTANCE.getbehavior_action_set_Baset2();

    /**
     * The meta object literal for the '<em><b>Baset3</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_ACTION_SET__BASET3 = eINSTANCE.getbehavior_action_set_Baset3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.element_valuesImpl <em>element values</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.element_valuesImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getelement_values()
     * @generated
     */
    EClass ELEMENT_VALUES = eINSTANCE.getelement_values();

    /**
     * The meta object literal for the '<em><b>Ev1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ELEMENT_VALUES__EV1 = eINSTANCE.getelement_values_Ev1();

    /**
     * The meta object literal for the '<em><b>Ev2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ELEMENT_VALUES__EV2 = eINSTANCE.getelement_values_Ev2();

    /**
     * The meta object literal for the '<em><b>Ev3</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ELEMENT_VALUES__EV3 = eINSTANCE.getelement_values_Ev3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.array_data_component_referenceImpl <em>array data component reference</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.array_data_component_referenceImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getarray_data_component_reference()
     * @generated
     */
    EClass ARRAY_DATA_COMPONENT_REFERENCE = eINSTANCE.getarray_data_component_reference();

    /**
     * The meta object literal for the '<em><b>Adcr1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ARRAY_DATA_COMPONENT_REFERENCE__ADCR1 = eINSTANCE.getarray_data_component_reference_Adcr1();

    /**
     * The meta object literal for the '<em><b>Adcr2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ARRAY_DATA_COMPONENT_REFERENCE__ADCR2 = eINSTANCE.getarray_data_component_reference_Adcr2();

    /**
     * The meta object literal for the '<em><b>Adcr3</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ARRAY_DATA_COMPONENT_REFERENCE__ADCR3 = eINSTANCE.getarray_data_component_reference_Adcr3();

    /**
     * The meta object literal for the '<em><b>Adcr4</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ARRAY_DATA_COMPONENT_REFERENCE__ADCR4 = eINSTANCE.getarray_data_component_reference_Adcr4();

    /**
     * The meta object literal for the '<em><b>Adcr5</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ARRAY_DATA_COMPONENT_REFERENCE__ADCR5 = eINSTANCE.getarray_data_component_reference_Adcr5();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.basic_actionImpl <em>basic action</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.basic_actionImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbasic_action()
     * @generated
     */
    EClass BASIC_ACTION = eINSTANCE.getbasic_action();

    /**
     * The meta object literal for the '<em><b>Bact1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BASIC_ACTION__BACT1 = eINSTANCE.getbasic_action_Bact1();

    /**
     * The meta object literal for the '<em><b>Bact2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BASIC_ACTION__BACT2 = eINSTANCE.getbasic_action_Bact2();

    /**
     * The meta object literal for the '<em><b>Bact3</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BASIC_ACTION__BACT3 = eINSTANCE.getbasic_action_Bact3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.assignment_actionImpl <em>assignment action</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.assignment_actionImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getassignment_action()
     * @generated
     */
    EClass ASSIGNMENT_ACTION = eINSTANCE.getassignment_action();

    /**
     * The meta object literal for the '<em><b>Aa1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ASSIGNMENT_ACTION__AA1 = eINSTANCE.getassignment_action_Aa1();

    /**
     * The meta object literal for the '<em><b>Aa2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ASSIGNMENT_ACTION__AA2 = eINSTANCE.getassignment_action_Aa2();

    /**
     * The meta object literal for the '<em><b>Aa3</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ASSIGNMENT_ACTION__AA3 = eINSTANCE.getassignment_action_Aa3();

    /**
     * The meta object literal for the '<em><b>Aa4</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ASSIGNMENT_ACTION__AA4 = eINSTANCE.getassignment_action_Aa4();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl <em>communication action</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.communication_actionImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getcommunication_action()
     * @generated
     */
    EClass COMMUNICATION_ACTION = eINSTANCE.getcommunication_action();

    /**
     * The meta object literal for the '<em><b>Ca1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA1 = eINSTANCE.getcommunication_action_Ca1();

    /**
     * The meta object literal for the '<em><b>Ca2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA2 = eINSTANCE.getcommunication_action_Ca2();

    /**
     * The meta object literal for the '<em><b>Ca3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA3 = eINSTANCE.getcommunication_action_Ca3();

    /**
     * The meta object literal for the '<em><b>Ca4</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMMUNICATION_ACTION__CA4 = eINSTANCE.getcommunication_action_Ca4();

    /**
     * The meta object literal for the '<em><b>Ca5</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA5 = eINSTANCE.getcommunication_action_Ca5();

    /**
     * The meta object literal for the '<em><b>Ca21</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA21 = eINSTANCE.getcommunication_action_Ca21();

    /**
     * The meta object literal for the '<em><b>Ca22</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA22 = eINSTANCE.getcommunication_action_Ca22();

    /**
     * The meta object literal for the '<em><b>Ca23</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA23 = eINSTANCE.getcommunication_action_Ca23();

    /**
     * The meta object literal for the '<em><b>Ca24</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA24 = eINSTANCE.getcommunication_action_Ca24();

    /**
     * The meta object literal for the '<em><b>Ca25</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA25 = eINSTANCE.getcommunication_action_Ca25();

    /**
     * The meta object literal for the '<em><b>Ca26</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA26 = eINSTANCE.getcommunication_action_Ca26();

    /**
     * The meta object literal for the '<em><b>Ca27</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA27 = eINSTANCE.getcommunication_action_Ca27();

    /**
     * The meta object literal for the '<em><b>Ca28</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA28 = eINSTANCE.getcommunication_action_Ca28();

    /**
     * The meta object literal for the '<em><b>Ca29</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA29 = eINSTANCE.getcommunication_action_Ca29();

    /**
     * The meta object literal for the '<em><b>Ca30</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA30 = eINSTANCE.getcommunication_action_Ca30();

    /**
     * The meta object literal for the '<em><b>Ca31</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA31 = eINSTANCE.getcommunication_action_Ca31();

    /**
     * The meta object literal for the '<em><b>Ca32</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA32 = eINSTANCE.getcommunication_action_Ca32();

    /**
     * The meta object literal for the '<em><b>Ca33</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMUNICATION_ACTION__CA33 = eINSTANCE.getcommunication_action_Ca33();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.timed_actionImpl <em>timed action</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.timed_actionImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#gettimed_action()
     * @generated
     */
    EClass TIMED_ACTION = eINSTANCE.gettimed_action();

    /**
     * The meta object literal for the '<em><b>Ta1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TIMED_ACTION__TA1 = eINSTANCE.gettimed_action_Ta1();

    /**
     * The meta object literal for the '<em><b>Ta1bis</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TIMED_ACTION__TA1BIS = eINSTANCE.gettimed_action_Ta1bis();

    /**
     * The meta object literal for the '<em><b>Ta2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TIMED_ACTION__TA2 = eINSTANCE.gettimed_action_Ta2();

    /**
     * The meta object literal for the '<em><b>Ta3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TIMED_ACTION__TA3 = eINSTANCE.gettimed_action_Ta3();

    /**
     * The meta object literal for the '<em><b>Ta4</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TIMED_ACTION__TA4 = eINSTANCE.gettimed_action_Ta4();

    /**
     * The meta object literal for the '<em><b>Ta5</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TIMED_ACTION__TA5 = eINSTANCE.gettimed_action_Ta5();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.subprogram_parameter_listImpl <em>subprogram parameter list</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.subprogram_parameter_listImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getsubprogram_parameter_list()
     * @generated
     */
    EClass SUBPROGRAM_PARAMETER_LIST = eINSTANCE.getsubprogram_parameter_list();

    /**
     * The meta object literal for the '<em><b>Spl1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUBPROGRAM_PARAMETER_LIST__SPL1 = eINSTANCE.getsubprogram_parameter_list_Spl1();

    /**
     * The meta object literal for the '<em><b>Spl2</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SUBPROGRAM_PARAMETER_LIST__SPL2 = eINSTANCE.getsubprogram_parameter_list_Spl2();

    /**
     * The meta object literal for the '<em><b>Spl3</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUBPROGRAM_PARAMETER_LIST__SPL3 = eINSTANCE.getsubprogram_parameter_list_Spl3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.parameter_labelImpl <em>parameter label</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.parameter_labelImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getparameter_label()
     * @generated
     */
    EClass PARAMETER_LABEL = eINSTANCE.getparameter_label();

    /**
     * The meta object literal for the '<em><b>Pl1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARAMETER_LABEL__PL1 = eINSTANCE.getparameter_label_Pl1();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.data_component_referenceImpl <em>data component reference</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.data_component_referenceImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getdata_component_reference()
     * @generated
     */
    EClass DATA_COMPONENT_REFERENCE = eINSTANCE.getdata_component_reference();

    /**
     * The meta object literal for the '<em><b>Dcr1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATA_COMPONENT_REFERENCE__DCR1 = eINSTANCE.getdata_component_reference_Dcr1();

    /**
     * The meta object literal for the '<em><b>Dcr2</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATA_COMPONENT_REFERENCE__DCR2 = eINSTANCE.getdata_component_reference_Dcr2();

    /**
     * The meta object literal for the '<em><b>Dcr3</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATA_COMPONENT_REFERENCE__DCR3 = eINSTANCE.getdata_component_reference_Dcr3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.nameImpl <em>name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.nameImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getname()
     * @generated
     */
    EClass NAME = eINSTANCE.getname();

    /**
     * The meta object literal for the '<em><b>N1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NAME__N1 = eINSTANCE.getname_N1();

    /**
     * The meta object literal for the '<em><b>N2</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NAME__N2 = eINSTANCE.getname_N2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.array_indexImpl <em>array index</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.array_indexImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getarray_index()
     * @generated
     */
    EClass ARRAY_INDEX = eINSTANCE.getarray_index();

    /**
     * The meta object literal for the '<em><b>Ai1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ARRAY_INDEX__AI1 = eINSTANCE.getarray_index_Ai1();

    /**
     * The meta object literal for the '<em><b>Ai2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ARRAY_INDEX__AI2 = eINSTANCE.getarray_index_Ai2();

    /**
     * The meta object literal for the '<em><b>Ai3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ARRAY_INDEX__AI3 = eINSTANCE.getarray_index_Ai3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.integer_valueImpl <em>integer value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.integer_valueImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getinteger_value()
     * @generated
     */
    EClass INTEGER_VALUE = eINSTANCE.getinteger_value();

    /**
     * The meta object literal for the '<em><b>Ivname</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_VALUE__IVNAME = eINSTANCE.getinteger_value_Ivname();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.numeric_literalImpl <em>numeric literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.numeric_literalImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getnumeric_literal()
     * @generated
     */
    EClass NUMERIC_LITERAL = eINSTANCE.getnumeric_literal();

    /**
     * The meta object literal for the '<em><b>Nlname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NUMERIC_LITERAL__NLNAME1 = eINSTANCE.getnumeric_literal_Nlname1();

    /**
     * The meta object literal for the '<em><b>Nlname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NUMERIC_LITERAL__NLNAME2 = eINSTANCE.getnumeric_literal_Nlname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.integer_literalImpl <em>integer literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.integer_literalImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getinteger_literal()
     * @generated
     */
    EClass INTEGER_LITERAL = eINSTANCE.getinteger_literal();

    /**
     * The meta object literal for the '<em><b>Ilname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_LITERAL__ILNAME1 = eINSTANCE.getinteger_literal_Ilname1();

    /**
     * The meta object literal for the '<em><b>Ilname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_LITERAL__ILNAME2 = eINSTANCE.getinteger_literal_Ilname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.real_literalImpl <em>real literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.real_literalImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getreal_literal()
     * @generated
     */
    EClass REAL_LITERAL = eINSTANCE.getreal_literal();

    /**
     * The meta object literal for the '<em><b>Rlname</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REAL_LITERAL__RLNAME = eINSTANCE.getreal_literal_Rlname();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.decimal_integer_literalImpl <em>decimal integer literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.decimal_integer_literalImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getdecimal_integer_literal()
     * @generated
     */
    EClass DECIMAL_INTEGER_LITERAL = eINSTANCE.getdecimal_integer_literal();

    /**
     * The meta object literal for the '<em><b>Dilname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DECIMAL_INTEGER_LITERAL__DILNAME1 = eINSTANCE.getdecimal_integer_literal_Dilname1();

    /**
     * The meta object literal for the '<em><b>Dilname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DECIMAL_INTEGER_LITERAL__DILNAME2 = eINSTANCE.getdecimal_integer_literal_Dilname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.decimal_real_literalImpl <em>decimal real literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.decimal_real_literalImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getdecimal_real_literal()
     * @generated
     */
    EClass DECIMAL_REAL_LITERAL = eINSTANCE.getdecimal_real_literal();

    /**
     * The meta object literal for the '<em><b>Drlname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DECIMAL_REAL_LITERAL__DRLNAME1 = eINSTANCE.getdecimal_real_literal_Drlname1();

    /**
     * The meta object literal for the '<em><b>Drlname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DECIMAL_REAL_LITERAL__DRLNAME2 = eINSTANCE.getdecimal_real_literal_Drlname2();

    /**
     * The meta object literal for the '<em><b>Drlname3</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DECIMAL_REAL_LITERAL__DRLNAME3 = eINSTANCE.getdecimal_real_literal_Drlname3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.numeralImpl <em>numeral</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.numeralImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getnumeral()
     * @generated
     */
    EClass NUMERAL = eINSTANCE.getnumeral();

    /**
     * The meta object literal for the '<em><b>Nname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NUMERAL__NNAME1 = eINSTANCE.getnumeral_Nname1();

    /**
     * The meta object literal for the '<em><b>Nname2</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NUMERAL__NNAME2 = eINSTANCE.getnumeral_Nname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.exponentImpl <em>exponent</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.exponentImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getexponent()
     * @generated
     */
    EClass EXPONENT = eINSTANCE.getexponent();

    /**
     * The meta object literal for the '<em><b>Ename1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPONENT__ENAME1 = eINSTANCE.getexponent_Ename1();

    /**
     * The meta object literal for the '<em><b>Ename2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPONENT__ENAME2 = eINSTANCE.getexponent_Ename2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.positive_exponentImpl <em>positive exponent</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.positive_exponentImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getpositive_exponent()
     * @generated
     */
    EClass POSITIVE_EXPONENT = eINSTANCE.getpositive_exponent();

    /**
     * The meta object literal for the '<em><b>Pename</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference POSITIVE_EXPONENT__PENAME = eINSTANCE.getpositive_exponent_Pename();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.based_integer_literalImpl <em>based integer literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.based_integer_literalImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbased_integer_literal()
     * @generated
     */
    EClass BASED_INTEGER_LITERAL = eINSTANCE.getbased_integer_literal();

    /**
     * The meta object literal for the '<em><b>Bilname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BASED_INTEGER_LITERAL__BILNAME1 = eINSTANCE.getbased_integer_literal_Bilname1();

    /**
     * The meta object literal for the '<em><b>Bilname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BASED_INTEGER_LITERAL__BILNAME2 = eINSTANCE.getbased_integer_literal_Bilname2();

    /**
     * The meta object literal for the '<em><b>Bilname3</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BASED_INTEGER_LITERAL__BILNAME3 = eINSTANCE.getbased_integer_literal_Bilname3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.baseImpl <em>base</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.baseImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbase()
     * @generated
     */
    EClass BASE = eINSTANCE.getbase();

    /**
     * The meta object literal for the '<em><b>Bname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BASE__BNAME1 = eINSTANCE.getbase_Bname1();

    /**
     * The meta object literal for the '<em><b>Bname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BASE__BNAME2 = eINSTANCE.getbase_Bname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.based_numeralImpl <em>based numeral</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.based_numeralImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbased_numeral()
     * @generated
     */
    EClass BASED_NUMERAL = eINSTANCE.getbased_numeral();

    /**
     * The meta object literal for the '<em><b>Bnname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BASED_NUMERAL__BNNAME1 = eINSTANCE.getbased_numeral_Bnname1();

    /**
     * The meta object literal for the '<em><b>Bnname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BASED_NUMERAL__BNNAME2 = eINSTANCE.getbased_numeral_Bnname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_timeImpl <em>behavior time</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.behavior_timeImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbehavior_time()
     * @generated
     */
    EClass BEHAVIOR_TIME = eINSTANCE.getbehavior_time();

    /**
     * The meta object literal for the '<em><b>Btname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_TIME__BTNAME1 = eINSTANCE.getbehavior_time_Btname1();

    /**
     * The meta object literal for the '<em><b>Btname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BEHAVIOR_TIME__BTNAME2 = eINSTANCE.getbehavior_time_Btname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.unit_identifierImpl <em>unit identifier</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.unit_identifierImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getunit_identifier()
     * @generated
     */
    EClass UNIT_IDENTIFIER = eINSTANCE.getunit_identifier();

    /**
     * The meta object literal for the '<em><b>Uiname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNIT_IDENTIFIER__UINAME1 = eINSTANCE.getunit_identifier_Uiname1();

    /**
     * The meta object literal for the '<em><b>Uiname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNIT_IDENTIFIER__UINAME2 = eINSTANCE.getunit_identifier_Uiname2();

    /**
     * The meta object literal for the '<em><b>Uiname3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNIT_IDENTIFIER__UINAME3 = eINSTANCE.getunit_identifier_Uiname3();

    /**
     * The meta object literal for the '<em><b>Uiname4</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNIT_IDENTIFIER__UINAME4 = eINSTANCE.getunit_identifier_Uiname4();

    /**
     * The meta object literal for the '<em><b>Uiname5</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNIT_IDENTIFIER__UINAME5 = eINSTANCE.getunit_identifier_Uiname5();

    /**
     * The meta object literal for the '<em><b>Uiname6</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNIT_IDENTIFIER__UINAME6 = eINSTANCE.getunit_identifier_Uiname6();

    /**
     * The meta object literal for the '<em><b>Uiname7</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNIT_IDENTIFIER__UINAME7 = eINSTANCE.getunit_identifier_Uiname7();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.integer_rangeImpl <em>integer range</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.integer_rangeImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getinteger_range()
     * @generated
     */
    EClass INTEGER_RANGE = eINSTANCE.getinteger_range();

    /**
     * The meta object literal for the '<em><b>Min</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_RANGE__MIN = eINSTANCE.getinteger_range_Min();

    /**
     * The meta object literal for the '<em><b>Irsep</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTEGER_RANGE__IRSEP = eINSTANCE.getinteger_range_Irsep();

    /**
     * The meta object literal for the '<em><b>Max</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_RANGE__MAX = eINSTANCE.getinteger_range_Max();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.integerImpl <em>integer</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.integerImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getinteger()
     * @generated
     */
    EClass INTEGER = eINSTANCE.getinteger();

    /**
     * The meta object literal for the '<em><b>Value1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTEGER__VALUE1 = eINSTANCE.getinteger_Value1();

    /**
     * The meta object literal for the '<em><b>Value2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTEGER__VALUE2 = eINSTANCE.getinteger_Value2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.value_expressionImpl <em>value expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.value_expressionImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getvalue_expression()
     * @generated
     */
    EClass VALUE_EXPRESSION = eINSTANCE.getvalue_expression();

    /**
     * The meta object literal for the '<em><b>Vename1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE_EXPRESSION__VENAME1 = eINSTANCE.getvalue_expression_Vename1();

    /**
     * The meta object literal for the '<em><b>Vename3</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE_EXPRESSION__VENAME3 = eINSTANCE.getvalue_expression_Vename3();

    /**
     * The meta object literal for the '<em><b>Vename2</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE_EXPRESSION__VENAME2 = eINSTANCE.getvalue_expression_Vename2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.relationImpl <em>relation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.relationImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getrelation()
     * @generated
     */
    EClass RELATION = eINSTANCE.getrelation();

    /**
     * The meta object literal for the '<em><b>Rname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RELATION__RNAME1 = eINSTANCE.getrelation_Rname1();

    /**
     * The meta object literal for the '<em><b>Rname3</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RELATION__RNAME3 = eINSTANCE.getrelation_Rname3();

    /**
     * The meta object literal for the '<em><b>Rname2</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RELATION__RNAME2 = eINSTANCE.getrelation_Rname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.simple_expressionImpl <em>simple expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.simple_expressionImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getsimple_expression()
     * @generated
     */
    EClass SIMPLE_EXPRESSION = eINSTANCE.getsimple_expression();

    /**
     * The meta object literal for the '<em><b>Sename2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMPLE_EXPRESSION__SENAME2 = eINSTANCE.getsimple_expression_Sename2();

    /**
     * The meta object literal for the '<em><b>Sename1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMPLE_EXPRESSION__SENAME1 = eINSTANCE.getsimple_expression_Sename1();

    /**
     * The meta object literal for the '<em><b>Sename3</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMPLE_EXPRESSION__SENAME3 = eINSTANCE.getsimple_expression_Sename3();

    /**
     * The meta object literal for the '<em><b>Sename4</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMPLE_EXPRESSION__SENAME4 = eINSTANCE.getsimple_expression_Sename4();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.termImpl <em>term</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.termImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getterm()
     * @generated
     */
    EClass TERM = eINSTANCE.getterm();

    /**
     * The meta object literal for the '<em><b>Tname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TERM__TNAME1 = eINSTANCE.getterm_Tname1();

    /**
     * The meta object literal for the '<em><b>Tname3</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TERM__TNAME3 = eINSTANCE.getterm_Tname3();

    /**
     * The meta object literal for the '<em><b>Tname2</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TERM__TNAME2 = eINSTANCE.getterm_Tname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.factorImpl <em>factor</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.factorImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getfactor()
     * @generated
     */
    EClass FACTOR = eINSTANCE.getfactor();

    /**
     * The meta object literal for the '<em><b>Fname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FACTOR__FNAME1 = eINSTANCE.getfactor_Fname1();

    /**
     * The meta object literal for the '<em><b>Fname3</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FACTOR__FNAME3 = eINSTANCE.getfactor_Fname3();

    /**
     * The meta object literal for the '<em><b>Fname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FACTOR__FNAME2 = eINSTANCE.getfactor_Fname2();

    /**
     * The meta object literal for the '<em><b>Fname4</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FACTOR__FNAME4 = eINSTANCE.getfactor_Fname4();

    /**
     * The meta object literal for the '<em><b>Fname5</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FACTOR__FNAME5 = eINSTANCE.getfactor_Fname5();

    /**
     * The meta object literal for the '<em><b>Fname6</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FACTOR__FNAME6 = eINSTANCE.getfactor_Fname6();

    /**
     * The meta object literal for the '<em><b>Fname7</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FACTOR__FNAME7 = eINSTANCE.getfactor_Fname7();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.valueImpl <em>value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.valueImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getvalue()
     * @generated
     */
    EClass VALUE = eINSTANCE.getvalue();

    /**
     * The meta object literal for the '<em><b>Vname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE__VNAME1 = eINSTANCE.getvalue_Vname1();

    /**
     * The meta object literal for the '<em><b>Vname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE__VNAME2 = eINSTANCE.getvalue_Vname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.value_variableImpl <em>value variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.value_variableImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getvalue_variable()
     * @generated
     */
    EClass VALUE_VARIABLE = eINSTANCE.getvalue_variable();

    /**
     * The meta object literal for the '<em><b>Vvname0</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VALUE_VARIABLE__VVNAME0 = eINSTANCE.getvalue_variable_Vvname0();

    /**
     * The meta object literal for the '<em><b>Vvname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VALUE_VARIABLE__VVNAME1 = eINSTANCE.getvalue_variable_Vvname1();

    /**
     * The meta object literal for the '<em><b>Vvname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VALUE_VARIABLE__VVNAME2 = eINSTANCE.getvalue_variable_Vvname2();

    /**
     * The meta object literal for the '<em><b>Vvname3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VALUE_VARIABLE__VVNAME3 = eINSTANCE.getvalue_variable_Vvname3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.value_constantImpl <em>value constant</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.value_constantImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getvalue_constant()
     * @generated
     */
    EClass VALUE_CONSTANT = eINSTANCE.getvalue_constant();

    /**
     * The meta object literal for the '<em><b>Vcname1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE_CONSTANT__VCNAME1 = eINSTANCE.getvalue_constant_Vcname1();

    /**
     * The meta object literal for the '<em><b>Vcname2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE_CONSTANT__VCNAME2 = eINSTANCE.getvalue_constant_Vcname2();

    /**
     * The meta object literal for the '<em><b>Vcname3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VALUE_CONSTANT__VCNAME3 = eINSTANCE.getvalue_constant_Vcname3();

    /**
     * The meta object literal for the '<em><b>Vcname4</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VALUE_CONSTANT__VCNAME4 = eINSTANCE.getvalue_constant_Vcname4();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.logical_operatorImpl <em>logical operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.logical_operatorImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getlogical_operator()
     * @generated
     */
    EClass LOGICAL_OPERATOR = eINSTANCE.getlogical_operator();

    /**
     * The meta object literal for the '<em><b>Loname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LOGICAL_OPERATOR__LONAME1 = eINSTANCE.getlogical_operator_Loname1();

    /**
     * The meta object literal for the '<em><b>Loname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LOGICAL_OPERATOR__LONAME2 = eINSTANCE.getlogical_operator_Loname2();

    /**
     * The meta object literal for the '<em><b>Loname3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LOGICAL_OPERATOR__LONAME3 = eINSTANCE.getlogical_operator_Loname3();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.relational_operatorImpl <em>relational operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.relational_operatorImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getrelational_operator()
     * @generated
     */
    EClass RELATIONAL_OPERATOR = eINSTANCE.getrelational_operator();

    /**
     * The meta object literal for the '<em><b>Roname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RELATIONAL_OPERATOR__RONAME1 = eINSTANCE.getrelational_operator_Roname1();

    /**
     * The meta object literal for the '<em><b>Roname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RELATIONAL_OPERATOR__RONAME2 = eINSTANCE.getrelational_operator_Roname2();

    /**
     * The meta object literal for the '<em><b>Roname3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RELATIONAL_OPERATOR__RONAME3 = eINSTANCE.getrelational_operator_Roname3();

    /**
     * The meta object literal for the '<em><b>Roname4</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RELATIONAL_OPERATOR__RONAME4 = eINSTANCE.getrelational_operator_Roname4();

    /**
     * The meta object literal for the '<em><b>Roname5</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RELATIONAL_OPERATOR__RONAME5 = eINSTANCE.getrelational_operator_Roname5();

    /**
     * The meta object literal for the '<em><b>Roname6</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RELATIONAL_OPERATOR__RONAME6 = eINSTANCE.getrelational_operator_Roname6();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.binary_adding_operatorImpl <em>binary adding operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.binary_adding_operatorImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbinary_adding_operator()
     * @generated
     */
    EClass BINARY_ADDING_OPERATOR = eINSTANCE.getbinary_adding_operator();

    /**
     * The meta object literal for the '<em><b>Baoname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BINARY_ADDING_OPERATOR__BAONAME1 = eINSTANCE.getbinary_adding_operator_Baoname1();

    /**
     * The meta object literal for the '<em><b>Baoname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BINARY_ADDING_OPERATOR__BAONAME2 = eINSTANCE.getbinary_adding_operator_Baoname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.unary_adding_operatorImpl <em>unary adding operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.unary_adding_operatorImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getunary_adding_operator()
     * @generated
     */
    EClass UNARY_ADDING_OPERATOR = eINSTANCE.getunary_adding_operator();

    /**
     * The meta object literal for the '<em><b>Uaoname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNARY_ADDING_OPERATOR__UAONAME1 = eINSTANCE.getunary_adding_operator_Uaoname1();

    /**
     * The meta object literal for the '<em><b>Uaoname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNARY_ADDING_OPERATOR__UAONAME2 = eINSTANCE.getunary_adding_operator_Uaoname2();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.multiplying_operatorImpl <em>multiplying operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.multiplying_operatorImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getmultiplying_operator()
     * @generated
     */
    EClass MULTIPLYING_OPERATOR = eINSTANCE.getmultiplying_operator();

    /**
     * The meta object literal for the '<em><b>Moname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MULTIPLYING_OPERATOR__MONAME1 = eINSTANCE.getmultiplying_operator_Moname1();

    /**
     * The meta object literal for the '<em><b>Moname2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MULTIPLYING_OPERATOR__MONAME2 = eINSTANCE.getmultiplying_operator_Moname2();

    /**
     * The meta object literal for the '<em><b>Moname3</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MULTIPLYING_OPERATOR__MONAME3 = eINSTANCE.getmultiplying_operator_Moname3();

    /**
     * The meta object literal for the '<em><b>Moname4</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MULTIPLYING_OPERATOR__MONAME4 = eINSTANCE.getmultiplying_operator_Moname4();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.binary_numeric_operatorImpl <em>binary numeric operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.binary_numeric_operatorImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getbinary_numeric_operator()
     * @generated
     */
    EClass BINARY_NUMERIC_OPERATOR = eINSTANCE.getbinary_numeric_operator();

    /**
     * The meta object literal for the '<em><b>Bnoname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BINARY_NUMERIC_OPERATOR__BNONAME = eINSTANCE.getbinary_numeric_operator_Bnoname();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.unary_numeric_operatorImpl <em>unary numeric operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.unary_numeric_operatorImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getunary_numeric_operator()
     * @generated
     */
    EClass UNARY_NUMERIC_OPERATOR = eINSTANCE.getunary_numeric_operator();

    /**
     * The meta object literal for the '<em><b>Unoname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNARY_NUMERIC_OPERATOR__UNONAME = eINSTANCE.getunary_numeric_operator_Unoname();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.unary_boolean_operatorImpl <em>unary boolean operator</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.unary_boolean_operatorImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getunary_boolean_operator()
     * @generated
     */
    EClass UNARY_BOOLEAN_OPERATOR = eINSTANCE.getunary_boolean_operator();

    /**
     * The meta object literal for the '<em><b>Uboname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNARY_BOOLEAN_OPERATOR__UBONAME = eINSTANCE.getunary_boolean_operator_Uboname();

    /**
     * The meta object literal for the '{@link org.topcased.adele.xtext.baaction.baAction.impl.boolean_literalImpl <em>boolean literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.topcased.adele.xtext.baaction.baAction.impl.boolean_literalImpl
     * @see org.topcased.adele.xtext.baaction.baAction.impl.BaActionPackageImpl#getboolean_literal()
     * @generated
     */
    EClass BOOLEAN_LITERAL = eINSTANCE.getboolean_literal();

    /**
     * The meta object literal for the '<em><b>Blname1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_LITERAL__BLNAME1 = eINSTANCE.getboolean_literal_Blname1();

  }

} //BaActionPackage
