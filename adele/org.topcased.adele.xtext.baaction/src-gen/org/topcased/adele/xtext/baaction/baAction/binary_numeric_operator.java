/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>binary numeric operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.binary_numeric_operator#getBnoname <em>Bnoname</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbinary_numeric_operator()
 * @model
 * @generated
 */
public interface binary_numeric_operator extends EObject
{
  /**
   * Returns the value of the '<em><b>Bnoname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bnoname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bnoname</em>' attribute.
   * @see #setBnoname(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbinary_numeric_operator_Bnoname()
   * @model
   * @generated
   */
  String getBnoname();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.binary_numeric_operator#getBnoname <em>Bnoname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bnoname</em>' attribute.
   * @see #getBnoname()
   * @generated
   */
  void setBnoname(String value);

} // binary_numeric_operator
