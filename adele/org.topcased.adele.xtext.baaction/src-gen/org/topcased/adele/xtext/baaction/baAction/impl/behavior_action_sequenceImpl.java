/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.behavior_action;
import org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>behavior action sequence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_sequenceImpl#getBaseq1 <em>Baseq1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_sequenceImpl#getBaseq2 <em>Baseq2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_sequenceImpl#getBaseq3 <em>Baseq3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class behavior_action_sequenceImpl extends MinimalEObjectImpl.Container implements behavior_action_sequence
{
  /**
   * The cached value of the '{@link #getBaseq1() <em>Baseq1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaseq1()
   * @generated
   * @ordered
   */
  protected behavior_action baseq1;

  /**
   * The cached value of the '{@link #getBaseq2() <em>Baseq2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaseq2()
   * @generated
   * @ordered
   */
  protected EList<String> baseq2;

  /**
   * The cached value of the '{@link #getBaseq3() <em>Baseq3</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaseq3()
   * @generated
   * @ordered
   */
  protected EList<behavior_action> baseq3;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected behavior_action_sequenceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.BEHAVIOR_ACTION_SEQUENCE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_action getBaseq1()
  {
    return baseq1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBaseq1(behavior_action newBaseq1, NotificationChain msgs)
  {
    behavior_action oldBaseq1 = baseq1;
    baseq1 = newBaseq1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ1, oldBaseq1, newBaseq1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBaseq1(behavior_action newBaseq1)
  {
    if (newBaseq1 != baseq1)
    {
      NotificationChain msgs = null;
      if (baseq1 != null)
        msgs = ((InternalEObject)baseq1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ1, null, msgs);
      if (newBaseq1 != null)
        msgs = ((InternalEObject)newBaseq1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ1, null, msgs);
      msgs = basicSetBaseq1(newBaseq1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ1, newBaseq1, newBaseq1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getBaseq2()
  {
    if (baseq2 == null)
    {
      baseq2 = new EDataTypeEList<String>(String.class, this, BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ2);
    }
    return baseq2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<behavior_action> getBaseq3()
  {
    if (baseq3 == null)
    {
      baseq3 = new EObjectContainmentEList<behavior_action>(behavior_action.class, this, BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ3);
    }
    return baseq3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ1:
        return basicSetBaseq1(null, msgs);
      case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ3:
        return ((InternalEList<?>)getBaseq3()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ1:
        return getBaseq1();
      case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ2:
        return getBaseq2();
      case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ3:
        return getBaseq3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ1:
        setBaseq1((behavior_action)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ2:
        getBaseq2().clear();
        getBaseq2().addAll((Collection<? extends String>)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ3:
        getBaseq3().clear();
        getBaseq3().addAll((Collection<? extends behavior_action>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ1:
        setBaseq1((behavior_action)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ2:
        getBaseq2().clear();
        return;
      case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ3:
        getBaseq3().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ1:
        return baseq1 != null;
      case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ2:
        return baseq2 != null && !baseq2.isEmpty();
      case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE__BASEQ3:
        return baseq3 != null && !baseq3.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (baseq2: ");
    result.append(baseq2);
    result.append(')');
    return result.toString();
  }

} //behavior_action_sequenceImpl
