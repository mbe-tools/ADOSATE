/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.logical_operator;
import org.topcased.adele.xtext.baaction.baAction.relation;
import org.topcased.adele.xtext.baaction.baAction.value_expression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>value expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.value_expressionImpl#getVename1 <em>Vename1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.value_expressionImpl#getVename3 <em>Vename3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.value_expressionImpl#getVename2 <em>Vename2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class value_expressionImpl extends MinimalEObjectImpl.Container implements value_expression
{
  /**
   * The cached value of the '{@link #getVename1() <em>Vename1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVename1()
   * @generated
   * @ordered
   */
  protected relation vename1;

  /**
   * The cached value of the '{@link #getVename3() <em>Vename3</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVename3()
   * @generated
   * @ordered
   */
  protected EList<logical_operator> vename3;

  /**
   * The cached value of the '{@link #getVename2() <em>Vename2</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVename2()
   * @generated
   * @ordered
   */
  protected EList<relation> vename2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected value_expressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.VALUE_EXPRESSION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public relation getVename1()
  {
    return vename1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetVename1(relation newVename1, NotificationChain msgs)
  {
    relation oldVename1 = vename1;
    vename1 = newVename1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.VALUE_EXPRESSION__VENAME1, oldVename1, newVename1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVename1(relation newVename1)
  {
    if (newVename1 != vename1)
    {
      NotificationChain msgs = null;
      if (vename1 != null)
        msgs = ((InternalEObject)vename1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.VALUE_EXPRESSION__VENAME1, null, msgs);
      if (newVename1 != null)
        msgs = ((InternalEObject)newVename1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.VALUE_EXPRESSION__VENAME1, null, msgs);
      msgs = basicSetVename1(newVename1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.VALUE_EXPRESSION__VENAME1, newVename1, newVename1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<logical_operator> getVename3()
  {
    if (vename3 == null)
    {
      vename3 = new EObjectContainmentEList<logical_operator>(logical_operator.class, this, BaActionPackage.VALUE_EXPRESSION__VENAME3);
    }
    return vename3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<relation> getVename2()
  {
    if (vename2 == null)
    {
      vename2 = new EObjectContainmentEList<relation>(relation.class, this, BaActionPackage.VALUE_EXPRESSION__VENAME2);
    }
    return vename2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.VALUE_EXPRESSION__VENAME1:
        return basicSetVename1(null, msgs);
      case BaActionPackage.VALUE_EXPRESSION__VENAME3:
        return ((InternalEList<?>)getVename3()).basicRemove(otherEnd, msgs);
      case BaActionPackage.VALUE_EXPRESSION__VENAME2:
        return ((InternalEList<?>)getVename2()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.VALUE_EXPRESSION__VENAME1:
        return getVename1();
      case BaActionPackage.VALUE_EXPRESSION__VENAME3:
        return getVename3();
      case BaActionPackage.VALUE_EXPRESSION__VENAME2:
        return getVename2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.VALUE_EXPRESSION__VENAME1:
        setVename1((relation)newValue);
        return;
      case BaActionPackage.VALUE_EXPRESSION__VENAME3:
        getVename3().clear();
        getVename3().addAll((Collection<? extends logical_operator>)newValue);
        return;
      case BaActionPackage.VALUE_EXPRESSION__VENAME2:
        getVename2().clear();
        getVename2().addAll((Collection<? extends relation>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.VALUE_EXPRESSION__VENAME1:
        setVename1((relation)null);
        return;
      case BaActionPackage.VALUE_EXPRESSION__VENAME3:
        getVename3().clear();
        return;
      case BaActionPackage.VALUE_EXPRESSION__VENAME2:
        getVename2().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.VALUE_EXPRESSION__VENAME1:
        return vename1 != null;
      case BaActionPackage.VALUE_EXPRESSION__VENAME3:
        return vename3 != null && !vename3.isEmpty();
      case BaActionPackage.VALUE_EXPRESSION__VENAME2:
        return vename2 != null && !vename2.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //value_expressionImpl
