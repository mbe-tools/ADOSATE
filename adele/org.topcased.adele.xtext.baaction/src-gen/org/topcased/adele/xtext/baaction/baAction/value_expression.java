/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>value expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.value_expression#getVename1 <em>Vename1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.value_expression#getVename3 <em>Vename3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.value_expression#getVename2 <em>Vename2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getvalue_expression()
 * @model
 * @generated
 */
public interface value_expression extends EObject
{
  /**
   * Returns the value of the '<em><b>Vename1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Vename1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Vename1</em>' containment reference.
   * @see #setVename1(relation)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getvalue_expression_Vename1()
   * @model containment="true"
   * @generated
   */
  relation getVename1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.value_expression#getVename1 <em>Vename1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Vename1</em>' containment reference.
   * @see #getVename1()
   * @generated
   */
  void setVename1(relation value);

  /**
   * Returns the value of the '<em><b>Vename3</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baaction.baAction.logical_operator}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Vename3</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Vename3</em>' containment reference list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getvalue_expression_Vename3()
   * @model containment="true"
   * @generated
   */
  EList<logical_operator> getVename3();

  /**
   * Returns the value of the '<em><b>Vename2</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baaction.baAction.relation}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Vename2</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Vename2</em>' containment reference list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getvalue_expression_Vename2()
   * @model containment="true"
   * @generated
   */
  EList<relation> getVename2();

} // value_expression
