/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.unary_numeric_operator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>unary numeric operator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.unary_numeric_operatorImpl#getUnoname <em>Unoname</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class unary_numeric_operatorImpl extends MinimalEObjectImpl.Container implements unary_numeric_operator
{
  /**
   * The default value of the '{@link #getUnoname() <em>Unoname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUnoname()
   * @generated
   * @ordered
   */
  protected static final String UNONAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUnoname() <em>Unoname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUnoname()
   * @generated
   * @ordered
   */
  protected String unoname = UNONAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected unary_numeric_operatorImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.UNARY_NUMERIC_OPERATOR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getUnoname()
  {
    return unoname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUnoname(String newUnoname)
  {
    String oldUnoname = unoname;
    unoname = newUnoname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.UNARY_NUMERIC_OPERATOR__UNONAME, oldUnoname, unoname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.UNARY_NUMERIC_OPERATOR__UNONAME:
        return getUnoname();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.UNARY_NUMERIC_OPERATOR__UNONAME:
        setUnoname((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.UNARY_NUMERIC_OPERATOR__UNONAME:
        setUnoname(UNONAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.UNARY_NUMERIC_OPERATOR__UNONAME:
        return UNONAME_EDEFAULT == null ? unoname != null : !UNONAME_EDEFAULT.equals(unoname);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (unoname: ");
    result.append(unoname);
    result.append(')');
    return result.toString();
  }

} //unary_numeric_operatorImpl
