/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>factor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname1 <em>Fname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname3 <em>Fname3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname2 <em>Fname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname4 <em>Fname4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname5 <em>Fname5</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname6 <em>Fname6</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname7 <em>Fname7</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getfactor()
 * @model
 * @generated
 */
public interface factor extends EObject
{
  /**
   * Returns the value of the '<em><b>Fname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fname1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fname1</em>' containment reference.
   * @see #setFname1(value)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getfactor_Fname1()
   * @model containment="true"
   * @generated
   */
  value getFname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname1 <em>Fname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Fname1</em>' containment reference.
   * @see #getFname1()
   * @generated
   */
  void setFname1(value value);

  /**
   * Returns the value of the '<em><b>Fname3</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baaction.baAction.binary_numeric_operator}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fname3</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fname3</em>' containment reference list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getfactor_Fname3()
   * @model containment="true"
   * @generated
   */
  EList<binary_numeric_operator> getFname3();

  /**
   * Returns the value of the '<em><b>Fname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fname2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fname2</em>' containment reference.
   * @see #setFname2(value)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getfactor_Fname2()
   * @model containment="true"
   * @generated
   */
  value getFname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname2 <em>Fname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Fname2</em>' containment reference.
   * @see #getFname2()
   * @generated
   */
  void setFname2(value value);

  /**
   * Returns the value of the '<em><b>Fname4</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fname4</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fname4</em>' containment reference.
   * @see #setFname4(unary_numeric_operator)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getfactor_Fname4()
   * @model containment="true"
   * @generated
   */
  unary_numeric_operator getFname4();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname4 <em>Fname4</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Fname4</em>' containment reference.
   * @see #getFname4()
   * @generated
   */
  void setFname4(unary_numeric_operator value);

  /**
   * Returns the value of the '<em><b>Fname5</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fname5</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fname5</em>' containment reference.
   * @see #setFname5(value)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getfactor_Fname5()
   * @model containment="true"
   * @generated
   */
  value getFname5();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname5 <em>Fname5</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Fname5</em>' containment reference.
   * @see #getFname5()
   * @generated
   */
  void setFname5(value value);

  /**
   * Returns the value of the '<em><b>Fname6</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fname6</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fname6</em>' containment reference.
   * @see #setFname6(unary_boolean_operator)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getfactor_Fname6()
   * @model containment="true"
   * @generated
   */
  unary_boolean_operator getFname6();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname6 <em>Fname6</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Fname6</em>' containment reference.
   * @see #getFname6()
   * @generated
   */
  void setFname6(unary_boolean_operator value);

  /**
   * Returns the value of the '<em><b>Fname7</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fname7</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fname7</em>' containment reference.
   * @see #setFname7(value)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getfactor_Fname7()
   * @model containment="true"
   * @generated
   */
  value getFname7();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.factor#getFname7 <em>Fname7</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Fname7</em>' containment reference.
   * @see #getFname7()
   * @generated
   */
  void setFname7(value value);

} // factor
