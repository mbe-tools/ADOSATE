/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>unary adding operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.unary_adding_operator#getUaoname1 <em>Uaoname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.unary_adding_operator#getUaoname2 <em>Uaoname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getunary_adding_operator()
 * @model
 * @generated
 */
public interface unary_adding_operator extends EObject
{
  /**
   * Returns the value of the '<em><b>Uaoname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uaoname1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uaoname1</em>' attribute.
   * @see #setUaoname1(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getunary_adding_operator_Uaoname1()
   * @model
   * @generated
   */
  String getUaoname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.unary_adding_operator#getUaoname1 <em>Uaoname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uaoname1</em>' attribute.
   * @see #getUaoname1()
   * @generated
   */
  void setUaoname1(String value);

  /**
   * Returns the value of the '<em><b>Uaoname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uaoname2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uaoname2</em>' attribute.
   * @see #setUaoname2(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getunary_adding_operator_Uaoname2()
   * @model
   * @generated
   */
  String getUaoname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.unary_adding_operator#getUaoname2 <em>Uaoname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uaoname2</em>' attribute.
   * @see #getUaoname2()
   * @generated
   */
  void setUaoname2(String value);

} // unary_adding_operator
