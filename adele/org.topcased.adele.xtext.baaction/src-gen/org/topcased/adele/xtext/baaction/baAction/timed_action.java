/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>timed action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa1 <em>Ta1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa1bis <em>Ta1bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa2 <em>Ta2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa3 <em>Ta3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa4 <em>Ta4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa5 <em>Ta5</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#gettimed_action()
 * @model
 * @generated
 */
public interface timed_action extends EObject
{
  /**
   * Returns the value of the '<em><b>Ta1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ta1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ta1</em>' attribute.
   * @see #setTa1(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#gettimed_action_Ta1()
   * @model
   * @generated
   */
  String getTa1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa1 <em>Ta1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ta1</em>' attribute.
   * @see #getTa1()
   * @generated
   */
  void setTa1(String value);

  /**
   * Returns the value of the '<em><b>Ta1bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ta1bis</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ta1bis</em>' attribute.
   * @see #setTa1bis(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#gettimed_action_Ta1bis()
   * @model
   * @generated
   */
  String getTa1bis();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa1bis <em>Ta1bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ta1bis</em>' attribute.
   * @see #getTa1bis()
   * @generated
   */
  void setTa1bis(String value);

  /**
   * Returns the value of the '<em><b>Ta2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ta2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ta2</em>' containment reference.
   * @see #setTa2(behavior_time)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#gettimed_action_Ta2()
   * @model containment="true"
   * @generated
   */
  behavior_time getTa2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa2 <em>Ta2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ta2</em>' containment reference.
   * @see #getTa2()
   * @generated
   */
  void setTa2(behavior_time value);

  /**
   * Returns the value of the '<em><b>Ta3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ta3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ta3</em>' attribute.
   * @see #setTa3(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#gettimed_action_Ta3()
   * @model
   * @generated
   */
  String getTa3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa3 <em>Ta3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ta3</em>' attribute.
   * @see #getTa3()
   * @generated
   */
  void setTa3(String value);

  /**
   * Returns the value of the '<em><b>Ta4</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ta4</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ta4</em>' containment reference.
   * @see #setTa4(behavior_time)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#gettimed_action_Ta4()
   * @model containment="true"
   * @generated
   */
  behavior_time getTa4();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa4 <em>Ta4</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ta4</em>' containment reference.
   * @see #getTa4()
   * @generated
   */
  void setTa4(behavior_time value);

  /**
   * Returns the value of the '<em><b>Ta5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ta5</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ta5</em>' attribute.
   * @see #setTa5(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#gettimed_action_Ta5()
   * @model
   * @generated
   */
  String getTa5();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.timed_action#getTa5 <em>Ta5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ta5</em>' attribute.
   * @see #getTa5()
   * @generated
   */
  void setTa5(String value);

} // timed_action
