/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>behavior action block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac1 <em>Bac1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac2 <em>Bac2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac3 <em>Bac3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac4 <em>Bac4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac5 <em>Bac5</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_block()
 * @model
 * @generated
 */
public interface behavior_action_block extends EObject
{
  /**
   * Returns the value of the '<em><b>Bac1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bac1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bac1</em>' attribute.
   * @see #setBac1(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_block_Bac1()
   * @model
   * @generated
   */
  String getBac1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac1 <em>Bac1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bac1</em>' attribute.
   * @see #getBac1()
   * @generated
   */
  void setBac1(String value);

  /**
   * Returns the value of the '<em><b>Bac2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bac2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bac2</em>' containment reference.
   * @see #setBac2(behavior_actions)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_block_Bac2()
   * @model containment="true"
   * @generated
   */
  behavior_actions getBac2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac2 <em>Bac2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bac2</em>' containment reference.
   * @see #getBac2()
   * @generated
   */
  void setBac2(behavior_actions value);

  /**
   * Returns the value of the '<em><b>Bac3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bac3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bac3</em>' attribute.
   * @see #setBac3(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_block_Bac3()
   * @model
   * @generated
   */
  String getBac3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac3 <em>Bac3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bac3</em>' attribute.
   * @see #getBac3()
   * @generated
   */
  void setBac3(String value);

  /**
   * Returns the value of the '<em><b>Bac4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bac4</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bac4</em>' attribute.
   * @see #setBac4(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_block_Bac4()
   * @model
   * @generated
   */
  String getBac4();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac4 <em>Bac4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bac4</em>' attribute.
   * @see #getBac4()
   * @generated
   */
  void setBac4(String value);

  /**
   * Returns the value of the '<em><b>Bac5</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bac5</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bac5</em>' containment reference.
   * @see #setBac5(behavior_time)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_block_Bac5()
   * @model containment="true"
   * @generated
   */
  behavior_time getBac5();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block#getBac5 <em>Bac5</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bac5</em>' containment reference.
   * @see #getBac5()
   * @generated
   */
  void setBac5(behavior_time value);

} // behavior_action_block
