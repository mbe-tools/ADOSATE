/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage
 * @generated
 */
public interface BaActionFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  BaActionFactory eINSTANCE = org.topcased.adele.xtext.baaction.baAction.impl.BaActionFactoryImpl.init();

  /**
   * Returns a new object of class '<em>behavior action block</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>behavior action block</em>'.
   * @generated
   */
  behavior_action_block createbehavior_action_block();

  /**
   * Returns a new object of class '<em>behavior action</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>behavior action</em>'.
   * @generated
   */
  behavior_action createbehavior_action();

  /**
   * Returns a new object of class '<em>behavior actions</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>behavior actions</em>'.
   * @generated
   */
  behavior_actions createbehavior_actions();

  /**
   * Returns a new object of class '<em>behavior action sequence</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>behavior action sequence</em>'.
   * @generated
   */
  behavior_action_sequence createbehavior_action_sequence();

  /**
   * Returns a new object of class '<em>behavior action set</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>behavior action set</em>'.
   * @generated
   */
  behavior_action_set createbehavior_action_set();

  /**
   * Returns a new object of class '<em>element values</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>element values</em>'.
   * @generated
   */
  element_values createelement_values();

  /**
   * Returns a new object of class '<em>array data component reference</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>array data component reference</em>'.
   * @generated
   */
  array_data_component_reference createarray_data_component_reference();

  /**
   * Returns a new object of class '<em>basic action</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>basic action</em>'.
   * @generated
   */
  basic_action createbasic_action();

  /**
   * Returns a new object of class '<em>assignment action</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>assignment action</em>'.
   * @generated
   */
  assignment_action createassignment_action();

  /**
   * Returns a new object of class '<em>communication action</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>communication action</em>'.
   * @generated
   */
  communication_action createcommunication_action();

  /**
   * Returns a new object of class '<em>timed action</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>timed action</em>'.
   * @generated
   */
  timed_action createtimed_action();

  /**
   * Returns a new object of class '<em>subprogram parameter list</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>subprogram parameter list</em>'.
   * @generated
   */
  subprogram_parameter_list createsubprogram_parameter_list();

  /**
   * Returns a new object of class '<em>parameter label</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>parameter label</em>'.
   * @generated
   */
  parameter_label createparameter_label();

  /**
   * Returns a new object of class '<em>data component reference</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>data component reference</em>'.
   * @generated
   */
  data_component_reference createdata_component_reference();

  /**
   * Returns a new object of class '<em>name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>name</em>'.
   * @generated
   */
  name createname();

  /**
   * Returns a new object of class '<em>array index</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>array index</em>'.
   * @generated
   */
  array_index createarray_index();

  /**
   * Returns a new object of class '<em>integer value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>integer value</em>'.
   * @generated
   */
  integer_value createinteger_value();

  /**
   * Returns a new object of class '<em>numeric literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>numeric literal</em>'.
   * @generated
   */
  numeric_literal createnumeric_literal();

  /**
   * Returns a new object of class '<em>integer literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>integer literal</em>'.
   * @generated
   */
  integer_literal createinteger_literal();

  /**
   * Returns a new object of class '<em>real literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>real literal</em>'.
   * @generated
   */
  real_literal createreal_literal();

  /**
   * Returns a new object of class '<em>decimal integer literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>decimal integer literal</em>'.
   * @generated
   */
  decimal_integer_literal createdecimal_integer_literal();

  /**
   * Returns a new object of class '<em>decimal real literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>decimal real literal</em>'.
   * @generated
   */
  decimal_real_literal createdecimal_real_literal();

  /**
   * Returns a new object of class '<em>numeral</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>numeral</em>'.
   * @generated
   */
  numeral createnumeral();

  /**
   * Returns a new object of class '<em>exponent</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>exponent</em>'.
   * @generated
   */
  exponent createexponent();

  /**
   * Returns a new object of class '<em>positive exponent</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>positive exponent</em>'.
   * @generated
   */
  positive_exponent createpositive_exponent();

  /**
   * Returns a new object of class '<em>based integer literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>based integer literal</em>'.
   * @generated
   */
  based_integer_literal createbased_integer_literal();

  /**
   * Returns a new object of class '<em>base</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>base</em>'.
   * @generated
   */
  base createbase();

  /**
   * Returns a new object of class '<em>based numeral</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>based numeral</em>'.
   * @generated
   */
  based_numeral createbased_numeral();

  /**
   * Returns a new object of class '<em>behavior time</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>behavior time</em>'.
   * @generated
   */
  behavior_time createbehavior_time();

  /**
   * Returns a new object of class '<em>unit identifier</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>unit identifier</em>'.
   * @generated
   */
  unit_identifier createunit_identifier();

  /**
   * Returns a new object of class '<em>integer range</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>integer range</em>'.
   * @generated
   */
  integer_range createinteger_range();

  /**
   * Returns a new object of class '<em>integer</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>integer</em>'.
   * @generated
   */
  integer createinteger();

  /**
   * Returns a new object of class '<em>value expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>value expression</em>'.
   * @generated
   */
  value_expression createvalue_expression();

  /**
   * Returns a new object of class '<em>relation</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>relation</em>'.
   * @generated
   */
  relation createrelation();

  /**
   * Returns a new object of class '<em>simple expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>simple expression</em>'.
   * @generated
   */
  simple_expression createsimple_expression();

  /**
   * Returns a new object of class '<em>term</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>term</em>'.
   * @generated
   */
  term createterm();

  /**
   * Returns a new object of class '<em>factor</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>factor</em>'.
   * @generated
   */
  factor createfactor();

  /**
   * Returns a new object of class '<em>value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>value</em>'.
   * @generated
   */
  value createvalue();

  /**
   * Returns a new object of class '<em>value variable</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>value variable</em>'.
   * @generated
   */
  value_variable createvalue_variable();

  /**
   * Returns a new object of class '<em>value constant</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>value constant</em>'.
   * @generated
   */
  value_constant createvalue_constant();

  /**
   * Returns a new object of class '<em>logical operator</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>logical operator</em>'.
   * @generated
   */
  logical_operator createlogical_operator();

  /**
   * Returns a new object of class '<em>relational operator</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>relational operator</em>'.
   * @generated
   */
  relational_operator createrelational_operator();

  /**
   * Returns a new object of class '<em>binary adding operator</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>binary adding operator</em>'.
   * @generated
   */
  binary_adding_operator createbinary_adding_operator();

  /**
   * Returns a new object of class '<em>unary adding operator</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>unary adding operator</em>'.
   * @generated
   */
  unary_adding_operator createunary_adding_operator();

  /**
   * Returns a new object of class '<em>multiplying operator</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>multiplying operator</em>'.
   * @generated
   */
  multiplying_operator createmultiplying_operator();

  /**
   * Returns a new object of class '<em>binary numeric operator</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>binary numeric operator</em>'.
   * @generated
   */
  binary_numeric_operator createbinary_numeric_operator();

  /**
   * Returns a new object of class '<em>unary numeric operator</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>unary numeric operator</em>'.
   * @generated
   */
  unary_numeric_operator createunary_numeric_operator();

  /**
   * Returns a new object of class '<em>unary boolean operator</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>unary boolean operator</em>'.
   * @generated
   */
  unary_boolean_operator createunary_boolean_operator();

  /**
   * Returns a new object of class '<em>boolean literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>boolean literal</em>'.
   * @generated
   */
  boolean_literal createboolean_literal();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  BaActionPackage getBaActionPackage();

} //BaActionFactory
