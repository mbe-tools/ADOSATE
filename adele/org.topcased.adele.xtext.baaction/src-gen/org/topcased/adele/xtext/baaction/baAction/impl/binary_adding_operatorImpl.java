/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.binary_adding_operator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>binary adding operator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.binary_adding_operatorImpl#getBaoname1 <em>Baoname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.binary_adding_operatorImpl#getBaoname2 <em>Baoname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class binary_adding_operatorImpl extends MinimalEObjectImpl.Container implements binary_adding_operator
{
  /**
   * The default value of the '{@link #getBaoname1() <em>Baoname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaoname1()
   * @generated
   * @ordered
   */
  protected static final String BAONAME1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBaoname1() <em>Baoname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaoname1()
   * @generated
   * @ordered
   */
  protected String baoname1 = BAONAME1_EDEFAULT;

  /**
   * The default value of the '{@link #getBaoname2() <em>Baoname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaoname2()
   * @generated
   * @ordered
   */
  protected static final String BAONAME2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBaoname2() <em>Baoname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaoname2()
   * @generated
   * @ordered
   */
  protected String baoname2 = BAONAME2_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected binary_adding_operatorImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.BINARY_ADDING_OPERATOR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBaoname1()
  {
    return baoname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBaoname1(String newBaoname1)
  {
    String oldBaoname1 = baoname1;
    baoname1 = newBaoname1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BINARY_ADDING_OPERATOR__BAONAME1, oldBaoname1, baoname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBaoname2()
  {
    return baoname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBaoname2(String newBaoname2)
  {
    String oldBaoname2 = baoname2;
    baoname2 = newBaoname2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BINARY_ADDING_OPERATOR__BAONAME2, oldBaoname2, baoname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.BINARY_ADDING_OPERATOR__BAONAME1:
        return getBaoname1();
      case BaActionPackage.BINARY_ADDING_OPERATOR__BAONAME2:
        return getBaoname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.BINARY_ADDING_OPERATOR__BAONAME1:
        setBaoname1((String)newValue);
        return;
      case BaActionPackage.BINARY_ADDING_OPERATOR__BAONAME2:
        setBaoname2((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BINARY_ADDING_OPERATOR__BAONAME1:
        setBaoname1(BAONAME1_EDEFAULT);
        return;
      case BaActionPackage.BINARY_ADDING_OPERATOR__BAONAME2:
        setBaoname2(BAONAME2_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BINARY_ADDING_OPERATOR__BAONAME1:
        return BAONAME1_EDEFAULT == null ? baoname1 != null : !BAONAME1_EDEFAULT.equals(baoname1);
      case BaActionPackage.BINARY_ADDING_OPERATOR__BAONAME2:
        return BAONAME2_EDEFAULT == null ? baoname2 != null : !BAONAME2_EDEFAULT.equals(baoname2);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (baoname1: ");
    result.append(baoname1);
    result.append(", baoname2: ");
    result.append(baoname2);
    result.append(')');
    return result.toString();
  }

} //binary_adding_operatorImpl
