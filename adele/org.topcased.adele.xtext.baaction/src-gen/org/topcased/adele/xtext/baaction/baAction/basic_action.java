/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>basic action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.basic_action#getBact1 <em>Bact1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.basic_action#getBact2 <em>Bact2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.basic_action#getBact3 <em>Bact3</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbasic_action()
 * @model
 * @generated
 */
public interface basic_action extends EObject
{
  /**
   * Returns the value of the '<em><b>Bact1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bact1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bact1</em>' containment reference.
   * @see #setBact1(assignment_action)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbasic_action_Bact1()
   * @model containment="true"
   * @generated
   */
  assignment_action getBact1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.basic_action#getBact1 <em>Bact1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bact1</em>' containment reference.
   * @see #getBact1()
   * @generated
   */
  void setBact1(assignment_action value);

  /**
   * Returns the value of the '<em><b>Bact2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bact2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bact2</em>' containment reference.
   * @see #setBact2(communication_action)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbasic_action_Bact2()
   * @model containment="true"
   * @generated
   */
  communication_action getBact2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.basic_action#getBact2 <em>Bact2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bact2</em>' containment reference.
   * @see #getBact2()
   * @generated
   */
  void setBact2(communication_action value);

  /**
   * Returns the value of the '<em><b>Bact3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bact3</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bact3</em>' containment reference.
   * @see #setBact3(timed_action)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbasic_action_Bact3()
   * @model containment="true"
   * @generated
   */
  timed_action getBact3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.basic_action#getBact3 <em>Bact3</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bact3</em>' containment reference.
   * @see #getBact3()
   * @generated
   */
  void setBact3(timed_action value);

} // basic_action
