/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>decimal real literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.decimal_real_literal#getDrlname1 <em>Drlname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.decimal_real_literal#getDrlname2 <em>Drlname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.decimal_real_literal#getDrlname3 <em>Drlname3</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getdecimal_real_literal()
 * @model
 * @generated
 */
public interface decimal_real_literal extends EObject
{
  /**
   * Returns the value of the '<em><b>Drlname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Drlname1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Drlname1</em>' containment reference.
   * @see #setDrlname1(numeral)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getdecimal_real_literal_Drlname1()
   * @model containment="true"
   * @generated
   */
  numeral getDrlname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.decimal_real_literal#getDrlname1 <em>Drlname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Drlname1</em>' containment reference.
   * @see #getDrlname1()
   * @generated
   */
  void setDrlname1(numeral value);

  /**
   * Returns the value of the '<em><b>Drlname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Drlname2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Drlname2</em>' containment reference.
   * @see #setDrlname2(numeral)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getdecimal_real_literal_Drlname2()
   * @model containment="true"
   * @generated
   */
  numeral getDrlname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.decimal_real_literal#getDrlname2 <em>Drlname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Drlname2</em>' containment reference.
   * @see #getDrlname2()
   * @generated
   */
  void setDrlname2(numeral value);

  /**
   * Returns the value of the '<em><b>Drlname3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Drlname3</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Drlname3</em>' containment reference.
   * @see #setDrlname3(exponent)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getdecimal_real_literal_Drlname3()
   * @model containment="true"
   * @generated
   */
  exponent getDrlname3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.decimal_real_literal#getDrlname3 <em>Drlname3</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Drlname3</em>' containment reference.
   * @see #getDrlname3()
   * @generated
   */
  void setDrlname3(exponent value);

} // decimal_real_literal
