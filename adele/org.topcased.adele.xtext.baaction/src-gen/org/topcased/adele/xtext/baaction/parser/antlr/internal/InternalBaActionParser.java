package org.topcased.adele.xtext.baaction.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.topcased.adele.xtext.baaction.services.BaActionGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalBaActionParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING_LITERAL", "RULE_DIGIT", "RULE_EXTENDED_DIGIT", "RULE_INT", "RULE_ID", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'{'", "'}'", "'timeout'", "'if'", "'('", "')'", "'elsif'", "'else'", "'end'", "'for'", "':'", "'in'", "'forall'", "'while'", "'do'", "'until'", "';'", "'&'", "','", "':='", "'any'", "'!'", "'>>'", "'?'", "'!<'", "'!>'", "'*!<'", "'*!>'", "'computation'", "'..'", "'.'", "'['", "']'", "'_'", "'E'", "'+'", "'E -'", "'#'", "'ps'", "'ns'", "'us'", "'ms'", "'sec'", "'min'", "'hr'", "'count'", "'fresh'", "'::'", "'and'", "'or'", "'xor'", "'='", "'!='", "'<'", "'<='", "'>'", "'>='", "'-'", "'*'", "'/'", "'mod'", "'rem'", "'**'", "'abs'", "'not'", "'true'", "'false'", "' '"
    };
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int RULE_ID=8;
    public static final int T__66=66;
    public static final int T__67=67;
    public static final int T__64=64;
    public static final int T__29=29;
    public static final int T__65=65;
    public static final int T__28=28;
    public static final int T__62=62;
    public static final int T__27=27;
    public static final int T__63=63;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=13;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int T__61=61;
    public static final int T__60=60;
    public static final int EOF=-1;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__19=19;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__16=16;
    public static final int T__51=51;
    public static final int T__15=15;
    public static final int T__52=52;
    public static final int T__18=18;
    public static final int T__53=53;
    public static final int RULE_STRING_LITERAL=4;
    public static final int T__54=54;
    public static final int T__17=17;
    public static final int T__14=14;
    public static final int T__59=59;
    public static final int RULE_INT=7;
    public static final int T__50=50;
    public static final int RULE_EXTENDED_DIGIT=6;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__80=80;
    public static final int T__46=46;
    public static final int T__81=81;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RULE_SL_COMMENT=11;
    public static final int RULE_ML_COMMENT=10;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_STRING=9;
    public static final int T__32=32;
    public static final int T__71=71;
    public static final int T__33=33;
    public static final int T__72=72;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__70=70;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int RULE_WS=12;
    public static final int T__76=76;
    public static final int RULE_DIGIT=5;
    public static final int T__75=75;
    public static final int T__74=74;
    public static final int T__73=73;
    public static final int T__79=79;
    public static final int T__78=78;
    public static final int T__77=77;

    // delegates
    // delegators


        public InternalBaActionParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalBaActionParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalBaActionParser.tokenNames; }
    public String getGrammarFileName() { return "../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g"; }



     	private BaActionGrammarAccess grammarAccess;
     	
        public InternalBaActionParser(TokenStream input, BaActionGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "behavior_action_block";	
       	}
       	
       	@Override
       	protected BaActionGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRulebehavior_action_block"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:67:1: entryRulebehavior_action_block returns [EObject current=null] : iv_rulebehavior_action_block= rulebehavior_action_block EOF ;
    public final EObject entryRulebehavior_action_block() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebehavior_action_block = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:68:2: (iv_rulebehavior_action_block= rulebehavior_action_block EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:69:2: iv_rulebehavior_action_block= rulebehavior_action_block EOF
            {
             newCompositeNode(grammarAccess.getBehavior_action_blockRule()); 
            pushFollow(FOLLOW_rulebehavior_action_block_in_entryRulebehavior_action_block75);
            iv_rulebehavior_action_block=rulebehavior_action_block();

            state._fsp--;

             current =iv_rulebehavior_action_block; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebehavior_action_block85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebehavior_action_block"


    // $ANTLR start "rulebehavior_action_block"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:76:1: rulebehavior_action_block returns [EObject current=null] : ( ( (lv_bac1_0_0= '{' ) ) ( (lv_bac2_1_0= rulebehavior_actions ) ) ( (lv_bac3_2_0= '}' ) ) ( ( (lv_bac4_3_0= 'timeout' ) ) ( (lv_bac5_4_0= rulebehavior_time ) ) )? ) ;
    public final EObject rulebehavior_action_block() throws RecognitionException {
        EObject current = null;

        Token lv_bac1_0_0=null;
        Token lv_bac3_2_0=null;
        Token lv_bac4_3_0=null;
        EObject lv_bac2_1_0 = null;

        EObject lv_bac5_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:79:28: ( ( ( (lv_bac1_0_0= '{' ) ) ( (lv_bac2_1_0= rulebehavior_actions ) ) ( (lv_bac3_2_0= '}' ) ) ( ( (lv_bac4_3_0= 'timeout' ) ) ( (lv_bac5_4_0= rulebehavior_time ) ) )? ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:80:1: ( ( (lv_bac1_0_0= '{' ) ) ( (lv_bac2_1_0= rulebehavior_actions ) ) ( (lv_bac3_2_0= '}' ) ) ( ( (lv_bac4_3_0= 'timeout' ) ) ( (lv_bac5_4_0= rulebehavior_time ) ) )? )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:80:1: ( ( (lv_bac1_0_0= '{' ) ) ( (lv_bac2_1_0= rulebehavior_actions ) ) ( (lv_bac3_2_0= '}' ) ) ( ( (lv_bac4_3_0= 'timeout' ) ) ( (lv_bac5_4_0= rulebehavior_time ) ) )? )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:80:2: ( (lv_bac1_0_0= '{' ) ) ( (lv_bac2_1_0= rulebehavior_actions ) ) ( (lv_bac3_2_0= '}' ) ) ( ( (lv_bac4_3_0= 'timeout' ) ) ( (lv_bac5_4_0= rulebehavior_time ) ) )?
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:80:2: ( (lv_bac1_0_0= '{' ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:81:1: (lv_bac1_0_0= '{' )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:81:1: (lv_bac1_0_0= '{' )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:82:3: lv_bac1_0_0= '{'
            {
            lv_bac1_0_0=(Token)match(input,14,FOLLOW_14_in_rulebehavior_action_block128); 

                    newLeafNode(lv_bac1_0_0, grammarAccess.getBehavior_action_blockAccess().getBac1LeftCurlyBracketKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getBehavior_action_blockRule());
            	        }
                   		setWithLastConsumed(current, "bac1", lv_bac1_0_0, "{");
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:95:2: ( (lv_bac2_1_0= rulebehavior_actions ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:96:1: (lv_bac2_1_0= rulebehavior_actions )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:96:1: (lv_bac2_1_0= rulebehavior_actions )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:97:3: lv_bac2_1_0= rulebehavior_actions
            {
             
            	        newCompositeNode(grammarAccess.getBehavior_action_blockAccess().getBac2Behavior_actionsParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_rulebehavior_actions_in_rulebehavior_action_block162);
            lv_bac2_1_0=rulebehavior_actions();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBehavior_action_blockRule());
            	        }
                   		set(
                   			current, 
                   			"bac2",
                    		lv_bac2_1_0, 
                    		"behavior_actions");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:113:2: ( (lv_bac3_2_0= '}' ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:114:1: (lv_bac3_2_0= '}' )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:114:1: (lv_bac3_2_0= '}' )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:115:3: lv_bac3_2_0= '}'
            {
            lv_bac3_2_0=(Token)match(input,15,FOLLOW_15_in_rulebehavior_action_block180); 

                    newLeafNode(lv_bac3_2_0, grammarAccess.getBehavior_action_blockAccess().getBac3RightCurlyBracketKeyword_2_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getBehavior_action_blockRule());
            	        }
                   		setWithLastConsumed(current, "bac3", lv_bac3_2_0, "}");
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:128:2: ( ( (lv_bac4_3_0= 'timeout' ) ) ( (lv_bac5_4_0= rulebehavior_time ) ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==16) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:128:3: ( (lv_bac4_3_0= 'timeout' ) ) ( (lv_bac5_4_0= rulebehavior_time ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:128:3: ( (lv_bac4_3_0= 'timeout' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:129:1: (lv_bac4_3_0= 'timeout' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:129:1: (lv_bac4_3_0= 'timeout' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:130:3: lv_bac4_3_0= 'timeout'
                    {
                    lv_bac4_3_0=(Token)match(input,16,FOLLOW_16_in_rulebehavior_action_block212); 

                            newLeafNode(lv_bac4_3_0, grammarAccess.getBehavior_action_blockAccess().getBac4TimeoutKeyword_3_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_action_blockRule());
                    	        }
                           		setWithLastConsumed(current, "bac4", lv_bac4_3_0, "timeout");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:143:2: ( (lv_bac5_4_0= rulebehavior_time ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:144:1: (lv_bac5_4_0= rulebehavior_time )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:144:1: (lv_bac5_4_0= rulebehavior_time )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:145:3: lv_bac5_4_0= rulebehavior_time
                    {
                     
                    	        newCompositeNode(grammarAccess.getBehavior_action_blockAccess().getBac5Behavior_timeParserRuleCall_3_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulebehavior_time_in_rulebehavior_action_block246);
                    lv_bac5_4_0=rulebehavior_time();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBehavior_action_blockRule());
                    	        }
                           		set(
                           			current, 
                           			"bac5",
                            		lv_bac5_4_0, 
                            		"behavior_time");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebehavior_action_block"


    // $ANTLR start "entryRulebehavior_action"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:169:1: entryRulebehavior_action returns [EObject current=null] : iv_rulebehavior_action= rulebehavior_action EOF ;
    public final EObject entryRulebehavior_action() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebehavior_action = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:170:2: (iv_rulebehavior_action= rulebehavior_action EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:171:2: iv_rulebehavior_action= rulebehavior_action EOF
            {
             newCompositeNode(grammarAccess.getBehavior_actionRule()); 
            pushFollow(FOLLOW_rulebehavior_action_in_entryRulebehavior_action284);
            iv_rulebehavior_action=rulebehavior_action();

            state._fsp--;

             current =iv_rulebehavior_action; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebehavior_action294); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebehavior_action"


    // $ANTLR start "rulebehavior_action"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:178:1: rulebehavior_action returns [EObject current=null] : ( ( (lv_ba1_0_0= rulebasic_action ) ) | ( (lv_ba2_1_0= rulebehavior_action_block ) ) | ( ( (lv_ba3_2_0= 'if' ) ) rulebaWS ( (lv_ba3bis_4_0= '(' ) ) ( (lv_ba4_5_0= rulevalue_expression ) ) ( (lv_ba5_6_0= ')' ) ) ( (lv_ba6_7_0= rulebehavior_actions ) ) ( ( (lv_ba7_8_0= 'elsif' ) ) rulebaWS ( (lv_ba7bis_10_0= '(' ) ) ( (lv_ba8_11_0= rulevalue_expression ) ) ( (lv_ba9_12_0= ')' ) ) ( (lv_ba10_13_0= rulebehavior_actions ) ) )* ( ( (lv_ba11_14_0= 'else' ) ) ( (lv_ba12_15_0= rulebehavior_actions ) ) )? ( (lv_ba13_16_0= 'end' ) ) rulebaWS ( (lv_ba13bis_18_0= 'if' ) ) rulebaWS ) | ( ( (lv_ba14_20_0= 'for' ) ) rulebaWS ( (lv_ba14bis_22_0= '(' ) ) rulebaWS ( (lv_ba15_24_0= RULE_STRING_LITERAL ) ) ( (lv_ba16_25_0= ':' ) ) ( (lv_ba17_26_0= RULE_STRING_LITERAL ) ) rulebaWS ( (lv_ba18_28_0= 'in' ) ) ( (lv_ba19_29_0= ruleelement_values ) ) ( (lv_ba20_30_0= ')' ) ) rulebaWS ( (lv_ba20bis_32_0= '{' ) ) ( (lv_ba21_33_0= rulebehavior_actions ) ) ( (lv_ba22_34_0= '}' ) ) ) | ( ( (lv_ba23_35_0= 'forall' ) ) rulebaWS ( (lv_ba23bis_37_0= '(' ) ) rulebaWS ( (lv_ba24_39_0= RULE_STRING_LITERAL ) ) ( (lv_ba25_40_0= ':' ) ) ( (lv_ba26_41_0= RULE_STRING_LITERAL ) ) rulebaWS ( (lv_ba27_43_0= 'in' ) ) ( (lv_ba29_44_0= ruleelement_values ) ) ( (lv_ba30_45_0= ')' ) ) rulebaWS ( (lv_ba30bis_47_0= '{' ) ) ( (lv_ba31_48_0= rulebehavior_actions ) ) ( (lv_ba32_49_0= '}' ) ) ) | ( ( (lv_ba33_50_0= 'while' ) ) rulebaWS ( (lv_ba33bis_52_0= '(' ) ) ( (lv_ba34_53_0= rulevalue_expression ) ) ( (lv_ba35_54_0= ')' ) ) rulebaWS ( (lv_ba35bis_56_0= '{' ) ) ( (lv_ba36_57_0= rulebehavior_actions ) ) ( (lv_ba37_58_0= '}' ) ) ) | ( ( (lv_ba38_59_0= 'do' ) ) ( (lv_ba39_60_0= rulebehavior_actions ) ) ( (lv_ba40_61_0= 'until' ) ) rulebaWS ( (lv_ba40bis_63_0= '(' ) ) ( (lv_ba41_64_0= rulevalue_expression ) ) ( (lv_ba42_65_0= ')' ) ) ) ) ;
    public final EObject rulebehavior_action() throws RecognitionException {
        EObject current = null;

        Token lv_ba3_2_0=null;
        Token lv_ba3bis_4_0=null;
        Token lv_ba5_6_0=null;
        Token lv_ba7_8_0=null;
        Token lv_ba7bis_10_0=null;
        Token lv_ba9_12_0=null;
        Token lv_ba11_14_0=null;
        Token lv_ba13_16_0=null;
        Token lv_ba13bis_18_0=null;
        Token lv_ba14_20_0=null;
        Token lv_ba14bis_22_0=null;
        Token lv_ba15_24_0=null;
        Token lv_ba16_25_0=null;
        Token lv_ba17_26_0=null;
        Token lv_ba18_28_0=null;
        Token lv_ba20_30_0=null;
        Token lv_ba20bis_32_0=null;
        Token lv_ba22_34_0=null;
        Token lv_ba23_35_0=null;
        Token lv_ba23bis_37_0=null;
        Token lv_ba24_39_0=null;
        Token lv_ba25_40_0=null;
        Token lv_ba26_41_0=null;
        Token lv_ba27_43_0=null;
        Token lv_ba30_45_0=null;
        Token lv_ba30bis_47_0=null;
        Token lv_ba32_49_0=null;
        Token lv_ba33_50_0=null;
        Token lv_ba33bis_52_0=null;
        Token lv_ba35_54_0=null;
        Token lv_ba35bis_56_0=null;
        Token lv_ba37_58_0=null;
        Token lv_ba38_59_0=null;
        Token lv_ba40_61_0=null;
        Token lv_ba40bis_63_0=null;
        Token lv_ba42_65_0=null;
        EObject lv_ba1_0_0 = null;

        EObject lv_ba2_1_0 = null;

        EObject lv_ba4_5_0 = null;

        EObject lv_ba6_7_0 = null;

        EObject lv_ba8_11_0 = null;

        EObject lv_ba10_13_0 = null;

        EObject lv_ba12_15_0 = null;

        EObject lv_ba19_29_0 = null;

        EObject lv_ba21_33_0 = null;

        EObject lv_ba29_44_0 = null;

        EObject lv_ba31_48_0 = null;

        EObject lv_ba34_53_0 = null;

        EObject lv_ba36_57_0 = null;

        EObject lv_ba39_60_0 = null;

        EObject lv_ba41_64_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:181:28: ( ( ( (lv_ba1_0_0= rulebasic_action ) ) | ( (lv_ba2_1_0= rulebehavior_action_block ) ) | ( ( (lv_ba3_2_0= 'if' ) ) rulebaWS ( (lv_ba3bis_4_0= '(' ) ) ( (lv_ba4_5_0= rulevalue_expression ) ) ( (lv_ba5_6_0= ')' ) ) ( (lv_ba6_7_0= rulebehavior_actions ) ) ( ( (lv_ba7_8_0= 'elsif' ) ) rulebaWS ( (lv_ba7bis_10_0= '(' ) ) ( (lv_ba8_11_0= rulevalue_expression ) ) ( (lv_ba9_12_0= ')' ) ) ( (lv_ba10_13_0= rulebehavior_actions ) ) )* ( ( (lv_ba11_14_0= 'else' ) ) ( (lv_ba12_15_0= rulebehavior_actions ) ) )? ( (lv_ba13_16_0= 'end' ) ) rulebaWS ( (lv_ba13bis_18_0= 'if' ) ) rulebaWS ) | ( ( (lv_ba14_20_0= 'for' ) ) rulebaWS ( (lv_ba14bis_22_0= '(' ) ) rulebaWS ( (lv_ba15_24_0= RULE_STRING_LITERAL ) ) ( (lv_ba16_25_0= ':' ) ) ( (lv_ba17_26_0= RULE_STRING_LITERAL ) ) rulebaWS ( (lv_ba18_28_0= 'in' ) ) ( (lv_ba19_29_0= ruleelement_values ) ) ( (lv_ba20_30_0= ')' ) ) rulebaWS ( (lv_ba20bis_32_0= '{' ) ) ( (lv_ba21_33_0= rulebehavior_actions ) ) ( (lv_ba22_34_0= '}' ) ) ) | ( ( (lv_ba23_35_0= 'forall' ) ) rulebaWS ( (lv_ba23bis_37_0= '(' ) ) rulebaWS ( (lv_ba24_39_0= RULE_STRING_LITERAL ) ) ( (lv_ba25_40_0= ':' ) ) ( (lv_ba26_41_0= RULE_STRING_LITERAL ) ) rulebaWS ( (lv_ba27_43_0= 'in' ) ) ( (lv_ba29_44_0= ruleelement_values ) ) ( (lv_ba30_45_0= ')' ) ) rulebaWS ( (lv_ba30bis_47_0= '{' ) ) ( (lv_ba31_48_0= rulebehavior_actions ) ) ( (lv_ba32_49_0= '}' ) ) ) | ( ( (lv_ba33_50_0= 'while' ) ) rulebaWS ( (lv_ba33bis_52_0= '(' ) ) ( (lv_ba34_53_0= rulevalue_expression ) ) ( (lv_ba35_54_0= ')' ) ) rulebaWS ( (lv_ba35bis_56_0= '{' ) ) ( (lv_ba36_57_0= rulebehavior_actions ) ) ( (lv_ba37_58_0= '}' ) ) ) | ( ( (lv_ba38_59_0= 'do' ) ) ( (lv_ba39_60_0= rulebehavior_actions ) ) ( (lv_ba40_61_0= 'until' ) ) rulebaWS ( (lv_ba40bis_63_0= '(' ) ) ( (lv_ba41_64_0= rulevalue_expression ) ) ( (lv_ba42_65_0= ')' ) ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:182:1: ( ( (lv_ba1_0_0= rulebasic_action ) ) | ( (lv_ba2_1_0= rulebehavior_action_block ) ) | ( ( (lv_ba3_2_0= 'if' ) ) rulebaWS ( (lv_ba3bis_4_0= '(' ) ) ( (lv_ba4_5_0= rulevalue_expression ) ) ( (lv_ba5_6_0= ')' ) ) ( (lv_ba6_7_0= rulebehavior_actions ) ) ( ( (lv_ba7_8_0= 'elsif' ) ) rulebaWS ( (lv_ba7bis_10_0= '(' ) ) ( (lv_ba8_11_0= rulevalue_expression ) ) ( (lv_ba9_12_0= ')' ) ) ( (lv_ba10_13_0= rulebehavior_actions ) ) )* ( ( (lv_ba11_14_0= 'else' ) ) ( (lv_ba12_15_0= rulebehavior_actions ) ) )? ( (lv_ba13_16_0= 'end' ) ) rulebaWS ( (lv_ba13bis_18_0= 'if' ) ) rulebaWS ) | ( ( (lv_ba14_20_0= 'for' ) ) rulebaWS ( (lv_ba14bis_22_0= '(' ) ) rulebaWS ( (lv_ba15_24_0= RULE_STRING_LITERAL ) ) ( (lv_ba16_25_0= ':' ) ) ( (lv_ba17_26_0= RULE_STRING_LITERAL ) ) rulebaWS ( (lv_ba18_28_0= 'in' ) ) ( (lv_ba19_29_0= ruleelement_values ) ) ( (lv_ba20_30_0= ')' ) ) rulebaWS ( (lv_ba20bis_32_0= '{' ) ) ( (lv_ba21_33_0= rulebehavior_actions ) ) ( (lv_ba22_34_0= '}' ) ) ) | ( ( (lv_ba23_35_0= 'forall' ) ) rulebaWS ( (lv_ba23bis_37_0= '(' ) ) rulebaWS ( (lv_ba24_39_0= RULE_STRING_LITERAL ) ) ( (lv_ba25_40_0= ':' ) ) ( (lv_ba26_41_0= RULE_STRING_LITERAL ) ) rulebaWS ( (lv_ba27_43_0= 'in' ) ) ( (lv_ba29_44_0= ruleelement_values ) ) ( (lv_ba30_45_0= ')' ) ) rulebaWS ( (lv_ba30bis_47_0= '{' ) ) ( (lv_ba31_48_0= rulebehavior_actions ) ) ( (lv_ba32_49_0= '}' ) ) ) | ( ( (lv_ba33_50_0= 'while' ) ) rulebaWS ( (lv_ba33bis_52_0= '(' ) ) ( (lv_ba34_53_0= rulevalue_expression ) ) ( (lv_ba35_54_0= ')' ) ) rulebaWS ( (lv_ba35bis_56_0= '{' ) ) ( (lv_ba36_57_0= rulebehavior_actions ) ) ( (lv_ba37_58_0= '}' ) ) ) | ( ( (lv_ba38_59_0= 'do' ) ) ( (lv_ba39_60_0= rulebehavior_actions ) ) ( (lv_ba40_61_0= 'until' ) ) rulebaWS ( (lv_ba40bis_63_0= '(' ) ) ( (lv_ba41_64_0= rulevalue_expression ) ) ( (lv_ba42_65_0= ')' ) ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:182:1: ( ( (lv_ba1_0_0= rulebasic_action ) ) | ( (lv_ba2_1_0= rulebehavior_action_block ) ) | ( ( (lv_ba3_2_0= 'if' ) ) rulebaWS ( (lv_ba3bis_4_0= '(' ) ) ( (lv_ba4_5_0= rulevalue_expression ) ) ( (lv_ba5_6_0= ')' ) ) ( (lv_ba6_7_0= rulebehavior_actions ) ) ( ( (lv_ba7_8_0= 'elsif' ) ) rulebaWS ( (lv_ba7bis_10_0= '(' ) ) ( (lv_ba8_11_0= rulevalue_expression ) ) ( (lv_ba9_12_0= ')' ) ) ( (lv_ba10_13_0= rulebehavior_actions ) ) )* ( ( (lv_ba11_14_0= 'else' ) ) ( (lv_ba12_15_0= rulebehavior_actions ) ) )? ( (lv_ba13_16_0= 'end' ) ) rulebaWS ( (lv_ba13bis_18_0= 'if' ) ) rulebaWS ) | ( ( (lv_ba14_20_0= 'for' ) ) rulebaWS ( (lv_ba14bis_22_0= '(' ) ) rulebaWS ( (lv_ba15_24_0= RULE_STRING_LITERAL ) ) ( (lv_ba16_25_0= ':' ) ) ( (lv_ba17_26_0= RULE_STRING_LITERAL ) ) rulebaWS ( (lv_ba18_28_0= 'in' ) ) ( (lv_ba19_29_0= ruleelement_values ) ) ( (lv_ba20_30_0= ')' ) ) rulebaWS ( (lv_ba20bis_32_0= '{' ) ) ( (lv_ba21_33_0= rulebehavior_actions ) ) ( (lv_ba22_34_0= '}' ) ) ) | ( ( (lv_ba23_35_0= 'forall' ) ) rulebaWS ( (lv_ba23bis_37_0= '(' ) ) rulebaWS ( (lv_ba24_39_0= RULE_STRING_LITERAL ) ) ( (lv_ba25_40_0= ':' ) ) ( (lv_ba26_41_0= RULE_STRING_LITERAL ) ) rulebaWS ( (lv_ba27_43_0= 'in' ) ) ( (lv_ba29_44_0= ruleelement_values ) ) ( (lv_ba30_45_0= ')' ) ) rulebaWS ( (lv_ba30bis_47_0= '{' ) ) ( (lv_ba31_48_0= rulebehavior_actions ) ) ( (lv_ba32_49_0= '}' ) ) ) | ( ( (lv_ba33_50_0= 'while' ) ) rulebaWS ( (lv_ba33bis_52_0= '(' ) ) ( (lv_ba34_53_0= rulevalue_expression ) ) ( (lv_ba35_54_0= ')' ) ) rulebaWS ( (lv_ba35bis_56_0= '{' ) ) ( (lv_ba36_57_0= rulebehavior_actions ) ) ( (lv_ba37_58_0= '}' ) ) ) | ( ( (lv_ba38_59_0= 'do' ) ) ( (lv_ba39_60_0= rulebehavior_actions ) ) ( (lv_ba40_61_0= 'until' ) ) rulebaWS ( (lv_ba40bis_63_0= '(' ) ) ( (lv_ba41_64_0= rulevalue_expression ) ) ( (lv_ba42_65_0= ')' ) ) ) )
            int alt4=7;
            switch ( input.LA(1) ) {
            case RULE_STRING_LITERAL:
            case 40:
            case 41:
            case 42:
                {
                alt4=1;
                }
                break;
            case 14:
                {
                alt4=2;
                }
                break;
            case 17:
                {
                alt4=3;
                }
                break;
            case 23:
                {
                alt4=4;
                }
                break;
            case 26:
                {
                alt4=5;
                }
                break;
            case 27:
                {
                alt4=6;
                }
                break;
            case 28:
                {
                alt4=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:182:2: ( (lv_ba1_0_0= rulebasic_action ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:182:2: ( (lv_ba1_0_0= rulebasic_action ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:183:1: (lv_ba1_0_0= rulebasic_action )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:183:1: (lv_ba1_0_0= rulebasic_action )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:184:3: lv_ba1_0_0= rulebasic_action
                    {
                     
                    	        newCompositeNode(grammarAccess.getBehavior_actionAccess().getBa1Basic_actionParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_rulebasic_action_in_rulebehavior_action340);
                    lv_ba1_0_0=rulebasic_action();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBehavior_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"ba1",
                            		lv_ba1_0_0, 
                            		"basic_action");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:201:6: ( (lv_ba2_1_0= rulebehavior_action_block ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:201:6: ( (lv_ba2_1_0= rulebehavior_action_block ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:202:1: (lv_ba2_1_0= rulebehavior_action_block )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:202:1: (lv_ba2_1_0= rulebehavior_action_block )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:203:3: lv_ba2_1_0= rulebehavior_action_block
                    {
                     
                    	        newCompositeNode(grammarAccess.getBehavior_actionAccess().getBa2Behavior_action_blockParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulebehavior_action_block_in_rulebehavior_action367);
                    lv_ba2_1_0=rulebehavior_action_block();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBehavior_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"ba2",
                            		lv_ba2_1_0, 
                            		"behavior_action_block");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:220:6: ( ( (lv_ba3_2_0= 'if' ) ) rulebaWS ( (lv_ba3bis_4_0= '(' ) ) ( (lv_ba4_5_0= rulevalue_expression ) ) ( (lv_ba5_6_0= ')' ) ) ( (lv_ba6_7_0= rulebehavior_actions ) ) ( ( (lv_ba7_8_0= 'elsif' ) ) rulebaWS ( (lv_ba7bis_10_0= '(' ) ) ( (lv_ba8_11_0= rulevalue_expression ) ) ( (lv_ba9_12_0= ')' ) ) ( (lv_ba10_13_0= rulebehavior_actions ) ) )* ( ( (lv_ba11_14_0= 'else' ) ) ( (lv_ba12_15_0= rulebehavior_actions ) ) )? ( (lv_ba13_16_0= 'end' ) ) rulebaWS ( (lv_ba13bis_18_0= 'if' ) ) rulebaWS )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:220:6: ( ( (lv_ba3_2_0= 'if' ) ) rulebaWS ( (lv_ba3bis_4_0= '(' ) ) ( (lv_ba4_5_0= rulevalue_expression ) ) ( (lv_ba5_6_0= ')' ) ) ( (lv_ba6_7_0= rulebehavior_actions ) ) ( ( (lv_ba7_8_0= 'elsif' ) ) rulebaWS ( (lv_ba7bis_10_0= '(' ) ) ( (lv_ba8_11_0= rulevalue_expression ) ) ( (lv_ba9_12_0= ')' ) ) ( (lv_ba10_13_0= rulebehavior_actions ) ) )* ( ( (lv_ba11_14_0= 'else' ) ) ( (lv_ba12_15_0= rulebehavior_actions ) ) )? ( (lv_ba13_16_0= 'end' ) ) rulebaWS ( (lv_ba13bis_18_0= 'if' ) ) rulebaWS )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:220:7: ( (lv_ba3_2_0= 'if' ) ) rulebaWS ( (lv_ba3bis_4_0= '(' ) ) ( (lv_ba4_5_0= rulevalue_expression ) ) ( (lv_ba5_6_0= ')' ) ) ( (lv_ba6_7_0= rulebehavior_actions ) ) ( ( (lv_ba7_8_0= 'elsif' ) ) rulebaWS ( (lv_ba7bis_10_0= '(' ) ) ( (lv_ba8_11_0= rulevalue_expression ) ) ( (lv_ba9_12_0= ')' ) ) ( (lv_ba10_13_0= rulebehavior_actions ) ) )* ( ( (lv_ba11_14_0= 'else' ) ) ( (lv_ba12_15_0= rulebehavior_actions ) ) )? ( (lv_ba13_16_0= 'end' ) ) rulebaWS ( (lv_ba13bis_18_0= 'if' ) ) rulebaWS
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:220:7: ( (lv_ba3_2_0= 'if' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:221:1: (lv_ba3_2_0= 'if' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:221:1: (lv_ba3_2_0= 'if' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:222:3: lv_ba3_2_0= 'if'
                    {
                    lv_ba3_2_0=(Token)match(input,17,FOLLOW_17_in_rulebehavior_action392); 

                            newLeafNode(lv_ba3_2_0, grammarAccess.getBehavior_actionAccess().getBa3IfKeyword_2_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba3", lv_ba3_2_0, "if");
                    	    

                    }


                    }

                     
                            newCompositeNode(grammarAccess.getBehavior_actionAccess().getBaWSParserRuleCall_2_1()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulebehavior_action421);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:243:1: ( (lv_ba3bis_4_0= '(' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:244:1: (lv_ba3bis_4_0= '(' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:244:1: (lv_ba3bis_4_0= '(' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:245:3: lv_ba3bis_4_0= '('
                    {
                    lv_ba3bis_4_0=(Token)match(input,18,FOLLOW_18_in_rulebehavior_action438); 

                            newLeafNode(lv_ba3bis_4_0, grammarAccess.getBehavior_actionAccess().getBa3bisLeftParenthesisKeyword_2_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba3bis", lv_ba3bis_4_0, "(");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:258:2: ( (lv_ba4_5_0= rulevalue_expression ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:259:1: (lv_ba4_5_0= rulevalue_expression )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:259:1: (lv_ba4_5_0= rulevalue_expression )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:260:3: lv_ba4_5_0= rulevalue_expression
                    {
                     
                    	        newCompositeNode(grammarAccess.getBehavior_actionAccess().getBa4Value_expressionParserRuleCall_2_3_0()); 
                    	    
                    pushFollow(FOLLOW_rulevalue_expression_in_rulebehavior_action472);
                    lv_ba4_5_0=rulevalue_expression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBehavior_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"ba4",
                            		lv_ba4_5_0, 
                            		"value_expression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:276:2: ( (lv_ba5_6_0= ')' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:277:1: (lv_ba5_6_0= ')' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:277:1: (lv_ba5_6_0= ')' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:278:3: lv_ba5_6_0= ')'
                    {
                    lv_ba5_6_0=(Token)match(input,19,FOLLOW_19_in_rulebehavior_action490); 

                            newLeafNode(lv_ba5_6_0, grammarAccess.getBehavior_actionAccess().getBa5RightParenthesisKeyword_2_4_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba5", lv_ba5_6_0, ")");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:291:2: ( (lv_ba6_7_0= rulebehavior_actions ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:292:1: (lv_ba6_7_0= rulebehavior_actions )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:292:1: (lv_ba6_7_0= rulebehavior_actions )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:293:3: lv_ba6_7_0= rulebehavior_actions
                    {
                     
                    	        newCompositeNode(grammarAccess.getBehavior_actionAccess().getBa6Behavior_actionsParserRuleCall_2_5_0()); 
                    	    
                    pushFollow(FOLLOW_rulebehavior_actions_in_rulebehavior_action524);
                    lv_ba6_7_0=rulebehavior_actions();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBehavior_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"ba6",
                            		lv_ba6_7_0, 
                            		"behavior_actions");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:309:2: ( ( (lv_ba7_8_0= 'elsif' ) ) rulebaWS ( (lv_ba7bis_10_0= '(' ) ) ( (lv_ba8_11_0= rulevalue_expression ) ) ( (lv_ba9_12_0= ')' ) ) ( (lv_ba10_13_0= rulebehavior_actions ) ) )*
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0==20) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:309:3: ( (lv_ba7_8_0= 'elsif' ) ) rulebaWS ( (lv_ba7bis_10_0= '(' ) ) ( (lv_ba8_11_0= rulevalue_expression ) ) ( (lv_ba9_12_0= ')' ) ) ( (lv_ba10_13_0= rulebehavior_actions ) )
                    	    {
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:309:3: ( (lv_ba7_8_0= 'elsif' ) )
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:310:1: (lv_ba7_8_0= 'elsif' )
                    	    {
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:310:1: (lv_ba7_8_0= 'elsif' )
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:311:3: lv_ba7_8_0= 'elsif'
                    	    {
                    	    lv_ba7_8_0=(Token)match(input,20,FOLLOW_20_in_rulebehavior_action543); 

                    	            newLeafNode(lv_ba7_8_0, grammarAccess.getBehavior_actionAccess().getBa7ElsifKeyword_2_6_0_0());
                    	        

                    	    	        if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	    	        }
                    	           		addWithLastConsumed(current, "ba7", lv_ba7_8_0, "elsif");
                    	    	    

                    	    }


                    	    }

                    	     
                    	            newCompositeNode(grammarAccess.getBehavior_actionAccess().getBaWSParserRuleCall_2_6_1()); 
                    	        
                    	    pushFollow(FOLLOW_rulebaWS_in_rulebehavior_action572);
                    	    rulebaWS();

                    	    state._fsp--;

                    	     
                    	            afterParserOrEnumRuleCall();
                    	        
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:332:1: ( (lv_ba7bis_10_0= '(' ) )
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:333:1: (lv_ba7bis_10_0= '(' )
                    	    {
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:333:1: (lv_ba7bis_10_0= '(' )
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:334:3: lv_ba7bis_10_0= '('
                    	    {
                    	    lv_ba7bis_10_0=(Token)match(input,18,FOLLOW_18_in_rulebehavior_action589); 

                    	            newLeafNode(lv_ba7bis_10_0, grammarAccess.getBehavior_actionAccess().getBa7bisLeftParenthesisKeyword_2_6_2_0());
                    	        

                    	    	        if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	    	        }
                    	           		addWithLastConsumed(current, "ba7bis", lv_ba7bis_10_0, "(");
                    	    	    

                    	    }


                    	    }

                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:347:2: ( (lv_ba8_11_0= rulevalue_expression ) )
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:348:1: (lv_ba8_11_0= rulevalue_expression )
                    	    {
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:348:1: (lv_ba8_11_0= rulevalue_expression )
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:349:3: lv_ba8_11_0= rulevalue_expression
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getBehavior_actionAccess().getBa8Value_expressionParserRuleCall_2_6_3_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_rulevalue_expression_in_rulebehavior_action623);
                    	    lv_ba8_11_0=rulevalue_expression();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getBehavior_actionRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"ba8",
                    	            		lv_ba8_11_0, 
                    	            		"value_expression");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }

                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:365:2: ( (lv_ba9_12_0= ')' ) )
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:366:1: (lv_ba9_12_0= ')' )
                    	    {
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:366:1: (lv_ba9_12_0= ')' )
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:367:3: lv_ba9_12_0= ')'
                    	    {
                    	    lv_ba9_12_0=(Token)match(input,19,FOLLOW_19_in_rulebehavior_action641); 

                    	            newLeafNode(lv_ba9_12_0, grammarAccess.getBehavior_actionAccess().getBa9RightParenthesisKeyword_2_6_4_0());
                    	        

                    	    	        if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	    	        }
                    	           		addWithLastConsumed(current, "ba9", lv_ba9_12_0, ")");
                    	    	    

                    	    }


                    	    }

                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:380:2: ( (lv_ba10_13_0= rulebehavior_actions ) )
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:381:1: (lv_ba10_13_0= rulebehavior_actions )
                    	    {
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:381:1: (lv_ba10_13_0= rulebehavior_actions )
                    	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:382:3: lv_ba10_13_0= rulebehavior_actions
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getBehavior_actionAccess().getBa10Behavior_actionsParserRuleCall_2_6_5_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_rulebehavior_actions_in_rulebehavior_action675);
                    	    lv_ba10_13_0=rulebehavior_actions();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getBehavior_actionRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"ba10",
                    	            		lv_ba10_13_0, 
                    	            		"behavior_actions");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:398:4: ( ( (lv_ba11_14_0= 'else' ) ) ( (lv_ba12_15_0= rulebehavior_actions ) ) )?
                    int alt3=2;
                    int LA3_0 = input.LA(1);

                    if ( (LA3_0==21) ) {
                        alt3=1;
                    }
                    switch (alt3) {
                        case 1 :
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:398:5: ( (lv_ba11_14_0= 'else' ) ) ( (lv_ba12_15_0= rulebehavior_actions ) )
                            {
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:398:5: ( (lv_ba11_14_0= 'else' ) )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:399:1: (lv_ba11_14_0= 'else' )
                            {
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:399:1: (lv_ba11_14_0= 'else' )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:400:3: lv_ba11_14_0= 'else'
                            {
                            lv_ba11_14_0=(Token)match(input,21,FOLLOW_21_in_rulebehavior_action696); 

                                    newLeafNode(lv_ba11_14_0, grammarAccess.getBehavior_actionAccess().getBa11ElseKeyword_2_7_0_0());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                            	        }
                                   		setWithLastConsumed(current, "ba11", lv_ba11_14_0, "else");
                            	    

                            }


                            }

                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:413:2: ( (lv_ba12_15_0= rulebehavior_actions ) )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:414:1: (lv_ba12_15_0= rulebehavior_actions )
                            {
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:414:1: (lv_ba12_15_0= rulebehavior_actions )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:415:3: lv_ba12_15_0= rulebehavior_actions
                            {
                             
                            	        newCompositeNode(grammarAccess.getBehavior_actionAccess().getBa12Behavior_actionsParserRuleCall_2_7_1_0()); 
                            	    
                            pushFollow(FOLLOW_rulebehavior_actions_in_rulebehavior_action730);
                            lv_ba12_15_0=rulebehavior_actions();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getBehavior_actionRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"ba12",
                                    		lv_ba12_15_0, 
                                    		"behavior_actions");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:431:4: ( (lv_ba13_16_0= 'end' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:432:1: (lv_ba13_16_0= 'end' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:432:1: (lv_ba13_16_0= 'end' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:433:3: lv_ba13_16_0= 'end'
                    {
                    lv_ba13_16_0=(Token)match(input,22,FOLLOW_22_in_rulebehavior_action750); 

                            newLeafNode(lv_ba13_16_0, grammarAccess.getBehavior_actionAccess().getBa13EndKeyword_2_8_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba13", lv_ba13_16_0, "end");
                    	    

                    }


                    }

                     
                            newCompositeNode(grammarAccess.getBehavior_actionAccess().getBaWSParserRuleCall_2_9()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulebehavior_action779);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:454:1: ( (lv_ba13bis_18_0= 'if' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:455:1: (lv_ba13bis_18_0= 'if' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:455:1: (lv_ba13bis_18_0= 'if' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:456:3: lv_ba13bis_18_0= 'if'
                    {
                    lv_ba13bis_18_0=(Token)match(input,17,FOLLOW_17_in_rulebehavior_action796); 

                            newLeafNode(lv_ba13bis_18_0, grammarAccess.getBehavior_actionAccess().getBa13bisIfKeyword_2_10_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba13bis", lv_ba13bis_18_0, "if");
                    	    

                    }


                    }

                     
                            newCompositeNode(grammarAccess.getBehavior_actionAccess().getBaWSParserRuleCall_2_11()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulebehavior_action825);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        

                    }


                    }
                    break;
                case 4 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:478:6: ( ( (lv_ba14_20_0= 'for' ) ) rulebaWS ( (lv_ba14bis_22_0= '(' ) ) rulebaWS ( (lv_ba15_24_0= RULE_STRING_LITERAL ) ) ( (lv_ba16_25_0= ':' ) ) ( (lv_ba17_26_0= RULE_STRING_LITERAL ) ) rulebaWS ( (lv_ba18_28_0= 'in' ) ) ( (lv_ba19_29_0= ruleelement_values ) ) ( (lv_ba20_30_0= ')' ) ) rulebaWS ( (lv_ba20bis_32_0= '{' ) ) ( (lv_ba21_33_0= rulebehavior_actions ) ) ( (lv_ba22_34_0= '}' ) ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:478:6: ( ( (lv_ba14_20_0= 'for' ) ) rulebaWS ( (lv_ba14bis_22_0= '(' ) ) rulebaWS ( (lv_ba15_24_0= RULE_STRING_LITERAL ) ) ( (lv_ba16_25_0= ':' ) ) ( (lv_ba17_26_0= RULE_STRING_LITERAL ) ) rulebaWS ( (lv_ba18_28_0= 'in' ) ) ( (lv_ba19_29_0= ruleelement_values ) ) ( (lv_ba20_30_0= ')' ) ) rulebaWS ( (lv_ba20bis_32_0= '{' ) ) ( (lv_ba21_33_0= rulebehavior_actions ) ) ( (lv_ba22_34_0= '}' ) ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:478:7: ( (lv_ba14_20_0= 'for' ) ) rulebaWS ( (lv_ba14bis_22_0= '(' ) ) rulebaWS ( (lv_ba15_24_0= RULE_STRING_LITERAL ) ) ( (lv_ba16_25_0= ':' ) ) ( (lv_ba17_26_0= RULE_STRING_LITERAL ) ) rulebaWS ( (lv_ba18_28_0= 'in' ) ) ( (lv_ba19_29_0= ruleelement_values ) ) ( (lv_ba20_30_0= ')' ) ) rulebaWS ( (lv_ba20bis_32_0= '{' ) ) ( (lv_ba21_33_0= rulebehavior_actions ) ) ( (lv_ba22_34_0= '}' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:478:7: ( (lv_ba14_20_0= 'for' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:479:1: (lv_ba14_20_0= 'for' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:479:1: (lv_ba14_20_0= 'for' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:480:3: lv_ba14_20_0= 'for'
                    {
                    lv_ba14_20_0=(Token)match(input,23,FOLLOW_23_in_rulebehavior_action850); 

                            newLeafNode(lv_ba14_20_0, grammarAccess.getBehavior_actionAccess().getBa14ForKeyword_3_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba14", lv_ba14_20_0, "for");
                    	    

                    }


                    }

                     
                            newCompositeNode(grammarAccess.getBehavior_actionAccess().getBaWSParserRuleCall_3_1()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulebehavior_action879);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:501:1: ( (lv_ba14bis_22_0= '(' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:502:1: (lv_ba14bis_22_0= '(' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:502:1: (lv_ba14bis_22_0= '(' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:503:3: lv_ba14bis_22_0= '('
                    {
                    lv_ba14bis_22_0=(Token)match(input,18,FOLLOW_18_in_rulebehavior_action896); 

                            newLeafNode(lv_ba14bis_22_0, grammarAccess.getBehavior_actionAccess().getBa14bisLeftParenthesisKeyword_3_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba14bis", lv_ba14bis_22_0, "(");
                    	    

                    }


                    }

                     
                            newCompositeNode(grammarAccess.getBehavior_actionAccess().getBaWSParserRuleCall_3_3()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulebehavior_action925);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:524:1: ( (lv_ba15_24_0= RULE_STRING_LITERAL ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:525:1: (lv_ba15_24_0= RULE_STRING_LITERAL )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:525:1: (lv_ba15_24_0= RULE_STRING_LITERAL )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:526:3: lv_ba15_24_0= RULE_STRING_LITERAL
                    {
                    lv_ba15_24_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulebehavior_action941); 

                    			newLeafNode(lv_ba15_24_0, grammarAccess.getBehavior_actionAccess().getBa15String_literalTerminalRuleCall_3_4_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"ba15",
                            		lv_ba15_24_0, 
                            		"string_literal");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:542:2: ( (lv_ba16_25_0= ':' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:543:1: (lv_ba16_25_0= ':' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:543:1: (lv_ba16_25_0= ':' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:544:3: lv_ba16_25_0= ':'
                    {
                    lv_ba16_25_0=(Token)match(input,24,FOLLOW_24_in_rulebehavior_action964); 

                            newLeafNode(lv_ba16_25_0, grammarAccess.getBehavior_actionAccess().getBa16ColonKeyword_3_5_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba16", lv_ba16_25_0, ":");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:557:2: ( (lv_ba17_26_0= RULE_STRING_LITERAL ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:558:1: (lv_ba17_26_0= RULE_STRING_LITERAL )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:558:1: (lv_ba17_26_0= RULE_STRING_LITERAL )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:559:3: lv_ba17_26_0= RULE_STRING_LITERAL
                    {
                    lv_ba17_26_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulebehavior_action994); 

                    			newLeafNode(lv_ba17_26_0, grammarAccess.getBehavior_actionAccess().getBa17String_literalTerminalRuleCall_3_6_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"ba17",
                            		lv_ba17_26_0, 
                            		"string_literal");
                    	    

                    }


                    }

                     
                            newCompositeNode(grammarAccess.getBehavior_actionAccess().getBaWSParserRuleCall_3_7()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulebehavior_action1015);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:583:1: ( (lv_ba18_28_0= 'in' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:584:1: (lv_ba18_28_0= 'in' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:584:1: (lv_ba18_28_0= 'in' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:585:3: lv_ba18_28_0= 'in'
                    {
                    lv_ba18_28_0=(Token)match(input,25,FOLLOW_25_in_rulebehavior_action1032); 

                            newLeafNode(lv_ba18_28_0, grammarAccess.getBehavior_actionAccess().getBa18InKeyword_3_8_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba18", lv_ba18_28_0, "in");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:598:2: ( (lv_ba19_29_0= ruleelement_values ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:599:1: (lv_ba19_29_0= ruleelement_values )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:599:1: (lv_ba19_29_0= ruleelement_values )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:600:3: lv_ba19_29_0= ruleelement_values
                    {
                     
                    	        newCompositeNode(grammarAccess.getBehavior_actionAccess().getBa19Element_valuesParserRuleCall_3_9_0()); 
                    	    
                    pushFollow(FOLLOW_ruleelement_values_in_rulebehavior_action1066);
                    lv_ba19_29_0=ruleelement_values();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBehavior_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"ba19",
                            		lv_ba19_29_0, 
                            		"element_values");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:616:2: ( (lv_ba20_30_0= ')' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:617:1: (lv_ba20_30_0= ')' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:617:1: (lv_ba20_30_0= ')' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:618:3: lv_ba20_30_0= ')'
                    {
                    lv_ba20_30_0=(Token)match(input,19,FOLLOW_19_in_rulebehavior_action1084); 

                            newLeafNode(lv_ba20_30_0, grammarAccess.getBehavior_actionAccess().getBa20RightParenthesisKeyword_3_10_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba20", lv_ba20_30_0, ")");
                    	    

                    }


                    }

                     
                            newCompositeNode(grammarAccess.getBehavior_actionAccess().getBaWSParserRuleCall_3_11()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulebehavior_action1113);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:639:1: ( (lv_ba20bis_32_0= '{' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:640:1: (lv_ba20bis_32_0= '{' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:640:1: (lv_ba20bis_32_0= '{' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:641:3: lv_ba20bis_32_0= '{'
                    {
                    lv_ba20bis_32_0=(Token)match(input,14,FOLLOW_14_in_rulebehavior_action1130); 

                            newLeafNode(lv_ba20bis_32_0, grammarAccess.getBehavior_actionAccess().getBa20bisLeftCurlyBracketKeyword_3_12_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba20bis", lv_ba20bis_32_0, "{");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:654:2: ( (lv_ba21_33_0= rulebehavior_actions ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:655:1: (lv_ba21_33_0= rulebehavior_actions )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:655:1: (lv_ba21_33_0= rulebehavior_actions )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:656:3: lv_ba21_33_0= rulebehavior_actions
                    {
                     
                    	        newCompositeNode(grammarAccess.getBehavior_actionAccess().getBa21Behavior_actionsParserRuleCall_3_13_0()); 
                    	    
                    pushFollow(FOLLOW_rulebehavior_actions_in_rulebehavior_action1164);
                    lv_ba21_33_0=rulebehavior_actions();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBehavior_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"ba21",
                            		lv_ba21_33_0, 
                            		"behavior_actions");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:672:2: ( (lv_ba22_34_0= '}' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:673:1: (lv_ba22_34_0= '}' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:673:1: (lv_ba22_34_0= '}' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:674:3: lv_ba22_34_0= '}'
                    {
                    lv_ba22_34_0=(Token)match(input,15,FOLLOW_15_in_rulebehavior_action1182); 

                            newLeafNode(lv_ba22_34_0, grammarAccess.getBehavior_actionAccess().getBa22RightCurlyBracketKeyword_3_14_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba22", lv_ba22_34_0, "}");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:688:6: ( ( (lv_ba23_35_0= 'forall' ) ) rulebaWS ( (lv_ba23bis_37_0= '(' ) ) rulebaWS ( (lv_ba24_39_0= RULE_STRING_LITERAL ) ) ( (lv_ba25_40_0= ':' ) ) ( (lv_ba26_41_0= RULE_STRING_LITERAL ) ) rulebaWS ( (lv_ba27_43_0= 'in' ) ) ( (lv_ba29_44_0= ruleelement_values ) ) ( (lv_ba30_45_0= ')' ) ) rulebaWS ( (lv_ba30bis_47_0= '{' ) ) ( (lv_ba31_48_0= rulebehavior_actions ) ) ( (lv_ba32_49_0= '}' ) ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:688:6: ( ( (lv_ba23_35_0= 'forall' ) ) rulebaWS ( (lv_ba23bis_37_0= '(' ) ) rulebaWS ( (lv_ba24_39_0= RULE_STRING_LITERAL ) ) ( (lv_ba25_40_0= ':' ) ) ( (lv_ba26_41_0= RULE_STRING_LITERAL ) ) rulebaWS ( (lv_ba27_43_0= 'in' ) ) ( (lv_ba29_44_0= ruleelement_values ) ) ( (lv_ba30_45_0= ')' ) ) rulebaWS ( (lv_ba30bis_47_0= '{' ) ) ( (lv_ba31_48_0= rulebehavior_actions ) ) ( (lv_ba32_49_0= '}' ) ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:688:7: ( (lv_ba23_35_0= 'forall' ) ) rulebaWS ( (lv_ba23bis_37_0= '(' ) ) rulebaWS ( (lv_ba24_39_0= RULE_STRING_LITERAL ) ) ( (lv_ba25_40_0= ':' ) ) ( (lv_ba26_41_0= RULE_STRING_LITERAL ) ) rulebaWS ( (lv_ba27_43_0= 'in' ) ) ( (lv_ba29_44_0= ruleelement_values ) ) ( (lv_ba30_45_0= ')' ) ) rulebaWS ( (lv_ba30bis_47_0= '{' ) ) ( (lv_ba31_48_0= rulebehavior_actions ) ) ( (lv_ba32_49_0= '}' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:688:7: ( (lv_ba23_35_0= 'forall' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:689:1: (lv_ba23_35_0= 'forall' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:689:1: (lv_ba23_35_0= 'forall' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:690:3: lv_ba23_35_0= 'forall'
                    {
                    lv_ba23_35_0=(Token)match(input,26,FOLLOW_26_in_rulebehavior_action1221); 

                            newLeafNode(lv_ba23_35_0, grammarAccess.getBehavior_actionAccess().getBa23ForallKeyword_4_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba23", lv_ba23_35_0, "forall");
                    	    

                    }


                    }

                     
                            newCompositeNode(grammarAccess.getBehavior_actionAccess().getBaWSParserRuleCall_4_1()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulebehavior_action1250);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:711:1: ( (lv_ba23bis_37_0= '(' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:712:1: (lv_ba23bis_37_0= '(' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:712:1: (lv_ba23bis_37_0= '(' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:713:3: lv_ba23bis_37_0= '('
                    {
                    lv_ba23bis_37_0=(Token)match(input,18,FOLLOW_18_in_rulebehavior_action1267); 

                            newLeafNode(lv_ba23bis_37_0, grammarAccess.getBehavior_actionAccess().getBa23bisLeftParenthesisKeyword_4_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba23bis", lv_ba23bis_37_0, "(");
                    	    

                    }


                    }

                     
                            newCompositeNode(grammarAccess.getBehavior_actionAccess().getBaWSParserRuleCall_4_3()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulebehavior_action1296);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:734:1: ( (lv_ba24_39_0= RULE_STRING_LITERAL ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:735:1: (lv_ba24_39_0= RULE_STRING_LITERAL )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:735:1: (lv_ba24_39_0= RULE_STRING_LITERAL )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:736:3: lv_ba24_39_0= RULE_STRING_LITERAL
                    {
                    lv_ba24_39_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulebehavior_action1312); 

                    			newLeafNode(lv_ba24_39_0, grammarAccess.getBehavior_actionAccess().getBa24String_literalTerminalRuleCall_4_4_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"ba24",
                            		lv_ba24_39_0, 
                            		"string_literal");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:752:2: ( (lv_ba25_40_0= ':' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:753:1: (lv_ba25_40_0= ':' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:753:1: (lv_ba25_40_0= ':' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:754:3: lv_ba25_40_0= ':'
                    {
                    lv_ba25_40_0=(Token)match(input,24,FOLLOW_24_in_rulebehavior_action1335); 

                            newLeafNode(lv_ba25_40_0, grammarAccess.getBehavior_actionAccess().getBa25ColonKeyword_4_5_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba25", lv_ba25_40_0, ":");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:767:2: ( (lv_ba26_41_0= RULE_STRING_LITERAL ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:768:1: (lv_ba26_41_0= RULE_STRING_LITERAL )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:768:1: (lv_ba26_41_0= RULE_STRING_LITERAL )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:769:3: lv_ba26_41_0= RULE_STRING_LITERAL
                    {
                    lv_ba26_41_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulebehavior_action1365); 

                    			newLeafNode(lv_ba26_41_0, grammarAccess.getBehavior_actionAccess().getBa26String_literalTerminalRuleCall_4_6_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"ba26",
                            		lv_ba26_41_0, 
                            		"string_literal");
                    	    

                    }


                    }

                     
                            newCompositeNode(grammarAccess.getBehavior_actionAccess().getBaWSParserRuleCall_4_7()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulebehavior_action1386);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:793:1: ( (lv_ba27_43_0= 'in' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:794:1: (lv_ba27_43_0= 'in' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:794:1: (lv_ba27_43_0= 'in' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:795:3: lv_ba27_43_0= 'in'
                    {
                    lv_ba27_43_0=(Token)match(input,25,FOLLOW_25_in_rulebehavior_action1403); 

                            newLeafNode(lv_ba27_43_0, grammarAccess.getBehavior_actionAccess().getBa27InKeyword_4_8_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba27", lv_ba27_43_0, "in");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:808:2: ( (lv_ba29_44_0= ruleelement_values ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:809:1: (lv_ba29_44_0= ruleelement_values )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:809:1: (lv_ba29_44_0= ruleelement_values )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:810:3: lv_ba29_44_0= ruleelement_values
                    {
                     
                    	        newCompositeNode(grammarAccess.getBehavior_actionAccess().getBa29Element_valuesParserRuleCall_4_9_0()); 
                    	    
                    pushFollow(FOLLOW_ruleelement_values_in_rulebehavior_action1437);
                    lv_ba29_44_0=ruleelement_values();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBehavior_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"ba29",
                            		lv_ba29_44_0, 
                            		"element_values");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:826:2: ( (lv_ba30_45_0= ')' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:827:1: (lv_ba30_45_0= ')' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:827:1: (lv_ba30_45_0= ')' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:828:3: lv_ba30_45_0= ')'
                    {
                    lv_ba30_45_0=(Token)match(input,19,FOLLOW_19_in_rulebehavior_action1455); 

                            newLeafNode(lv_ba30_45_0, grammarAccess.getBehavior_actionAccess().getBa30RightParenthesisKeyword_4_10_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba30", lv_ba30_45_0, ")");
                    	    

                    }


                    }

                     
                            newCompositeNode(grammarAccess.getBehavior_actionAccess().getBaWSParserRuleCall_4_11()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulebehavior_action1484);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:849:1: ( (lv_ba30bis_47_0= '{' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:850:1: (lv_ba30bis_47_0= '{' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:850:1: (lv_ba30bis_47_0= '{' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:851:3: lv_ba30bis_47_0= '{'
                    {
                    lv_ba30bis_47_0=(Token)match(input,14,FOLLOW_14_in_rulebehavior_action1501); 

                            newLeafNode(lv_ba30bis_47_0, grammarAccess.getBehavior_actionAccess().getBa30bisLeftCurlyBracketKeyword_4_12_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba30bis", lv_ba30bis_47_0, "{");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:864:2: ( (lv_ba31_48_0= rulebehavior_actions ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:865:1: (lv_ba31_48_0= rulebehavior_actions )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:865:1: (lv_ba31_48_0= rulebehavior_actions )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:866:3: lv_ba31_48_0= rulebehavior_actions
                    {
                     
                    	        newCompositeNode(grammarAccess.getBehavior_actionAccess().getBa31Behavior_actionsParserRuleCall_4_13_0()); 
                    	    
                    pushFollow(FOLLOW_rulebehavior_actions_in_rulebehavior_action1535);
                    lv_ba31_48_0=rulebehavior_actions();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBehavior_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"ba31",
                            		lv_ba31_48_0, 
                            		"behavior_actions");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:882:2: ( (lv_ba32_49_0= '}' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:883:1: (lv_ba32_49_0= '}' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:883:1: (lv_ba32_49_0= '}' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:884:3: lv_ba32_49_0= '}'
                    {
                    lv_ba32_49_0=(Token)match(input,15,FOLLOW_15_in_rulebehavior_action1553); 

                            newLeafNode(lv_ba32_49_0, grammarAccess.getBehavior_actionAccess().getBa32RightCurlyBracketKeyword_4_14_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba32", lv_ba32_49_0, "}");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 6 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:898:6: ( ( (lv_ba33_50_0= 'while' ) ) rulebaWS ( (lv_ba33bis_52_0= '(' ) ) ( (lv_ba34_53_0= rulevalue_expression ) ) ( (lv_ba35_54_0= ')' ) ) rulebaWS ( (lv_ba35bis_56_0= '{' ) ) ( (lv_ba36_57_0= rulebehavior_actions ) ) ( (lv_ba37_58_0= '}' ) ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:898:6: ( ( (lv_ba33_50_0= 'while' ) ) rulebaWS ( (lv_ba33bis_52_0= '(' ) ) ( (lv_ba34_53_0= rulevalue_expression ) ) ( (lv_ba35_54_0= ')' ) ) rulebaWS ( (lv_ba35bis_56_0= '{' ) ) ( (lv_ba36_57_0= rulebehavior_actions ) ) ( (lv_ba37_58_0= '}' ) ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:898:7: ( (lv_ba33_50_0= 'while' ) ) rulebaWS ( (lv_ba33bis_52_0= '(' ) ) ( (lv_ba34_53_0= rulevalue_expression ) ) ( (lv_ba35_54_0= ')' ) ) rulebaWS ( (lv_ba35bis_56_0= '{' ) ) ( (lv_ba36_57_0= rulebehavior_actions ) ) ( (lv_ba37_58_0= '}' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:898:7: ( (lv_ba33_50_0= 'while' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:899:1: (lv_ba33_50_0= 'while' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:899:1: (lv_ba33_50_0= 'while' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:900:3: lv_ba33_50_0= 'while'
                    {
                    lv_ba33_50_0=(Token)match(input,27,FOLLOW_27_in_rulebehavior_action1592); 

                            newLeafNode(lv_ba33_50_0, grammarAccess.getBehavior_actionAccess().getBa33WhileKeyword_5_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba33", lv_ba33_50_0, "while");
                    	    

                    }


                    }

                     
                            newCompositeNode(grammarAccess.getBehavior_actionAccess().getBaWSParserRuleCall_5_1()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulebehavior_action1621);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:921:1: ( (lv_ba33bis_52_0= '(' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:922:1: (lv_ba33bis_52_0= '(' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:922:1: (lv_ba33bis_52_0= '(' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:923:3: lv_ba33bis_52_0= '('
                    {
                    lv_ba33bis_52_0=(Token)match(input,18,FOLLOW_18_in_rulebehavior_action1638); 

                            newLeafNode(lv_ba33bis_52_0, grammarAccess.getBehavior_actionAccess().getBa33bisLeftParenthesisKeyword_5_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba33bis", lv_ba33bis_52_0, "(");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:936:2: ( (lv_ba34_53_0= rulevalue_expression ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:937:1: (lv_ba34_53_0= rulevalue_expression )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:937:1: (lv_ba34_53_0= rulevalue_expression )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:938:3: lv_ba34_53_0= rulevalue_expression
                    {
                     
                    	        newCompositeNode(grammarAccess.getBehavior_actionAccess().getBa34Value_expressionParserRuleCall_5_3_0()); 
                    	    
                    pushFollow(FOLLOW_rulevalue_expression_in_rulebehavior_action1672);
                    lv_ba34_53_0=rulevalue_expression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBehavior_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"ba34",
                            		lv_ba34_53_0, 
                            		"value_expression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:954:2: ( (lv_ba35_54_0= ')' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:955:1: (lv_ba35_54_0= ')' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:955:1: (lv_ba35_54_0= ')' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:956:3: lv_ba35_54_0= ')'
                    {
                    lv_ba35_54_0=(Token)match(input,19,FOLLOW_19_in_rulebehavior_action1690); 

                            newLeafNode(lv_ba35_54_0, grammarAccess.getBehavior_actionAccess().getBa35RightParenthesisKeyword_5_4_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba35", lv_ba35_54_0, ")");
                    	    

                    }


                    }

                     
                            newCompositeNode(grammarAccess.getBehavior_actionAccess().getBaWSParserRuleCall_5_5()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulebehavior_action1719);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:977:1: ( (lv_ba35bis_56_0= '{' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:978:1: (lv_ba35bis_56_0= '{' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:978:1: (lv_ba35bis_56_0= '{' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:979:3: lv_ba35bis_56_0= '{'
                    {
                    lv_ba35bis_56_0=(Token)match(input,14,FOLLOW_14_in_rulebehavior_action1736); 

                            newLeafNode(lv_ba35bis_56_0, grammarAccess.getBehavior_actionAccess().getBa35bisLeftCurlyBracketKeyword_5_6_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba35bis", lv_ba35bis_56_0, "{");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:992:2: ( (lv_ba36_57_0= rulebehavior_actions ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:993:1: (lv_ba36_57_0= rulebehavior_actions )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:993:1: (lv_ba36_57_0= rulebehavior_actions )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:994:3: lv_ba36_57_0= rulebehavior_actions
                    {
                     
                    	        newCompositeNode(grammarAccess.getBehavior_actionAccess().getBa36Behavior_actionsParserRuleCall_5_7_0()); 
                    	    
                    pushFollow(FOLLOW_rulebehavior_actions_in_rulebehavior_action1770);
                    lv_ba36_57_0=rulebehavior_actions();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBehavior_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"ba36",
                            		lv_ba36_57_0, 
                            		"behavior_actions");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1010:2: ( (lv_ba37_58_0= '}' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1011:1: (lv_ba37_58_0= '}' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1011:1: (lv_ba37_58_0= '}' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1012:3: lv_ba37_58_0= '}'
                    {
                    lv_ba37_58_0=(Token)match(input,15,FOLLOW_15_in_rulebehavior_action1788); 

                            newLeafNode(lv_ba37_58_0, grammarAccess.getBehavior_actionAccess().getBa37RightCurlyBracketKeyword_5_8_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba37", lv_ba37_58_0, "}");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 7 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1026:6: ( ( (lv_ba38_59_0= 'do' ) ) ( (lv_ba39_60_0= rulebehavior_actions ) ) ( (lv_ba40_61_0= 'until' ) ) rulebaWS ( (lv_ba40bis_63_0= '(' ) ) ( (lv_ba41_64_0= rulevalue_expression ) ) ( (lv_ba42_65_0= ')' ) ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1026:6: ( ( (lv_ba38_59_0= 'do' ) ) ( (lv_ba39_60_0= rulebehavior_actions ) ) ( (lv_ba40_61_0= 'until' ) ) rulebaWS ( (lv_ba40bis_63_0= '(' ) ) ( (lv_ba41_64_0= rulevalue_expression ) ) ( (lv_ba42_65_0= ')' ) ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1026:7: ( (lv_ba38_59_0= 'do' ) ) ( (lv_ba39_60_0= rulebehavior_actions ) ) ( (lv_ba40_61_0= 'until' ) ) rulebaWS ( (lv_ba40bis_63_0= '(' ) ) ( (lv_ba41_64_0= rulevalue_expression ) ) ( (lv_ba42_65_0= ')' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1026:7: ( (lv_ba38_59_0= 'do' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1027:1: (lv_ba38_59_0= 'do' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1027:1: (lv_ba38_59_0= 'do' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1028:3: lv_ba38_59_0= 'do'
                    {
                    lv_ba38_59_0=(Token)match(input,28,FOLLOW_28_in_rulebehavior_action1827); 

                            newLeafNode(lv_ba38_59_0, grammarAccess.getBehavior_actionAccess().getBa38DoKeyword_6_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba38", lv_ba38_59_0, "do");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1041:2: ( (lv_ba39_60_0= rulebehavior_actions ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1042:1: (lv_ba39_60_0= rulebehavior_actions )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1042:1: (lv_ba39_60_0= rulebehavior_actions )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1043:3: lv_ba39_60_0= rulebehavior_actions
                    {
                     
                    	        newCompositeNode(grammarAccess.getBehavior_actionAccess().getBa39Behavior_actionsParserRuleCall_6_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulebehavior_actions_in_rulebehavior_action1861);
                    lv_ba39_60_0=rulebehavior_actions();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBehavior_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"ba39",
                            		lv_ba39_60_0, 
                            		"behavior_actions");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1059:2: ( (lv_ba40_61_0= 'until' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1060:1: (lv_ba40_61_0= 'until' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1060:1: (lv_ba40_61_0= 'until' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1061:3: lv_ba40_61_0= 'until'
                    {
                    lv_ba40_61_0=(Token)match(input,29,FOLLOW_29_in_rulebehavior_action1879); 

                            newLeafNode(lv_ba40_61_0, grammarAccess.getBehavior_actionAccess().getBa40UntilKeyword_6_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba40", lv_ba40_61_0, "until");
                    	    

                    }


                    }

                     
                            newCompositeNode(grammarAccess.getBehavior_actionAccess().getBaWSParserRuleCall_6_3()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulebehavior_action1908);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1082:1: ( (lv_ba40bis_63_0= '(' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1083:1: (lv_ba40bis_63_0= '(' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1083:1: (lv_ba40bis_63_0= '(' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1084:3: lv_ba40bis_63_0= '('
                    {
                    lv_ba40bis_63_0=(Token)match(input,18,FOLLOW_18_in_rulebehavior_action1925); 

                            newLeafNode(lv_ba40bis_63_0, grammarAccess.getBehavior_actionAccess().getBa40bisLeftParenthesisKeyword_6_4_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba40bis", lv_ba40bis_63_0, "(");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1097:2: ( (lv_ba41_64_0= rulevalue_expression ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1098:1: (lv_ba41_64_0= rulevalue_expression )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1098:1: (lv_ba41_64_0= rulevalue_expression )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1099:3: lv_ba41_64_0= rulevalue_expression
                    {
                     
                    	        newCompositeNode(grammarAccess.getBehavior_actionAccess().getBa41Value_expressionParserRuleCall_6_5_0()); 
                    	    
                    pushFollow(FOLLOW_rulevalue_expression_in_rulebehavior_action1959);
                    lv_ba41_64_0=rulevalue_expression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBehavior_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"ba41",
                            		lv_ba41_64_0, 
                            		"value_expression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1115:2: ( (lv_ba42_65_0= ')' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1116:1: (lv_ba42_65_0= ')' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1116:1: (lv_ba42_65_0= ')' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1117:3: lv_ba42_65_0= ')'
                    {
                    lv_ba42_65_0=(Token)match(input,19,FOLLOW_19_in_rulebehavior_action1977); 

                            newLeafNode(lv_ba42_65_0, grammarAccess.getBehavior_actionAccess().getBa42RightParenthesisKeyword_6_6_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBehavior_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ba42", lv_ba42_65_0, ")");
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebehavior_action"


    // $ANTLR start "entryRulebehavior_actions"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1138:1: entryRulebehavior_actions returns [EObject current=null] : iv_rulebehavior_actions= rulebehavior_actions EOF ;
    public final EObject entryRulebehavior_actions() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebehavior_actions = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1139:2: (iv_rulebehavior_actions= rulebehavior_actions EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1140:2: iv_rulebehavior_actions= rulebehavior_actions EOF
            {
             newCompositeNode(grammarAccess.getBehavior_actionsRule()); 
            pushFollow(FOLLOW_rulebehavior_actions_in_entryRulebehavior_actions2027);
            iv_rulebehavior_actions=rulebehavior_actions();

            state._fsp--;

             current =iv_rulebehavior_actions; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebehavior_actions2037); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebehavior_actions"


    // $ANTLR start "rulebehavior_actions"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1147:1: rulebehavior_actions returns [EObject current=null] : ( rulebaWS ( (lv_bas1_1_0= rulebehavior_action ) ) ( ( ( (lv_baseq2_2_0= ';' ) ) | ( (lv_baseq4_3_0= '&' ) ) ) ( (lv_baseq3_4_0= rulebehavior_action ) ) )* rulebaWS ) ;
    public final EObject rulebehavior_actions() throws RecognitionException {
        EObject current = null;

        Token lv_baseq2_2_0=null;
        Token lv_baseq4_3_0=null;
        EObject lv_bas1_1_0 = null;

        EObject lv_baseq3_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1150:28: ( ( rulebaWS ( (lv_bas1_1_0= rulebehavior_action ) ) ( ( ( (lv_baseq2_2_0= ';' ) ) | ( (lv_baseq4_3_0= '&' ) ) ) ( (lv_baseq3_4_0= rulebehavior_action ) ) )* rulebaWS ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1151:1: ( rulebaWS ( (lv_bas1_1_0= rulebehavior_action ) ) ( ( ( (lv_baseq2_2_0= ';' ) ) | ( (lv_baseq4_3_0= '&' ) ) ) ( (lv_baseq3_4_0= rulebehavior_action ) ) )* rulebaWS )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1151:1: ( rulebaWS ( (lv_bas1_1_0= rulebehavior_action ) ) ( ( ( (lv_baseq2_2_0= ';' ) ) | ( (lv_baseq4_3_0= '&' ) ) ) ( (lv_baseq3_4_0= rulebehavior_action ) ) )* rulebaWS )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1152:5: rulebaWS ( (lv_bas1_1_0= rulebehavior_action ) ) ( ( ( (lv_baseq2_2_0= ';' ) ) | ( (lv_baseq4_3_0= '&' ) ) ) ( (lv_baseq3_4_0= rulebehavior_action ) ) )* rulebaWS
            {
             
                    newCompositeNode(grammarAccess.getBehavior_actionsAccess().getBaWSParserRuleCall_0()); 
                
            pushFollow(FOLLOW_rulebaWS_in_rulebehavior_actions2078);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1159:1: ( (lv_bas1_1_0= rulebehavior_action ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1160:1: (lv_bas1_1_0= rulebehavior_action )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1160:1: (lv_bas1_1_0= rulebehavior_action )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1161:3: lv_bas1_1_0= rulebehavior_action
            {
             
            	        newCompositeNode(grammarAccess.getBehavior_actionsAccess().getBas1Behavior_actionParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_rulebehavior_action_in_rulebehavior_actions2098);
            lv_bas1_1_0=rulebehavior_action();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBehavior_actionsRule());
            	        }
                   		set(
                   			current, 
                   			"bas1",
                    		lv_bas1_1_0, 
                    		"behavior_action");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1177:2: ( ( ( (lv_baseq2_2_0= ';' ) ) | ( (lv_baseq4_3_0= '&' ) ) ) ( (lv_baseq3_4_0= rulebehavior_action ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>=30 && LA6_0<=31)) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1177:3: ( ( (lv_baseq2_2_0= ';' ) ) | ( (lv_baseq4_3_0= '&' ) ) ) ( (lv_baseq3_4_0= rulebehavior_action ) )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1177:3: ( ( (lv_baseq2_2_0= ';' ) ) | ( (lv_baseq4_3_0= '&' ) ) )
            	    int alt5=2;
            	    int LA5_0 = input.LA(1);

            	    if ( (LA5_0==30) ) {
            	        alt5=1;
            	    }
            	    else if ( (LA5_0==31) ) {
            	        alt5=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 5, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt5) {
            	        case 1 :
            	            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1177:4: ( (lv_baseq2_2_0= ';' ) )
            	            {
            	            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1177:4: ( (lv_baseq2_2_0= ';' ) )
            	            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1178:1: (lv_baseq2_2_0= ';' )
            	            {
            	            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1178:1: (lv_baseq2_2_0= ';' )
            	            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1179:3: lv_baseq2_2_0= ';'
            	            {
            	            lv_baseq2_2_0=(Token)match(input,30,FOLLOW_30_in_rulebehavior_actions2118); 

            	                    newLeafNode(lv_baseq2_2_0, grammarAccess.getBehavior_actionsAccess().getBaseq2SemicolonKeyword_2_0_0_0());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getBehavior_actionsRule());
            	            	        }
            	                   		addWithLastConsumed(current, "baseq2", lv_baseq2_2_0, ";");
            	            	    

            	            }


            	            }


            	            }
            	            break;
            	        case 2 :
            	            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1193:6: ( (lv_baseq4_3_0= '&' ) )
            	            {
            	            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1193:6: ( (lv_baseq4_3_0= '&' ) )
            	            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1194:1: (lv_baseq4_3_0= '&' )
            	            {
            	            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1194:1: (lv_baseq4_3_0= '&' )
            	            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1195:3: lv_baseq4_3_0= '&'
            	            {
            	            lv_baseq4_3_0=(Token)match(input,31,FOLLOW_31_in_rulebehavior_actions2155); 

            	                    newLeafNode(lv_baseq4_3_0, grammarAccess.getBehavior_actionsAccess().getBaseq4AmpersandKeyword_2_0_1_0());
            	                

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getBehavior_actionsRule());
            	            	        }
            	                   		addWithLastConsumed(current, "baseq4", lv_baseq4_3_0, "&");
            	            	    

            	            }


            	            }


            	            }
            	            break;

            	    }

            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1208:3: ( (lv_baseq3_4_0= rulebehavior_action ) )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1209:1: (lv_baseq3_4_0= rulebehavior_action )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1209:1: (lv_baseq3_4_0= rulebehavior_action )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1210:3: lv_baseq3_4_0= rulebehavior_action
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getBehavior_actionsAccess().getBaseq3Behavior_actionParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulebehavior_action_in_rulebehavior_actions2190);
            	    lv_baseq3_4_0=rulebehavior_action();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getBehavior_actionsRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"baseq3",
            	            		lv_baseq3_4_0, 
            	            		"behavior_action");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             
                    newCompositeNode(grammarAccess.getBehavior_actionsAccess().getBaWSParserRuleCall_3()); 
                
            pushFollow(FOLLOW_rulebaWS_in_rulebehavior_actions2208);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebehavior_actions"


    // $ANTLR start "entryRuleelement_values"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1246:1: entryRuleelement_values returns [EObject current=null] : iv_ruleelement_values= ruleelement_values EOF ;
    public final EObject entryRuleelement_values() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleelement_values = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1247:2: (iv_ruleelement_values= ruleelement_values EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1248:2: iv_ruleelement_values= ruleelement_values EOF
            {
             newCompositeNode(grammarAccess.getElement_valuesRule()); 
            pushFollow(FOLLOW_ruleelement_values_in_entryRuleelement_values2247);
            iv_ruleelement_values=ruleelement_values();

            state._fsp--;

             current =iv_ruleelement_values; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleelement_values2257); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleelement_values"


    // $ANTLR start "ruleelement_values"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1255:1: ruleelement_values returns [EObject current=null] : ( ( (lv_ev1_0_0= ruleinteger_range ) ) | ( rulebaWS ( (lv_ev2_2_0= RULE_STRING_LITERAL ) ) rulebaWS ) | ( (lv_ev3_4_0= rulearray_data_component_reference ) ) ) ;
    public final EObject ruleelement_values() throws RecognitionException {
        EObject current = null;

        Token lv_ev2_2_0=null;
        EObject lv_ev1_0_0 = null;

        EObject lv_ev3_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1258:28: ( ( ( (lv_ev1_0_0= ruleinteger_range ) ) | ( rulebaWS ( (lv_ev2_2_0= RULE_STRING_LITERAL ) ) rulebaWS ) | ( (lv_ev3_4_0= rulearray_data_component_reference ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1259:1: ( ( (lv_ev1_0_0= ruleinteger_range ) ) | ( rulebaWS ( (lv_ev2_2_0= RULE_STRING_LITERAL ) ) rulebaWS ) | ( (lv_ev3_4_0= rulearray_data_component_reference ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1259:1: ( ( (lv_ev1_0_0= ruleinteger_range ) ) | ( rulebaWS ( (lv_ev2_2_0= RULE_STRING_LITERAL ) ) rulebaWS ) | ( (lv_ev3_4_0= rulearray_data_component_reference ) ) )
            int alt7=3;
            alt7 = dfa7.predict(input);
            switch (alt7) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1259:2: ( (lv_ev1_0_0= ruleinteger_range ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1259:2: ( (lv_ev1_0_0= ruleinteger_range ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1260:1: (lv_ev1_0_0= ruleinteger_range )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1260:1: (lv_ev1_0_0= ruleinteger_range )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1261:3: lv_ev1_0_0= ruleinteger_range
                    {
                     
                    	        newCompositeNode(grammarAccess.getElement_valuesAccess().getEv1Integer_rangeParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleinteger_range_in_ruleelement_values2303);
                    lv_ev1_0_0=ruleinteger_range();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getElement_valuesRule());
                    	        }
                           		set(
                           			current, 
                           			"ev1",
                            		lv_ev1_0_0, 
                            		"integer_range");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1278:6: ( rulebaWS ( (lv_ev2_2_0= RULE_STRING_LITERAL ) ) rulebaWS )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1278:6: ( rulebaWS ( (lv_ev2_2_0= RULE_STRING_LITERAL ) ) rulebaWS )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1279:5: rulebaWS ( (lv_ev2_2_0= RULE_STRING_LITERAL ) ) rulebaWS
                    {
                     
                            newCompositeNode(grammarAccess.getElement_valuesAccess().getBaWSParserRuleCall_1_0()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_ruleelement_values2326);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1286:1: ( (lv_ev2_2_0= RULE_STRING_LITERAL ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1287:1: (lv_ev2_2_0= RULE_STRING_LITERAL )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1287:1: (lv_ev2_2_0= RULE_STRING_LITERAL )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1288:3: lv_ev2_2_0= RULE_STRING_LITERAL
                    {
                    lv_ev2_2_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_ruleelement_values2342); 

                    			newLeafNode(lv_ev2_2_0, grammarAccess.getElement_valuesAccess().getEv2String_literalTerminalRuleCall_1_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getElement_valuesRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"ev2",
                            		lv_ev2_2_0, 
                            		"string_literal");
                    	    

                    }


                    }

                     
                            newCompositeNode(grammarAccess.getElement_valuesAccess().getBaWSParserRuleCall_1_2()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_ruleelement_values2363);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        

                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1313:6: ( (lv_ev3_4_0= rulearray_data_component_reference ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1313:6: ( (lv_ev3_4_0= rulearray_data_component_reference ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1314:1: (lv_ev3_4_0= rulearray_data_component_reference )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1314:1: (lv_ev3_4_0= rulearray_data_component_reference )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1315:3: lv_ev3_4_0= rulearray_data_component_reference
                    {
                     
                    	        newCompositeNode(grammarAccess.getElement_valuesAccess().getEv3Array_data_component_referenceParserRuleCall_2_0()); 
                    	    
                    pushFollow(FOLLOW_rulearray_data_component_reference_in_ruleelement_values2390);
                    lv_ev3_4_0=rulearray_data_component_reference();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getElement_valuesRule());
                    	        }
                           		set(
                           			current, 
                           			"ev3",
                            		lv_ev3_4_0, 
                            		"array_data_component_reference");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleelement_values"


    // $ANTLR start "entryRulearray_data_component_reference"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1339:1: entryRulearray_data_component_reference returns [EObject current=null] : iv_rulearray_data_component_reference= rulearray_data_component_reference EOF ;
    public final EObject entryRulearray_data_component_reference() throws RecognitionException {
        EObject current = null;

        EObject iv_rulearray_data_component_reference = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1340:2: (iv_rulearray_data_component_reference= rulearray_data_component_reference EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1341:2: iv_rulearray_data_component_reference= rulearray_data_component_reference EOF
            {
             newCompositeNode(grammarAccess.getArray_data_component_referenceRule()); 
            pushFollow(FOLLOW_rulearray_data_component_reference_in_entryRulearray_data_component_reference2426);
            iv_rulearray_data_component_reference=rulearray_data_component_reference();

            state._fsp--;

             current =iv_rulearray_data_component_reference; 
            match(input,EOF,FOLLOW_EOF_in_entryRulearray_data_component_reference2436); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulearray_data_component_reference"


    // $ANTLR start "rulearray_data_component_reference"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1348:1: rulearray_data_component_reference returns [EObject current=null] : ( rulebaWS ( (lv_adcr1_1_0= '(' ) ) rulebaWS ( (lv_adcr2_3_0= ruledata_component_reference ) ) ( rulebaWS ( (lv_adcr3_5_0= ',' ) ) rulebaWS ( (lv_adcr4_7_0= ruledata_component_reference ) ) )* rulebaWS ( (lv_adcr5_9_0= ')' ) ) rulebaWS ) ;
    public final EObject rulearray_data_component_reference() throws RecognitionException {
        EObject current = null;

        Token lv_adcr1_1_0=null;
        Token lv_adcr3_5_0=null;
        Token lv_adcr5_9_0=null;
        EObject lv_adcr2_3_0 = null;

        EObject lv_adcr4_7_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1351:28: ( ( rulebaWS ( (lv_adcr1_1_0= '(' ) ) rulebaWS ( (lv_adcr2_3_0= ruledata_component_reference ) ) ( rulebaWS ( (lv_adcr3_5_0= ',' ) ) rulebaWS ( (lv_adcr4_7_0= ruledata_component_reference ) ) )* rulebaWS ( (lv_adcr5_9_0= ')' ) ) rulebaWS ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1352:1: ( rulebaWS ( (lv_adcr1_1_0= '(' ) ) rulebaWS ( (lv_adcr2_3_0= ruledata_component_reference ) ) ( rulebaWS ( (lv_adcr3_5_0= ',' ) ) rulebaWS ( (lv_adcr4_7_0= ruledata_component_reference ) ) )* rulebaWS ( (lv_adcr5_9_0= ')' ) ) rulebaWS )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1352:1: ( rulebaWS ( (lv_adcr1_1_0= '(' ) ) rulebaWS ( (lv_adcr2_3_0= ruledata_component_reference ) ) ( rulebaWS ( (lv_adcr3_5_0= ',' ) ) rulebaWS ( (lv_adcr4_7_0= ruledata_component_reference ) ) )* rulebaWS ( (lv_adcr5_9_0= ')' ) ) rulebaWS )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1353:5: rulebaWS ( (lv_adcr1_1_0= '(' ) ) rulebaWS ( (lv_adcr2_3_0= ruledata_component_reference ) ) ( rulebaWS ( (lv_adcr3_5_0= ',' ) ) rulebaWS ( (lv_adcr4_7_0= ruledata_component_reference ) ) )* rulebaWS ( (lv_adcr5_9_0= ')' ) ) rulebaWS
            {
             
                    newCompositeNode(grammarAccess.getArray_data_component_referenceAccess().getBaWSParserRuleCall_0()); 
                
            pushFollow(FOLLOW_rulebaWS_in_rulearray_data_component_reference2477);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1360:1: ( (lv_adcr1_1_0= '(' ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1361:1: (lv_adcr1_1_0= '(' )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1361:1: (lv_adcr1_1_0= '(' )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1362:3: lv_adcr1_1_0= '('
            {
            lv_adcr1_1_0=(Token)match(input,18,FOLLOW_18_in_rulearray_data_component_reference2494); 

                    newLeafNode(lv_adcr1_1_0, grammarAccess.getArray_data_component_referenceAccess().getAdcr1LeftParenthesisKeyword_1_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getArray_data_component_referenceRule());
            	        }
                   		setWithLastConsumed(current, "adcr1", lv_adcr1_1_0, "(");
            	    

            }


            }

             
                    newCompositeNode(grammarAccess.getArray_data_component_referenceAccess().getBaWSParserRuleCall_2()); 
                
            pushFollow(FOLLOW_rulebaWS_in_rulearray_data_component_reference2523);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1383:1: ( (lv_adcr2_3_0= ruledata_component_reference ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1384:1: (lv_adcr2_3_0= ruledata_component_reference )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1384:1: (lv_adcr2_3_0= ruledata_component_reference )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1385:3: lv_adcr2_3_0= ruledata_component_reference
            {
             
            	        newCompositeNode(grammarAccess.getArray_data_component_referenceAccess().getAdcr2Data_component_referenceParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruledata_component_reference_in_rulearray_data_component_reference2543);
            lv_adcr2_3_0=ruledata_component_reference();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getArray_data_component_referenceRule());
            	        }
                   		set(
                   			current, 
                   			"adcr2",
                    		lv_adcr2_3_0, 
                    		"data_component_reference");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1401:2: ( rulebaWS ( (lv_adcr3_5_0= ',' ) ) rulebaWS ( (lv_adcr4_7_0= ruledata_component_reference ) ) )*
            loop8:
            do {
                int alt8=2;
                alt8 = dfa8.predict(input);
                switch (alt8) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1402:5: rulebaWS ( (lv_adcr3_5_0= ',' ) ) rulebaWS ( (lv_adcr4_7_0= ruledata_component_reference ) )
            	    {
            	     
            	            newCompositeNode(grammarAccess.getArray_data_component_referenceAccess().getBaWSParserRuleCall_4_0()); 
            	        
            	    pushFollow(FOLLOW_rulebaWS_in_rulearray_data_component_reference2560);
            	    rulebaWS();

            	    state._fsp--;

            	     
            	            afterParserOrEnumRuleCall();
            	        
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1409:1: ( (lv_adcr3_5_0= ',' ) )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1410:1: (lv_adcr3_5_0= ',' )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1410:1: (lv_adcr3_5_0= ',' )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1411:3: lv_adcr3_5_0= ','
            	    {
            	    lv_adcr3_5_0=(Token)match(input,32,FOLLOW_32_in_rulearray_data_component_reference2577); 

            	            newLeafNode(lv_adcr3_5_0, grammarAccess.getArray_data_component_referenceAccess().getAdcr3CommaKeyword_4_1_0());
            	        

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getArray_data_component_referenceRule());
            	    	        }
            	           		addWithLastConsumed(current, "adcr3", lv_adcr3_5_0, ",");
            	    	    

            	    }


            	    }

            	     
            	            newCompositeNode(grammarAccess.getArray_data_component_referenceAccess().getBaWSParserRuleCall_4_2()); 
            	        
            	    pushFollow(FOLLOW_rulebaWS_in_rulearray_data_component_reference2606);
            	    rulebaWS();

            	    state._fsp--;

            	     
            	            afterParserOrEnumRuleCall();
            	        
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1432:1: ( (lv_adcr4_7_0= ruledata_component_reference ) )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1433:1: (lv_adcr4_7_0= ruledata_component_reference )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1433:1: (lv_adcr4_7_0= ruledata_component_reference )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1434:3: lv_adcr4_7_0= ruledata_component_reference
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getArray_data_component_referenceAccess().getAdcr4Data_component_referenceParserRuleCall_4_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruledata_component_reference_in_rulearray_data_component_reference2626);
            	    lv_adcr4_7_0=ruledata_component_reference();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getArray_data_component_referenceRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"adcr4",
            	            		lv_adcr4_7_0, 
            	            		"data_component_reference");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             
                    newCompositeNode(grammarAccess.getArray_data_component_referenceAccess().getBaWSParserRuleCall_5()); 
                
            pushFollow(FOLLOW_rulebaWS_in_rulearray_data_component_reference2644);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1458:1: ( (lv_adcr5_9_0= ')' ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1459:1: (lv_adcr5_9_0= ')' )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1459:1: (lv_adcr5_9_0= ')' )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1460:3: lv_adcr5_9_0= ')'
            {
            lv_adcr5_9_0=(Token)match(input,19,FOLLOW_19_in_rulearray_data_component_reference2661); 

                    newLeafNode(lv_adcr5_9_0, grammarAccess.getArray_data_component_referenceAccess().getAdcr5RightParenthesisKeyword_6_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getArray_data_component_referenceRule());
            	        }
                   		setWithLastConsumed(current, "adcr5", lv_adcr5_9_0, ")");
            	    

            }


            }

             
                    newCompositeNode(grammarAccess.getArray_data_component_referenceAccess().getBaWSParserRuleCall_7()); 
                
            pushFollow(FOLLOW_rulebaWS_in_rulearray_data_component_reference2690);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulearray_data_component_reference"


    // $ANTLR start "entryRulebasic_action"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1489:1: entryRulebasic_action returns [EObject current=null] : iv_rulebasic_action= rulebasic_action EOF ;
    public final EObject entryRulebasic_action() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebasic_action = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1490:2: (iv_rulebasic_action= rulebasic_action EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1491:2: iv_rulebasic_action= rulebasic_action EOF
            {
             newCompositeNode(grammarAccess.getBasic_actionRule()); 
            pushFollow(FOLLOW_rulebasic_action_in_entryRulebasic_action2725);
            iv_rulebasic_action=rulebasic_action();

            state._fsp--;

             current =iv_rulebasic_action; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebasic_action2735); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebasic_action"


    // $ANTLR start "rulebasic_action"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1498:1: rulebasic_action returns [EObject current=null] : ( ( (lv_bact1_0_0= ruleassignment_action ) ) | ( (lv_bact2_1_0= rulecommunication_action ) ) | ( (lv_bact3_2_0= ruletimed_action ) ) ) ;
    public final EObject rulebasic_action() throws RecognitionException {
        EObject current = null;

        EObject lv_bact1_0_0 = null;

        EObject lv_bact2_1_0 = null;

        EObject lv_bact3_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1501:28: ( ( ( (lv_bact1_0_0= ruleassignment_action ) ) | ( (lv_bact2_1_0= rulecommunication_action ) ) | ( (lv_bact3_2_0= ruletimed_action ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1502:1: ( ( (lv_bact1_0_0= ruleassignment_action ) ) | ( (lv_bact2_1_0= rulecommunication_action ) ) | ( (lv_bact3_2_0= ruletimed_action ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1502:1: ( ( (lv_bact1_0_0= ruleassignment_action ) ) | ( (lv_bact2_1_0= rulecommunication_action ) ) | ( (lv_bact3_2_0= ruletimed_action ) ) )
            int alt9=3;
            switch ( input.LA(1) ) {
            case RULE_STRING_LITERAL:
                {
                int LA9_1 = input.LA(2);

                if ( ((LA9_1>=35 && LA9_1<=39)) ) {
                    alt9=2;
                }
                else if ( (LA9_1==33) ) {
                    alt9=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 9, 1, input);

                    throw nvae;
                }
                }
                break;
            case 40:
            case 41:
                {
                alt9=2;
                }
                break;
            case 42:
                {
                alt9=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1502:2: ( (lv_bact1_0_0= ruleassignment_action ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1502:2: ( (lv_bact1_0_0= ruleassignment_action ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1503:1: (lv_bact1_0_0= ruleassignment_action )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1503:1: (lv_bact1_0_0= ruleassignment_action )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1504:3: lv_bact1_0_0= ruleassignment_action
                    {
                     
                    	        newCompositeNode(grammarAccess.getBasic_actionAccess().getBact1Assignment_actionParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleassignment_action_in_rulebasic_action2781);
                    lv_bact1_0_0=ruleassignment_action();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBasic_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"bact1",
                            		lv_bact1_0_0, 
                            		"assignment_action");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1521:6: ( (lv_bact2_1_0= rulecommunication_action ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1521:6: ( (lv_bact2_1_0= rulecommunication_action ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1522:1: (lv_bact2_1_0= rulecommunication_action )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1522:1: (lv_bact2_1_0= rulecommunication_action )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1523:3: lv_bact2_1_0= rulecommunication_action
                    {
                     
                    	        newCompositeNode(grammarAccess.getBasic_actionAccess().getBact2Communication_actionParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulecommunication_action_in_rulebasic_action2808);
                    lv_bact2_1_0=rulecommunication_action();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBasic_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"bact2",
                            		lv_bact2_1_0, 
                            		"communication_action");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1540:6: ( (lv_bact3_2_0= ruletimed_action ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1540:6: ( (lv_bact3_2_0= ruletimed_action ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1541:1: (lv_bact3_2_0= ruletimed_action )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1541:1: (lv_bact3_2_0= ruletimed_action )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1542:3: lv_bact3_2_0= ruletimed_action
                    {
                     
                    	        newCompositeNode(grammarAccess.getBasic_actionAccess().getBact3Timed_actionParserRuleCall_2_0()); 
                    	    
                    pushFollow(FOLLOW_ruletimed_action_in_rulebasic_action2835);
                    lv_bact3_2_0=ruletimed_action();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBasic_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"bact3",
                            		lv_bact3_2_0, 
                            		"timed_action");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebasic_action"


    // $ANTLR start "entryRuleassignment_action"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1566:1: entryRuleassignment_action returns [EObject current=null] : iv_ruleassignment_action= ruleassignment_action EOF ;
    public final EObject entryRuleassignment_action() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleassignment_action = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1567:2: (iv_ruleassignment_action= ruleassignment_action EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1568:2: iv_ruleassignment_action= ruleassignment_action EOF
            {
             newCompositeNode(grammarAccess.getAssignment_actionRule()); 
            pushFollow(FOLLOW_ruleassignment_action_in_entryRuleassignment_action2871);
            iv_ruleassignment_action=ruleassignment_action();

            state._fsp--;

             current =iv_ruleassignment_action; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleassignment_action2881); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleassignment_action"


    // $ANTLR start "ruleassignment_action"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1575:1: ruleassignment_action returns [EObject current=null] : ( ( (lv_aa1_0_0= RULE_STRING_LITERAL ) ) ( (lv_aa2_1_0= ':=' ) ) ( ( (lv_aa3_2_0= rulevalue_expression ) ) | ( (lv_aa4_3_0= 'any' ) ) ) ) ;
    public final EObject ruleassignment_action() throws RecognitionException {
        EObject current = null;

        Token lv_aa1_0_0=null;
        Token lv_aa2_1_0=null;
        Token lv_aa4_3_0=null;
        EObject lv_aa3_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1578:28: ( ( ( (lv_aa1_0_0= RULE_STRING_LITERAL ) ) ( (lv_aa2_1_0= ':=' ) ) ( ( (lv_aa3_2_0= rulevalue_expression ) ) | ( (lv_aa4_3_0= 'any' ) ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1579:1: ( ( (lv_aa1_0_0= RULE_STRING_LITERAL ) ) ( (lv_aa2_1_0= ':=' ) ) ( ( (lv_aa3_2_0= rulevalue_expression ) ) | ( (lv_aa4_3_0= 'any' ) ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1579:1: ( ( (lv_aa1_0_0= RULE_STRING_LITERAL ) ) ( (lv_aa2_1_0= ':=' ) ) ( ( (lv_aa3_2_0= rulevalue_expression ) ) | ( (lv_aa4_3_0= 'any' ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1579:2: ( (lv_aa1_0_0= RULE_STRING_LITERAL ) ) ( (lv_aa2_1_0= ':=' ) ) ( ( (lv_aa3_2_0= rulevalue_expression ) ) | ( (lv_aa4_3_0= 'any' ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1579:2: ( (lv_aa1_0_0= RULE_STRING_LITERAL ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1580:1: (lv_aa1_0_0= RULE_STRING_LITERAL )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1580:1: (lv_aa1_0_0= RULE_STRING_LITERAL )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1581:3: lv_aa1_0_0= RULE_STRING_LITERAL
            {
            lv_aa1_0_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_ruleassignment_action2923); 

            			newLeafNode(lv_aa1_0_0, grammarAccess.getAssignment_actionAccess().getAa1String_literalTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getAssignment_actionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"aa1",
                    		lv_aa1_0_0, 
                    		"string_literal");
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1597:2: ( (lv_aa2_1_0= ':=' ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1598:1: (lv_aa2_1_0= ':=' )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1598:1: (lv_aa2_1_0= ':=' )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1599:3: lv_aa2_1_0= ':='
            {
            lv_aa2_1_0=(Token)match(input,33,FOLLOW_33_in_ruleassignment_action2946); 

                    newLeafNode(lv_aa2_1_0, grammarAccess.getAssignment_actionAccess().getAa2ColonEqualsSignKeyword_1_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getAssignment_actionRule());
            	        }
                   		setWithLastConsumed(current, "aa2", lv_aa2_1_0, ":=");
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1612:2: ( ( (lv_aa3_2_0= rulevalue_expression ) ) | ( (lv_aa4_3_0= 'any' ) ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==EOF||(LA10_0>=RULE_STRING_LITERAL && LA10_0<=RULE_DIGIT)||LA10_0==RULE_INT||(LA10_0>=14 && LA10_0<=15)||(LA10_0>=17 && LA10_0<=23)||(LA10_0>=25 && LA10_0<=32)||LA10_0==37||(LA10_0>=40 && LA10_0<=43)||LA10_0==49||(LA10_0>=52 && LA10_0<=60)||(LA10_0>=62 && LA10_0<=81)) ) {
                alt10=1;
            }
            else if ( (LA10_0==34) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1612:3: ( (lv_aa3_2_0= rulevalue_expression ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1612:3: ( (lv_aa3_2_0= rulevalue_expression ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1613:1: (lv_aa3_2_0= rulevalue_expression )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1613:1: (lv_aa3_2_0= rulevalue_expression )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1614:3: lv_aa3_2_0= rulevalue_expression
                    {
                     
                    	        newCompositeNode(grammarAccess.getAssignment_actionAccess().getAa3Value_expressionParserRuleCall_2_0_0()); 
                    	    
                    pushFollow(FOLLOW_rulevalue_expression_in_ruleassignment_action2981);
                    lv_aa3_2_0=rulevalue_expression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getAssignment_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"aa3",
                            		lv_aa3_2_0, 
                            		"value_expression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1631:6: ( (lv_aa4_3_0= 'any' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1631:6: ( (lv_aa4_3_0= 'any' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1632:1: (lv_aa4_3_0= 'any' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1632:1: (lv_aa4_3_0= 'any' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1633:3: lv_aa4_3_0= 'any'
                    {
                    lv_aa4_3_0=(Token)match(input,34,FOLLOW_34_in_ruleassignment_action3005); 

                            newLeafNode(lv_aa4_3_0, grammarAccess.getAssignment_actionAccess().getAa4AnyKeyword_2_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getAssignment_actionRule());
                    	        }
                           		setWithLastConsumed(current, "aa4", lv_aa4_3_0, "any");
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleassignment_action"


    // $ANTLR start "entryRulecommunication_action"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1654:1: entryRulecommunication_action returns [EObject current=null] : iv_rulecommunication_action= rulecommunication_action EOF ;
    public final EObject entryRulecommunication_action() throws RecognitionException {
        EObject current = null;

        EObject iv_rulecommunication_action = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1655:2: (iv_rulecommunication_action= rulecommunication_action EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1656:2: iv_rulecommunication_action= rulecommunication_action EOF
            {
             newCompositeNode(grammarAccess.getCommunication_actionRule()); 
            pushFollow(FOLLOW_rulecommunication_action_in_entryRulecommunication_action3055);
            iv_rulecommunication_action=rulecommunication_action();

            state._fsp--;

             current =iv_rulecommunication_action; 
            match(input,EOF,FOLLOW_EOF_in_entryRulecommunication_action3065); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulecommunication_action"


    // $ANTLR start "rulecommunication_action"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1663:1: rulecommunication_action returns [EObject current=null] : ( ( ( (lv_ca1_0_0= RULE_STRING_LITERAL ) ) ( (lv_ca2_1_0= '!' ) ) ( ( (lv_ca3_2_0= '(' ) ) ( (lv_ca4_3_0= rulesubprogram_parameter_list ) ) ( (lv_ca5_4_0= ')' ) ) )? ) | ( ( (lv_ca21_5_0= RULE_STRING_LITERAL ) ) ( (lv_ca22_6_0= '>>' ) ) ) | ( ( (lv_ca23_7_0= RULE_STRING_LITERAL ) ) ( (lv_ca24_8_0= '?' ) ) ( ( (lv_ca25_9_0= '(' ) ) ( (lv_ca26_10_0= RULE_STRING_LITERAL ) ) ( (lv_ca27_11_0= ')' ) ) )? ) | ( ( (lv_ca28_12_0= RULE_STRING_LITERAL ) ) ( (lv_ca29_13_0= '!<' ) ) ) | ( ( (lv_ca30_14_0= RULE_STRING_LITERAL ) ) ( (lv_ca31_15_0= '!>' ) ) ) | ( (lv_ca32_16_0= '*!<' ) ) | ( (lv_ca33_17_0= '*!>' ) ) ) ;
    public final EObject rulecommunication_action() throws RecognitionException {
        EObject current = null;

        Token lv_ca1_0_0=null;
        Token lv_ca2_1_0=null;
        Token lv_ca3_2_0=null;
        Token lv_ca5_4_0=null;
        Token lv_ca21_5_0=null;
        Token lv_ca22_6_0=null;
        Token lv_ca23_7_0=null;
        Token lv_ca24_8_0=null;
        Token lv_ca25_9_0=null;
        Token lv_ca26_10_0=null;
        Token lv_ca27_11_0=null;
        Token lv_ca28_12_0=null;
        Token lv_ca29_13_0=null;
        Token lv_ca30_14_0=null;
        Token lv_ca31_15_0=null;
        Token lv_ca32_16_0=null;
        Token lv_ca33_17_0=null;
        EObject lv_ca4_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1666:28: ( ( ( ( (lv_ca1_0_0= RULE_STRING_LITERAL ) ) ( (lv_ca2_1_0= '!' ) ) ( ( (lv_ca3_2_0= '(' ) ) ( (lv_ca4_3_0= rulesubprogram_parameter_list ) ) ( (lv_ca5_4_0= ')' ) ) )? ) | ( ( (lv_ca21_5_0= RULE_STRING_LITERAL ) ) ( (lv_ca22_6_0= '>>' ) ) ) | ( ( (lv_ca23_7_0= RULE_STRING_LITERAL ) ) ( (lv_ca24_8_0= '?' ) ) ( ( (lv_ca25_9_0= '(' ) ) ( (lv_ca26_10_0= RULE_STRING_LITERAL ) ) ( (lv_ca27_11_0= ')' ) ) )? ) | ( ( (lv_ca28_12_0= RULE_STRING_LITERAL ) ) ( (lv_ca29_13_0= '!<' ) ) ) | ( ( (lv_ca30_14_0= RULE_STRING_LITERAL ) ) ( (lv_ca31_15_0= '!>' ) ) ) | ( (lv_ca32_16_0= '*!<' ) ) | ( (lv_ca33_17_0= '*!>' ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1667:1: ( ( ( (lv_ca1_0_0= RULE_STRING_LITERAL ) ) ( (lv_ca2_1_0= '!' ) ) ( ( (lv_ca3_2_0= '(' ) ) ( (lv_ca4_3_0= rulesubprogram_parameter_list ) ) ( (lv_ca5_4_0= ')' ) ) )? ) | ( ( (lv_ca21_5_0= RULE_STRING_LITERAL ) ) ( (lv_ca22_6_0= '>>' ) ) ) | ( ( (lv_ca23_7_0= RULE_STRING_LITERAL ) ) ( (lv_ca24_8_0= '?' ) ) ( ( (lv_ca25_9_0= '(' ) ) ( (lv_ca26_10_0= RULE_STRING_LITERAL ) ) ( (lv_ca27_11_0= ')' ) ) )? ) | ( ( (lv_ca28_12_0= RULE_STRING_LITERAL ) ) ( (lv_ca29_13_0= '!<' ) ) ) | ( ( (lv_ca30_14_0= RULE_STRING_LITERAL ) ) ( (lv_ca31_15_0= '!>' ) ) ) | ( (lv_ca32_16_0= '*!<' ) ) | ( (lv_ca33_17_0= '*!>' ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1667:1: ( ( ( (lv_ca1_0_0= RULE_STRING_LITERAL ) ) ( (lv_ca2_1_0= '!' ) ) ( ( (lv_ca3_2_0= '(' ) ) ( (lv_ca4_3_0= rulesubprogram_parameter_list ) ) ( (lv_ca5_4_0= ')' ) ) )? ) | ( ( (lv_ca21_5_0= RULE_STRING_LITERAL ) ) ( (lv_ca22_6_0= '>>' ) ) ) | ( ( (lv_ca23_7_0= RULE_STRING_LITERAL ) ) ( (lv_ca24_8_0= '?' ) ) ( ( (lv_ca25_9_0= '(' ) ) ( (lv_ca26_10_0= RULE_STRING_LITERAL ) ) ( (lv_ca27_11_0= ')' ) ) )? ) | ( ( (lv_ca28_12_0= RULE_STRING_LITERAL ) ) ( (lv_ca29_13_0= '!<' ) ) ) | ( ( (lv_ca30_14_0= RULE_STRING_LITERAL ) ) ( (lv_ca31_15_0= '!>' ) ) ) | ( (lv_ca32_16_0= '*!<' ) ) | ( (lv_ca33_17_0= '*!>' ) ) )
            int alt13=7;
            switch ( input.LA(1) ) {
            case RULE_STRING_LITERAL:
                {
                switch ( input.LA(2) ) {
                case 38:
                    {
                    alt13=4;
                    }
                    break;
                case 35:
                    {
                    alt13=1;
                    }
                    break;
                case 36:
                    {
                    alt13=2;
                    }
                    break;
                case 37:
                    {
                    alt13=3;
                    }
                    break;
                case 39:
                    {
                    alt13=5;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 13, 1, input);

                    throw nvae;
                }

                }
                break;
            case 40:
                {
                alt13=6;
                }
                break;
            case 41:
                {
                alt13=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1667:2: ( ( (lv_ca1_0_0= RULE_STRING_LITERAL ) ) ( (lv_ca2_1_0= '!' ) ) ( ( (lv_ca3_2_0= '(' ) ) ( (lv_ca4_3_0= rulesubprogram_parameter_list ) ) ( (lv_ca5_4_0= ')' ) ) )? )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1667:2: ( ( (lv_ca1_0_0= RULE_STRING_LITERAL ) ) ( (lv_ca2_1_0= '!' ) ) ( ( (lv_ca3_2_0= '(' ) ) ( (lv_ca4_3_0= rulesubprogram_parameter_list ) ) ( (lv_ca5_4_0= ')' ) ) )? )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1667:3: ( (lv_ca1_0_0= RULE_STRING_LITERAL ) ) ( (lv_ca2_1_0= '!' ) ) ( ( (lv_ca3_2_0= '(' ) ) ( (lv_ca4_3_0= rulesubprogram_parameter_list ) ) ( (lv_ca5_4_0= ')' ) ) )?
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1667:3: ( (lv_ca1_0_0= RULE_STRING_LITERAL ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1668:1: (lv_ca1_0_0= RULE_STRING_LITERAL )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1668:1: (lv_ca1_0_0= RULE_STRING_LITERAL )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1669:3: lv_ca1_0_0= RULE_STRING_LITERAL
                    {
                    lv_ca1_0_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulecommunication_action3108); 

                    			newLeafNode(lv_ca1_0_0, grammarAccess.getCommunication_actionAccess().getCa1String_literalTerminalRuleCall_0_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"ca1",
                            		lv_ca1_0_0, 
                            		"string_literal");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1685:2: ( (lv_ca2_1_0= '!' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1686:1: (lv_ca2_1_0= '!' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1686:1: (lv_ca2_1_0= '!' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1687:3: lv_ca2_1_0= '!'
                    {
                    lv_ca2_1_0=(Token)match(input,35,FOLLOW_35_in_rulecommunication_action3131); 

                            newLeafNode(lv_ca2_1_0, grammarAccess.getCommunication_actionAccess().getCa2ExclamationMarkKeyword_0_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ca2", lv_ca2_1_0, "!");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1700:2: ( ( (lv_ca3_2_0= '(' ) ) ( (lv_ca4_3_0= rulesubprogram_parameter_list ) ) ( (lv_ca5_4_0= ')' ) ) )?
                    int alt11=2;
                    int LA11_0 = input.LA(1);

                    if ( (LA11_0==18) ) {
                        alt11=1;
                    }
                    switch (alt11) {
                        case 1 :
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1700:3: ( (lv_ca3_2_0= '(' ) ) ( (lv_ca4_3_0= rulesubprogram_parameter_list ) ) ( (lv_ca5_4_0= ')' ) )
                            {
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1700:3: ( (lv_ca3_2_0= '(' ) )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1701:1: (lv_ca3_2_0= '(' )
                            {
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1701:1: (lv_ca3_2_0= '(' )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1702:3: lv_ca3_2_0= '('
                            {
                            lv_ca3_2_0=(Token)match(input,18,FOLLOW_18_in_rulecommunication_action3163); 

                                    newLeafNode(lv_ca3_2_0, grammarAccess.getCommunication_actionAccess().getCa3LeftParenthesisKeyword_0_2_0_0());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                            	        }
                                   		setWithLastConsumed(current, "ca3", lv_ca3_2_0, "(");
                            	    

                            }


                            }

                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1715:2: ( (lv_ca4_3_0= rulesubprogram_parameter_list ) )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1716:1: (lv_ca4_3_0= rulesubprogram_parameter_list )
                            {
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1716:1: (lv_ca4_3_0= rulesubprogram_parameter_list )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1717:3: lv_ca4_3_0= rulesubprogram_parameter_list
                            {
                             
                            	        newCompositeNode(grammarAccess.getCommunication_actionAccess().getCa4Subprogram_parameter_listParserRuleCall_0_2_1_0()); 
                            	    
                            pushFollow(FOLLOW_rulesubprogram_parameter_list_in_rulecommunication_action3197);
                            lv_ca4_3_0=rulesubprogram_parameter_list();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getCommunication_actionRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"ca4",
                                    		lv_ca4_3_0, 
                                    		"subprogram_parameter_list");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }

                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1733:2: ( (lv_ca5_4_0= ')' ) )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1734:1: (lv_ca5_4_0= ')' )
                            {
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1734:1: (lv_ca5_4_0= ')' )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1735:3: lv_ca5_4_0= ')'
                            {
                            lv_ca5_4_0=(Token)match(input,19,FOLLOW_19_in_rulecommunication_action3215); 

                                    newLeafNode(lv_ca5_4_0, grammarAccess.getCommunication_actionAccess().getCa5RightParenthesisKeyword_0_2_2_0());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                            	        }
                                   		setWithLastConsumed(current, "ca5", lv_ca5_4_0, ")");
                            	    

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1749:6: ( ( (lv_ca21_5_0= RULE_STRING_LITERAL ) ) ( (lv_ca22_6_0= '>>' ) ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1749:6: ( ( (lv_ca21_5_0= RULE_STRING_LITERAL ) ) ( (lv_ca22_6_0= '>>' ) ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1749:7: ( (lv_ca21_5_0= RULE_STRING_LITERAL ) ) ( (lv_ca22_6_0= '>>' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1749:7: ( (lv_ca21_5_0= RULE_STRING_LITERAL ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1750:1: (lv_ca21_5_0= RULE_STRING_LITERAL )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1750:1: (lv_ca21_5_0= RULE_STRING_LITERAL )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1751:3: lv_ca21_5_0= RULE_STRING_LITERAL
                    {
                    lv_ca21_5_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulecommunication_action3255); 

                    			newLeafNode(lv_ca21_5_0, grammarAccess.getCommunication_actionAccess().getCa21String_literalTerminalRuleCall_1_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"ca21",
                            		lv_ca21_5_0, 
                            		"string_literal");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1767:2: ( (lv_ca22_6_0= '>>' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1768:1: (lv_ca22_6_0= '>>' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1768:1: (lv_ca22_6_0= '>>' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1769:3: lv_ca22_6_0= '>>'
                    {
                    lv_ca22_6_0=(Token)match(input,36,FOLLOW_36_in_rulecommunication_action3278); 

                            newLeafNode(lv_ca22_6_0, grammarAccess.getCommunication_actionAccess().getCa22GreaterThanSignGreaterThanSignKeyword_1_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ca22", lv_ca22_6_0, ">>");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1783:6: ( ( (lv_ca23_7_0= RULE_STRING_LITERAL ) ) ( (lv_ca24_8_0= '?' ) ) ( ( (lv_ca25_9_0= '(' ) ) ( (lv_ca26_10_0= RULE_STRING_LITERAL ) ) ( (lv_ca27_11_0= ')' ) ) )? )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1783:6: ( ( (lv_ca23_7_0= RULE_STRING_LITERAL ) ) ( (lv_ca24_8_0= '?' ) ) ( ( (lv_ca25_9_0= '(' ) ) ( (lv_ca26_10_0= RULE_STRING_LITERAL ) ) ( (lv_ca27_11_0= ')' ) ) )? )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1783:7: ( (lv_ca23_7_0= RULE_STRING_LITERAL ) ) ( (lv_ca24_8_0= '?' ) ) ( ( (lv_ca25_9_0= '(' ) ) ( (lv_ca26_10_0= RULE_STRING_LITERAL ) ) ( (lv_ca27_11_0= ')' ) ) )?
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1783:7: ( (lv_ca23_7_0= RULE_STRING_LITERAL ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1784:1: (lv_ca23_7_0= RULE_STRING_LITERAL )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1784:1: (lv_ca23_7_0= RULE_STRING_LITERAL )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1785:3: lv_ca23_7_0= RULE_STRING_LITERAL
                    {
                    lv_ca23_7_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulecommunication_action3316); 

                    			newLeafNode(lv_ca23_7_0, grammarAccess.getCommunication_actionAccess().getCa23String_literalTerminalRuleCall_2_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"ca23",
                            		lv_ca23_7_0, 
                            		"string_literal");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1801:2: ( (lv_ca24_8_0= '?' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1802:1: (lv_ca24_8_0= '?' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1802:1: (lv_ca24_8_0= '?' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1803:3: lv_ca24_8_0= '?'
                    {
                    lv_ca24_8_0=(Token)match(input,37,FOLLOW_37_in_rulecommunication_action3339); 

                            newLeafNode(lv_ca24_8_0, grammarAccess.getCommunication_actionAccess().getCa24QuestionMarkKeyword_2_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ca24", lv_ca24_8_0, "?");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1816:2: ( ( (lv_ca25_9_0= '(' ) ) ( (lv_ca26_10_0= RULE_STRING_LITERAL ) ) ( (lv_ca27_11_0= ')' ) ) )?
                    int alt12=2;
                    int LA12_0 = input.LA(1);

                    if ( (LA12_0==18) ) {
                        alt12=1;
                    }
                    switch (alt12) {
                        case 1 :
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1816:3: ( (lv_ca25_9_0= '(' ) ) ( (lv_ca26_10_0= RULE_STRING_LITERAL ) ) ( (lv_ca27_11_0= ')' ) )
                            {
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1816:3: ( (lv_ca25_9_0= '(' ) )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1817:1: (lv_ca25_9_0= '(' )
                            {
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1817:1: (lv_ca25_9_0= '(' )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1818:3: lv_ca25_9_0= '('
                            {
                            lv_ca25_9_0=(Token)match(input,18,FOLLOW_18_in_rulecommunication_action3371); 

                                    newLeafNode(lv_ca25_9_0, grammarAccess.getCommunication_actionAccess().getCa25LeftParenthesisKeyword_2_2_0_0());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                            	        }
                                   		setWithLastConsumed(current, "ca25", lv_ca25_9_0, "(");
                            	    

                            }


                            }

                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1831:2: ( (lv_ca26_10_0= RULE_STRING_LITERAL ) )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1832:1: (lv_ca26_10_0= RULE_STRING_LITERAL )
                            {
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1832:1: (lv_ca26_10_0= RULE_STRING_LITERAL )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1833:3: lv_ca26_10_0= RULE_STRING_LITERAL
                            {
                            lv_ca26_10_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulecommunication_action3401); 

                            			newLeafNode(lv_ca26_10_0, grammarAccess.getCommunication_actionAccess().getCa26String_literalTerminalRuleCall_2_2_1_0()); 
                            		

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                            	        }
                                   		setWithLastConsumed(
                                   			current, 
                                   			"ca26",
                                    		lv_ca26_10_0, 
                                    		"string_literal");
                            	    

                            }


                            }

                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1849:2: ( (lv_ca27_11_0= ')' ) )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1850:1: (lv_ca27_11_0= ')' )
                            {
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1850:1: (lv_ca27_11_0= ')' )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1851:3: lv_ca27_11_0= ')'
                            {
                            lv_ca27_11_0=(Token)match(input,19,FOLLOW_19_in_rulecommunication_action3424); 

                                    newLeafNode(lv_ca27_11_0, grammarAccess.getCommunication_actionAccess().getCa27RightParenthesisKeyword_2_2_2_0());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                            	        }
                                   		setWithLastConsumed(current, "ca27", lv_ca27_11_0, ")");
                            	    

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 4 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1865:6: ( ( (lv_ca28_12_0= RULE_STRING_LITERAL ) ) ( (lv_ca29_13_0= '!<' ) ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1865:6: ( ( (lv_ca28_12_0= RULE_STRING_LITERAL ) ) ( (lv_ca29_13_0= '!<' ) ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1865:7: ( (lv_ca28_12_0= RULE_STRING_LITERAL ) ) ( (lv_ca29_13_0= '!<' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1865:7: ( (lv_ca28_12_0= RULE_STRING_LITERAL ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1866:1: (lv_ca28_12_0= RULE_STRING_LITERAL )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1866:1: (lv_ca28_12_0= RULE_STRING_LITERAL )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1867:3: lv_ca28_12_0= RULE_STRING_LITERAL
                    {
                    lv_ca28_12_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulecommunication_action3464); 

                    			newLeafNode(lv_ca28_12_0, grammarAccess.getCommunication_actionAccess().getCa28String_literalTerminalRuleCall_3_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"ca28",
                            		lv_ca28_12_0, 
                            		"string_literal");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1883:2: ( (lv_ca29_13_0= '!<' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1884:1: (lv_ca29_13_0= '!<' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1884:1: (lv_ca29_13_0= '!<' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1885:3: lv_ca29_13_0= '!<'
                    {
                    lv_ca29_13_0=(Token)match(input,38,FOLLOW_38_in_rulecommunication_action3487); 

                            newLeafNode(lv_ca29_13_0, grammarAccess.getCommunication_actionAccess().getCa29ExclamationMarkLessThanSignKeyword_3_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ca29", lv_ca29_13_0, "!<");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1899:6: ( ( (lv_ca30_14_0= RULE_STRING_LITERAL ) ) ( (lv_ca31_15_0= '!>' ) ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1899:6: ( ( (lv_ca30_14_0= RULE_STRING_LITERAL ) ) ( (lv_ca31_15_0= '!>' ) ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1899:7: ( (lv_ca30_14_0= RULE_STRING_LITERAL ) ) ( (lv_ca31_15_0= '!>' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1899:7: ( (lv_ca30_14_0= RULE_STRING_LITERAL ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1900:1: (lv_ca30_14_0= RULE_STRING_LITERAL )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1900:1: (lv_ca30_14_0= RULE_STRING_LITERAL )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1901:3: lv_ca30_14_0= RULE_STRING_LITERAL
                    {
                    lv_ca30_14_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulecommunication_action3525); 

                    			newLeafNode(lv_ca30_14_0, grammarAccess.getCommunication_actionAccess().getCa30String_literalTerminalRuleCall_4_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"ca30",
                            		lv_ca30_14_0, 
                            		"string_literal");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1917:2: ( (lv_ca31_15_0= '!>' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1918:1: (lv_ca31_15_0= '!>' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1918:1: (lv_ca31_15_0= '!>' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1919:3: lv_ca31_15_0= '!>'
                    {
                    lv_ca31_15_0=(Token)match(input,39,FOLLOW_39_in_rulecommunication_action3548); 

                            newLeafNode(lv_ca31_15_0, grammarAccess.getCommunication_actionAccess().getCa31ExclamationMarkGreaterThanSignKeyword_4_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ca31", lv_ca31_15_0, "!>");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 6 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1933:6: ( (lv_ca32_16_0= '*!<' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1933:6: ( (lv_ca32_16_0= '*!<' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1934:1: (lv_ca32_16_0= '*!<' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1934:1: (lv_ca32_16_0= '*!<' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1935:3: lv_ca32_16_0= '*!<'
                    {
                    lv_ca32_16_0=(Token)match(input,40,FOLLOW_40_in_rulecommunication_action3586); 

                            newLeafNode(lv_ca32_16_0, grammarAccess.getCommunication_actionAccess().getCa32AsteriskExclamationMarkLessThanSignKeyword_5_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ca32", lv_ca32_16_0, "*!<");
                    	    

                    }


                    }


                    }
                    break;
                case 7 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1949:6: ( (lv_ca33_17_0= '*!>' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1949:6: ( (lv_ca33_17_0= '*!>' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1950:1: (lv_ca33_17_0= '*!>' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1950:1: (lv_ca33_17_0= '*!>' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1951:3: lv_ca33_17_0= '*!>'
                    {
                    lv_ca33_17_0=(Token)match(input,41,FOLLOW_41_in_rulecommunication_action3623); 

                            newLeafNode(lv_ca33_17_0, grammarAccess.getCommunication_actionAccess().getCa33AsteriskExclamationMarkGreaterThanSignKeyword_6_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCommunication_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ca33", lv_ca33_17_0, "*!>");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulecommunication_action"


    // $ANTLR start "entryRuletimed_action"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1972:1: entryRuletimed_action returns [EObject current=null] : iv_ruletimed_action= ruletimed_action EOF ;
    public final EObject entryRuletimed_action() throws RecognitionException {
        EObject current = null;

        EObject iv_ruletimed_action = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1973:2: (iv_ruletimed_action= ruletimed_action EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1974:2: iv_ruletimed_action= ruletimed_action EOF
            {
             newCompositeNode(grammarAccess.getTimed_actionRule()); 
            pushFollow(FOLLOW_ruletimed_action_in_entryRuletimed_action3672);
            iv_ruletimed_action=ruletimed_action();

            state._fsp--;

             current =iv_ruletimed_action; 
            match(input,EOF,FOLLOW_EOF_in_entryRuletimed_action3682); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuletimed_action"


    // $ANTLR start "ruletimed_action"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1981:1: ruletimed_action returns [EObject current=null] : ( ( (lv_ta1_0_0= 'computation' ) ) rulebaWS ( (lv_ta1bis_2_0= '(' ) ) ( (lv_ta2_3_0= rulebehavior_time ) ) ( ( (lv_ta3_4_0= '..' ) ) ( (lv_ta4_5_0= rulebehavior_time ) ) )? ( (lv_ta5_6_0= ')' ) ) ) ;
    public final EObject ruletimed_action() throws RecognitionException {
        EObject current = null;

        Token lv_ta1_0_0=null;
        Token lv_ta1bis_2_0=null;
        Token lv_ta3_4_0=null;
        Token lv_ta5_6_0=null;
        EObject lv_ta2_3_0 = null;

        EObject lv_ta4_5_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1984:28: ( ( ( (lv_ta1_0_0= 'computation' ) ) rulebaWS ( (lv_ta1bis_2_0= '(' ) ) ( (lv_ta2_3_0= rulebehavior_time ) ) ( ( (lv_ta3_4_0= '..' ) ) ( (lv_ta4_5_0= rulebehavior_time ) ) )? ( (lv_ta5_6_0= ')' ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1985:1: ( ( (lv_ta1_0_0= 'computation' ) ) rulebaWS ( (lv_ta1bis_2_0= '(' ) ) ( (lv_ta2_3_0= rulebehavior_time ) ) ( ( (lv_ta3_4_0= '..' ) ) ( (lv_ta4_5_0= rulebehavior_time ) ) )? ( (lv_ta5_6_0= ')' ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1985:1: ( ( (lv_ta1_0_0= 'computation' ) ) rulebaWS ( (lv_ta1bis_2_0= '(' ) ) ( (lv_ta2_3_0= rulebehavior_time ) ) ( ( (lv_ta3_4_0= '..' ) ) ( (lv_ta4_5_0= rulebehavior_time ) ) )? ( (lv_ta5_6_0= ')' ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1985:2: ( (lv_ta1_0_0= 'computation' ) ) rulebaWS ( (lv_ta1bis_2_0= '(' ) ) ( (lv_ta2_3_0= rulebehavior_time ) ) ( ( (lv_ta3_4_0= '..' ) ) ( (lv_ta4_5_0= rulebehavior_time ) ) )? ( (lv_ta5_6_0= ')' ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1985:2: ( (lv_ta1_0_0= 'computation' ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1986:1: (lv_ta1_0_0= 'computation' )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1986:1: (lv_ta1_0_0= 'computation' )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:1987:3: lv_ta1_0_0= 'computation'
            {
            lv_ta1_0_0=(Token)match(input,42,FOLLOW_42_in_ruletimed_action3725); 

                    newLeafNode(lv_ta1_0_0, grammarAccess.getTimed_actionAccess().getTa1ComputationKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTimed_actionRule());
            	        }
                   		setWithLastConsumed(current, "ta1", lv_ta1_0_0, "computation");
            	    

            }


            }

             
                    newCompositeNode(grammarAccess.getTimed_actionAccess().getBaWSParserRuleCall_1()); 
                
            pushFollow(FOLLOW_rulebaWS_in_ruletimed_action3754);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2008:1: ( (lv_ta1bis_2_0= '(' ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2009:1: (lv_ta1bis_2_0= '(' )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2009:1: (lv_ta1bis_2_0= '(' )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2010:3: lv_ta1bis_2_0= '('
            {
            lv_ta1bis_2_0=(Token)match(input,18,FOLLOW_18_in_ruletimed_action3771); 

                    newLeafNode(lv_ta1bis_2_0, grammarAccess.getTimed_actionAccess().getTa1bisLeftParenthesisKeyword_2_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTimed_actionRule());
            	        }
                   		setWithLastConsumed(current, "ta1bis", lv_ta1bis_2_0, "(");
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2023:2: ( (lv_ta2_3_0= rulebehavior_time ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2024:1: (lv_ta2_3_0= rulebehavior_time )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2024:1: (lv_ta2_3_0= rulebehavior_time )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2025:3: lv_ta2_3_0= rulebehavior_time
            {
             
            	        newCompositeNode(grammarAccess.getTimed_actionAccess().getTa2Behavior_timeParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_rulebehavior_time_in_ruletimed_action3805);
            lv_ta2_3_0=rulebehavior_time();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTimed_actionRule());
            	        }
                   		set(
                   			current, 
                   			"ta2",
                    		lv_ta2_3_0, 
                    		"behavior_time");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2041:2: ( ( (lv_ta3_4_0= '..' ) ) ( (lv_ta4_5_0= rulebehavior_time ) ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==43) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2041:3: ( (lv_ta3_4_0= '..' ) ) ( (lv_ta4_5_0= rulebehavior_time ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2041:3: ( (lv_ta3_4_0= '..' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2042:1: (lv_ta3_4_0= '..' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2042:1: (lv_ta3_4_0= '..' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2043:3: lv_ta3_4_0= '..'
                    {
                    lv_ta3_4_0=(Token)match(input,43,FOLLOW_43_in_ruletimed_action3824); 

                            newLeafNode(lv_ta3_4_0, grammarAccess.getTimed_actionAccess().getTa3FullStopFullStopKeyword_4_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getTimed_actionRule());
                    	        }
                           		setWithLastConsumed(current, "ta3", lv_ta3_4_0, "..");
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2056:2: ( (lv_ta4_5_0= rulebehavior_time ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2057:1: (lv_ta4_5_0= rulebehavior_time )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2057:1: (lv_ta4_5_0= rulebehavior_time )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2058:3: lv_ta4_5_0= rulebehavior_time
                    {
                     
                    	        newCompositeNode(grammarAccess.getTimed_actionAccess().getTa4Behavior_timeParserRuleCall_4_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulebehavior_time_in_ruletimed_action3858);
                    lv_ta4_5_0=rulebehavior_time();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTimed_actionRule());
                    	        }
                           		set(
                           			current, 
                           			"ta4",
                            		lv_ta4_5_0, 
                            		"behavior_time");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2074:4: ( (lv_ta5_6_0= ')' ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2075:1: (lv_ta5_6_0= ')' )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2075:1: (lv_ta5_6_0= ')' )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2076:3: lv_ta5_6_0= ')'
            {
            lv_ta5_6_0=(Token)match(input,19,FOLLOW_19_in_ruletimed_action3878); 

                    newLeafNode(lv_ta5_6_0, grammarAccess.getTimed_actionAccess().getTa5RightParenthesisKeyword_5_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTimed_actionRule());
            	        }
                   		setWithLastConsumed(current, "ta5", lv_ta5_6_0, ")");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruletimed_action"


    // $ANTLR start "entryRulesubprogram_parameter_list"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2097:1: entryRulesubprogram_parameter_list returns [EObject current=null] : iv_rulesubprogram_parameter_list= rulesubprogram_parameter_list EOF ;
    public final EObject entryRulesubprogram_parameter_list() throws RecognitionException {
        EObject current = null;

        EObject iv_rulesubprogram_parameter_list = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2098:2: (iv_rulesubprogram_parameter_list= rulesubprogram_parameter_list EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2099:2: iv_rulesubprogram_parameter_list= rulesubprogram_parameter_list EOF
            {
             newCompositeNode(grammarAccess.getSubprogram_parameter_listRule()); 
            pushFollow(FOLLOW_rulesubprogram_parameter_list_in_entryRulesubprogram_parameter_list3927);
            iv_rulesubprogram_parameter_list=rulesubprogram_parameter_list();

            state._fsp--;

             current =iv_rulesubprogram_parameter_list; 
            match(input,EOF,FOLLOW_EOF_in_entryRulesubprogram_parameter_list3937); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulesubprogram_parameter_list"


    // $ANTLR start "rulesubprogram_parameter_list"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2106:1: rulesubprogram_parameter_list returns [EObject current=null] : ( ( (lv_spl1_0_0= ruleparameter_label ) ) ( ( (lv_spl2_1_0= ',' ) ) ( (lv_spl3_2_0= ruleparameter_label ) ) )* ) ;
    public final EObject rulesubprogram_parameter_list() throws RecognitionException {
        EObject current = null;

        Token lv_spl2_1_0=null;
        EObject lv_spl1_0_0 = null;

        EObject lv_spl3_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2109:28: ( ( ( (lv_spl1_0_0= ruleparameter_label ) ) ( ( (lv_spl2_1_0= ',' ) ) ( (lv_spl3_2_0= ruleparameter_label ) ) )* ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2110:1: ( ( (lv_spl1_0_0= ruleparameter_label ) ) ( ( (lv_spl2_1_0= ',' ) ) ( (lv_spl3_2_0= ruleparameter_label ) ) )* )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2110:1: ( ( (lv_spl1_0_0= ruleparameter_label ) ) ( ( (lv_spl2_1_0= ',' ) ) ( (lv_spl3_2_0= ruleparameter_label ) ) )* )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2110:2: ( (lv_spl1_0_0= ruleparameter_label ) ) ( ( (lv_spl2_1_0= ',' ) ) ( (lv_spl3_2_0= ruleparameter_label ) ) )*
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2110:2: ( (lv_spl1_0_0= ruleparameter_label ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2111:1: (lv_spl1_0_0= ruleparameter_label )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2111:1: (lv_spl1_0_0= ruleparameter_label )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2112:3: lv_spl1_0_0= ruleparameter_label
            {
             
            	        newCompositeNode(grammarAccess.getSubprogram_parameter_listAccess().getSpl1Parameter_labelParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleparameter_label_in_rulesubprogram_parameter_list3983);
            lv_spl1_0_0=ruleparameter_label();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSubprogram_parameter_listRule());
            	        }
                   		set(
                   			current, 
                   			"spl1",
                    		lv_spl1_0_0, 
                    		"parameter_label");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2128:2: ( ( (lv_spl2_1_0= ',' ) ) ( (lv_spl3_2_0= ruleparameter_label ) ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==32) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2128:3: ( (lv_spl2_1_0= ',' ) ) ( (lv_spl3_2_0= ruleparameter_label ) )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2128:3: ( (lv_spl2_1_0= ',' ) )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2129:1: (lv_spl2_1_0= ',' )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2129:1: (lv_spl2_1_0= ',' )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2130:3: lv_spl2_1_0= ','
            	    {
            	    lv_spl2_1_0=(Token)match(input,32,FOLLOW_32_in_rulesubprogram_parameter_list4002); 

            	            newLeafNode(lv_spl2_1_0, grammarAccess.getSubprogram_parameter_listAccess().getSpl2CommaKeyword_1_0_0());
            	        

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getSubprogram_parameter_listRule());
            	    	        }
            	           		addWithLastConsumed(current, "spl2", lv_spl2_1_0, ",");
            	    	    

            	    }


            	    }

            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2143:2: ( (lv_spl3_2_0= ruleparameter_label ) )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2144:1: (lv_spl3_2_0= ruleparameter_label )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2144:1: (lv_spl3_2_0= ruleparameter_label )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2145:3: lv_spl3_2_0= ruleparameter_label
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getSubprogram_parameter_listAccess().getSpl3Parameter_labelParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleparameter_label_in_rulesubprogram_parameter_list4036);
            	    lv_spl3_2_0=ruleparameter_label();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getSubprogram_parameter_listRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"spl3",
            	            		lv_spl3_2_0, 
            	            		"parameter_label");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulesubprogram_parameter_list"


    // $ANTLR start "entryRuleparameter_label"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2169:1: entryRuleparameter_label returns [EObject current=null] : iv_ruleparameter_label= ruleparameter_label EOF ;
    public final EObject entryRuleparameter_label() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleparameter_label = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2170:2: (iv_ruleparameter_label= ruleparameter_label EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2171:2: iv_ruleparameter_label= ruleparameter_label EOF
            {
             newCompositeNode(grammarAccess.getParameter_labelRule()); 
            pushFollow(FOLLOW_ruleparameter_label_in_entryRuleparameter_label4074);
            iv_ruleparameter_label=ruleparameter_label();

            state._fsp--;

             current =iv_ruleparameter_label; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleparameter_label4084); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleparameter_label"


    // $ANTLR start "ruleparameter_label"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2178:1: ruleparameter_label returns [EObject current=null] : ( (lv_pl1_0_0= rulevalue_expression ) ) ;
    public final EObject ruleparameter_label() throws RecognitionException {
        EObject current = null;

        EObject lv_pl1_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2181:28: ( ( (lv_pl1_0_0= rulevalue_expression ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2182:1: ( (lv_pl1_0_0= rulevalue_expression ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2182:1: ( (lv_pl1_0_0= rulevalue_expression ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2183:1: (lv_pl1_0_0= rulevalue_expression )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2183:1: (lv_pl1_0_0= rulevalue_expression )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2184:3: lv_pl1_0_0= rulevalue_expression
            {
             
            	        newCompositeNode(grammarAccess.getParameter_labelAccess().getPl1Value_expressionParserRuleCall_0()); 
            	    
            pushFollow(FOLLOW_rulevalue_expression_in_ruleparameter_label4129);
            lv_pl1_0_0=rulevalue_expression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getParameter_labelRule());
            	        }
                   		set(
                   			current, 
                   			"pl1",
                    		lv_pl1_0_0, 
                    		"value_expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleparameter_label"


    // $ANTLR start "entryRuledata_component_reference"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2208:1: entryRuledata_component_reference returns [EObject current=null] : iv_ruledata_component_reference= ruledata_component_reference EOF ;
    public final EObject entryRuledata_component_reference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruledata_component_reference = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2209:2: (iv_ruledata_component_reference= ruledata_component_reference EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2210:2: iv_ruledata_component_reference= ruledata_component_reference EOF
            {
             newCompositeNode(grammarAccess.getData_component_referenceRule()); 
            pushFollow(FOLLOW_ruledata_component_reference_in_entryRuledata_component_reference4164);
            iv_ruledata_component_reference=ruledata_component_reference();

            state._fsp--;

             current =iv_ruledata_component_reference; 
            match(input,EOF,FOLLOW_EOF_in_entryRuledata_component_reference4174); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuledata_component_reference"


    // $ANTLR start "ruledata_component_reference"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2217:1: ruledata_component_reference returns [EObject current=null] : ( ( (lv_dcr1_0_0= RULE_STRING_LITERAL ) ) ( ( (lv_dcr2_1_0= '.' ) ) ( (lv_dcr3_2_0= RULE_STRING_LITERAL ) ) )* ) ;
    public final EObject ruledata_component_reference() throws RecognitionException {
        EObject current = null;

        Token lv_dcr1_0_0=null;
        Token lv_dcr2_1_0=null;
        Token lv_dcr3_2_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2220:28: ( ( ( (lv_dcr1_0_0= RULE_STRING_LITERAL ) ) ( ( (lv_dcr2_1_0= '.' ) ) ( (lv_dcr3_2_0= RULE_STRING_LITERAL ) ) )* ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2221:1: ( ( (lv_dcr1_0_0= RULE_STRING_LITERAL ) ) ( ( (lv_dcr2_1_0= '.' ) ) ( (lv_dcr3_2_0= RULE_STRING_LITERAL ) ) )* )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2221:1: ( ( (lv_dcr1_0_0= RULE_STRING_LITERAL ) ) ( ( (lv_dcr2_1_0= '.' ) ) ( (lv_dcr3_2_0= RULE_STRING_LITERAL ) ) )* )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2221:2: ( (lv_dcr1_0_0= RULE_STRING_LITERAL ) ) ( ( (lv_dcr2_1_0= '.' ) ) ( (lv_dcr3_2_0= RULE_STRING_LITERAL ) ) )*
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2221:2: ( (lv_dcr1_0_0= RULE_STRING_LITERAL ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2222:1: (lv_dcr1_0_0= RULE_STRING_LITERAL )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2222:1: (lv_dcr1_0_0= RULE_STRING_LITERAL )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2223:3: lv_dcr1_0_0= RULE_STRING_LITERAL
            {
            lv_dcr1_0_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_ruledata_component_reference4216); 

            			newLeafNode(lv_dcr1_0_0, grammarAccess.getData_component_referenceAccess().getDcr1String_literalTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getData_component_referenceRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"dcr1",
                    		lv_dcr1_0_0, 
                    		"string_literal");
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2239:2: ( ( (lv_dcr2_1_0= '.' ) ) ( (lv_dcr3_2_0= RULE_STRING_LITERAL ) ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==44) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2239:3: ( (lv_dcr2_1_0= '.' ) ) ( (lv_dcr3_2_0= RULE_STRING_LITERAL ) )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2239:3: ( (lv_dcr2_1_0= '.' ) )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2240:1: (lv_dcr2_1_0= '.' )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2240:1: (lv_dcr2_1_0= '.' )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2241:3: lv_dcr2_1_0= '.'
            	    {
            	    lv_dcr2_1_0=(Token)match(input,44,FOLLOW_44_in_ruledata_component_reference4240); 

            	            newLeafNode(lv_dcr2_1_0, grammarAccess.getData_component_referenceAccess().getDcr2FullStopKeyword_1_0_0());
            	        

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getData_component_referenceRule());
            	    	        }
            	           		addWithLastConsumed(current, "dcr2", lv_dcr2_1_0, ".");
            	    	    

            	    }


            	    }

            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2254:2: ( (lv_dcr3_2_0= RULE_STRING_LITERAL ) )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2255:1: (lv_dcr3_2_0= RULE_STRING_LITERAL )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2255:1: (lv_dcr3_2_0= RULE_STRING_LITERAL )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2256:3: lv_dcr3_2_0= RULE_STRING_LITERAL
            	    {
            	    lv_dcr3_2_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_ruledata_component_reference4270); 

            	    			newLeafNode(lv_dcr3_2_0, grammarAccess.getData_component_referenceAccess().getDcr3String_literalTerminalRuleCall_1_1_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getData_component_referenceRule());
            	    	        }
            	           		addWithLastConsumed(
            	           			current, 
            	           			"dcr3",
            	            		lv_dcr3_2_0, 
            	            		"string_literal");
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruledata_component_reference"


    // $ANTLR start "entryRulearray_index"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2282:1: entryRulearray_index returns [EObject current=null] : iv_rulearray_index= rulearray_index EOF ;
    public final EObject entryRulearray_index() throws RecognitionException {
        EObject current = null;

        EObject iv_rulearray_index = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2283:2: (iv_rulearray_index= rulearray_index EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2284:2: iv_rulearray_index= rulearray_index EOF
            {
             newCompositeNode(grammarAccess.getArray_indexRule()); 
            pushFollow(FOLLOW_rulearray_index_in_entryRulearray_index4315);
            iv_rulearray_index=rulearray_index();

            state._fsp--;

             current =iv_rulearray_index; 
            match(input,EOF,FOLLOW_EOF_in_entryRulearray_index4325); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulearray_index"


    // $ANTLR start "rulearray_index"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2291:1: rulearray_index returns [EObject current=null] : ( ( (lv_ai1_0_0= '[' ) ) ( (lv_ai2_1_0= ruleinteger_literal ) ) ( (lv_ai3_2_0= ']' ) ) ) ;
    public final EObject rulearray_index() throws RecognitionException {
        EObject current = null;

        Token lv_ai1_0_0=null;
        Token lv_ai3_2_0=null;
        EObject lv_ai2_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2294:28: ( ( ( (lv_ai1_0_0= '[' ) ) ( (lv_ai2_1_0= ruleinteger_literal ) ) ( (lv_ai3_2_0= ']' ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2295:1: ( ( (lv_ai1_0_0= '[' ) ) ( (lv_ai2_1_0= ruleinteger_literal ) ) ( (lv_ai3_2_0= ']' ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2295:1: ( ( (lv_ai1_0_0= '[' ) ) ( (lv_ai2_1_0= ruleinteger_literal ) ) ( (lv_ai3_2_0= ']' ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2295:2: ( (lv_ai1_0_0= '[' ) ) ( (lv_ai2_1_0= ruleinteger_literal ) ) ( (lv_ai3_2_0= ']' ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2295:2: ( (lv_ai1_0_0= '[' ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2296:1: (lv_ai1_0_0= '[' )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2296:1: (lv_ai1_0_0= '[' )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2297:3: lv_ai1_0_0= '['
            {
            lv_ai1_0_0=(Token)match(input,45,FOLLOW_45_in_rulearray_index4368); 

                    newLeafNode(lv_ai1_0_0, grammarAccess.getArray_indexAccess().getAi1LeftSquareBracketKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getArray_indexRule());
            	        }
                   		setWithLastConsumed(current, "ai1", lv_ai1_0_0, "[");
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2310:2: ( (lv_ai2_1_0= ruleinteger_literal ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2311:1: (lv_ai2_1_0= ruleinteger_literal )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2311:1: (lv_ai2_1_0= ruleinteger_literal )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2312:3: lv_ai2_1_0= ruleinteger_literal
            {
             
            	        newCompositeNode(grammarAccess.getArray_indexAccess().getAi2Integer_literalParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleinteger_literal_in_rulearray_index4402);
            lv_ai2_1_0=ruleinteger_literal();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getArray_indexRule());
            	        }
                   		set(
                   			current, 
                   			"ai2",
                    		lv_ai2_1_0, 
                    		"integer_literal");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2328:2: ( (lv_ai3_2_0= ']' ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2329:1: (lv_ai3_2_0= ']' )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2329:1: (lv_ai3_2_0= ']' )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2330:3: lv_ai3_2_0= ']'
            {
            lv_ai3_2_0=(Token)match(input,46,FOLLOW_46_in_rulearray_index4420); 

                    newLeafNode(lv_ai3_2_0, grammarAccess.getArray_indexAccess().getAi3RightSquareBracketKeyword_2_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getArray_indexRule());
            	        }
                   		setWithLastConsumed(current, "ai3", lv_ai3_2_0, "]");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulearray_index"


    // $ANTLR start "entryRuleinteger_value"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2351:1: entryRuleinteger_value returns [EObject current=null] : iv_ruleinteger_value= ruleinteger_value EOF ;
    public final EObject entryRuleinteger_value() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleinteger_value = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2352:2: (iv_ruleinteger_value= ruleinteger_value EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2353:2: iv_ruleinteger_value= ruleinteger_value EOF
            {
             newCompositeNode(grammarAccess.getInteger_valueRule()); 
            pushFollow(FOLLOW_ruleinteger_value_in_entryRuleinteger_value4469);
            iv_ruleinteger_value=ruleinteger_value();

            state._fsp--;

             current =iv_ruleinteger_value; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleinteger_value4479); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleinteger_value"


    // $ANTLR start "ruleinteger_value"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2360:1: ruleinteger_value returns [EObject current=null] : ( (lv_ivname_0_0= rulenumeric_literal ) ) ;
    public final EObject ruleinteger_value() throws RecognitionException {
        EObject current = null;

        EObject lv_ivname_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2363:28: ( ( (lv_ivname_0_0= rulenumeric_literal ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2364:1: ( (lv_ivname_0_0= rulenumeric_literal ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2364:1: ( (lv_ivname_0_0= rulenumeric_literal ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2365:1: (lv_ivname_0_0= rulenumeric_literal )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2365:1: (lv_ivname_0_0= rulenumeric_literal )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2366:3: lv_ivname_0_0= rulenumeric_literal
            {
             
            	        newCompositeNode(grammarAccess.getInteger_valueAccess().getIvnameNumeric_literalParserRuleCall_0()); 
            	    
            pushFollow(FOLLOW_rulenumeric_literal_in_ruleinteger_value4524);
            lv_ivname_0_0=rulenumeric_literal();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getInteger_valueRule());
            	        }
                   		set(
                   			current, 
                   			"ivname",
                    		lv_ivname_0_0, 
                    		"numeric_literal");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleinteger_value"


    // $ANTLR start "entryRulenumeric_literal"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2390:1: entryRulenumeric_literal returns [EObject current=null] : iv_rulenumeric_literal= rulenumeric_literal EOF ;
    public final EObject entryRulenumeric_literal() throws RecognitionException {
        EObject current = null;

        EObject iv_rulenumeric_literal = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2391:2: (iv_rulenumeric_literal= rulenumeric_literal EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2392:2: iv_rulenumeric_literal= rulenumeric_literal EOF
            {
             newCompositeNode(grammarAccess.getNumeric_literalRule()); 
            pushFollow(FOLLOW_rulenumeric_literal_in_entryRulenumeric_literal4559);
            iv_rulenumeric_literal=rulenumeric_literal();

            state._fsp--;

             current =iv_rulenumeric_literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRulenumeric_literal4569); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulenumeric_literal"


    // $ANTLR start "rulenumeric_literal"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2399:1: rulenumeric_literal returns [EObject current=null] : ( ( (lv_nlname1_0_0= ruleinteger_literal ) ) | ( (lv_nlname2_1_0= rulereal_literal ) ) ) ;
    public final EObject rulenumeric_literal() throws RecognitionException {
        EObject current = null;

        EObject lv_nlname1_0_0 = null;

        EObject lv_nlname2_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2402:28: ( ( ( (lv_nlname1_0_0= ruleinteger_literal ) ) | ( (lv_nlname2_1_0= rulereal_literal ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2403:1: ( ( (lv_nlname1_0_0= ruleinteger_literal ) ) | ( (lv_nlname2_1_0= rulereal_literal ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2403:1: ( ( (lv_nlname1_0_0= ruleinteger_literal ) ) | ( (lv_nlname2_1_0= rulereal_literal ) ) )
            int alt17=2;
            alt17 = dfa17.predict(input);
            switch (alt17) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2403:2: ( (lv_nlname1_0_0= ruleinteger_literal ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2403:2: ( (lv_nlname1_0_0= ruleinteger_literal ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2404:1: (lv_nlname1_0_0= ruleinteger_literal )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2404:1: (lv_nlname1_0_0= ruleinteger_literal )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2405:3: lv_nlname1_0_0= ruleinteger_literal
                    {
                     
                    	        newCompositeNode(grammarAccess.getNumeric_literalAccess().getNlname1Integer_literalParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleinteger_literal_in_rulenumeric_literal4615);
                    lv_nlname1_0_0=ruleinteger_literal();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getNumeric_literalRule());
                    	        }
                           		set(
                           			current, 
                           			"nlname1",
                            		lv_nlname1_0_0, 
                            		"integer_literal");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2422:6: ( (lv_nlname2_1_0= rulereal_literal ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2422:6: ( (lv_nlname2_1_0= rulereal_literal ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2423:1: (lv_nlname2_1_0= rulereal_literal )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2423:1: (lv_nlname2_1_0= rulereal_literal )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2424:3: lv_nlname2_1_0= rulereal_literal
                    {
                     
                    	        newCompositeNode(grammarAccess.getNumeric_literalAccess().getNlname2Real_literalParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulereal_literal_in_rulenumeric_literal4642);
                    lv_nlname2_1_0=rulereal_literal();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getNumeric_literalRule());
                    	        }
                           		set(
                           			current, 
                           			"nlname2",
                            		lv_nlname2_1_0, 
                            		"real_literal");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulenumeric_literal"


    // $ANTLR start "entryRuleinteger_literal"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2448:1: entryRuleinteger_literal returns [EObject current=null] : iv_ruleinteger_literal= ruleinteger_literal EOF ;
    public final EObject entryRuleinteger_literal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleinteger_literal = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2449:2: (iv_ruleinteger_literal= ruleinteger_literal EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2450:2: iv_ruleinteger_literal= ruleinteger_literal EOF
            {
             newCompositeNode(grammarAccess.getInteger_literalRule()); 
            pushFollow(FOLLOW_ruleinteger_literal_in_entryRuleinteger_literal4678);
            iv_ruleinteger_literal=ruleinteger_literal();

            state._fsp--;

             current =iv_ruleinteger_literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleinteger_literal4688); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleinteger_literal"


    // $ANTLR start "ruleinteger_literal"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2457:1: ruleinteger_literal returns [EObject current=null] : ( ( (lv_ilname1_0_0= ruledecimal_integer_literal ) ) | ( (lv_ilname2_1_0= rulebased_integer_literal ) ) ) ;
    public final EObject ruleinteger_literal() throws RecognitionException {
        EObject current = null;

        EObject lv_ilname1_0_0 = null;

        EObject lv_ilname2_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2460:28: ( ( ( (lv_ilname1_0_0= ruledecimal_integer_literal ) ) | ( (lv_ilname2_1_0= rulebased_integer_literal ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2461:1: ( ( (lv_ilname1_0_0= ruledecimal_integer_literal ) ) | ( (lv_ilname2_1_0= rulebased_integer_literal ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2461:1: ( ( (lv_ilname1_0_0= ruledecimal_integer_literal ) ) | ( (lv_ilname2_1_0= rulebased_integer_literal ) ) )
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==RULE_INT) ) {
                alt18=1;
            }
            else if ( (LA18_0==RULE_DIGIT) ) {
                switch ( input.LA(2) ) {
                case EOF:
                case RULE_INT:
                case 15:
                case 19:
                case 20:
                case 21:
                case 22:
                case 29:
                case 30:
                case 31:
                case 32:
                case 43:
                case 46:
                case 47:
                case 48:
                case 49:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                case 68:
                case 69:
                case 70:
                case 71:
                case 72:
                case 73:
                case 74:
                case 75:
                case 76:
                case 81:
                    {
                    alt18=1;
                    }
                    break;
                case RULE_DIGIT:
                    {
                    int LA18_3 = input.LA(3);

                    if ( (LA18_3==51) ) {
                        alt18=2;
                    }
                    else if ( (LA18_3==EOF||LA18_3==RULE_DIGIT||LA18_3==RULE_INT||LA18_3==15||(LA18_3>=19 && LA18_3<=22)||(LA18_3>=29 && LA18_3<=32)||LA18_3==43||(LA18_3>=46 && LA18_3<=49)||(LA18_3>=52 && LA18_3<=58)||(LA18_3>=62 && LA18_3<=76)||LA18_3==81) ) {
                        alt18=1;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 18, 3, input);

                        throw nvae;
                    }
                    }
                    break;
                case 51:
                    {
                    alt18=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 18, 2, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }
            switch (alt18) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2461:2: ( (lv_ilname1_0_0= ruledecimal_integer_literal ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2461:2: ( (lv_ilname1_0_0= ruledecimal_integer_literal ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2462:1: (lv_ilname1_0_0= ruledecimal_integer_literal )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2462:1: (lv_ilname1_0_0= ruledecimal_integer_literal )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2463:3: lv_ilname1_0_0= ruledecimal_integer_literal
                    {
                     
                    	        newCompositeNode(grammarAccess.getInteger_literalAccess().getIlname1Decimal_integer_literalParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruledecimal_integer_literal_in_ruleinteger_literal4734);
                    lv_ilname1_0_0=ruledecimal_integer_literal();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getInteger_literalRule());
                    	        }
                           		set(
                           			current, 
                           			"ilname1",
                            		lv_ilname1_0_0, 
                            		"decimal_integer_literal");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2480:6: ( (lv_ilname2_1_0= rulebased_integer_literal ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2480:6: ( (lv_ilname2_1_0= rulebased_integer_literal ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2481:1: (lv_ilname2_1_0= rulebased_integer_literal )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2481:1: (lv_ilname2_1_0= rulebased_integer_literal )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2482:3: lv_ilname2_1_0= rulebased_integer_literal
                    {
                     
                    	        newCompositeNode(grammarAccess.getInteger_literalAccess().getIlname2Based_integer_literalParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulebased_integer_literal_in_ruleinteger_literal4761);
                    lv_ilname2_1_0=rulebased_integer_literal();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getInteger_literalRule());
                    	        }
                           		set(
                           			current, 
                           			"ilname2",
                            		lv_ilname2_1_0, 
                            		"based_integer_literal");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleinteger_literal"


    // $ANTLR start "entryRulereal_literal"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2506:1: entryRulereal_literal returns [EObject current=null] : iv_rulereal_literal= rulereal_literal EOF ;
    public final EObject entryRulereal_literal() throws RecognitionException {
        EObject current = null;

        EObject iv_rulereal_literal = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2507:2: (iv_rulereal_literal= rulereal_literal EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2508:2: iv_rulereal_literal= rulereal_literal EOF
            {
             newCompositeNode(grammarAccess.getReal_literalRule()); 
            pushFollow(FOLLOW_rulereal_literal_in_entryRulereal_literal4797);
            iv_rulereal_literal=rulereal_literal();

            state._fsp--;

             current =iv_rulereal_literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRulereal_literal4807); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulereal_literal"


    // $ANTLR start "rulereal_literal"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2515:1: rulereal_literal returns [EObject current=null] : ( (lv_rlname_0_0= ruledecimal_real_literal ) ) ;
    public final EObject rulereal_literal() throws RecognitionException {
        EObject current = null;

        EObject lv_rlname_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2518:28: ( ( (lv_rlname_0_0= ruledecimal_real_literal ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2519:1: ( (lv_rlname_0_0= ruledecimal_real_literal ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2519:1: ( (lv_rlname_0_0= ruledecimal_real_literal ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2520:1: (lv_rlname_0_0= ruledecimal_real_literal )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2520:1: (lv_rlname_0_0= ruledecimal_real_literal )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2521:3: lv_rlname_0_0= ruledecimal_real_literal
            {
             
            	        newCompositeNode(grammarAccess.getReal_literalAccess().getRlnameDecimal_real_literalParserRuleCall_0()); 
            	    
            pushFollow(FOLLOW_ruledecimal_real_literal_in_rulereal_literal4852);
            lv_rlname_0_0=ruledecimal_real_literal();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getReal_literalRule());
            	        }
                   		set(
                   			current, 
                   			"rlname",
                    		lv_rlname_0_0, 
                    		"decimal_real_literal");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulereal_literal"


    // $ANTLR start "entryRuledecimal_integer_literal"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2545:1: entryRuledecimal_integer_literal returns [EObject current=null] : iv_ruledecimal_integer_literal= ruledecimal_integer_literal EOF ;
    public final EObject entryRuledecimal_integer_literal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruledecimal_integer_literal = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2546:2: (iv_ruledecimal_integer_literal= ruledecimal_integer_literal EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2547:2: iv_ruledecimal_integer_literal= ruledecimal_integer_literal EOF
            {
             newCompositeNode(grammarAccess.getDecimal_integer_literalRule()); 
            pushFollow(FOLLOW_ruledecimal_integer_literal_in_entryRuledecimal_integer_literal4887);
            iv_ruledecimal_integer_literal=ruledecimal_integer_literal();

            state._fsp--;

             current =iv_ruledecimal_integer_literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRuledecimal_integer_literal4897); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuledecimal_integer_literal"


    // $ANTLR start "ruledecimal_integer_literal"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2554:1: ruledecimal_integer_literal returns [EObject current=null] : ( ( (lv_dilname1_0_0= rulenumeral ) ) ( (lv_dilname2_1_0= rulepositive_exponent ) )? ) ;
    public final EObject ruledecimal_integer_literal() throws RecognitionException {
        EObject current = null;

        EObject lv_dilname1_0_0 = null;

        EObject lv_dilname2_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2557:28: ( ( ( (lv_dilname1_0_0= rulenumeral ) ) ( (lv_dilname2_1_0= rulepositive_exponent ) )? ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2558:1: ( ( (lv_dilname1_0_0= rulenumeral ) ) ( (lv_dilname2_1_0= rulepositive_exponent ) )? )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2558:1: ( ( (lv_dilname1_0_0= rulenumeral ) ) ( (lv_dilname2_1_0= rulepositive_exponent ) )? )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2558:2: ( (lv_dilname1_0_0= rulenumeral ) ) ( (lv_dilname2_1_0= rulepositive_exponent ) )?
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2558:2: ( (lv_dilname1_0_0= rulenumeral ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2559:1: (lv_dilname1_0_0= rulenumeral )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2559:1: (lv_dilname1_0_0= rulenumeral )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2560:3: lv_dilname1_0_0= rulenumeral
            {
             
            	        newCompositeNode(grammarAccess.getDecimal_integer_literalAccess().getDilname1NumeralParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_rulenumeral_in_ruledecimal_integer_literal4943);
            lv_dilname1_0_0=rulenumeral();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getDecimal_integer_literalRule());
            	        }
                   		set(
                   			current, 
                   			"dilname1",
                    		lv_dilname1_0_0, 
                    		"numeral");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2576:2: ( (lv_dilname2_1_0= rulepositive_exponent ) )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==48) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2577:1: (lv_dilname2_1_0= rulepositive_exponent )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2577:1: (lv_dilname2_1_0= rulepositive_exponent )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2578:3: lv_dilname2_1_0= rulepositive_exponent
                    {
                     
                    	        newCompositeNode(grammarAccess.getDecimal_integer_literalAccess().getDilname2Positive_exponentParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulepositive_exponent_in_ruledecimal_integer_literal4964);
                    lv_dilname2_1_0=rulepositive_exponent();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDecimal_integer_literalRule());
                    	        }
                           		set(
                           			current, 
                           			"dilname2",
                            		lv_dilname2_1_0, 
                            		"positive_exponent");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruledecimal_integer_literal"


    // $ANTLR start "entryRuledecimal_real_literal"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2602:1: entryRuledecimal_real_literal returns [EObject current=null] : iv_ruledecimal_real_literal= ruledecimal_real_literal EOF ;
    public final EObject entryRuledecimal_real_literal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruledecimal_real_literal = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2603:2: (iv_ruledecimal_real_literal= ruledecimal_real_literal EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2604:2: iv_ruledecimal_real_literal= ruledecimal_real_literal EOF
            {
             newCompositeNode(grammarAccess.getDecimal_real_literalRule()); 
            pushFollow(FOLLOW_ruledecimal_real_literal_in_entryRuledecimal_real_literal5001);
            iv_ruledecimal_real_literal=ruledecimal_real_literal();

            state._fsp--;

             current =iv_ruledecimal_real_literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRuledecimal_real_literal5011); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuledecimal_real_literal"


    // $ANTLR start "ruledecimal_real_literal"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2611:1: ruledecimal_real_literal returns [EObject current=null] : ( ( (lv_drlname1_0_0= rulenumeral ) ) otherlv_1= '.' ( (lv_drlname2_2_0= rulenumeral ) ) ( (lv_drlname3_3_0= ruleexponent ) )? ) ;
    public final EObject ruledecimal_real_literal() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_drlname1_0_0 = null;

        EObject lv_drlname2_2_0 = null;

        EObject lv_drlname3_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2614:28: ( ( ( (lv_drlname1_0_0= rulenumeral ) ) otherlv_1= '.' ( (lv_drlname2_2_0= rulenumeral ) ) ( (lv_drlname3_3_0= ruleexponent ) )? ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2615:1: ( ( (lv_drlname1_0_0= rulenumeral ) ) otherlv_1= '.' ( (lv_drlname2_2_0= rulenumeral ) ) ( (lv_drlname3_3_0= ruleexponent ) )? )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2615:1: ( ( (lv_drlname1_0_0= rulenumeral ) ) otherlv_1= '.' ( (lv_drlname2_2_0= rulenumeral ) ) ( (lv_drlname3_3_0= ruleexponent ) )? )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2615:2: ( (lv_drlname1_0_0= rulenumeral ) ) otherlv_1= '.' ( (lv_drlname2_2_0= rulenumeral ) ) ( (lv_drlname3_3_0= ruleexponent ) )?
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2615:2: ( (lv_drlname1_0_0= rulenumeral ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2616:1: (lv_drlname1_0_0= rulenumeral )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2616:1: (lv_drlname1_0_0= rulenumeral )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2617:3: lv_drlname1_0_0= rulenumeral
            {
             
            	        newCompositeNode(grammarAccess.getDecimal_real_literalAccess().getDrlname1NumeralParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_rulenumeral_in_ruledecimal_real_literal5057);
            lv_drlname1_0_0=rulenumeral();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getDecimal_real_literalRule());
            	        }
                   		set(
                   			current, 
                   			"drlname1",
                    		lv_drlname1_0_0, 
                    		"numeral");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,44,FOLLOW_44_in_ruledecimal_real_literal5069); 

                	newLeafNode(otherlv_1, grammarAccess.getDecimal_real_literalAccess().getFullStopKeyword_1());
                
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2637:1: ( (lv_drlname2_2_0= rulenumeral ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2638:1: (lv_drlname2_2_0= rulenumeral )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2638:1: (lv_drlname2_2_0= rulenumeral )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2639:3: lv_drlname2_2_0= rulenumeral
            {
             
            	        newCompositeNode(grammarAccess.getDecimal_real_literalAccess().getDrlname2NumeralParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_rulenumeral_in_ruledecimal_real_literal5090);
            lv_drlname2_2_0=rulenumeral();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getDecimal_real_literalRule());
            	        }
                   		set(
                   			current, 
                   			"drlname2",
                    		lv_drlname2_2_0, 
                    		"numeral");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2655:2: ( (lv_drlname3_3_0= ruleexponent ) )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==48||LA20_0==50) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2656:1: (lv_drlname3_3_0= ruleexponent )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2656:1: (lv_drlname3_3_0= ruleexponent )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2657:3: lv_drlname3_3_0= ruleexponent
                    {
                     
                    	        newCompositeNode(grammarAccess.getDecimal_real_literalAccess().getDrlname3ExponentParserRuleCall_3_0()); 
                    	    
                    pushFollow(FOLLOW_ruleexponent_in_ruledecimal_real_literal5111);
                    lv_drlname3_3_0=ruleexponent();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getDecimal_real_literalRule());
                    	        }
                           		set(
                           			current, 
                           			"drlname3",
                            		lv_drlname3_3_0, 
                            		"exponent");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruledecimal_real_literal"


    // $ANTLR start "entryRulenumeral"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2681:1: entryRulenumeral returns [EObject current=null] : iv_rulenumeral= rulenumeral EOF ;
    public final EObject entryRulenumeral() throws RecognitionException {
        EObject current = null;

        EObject iv_rulenumeral = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2682:2: (iv_rulenumeral= rulenumeral EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2683:2: iv_rulenumeral= rulenumeral EOF
            {
             newCompositeNode(grammarAccess.getNumeralRule()); 
            pushFollow(FOLLOW_rulenumeral_in_entryRulenumeral5148);
            iv_rulenumeral=rulenumeral();

            state._fsp--;

             current =iv_rulenumeral; 
            match(input,EOF,FOLLOW_EOF_in_entryRulenumeral5158); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulenumeral"


    // $ANTLR start "rulenumeral"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2690:1: rulenumeral returns [EObject current=null] : ( ( (lv_nname1_0_0= ruleinteger ) ) ( (otherlv_1= '_' )? ( (lv_nname2_2_0= ruleinteger ) ) )* ) ;
    public final EObject rulenumeral() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_nname1_0_0 = null;

        EObject lv_nname2_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2693:28: ( ( ( (lv_nname1_0_0= ruleinteger ) ) ( (otherlv_1= '_' )? ( (lv_nname2_2_0= ruleinteger ) ) )* ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2694:1: ( ( (lv_nname1_0_0= ruleinteger ) ) ( (otherlv_1= '_' )? ( (lv_nname2_2_0= ruleinteger ) ) )* )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2694:1: ( ( (lv_nname1_0_0= ruleinteger ) ) ( (otherlv_1= '_' )? ( (lv_nname2_2_0= ruleinteger ) ) )* )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2694:2: ( (lv_nname1_0_0= ruleinteger ) ) ( (otherlv_1= '_' )? ( (lv_nname2_2_0= ruleinteger ) ) )*
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2694:2: ( (lv_nname1_0_0= ruleinteger ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2695:1: (lv_nname1_0_0= ruleinteger )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2695:1: (lv_nname1_0_0= ruleinteger )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2696:3: lv_nname1_0_0= ruleinteger
            {
             
            	        newCompositeNode(grammarAccess.getNumeralAccess().getNname1IntegerParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleinteger_in_rulenumeral5204);
            lv_nname1_0_0=ruleinteger();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getNumeralRule());
            	        }
                   		set(
                   			current, 
                   			"nname1",
                    		lv_nname1_0_0, 
                    		"integer");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2712:2: ( (otherlv_1= '_' )? ( (lv_nname2_2_0= ruleinteger ) ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==RULE_DIGIT||LA22_0==RULE_INT||LA22_0==47) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2712:3: (otherlv_1= '_' )? ( (lv_nname2_2_0= ruleinteger ) )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2712:3: (otherlv_1= '_' )?
            	    int alt21=2;
            	    int LA21_0 = input.LA(1);

            	    if ( (LA21_0==47) ) {
            	        alt21=1;
            	    }
            	    switch (alt21) {
            	        case 1 :
            	            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2712:5: otherlv_1= '_'
            	            {
            	            otherlv_1=(Token)match(input,47,FOLLOW_47_in_rulenumeral5218); 

            	                	newLeafNode(otherlv_1, grammarAccess.getNumeralAccess().get_Keyword_1_0());
            	                

            	            }
            	            break;

            	    }

            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2716:3: ( (lv_nname2_2_0= ruleinteger ) )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2717:1: (lv_nname2_2_0= ruleinteger )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2717:1: (lv_nname2_2_0= ruleinteger )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2718:3: lv_nname2_2_0= ruleinteger
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getNumeralAccess().getNname2IntegerParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleinteger_in_rulenumeral5241);
            	    lv_nname2_2_0=ruleinteger();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getNumeralRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"nname2",
            	            		lv_nname2_2_0, 
            	            		"integer");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulenumeral"


    // $ANTLR start "entryRuleexponent"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2742:1: entryRuleexponent returns [EObject current=null] : iv_ruleexponent= ruleexponent EOF ;
    public final EObject entryRuleexponent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleexponent = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2743:2: (iv_ruleexponent= ruleexponent EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2744:2: iv_ruleexponent= ruleexponent EOF
            {
             newCompositeNode(grammarAccess.getExponentRule()); 
            pushFollow(FOLLOW_ruleexponent_in_entryRuleexponent5279);
            iv_ruleexponent=ruleexponent();

            state._fsp--;

             current =iv_ruleexponent; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleexponent5289); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleexponent"


    // $ANTLR start "ruleexponent"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2751:1: ruleexponent returns [EObject current=null] : ( (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_ename1_2_0= rulenumeral ) ) ) | (otherlv_3= 'E -' ( (lv_ename2_4_0= rulenumeral ) ) ) ) ;
    public final EObject ruleexponent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_ename1_2_0 = null;

        EObject lv_ename2_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2754:28: ( ( (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_ename1_2_0= rulenumeral ) ) ) | (otherlv_3= 'E -' ( (lv_ename2_4_0= rulenumeral ) ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2755:1: ( (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_ename1_2_0= rulenumeral ) ) ) | (otherlv_3= 'E -' ( (lv_ename2_4_0= rulenumeral ) ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2755:1: ( (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_ename1_2_0= rulenumeral ) ) ) | (otherlv_3= 'E -' ( (lv_ename2_4_0= rulenumeral ) ) ) )
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==48) ) {
                alt24=1;
            }
            else if ( (LA24_0==50) ) {
                alt24=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }
            switch (alt24) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2755:2: (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_ename1_2_0= rulenumeral ) ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2755:2: (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_ename1_2_0= rulenumeral ) ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2755:4: otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_ename1_2_0= rulenumeral ) )
                    {
                    otherlv_0=(Token)match(input,48,FOLLOW_48_in_ruleexponent5327); 

                        	newLeafNode(otherlv_0, grammarAccess.getExponentAccess().getEKeyword_0_0());
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2759:1: (otherlv_1= '+' )?
                    int alt23=2;
                    int LA23_0 = input.LA(1);

                    if ( (LA23_0==49) ) {
                        alt23=1;
                    }
                    switch (alt23) {
                        case 1 :
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2759:3: otherlv_1= '+'
                            {
                            otherlv_1=(Token)match(input,49,FOLLOW_49_in_ruleexponent5340); 

                                	newLeafNode(otherlv_1, grammarAccess.getExponentAccess().getPlusSignKeyword_0_1());
                                

                            }
                            break;

                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2763:3: ( (lv_ename1_2_0= rulenumeral ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2764:1: (lv_ename1_2_0= rulenumeral )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2764:1: (lv_ename1_2_0= rulenumeral )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2765:3: lv_ename1_2_0= rulenumeral
                    {
                     
                    	        newCompositeNode(grammarAccess.getExponentAccess().getEname1NumeralParserRuleCall_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_rulenumeral_in_ruleexponent5363);
                    lv_ename1_2_0=rulenumeral();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getExponentRule());
                    	        }
                           		set(
                           			current, 
                           			"ename1",
                            		lv_ename1_2_0, 
                            		"numeral");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2782:6: (otherlv_3= 'E -' ( (lv_ename2_4_0= rulenumeral ) ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2782:6: (otherlv_3= 'E -' ( (lv_ename2_4_0= rulenumeral ) ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2782:8: otherlv_3= 'E -' ( (lv_ename2_4_0= rulenumeral ) )
                    {
                    otherlv_3=(Token)match(input,50,FOLLOW_50_in_ruleexponent5383); 

                        	newLeafNode(otherlv_3, grammarAccess.getExponentAccess().getEKeyword_1_0());
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2786:1: ( (lv_ename2_4_0= rulenumeral ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2787:1: (lv_ename2_4_0= rulenumeral )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2787:1: (lv_ename2_4_0= rulenumeral )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2788:3: lv_ename2_4_0= rulenumeral
                    {
                     
                    	        newCompositeNode(grammarAccess.getExponentAccess().getEname2NumeralParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulenumeral_in_ruleexponent5404);
                    lv_ename2_4_0=rulenumeral();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getExponentRule());
                    	        }
                           		set(
                           			current, 
                           			"ename2",
                            		lv_ename2_4_0, 
                            		"numeral");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleexponent"


    // $ANTLR start "entryRulepositive_exponent"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2812:1: entryRulepositive_exponent returns [EObject current=null] : iv_rulepositive_exponent= rulepositive_exponent EOF ;
    public final EObject entryRulepositive_exponent() throws RecognitionException {
        EObject current = null;

        EObject iv_rulepositive_exponent = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2813:2: (iv_rulepositive_exponent= rulepositive_exponent EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2814:2: iv_rulepositive_exponent= rulepositive_exponent EOF
            {
             newCompositeNode(grammarAccess.getPositive_exponentRule()); 
            pushFollow(FOLLOW_rulepositive_exponent_in_entryRulepositive_exponent5441);
            iv_rulepositive_exponent=rulepositive_exponent();

            state._fsp--;

             current =iv_rulepositive_exponent; 
            match(input,EOF,FOLLOW_EOF_in_entryRulepositive_exponent5451); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulepositive_exponent"


    // $ANTLR start "rulepositive_exponent"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2821:1: rulepositive_exponent returns [EObject current=null] : (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_pename_2_0= rulenumeral ) ) ) ;
    public final EObject rulepositive_exponent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_pename_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2824:28: ( (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_pename_2_0= rulenumeral ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2825:1: (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_pename_2_0= rulenumeral ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2825:1: (otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_pename_2_0= rulenumeral ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2825:3: otherlv_0= 'E' (otherlv_1= '+' )? ( (lv_pename_2_0= rulenumeral ) )
            {
            otherlv_0=(Token)match(input,48,FOLLOW_48_in_rulepositive_exponent5488); 

                	newLeafNode(otherlv_0, grammarAccess.getPositive_exponentAccess().getEKeyword_0());
                
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2829:1: (otherlv_1= '+' )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==49) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2829:3: otherlv_1= '+'
                    {
                    otherlv_1=(Token)match(input,49,FOLLOW_49_in_rulepositive_exponent5501); 

                        	newLeafNode(otherlv_1, grammarAccess.getPositive_exponentAccess().getPlusSignKeyword_1());
                        

                    }
                    break;

            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2833:3: ( (lv_pename_2_0= rulenumeral ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2834:1: (lv_pename_2_0= rulenumeral )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2834:1: (lv_pename_2_0= rulenumeral )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2835:3: lv_pename_2_0= rulenumeral
            {
             
            	        newCompositeNode(grammarAccess.getPositive_exponentAccess().getPenameNumeralParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_rulenumeral_in_rulepositive_exponent5524);
            lv_pename_2_0=rulenumeral();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPositive_exponentRule());
            	        }
                   		set(
                   			current, 
                   			"pename",
                    		lv_pename_2_0, 
                    		"numeral");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulepositive_exponent"


    // $ANTLR start "entryRulebased_integer_literal"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2859:1: entryRulebased_integer_literal returns [EObject current=null] : iv_rulebased_integer_literal= rulebased_integer_literal EOF ;
    public final EObject entryRulebased_integer_literal() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebased_integer_literal = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2860:2: (iv_rulebased_integer_literal= rulebased_integer_literal EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2861:2: iv_rulebased_integer_literal= rulebased_integer_literal EOF
            {
             newCompositeNode(grammarAccess.getBased_integer_literalRule()); 
            pushFollow(FOLLOW_rulebased_integer_literal_in_entryRulebased_integer_literal5560);
            iv_rulebased_integer_literal=rulebased_integer_literal();

            state._fsp--;

             current =iv_rulebased_integer_literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebased_integer_literal5570); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebased_integer_literal"


    // $ANTLR start "rulebased_integer_literal"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2868:1: rulebased_integer_literal returns [EObject current=null] : ( ( (lv_bilname1_0_0= rulebase ) ) otherlv_1= '#' ( (lv_bilname2_2_0= rulebased_numeral ) ) otherlv_3= '#' ( (lv_bilname3_4_0= rulepositive_exponent ) )? ) ;
    public final EObject rulebased_integer_literal() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_bilname1_0_0 = null;

        EObject lv_bilname2_2_0 = null;

        EObject lv_bilname3_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2871:28: ( ( ( (lv_bilname1_0_0= rulebase ) ) otherlv_1= '#' ( (lv_bilname2_2_0= rulebased_numeral ) ) otherlv_3= '#' ( (lv_bilname3_4_0= rulepositive_exponent ) )? ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2872:1: ( ( (lv_bilname1_0_0= rulebase ) ) otherlv_1= '#' ( (lv_bilname2_2_0= rulebased_numeral ) ) otherlv_3= '#' ( (lv_bilname3_4_0= rulepositive_exponent ) )? )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2872:1: ( ( (lv_bilname1_0_0= rulebase ) ) otherlv_1= '#' ( (lv_bilname2_2_0= rulebased_numeral ) ) otherlv_3= '#' ( (lv_bilname3_4_0= rulepositive_exponent ) )? )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2872:2: ( (lv_bilname1_0_0= rulebase ) ) otherlv_1= '#' ( (lv_bilname2_2_0= rulebased_numeral ) ) otherlv_3= '#' ( (lv_bilname3_4_0= rulepositive_exponent ) )?
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2872:2: ( (lv_bilname1_0_0= rulebase ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2873:1: (lv_bilname1_0_0= rulebase )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2873:1: (lv_bilname1_0_0= rulebase )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2874:3: lv_bilname1_0_0= rulebase
            {
             
            	        newCompositeNode(grammarAccess.getBased_integer_literalAccess().getBilname1BaseParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_rulebase_in_rulebased_integer_literal5616);
            lv_bilname1_0_0=rulebase();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBased_integer_literalRule());
            	        }
                   		set(
                   			current, 
                   			"bilname1",
                    		lv_bilname1_0_0, 
                    		"base");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,51,FOLLOW_51_in_rulebased_integer_literal5628); 

                	newLeafNode(otherlv_1, grammarAccess.getBased_integer_literalAccess().getNumberSignKeyword_1());
                
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2894:1: ( (lv_bilname2_2_0= rulebased_numeral ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2895:1: (lv_bilname2_2_0= rulebased_numeral )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2895:1: (lv_bilname2_2_0= rulebased_numeral )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2896:3: lv_bilname2_2_0= rulebased_numeral
            {
             
            	        newCompositeNode(grammarAccess.getBased_integer_literalAccess().getBilname2Based_numeralParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_rulebased_numeral_in_rulebased_integer_literal5649);
            lv_bilname2_2_0=rulebased_numeral();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBased_integer_literalRule());
            	        }
                   		set(
                   			current, 
                   			"bilname2",
                    		lv_bilname2_2_0, 
                    		"based_numeral");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,51,FOLLOW_51_in_rulebased_integer_literal5661); 

                	newLeafNode(otherlv_3, grammarAccess.getBased_integer_literalAccess().getNumberSignKeyword_3());
                
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2916:1: ( (lv_bilname3_4_0= rulepositive_exponent ) )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==48) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2917:1: (lv_bilname3_4_0= rulepositive_exponent )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2917:1: (lv_bilname3_4_0= rulepositive_exponent )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2918:3: lv_bilname3_4_0= rulepositive_exponent
                    {
                     
                    	        newCompositeNode(grammarAccess.getBased_integer_literalAccess().getBilname3Positive_exponentParserRuleCall_4_0()); 
                    	    
                    pushFollow(FOLLOW_rulepositive_exponent_in_rulebased_integer_literal5682);
                    lv_bilname3_4_0=rulepositive_exponent();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBased_integer_literalRule());
                    	        }
                           		set(
                           			current, 
                           			"bilname3",
                            		lv_bilname3_4_0, 
                            		"positive_exponent");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebased_integer_literal"


    // $ANTLR start "entryRulebase"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2942:1: entryRulebase returns [EObject current=null] : iv_rulebase= rulebase EOF ;
    public final EObject entryRulebase() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebase = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2943:2: (iv_rulebase= rulebase EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2944:2: iv_rulebase= rulebase EOF
            {
             newCompositeNode(grammarAccess.getBaseRule()); 
            pushFollow(FOLLOW_rulebase_in_entryRulebase5719);
            iv_rulebase=rulebase();

            state._fsp--;

             current =iv_rulebase; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebase5729); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebase"


    // $ANTLR start "rulebase"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2951:1: rulebase returns [EObject current=null] : ( ( (lv_bname1_0_0= RULE_DIGIT ) ) ( (lv_bname2_1_0= RULE_DIGIT ) )? ) ;
    public final EObject rulebase() throws RecognitionException {
        EObject current = null;

        Token lv_bname1_0_0=null;
        Token lv_bname2_1_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2954:28: ( ( ( (lv_bname1_0_0= RULE_DIGIT ) ) ( (lv_bname2_1_0= RULE_DIGIT ) )? ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2955:1: ( ( (lv_bname1_0_0= RULE_DIGIT ) ) ( (lv_bname2_1_0= RULE_DIGIT ) )? )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2955:1: ( ( (lv_bname1_0_0= RULE_DIGIT ) ) ( (lv_bname2_1_0= RULE_DIGIT ) )? )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2955:2: ( (lv_bname1_0_0= RULE_DIGIT ) ) ( (lv_bname2_1_0= RULE_DIGIT ) )?
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2955:2: ( (lv_bname1_0_0= RULE_DIGIT ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2956:1: (lv_bname1_0_0= RULE_DIGIT )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2956:1: (lv_bname1_0_0= RULE_DIGIT )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2957:3: lv_bname1_0_0= RULE_DIGIT
            {
            lv_bname1_0_0=(Token)match(input,RULE_DIGIT,FOLLOW_RULE_DIGIT_in_rulebase5771); 

            			newLeafNode(lv_bname1_0_0, grammarAccess.getBaseAccess().getBname1DigitTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getBaseRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"bname1",
                    		lv_bname1_0_0, 
                    		"digit");
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2973:2: ( (lv_bname2_1_0= RULE_DIGIT ) )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==RULE_DIGIT) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2974:1: (lv_bname2_1_0= RULE_DIGIT )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2974:1: (lv_bname2_1_0= RULE_DIGIT )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2975:3: lv_bname2_1_0= RULE_DIGIT
                    {
                    lv_bname2_1_0=(Token)match(input,RULE_DIGIT,FOLLOW_RULE_DIGIT_in_rulebase5793); 

                    			newLeafNode(lv_bname2_1_0, grammarAccess.getBaseAccess().getBname2DigitTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBaseRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"bname2",
                            		lv_bname2_1_0, 
                            		"digit");
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebase"


    // $ANTLR start "entryRulebased_numeral"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:2999:1: entryRulebased_numeral returns [EObject current=null] : iv_rulebased_numeral= rulebased_numeral EOF ;
    public final EObject entryRulebased_numeral() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebased_numeral = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3000:2: (iv_rulebased_numeral= rulebased_numeral EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3001:2: iv_rulebased_numeral= rulebased_numeral EOF
            {
             newCompositeNode(grammarAccess.getBased_numeralRule()); 
            pushFollow(FOLLOW_rulebased_numeral_in_entryRulebased_numeral5835);
            iv_rulebased_numeral=rulebased_numeral();

            state._fsp--;

             current =iv_rulebased_numeral; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebased_numeral5845); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebased_numeral"


    // $ANTLR start "rulebased_numeral"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3008:1: rulebased_numeral returns [EObject current=null] : ( ( (lv_bnname1_0_0= RULE_EXTENDED_DIGIT ) ) ( (otherlv_1= '_' )? ( (lv_bnname2_2_0= RULE_EXTENDED_DIGIT ) ) )? ) ;
    public final EObject rulebased_numeral() throws RecognitionException {
        EObject current = null;

        Token lv_bnname1_0_0=null;
        Token otherlv_1=null;
        Token lv_bnname2_2_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3011:28: ( ( ( (lv_bnname1_0_0= RULE_EXTENDED_DIGIT ) ) ( (otherlv_1= '_' )? ( (lv_bnname2_2_0= RULE_EXTENDED_DIGIT ) ) )? ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3012:1: ( ( (lv_bnname1_0_0= RULE_EXTENDED_DIGIT ) ) ( (otherlv_1= '_' )? ( (lv_bnname2_2_0= RULE_EXTENDED_DIGIT ) ) )? )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3012:1: ( ( (lv_bnname1_0_0= RULE_EXTENDED_DIGIT ) ) ( (otherlv_1= '_' )? ( (lv_bnname2_2_0= RULE_EXTENDED_DIGIT ) ) )? )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3012:2: ( (lv_bnname1_0_0= RULE_EXTENDED_DIGIT ) ) ( (otherlv_1= '_' )? ( (lv_bnname2_2_0= RULE_EXTENDED_DIGIT ) ) )?
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3012:2: ( (lv_bnname1_0_0= RULE_EXTENDED_DIGIT ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3013:1: (lv_bnname1_0_0= RULE_EXTENDED_DIGIT )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3013:1: (lv_bnname1_0_0= RULE_EXTENDED_DIGIT )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3014:3: lv_bnname1_0_0= RULE_EXTENDED_DIGIT
            {
            lv_bnname1_0_0=(Token)match(input,RULE_EXTENDED_DIGIT,FOLLOW_RULE_EXTENDED_DIGIT_in_rulebased_numeral5887); 

            			newLeafNode(lv_bnname1_0_0, grammarAccess.getBased_numeralAccess().getBnname1Extended_digitTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getBased_numeralRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"bnname1",
                    		lv_bnname1_0_0, 
                    		"extended_digit");
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3030:2: ( (otherlv_1= '_' )? ( (lv_bnname2_2_0= RULE_EXTENDED_DIGIT ) ) )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==RULE_EXTENDED_DIGIT||LA29_0==47) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3030:3: (otherlv_1= '_' )? ( (lv_bnname2_2_0= RULE_EXTENDED_DIGIT ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3030:3: (otherlv_1= '_' )?
                    int alt28=2;
                    int LA28_0 = input.LA(1);

                    if ( (LA28_0==47) ) {
                        alt28=1;
                    }
                    switch (alt28) {
                        case 1 :
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3030:5: otherlv_1= '_'
                            {
                            otherlv_1=(Token)match(input,47,FOLLOW_47_in_rulebased_numeral5906); 

                                	newLeafNode(otherlv_1, grammarAccess.getBased_numeralAccess().get_Keyword_1_0());
                                

                            }
                            break;

                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3034:3: ( (lv_bnname2_2_0= RULE_EXTENDED_DIGIT ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3035:1: (lv_bnname2_2_0= RULE_EXTENDED_DIGIT )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3035:1: (lv_bnname2_2_0= RULE_EXTENDED_DIGIT )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3036:3: lv_bnname2_2_0= RULE_EXTENDED_DIGIT
                    {
                    lv_bnname2_2_0=(Token)match(input,RULE_EXTENDED_DIGIT,FOLLOW_RULE_EXTENDED_DIGIT_in_rulebased_numeral5925); 

                    			newLeafNode(lv_bnname2_2_0, grammarAccess.getBased_numeralAccess().getBnname2Extended_digitTerminalRuleCall_1_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBased_numeralRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"bnname2",
                            		lv_bnname2_2_0, 
                            		"extended_digit");
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebased_numeral"


    // $ANTLR start "entryRulebehavior_time"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3060:1: entryRulebehavior_time returns [EObject current=null] : iv_rulebehavior_time= rulebehavior_time EOF ;
    public final EObject entryRulebehavior_time() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebehavior_time = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3061:2: (iv_rulebehavior_time= rulebehavior_time EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3062:2: iv_rulebehavior_time= rulebehavior_time EOF
            {
             newCompositeNode(grammarAccess.getBehavior_timeRule()); 
            pushFollow(FOLLOW_rulebehavior_time_in_entryRulebehavior_time5968);
            iv_rulebehavior_time=rulebehavior_time();

            state._fsp--;

             current =iv_rulebehavior_time; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebehavior_time5978); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebehavior_time"


    // $ANTLR start "rulebehavior_time"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3069:1: rulebehavior_time returns [EObject current=null] : ( rulebaWS ( (lv_btname1_1_0= ruleinteger_value ) ) rulebaWS ( (lv_btname2_3_0= ruleunit_identifier ) ) rulebaWS ) ;
    public final EObject rulebehavior_time() throws RecognitionException {
        EObject current = null;

        EObject lv_btname1_1_0 = null;

        EObject lv_btname2_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3072:28: ( ( rulebaWS ( (lv_btname1_1_0= ruleinteger_value ) ) rulebaWS ( (lv_btname2_3_0= ruleunit_identifier ) ) rulebaWS ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3073:1: ( rulebaWS ( (lv_btname1_1_0= ruleinteger_value ) ) rulebaWS ( (lv_btname2_3_0= ruleunit_identifier ) ) rulebaWS )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3073:1: ( rulebaWS ( (lv_btname1_1_0= ruleinteger_value ) ) rulebaWS ( (lv_btname2_3_0= ruleunit_identifier ) ) rulebaWS )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3074:5: rulebaWS ( (lv_btname1_1_0= ruleinteger_value ) ) rulebaWS ( (lv_btname2_3_0= ruleunit_identifier ) ) rulebaWS
            {
             
                    newCompositeNode(grammarAccess.getBehavior_timeAccess().getBaWSParserRuleCall_0()); 
                
            pushFollow(FOLLOW_rulebaWS_in_rulebehavior_time6019);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3081:1: ( (lv_btname1_1_0= ruleinteger_value ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3082:1: (lv_btname1_1_0= ruleinteger_value )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3082:1: (lv_btname1_1_0= ruleinteger_value )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3083:3: lv_btname1_1_0= ruleinteger_value
            {
             
            	        newCompositeNode(grammarAccess.getBehavior_timeAccess().getBtname1Integer_valueParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleinteger_value_in_rulebehavior_time6039);
            lv_btname1_1_0=ruleinteger_value();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBehavior_timeRule());
            	        }
                   		set(
                   			current, 
                   			"btname1",
                    		lv_btname1_1_0, 
                    		"integer_value");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

             
                    newCompositeNode(grammarAccess.getBehavior_timeAccess().getBaWSParserRuleCall_2()); 
                
            pushFollow(FOLLOW_rulebaWS_in_rulebehavior_time6055);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3107:1: ( (lv_btname2_3_0= ruleunit_identifier ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3108:1: (lv_btname2_3_0= ruleunit_identifier )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3108:1: (lv_btname2_3_0= ruleunit_identifier )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3109:3: lv_btname2_3_0= ruleunit_identifier
            {
             
            	        newCompositeNode(grammarAccess.getBehavior_timeAccess().getBtname2Unit_identifierParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleunit_identifier_in_rulebehavior_time6075);
            lv_btname2_3_0=ruleunit_identifier();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBehavior_timeRule());
            	        }
                   		set(
                   			current, 
                   			"btname2",
                    		lv_btname2_3_0, 
                    		"unit_identifier");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

             
                    newCompositeNode(grammarAccess.getBehavior_timeAccess().getBaWSParserRuleCall_4()); 
                
            pushFollow(FOLLOW_rulebaWS_in_rulebehavior_time6091);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebehavior_time"


    // $ANTLR start "entryRuleunit_identifier"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3141:1: entryRuleunit_identifier returns [EObject current=null] : iv_ruleunit_identifier= ruleunit_identifier EOF ;
    public final EObject entryRuleunit_identifier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleunit_identifier = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3142:2: (iv_ruleunit_identifier= ruleunit_identifier EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3143:2: iv_ruleunit_identifier= ruleunit_identifier EOF
            {
             newCompositeNode(grammarAccess.getUnit_identifierRule()); 
            pushFollow(FOLLOW_ruleunit_identifier_in_entryRuleunit_identifier6126);
            iv_ruleunit_identifier=ruleunit_identifier();

            state._fsp--;

             current =iv_ruleunit_identifier; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleunit_identifier6136); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleunit_identifier"


    // $ANTLR start "ruleunit_identifier"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3150:1: ruleunit_identifier returns [EObject current=null] : ( ( (lv_uiname1_0_0= 'ps' ) ) | ( (lv_uiname2_1_0= 'ns' ) ) | ( (lv_uiname3_2_0= 'us' ) ) | ( (lv_uiname4_3_0= 'ms' ) ) | ( (lv_uiname5_4_0= 'sec' ) ) | ( (lv_uiname6_5_0= 'min' ) ) | ( (lv_uiname7_6_0= 'hr' ) ) ) ;
    public final EObject ruleunit_identifier() throws RecognitionException {
        EObject current = null;

        Token lv_uiname1_0_0=null;
        Token lv_uiname2_1_0=null;
        Token lv_uiname3_2_0=null;
        Token lv_uiname4_3_0=null;
        Token lv_uiname5_4_0=null;
        Token lv_uiname6_5_0=null;
        Token lv_uiname7_6_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3153:28: ( ( ( (lv_uiname1_0_0= 'ps' ) ) | ( (lv_uiname2_1_0= 'ns' ) ) | ( (lv_uiname3_2_0= 'us' ) ) | ( (lv_uiname4_3_0= 'ms' ) ) | ( (lv_uiname5_4_0= 'sec' ) ) | ( (lv_uiname6_5_0= 'min' ) ) | ( (lv_uiname7_6_0= 'hr' ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3154:1: ( ( (lv_uiname1_0_0= 'ps' ) ) | ( (lv_uiname2_1_0= 'ns' ) ) | ( (lv_uiname3_2_0= 'us' ) ) | ( (lv_uiname4_3_0= 'ms' ) ) | ( (lv_uiname5_4_0= 'sec' ) ) | ( (lv_uiname6_5_0= 'min' ) ) | ( (lv_uiname7_6_0= 'hr' ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3154:1: ( ( (lv_uiname1_0_0= 'ps' ) ) | ( (lv_uiname2_1_0= 'ns' ) ) | ( (lv_uiname3_2_0= 'us' ) ) | ( (lv_uiname4_3_0= 'ms' ) ) | ( (lv_uiname5_4_0= 'sec' ) ) | ( (lv_uiname6_5_0= 'min' ) ) | ( (lv_uiname7_6_0= 'hr' ) ) )
            int alt30=7;
            switch ( input.LA(1) ) {
            case 52:
                {
                alt30=1;
                }
                break;
            case 53:
                {
                alt30=2;
                }
                break;
            case 54:
                {
                alt30=3;
                }
                break;
            case 55:
                {
                alt30=4;
                }
                break;
            case 56:
                {
                alt30=5;
                }
                break;
            case 57:
                {
                alt30=6;
                }
                break;
            case 58:
                {
                alt30=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }

            switch (alt30) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3154:2: ( (lv_uiname1_0_0= 'ps' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3154:2: ( (lv_uiname1_0_0= 'ps' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3155:1: (lv_uiname1_0_0= 'ps' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3155:1: (lv_uiname1_0_0= 'ps' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3156:3: lv_uiname1_0_0= 'ps'
                    {
                    lv_uiname1_0_0=(Token)match(input,52,FOLLOW_52_in_ruleunit_identifier6179); 

                            newLeafNode(lv_uiname1_0_0, grammarAccess.getUnit_identifierAccess().getUiname1PsKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnit_identifierRule());
                    	        }
                           		setWithLastConsumed(current, "uiname1", lv_uiname1_0_0, "ps");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3170:6: ( (lv_uiname2_1_0= 'ns' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3170:6: ( (lv_uiname2_1_0= 'ns' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3171:1: (lv_uiname2_1_0= 'ns' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3171:1: (lv_uiname2_1_0= 'ns' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3172:3: lv_uiname2_1_0= 'ns'
                    {
                    lv_uiname2_1_0=(Token)match(input,53,FOLLOW_53_in_ruleunit_identifier6216); 

                            newLeafNode(lv_uiname2_1_0, grammarAccess.getUnit_identifierAccess().getUiname2NsKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnit_identifierRule());
                    	        }
                           		setWithLastConsumed(current, "uiname2", lv_uiname2_1_0, "ns");
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3186:6: ( (lv_uiname3_2_0= 'us' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3186:6: ( (lv_uiname3_2_0= 'us' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3187:1: (lv_uiname3_2_0= 'us' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3187:1: (lv_uiname3_2_0= 'us' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3188:3: lv_uiname3_2_0= 'us'
                    {
                    lv_uiname3_2_0=(Token)match(input,54,FOLLOW_54_in_ruleunit_identifier6253); 

                            newLeafNode(lv_uiname3_2_0, grammarAccess.getUnit_identifierAccess().getUiname3UsKeyword_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnit_identifierRule());
                    	        }
                           		setWithLastConsumed(current, "uiname3", lv_uiname3_2_0, "us");
                    	    

                    }


                    }


                    }
                    break;
                case 4 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3202:6: ( (lv_uiname4_3_0= 'ms' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3202:6: ( (lv_uiname4_3_0= 'ms' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3203:1: (lv_uiname4_3_0= 'ms' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3203:1: (lv_uiname4_3_0= 'ms' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3204:3: lv_uiname4_3_0= 'ms'
                    {
                    lv_uiname4_3_0=(Token)match(input,55,FOLLOW_55_in_ruleunit_identifier6290); 

                            newLeafNode(lv_uiname4_3_0, grammarAccess.getUnit_identifierAccess().getUiname4MsKeyword_3_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnit_identifierRule());
                    	        }
                           		setWithLastConsumed(current, "uiname4", lv_uiname4_3_0, "ms");
                    	    

                    }


                    }


                    }
                    break;
                case 5 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3218:6: ( (lv_uiname5_4_0= 'sec' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3218:6: ( (lv_uiname5_4_0= 'sec' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3219:1: (lv_uiname5_4_0= 'sec' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3219:1: (lv_uiname5_4_0= 'sec' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3220:3: lv_uiname5_4_0= 'sec'
                    {
                    lv_uiname5_4_0=(Token)match(input,56,FOLLOW_56_in_ruleunit_identifier6327); 

                            newLeafNode(lv_uiname5_4_0, grammarAccess.getUnit_identifierAccess().getUiname5SecKeyword_4_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnit_identifierRule());
                    	        }
                           		setWithLastConsumed(current, "uiname5", lv_uiname5_4_0, "sec");
                    	    

                    }


                    }


                    }
                    break;
                case 6 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3234:6: ( (lv_uiname6_5_0= 'min' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3234:6: ( (lv_uiname6_5_0= 'min' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3235:1: (lv_uiname6_5_0= 'min' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3235:1: (lv_uiname6_5_0= 'min' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3236:3: lv_uiname6_5_0= 'min'
                    {
                    lv_uiname6_5_0=(Token)match(input,57,FOLLOW_57_in_ruleunit_identifier6364); 

                            newLeafNode(lv_uiname6_5_0, grammarAccess.getUnit_identifierAccess().getUiname6MinKeyword_5_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnit_identifierRule());
                    	        }
                           		setWithLastConsumed(current, "uiname6", lv_uiname6_5_0, "min");
                    	    

                    }


                    }


                    }
                    break;
                case 7 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3250:6: ( (lv_uiname7_6_0= 'hr' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3250:6: ( (lv_uiname7_6_0= 'hr' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3251:1: (lv_uiname7_6_0= 'hr' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3251:1: (lv_uiname7_6_0= 'hr' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3252:3: lv_uiname7_6_0= 'hr'
                    {
                    lv_uiname7_6_0=(Token)match(input,58,FOLLOW_58_in_ruleunit_identifier6401); 

                            newLeafNode(lv_uiname7_6_0, grammarAccess.getUnit_identifierAccess().getUiname7HrKeyword_6_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnit_identifierRule());
                    	        }
                           		setWithLastConsumed(current, "uiname7", lv_uiname7_6_0, "hr");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleunit_identifier"


    // $ANTLR start "entryRuleinteger_range"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3273:1: entryRuleinteger_range returns [EObject current=null] : iv_ruleinteger_range= ruleinteger_range EOF ;
    public final EObject entryRuleinteger_range() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleinteger_range = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3274:2: (iv_ruleinteger_range= ruleinteger_range EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3275:2: iv_ruleinteger_range= ruleinteger_range EOF
            {
             newCompositeNode(grammarAccess.getInteger_rangeRule()); 
            pushFollow(FOLLOW_ruleinteger_range_in_entryRuleinteger_range6450);
            iv_ruleinteger_range=ruleinteger_range();

            state._fsp--;

             current =iv_ruleinteger_range; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleinteger_range6460); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleinteger_range"


    // $ANTLR start "ruleinteger_range"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3282:1: ruleinteger_range returns [EObject current=null] : ( rulebaWS ( (lv_min_1_0= ruleinteger_value ) ) rulebaWS ( (lv_irsep_3_0= '..' ) ) rulebaWS ( (lv_max_5_0= ruleinteger_value ) ) rulebaWS ) ;
    public final EObject ruleinteger_range() throws RecognitionException {
        EObject current = null;

        Token lv_irsep_3_0=null;
        EObject lv_min_1_0 = null;

        EObject lv_max_5_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3285:28: ( ( rulebaWS ( (lv_min_1_0= ruleinteger_value ) ) rulebaWS ( (lv_irsep_3_0= '..' ) ) rulebaWS ( (lv_max_5_0= ruleinteger_value ) ) rulebaWS ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3286:1: ( rulebaWS ( (lv_min_1_0= ruleinteger_value ) ) rulebaWS ( (lv_irsep_3_0= '..' ) ) rulebaWS ( (lv_max_5_0= ruleinteger_value ) ) rulebaWS )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3286:1: ( rulebaWS ( (lv_min_1_0= ruleinteger_value ) ) rulebaWS ( (lv_irsep_3_0= '..' ) ) rulebaWS ( (lv_max_5_0= ruleinteger_value ) ) rulebaWS )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3287:5: rulebaWS ( (lv_min_1_0= ruleinteger_value ) ) rulebaWS ( (lv_irsep_3_0= '..' ) ) rulebaWS ( (lv_max_5_0= ruleinteger_value ) ) rulebaWS
            {
             
                    newCompositeNode(grammarAccess.getInteger_rangeAccess().getBaWSParserRuleCall_0()); 
                
            pushFollow(FOLLOW_rulebaWS_in_ruleinteger_range6501);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3294:1: ( (lv_min_1_0= ruleinteger_value ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3295:1: (lv_min_1_0= ruleinteger_value )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3295:1: (lv_min_1_0= ruleinteger_value )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3296:3: lv_min_1_0= ruleinteger_value
            {
             
            	        newCompositeNode(grammarAccess.getInteger_rangeAccess().getMinInteger_valueParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleinteger_value_in_ruleinteger_range6521);
            lv_min_1_0=ruleinteger_value();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getInteger_rangeRule());
            	        }
                   		set(
                   			current, 
                   			"min",
                    		lv_min_1_0, 
                    		"integer_value");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

             
                    newCompositeNode(grammarAccess.getInteger_rangeAccess().getBaWSParserRuleCall_2()); 
                
            pushFollow(FOLLOW_rulebaWS_in_ruleinteger_range6537);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3320:1: ( (lv_irsep_3_0= '..' ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3321:1: (lv_irsep_3_0= '..' )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3321:1: (lv_irsep_3_0= '..' )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3322:3: lv_irsep_3_0= '..'
            {
            lv_irsep_3_0=(Token)match(input,43,FOLLOW_43_in_ruleinteger_range6554); 

                    newLeafNode(lv_irsep_3_0, grammarAccess.getInteger_rangeAccess().getIrsepFullStopFullStopKeyword_3_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getInteger_rangeRule());
            	        }
                   		setWithLastConsumed(current, "irsep", lv_irsep_3_0, "..");
            	    

            }


            }

             
                    newCompositeNode(grammarAccess.getInteger_rangeAccess().getBaWSParserRuleCall_4()); 
                
            pushFollow(FOLLOW_rulebaWS_in_ruleinteger_range6583);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3343:1: ( (lv_max_5_0= ruleinteger_value ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3344:1: (lv_max_5_0= ruleinteger_value )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3344:1: (lv_max_5_0= ruleinteger_value )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3345:3: lv_max_5_0= ruleinteger_value
            {
             
            	        newCompositeNode(grammarAccess.getInteger_rangeAccess().getMaxInteger_valueParserRuleCall_5_0()); 
            	    
            pushFollow(FOLLOW_ruleinteger_value_in_ruleinteger_range6603);
            lv_max_5_0=ruleinteger_value();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getInteger_rangeRule());
            	        }
                   		set(
                   			current, 
                   			"max",
                    		lv_max_5_0, 
                    		"integer_value");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

             
                    newCompositeNode(grammarAccess.getInteger_rangeAccess().getBaWSParserRuleCall_6()); 
                
            pushFollow(FOLLOW_rulebaWS_in_ruleinteger_range6619);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleinteger_range"


    // $ANTLR start "entryRuleinteger"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3377:1: entryRuleinteger returns [EObject current=null] : iv_ruleinteger= ruleinteger EOF ;
    public final EObject entryRuleinteger() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleinteger = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3378:2: (iv_ruleinteger= ruleinteger EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3379:2: iv_ruleinteger= ruleinteger EOF
            {
             newCompositeNode(grammarAccess.getIntegerRule()); 
            pushFollow(FOLLOW_ruleinteger_in_entryRuleinteger6654);
            iv_ruleinteger=ruleinteger();

            state._fsp--;

             current =iv_ruleinteger; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleinteger6664); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleinteger"


    // $ANTLR start "ruleinteger"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3386:1: ruleinteger returns [EObject current=null] : ( ( (lv_value1_0_0= RULE_INT ) ) | ( (lv_value2_1_0= RULE_DIGIT ) ) ) ;
    public final EObject ruleinteger() throws RecognitionException {
        EObject current = null;

        Token lv_value1_0_0=null;
        Token lv_value2_1_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3389:28: ( ( ( (lv_value1_0_0= RULE_INT ) ) | ( (lv_value2_1_0= RULE_DIGIT ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3390:1: ( ( (lv_value1_0_0= RULE_INT ) ) | ( (lv_value2_1_0= RULE_DIGIT ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3390:1: ( ( (lv_value1_0_0= RULE_INT ) ) | ( (lv_value2_1_0= RULE_DIGIT ) ) )
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==RULE_INT) ) {
                alt31=1;
            }
            else if ( (LA31_0==RULE_DIGIT) ) {
                alt31=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }
            switch (alt31) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3390:2: ( (lv_value1_0_0= RULE_INT ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3390:2: ( (lv_value1_0_0= RULE_INT ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3391:1: (lv_value1_0_0= RULE_INT )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3391:1: (lv_value1_0_0= RULE_INT )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3392:3: lv_value1_0_0= RULE_INT
                    {
                    lv_value1_0_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleinteger6706); 

                    			newLeafNode(lv_value1_0_0, grammarAccess.getIntegerAccess().getValue1INTTerminalRuleCall_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getIntegerRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value1",
                            		lv_value1_0_0, 
                            		"INT");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3409:6: ( (lv_value2_1_0= RULE_DIGIT ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3409:6: ( (lv_value2_1_0= RULE_DIGIT ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3410:1: (lv_value2_1_0= RULE_DIGIT )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3410:1: (lv_value2_1_0= RULE_DIGIT )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3411:3: lv_value2_1_0= RULE_DIGIT
                    {
                    lv_value2_1_0=(Token)match(input,RULE_DIGIT,FOLLOW_RULE_DIGIT_in_ruleinteger6734); 

                    			newLeafNode(lv_value2_1_0, grammarAccess.getIntegerAccess().getValue2DigitTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getIntegerRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value2",
                            		lv_value2_1_0, 
                            		"digit");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleinteger"


    // $ANTLR start "entryRulevalue_expression"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3435:1: entryRulevalue_expression returns [EObject current=null] : iv_rulevalue_expression= rulevalue_expression EOF ;
    public final EObject entryRulevalue_expression() throws RecognitionException {
        EObject current = null;

        EObject iv_rulevalue_expression = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3436:2: (iv_rulevalue_expression= rulevalue_expression EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3437:2: iv_rulevalue_expression= rulevalue_expression EOF
            {
             newCompositeNode(grammarAccess.getValue_expressionRule()); 
            pushFollow(FOLLOW_rulevalue_expression_in_entryRulevalue_expression6775);
            iv_rulevalue_expression=rulevalue_expression();

            state._fsp--;

             current =iv_rulevalue_expression; 
            match(input,EOF,FOLLOW_EOF_in_entryRulevalue_expression6785); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulevalue_expression"


    // $ANTLR start "rulevalue_expression"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3444:1: rulevalue_expression returns [EObject current=null] : ( ( (lv_vename1_0_0= rulerelation ) ) ( ( (lv_vename3_1_0= rulelogical_operator ) ) ( (lv_vename2_2_0= rulerelation ) ) )* ) ;
    public final EObject rulevalue_expression() throws RecognitionException {
        EObject current = null;

        EObject lv_vename1_0_0 = null;

        EObject lv_vename3_1_0 = null;

        EObject lv_vename2_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3447:28: ( ( ( (lv_vename1_0_0= rulerelation ) ) ( ( (lv_vename3_1_0= rulelogical_operator ) ) ( (lv_vename2_2_0= rulerelation ) ) )* ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3448:1: ( ( (lv_vename1_0_0= rulerelation ) ) ( ( (lv_vename3_1_0= rulelogical_operator ) ) ( (lv_vename2_2_0= rulerelation ) ) )* )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3448:1: ( ( (lv_vename1_0_0= rulerelation ) ) ( ( (lv_vename3_1_0= rulelogical_operator ) ) ( (lv_vename2_2_0= rulerelation ) ) )* )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3448:2: ( (lv_vename1_0_0= rulerelation ) ) ( ( (lv_vename3_1_0= rulelogical_operator ) ) ( (lv_vename2_2_0= rulerelation ) ) )*
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3448:2: ( (lv_vename1_0_0= rulerelation ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3449:1: (lv_vename1_0_0= rulerelation )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3449:1: (lv_vename1_0_0= rulerelation )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3450:3: lv_vename1_0_0= rulerelation
            {
             
            	        newCompositeNode(grammarAccess.getValue_expressionAccess().getVename1RelationParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_rulerelation_in_rulevalue_expression6831);
            lv_vename1_0_0=rulerelation();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getValue_expressionRule());
            	        }
                   		set(
                   			current, 
                   			"vename1",
                    		lv_vename1_0_0, 
                    		"relation");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3466:2: ( ( (lv_vename3_1_0= rulelogical_operator ) ) ( (lv_vename2_2_0= rulerelation ) ) )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( ((LA32_0>=62 && LA32_0<=64)) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3466:3: ( (lv_vename3_1_0= rulelogical_operator ) ) ( (lv_vename2_2_0= rulerelation ) )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3466:3: ( (lv_vename3_1_0= rulelogical_operator ) )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3467:1: (lv_vename3_1_0= rulelogical_operator )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3467:1: (lv_vename3_1_0= rulelogical_operator )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3468:3: lv_vename3_1_0= rulelogical_operator
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getValue_expressionAccess().getVename3Logical_operatorParserRuleCall_1_0_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulelogical_operator_in_rulevalue_expression6853);
            	    lv_vename3_1_0=rulelogical_operator();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getValue_expressionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"vename3",
            	            		lv_vename3_1_0, 
            	            		"logical_operator");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }

            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3484:2: ( (lv_vename2_2_0= rulerelation ) )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3485:1: (lv_vename2_2_0= rulerelation )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3485:1: (lv_vename2_2_0= rulerelation )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3486:3: lv_vename2_2_0= rulerelation
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getValue_expressionAccess().getVename2RelationParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulerelation_in_rulevalue_expression6874);
            	    lv_vename2_2_0=rulerelation();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getValue_expressionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"vename2",
            	            		lv_vename2_2_0, 
            	            		"relation");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulevalue_expression"


    // $ANTLR start "entryRulerelation"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3510:1: entryRulerelation returns [EObject current=null] : iv_rulerelation= rulerelation EOF ;
    public final EObject entryRulerelation() throws RecognitionException {
        EObject current = null;

        EObject iv_rulerelation = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3511:2: (iv_rulerelation= rulerelation EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3512:2: iv_rulerelation= rulerelation EOF
            {
             newCompositeNode(grammarAccess.getRelationRule()); 
            pushFollow(FOLLOW_rulerelation_in_entryRulerelation6912);
            iv_rulerelation=rulerelation();

            state._fsp--;

             current =iv_rulerelation; 
            match(input,EOF,FOLLOW_EOF_in_entryRulerelation6922); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulerelation"


    // $ANTLR start "rulerelation"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3519:1: rulerelation returns [EObject current=null] : ( ( (lv_rname1_0_0= rulesimple_expression ) ) ( ( (lv_rname3_1_0= rulerelational_operator ) ) ( (lv_rname2_2_0= rulesimple_expression ) ) )? ) ;
    public final EObject rulerelation() throws RecognitionException {
        EObject current = null;

        EObject lv_rname1_0_0 = null;

        EObject lv_rname3_1_0 = null;

        EObject lv_rname2_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3522:28: ( ( ( (lv_rname1_0_0= rulesimple_expression ) ) ( ( (lv_rname3_1_0= rulerelational_operator ) ) ( (lv_rname2_2_0= rulesimple_expression ) ) )? ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3523:1: ( ( (lv_rname1_0_0= rulesimple_expression ) ) ( ( (lv_rname3_1_0= rulerelational_operator ) ) ( (lv_rname2_2_0= rulesimple_expression ) ) )? )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3523:1: ( ( (lv_rname1_0_0= rulesimple_expression ) ) ( ( (lv_rname3_1_0= rulerelational_operator ) ) ( (lv_rname2_2_0= rulesimple_expression ) ) )? )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3523:2: ( (lv_rname1_0_0= rulesimple_expression ) ) ( ( (lv_rname3_1_0= rulerelational_operator ) ) ( (lv_rname2_2_0= rulesimple_expression ) ) )?
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3523:2: ( (lv_rname1_0_0= rulesimple_expression ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3524:1: (lv_rname1_0_0= rulesimple_expression )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3524:1: (lv_rname1_0_0= rulesimple_expression )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3525:3: lv_rname1_0_0= rulesimple_expression
            {
             
            	        newCompositeNode(grammarAccess.getRelationAccess().getRname1Simple_expressionParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_rulesimple_expression_in_rulerelation6968);
            lv_rname1_0_0=rulesimple_expression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRelationRule());
            	        }
                   		set(
                   			current, 
                   			"rname1",
                    		lv_rname1_0_0, 
                    		"simple_expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3541:2: ( ( (lv_rname3_1_0= rulerelational_operator ) ) ( (lv_rname2_2_0= rulesimple_expression ) ) )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( ((LA33_0>=65 && LA33_0<=70)) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3541:3: ( (lv_rname3_1_0= rulerelational_operator ) ) ( (lv_rname2_2_0= rulesimple_expression ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3541:3: ( (lv_rname3_1_0= rulerelational_operator ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3542:1: (lv_rname3_1_0= rulerelational_operator )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3542:1: (lv_rname3_1_0= rulerelational_operator )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3543:3: lv_rname3_1_0= rulerelational_operator
                    {
                     
                    	        newCompositeNode(grammarAccess.getRelationAccess().getRname3Relational_operatorParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FOLLOW_rulerelational_operator_in_rulerelation6990);
                    lv_rname3_1_0=rulerelational_operator();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getRelationRule());
                    	        }
                           		add(
                           			current, 
                           			"rname3",
                            		lv_rname3_1_0, 
                            		"relational_operator");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3559:2: ( (lv_rname2_2_0= rulesimple_expression ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3560:1: (lv_rname2_2_0= rulesimple_expression )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3560:1: (lv_rname2_2_0= rulesimple_expression )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3561:3: lv_rname2_2_0= rulesimple_expression
                    {
                     
                    	        newCompositeNode(grammarAccess.getRelationAccess().getRname2Simple_expressionParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulesimple_expression_in_rulerelation7011);
                    lv_rname2_2_0=rulesimple_expression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getRelationRule());
                    	        }
                           		add(
                           			current, 
                           			"rname2",
                            		lv_rname2_2_0, 
                            		"simple_expression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulerelation"


    // $ANTLR start "entryRulesimple_expression"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3585:1: entryRulesimple_expression returns [EObject current=null] : iv_rulesimple_expression= rulesimple_expression EOF ;
    public final EObject entryRulesimple_expression() throws RecognitionException {
        EObject current = null;

        EObject iv_rulesimple_expression = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3586:2: (iv_rulesimple_expression= rulesimple_expression EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3587:2: iv_rulesimple_expression= rulesimple_expression EOF
            {
             newCompositeNode(grammarAccess.getSimple_expressionRule()); 
            pushFollow(FOLLOW_rulesimple_expression_in_entryRulesimple_expression7049);
            iv_rulesimple_expression=rulesimple_expression();

            state._fsp--;

             current =iv_rulesimple_expression; 
            match(input,EOF,FOLLOW_EOF_in_entryRulesimple_expression7059); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulesimple_expression"


    // $ANTLR start "rulesimple_expression"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3594:1: rulesimple_expression returns [EObject current=null] : ( ( rulebaWS ( (lv_sename2_1_0= ruleunary_adding_operator ) ) )? ( (lv_sename1_2_0= ruleterm ) ) ( ( (lv_sename3_3_0= rulebinary_adding_operator ) ) ( (lv_sename4_4_0= ruleterm ) ) )* ) ;
    public final EObject rulesimple_expression() throws RecognitionException {
        EObject current = null;

        EObject lv_sename2_1_0 = null;

        EObject lv_sename1_2_0 = null;

        EObject lv_sename3_3_0 = null;

        EObject lv_sename4_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3597:28: ( ( ( rulebaWS ( (lv_sename2_1_0= ruleunary_adding_operator ) ) )? ( (lv_sename1_2_0= ruleterm ) ) ( ( (lv_sename3_3_0= rulebinary_adding_operator ) ) ( (lv_sename4_4_0= ruleterm ) ) )* ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3598:1: ( ( rulebaWS ( (lv_sename2_1_0= ruleunary_adding_operator ) ) )? ( (lv_sename1_2_0= ruleterm ) ) ( ( (lv_sename3_3_0= rulebinary_adding_operator ) ) ( (lv_sename4_4_0= ruleterm ) ) )* )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3598:1: ( ( rulebaWS ( (lv_sename2_1_0= ruleunary_adding_operator ) ) )? ( (lv_sename1_2_0= ruleterm ) ) ( ( (lv_sename3_3_0= rulebinary_adding_operator ) ) ( (lv_sename4_4_0= ruleterm ) ) )* )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3598:2: ( rulebaWS ( (lv_sename2_1_0= ruleunary_adding_operator ) ) )? ( (lv_sename1_2_0= ruleterm ) ) ( ( (lv_sename3_3_0= rulebinary_adding_operator ) ) ( (lv_sename4_4_0= ruleterm ) ) )*
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3598:2: ( rulebaWS ( (lv_sename2_1_0= ruleunary_adding_operator ) ) )?
            int alt34=2;
            alt34 = dfa34.predict(input);
            switch (alt34) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3599:5: rulebaWS ( (lv_sename2_1_0= ruleunary_adding_operator ) )
                    {
                     
                            newCompositeNode(grammarAccess.getSimple_expressionAccess().getBaWSParserRuleCall_0_0()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulesimple_expression7101);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3606:1: ( (lv_sename2_1_0= ruleunary_adding_operator ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3607:1: (lv_sename2_1_0= ruleunary_adding_operator )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3607:1: (lv_sename2_1_0= ruleunary_adding_operator )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3608:3: lv_sename2_1_0= ruleunary_adding_operator
                    {
                     
                    	        newCompositeNode(grammarAccess.getSimple_expressionAccess().getSename2Unary_adding_operatorParserRuleCall_0_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleunary_adding_operator_in_rulesimple_expression7121);
                    lv_sename2_1_0=ruleunary_adding_operator();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSimple_expressionRule());
                    	        }
                           		set(
                           			current, 
                           			"sename2",
                            		lv_sename2_1_0, 
                            		"unary_adding_operator");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3624:4: ( (lv_sename1_2_0= ruleterm ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3625:1: (lv_sename1_2_0= ruleterm )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3625:1: (lv_sename1_2_0= ruleterm )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3626:3: lv_sename1_2_0= ruleterm
            {
             
            	        newCompositeNode(grammarAccess.getSimple_expressionAccess().getSename1TermParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleterm_in_rulesimple_expression7144);
            lv_sename1_2_0=ruleterm();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSimple_expressionRule());
            	        }
                   		set(
                   			current, 
                   			"sename1",
                    		lv_sename1_2_0, 
                    		"term");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3642:2: ( ( (lv_sename3_3_0= rulebinary_adding_operator ) ) ( (lv_sename4_4_0= ruleterm ) ) )*
            loop35:
            do {
                int alt35=2;
                int LA35_0 = input.LA(1);

                if ( (LA35_0==49||LA35_0==71) ) {
                    alt35=1;
                }


                switch (alt35) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3642:3: ( (lv_sename3_3_0= rulebinary_adding_operator ) ) ( (lv_sename4_4_0= ruleterm ) )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3642:3: ( (lv_sename3_3_0= rulebinary_adding_operator ) )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3643:1: (lv_sename3_3_0= rulebinary_adding_operator )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3643:1: (lv_sename3_3_0= rulebinary_adding_operator )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3644:3: lv_sename3_3_0= rulebinary_adding_operator
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getSimple_expressionAccess().getSename3Binary_adding_operatorParserRuleCall_2_0_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulebinary_adding_operator_in_rulesimple_expression7166);
            	    lv_sename3_3_0=rulebinary_adding_operator();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getSimple_expressionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"sename3",
            	            		lv_sename3_3_0, 
            	            		"binary_adding_operator");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }

            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3660:2: ( (lv_sename4_4_0= ruleterm ) )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3661:1: (lv_sename4_4_0= ruleterm )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3661:1: (lv_sename4_4_0= ruleterm )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3662:3: lv_sename4_4_0= ruleterm
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getSimple_expressionAccess().getSename4TermParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleterm_in_rulesimple_expression7187);
            	    lv_sename4_4_0=ruleterm();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getSimple_expressionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"sename4",
            	            		lv_sename4_4_0, 
            	            		"term");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulesimple_expression"


    // $ANTLR start "entryRuleterm"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3686:1: entryRuleterm returns [EObject current=null] : iv_ruleterm= ruleterm EOF ;
    public final EObject entryRuleterm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleterm = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3687:2: (iv_ruleterm= ruleterm EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3688:2: iv_ruleterm= ruleterm EOF
            {
             newCompositeNode(grammarAccess.getTermRule()); 
            pushFollow(FOLLOW_ruleterm_in_entryRuleterm7225);
            iv_ruleterm=ruleterm();

            state._fsp--;

             current =iv_ruleterm; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleterm7235); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleterm"


    // $ANTLR start "ruleterm"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3695:1: ruleterm returns [EObject current=null] : ( ( (lv_tname1_0_0= rulefactor ) ) ( ( (lv_tname3_1_0= rulemultiplying_operator ) ) ( (lv_tname2_2_0= rulefactor ) ) )* ) ;
    public final EObject ruleterm() throws RecognitionException {
        EObject current = null;

        EObject lv_tname1_0_0 = null;

        EObject lv_tname3_1_0 = null;

        EObject lv_tname2_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3698:28: ( ( ( (lv_tname1_0_0= rulefactor ) ) ( ( (lv_tname3_1_0= rulemultiplying_operator ) ) ( (lv_tname2_2_0= rulefactor ) ) )* ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3699:1: ( ( (lv_tname1_0_0= rulefactor ) ) ( ( (lv_tname3_1_0= rulemultiplying_operator ) ) ( (lv_tname2_2_0= rulefactor ) ) )* )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3699:1: ( ( (lv_tname1_0_0= rulefactor ) ) ( ( (lv_tname3_1_0= rulemultiplying_operator ) ) ( (lv_tname2_2_0= rulefactor ) ) )* )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3699:2: ( (lv_tname1_0_0= rulefactor ) ) ( ( (lv_tname3_1_0= rulemultiplying_operator ) ) ( (lv_tname2_2_0= rulefactor ) ) )*
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3699:2: ( (lv_tname1_0_0= rulefactor ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3700:1: (lv_tname1_0_0= rulefactor )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3700:1: (lv_tname1_0_0= rulefactor )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3701:3: lv_tname1_0_0= rulefactor
            {
             
            	        newCompositeNode(grammarAccess.getTermAccess().getTname1FactorParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_rulefactor_in_ruleterm7281);
            lv_tname1_0_0=rulefactor();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTermRule());
            	        }
                   		set(
                   			current, 
                   			"tname1",
                    		lv_tname1_0_0, 
                    		"factor");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3717:2: ( ( (lv_tname3_1_0= rulemultiplying_operator ) ) ( (lv_tname2_2_0= rulefactor ) ) )*
            loop36:
            do {
                int alt36=2;
                int LA36_0 = input.LA(1);

                if ( ((LA36_0>=72 && LA36_0<=75)) ) {
                    alt36=1;
                }


                switch (alt36) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3717:3: ( (lv_tname3_1_0= rulemultiplying_operator ) ) ( (lv_tname2_2_0= rulefactor ) )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3717:3: ( (lv_tname3_1_0= rulemultiplying_operator ) )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3718:1: (lv_tname3_1_0= rulemultiplying_operator )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3718:1: (lv_tname3_1_0= rulemultiplying_operator )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3719:3: lv_tname3_1_0= rulemultiplying_operator
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getTermAccess().getTname3Multiplying_operatorParserRuleCall_1_0_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulemultiplying_operator_in_ruleterm7303);
            	    lv_tname3_1_0=rulemultiplying_operator();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getTermRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"tname3",
            	            		lv_tname3_1_0, 
            	            		"multiplying_operator");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }

            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3735:2: ( (lv_tname2_2_0= rulefactor ) )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3736:1: (lv_tname2_2_0= rulefactor )
            	    {
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3736:1: (lv_tname2_2_0= rulefactor )
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3737:3: lv_tname2_2_0= rulefactor
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getTermAccess().getTname2FactorParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulefactor_in_ruleterm7324);
            	    lv_tname2_2_0=rulefactor();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getTermRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"tname2",
            	            		lv_tname2_2_0, 
            	            		"factor");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop36;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleterm"


    // $ANTLR start "entryRulefactor"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3761:1: entryRulefactor returns [EObject current=null] : iv_rulefactor= rulefactor EOF ;
    public final EObject entryRulefactor() throws RecognitionException {
        EObject current = null;

        EObject iv_rulefactor = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3762:2: (iv_rulefactor= rulefactor EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3763:2: iv_rulefactor= rulefactor EOF
            {
             newCompositeNode(grammarAccess.getFactorRule()); 
            pushFollow(FOLLOW_rulefactor_in_entryRulefactor7362);
            iv_rulefactor=rulefactor();

            state._fsp--;

             current =iv_rulefactor; 
            match(input,EOF,FOLLOW_EOF_in_entryRulefactor7372); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulefactor"


    // $ANTLR start "rulefactor"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3770:1: rulefactor returns [EObject current=null] : ( ( ( (lv_fname1_0_0= rulevalue ) ) ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )? ) | ( rulebaWS ( (lv_fname4_4_0= ruleunary_numeric_operator ) ) ( (lv_fname5_5_0= rulevalue ) ) ) | ( rulebaWS ( (lv_fname6_7_0= ruleunary_boolean_operator ) ) ( (lv_fname7_8_0= rulevalue ) ) ) ) ;
    public final EObject rulefactor() throws RecognitionException {
        EObject current = null;

        EObject lv_fname1_0_0 = null;

        EObject lv_fname3_1_0 = null;

        EObject lv_fname2_2_0 = null;

        EObject lv_fname4_4_0 = null;

        EObject lv_fname5_5_0 = null;

        EObject lv_fname6_7_0 = null;

        EObject lv_fname7_8_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3773:28: ( ( ( ( (lv_fname1_0_0= rulevalue ) ) ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )? ) | ( rulebaWS ( (lv_fname4_4_0= ruleunary_numeric_operator ) ) ( (lv_fname5_5_0= rulevalue ) ) ) | ( rulebaWS ( (lv_fname6_7_0= ruleunary_boolean_operator ) ) ( (lv_fname7_8_0= rulevalue ) ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3774:1: ( ( ( (lv_fname1_0_0= rulevalue ) ) ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )? ) | ( rulebaWS ( (lv_fname4_4_0= ruleunary_numeric_operator ) ) ( (lv_fname5_5_0= rulevalue ) ) ) | ( rulebaWS ( (lv_fname6_7_0= ruleunary_boolean_operator ) ) ( (lv_fname7_8_0= rulevalue ) ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3774:1: ( ( ( (lv_fname1_0_0= rulevalue ) ) ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )? ) | ( rulebaWS ( (lv_fname4_4_0= ruleunary_numeric_operator ) ) ( (lv_fname5_5_0= rulevalue ) ) ) | ( rulebaWS ( (lv_fname6_7_0= ruleunary_boolean_operator ) ) ( (lv_fname7_8_0= rulevalue ) ) ) )
            int alt38=3;
            alt38 = dfa38.predict(input);
            switch (alt38) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3774:2: ( ( (lv_fname1_0_0= rulevalue ) ) ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )? )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3774:2: ( ( (lv_fname1_0_0= rulevalue ) ) ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )? )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3774:3: ( (lv_fname1_0_0= rulevalue ) ) ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )?
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3774:3: ( (lv_fname1_0_0= rulevalue ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3775:1: (lv_fname1_0_0= rulevalue )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3775:1: (lv_fname1_0_0= rulevalue )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3776:3: lv_fname1_0_0= rulevalue
                    {
                     
                    	        newCompositeNode(grammarAccess.getFactorAccess().getFname1ValueParserRuleCall_0_0_0()); 
                    	    
                    pushFollow(FOLLOW_rulevalue_in_rulefactor7419);
                    lv_fname1_0_0=rulevalue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFactorRule());
                    	        }
                           		set(
                           			current, 
                           			"fname1",
                            		lv_fname1_0_0, 
                            		"value");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3792:2: ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )?
                    int alt37=2;
                    int LA37_0 = input.LA(1);

                    if ( (LA37_0==76) ) {
                        alt37=1;
                    }
                    switch (alt37) {
                        case 1 :
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3792:3: ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) )
                            {
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3792:3: ( (lv_fname3_1_0= rulebinary_numeric_operator ) )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3793:1: (lv_fname3_1_0= rulebinary_numeric_operator )
                            {
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3793:1: (lv_fname3_1_0= rulebinary_numeric_operator )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3794:3: lv_fname3_1_0= rulebinary_numeric_operator
                            {
                             
                            	        newCompositeNode(grammarAccess.getFactorAccess().getFname3Binary_numeric_operatorParserRuleCall_0_1_0_0()); 
                            	    
                            pushFollow(FOLLOW_rulebinary_numeric_operator_in_rulefactor7441);
                            lv_fname3_1_0=rulebinary_numeric_operator();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getFactorRule());
                            	        }
                                   		add(
                                   			current, 
                                   			"fname3",
                                    		lv_fname3_1_0, 
                                    		"binary_numeric_operator");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }

                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3810:2: ( (lv_fname2_2_0= rulevalue ) )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3811:1: (lv_fname2_2_0= rulevalue )
                            {
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3811:1: (lv_fname2_2_0= rulevalue )
                            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3812:3: lv_fname2_2_0= rulevalue
                            {
                             
                            	        newCompositeNode(grammarAccess.getFactorAccess().getFname2ValueParserRuleCall_0_1_1_0()); 
                            	    
                            pushFollow(FOLLOW_rulevalue_in_rulefactor7462);
                            lv_fname2_2_0=rulevalue();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getFactorRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"fname2",
                                    		lv_fname2_2_0, 
                                    		"value");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3829:6: ( rulebaWS ( (lv_fname4_4_0= ruleunary_numeric_operator ) ) ( (lv_fname5_5_0= rulevalue ) ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3829:6: ( rulebaWS ( (lv_fname4_4_0= ruleunary_numeric_operator ) ) ( (lv_fname5_5_0= rulevalue ) ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3830:5: rulebaWS ( (lv_fname4_4_0= ruleunary_numeric_operator ) ) ( (lv_fname5_5_0= rulevalue ) )
                    {
                     
                            newCompositeNode(grammarAccess.getFactorAccess().getBaWSParserRuleCall_1_0()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulefactor7488);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3837:1: ( (lv_fname4_4_0= ruleunary_numeric_operator ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3838:1: (lv_fname4_4_0= ruleunary_numeric_operator )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3838:1: (lv_fname4_4_0= ruleunary_numeric_operator )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3839:3: lv_fname4_4_0= ruleunary_numeric_operator
                    {
                     
                    	        newCompositeNode(grammarAccess.getFactorAccess().getFname4Unary_numeric_operatorParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleunary_numeric_operator_in_rulefactor7508);
                    lv_fname4_4_0=ruleunary_numeric_operator();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFactorRule());
                    	        }
                           		set(
                           			current, 
                           			"fname4",
                            		lv_fname4_4_0, 
                            		"unary_numeric_operator");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3855:2: ( (lv_fname5_5_0= rulevalue ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3856:1: (lv_fname5_5_0= rulevalue )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3856:1: (lv_fname5_5_0= rulevalue )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3857:3: lv_fname5_5_0= rulevalue
                    {
                     
                    	        newCompositeNode(grammarAccess.getFactorAccess().getFname5ValueParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_rulevalue_in_rulefactor7529);
                    lv_fname5_5_0=rulevalue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFactorRule());
                    	        }
                           		set(
                           			current, 
                           			"fname5",
                            		lv_fname5_5_0, 
                            		"value");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3874:6: ( rulebaWS ( (lv_fname6_7_0= ruleunary_boolean_operator ) ) ( (lv_fname7_8_0= rulevalue ) ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3874:6: ( rulebaWS ( (lv_fname6_7_0= ruleunary_boolean_operator ) ) ( (lv_fname7_8_0= rulevalue ) ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3875:5: rulebaWS ( (lv_fname6_7_0= ruleunary_boolean_operator ) ) ( (lv_fname7_8_0= rulevalue ) )
                    {
                     
                            newCompositeNode(grammarAccess.getFactorAccess().getBaWSParserRuleCall_2_0()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulefactor7553);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3882:1: ( (lv_fname6_7_0= ruleunary_boolean_operator ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3883:1: (lv_fname6_7_0= ruleunary_boolean_operator )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3883:1: (lv_fname6_7_0= ruleunary_boolean_operator )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3884:3: lv_fname6_7_0= ruleunary_boolean_operator
                    {
                     
                    	        newCompositeNode(grammarAccess.getFactorAccess().getFname6Unary_boolean_operatorParserRuleCall_2_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleunary_boolean_operator_in_rulefactor7573);
                    lv_fname6_7_0=ruleunary_boolean_operator();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFactorRule());
                    	        }
                           		set(
                           			current, 
                           			"fname6",
                            		lv_fname6_7_0, 
                            		"unary_boolean_operator");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3900:2: ( (lv_fname7_8_0= rulevalue ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3901:1: (lv_fname7_8_0= rulevalue )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3901:1: (lv_fname7_8_0= rulevalue )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3902:3: lv_fname7_8_0= rulevalue
                    {
                     
                    	        newCompositeNode(grammarAccess.getFactorAccess().getFname7ValueParserRuleCall_2_2_0()); 
                    	    
                    pushFollow(FOLLOW_rulevalue_in_rulefactor7594);
                    lv_fname7_8_0=rulevalue();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFactorRule());
                    	        }
                           		set(
                           			current, 
                           			"fname7",
                            		lv_fname7_8_0, 
                            		"value");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulefactor"


    // $ANTLR start "entryRulevalue"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3926:1: entryRulevalue returns [EObject current=null] : iv_rulevalue= rulevalue EOF ;
    public final EObject entryRulevalue() throws RecognitionException {
        EObject current = null;

        EObject iv_rulevalue = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3927:2: (iv_rulevalue= rulevalue EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3928:2: iv_rulevalue= rulevalue EOF
            {
             newCompositeNode(grammarAccess.getValueRule()); 
            pushFollow(FOLLOW_rulevalue_in_entryRulevalue7631);
            iv_rulevalue=rulevalue();

            state._fsp--;

             current =iv_rulevalue; 
            match(input,EOF,FOLLOW_EOF_in_entryRulevalue7641); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulevalue"


    // $ANTLR start "rulevalue"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3935:1: rulevalue returns [EObject current=null] : ( ( (lv_vname1_0_0= rulevalue_variable ) ) | ( rulebaWS ( (lv_vname2_2_0= rulevalue_constant ) ) rulebaWS ) ) ;
    public final EObject rulevalue() throws RecognitionException {
        EObject current = null;

        EObject lv_vname1_0_0 = null;

        EObject lv_vname2_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3938:28: ( ( ( (lv_vname1_0_0= rulevalue_variable ) ) | ( rulebaWS ( (lv_vname2_2_0= rulevalue_constant ) ) rulebaWS ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3939:1: ( ( (lv_vname1_0_0= rulevalue_variable ) ) | ( rulebaWS ( (lv_vname2_2_0= rulevalue_constant ) ) rulebaWS ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3939:1: ( ( (lv_vname1_0_0= rulevalue_variable ) ) | ( rulebaWS ( (lv_vname2_2_0= rulevalue_constant ) ) rulebaWS ) )
            int alt39=2;
            alt39 = dfa39.predict(input);
            switch (alt39) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3939:2: ( (lv_vname1_0_0= rulevalue_variable ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3939:2: ( (lv_vname1_0_0= rulevalue_variable ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3940:1: (lv_vname1_0_0= rulevalue_variable )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3940:1: (lv_vname1_0_0= rulevalue_variable )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3941:3: lv_vname1_0_0= rulevalue_variable
                    {
                     
                    	        newCompositeNode(grammarAccess.getValueAccess().getVname1Value_variableParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_rulevalue_variable_in_rulevalue7687);
                    lv_vname1_0_0=rulevalue_variable();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getValueRule());
                    	        }
                           		set(
                           			current, 
                           			"vname1",
                            		lv_vname1_0_0, 
                            		"value_variable");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3958:6: ( rulebaWS ( (lv_vname2_2_0= rulevalue_constant ) ) rulebaWS )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3958:6: ( rulebaWS ( (lv_vname2_2_0= rulevalue_constant ) ) rulebaWS )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3959:5: rulebaWS ( (lv_vname2_2_0= rulevalue_constant ) ) rulebaWS
                    {
                     
                            newCompositeNode(grammarAccess.getValueAccess().getBaWSParserRuleCall_1_0()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulevalue7710);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3966:1: ( (lv_vname2_2_0= rulevalue_constant ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3967:1: (lv_vname2_2_0= rulevalue_constant )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3967:1: (lv_vname2_2_0= rulevalue_constant )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:3968:3: lv_vname2_2_0= rulevalue_constant
                    {
                     
                    	        newCompositeNode(grammarAccess.getValueAccess().getVname2Value_constantParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulevalue_constant_in_rulevalue7730);
                    lv_vname2_2_0=rulevalue_constant();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getValueRule());
                    	        }
                           		set(
                           			current, 
                           			"vname2",
                            		lv_vname2_2_0, 
                            		"value_constant");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                     
                            newCompositeNode(grammarAccess.getValueAccess().getBaWSParserRuleCall_1_2()); 
                        
                    pushFollow(FOLLOW_rulebaWS_in_rulevalue7746);
                    rulebaWS();

                    state._fsp--;

                     
                            afterParserOrEnumRuleCall();
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulevalue"


    // $ANTLR start "entryRulevalue_variable"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4000:1: entryRulevalue_variable returns [EObject current=null] : iv_rulevalue_variable= rulevalue_variable EOF ;
    public final EObject entryRulevalue_variable() throws RecognitionException {
        EObject current = null;

        EObject iv_rulevalue_variable = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4001:2: (iv_rulevalue_variable= rulevalue_variable EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4002:2: iv_rulevalue_variable= rulevalue_variable EOF
            {
             newCompositeNode(grammarAccess.getValue_variableRule()); 
            pushFollow(FOLLOW_rulevalue_variable_in_entryRulevalue_variable7782);
            iv_rulevalue_variable=rulevalue_variable();

            state._fsp--;

             current =iv_rulevalue_variable; 
            match(input,EOF,FOLLOW_EOF_in_entryRulevalue_variable7792); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulevalue_variable"


    // $ANTLR start "rulevalue_variable"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4009:1: rulevalue_variable returns [EObject current=null] : ( rulebaWS ( (lv_vvname0_1_0= RULE_STRING_LITERAL ) ) rulebaWS ( ( (lv_vvname1_3_0= '?' ) ) | ( (lv_vvname2_4_0= 'count' ) ) | ( (lv_vvname3_5_0= 'fresh' ) ) )? rulebaWS ) ;
    public final EObject rulevalue_variable() throws RecognitionException {
        EObject current = null;

        Token lv_vvname0_1_0=null;
        Token lv_vvname1_3_0=null;
        Token lv_vvname2_4_0=null;
        Token lv_vvname3_5_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4012:28: ( ( rulebaWS ( (lv_vvname0_1_0= RULE_STRING_LITERAL ) ) rulebaWS ( ( (lv_vvname1_3_0= '?' ) ) | ( (lv_vvname2_4_0= 'count' ) ) | ( (lv_vvname3_5_0= 'fresh' ) ) )? rulebaWS ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4013:1: ( rulebaWS ( (lv_vvname0_1_0= RULE_STRING_LITERAL ) ) rulebaWS ( ( (lv_vvname1_3_0= '?' ) ) | ( (lv_vvname2_4_0= 'count' ) ) | ( (lv_vvname3_5_0= 'fresh' ) ) )? rulebaWS )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4013:1: ( rulebaWS ( (lv_vvname0_1_0= RULE_STRING_LITERAL ) ) rulebaWS ( ( (lv_vvname1_3_0= '?' ) ) | ( (lv_vvname2_4_0= 'count' ) ) | ( (lv_vvname3_5_0= 'fresh' ) ) )? rulebaWS )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4014:5: rulebaWS ( (lv_vvname0_1_0= RULE_STRING_LITERAL ) ) rulebaWS ( ( (lv_vvname1_3_0= '?' ) ) | ( (lv_vvname2_4_0= 'count' ) ) | ( (lv_vvname3_5_0= 'fresh' ) ) )? rulebaWS
            {
             
                    newCompositeNode(grammarAccess.getValue_variableAccess().getBaWSParserRuleCall_0()); 
                
            pushFollow(FOLLOW_rulebaWS_in_rulevalue_variable7833);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4021:1: ( (lv_vvname0_1_0= RULE_STRING_LITERAL ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4022:1: (lv_vvname0_1_0= RULE_STRING_LITERAL )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4022:1: (lv_vvname0_1_0= RULE_STRING_LITERAL )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4023:3: lv_vvname0_1_0= RULE_STRING_LITERAL
            {
            lv_vvname0_1_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulevalue_variable7849); 

            			newLeafNode(lv_vvname0_1_0, grammarAccess.getValue_variableAccess().getVvname0String_literalTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getValue_variableRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"vvname0",
                    		lv_vvname0_1_0, 
                    		"string_literal");
            	    

            }


            }

             
                    newCompositeNode(grammarAccess.getValue_variableAccess().getBaWSParserRuleCall_2()); 
                
            pushFollow(FOLLOW_rulebaWS_in_rulevalue_variable7870);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4047:1: ( ( (lv_vvname1_3_0= '?' ) ) | ( (lv_vvname2_4_0= 'count' ) ) | ( (lv_vvname3_5_0= 'fresh' ) ) )?
            int alt40=4;
            switch ( input.LA(1) ) {
                case 37:
                    {
                    alt40=1;
                    }
                    break;
                case 59:
                    {
                    alt40=2;
                    }
                    break;
                case 60:
                    {
                    alt40=3;
                    }
                    break;
            }

            switch (alt40) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4047:2: ( (lv_vvname1_3_0= '?' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4047:2: ( (lv_vvname1_3_0= '?' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4048:1: (lv_vvname1_3_0= '?' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4048:1: (lv_vvname1_3_0= '?' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4049:3: lv_vvname1_3_0= '?'
                    {
                    lv_vvname1_3_0=(Token)match(input,37,FOLLOW_37_in_rulevalue_variable7888); 

                            newLeafNode(lv_vvname1_3_0, grammarAccess.getValue_variableAccess().getVvname1QuestionMarkKeyword_3_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getValue_variableRule());
                    	        }
                           		setWithLastConsumed(current, "vvname1", lv_vvname1_3_0, "?");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4063:6: ( (lv_vvname2_4_0= 'count' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4063:6: ( (lv_vvname2_4_0= 'count' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4064:1: (lv_vvname2_4_0= 'count' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4064:1: (lv_vvname2_4_0= 'count' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4065:3: lv_vvname2_4_0= 'count'
                    {
                    lv_vvname2_4_0=(Token)match(input,59,FOLLOW_59_in_rulevalue_variable7925); 

                            newLeafNode(lv_vvname2_4_0, grammarAccess.getValue_variableAccess().getVvname2CountKeyword_3_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getValue_variableRule());
                    	        }
                           		setWithLastConsumed(current, "vvname2", lv_vvname2_4_0, "count");
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4079:6: ( (lv_vvname3_5_0= 'fresh' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4079:6: ( (lv_vvname3_5_0= 'fresh' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4080:1: (lv_vvname3_5_0= 'fresh' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4080:1: (lv_vvname3_5_0= 'fresh' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4081:3: lv_vvname3_5_0= 'fresh'
                    {
                    lv_vvname3_5_0=(Token)match(input,60,FOLLOW_60_in_rulevalue_variable7962); 

                            newLeafNode(lv_vvname3_5_0, grammarAccess.getValue_variableAccess().getVvname3FreshKeyword_3_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getValue_variableRule());
                    	        }
                           		setWithLastConsumed(current, "vvname3", lv_vvname3_5_0, "fresh");
                    	    

                    }


                    }


                    }
                    break;

            }

             
                    newCompositeNode(grammarAccess.getValue_variableAccess().getBaWSParserRuleCall_4()); 
                
            pushFollow(FOLLOW_rulebaWS_in_rulevalue_variable7993);
            rulebaWS();

            state._fsp--;

             
                    afterParserOrEnumRuleCall();
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulevalue_variable"


    // $ANTLR start "entryRulevalue_constant"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4110:1: entryRulevalue_constant returns [EObject current=null] : iv_rulevalue_constant= rulevalue_constant EOF ;
    public final EObject entryRulevalue_constant() throws RecognitionException {
        EObject current = null;

        EObject iv_rulevalue_constant = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4111:2: (iv_rulevalue_constant= rulevalue_constant EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4112:2: iv_rulevalue_constant= rulevalue_constant EOF
            {
             newCompositeNode(grammarAccess.getValue_constantRule()); 
            pushFollow(FOLLOW_rulevalue_constant_in_entryRulevalue_constant8028);
            iv_rulevalue_constant=rulevalue_constant();

            state._fsp--;

             current =iv_rulevalue_constant; 
            match(input,EOF,FOLLOW_EOF_in_entryRulevalue_constant8038); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulevalue_constant"


    // $ANTLR start "rulevalue_constant"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4119:1: rulevalue_constant returns [EObject current=null] : ( ( (lv_vcname1_0_0= ruleboolean_literal ) ) | ( (lv_vcname2_1_0= rulenumeric_literal ) ) | ( ( (lv_vcname3_2_0= RULE_STRING_LITERAL ) ) otherlv_3= '::' ( (lv_vcname4_4_0= RULE_STRING_LITERAL ) ) ) ) ;
    public final EObject rulevalue_constant() throws RecognitionException {
        EObject current = null;

        Token lv_vcname3_2_0=null;
        Token otherlv_3=null;
        Token lv_vcname4_4_0=null;
        EObject lv_vcname1_0_0 = null;

        EObject lv_vcname2_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4122:28: ( ( ( (lv_vcname1_0_0= ruleboolean_literal ) ) | ( (lv_vcname2_1_0= rulenumeric_literal ) ) | ( ( (lv_vcname3_2_0= RULE_STRING_LITERAL ) ) otherlv_3= '::' ( (lv_vcname4_4_0= RULE_STRING_LITERAL ) ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4123:1: ( ( (lv_vcname1_0_0= ruleboolean_literal ) ) | ( (lv_vcname2_1_0= rulenumeric_literal ) ) | ( ( (lv_vcname3_2_0= RULE_STRING_LITERAL ) ) otherlv_3= '::' ( (lv_vcname4_4_0= RULE_STRING_LITERAL ) ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4123:1: ( ( (lv_vcname1_0_0= ruleboolean_literal ) ) | ( (lv_vcname2_1_0= rulenumeric_literal ) ) | ( ( (lv_vcname3_2_0= RULE_STRING_LITERAL ) ) otherlv_3= '::' ( (lv_vcname4_4_0= RULE_STRING_LITERAL ) ) ) )
            int alt41=3;
            switch ( input.LA(1) ) {
            case 79:
            case 80:
                {
                alt41=1;
                }
                break;
            case RULE_DIGIT:
            case RULE_INT:
                {
                alt41=2;
                }
                break;
            case RULE_STRING_LITERAL:
                {
                alt41=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 41, 0, input);

                throw nvae;
            }

            switch (alt41) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4123:2: ( (lv_vcname1_0_0= ruleboolean_literal ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4123:2: ( (lv_vcname1_0_0= ruleboolean_literal ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4124:1: (lv_vcname1_0_0= ruleboolean_literal )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4124:1: (lv_vcname1_0_0= ruleboolean_literal )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4125:3: lv_vcname1_0_0= ruleboolean_literal
                    {
                     
                    	        newCompositeNode(grammarAccess.getValue_constantAccess().getVcname1Boolean_literalParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleboolean_literal_in_rulevalue_constant8084);
                    lv_vcname1_0_0=ruleboolean_literal();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getValue_constantRule());
                    	        }
                           		set(
                           			current, 
                           			"vcname1",
                            		lv_vcname1_0_0, 
                            		"boolean_literal");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4142:6: ( (lv_vcname2_1_0= rulenumeric_literal ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4142:6: ( (lv_vcname2_1_0= rulenumeric_literal ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4143:1: (lv_vcname2_1_0= rulenumeric_literal )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4143:1: (lv_vcname2_1_0= rulenumeric_literal )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4144:3: lv_vcname2_1_0= rulenumeric_literal
                    {
                     
                    	        newCompositeNode(grammarAccess.getValue_constantAccess().getVcname2Numeric_literalParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_rulenumeric_literal_in_rulevalue_constant8111);
                    lv_vcname2_1_0=rulenumeric_literal();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getValue_constantRule());
                    	        }
                           		set(
                           			current, 
                           			"vcname2",
                            		lv_vcname2_1_0, 
                            		"numeric_literal");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4161:6: ( ( (lv_vcname3_2_0= RULE_STRING_LITERAL ) ) otherlv_3= '::' ( (lv_vcname4_4_0= RULE_STRING_LITERAL ) ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4161:6: ( ( (lv_vcname3_2_0= RULE_STRING_LITERAL ) ) otherlv_3= '::' ( (lv_vcname4_4_0= RULE_STRING_LITERAL ) ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4161:7: ( (lv_vcname3_2_0= RULE_STRING_LITERAL ) ) otherlv_3= '::' ( (lv_vcname4_4_0= RULE_STRING_LITERAL ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4161:7: ( (lv_vcname3_2_0= RULE_STRING_LITERAL ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4162:1: (lv_vcname3_2_0= RULE_STRING_LITERAL )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4162:1: (lv_vcname3_2_0= RULE_STRING_LITERAL )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4163:3: lv_vcname3_2_0= RULE_STRING_LITERAL
                    {
                    lv_vcname3_2_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulevalue_constant8135); 

                    			newLeafNode(lv_vcname3_2_0, grammarAccess.getValue_constantAccess().getVcname3String_literalTerminalRuleCall_2_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getValue_constantRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"vcname3",
                            		lv_vcname3_2_0, 
                            		"string_literal");
                    	    

                    }


                    }

                    otherlv_3=(Token)match(input,61,FOLLOW_61_in_rulevalue_constant8152); 

                        	newLeafNode(otherlv_3, grammarAccess.getValue_constantAccess().getColonColonKeyword_2_1());
                        
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4183:1: ( (lv_vcname4_4_0= RULE_STRING_LITERAL ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4184:1: (lv_vcname4_4_0= RULE_STRING_LITERAL )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4184:1: (lv_vcname4_4_0= RULE_STRING_LITERAL )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4185:3: lv_vcname4_4_0= RULE_STRING_LITERAL
                    {
                    lv_vcname4_4_0=(Token)match(input,RULE_STRING_LITERAL,FOLLOW_RULE_STRING_LITERAL_in_rulevalue_constant8169); 

                    			newLeafNode(lv_vcname4_4_0, grammarAccess.getValue_constantAccess().getVcname4String_literalTerminalRuleCall_2_2_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getValue_constantRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"vcname4",
                            		lv_vcname4_4_0, 
                            		"string_literal");
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulevalue_constant"


    // $ANTLR start "entryRulelogical_operator"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4209:1: entryRulelogical_operator returns [EObject current=null] : iv_rulelogical_operator= rulelogical_operator EOF ;
    public final EObject entryRulelogical_operator() throws RecognitionException {
        EObject current = null;

        EObject iv_rulelogical_operator = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4210:2: (iv_rulelogical_operator= rulelogical_operator EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4211:2: iv_rulelogical_operator= rulelogical_operator EOF
            {
             newCompositeNode(grammarAccess.getLogical_operatorRule()); 
            pushFollow(FOLLOW_rulelogical_operator_in_entryRulelogical_operator8211);
            iv_rulelogical_operator=rulelogical_operator();

            state._fsp--;

             current =iv_rulelogical_operator; 
            match(input,EOF,FOLLOW_EOF_in_entryRulelogical_operator8221); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulelogical_operator"


    // $ANTLR start "rulelogical_operator"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4218:1: rulelogical_operator returns [EObject current=null] : ( ( (lv_loname1_0_0= 'and' ) ) | ( (lv_loname2_1_0= 'or' ) ) | ( (lv_loname3_2_0= 'xor' ) ) ) ;
    public final EObject rulelogical_operator() throws RecognitionException {
        EObject current = null;

        Token lv_loname1_0_0=null;
        Token lv_loname2_1_0=null;
        Token lv_loname3_2_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4221:28: ( ( ( (lv_loname1_0_0= 'and' ) ) | ( (lv_loname2_1_0= 'or' ) ) | ( (lv_loname3_2_0= 'xor' ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4222:1: ( ( (lv_loname1_0_0= 'and' ) ) | ( (lv_loname2_1_0= 'or' ) ) | ( (lv_loname3_2_0= 'xor' ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4222:1: ( ( (lv_loname1_0_0= 'and' ) ) | ( (lv_loname2_1_0= 'or' ) ) | ( (lv_loname3_2_0= 'xor' ) ) )
            int alt42=3;
            switch ( input.LA(1) ) {
            case 62:
                {
                alt42=1;
                }
                break;
            case 63:
                {
                alt42=2;
                }
                break;
            case 64:
                {
                alt42=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 42, 0, input);

                throw nvae;
            }

            switch (alt42) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4222:2: ( (lv_loname1_0_0= 'and' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4222:2: ( (lv_loname1_0_0= 'and' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4223:1: (lv_loname1_0_0= 'and' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4223:1: (lv_loname1_0_0= 'and' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4224:3: lv_loname1_0_0= 'and'
                    {
                    lv_loname1_0_0=(Token)match(input,62,FOLLOW_62_in_rulelogical_operator8264); 

                            newLeafNode(lv_loname1_0_0, grammarAccess.getLogical_operatorAccess().getLoname1AndKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getLogical_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "loname1", lv_loname1_0_0, "and");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4238:6: ( (lv_loname2_1_0= 'or' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4238:6: ( (lv_loname2_1_0= 'or' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4239:1: (lv_loname2_1_0= 'or' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4239:1: (lv_loname2_1_0= 'or' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4240:3: lv_loname2_1_0= 'or'
                    {
                    lv_loname2_1_0=(Token)match(input,63,FOLLOW_63_in_rulelogical_operator8301); 

                            newLeafNode(lv_loname2_1_0, grammarAccess.getLogical_operatorAccess().getLoname2OrKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getLogical_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "loname2", lv_loname2_1_0, "or");
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4254:6: ( (lv_loname3_2_0= 'xor' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4254:6: ( (lv_loname3_2_0= 'xor' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4255:1: (lv_loname3_2_0= 'xor' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4255:1: (lv_loname3_2_0= 'xor' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4256:3: lv_loname3_2_0= 'xor'
                    {
                    lv_loname3_2_0=(Token)match(input,64,FOLLOW_64_in_rulelogical_operator8338); 

                            newLeafNode(lv_loname3_2_0, grammarAccess.getLogical_operatorAccess().getLoname3XorKeyword_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getLogical_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "loname3", lv_loname3_2_0, "xor");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulelogical_operator"


    // $ANTLR start "entryRulerelational_operator"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4277:1: entryRulerelational_operator returns [EObject current=null] : iv_rulerelational_operator= rulerelational_operator EOF ;
    public final EObject entryRulerelational_operator() throws RecognitionException {
        EObject current = null;

        EObject iv_rulerelational_operator = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4278:2: (iv_rulerelational_operator= rulerelational_operator EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4279:2: iv_rulerelational_operator= rulerelational_operator EOF
            {
             newCompositeNode(grammarAccess.getRelational_operatorRule()); 
            pushFollow(FOLLOW_rulerelational_operator_in_entryRulerelational_operator8387);
            iv_rulerelational_operator=rulerelational_operator();

            state._fsp--;

             current =iv_rulerelational_operator; 
            match(input,EOF,FOLLOW_EOF_in_entryRulerelational_operator8397); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulerelational_operator"


    // $ANTLR start "rulerelational_operator"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4286:1: rulerelational_operator returns [EObject current=null] : ( ( (lv_roname1_0_0= '=' ) ) | ( (lv_roname2_1_0= '!=' ) ) | ( (lv_roname3_2_0= '<' ) ) | ( (lv_roname4_3_0= '<=' ) ) | ( (lv_roname5_4_0= '>' ) ) | ( (lv_roname6_5_0= '>=' ) ) ) ;
    public final EObject rulerelational_operator() throws RecognitionException {
        EObject current = null;

        Token lv_roname1_0_0=null;
        Token lv_roname2_1_0=null;
        Token lv_roname3_2_0=null;
        Token lv_roname4_3_0=null;
        Token lv_roname5_4_0=null;
        Token lv_roname6_5_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4289:28: ( ( ( (lv_roname1_0_0= '=' ) ) | ( (lv_roname2_1_0= '!=' ) ) | ( (lv_roname3_2_0= '<' ) ) | ( (lv_roname4_3_0= '<=' ) ) | ( (lv_roname5_4_0= '>' ) ) | ( (lv_roname6_5_0= '>=' ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4290:1: ( ( (lv_roname1_0_0= '=' ) ) | ( (lv_roname2_1_0= '!=' ) ) | ( (lv_roname3_2_0= '<' ) ) | ( (lv_roname4_3_0= '<=' ) ) | ( (lv_roname5_4_0= '>' ) ) | ( (lv_roname6_5_0= '>=' ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4290:1: ( ( (lv_roname1_0_0= '=' ) ) | ( (lv_roname2_1_0= '!=' ) ) | ( (lv_roname3_2_0= '<' ) ) | ( (lv_roname4_3_0= '<=' ) ) | ( (lv_roname5_4_0= '>' ) ) | ( (lv_roname6_5_0= '>=' ) ) )
            int alt43=6;
            switch ( input.LA(1) ) {
            case 65:
                {
                alt43=1;
                }
                break;
            case 66:
                {
                alt43=2;
                }
                break;
            case 67:
                {
                alt43=3;
                }
                break;
            case 68:
                {
                alt43=4;
                }
                break;
            case 69:
                {
                alt43=5;
                }
                break;
            case 70:
                {
                alt43=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 43, 0, input);

                throw nvae;
            }

            switch (alt43) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4290:2: ( (lv_roname1_0_0= '=' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4290:2: ( (lv_roname1_0_0= '=' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4291:1: (lv_roname1_0_0= '=' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4291:1: (lv_roname1_0_0= '=' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4292:3: lv_roname1_0_0= '='
                    {
                    lv_roname1_0_0=(Token)match(input,65,FOLLOW_65_in_rulerelational_operator8440); 

                            newLeafNode(lv_roname1_0_0, grammarAccess.getRelational_operatorAccess().getRoname1EqualsSignKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getRelational_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "roname1", lv_roname1_0_0, "=");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4306:6: ( (lv_roname2_1_0= '!=' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4306:6: ( (lv_roname2_1_0= '!=' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4307:1: (lv_roname2_1_0= '!=' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4307:1: (lv_roname2_1_0= '!=' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4308:3: lv_roname2_1_0= '!='
                    {
                    lv_roname2_1_0=(Token)match(input,66,FOLLOW_66_in_rulerelational_operator8477); 

                            newLeafNode(lv_roname2_1_0, grammarAccess.getRelational_operatorAccess().getRoname2ExclamationMarkEqualsSignKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getRelational_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "roname2", lv_roname2_1_0, "!=");
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4322:6: ( (lv_roname3_2_0= '<' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4322:6: ( (lv_roname3_2_0= '<' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4323:1: (lv_roname3_2_0= '<' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4323:1: (lv_roname3_2_0= '<' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4324:3: lv_roname3_2_0= '<'
                    {
                    lv_roname3_2_0=(Token)match(input,67,FOLLOW_67_in_rulerelational_operator8514); 

                            newLeafNode(lv_roname3_2_0, grammarAccess.getRelational_operatorAccess().getRoname3LessThanSignKeyword_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getRelational_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "roname3", lv_roname3_2_0, "<");
                    	    

                    }


                    }


                    }
                    break;
                case 4 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4338:6: ( (lv_roname4_3_0= '<=' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4338:6: ( (lv_roname4_3_0= '<=' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4339:1: (lv_roname4_3_0= '<=' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4339:1: (lv_roname4_3_0= '<=' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4340:3: lv_roname4_3_0= '<='
                    {
                    lv_roname4_3_0=(Token)match(input,68,FOLLOW_68_in_rulerelational_operator8551); 

                            newLeafNode(lv_roname4_3_0, grammarAccess.getRelational_operatorAccess().getRoname4LessThanSignEqualsSignKeyword_3_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getRelational_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "roname4", lv_roname4_3_0, "<=");
                    	    

                    }


                    }


                    }
                    break;
                case 5 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4354:6: ( (lv_roname5_4_0= '>' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4354:6: ( (lv_roname5_4_0= '>' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4355:1: (lv_roname5_4_0= '>' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4355:1: (lv_roname5_4_0= '>' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4356:3: lv_roname5_4_0= '>'
                    {
                    lv_roname5_4_0=(Token)match(input,69,FOLLOW_69_in_rulerelational_operator8588); 

                            newLeafNode(lv_roname5_4_0, grammarAccess.getRelational_operatorAccess().getRoname5GreaterThanSignKeyword_4_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getRelational_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "roname5", lv_roname5_4_0, ">");
                    	    

                    }


                    }


                    }
                    break;
                case 6 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4370:6: ( (lv_roname6_5_0= '>=' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4370:6: ( (lv_roname6_5_0= '>=' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4371:1: (lv_roname6_5_0= '>=' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4371:1: (lv_roname6_5_0= '>=' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4372:3: lv_roname6_5_0= '>='
                    {
                    lv_roname6_5_0=(Token)match(input,70,FOLLOW_70_in_rulerelational_operator8625); 

                            newLeafNode(lv_roname6_5_0, grammarAccess.getRelational_operatorAccess().getRoname6GreaterThanSignEqualsSignKeyword_5_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getRelational_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "roname6", lv_roname6_5_0, ">=");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulerelational_operator"


    // $ANTLR start "entryRulebinary_adding_operator"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4393:1: entryRulebinary_adding_operator returns [EObject current=null] : iv_rulebinary_adding_operator= rulebinary_adding_operator EOF ;
    public final EObject entryRulebinary_adding_operator() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebinary_adding_operator = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4394:2: (iv_rulebinary_adding_operator= rulebinary_adding_operator EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4395:2: iv_rulebinary_adding_operator= rulebinary_adding_operator EOF
            {
             newCompositeNode(grammarAccess.getBinary_adding_operatorRule()); 
            pushFollow(FOLLOW_rulebinary_adding_operator_in_entryRulebinary_adding_operator8674);
            iv_rulebinary_adding_operator=rulebinary_adding_operator();

            state._fsp--;

             current =iv_rulebinary_adding_operator; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebinary_adding_operator8684); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebinary_adding_operator"


    // $ANTLR start "rulebinary_adding_operator"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4402:1: rulebinary_adding_operator returns [EObject current=null] : ( ( (lv_baoname1_0_0= '+' ) ) | ( (lv_baoname2_1_0= '-' ) ) ) ;
    public final EObject rulebinary_adding_operator() throws RecognitionException {
        EObject current = null;

        Token lv_baoname1_0_0=null;
        Token lv_baoname2_1_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4405:28: ( ( ( (lv_baoname1_0_0= '+' ) ) | ( (lv_baoname2_1_0= '-' ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4406:1: ( ( (lv_baoname1_0_0= '+' ) ) | ( (lv_baoname2_1_0= '-' ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4406:1: ( ( (lv_baoname1_0_0= '+' ) ) | ( (lv_baoname2_1_0= '-' ) ) )
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==49) ) {
                alt44=1;
            }
            else if ( (LA44_0==71) ) {
                alt44=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 44, 0, input);

                throw nvae;
            }
            switch (alt44) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4406:2: ( (lv_baoname1_0_0= '+' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4406:2: ( (lv_baoname1_0_0= '+' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4407:1: (lv_baoname1_0_0= '+' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4407:1: (lv_baoname1_0_0= '+' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4408:3: lv_baoname1_0_0= '+'
                    {
                    lv_baoname1_0_0=(Token)match(input,49,FOLLOW_49_in_rulebinary_adding_operator8727); 

                            newLeafNode(lv_baoname1_0_0, grammarAccess.getBinary_adding_operatorAccess().getBaoname1PlusSignKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBinary_adding_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "baoname1", lv_baoname1_0_0, "+");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4422:6: ( (lv_baoname2_1_0= '-' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4422:6: ( (lv_baoname2_1_0= '-' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4423:1: (lv_baoname2_1_0= '-' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4423:1: (lv_baoname2_1_0= '-' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4424:3: lv_baoname2_1_0= '-'
                    {
                    lv_baoname2_1_0=(Token)match(input,71,FOLLOW_71_in_rulebinary_adding_operator8764); 

                            newLeafNode(lv_baoname2_1_0, grammarAccess.getBinary_adding_operatorAccess().getBaoname2HyphenMinusKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBinary_adding_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "baoname2", lv_baoname2_1_0, "-");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebinary_adding_operator"


    // $ANTLR start "entryRuleunary_adding_operator"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4445:1: entryRuleunary_adding_operator returns [EObject current=null] : iv_ruleunary_adding_operator= ruleunary_adding_operator EOF ;
    public final EObject entryRuleunary_adding_operator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleunary_adding_operator = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4446:2: (iv_ruleunary_adding_operator= ruleunary_adding_operator EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4447:2: iv_ruleunary_adding_operator= ruleunary_adding_operator EOF
            {
             newCompositeNode(grammarAccess.getUnary_adding_operatorRule()); 
            pushFollow(FOLLOW_ruleunary_adding_operator_in_entryRuleunary_adding_operator8813);
            iv_ruleunary_adding_operator=ruleunary_adding_operator();

            state._fsp--;

             current =iv_ruleunary_adding_operator; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleunary_adding_operator8823); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleunary_adding_operator"


    // $ANTLR start "ruleunary_adding_operator"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4454:1: ruleunary_adding_operator returns [EObject current=null] : ( ( (lv_uaoname1_0_0= '+' ) ) | ( (lv_uaoname2_1_0= '-' ) ) ) ;
    public final EObject ruleunary_adding_operator() throws RecognitionException {
        EObject current = null;

        Token lv_uaoname1_0_0=null;
        Token lv_uaoname2_1_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4457:28: ( ( ( (lv_uaoname1_0_0= '+' ) ) | ( (lv_uaoname2_1_0= '-' ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4458:1: ( ( (lv_uaoname1_0_0= '+' ) ) | ( (lv_uaoname2_1_0= '-' ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4458:1: ( ( (lv_uaoname1_0_0= '+' ) ) | ( (lv_uaoname2_1_0= '-' ) ) )
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==49) ) {
                alt45=1;
            }
            else if ( (LA45_0==71) ) {
                alt45=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 45, 0, input);

                throw nvae;
            }
            switch (alt45) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4458:2: ( (lv_uaoname1_0_0= '+' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4458:2: ( (lv_uaoname1_0_0= '+' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4459:1: (lv_uaoname1_0_0= '+' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4459:1: (lv_uaoname1_0_0= '+' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4460:3: lv_uaoname1_0_0= '+'
                    {
                    lv_uaoname1_0_0=(Token)match(input,49,FOLLOW_49_in_ruleunary_adding_operator8866); 

                            newLeafNode(lv_uaoname1_0_0, grammarAccess.getUnary_adding_operatorAccess().getUaoname1PlusSignKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnary_adding_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "uaoname1", lv_uaoname1_0_0, "+");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4474:6: ( (lv_uaoname2_1_0= '-' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4474:6: ( (lv_uaoname2_1_0= '-' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4475:1: (lv_uaoname2_1_0= '-' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4475:1: (lv_uaoname2_1_0= '-' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4476:3: lv_uaoname2_1_0= '-'
                    {
                    lv_uaoname2_1_0=(Token)match(input,71,FOLLOW_71_in_ruleunary_adding_operator8903); 

                            newLeafNode(lv_uaoname2_1_0, grammarAccess.getUnary_adding_operatorAccess().getUaoname2HyphenMinusKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getUnary_adding_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "uaoname2", lv_uaoname2_1_0, "-");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleunary_adding_operator"


    // $ANTLR start "entryRulemultiplying_operator"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4497:1: entryRulemultiplying_operator returns [EObject current=null] : iv_rulemultiplying_operator= rulemultiplying_operator EOF ;
    public final EObject entryRulemultiplying_operator() throws RecognitionException {
        EObject current = null;

        EObject iv_rulemultiplying_operator = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4498:2: (iv_rulemultiplying_operator= rulemultiplying_operator EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4499:2: iv_rulemultiplying_operator= rulemultiplying_operator EOF
            {
             newCompositeNode(grammarAccess.getMultiplying_operatorRule()); 
            pushFollow(FOLLOW_rulemultiplying_operator_in_entryRulemultiplying_operator8952);
            iv_rulemultiplying_operator=rulemultiplying_operator();

            state._fsp--;

             current =iv_rulemultiplying_operator; 
            match(input,EOF,FOLLOW_EOF_in_entryRulemultiplying_operator8962); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulemultiplying_operator"


    // $ANTLR start "rulemultiplying_operator"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4506:1: rulemultiplying_operator returns [EObject current=null] : ( ( (lv_moname1_0_0= '*' ) ) | ( (lv_moname2_1_0= '/' ) ) | ( (lv_moname3_2_0= 'mod' ) ) | ( (lv_moname4_3_0= 'rem' ) ) ) ;
    public final EObject rulemultiplying_operator() throws RecognitionException {
        EObject current = null;

        Token lv_moname1_0_0=null;
        Token lv_moname2_1_0=null;
        Token lv_moname3_2_0=null;
        Token lv_moname4_3_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4509:28: ( ( ( (lv_moname1_0_0= '*' ) ) | ( (lv_moname2_1_0= '/' ) ) | ( (lv_moname3_2_0= 'mod' ) ) | ( (lv_moname4_3_0= 'rem' ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4510:1: ( ( (lv_moname1_0_0= '*' ) ) | ( (lv_moname2_1_0= '/' ) ) | ( (lv_moname3_2_0= 'mod' ) ) | ( (lv_moname4_3_0= 'rem' ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4510:1: ( ( (lv_moname1_0_0= '*' ) ) | ( (lv_moname2_1_0= '/' ) ) | ( (lv_moname3_2_0= 'mod' ) ) | ( (lv_moname4_3_0= 'rem' ) ) )
            int alt46=4;
            switch ( input.LA(1) ) {
            case 72:
                {
                alt46=1;
                }
                break;
            case 73:
                {
                alt46=2;
                }
                break;
            case 74:
                {
                alt46=3;
                }
                break;
            case 75:
                {
                alt46=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 46, 0, input);

                throw nvae;
            }

            switch (alt46) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4510:2: ( (lv_moname1_0_0= '*' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4510:2: ( (lv_moname1_0_0= '*' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4511:1: (lv_moname1_0_0= '*' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4511:1: (lv_moname1_0_0= '*' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4512:3: lv_moname1_0_0= '*'
                    {
                    lv_moname1_0_0=(Token)match(input,72,FOLLOW_72_in_rulemultiplying_operator9005); 

                            newLeafNode(lv_moname1_0_0, grammarAccess.getMultiplying_operatorAccess().getMoname1AsteriskKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getMultiplying_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "moname1", lv_moname1_0_0, "*");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4526:6: ( (lv_moname2_1_0= '/' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4526:6: ( (lv_moname2_1_0= '/' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4527:1: (lv_moname2_1_0= '/' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4527:1: (lv_moname2_1_0= '/' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4528:3: lv_moname2_1_0= '/'
                    {
                    lv_moname2_1_0=(Token)match(input,73,FOLLOW_73_in_rulemultiplying_operator9042); 

                            newLeafNode(lv_moname2_1_0, grammarAccess.getMultiplying_operatorAccess().getMoname2SolidusKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getMultiplying_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "moname2", lv_moname2_1_0, "/");
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4542:6: ( (lv_moname3_2_0= 'mod' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4542:6: ( (lv_moname3_2_0= 'mod' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4543:1: (lv_moname3_2_0= 'mod' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4543:1: (lv_moname3_2_0= 'mod' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4544:3: lv_moname3_2_0= 'mod'
                    {
                    lv_moname3_2_0=(Token)match(input,74,FOLLOW_74_in_rulemultiplying_operator9079); 

                            newLeafNode(lv_moname3_2_0, grammarAccess.getMultiplying_operatorAccess().getMoname3ModKeyword_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getMultiplying_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "moname3", lv_moname3_2_0, "mod");
                    	    

                    }


                    }


                    }
                    break;
                case 4 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4558:6: ( (lv_moname4_3_0= 'rem' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4558:6: ( (lv_moname4_3_0= 'rem' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4559:1: (lv_moname4_3_0= 'rem' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4559:1: (lv_moname4_3_0= 'rem' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4560:3: lv_moname4_3_0= 'rem'
                    {
                    lv_moname4_3_0=(Token)match(input,75,FOLLOW_75_in_rulemultiplying_operator9116); 

                            newLeafNode(lv_moname4_3_0, grammarAccess.getMultiplying_operatorAccess().getMoname4RemKeyword_3_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getMultiplying_operatorRule());
                    	        }
                           		setWithLastConsumed(current, "moname4", lv_moname4_3_0, "rem");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulemultiplying_operator"


    // $ANTLR start "entryRulebinary_numeric_operator"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4581:1: entryRulebinary_numeric_operator returns [EObject current=null] : iv_rulebinary_numeric_operator= rulebinary_numeric_operator EOF ;
    public final EObject entryRulebinary_numeric_operator() throws RecognitionException {
        EObject current = null;

        EObject iv_rulebinary_numeric_operator = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4582:2: (iv_rulebinary_numeric_operator= rulebinary_numeric_operator EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4583:2: iv_rulebinary_numeric_operator= rulebinary_numeric_operator EOF
            {
             newCompositeNode(grammarAccess.getBinary_numeric_operatorRule()); 
            pushFollow(FOLLOW_rulebinary_numeric_operator_in_entryRulebinary_numeric_operator9165);
            iv_rulebinary_numeric_operator=rulebinary_numeric_operator();

            state._fsp--;

             current =iv_rulebinary_numeric_operator; 
            match(input,EOF,FOLLOW_EOF_in_entryRulebinary_numeric_operator9175); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebinary_numeric_operator"


    // $ANTLR start "rulebinary_numeric_operator"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4590:1: rulebinary_numeric_operator returns [EObject current=null] : ( (lv_bnoname_0_0= '**' ) ) ;
    public final EObject rulebinary_numeric_operator() throws RecognitionException {
        EObject current = null;

        Token lv_bnoname_0_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4593:28: ( ( (lv_bnoname_0_0= '**' ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4594:1: ( (lv_bnoname_0_0= '**' ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4594:1: ( (lv_bnoname_0_0= '**' ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4595:1: (lv_bnoname_0_0= '**' )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4595:1: (lv_bnoname_0_0= '**' )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4596:3: lv_bnoname_0_0= '**'
            {
            lv_bnoname_0_0=(Token)match(input,76,FOLLOW_76_in_rulebinary_numeric_operator9217); 

                    newLeafNode(lv_bnoname_0_0, grammarAccess.getBinary_numeric_operatorAccess().getBnonameAsteriskAsteriskKeyword_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getBinary_numeric_operatorRule());
            	        }
                   		setWithLastConsumed(current, "bnoname", lv_bnoname_0_0, "**");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebinary_numeric_operator"


    // $ANTLR start "entryRuleunary_numeric_operator"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4617:1: entryRuleunary_numeric_operator returns [EObject current=null] : iv_ruleunary_numeric_operator= ruleunary_numeric_operator EOF ;
    public final EObject entryRuleunary_numeric_operator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleunary_numeric_operator = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4618:2: (iv_ruleunary_numeric_operator= ruleunary_numeric_operator EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4619:2: iv_ruleunary_numeric_operator= ruleunary_numeric_operator EOF
            {
             newCompositeNode(grammarAccess.getUnary_numeric_operatorRule()); 
            pushFollow(FOLLOW_ruleunary_numeric_operator_in_entryRuleunary_numeric_operator9265);
            iv_ruleunary_numeric_operator=ruleunary_numeric_operator();

            state._fsp--;

             current =iv_ruleunary_numeric_operator; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleunary_numeric_operator9275); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleunary_numeric_operator"


    // $ANTLR start "ruleunary_numeric_operator"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4626:1: ruleunary_numeric_operator returns [EObject current=null] : ( (lv_unoname_0_0= 'abs' ) ) ;
    public final EObject ruleunary_numeric_operator() throws RecognitionException {
        EObject current = null;

        Token lv_unoname_0_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4629:28: ( ( (lv_unoname_0_0= 'abs' ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4630:1: ( (lv_unoname_0_0= 'abs' ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4630:1: ( (lv_unoname_0_0= 'abs' ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4631:1: (lv_unoname_0_0= 'abs' )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4631:1: (lv_unoname_0_0= 'abs' )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4632:3: lv_unoname_0_0= 'abs'
            {
            lv_unoname_0_0=(Token)match(input,77,FOLLOW_77_in_ruleunary_numeric_operator9317); 

                    newLeafNode(lv_unoname_0_0, grammarAccess.getUnary_numeric_operatorAccess().getUnonameAbsKeyword_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getUnary_numeric_operatorRule());
            	        }
                   		setWithLastConsumed(current, "unoname", lv_unoname_0_0, "abs");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleunary_numeric_operator"


    // $ANTLR start "entryRuleunary_boolean_operator"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4653:1: entryRuleunary_boolean_operator returns [EObject current=null] : iv_ruleunary_boolean_operator= ruleunary_boolean_operator EOF ;
    public final EObject entryRuleunary_boolean_operator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleunary_boolean_operator = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4654:2: (iv_ruleunary_boolean_operator= ruleunary_boolean_operator EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4655:2: iv_ruleunary_boolean_operator= ruleunary_boolean_operator EOF
            {
             newCompositeNode(grammarAccess.getUnary_boolean_operatorRule()); 
            pushFollow(FOLLOW_ruleunary_boolean_operator_in_entryRuleunary_boolean_operator9365);
            iv_ruleunary_boolean_operator=ruleunary_boolean_operator();

            state._fsp--;

             current =iv_ruleunary_boolean_operator; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleunary_boolean_operator9375); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleunary_boolean_operator"


    // $ANTLR start "ruleunary_boolean_operator"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4662:1: ruleunary_boolean_operator returns [EObject current=null] : ( (lv_uboname_0_0= 'not' ) ) ;
    public final EObject ruleunary_boolean_operator() throws RecognitionException {
        EObject current = null;

        Token lv_uboname_0_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4665:28: ( ( (lv_uboname_0_0= 'not' ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4666:1: ( (lv_uboname_0_0= 'not' ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4666:1: ( (lv_uboname_0_0= 'not' ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4667:1: (lv_uboname_0_0= 'not' )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4667:1: (lv_uboname_0_0= 'not' )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4668:3: lv_uboname_0_0= 'not'
            {
            lv_uboname_0_0=(Token)match(input,78,FOLLOW_78_in_ruleunary_boolean_operator9417); 

                    newLeafNode(lv_uboname_0_0, grammarAccess.getUnary_boolean_operatorAccess().getUbonameNotKeyword_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getUnary_boolean_operatorRule());
            	        }
                   		setWithLastConsumed(current, "uboname", lv_uboname_0_0, "not");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleunary_boolean_operator"


    // $ANTLR start "entryRuleboolean_literal"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4689:1: entryRuleboolean_literal returns [EObject current=null] : iv_ruleboolean_literal= ruleboolean_literal EOF ;
    public final EObject entryRuleboolean_literal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleboolean_literal = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4690:2: (iv_ruleboolean_literal= ruleboolean_literal EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4691:2: iv_ruleboolean_literal= ruleboolean_literal EOF
            {
             newCompositeNode(grammarAccess.getBoolean_literalRule()); 
            pushFollow(FOLLOW_ruleboolean_literal_in_entryRuleboolean_literal9465);
            iv_ruleboolean_literal=ruleboolean_literal();

            state._fsp--;

             current =iv_ruleboolean_literal; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleboolean_literal9475); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleboolean_literal"


    // $ANTLR start "ruleboolean_literal"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4698:1: ruleboolean_literal returns [EObject current=null] : ( ( (lv_blname1_0_0= 'true' ) ) | ( (lv_blname1_1_0= 'false' ) ) ) ;
    public final EObject ruleboolean_literal() throws RecognitionException {
        EObject current = null;

        Token lv_blname1_0_0=null;
        Token lv_blname1_1_0=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4701:28: ( ( ( (lv_blname1_0_0= 'true' ) ) | ( (lv_blname1_1_0= 'false' ) ) ) )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4702:1: ( ( (lv_blname1_0_0= 'true' ) ) | ( (lv_blname1_1_0= 'false' ) ) )
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4702:1: ( ( (lv_blname1_0_0= 'true' ) ) | ( (lv_blname1_1_0= 'false' ) ) )
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==79) ) {
                alt47=1;
            }
            else if ( (LA47_0==80) ) {
                alt47=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 47, 0, input);

                throw nvae;
            }
            switch (alt47) {
                case 1 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4702:2: ( (lv_blname1_0_0= 'true' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4702:2: ( (lv_blname1_0_0= 'true' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4703:1: (lv_blname1_0_0= 'true' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4703:1: (lv_blname1_0_0= 'true' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4704:3: lv_blname1_0_0= 'true'
                    {
                    lv_blname1_0_0=(Token)match(input,79,FOLLOW_79_in_ruleboolean_literal9518); 

                            newLeafNode(lv_blname1_0_0, grammarAccess.getBoolean_literalAccess().getBlname1TrueKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBoolean_literalRule());
                    	        }
                           		setWithLastConsumed(current, "blname1", lv_blname1_0_0, "true");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4718:6: ( (lv_blname1_1_0= 'false' ) )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4718:6: ( (lv_blname1_1_0= 'false' ) )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4719:1: (lv_blname1_1_0= 'false' )
                    {
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4719:1: (lv_blname1_1_0= 'false' )
                    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4720:3: lv_blname1_1_0= 'false'
                    {
                    lv_blname1_1_0=(Token)match(input,80,FOLLOW_80_in_ruleboolean_literal9555); 

                            newLeafNode(lv_blname1_1_0, grammarAccess.getBoolean_literalAccess().getBlname1FalseKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBoolean_literalRule());
                    	        }
                           		setWithLastConsumed(current, "blname1", lv_blname1_1_0, "false");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleboolean_literal"


    // $ANTLR start "entryRulebaWS"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4741:1: entryRulebaWS returns [String current=null] : iv_rulebaWS= rulebaWS EOF ;
    public final String entryRulebaWS() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_rulebaWS = null;


        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4742:2: (iv_rulebaWS= rulebaWS EOF )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4743:2: iv_rulebaWS= rulebaWS EOF
            {
             newCompositeNode(grammarAccess.getBaWSRule()); 
            pushFollow(FOLLOW_rulebaWS_in_entryRulebaWS9605);
            iv_rulebaWS=rulebaWS();

            state._fsp--;

             current =iv_rulebaWS.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRulebaWS9616); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulebaWS"


    // $ANTLR start "rulebaWS"
    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4750:1: rulebaWS returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= ' ' )* ;
    public final AntlrDatatypeRuleToken rulebaWS() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4753:28: ( (kw= ' ' )* )
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4754:1: (kw= ' ' )*
            {
            // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4754:1: (kw= ' ' )*
            loop48:
            do {
                int alt48=2;
                int LA48_0 = input.LA(1);

                if ( (LA48_0==81) ) {
                    alt48=1;
                }


                switch (alt48) {
            	case 1 :
            	    // ../org.topcased.adele.xtext.baaction/src-gen/org/topcased/adele/xtext/baaction/parser/antlr/internal/InternalBaAction.g:4755:2: kw= ' '
            	    {
            	    kw=(Token)match(input,81,FOLLOW_81_in_rulebaWS9654); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getBaWSAccess().getSpaceKeyword()); 
            	        

            	    }
            	    break;

            	default :
            	    break loop48;
                }
            } while (true);


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulebaWS"

    // Delegated rules


    protected DFA7 dfa7 = new DFA7(this);
    protected DFA8 dfa8 = new DFA8(this);
    protected DFA17 dfa17 = new DFA17(this);
    protected DFA34 dfa34 = new DFA34(this);
    protected DFA38 dfa38 = new DFA38(this);
    protected DFA39 dfa39 = new DFA39(this);
    static final String DFA7_eotS =
        "\5\uffff";
    static final String DFA7_eofS =
        "\5\uffff";
    static final String DFA7_minS =
        "\2\4\3\uffff";
    static final String DFA7_maxS =
        "\2\121\3\uffff";
    static final String DFA7_acceptS =
        "\2\uffff\1\1\1\2\1\3";
    static final String DFA7_specialS =
        "\5\uffff}>";
    static final String[] DFA7_transitionS = {
            "\1\3\1\2\1\uffff\1\2\12\uffff\1\4\76\uffff\1\1",
            "\1\3\1\2\1\uffff\1\2\12\uffff\1\4\76\uffff\1\1",
            "",
            "",
            ""
    };

    static final short[] DFA7_eot = DFA.unpackEncodedString(DFA7_eotS);
    static final short[] DFA7_eof = DFA.unpackEncodedString(DFA7_eofS);
    static final char[] DFA7_min = DFA.unpackEncodedStringToUnsignedChars(DFA7_minS);
    static final char[] DFA7_max = DFA.unpackEncodedStringToUnsignedChars(DFA7_maxS);
    static final short[] DFA7_accept = DFA.unpackEncodedString(DFA7_acceptS);
    static final short[] DFA7_special = DFA.unpackEncodedString(DFA7_specialS);
    static final short[][] DFA7_transition;

    static {
        int numStates = DFA7_transitionS.length;
        DFA7_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA7_transition[i] = DFA.unpackEncodedString(DFA7_transitionS[i]);
        }
    }

    class DFA7 extends DFA {

        public DFA7(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 7;
            this.eot = DFA7_eot;
            this.eof = DFA7_eof;
            this.min = DFA7_min;
            this.max = DFA7_max;
            this.accept = DFA7_accept;
            this.special = DFA7_special;
            this.transition = DFA7_transition;
        }
        public String getDescription() {
            return "1259:1: ( ( (lv_ev1_0_0= ruleinteger_range ) ) | ( rulebaWS ( (lv_ev2_2_0= RULE_STRING_LITERAL ) ) rulebaWS ) | ( (lv_ev3_4_0= rulearray_data_component_reference ) ) )";
        }
    }
    static final String DFA8_eotS =
        "\4\uffff";
    static final String DFA8_eofS =
        "\4\uffff";
    static final String DFA8_minS =
        "\2\23\2\uffff";
    static final String DFA8_maxS =
        "\2\121\2\uffff";
    static final String DFA8_acceptS =
        "\2\uffff\1\2\1\1";
    static final String DFA8_specialS =
        "\4\uffff}>";
    static final String[] DFA8_transitionS = {
            "\1\2\14\uffff\1\3\60\uffff\1\1",
            "\1\2\14\uffff\1\3\60\uffff\1\1",
            "",
            ""
    };

    static final short[] DFA8_eot = DFA.unpackEncodedString(DFA8_eotS);
    static final short[] DFA8_eof = DFA.unpackEncodedString(DFA8_eofS);
    static final char[] DFA8_min = DFA.unpackEncodedStringToUnsignedChars(DFA8_minS);
    static final char[] DFA8_max = DFA.unpackEncodedStringToUnsignedChars(DFA8_maxS);
    static final short[] DFA8_accept = DFA.unpackEncodedString(DFA8_acceptS);
    static final short[] DFA8_special = DFA.unpackEncodedString(DFA8_specialS);
    static final short[][] DFA8_transition;

    static {
        int numStates = DFA8_transitionS.length;
        DFA8_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA8_transition[i] = DFA.unpackEncodedString(DFA8_transitionS[i]);
        }
    }

    class DFA8 extends DFA {

        public DFA8(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 8;
            this.eot = DFA8_eot;
            this.eof = DFA8_eof;
            this.min = DFA8_min;
            this.max = DFA8_max;
            this.accept = DFA8_accept;
            this.special = DFA8_special;
            this.transition = DFA8_transition;
        }
        public String getDescription() {
            return "()* loopback of 1401:2: ( rulebaWS ( (lv_adcr3_5_0= ',' ) ) rulebaWS ( (lv_adcr4_7_0= ruledata_component_reference ) ) )*";
        }
    }
    static final String DFA17_eotS =
        "\11\uffff";
    static final String DFA17_eofS =
        "\1\uffff\2\7\1\uffff\2\7\2\uffff\1\7";
    static final String DFA17_minS =
        "\6\5\2\uffff\1\5";
    static final String DFA17_maxS =
        "\1\7\2\121\1\7\2\121\2\uffff\1\121";
    static final String DFA17_acceptS =
        "\6\uffff\1\2\1\1\1\uffff";
    static final String DFA17_specialS =
        "\11\uffff}>";
    static final String[] DFA17_transitionS = {
            "\1\2\1\uffff\1\1",
            "\1\5\1\uffff\1\4\7\uffff\1\7\3\uffff\4\7\6\uffff\4\7\12\uffff"+
            "\1\7\1\6\2\uffff\1\3\2\7\2\uffff\7\7\3\uffff\17\7\4\uffff\1"+
            "\7",
            "\1\10\1\uffff\1\4\7\uffff\1\7\3\uffff\4\7\6\uffff\4\7\12\uffff"+
            "\1\7\1\6\2\uffff\1\3\2\7\1\uffff\10\7\3\uffff\17\7\4\uffff\1"+
            "\7",
            "\1\5\1\uffff\1\4",
            "\1\5\1\uffff\1\4\7\uffff\1\7\3\uffff\4\7\6\uffff\4\7\12\uffff"+
            "\1\7\1\6\2\uffff\1\3\2\7\2\uffff\7\7\3\uffff\17\7\4\uffff\1"+
            "\7",
            "\1\5\1\uffff\1\4\7\uffff\1\7\3\uffff\4\7\6\uffff\4\7\12\uffff"+
            "\1\7\1\6\2\uffff\1\3\2\7\2\uffff\7\7\3\uffff\17\7\4\uffff\1"+
            "\7",
            "",
            "",
            "\1\5\1\uffff\1\4\7\uffff\1\7\3\uffff\4\7\6\uffff\4\7\12\uffff"+
            "\1\7\1\6\2\uffff\1\3\2\7\1\uffff\10\7\3\uffff\17\7\4\uffff\1"+
            "\7"
    };

    static final short[] DFA17_eot = DFA.unpackEncodedString(DFA17_eotS);
    static final short[] DFA17_eof = DFA.unpackEncodedString(DFA17_eofS);
    static final char[] DFA17_min = DFA.unpackEncodedStringToUnsignedChars(DFA17_minS);
    static final char[] DFA17_max = DFA.unpackEncodedStringToUnsignedChars(DFA17_maxS);
    static final short[] DFA17_accept = DFA.unpackEncodedString(DFA17_acceptS);
    static final short[] DFA17_special = DFA.unpackEncodedString(DFA17_specialS);
    static final short[][] DFA17_transition;

    static {
        int numStates = DFA17_transitionS.length;
        DFA17_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA17_transition[i] = DFA.unpackEncodedString(DFA17_transitionS[i]);
        }
    }

    class DFA17 extends DFA {

        public DFA17(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 17;
            this.eot = DFA17_eot;
            this.eof = DFA17_eof;
            this.min = DFA17_min;
            this.max = DFA17_max;
            this.accept = DFA17_accept;
            this.special = DFA17_special;
            this.transition = DFA17_transition;
        }
        public String getDescription() {
            return "2403:1: ( ( (lv_nlname1_0_0= ruleinteger_literal ) ) | ( (lv_nlname2_1_0= rulereal_literal ) ) )";
        }
    }
    static final String DFA34_eotS =
        "\4\uffff";
    static final String DFA34_eofS =
        "\4\uffff";
    static final String DFA34_minS =
        "\2\4\2\uffff";
    static final String DFA34_maxS =
        "\2\121\2\uffff";
    static final String DFA34_acceptS =
        "\2\uffff\1\1\1\2";
    static final String DFA34_specialS =
        "\4\uffff}>";
    static final String[] DFA34_transitionS = {
            "\2\3\1\uffff\1\3\51\uffff\1\2\25\uffff\1\2\5\uffff\4\3\1\1",
            "\2\3\1\uffff\1\3\51\uffff\1\2\25\uffff\1\2\5\uffff\4\3\1\1",
            "",
            ""
    };

    static final short[] DFA34_eot = DFA.unpackEncodedString(DFA34_eotS);
    static final short[] DFA34_eof = DFA.unpackEncodedString(DFA34_eofS);
    static final char[] DFA34_min = DFA.unpackEncodedStringToUnsignedChars(DFA34_minS);
    static final char[] DFA34_max = DFA.unpackEncodedStringToUnsignedChars(DFA34_maxS);
    static final short[] DFA34_accept = DFA.unpackEncodedString(DFA34_acceptS);
    static final short[] DFA34_special = DFA.unpackEncodedString(DFA34_specialS);
    static final short[][] DFA34_transition;

    static {
        int numStates = DFA34_transitionS.length;
        DFA34_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA34_transition[i] = DFA.unpackEncodedString(DFA34_transitionS[i]);
        }
    }

    class DFA34 extends DFA {

        public DFA34(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 34;
            this.eot = DFA34_eot;
            this.eof = DFA34_eof;
            this.min = DFA34_min;
            this.max = DFA34_max;
            this.accept = DFA34_accept;
            this.special = DFA34_special;
            this.transition = DFA34_transition;
        }
        public String getDescription() {
            return "3598:2: ( rulebaWS ( (lv_sename2_1_0= ruleunary_adding_operator ) ) )?";
        }
    }
    static final String DFA38_eotS =
        "\5\uffff";
    static final String DFA38_eofS =
        "\5\uffff";
    static final String DFA38_minS =
        "\2\4\3\uffff";
    static final String DFA38_maxS =
        "\2\121\3\uffff";
    static final String DFA38_acceptS =
        "\2\uffff\1\1\1\2\1\3";
    static final String DFA38_specialS =
        "\5\uffff}>";
    static final String[] DFA38_transitionS = {
            "\2\2\1\uffff\1\2\105\uffff\1\3\1\4\2\2\1\1",
            "\2\2\1\uffff\1\2\105\uffff\1\3\1\4\2\2\1\1",
            "",
            "",
            ""
    };

    static final short[] DFA38_eot = DFA.unpackEncodedString(DFA38_eotS);
    static final short[] DFA38_eof = DFA.unpackEncodedString(DFA38_eofS);
    static final char[] DFA38_min = DFA.unpackEncodedStringToUnsignedChars(DFA38_minS);
    static final char[] DFA38_max = DFA.unpackEncodedStringToUnsignedChars(DFA38_maxS);
    static final short[] DFA38_accept = DFA.unpackEncodedString(DFA38_acceptS);
    static final short[] DFA38_special = DFA.unpackEncodedString(DFA38_specialS);
    static final short[][] DFA38_transition;

    static {
        int numStates = DFA38_transitionS.length;
        DFA38_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA38_transition[i] = DFA.unpackEncodedString(DFA38_transitionS[i]);
        }
    }

    class DFA38 extends DFA {

        public DFA38(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 38;
            this.eot = DFA38_eot;
            this.eof = DFA38_eof;
            this.min = DFA38_min;
            this.max = DFA38_max;
            this.accept = DFA38_accept;
            this.special = DFA38_special;
            this.transition = DFA38_transition;
        }
        public String getDescription() {
            return "3774:1: ( ( ( (lv_fname1_0_0= rulevalue ) ) ( ( (lv_fname3_1_0= rulebinary_numeric_operator ) ) ( (lv_fname2_2_0= rulevalue ) ) )? ) | ( rulebaWS ( (lv_fname4_4_0= ruleunary_numeric_operator ) ) ( (lv_fname5_5_0= rulevalue ) ) ) | ( rulebaWS ( (lv_fname6_7_0= ruleunary_boolean_operator ) ) ( (lv_fname7_8_0= rulevalue ) ) ) )";
        }
    }
    static final String DFA39_eotS =
        "\5\uffff";
    static final String DFA39_eofS =
        "\2\uffff\1\4\2\uffff";
    static final String DFA39_minS =
        "\2\4\1\17\2\uffff";
    static final String DFA39_maxS =
        "\3\121\2\uffff";
    static final String DFA39_acceptS =
        "\3\uffff\1\2\1\1";
    static final String DFA39_specialS =
        "\5\uffff}>";
    static final String[] DFA39_transitionS = {
            "\1\2\1\3\1\uffff\1\3\107\uffff\2\3\1\1",
            "\1\2\1\3\1\uffff\1\3\107\uffff\2\3\1\1",
            "\1\4\3\uffff\4\4\6\uffff\4\4\4\uffff\1\4\13\uffff\1\4\11\uffff"+
            "\2\4\1\3\17\4\4\uffff\1\4",
            "",
            ""
    };

    static final short[] DFA39_eot = DFA.unpackEncodedString(DFA39_eotS);
    static final short[] DFA39_eof = DFA.unpackEncodedString(DFA39_eofS);
    static final char[] DFA39_min = DFA.unpackEncodedStringToUnsignedChars(DFA39_minS);
    static final char[] DFA39_max = DFA.unpackEncodedStringToUnsignedChars(DFA39_maxS);
    static final short[] DFA39_accept = DFA.unpackEncodedString(DFA39_acceptS);
    static final short[] DFA39_special = DFA.unpackEncodedString(DFA39_specialS);
    static final short[][] DFA39_transition;

    static {
        int numStates = DFA39_transitionS.length;
        DFA39_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA39_transition[i] = DFA.unpackEncodedString(DFA39_transitionS[i]);
        }
    }

    class DFA39 extends DFA {

        public DFA39(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 39;
            this.eot = DFA39_eot;
            this.eof = DFA39_eof;
            this.min = DFA39_min;
            this.max = DFA39_max;
            this.accept = DFA39_accept;
            this.special = DFA39_special;
            this.transition = DFA39_transition;
        }
        public String getDescription() {
            return "3939:1: ( ( (lv_vname1_0_0= rulevalue_variable ) ) | ( rulebaWS ( (lv_vname2_2_0= rulevalue_constant ) ) rulebaWS ) )";
        }
    }
 

    public static final BitSet FOLLOW_rulebehavior_action_block_in_entryRulebehavior_action_block75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebehavior_action_block85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rulebehavior_action_block128 = new BitSet(new long[]{0x000007001C824010L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebehavior_actions_in_rulebehavior_action_block162 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_rulebehavior_action_block180 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_16_in_rulebehavior_action_block212 = new BitSet(new long[]{0x00000000000000A0L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebehavior_time_in_rulebehavior_action_block246 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebehavior_action_in_entryRulebehavior_action284 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebehavior_action294 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebasic_action_in_rulebehavior_action340 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebehavior_action_block_in_rulebehavior_action367 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rulebehavior_action392 = new BitSet(new long[]{0x0000000000040000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_action421 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_rulebehavior_action438 = new BitSet(new long[]{0x00020000000000B0L,0x000000000003E080L});
    public static final BitSet FOLLOW_rulevalue_expression_in_rulebehavior_action472 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_rulebehavior_action490 = new BitSet(new long[]{0x000007001C824010L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebehavior_actions_in_rulebehavior_action524 = new BitSet(new long[]{0x0000000000700000L});
    public static final BitSet FOLLOW_20_in_rulebehavior_action543 = new BitSet(new long[]{0x0000000000040000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_action572 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_rulebehavior_action589 = new BitSet(new long[]{0x00020000000000B0L,0x000000000003E080L});
    public static final BitSet FOLLOW_rulevalue_expression_in_rulebehavior_action623 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_rulebehavior_action641 = new BitSet(new long[]{0x000007001C824010L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebehavior_actions_in_rulebehavior_action675 = new BitSet(new long[]{0x0000000000700000L});
    public static final BitSet FOLLOW_21_in_rulebehavior_action696 = new BitSet(new long[]{0x000007001C824010L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebehavior_actions_in_rulebehavior_action730 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_rulebehavior_action750 = new BitSet(new long[]{0x0000000000020000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_action779 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_rulebehavior_action796 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_action825 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rulebehavior_action850 = new BitSet(new long[]{0x0000000000040000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_action879 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_rulebehavior_action896 = new BitSet(new long[]{0x0000000000000010L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_action925 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulebehavior_action941 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_rulebehavior_action964 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulebehavior_action994 = new BitSet(new long[]{0x0000000002000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_action1015 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_rulebehavior_action1032 = new BitSet(new long[]{0x00000000000400B0L,0x0000000000020000L});
    public static final BitSet FOLLOW_ruleelement_values_in_rulebehavior_action1066 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_rulebehavior_action1084 = new BitSet(new long[]{0x0000000000004000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_action1113 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_rulebehavior_action1130 = new BitSet(new long[]{0x000007001C824010L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebehavior_actions_in_rulebehavior_action1164 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_rulebehavior_action1182 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rulebehavior_action1221 = new BitSet(new long[]{0x0000000000040000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_action1250 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_rulebehavior_action1267 = new BitSet(new long[]{0x0000000000000010L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_action1296 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulebehavior_action1312 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_rulebehavior_action1335 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulebehavior_action1365 = new BitSet(new long[]{0x0000000002000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_action1386 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_rulebehavior_action1403 = new BitSet(new long[]{0x00000000000400B0L,0x0000000000020000L});
    public static final BitSet FOLLOW_ruleelement_values_in_rulebehavior_action1437 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_rulebehavior_action1455 = new BitSet(new long[]{0x0000000000004000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_action1484 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_rulebehavior_action1501 = new BitSet(new long[]{0x000007001C824010L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebehavior_actions_in_rulebehavior_action1535 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_rulebehavior_action1553 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rulebehavior_action1592 = new BitSet(new long[]{0x0000000000040000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_action1621 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_rulebehavior_action1638 = new BitSet(new long[]{0x00020000000000B0L,0x000000000003E080L});
    public static final BitSet FOLLOW_rulevalue_expression_in_rulebehavior_action1672 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_rulebehavior_action1690 = new BitSet(new long[]{0x0000000000004000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_action1719 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_rulebehavior_action1736 = new BitSet(new long[]{0x000007001C824010L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebehavior_actions_in_rulebehavior_action1770 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_rulebehavior_action1788 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rulebehavior_action1827 = new BitSet(new long[]{0x000007001C824010L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebehavior_actions_in_rulebehavior_action1861 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_29_in_rulebehavior_action1879 = new BitSet(new long[]{0x0000000000040000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_action1908 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_rulebehavior_action1925 = new BitSet(new long[]{0x00020000000000B0L,0x000000000003E080L});
    public static final BitSet FOLLOW_rulevalue_expression_in_rulebehavior_action1959 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_rulebehavior_action1977 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebehavior_actions_in_entryRulebehavior_actions2027 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebehavior_actions2037 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_actions2078 = new BitSet(new long[]{0x000007001C824010L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebehavior_action_in_rulebehavior_actions2098 = new BitSet(new long[]{0x00000000C0000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_30_in_rulebehavior_actions2118 = new BitSet(new long[]{0x000007001C824010L,0x0000000000020000L});
    public static final BitSet FOLLOW_31_in_rulebehavior_actions2155 = new BitSet(new long[]{0x000007001C824010L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebehavior_action_in_rulebehavior_actions2190 = new BitSet(new long[]{0x00000000C0000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_actions2208 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleelement_values_in_entryRuleelement_values2247 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleelement_values2257 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_range_in_ruleelement_values2303 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebaWS_in_ruleelement_values2326 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_ruleelement_values2342 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_ruleelement_values2363 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulearray_data_component_reference_in_ruleelement_values2390 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulearray_data_component_reference_in_entryRulearray_data_component_reference2426 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulearray_data_component_reference2436 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebaWS_in_rulearray_data_component_reference2477 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_rulearray_data_component_reference2494 = new BitSet(new long[]{0x0000000000000010L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulearray_data_component_reference2523 = new BitSet(new long[]{0x0000000000000010L,0x0000000000020000L});
    public static final BitSet FOLLOW_ruledata_component_reference_in_rulearray_data_component_reference2543 = new BitSet(new long[]{0x0000000100080000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulearray_data_component_reference2560 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32_in_rulearray_data_component_reference2577 = new BitSet(new long[]{0x0000000000000010L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulearray_data_component_reference2606 = new BitSet(new long[]{0x0000000000000010L,0x0000000000020000L});
    public static final BitSet FOLLOW_ruledata_component_reference_in_rulearray_data_component_reference2626 = new BitSet(new long[]{0x0000000100080000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulearray_data_component_reference2644 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_rulearray_data_component_reference2661 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulearray_data_component_reference2690 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebasic_action_in_entryRulebasic_action2725 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebasic_action2735 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleassignment_action_in_rulebasic_action2781 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulecommunication_action_in_rulebasic_action2808 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruletimed_action_in_rulebasic_action2835 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleassignment_action_in_entryRuleassignment_action2871 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleassignment_action2881 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_ruleassignment_action2923 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33_in_ruleassignment_action2946 = new BitSet(new long[]{0x00020004000000B0L,0x000000000003E080L});
    public static final BitSet FOLLOW_rulevalue_expression_in_ruleassignment_action2981 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_ruleassignment_action3005 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulecommunication_action_in_entryRulecommunication_action3055 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulecommunication_action3065 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulecommunication_action3108 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_35_in_rulecommunication_action3131 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_18_in_rulecommunication_action3163 = new BitSet(new long[]{0x00020000000000B0L,0x000000000003E080L});
    public static final BitSet FOLLOW_rulesubprogram_parameter_list_in_rulecommunication_action3197 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_rulecommunication_action3215 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulecommunication_action3255 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_rulecommunication_action3278 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulecommunication_action3316 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_rulecommunication_action3339 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_18_in_rulecommunication_action3371 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulecommunication_action3401 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_rulecommunication_action3424 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulecommunication_action3464 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_38_in_rulecommunication_action3487 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulecommunication_action3525 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_39_in_rulecommunication_action3548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_rulecommunication_action3586 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_rulecommunication_action3623 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruletimed_action_in_entryRuletimed_action3672 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuletimed_action3682 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_ruletimed_action3725 = new BitSet(new long[]{0x0000000000040000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_ruletimed_action3754 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruletimed_action3771 = new BitSet(new long[]{0x00000000000000A0L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebehavior_time_in_ruletimed_action3805 = new BitSet(new long[]{0x0000080000080000L});
    public static final BitSet FOLLOW_43_in_ruletimed_action3824 = new BitSet(new long[]{0x00000000000000A0L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebehavior_time_in_ruletimed_action3858 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruletimed_action3878 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulesubprogram_parameter_list_in_entryRulesubprogram_parameter_list3927 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulesubprogram_parameter_list3937 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleparameter_label_in_rulesubprogram_parameter_list3983 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_32_in_rulesubprogram_parameter_list4002 = new BitSet(new long[]{0x00020000000000B0L,0x000000000003E080L});
    public static final BitSet FOLLOW_ruleparameter_label_in_rulesubprogram_parameter_list4036 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_ruleparameter_label_in_entryRuleparameter_label4074 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleparameter_label4084 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_expression_in_ruleparameter_label4129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledata_component_reference_in_entryRuledata_component_reference4164 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledata_component_reference4174 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_ruledata_component_reference4216 = new BitSet(new long[]{0x0000100000000002L});
    public static final BitSet FOLLOW_44_in_ruledata_component_reference4240 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_ruledata_component_reference4270 = new BitSet(new long[]{0x0000100000000002L});
    public static final BitSet FOLLOW_rulearray_index_in_entryRulearray_index4315 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulearray_index4325 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_rulearray_index4368 = new BitSet(new long[]{0x00000000000000A0L});
    public static final BitSet FOLLOW_ruleinteger_literal_in_rulearray_index4402 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_rulearray_index4420 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_value_in_entryRuleinteger_value4469 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleinteger_value4479 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeric_literal_in_ruleinteger_value4524 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeric_literal_in_entryRulenumeric_literal4559 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulenumeric_literal4569 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_literal_in_rulenumeric_literal4615 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulereal_literal_in_rulenumeric_literal4642 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_literal_in_entryRuleinteger_literal4678 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleinteger_literal4688 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledecimal_integer_literal_in_ruleinteger_literal4734 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebased_integer_literal_in_ruleinteger_literal4761 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulereal_literal_in_entryRulereal_literal4797 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulereal_literal4807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledecimal_real_literal_in_rulereal_literal4852 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledecimal_integer_literal_in_entryRuledecimal_integer_literal4887 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledecimal_integer_literal4897 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeral_in_ruledecimal_integer_literal4943 = new BitSet(new long[]{0x0001000000000002L});
    public static final BitSet FOLLOW_rulepositive_exponent_in_ruledecimal_integer_literal4964 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledecimal_real_literal_in_entryRuledecimal_real_literal5001 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledecimal_real_literal5011 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeral_in_ruledecimal_real_literal5057 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_44_in_ruledecimal_real_literal5069 = new BitSet(new long[]{0x00000000000000A0L});
    public static final BitSet FOLLOW_rulenumeral_in_ruledecimal_real_literal5090 = new BitSet(new long[]{0x0005000000000002L});
    public static final BitSet FOLLOW_ruleexponent_in_ruledecimal_real_literal5111 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeral_in_entryRulenumeral5148 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulenumeral5158 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_in_rulenumeral5204 = new BitSet(new long[]{0x00008000000000A2L});
    public static final BitSet FOLLOW_47_in_rulenumeral5218 = new BitSet(new long[]{0x00000000000000A0L});
    public static final BitSet FOLLOW_ruleinteger_in_rulenumeral5241 = new BitSet(new long[]{0x00008000000000A2L});
    public static final BitSet FOLLOW_ruleexponent_in_entryRuleexponent5279 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleexponent5289 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_ruleexponent5327 = new BitSet(new long[]{0x00020000000000A0L});
    public static final BitSet FOLLOW_49_in_ruleexponent5340 = new BitSet(new long[]{0x00000000000000A0L});
    public static final BitSet FOLLOW_rulenumeral_in_ruleexponent5363 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_ruleexponent5383 = new BitSet(new long[]{0x00000000000000A0L});
    public static final BitSet FOLLOW_rulenumeral_in_ruleexponent5404 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulepositive_exponent_in_entryRulepositive_exponent5441 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulepositive_exponent5451 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_rulepositive_exponent5488 = new BitSet(new long[]{0x00020000000000A0L});
    public static final BitSet FOLLOW_49_in_rulepositive_exponent5501 = new BitSet(new long[]{0x00000000000000A0L});
    public static final BitSet FOLLOW_rulenumeral_in_rulepositive_exponent5524 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebased_integer_literal_in_entryRulebased_integer_literal5560 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebased_integer_literal5570 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebase_in_rulebased_integer_literal5616 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_51_in_rulebased_integer_literal5628 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rulebased_numeral_in_rulebased_integer_literal5649 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_51_in_rulebased_integer_literal5661 = new BitSet(new long[]{0x0001000000000002L});
    public static final BitSet FOLLOW_rulepositive_exponent_in_rulebased_integer_literal5682 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebase_in_entryRulebase5719 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebase5729 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DIGIT_in_rulebase5771 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_RULE_DIGIT_in_rulebase5793 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebased_numeral_in_entryRulebased_numeral5835 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebased_numeral5845 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_EXTENDED_DIGIT_in_rulebased_numeral5887 = new BitSet(new long[]{0x0000800000000042L});
    public static final BitSet FOLLOW_47_in_rulebased_numeral5906 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_RULE_EXTENDED_DIGIT_in_rulebased_numeral5925 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebehavior_time_in_entryRulebehavior_time5968 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebehavior_time5978 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_time6019 = new BitSet(new long[]{0x00000000000000A0L,0x0000000000020000L});
    public static final BitSet FOLLOW_ruleinteger_value_in_rulebehavior_time6039 = new BitSet(new long[]{0x07F0000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_time6055 = new BitSet(new long[]{0x07F0000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_ruleunit_identifier_in_rulebehavior_time6075 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulebehavior_time6091 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunit_identifier_in_entryRuleunit_identifier6126 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleunit_identifier6136 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_ruleunit_identifier6179 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_53_in_ruleunit_identifier6216 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_54_in_ruleunit_identifier6253 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_55_in_ruleunit_identifier6290 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_56_in_ruleunit_identifier6327 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_57_in_ruleunit_identifier6364 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_58_in_ruleunit_identifier6401 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_range_in_entryRuleinteger_range6450 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleinteger_range6460 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebaWS_in_ruleinteger_range6501 = new BitSet(new long[]{0x00000000000000A0L,0x0000000000020000L});
    public static final BitSet FOLLOW_ruleinteger_value_in_ruleinteger_range6521 = new BitSet(new long[]{0x0000080000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_ruleinteger_range6537 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_ruleinteger_range6554 = new BitSet(new long[]{0x00000000000000A0L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_ruleinteger_range6583 = new BitSet(new long[]{0x00000000000000A0L,0x0000000000020000L});
    public static final BitSet FOLLOW_ruleinteger_value_in_ruleinteger_range6603 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_ruleinteger_range6619 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinteger_in_entryRuleinteger6654 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleinteger6664 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleinteger6706 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DIGIT_in_ruleinteger6734 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_expression_in_entryRulevalue_expression6775 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulevalue_expression6785 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulerelation_in_rulevalue_expression6831 = new BitSet(new long[]{0xC000000000000002L,0x0000000000000001L});
    public static final BitSet FOLLOW_rulelogical_operator_in_rulevalue_expression6853 = new BitSet(new long[]{0x00020000000000B0L,0x000000000003E080L});
    public static final BitSet FOLLOW_rulerelation_in_rulevalue_expression6874 = new BitSet(new long[]{0xC000000000000002L,0x0000000000000001L});
    public static final BitSet FOLLOW_rulerelation_in_entryRulerelation6912 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulerelation6922 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulesimple_expression_in_rulerelation6968 = new BitSet(new long[]{0x0000000000000002L,0x000000000000007EL});
    public static final BitSet FOLLOW_rulerelational_operator_in_rulerelation6990 = new BitSet(new long[]{0x00020000000000B0L,0x000000000003E080L});
    public static final BitSet FOLLOW_rulesimple_expression_in_rulerelation7011 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulesimple_expression_in_entryRulesimple_expression7049 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulesimple_expression7059 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebaWS_in_rulesimple_expression7101 = new BitSet(new long[]{0x0002000000000000L,0x0000000000020080L});
    public static final BitSet FOLLOW_ruleunary_adding_operator_in_rulesimple_expression7121 = new BitSet(new long[]{0x00020000000000B0L,0x000000000003E080L});
    public static final BitSet FOLLOW_ruleterm_in_rulesimple_expression7144 = new BitSet(new long[]{0x0002000000000002L,0x0000000000000080L});
    public static final BitSet FOLLOW_rulebinary_adding_operator_in_rulesimple_expression7166 = new BitSet(new long[]{0x00020000000000B0L,0x000000000003E080L});
    public static final BitSet FOLLOW_ruleterm_in_rulesimple_expression7187 = new BitSet(new long[]{0x0002000000000002L,0x0000000000000080L});
    public static final BitSet FOLLOW_ruleterm_in_entryRuleterm7225 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleterm7235 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulefactor_in_ruleterm7281 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000F00L});
    public static final BitSet FOLLOW_rulemultiplying_operator_in_ruleterm7303 = new BitSet(new long[]{0x00020000000000B0L,0x000000000003E080L});
    public static final BitSet FOLLOW_rulefactor_in_ruleterm7324 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000F00L});
    public static final BitSet FOLLOW_rulefactor_in_entryRulefactor7362 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulefactor7372 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_in_rulefactor7419 = new BitSet(new long[]{0x0000000000000002L,0x0000000000001000L});
    public static final BitSet FOLLOW_rulebinary_numeric_operator_in_rulefactor7441 = new BitSet(new long[]{0x00000000000000B0L,0x0000000000038000L});
    public static final BitSet FOLLOW_rulevalue_in_rulefactor7462 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebaWS_in_rulefactor7488 = new BitSet(new long[]{0x0000000000000000L,0x0000000000022000L});
    public static final BitSet FOLLOW_ruleunary_numeric_operator_in_rulefactor7508 = new BitSet(new long[]{0x00000000000000B0L,0x0000000000038000L});
    public static final BitSet FOLLOW_rulevalue_in_rulefactor7529 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebaWS_in_rulefactor7553 = new BitSet(new long[]{0x00020000000000B0L,0x000000000003E080L});
    public static final BitSet FOLLOW_ruleunary_boolean_operator_in_rulefactor7573 = new BitSet(new long[]{0x00000000000000B0L,0x0000000000038000L});
    public static final BitSet FOLLOW_rulevalue_in_rulefactor7594 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_in_entryRulevalue7631 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulevalue7641 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_variable_in_rulevalue7687 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebaWS_in_rulevalue7710 = new BitSet(new long[]{0x00000000000000B0L,0x0000000000038000L});
    public static final BitSet FOLLOW_rulevalue_constant_in_rulevalue7730 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulevalue7746 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_variable_in_entryRulevalue_variable7782 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulevalue_variable7792 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebaWS_in_rulevalue_variable7833 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulevalue_variable7849 = new BitSet(new long[]{0x1800002000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulevalue_variable7870 = new BitSet(new long[]{0x1800002000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_37_in_rulevalue_variable7888 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_59_in_rulevalue_variable7925 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_60_in_rulevalue_variable7962 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_rulebaWS_in_rulevalue_variable7993 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulevalue_constant_in_entryRulevalue_constant8028 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulevalue_constant8038 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleboolean_literal_in_rulevalue_constant8084 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulenumeric_literal_in_rulevalue_constant8111 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulevalue_constant8135 = new BitSet(new long[]{0x2000000000000000L});
    public static final BitSet FOLLOW_61_in_rulevalue_constant8152 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_LITERAL_in_rulevalue_constant8169 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulelogical_operator_in_entryRulelogical_operator8211 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulelogical_operator8221 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_62_in_rulelogical_operator8264 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_63_in_rulelogical_operator8301 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_64_in_rulelogical_operator8338 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulerelational_operator_in_entryRulerelational_operator8387 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulerelational_operator8397 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_65_in_rulerelational_operator8440 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_66_in_rulerelational_operator8477 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_67_in_rulerelational_operator8514 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_68_in_rulerelational_operator8551 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_69_in_rulerelational_operator8588 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_70_in_rulerelational_operator8625 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebinary_adding_operator_in_entryRulebinary_adding_operator8674 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebinary_adding_operator8684 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_rulebinary_adding_operator8727 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_71_in_rulebinary_adding_operator8764 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunary_adding_operator_in_entryRuleunary_adding_operator8813 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleunary_adding_operator8823 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_ruleunary_adding_operator8866 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_71_in_ruleunary_adding_operator8903 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulemultiplying_operator_in_entryRulemultiplying_operator8952 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulemultiplying_operator8962 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_72_in_rulemultiplying_operator9005 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_73_in_rulemultiplying_operator9042 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_74_in_rulemultiplying_operator9079 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_75_in_rulemultiplying_operator9116 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebinary_numeric_operator_in_entryRulebinary_numeric_operator9165 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebinary_numeric_operator9175 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_76_in_rulebinary_numeric_operator9217 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunary_numeric_operator_in_entryRuleunary_numeric_operator9265 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleunary_numeric_operator9275 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_77_in_ruleunary_numeric_operator9317 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleunary_boolean_operator_in_entryRuleunary_boolean_operator9365 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleunary_boolean_operator9375 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_78_in_ruleunary_boolean_operator9417 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleboolean_literal_in_entryRuleboolean_literal9465 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleboolean_literal9475 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_79_in_ruleboolean_literal9518 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_80_in_ruleboolean_literal9555 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulebaWS_in_entryRulebaWS9605 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulebaWS9616 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_81_in_rulebaWS9654 = new BitSet(new long[]{0x0000000000000002L,0x0000000000020000L});

}