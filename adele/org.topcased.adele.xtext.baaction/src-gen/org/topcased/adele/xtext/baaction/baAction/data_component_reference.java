/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>data component reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.data_component_reference#getDcr1 <em>Dcr1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.data_component_reference#getDcr2 <em>Dcr2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.data_component_reference#getDcr3 <em>Dcr3</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getdata_component_reference()
 * @model
 * @generated
 */
public interface data_component_reference extends EObject
{
  /**
   * Returns the value of the '<em><b>Dcr1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dcr1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dcr1</em>' attribute.
   * @see #setDcr1(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getdata_component_reference_Dcr1()
   * @model
   * @generated
   */
  String getDcr1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.data_component_reference#getDcr1 <em>Dcr1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dcr1</em>' attribute.
   * @see #getDcr1()
   * @generated
   */
  void setDcr1(String value);

  /**
   * Returns the value of the '<em><b>Dcr2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dcr2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dcr2</em>' attribute list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getdata_component_reference_Dcr2()
   * @model unique="false"
   * @generated
   */
  EList<String> getDcr2();

  /**
   * Returns the value of the '<em><b>Dcr3</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dcr3</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dcr3</em>' attribute list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getdata_component_reference_Dcr3()
   * @model unique="false"
   * @generated
   */
  EList<String> getDcr3();

} // data_component_reference
