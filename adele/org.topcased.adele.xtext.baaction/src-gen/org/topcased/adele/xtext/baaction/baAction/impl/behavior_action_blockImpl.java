/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.behavior_action_block;
import org.topcased.adele.xtext.baaction.baAction.behavior_actions;
import org.topcased.adele.xtext.baaction.baAction.behavior_time;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>behavior action block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_blockImpl#getBac1 <em>Bac1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_blockImpl#getBac2 <em>Bac2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_blockImpl#getBac3 <em>Bac3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_blockImpl#getBac4 <em>Bac4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_blockImpl#getBac5 <em>Bac5</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class behavior_action_blockImpl extends MinimalEObjectImpl.Container implements behavior_action_block
{
  /**
   * The default value of the '{@link #getBac1() <em>Bac1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBac1()
   * @generated
   * @ordered
   */
  protected static final String BAC1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBac1() <em>Bac1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBac1()
   * @generated
   * @ordered
   */
  protected String bac1 = BAC1_EDEFAULT;

  /**
   * The cached value of the '{@link #getBac2() <em>Bac2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBac2()
   * @generated
   * @ordered
   */
  protected behavior_actions bac2;

  /**
   * The default value of the '{@link #getBac3() <em>Bac3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBac3()
   * @generated
   * @ordered
   */
  protected static final String BAC3_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBac3() <em>Bac3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBac3()
   * @generated
   * @ordered
   */
  protected String bac3 = BAC3_EDEFAULT;

  /**
   * The default value of the '{@link #getBac4() <em>Bac4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBac4()
   * @generated
   * @ordered
   */
  protected static final String BAC4_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBac4() <em>Bac4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBac4()
   * @generated
   * @ordered
   */
  protected String bac4 = BAC4_EDEFAULT;

  /**
   * The cached value of the '{@link #getBac5() <em>Bac5</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBac5()
   * @generated
   * @ordered
   */
  protected behavior_time bac5;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected behavior_action_blockImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.BEHAVIOR_ACTION_BLOCK;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBac1()
  {
    return bac1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBac1(String newBac1)
  {
    String oldBac1 = bac1;
    bac1 = newBac1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC1, oldBac1, bac1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_actions getBac2()
  {
    return bac2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBac2(behavior_actions newBac2, NotificationChain msgs)
  {
    behavior_actions oldBac2 = bac2;
    bac2 = newBac2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC2, oldBac2, newBac2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBac2(behavior_actions newBac2)
  {
    if (newBac2 != bac2)
    {
      NotificationChain msgs = null;
      if (bac2 != null)
        msgs = ((InternalEObject)bac2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC2, null, msgs);
      if (newBac2 != null)
        msgs = ((InternalEObject)newBac2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC2, null, msgs);
      msgs = basicSetBac2(newBac2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC2, newBac2, newBac2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBac3()
  {
    return bac3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBac3(String newBac3)
  {
    String oldBac3 = bac3;
    bac3 = newBac3;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC3, oldBac3, bac3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBac4()
  {
    return bac4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBac4(String newBac4)
  {
    String oldBac4 = bac4;
    bac4 = newBac4;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC4, oldBac4, bac4));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_time getBac5()
  {
    return bac5;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBac5(behavior_time newBac5, NotificationChain msgs)
  {
    behavior_time oldBac5 = bac5;
    bac5 = newBac5;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC5, oldBac5, newBac5);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBac5(behavior_time newBac5)
  {
    if (newBac5 != bac5)
    {
      NotificationChain msgs = null;
      if (bac5 != null)
        msgs = ((InternalEObject)bac5).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC5, null, msgs);
      if (newBac5 != null)
        msgs = ((InternalEObject)newBac5).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC5, null, msgs);
      msgs = basicSetBac5(newBac5, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC5, newBac5, newBac5));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC2:
        return basicSetBac2(null, msgs);
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC5:
        return basicSetBac5(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC1:
        return getBac1();
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC2:
        return getBac2();
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC3:
        return getBac3();
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC4:
        return getBac4();
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC5:
        return getBac5();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC1:
        setBac1((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC2:
        setBac2((behavior_actions)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC3:
        setBac3((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC4:
        setBac4((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC5:
        setBac5((behavior_time)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC1:
        setBac1(BAC1_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC2:
        setBac2((behavior_actions)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC3:
        setBac3(BAC3_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC4:
        setBac4(BAC4_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC5:
        setBac5((behavior_time)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC1:
        return BAC1_EDEFAULT == null ? bac1 != null : !BAC1_EDEFAULT.equals(bac1);
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC2:
        return bac2 != null;
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC3:
        return BAC3_EDEFAULT == null ? bac3 != null : !BAC3_EDEFAULT.equals(bac3);
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC4:
        return BAC4_EDEFAULT == null ? bac4 != null : !BAC4_EDEFAULT.equals(bac4);
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK__BAC5:
        return bac5 != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (bac1: ");
    result.append(bac1);
    result.append(", bac3: ");
    result.append(bac3);
    result.append(", bac4: ");
    result.append(bac4);
    result.append(')');
    return result.toString();
  }

} //behavior_action_blockImpl
