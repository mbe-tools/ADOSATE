/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.based_integer_literal;
import org.topcased.adele.xtext.baaction.baAction.decimal_integer_literal;
import org.topcased.adele.xtext.baaction.baAction.integer_literal;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>integer literal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.integer_literalImpl#getIlname1 <em>Ilname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.integer_literalImpl#getIlname2 <em>Ilname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class integer_literalImpl extends MinimalEObjectImpl.Container implements integer_literal
{
  /**
   * The cached value of the '{@link #getIlname1() <em>Ilname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIlname1()
   * @generated
   * @ordered
   */
  protected decimal_integer_literal ilname1;

  /**
   * The cached value of the '{@link #getIlname2() <em>Ilname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIlname2()
   * @generated
   * @ordered
   */
  protected based_integer_literal ilname2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected integer_literalImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.INTEGER_LITERAL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public decimal_integer_literal getIlname1()
  {
    return ilname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIlname1(decimal_integer_literal newIlname1, NotificationChain msgs)
  {
    decimal_integer_literal oldIlname1 = ilname1;
    ilname1 = newIlname1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.INTEGER_LITERAL__ILNAME1, oldIlname1, newIlname1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIlname1(decimal_integer_literal newIlname1)
  {
    if (newIlname1 != ilname1)
    {
      NotificationChain msgs = null;
      if (ilname1 != null)
        msgs = ((InternalEObject)ilname1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.INTEGER_LITERAL__ILNAME1, null, msgs);
      if (newIlname1 != null)
        msgs = ((InternalEObject)newIlname1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.INTEGER_LITERAL__ILNAME1, null, msgs);
      msgs = basicSetIlname1(newIlname1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.INTEGER_LITERAL__ILNAME1, newIlname1, newIlname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public based_integer_literal getIlname2()
  {
    return ilname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIlname2(based_integer_literal newIlname2, NotificationChain msgs)
  {
    based_integer_literal oldIlname2 = ilname2;
    ilname2 = newIlname2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.INTEGER_LITERAL__ILNAME2, oldIlname2, newIlname2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIlname2(based_integer_literal newIlname2)
  {
    if (newIlname2 != ilname2)
    {
      NotificationChain msgs = null;
      if (ilname2 != null)
        msgs = ((InternalEObject)ilname2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.INTEGER_LITERAL__ILNAME2, null, msgs);
      if (newIlname2 != null)
        msgs = ((InternalEObject)newIlname2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.INTEGER_LITERAL__ILNAME2, null, msgs);
      msgs = basicSetIlname2(newIlname2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.INTEGER_LITERAL__ILNAME2, newIlname2, newIlname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.INTEGER_LITERAL__ILNAME1:
        return basicSetIlname1(null, msgs);
      case BaActionPackage.INTEGER_LITERAL__ILNAME2:
        return basicSetIlname2(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.INTEGER_LITERAL__ILNAME1:
        return getIlname1();
      case BaActionPackage.INTEGER_LITERAL__ILNAME2:
        return getIlname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.INTEGER_LITERAL__ILNAME1:
        setIlname1((decimal_integer_literal)newValue);
        return;
      case BaActionPackage.INTEGER_LITERAL__ILNAME2:
        setIlname2((based_integer_literal)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.INTEGER_LITERAL__ILNAME1:
        setIlname1((decimal_integer_literal)null);
        return;
      case BaActionPackage.INTEGER_LITERAL__ILNAME2:
        setIlname2((based_integer_literal)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.INTEGER_LITERAL__ILNAME1:
        return ilname1 != null;
      case BaActionPackage.INTEGER_LITERAL__ILNAME2:
        return ilname2 != null;
    }
    return super.eIsSet(featureID);
  }

} //integer_literalImpl
