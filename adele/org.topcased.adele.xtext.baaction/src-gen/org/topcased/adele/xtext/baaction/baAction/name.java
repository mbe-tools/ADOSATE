/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>name</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.name#getN1 <em>N1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.name#getN2 <em>N2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getname()
 * @model
 * @generated
 */
public interface name extends EObject
{
  /**
   * Returns the value of the '<em><b>N1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>N1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>N1</em>' attribute.
   * @see #setN1(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getname_N1()
   * @model
   * @generated
   */
  String getN1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.name#getN1 <em>N1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>N1</em>' attribute.
   * @see #getN1()
   * @generated
   */
  void setN1(String value);

  /**
   * Returns the value of the '<em><b>N2</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baaction.baAction.array_index}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>N2</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>N2</em>' containment reference list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getname_N2()
   * @model containment="true"
   * @generated
   */
  EList<array_index> getN2();

} // name
