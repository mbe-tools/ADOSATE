/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>behavior action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa1 <em>Ba1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa2 <em>Ba2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa3 <em>Ba3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa3bis <em>Ba3bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa4 <em>Ba4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa5 <em>Ba5</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa6 <em>Ba6</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa7 <em>Ba7</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa7bis <em>Ba7bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa8 <em>Ba8</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa9 <em>Ba9</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa10 <em>Ba10</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa11 <em>Ba11</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa12 <em>Ba12</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa13 <em>Ba13</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa13bis <em>Ba13bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa14 <em>Ba14</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa14bis <em>Ba14bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa15 <em>Ba15</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa16 <em>Ba16</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa17 <em>Ba17</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa18 <em>Ba18</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa19 <em>Ba19</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa20 <em>Ba20</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa20bis <em>Ba20bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa21 <em>Ba21</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa22 <em>Ba22</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa23 <em>Ba23</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa23bis <em>Ba23bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa24 <em>Ba24</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa25 <em>Ba25</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa26 <em>Ba26</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa27 <em>Ba27</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa29 <em>Ba29</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa30 <em>Ba30</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa30bis <em>Ba30bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa31 <em>Ba31</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa32 <em>Ba32</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa33 <em>Ba33</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa33bis <em>Ba33bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa34 <em>Ba34</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa35 <em>Ba35</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa35bis <em>Ba35bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa36 <em>Ba36</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa37 <em>Ba37</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa38 <em>Ba38</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa39 <em>Ba39</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa40 <em>Ba40</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa40bis <em>Ba40bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa41 <em>Ba41</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa42 <em>Ba42</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action()
 * @model
 * @generated
 */
public interface behavior_action extends EObject
{
  /**
   * Returns the value of the '<em><b>Ba1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba1</em>' containment reference.
   * @see #setBa1(basic_action)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba1()
   * @model containment="true"
   * @generated
   */
  basic_action getBa1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa1 <em>Ba1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba1</em>' containment reference.
   * @see #getBa1()
   * @generated
   */
  void setBa1(basic_action value);

  /**
   * Returns the value of the '<em><b>Ba2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba2</em>' containment reference.
   * @see #setBa2(behavior_action_block)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba2()
   * @model containment="true"
   * @generated
   */
  behavior_action_block getBa2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa2 <em>Ba2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba2</em>' containment reference.
   * @see #getBa2()
   * @generated
   */
  void setBa2(behavior_action_block value);

  /**
   * Returns the value of the '<em><b>Ba3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba3</em>' attribute.
   * @see #setBa3(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba3()
   * @model
   * @generated
   */
  String getBa3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa3 <em>Ba3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba3</em>' attribute.
   * @see #getBa3()
   * @generated
   */
  void setBa3(String value);

  /**
   * Returns the value of the '<em><b>Ba3bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba3bis</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba3bis</em>' attribute.
   * @see #setBa3bis(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba3bis()
   * @model
   * @generated
   */
  String getBa3bis();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa3bis <em>Ba3bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba3bis</em>' attribute.
   * @see #getBa3bis()
   * @generated
   */
  void setBa3bis(String value);

  /**
   * Returns the value of the '<em><b>Ba4</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba4</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba4</em>' containment reference.
   * @see #setBa4(value_expression)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba4()
   * @model containment="true"
   * @generated
   */
  value_expression getBa4();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa4 <em>Ba4</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba4</em>' containment reference.
   * @see #getBa4()
   * @generated
   */
  void setBa4(value_expression value);

  /**
   * Returns the value of the '<em><b>Ba5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba5</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba5</em>' attribute.
   * @see #setBa5(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba5()
   * @model
   * @generated
   */
  String getBa5();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa5 <em>Ba5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba5</em>' attribute.
   * @see #getBa5()
   * @generated
   */
  void setBa5(String value);

  /**
   * Returns the value of the '<em><b>Ba6</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba6</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba6</em>' containment reference.
   * @see #setBa6(behavior_actions)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba6()
   * @model containment="true"
   * @generated
   */
  behavior_actions getBa6();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa6 <em>Ba6</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba6</em>' containment reference.
   * @see #getBa6()
   * @generated
   */
  void setBa6(behavior_actions value);

  /**
   * Returns the value of the '<em><b>Ba7</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba7</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba7</em>' attribute list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba7()
   * @model unique="false"
   * @generated
   */
  EList<String> getBa7();

  /**
   * Returns the value of the '<em><b>Ba7bis</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba7bis</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba7bis</em>' attribute list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba7bis()
   * @model unique="false"
   * @generated
   */
  EList<String> getBa7bis();

  /**
   * Returns the value of the '<em><b>Ba8</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baaction.baAction.value_expression}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba8</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba8</em>' containment reference list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba8()
   * @model containment="true"
   * @generated
   */
  EList<value_expression> getBa8();

  /**
   * Returns the value of the '<em><b>Ba9</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba9</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba9</em>' attribute list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba9()
   * @model unique="false"
   * @generated
   */
  EList<String> getBa9();

  /**
   * Returns the value of the '<em><b>Ba10</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baaction.baAction.behavior_actions}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba10</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba10</em>' containment reference list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba10()
   * @model containment="true"
   * @generated
   */
  EList<behavior_actions> getBa10();

  /**
   * Returns the value of the '<em><b>Ba11</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba11</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba11</em>' attribute.
   * @see #setBa11(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba11()
   * @model
   * @generated
   */
  String getBa11();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa11 <em>Ba11</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba11</em>' attribute.
   * @see #getBa11()
   * @generated
   */
  void setBa11(String value);

  /**
   * Returns the value of the '<em><b>Ba12</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba12</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba12</em>' containment reference.
   * @see #setBa12(behavior_actions)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba12()
   * @model containment="true"
   * @generated
   */
  behavior_actions getBa12();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa12 <em>Ba12</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba12</em>' containment reference.
   * @see #getBa12()
   * @generated
   */
  void setBa12(behavior_actions value);

  /**
   * Returns the value of the '<em><b>Ba13</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba13</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba13</em>' attribute.
   * @see #setBa13(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba13()
   * @model
   * @generated
   */
  String getBa13();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa13 <em>Ba13</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba13</em>' attribute.
   * @see #getBa13()
   * @generated
   */
  void setBa13(String value);

  /**
   * Returns the value of the '<em><b>Ba13bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba13bis</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba13bis</em>' attribute.
   * @see #setBa13bis(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba13bis()
   * @model
   * @generated
   */
  String getBa13bis();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa13bis <em>Ba13bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba13bis</em>' attribute.
   * @see #getBa13bis()
   * @generated
   */
  void setBa13bis(String value);

  /**
   * Returns the value of the '<em><b>Ba14</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba14</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba14</em>' attribute.
   * @see #setBa14(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba14()
   * @model
   * @generated
   */
  String getBa14();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa14 <em>Ba14</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba14</em>' attribute.
   * @see #getBa14()
   * @generated
   */
  void setBa14(String value);

  /**
   * Returns the value of the '<em><b>Ba14bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba14bis</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba14bis</em>' attribute.
   * @see #setBa14bis(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba14bis()
   * @model
   * @generated
   */
  String getBa14bis();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa14bis <em>Ba14bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba14bis</em>' attribute.
   * @see #getBa14bis()
   * @generated
   */
  void setBa14bis(String value);

  /**
   * Returns the value of the '<em><b>Ba15</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba15</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba15</em>' attribute.
   * @see #setBa15(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba15()
   * @model
   * @generated
   */
  String getBa15();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa15 <em>Ba15</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba15</em>' attribute.
   * @see #getBa15()
   * @generated
   */
  void setBa15(String value);

  /**
   * Returns the value of the '<em><b>Ba16</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba16</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba16</em>' attribute.
   * @see #setBa16(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba16()
   * @model
   * @generated
   */
  String getBa16();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa16 <em>Ba16</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba16</em>' attribute.
   * @see #getBa16()
   * @generated
   */
  void setBa16(String value);

  /**
   * Returns the value of the '<em><b>Ba17</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba17</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba17</em>' attribute.
   * @see #setBa17(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba17()
   * @model
   * @generated
   */
  String getBa17();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa17 <em>Ba17</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba17</em>' attribute.
   * @see #getBa17()
   * @generated
   */
  void setBa17(String value);

  /**
   * Returns the value of the '<em><b>Ba18</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba18</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba18</em>' attribute.
   * @see #setBa18(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba18()
   * @model
   * @generated
   */
  String getBa18();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa18 <em>Ba18</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba18</em>' attribute.
   * @see #getBa18()
   * @generated
   */
  void setBa18(String value);

  /**
   * Returns the value of the '<em><b>Ba19</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba19</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba19</em>' containment reference.
   * @see #setBa19(element_values)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba19()
   * @model containment="true"
   * @generated
   */
  element_values getBa19();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa19 <em>Ba19</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba19</em>' containment reference.
   * @see #getBa19()
   * @generated
   */
  void setBa19(element_values value);

  /**
   * Returns the value of the '<em><b>Ba20</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba20</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba20</em>' attribute.
   * @see #setBa20(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba20()
   * @model
   * @generated
   */
  String getBa20();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa20 <em>Ba20</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba20</em>' attribute.
   * @see #getBa20()
   * @generated
   */
  void setBa20(String value);

  /**
   * Returns the value of the '<em><b>Ba20bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba20bis</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba20bis</em>' attribute.
   * @see #setBa20bis(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba20bis()
   * @model
   * @generated
   */
  String getBa20bis();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa20bis <em>Ba20bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba20bis</em>' attribute.
   * @see #getBa20bis()
   * @generated
   */
  void setBa20bis(String value);

  /**
   * Returns the value of the '<em><b>Ba21</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba21</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba21</em>' containment reference.
   * @see #setBa21(behavior_actions)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba21()
   * @model containment="true"
   * @generated
   */
  behavior_actions getBa21();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa21 <em>Ba21</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba21</em>' containment reference.
   * @see #getBa21()
   * @generated
   */
  void setBa21(behavior_actions value);

  /**
   * Returns the value of the '<em><b>Ba22</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba22</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba22</em>' attribute.
   * @see #setBa22(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba22()
   * @model
   * @generated
   */
  String getBa22();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa22 <em>Ba22</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba22</em>' attribute.
   * @see #getBa22()
   * @generated
   */
  void setBa22(String value);

  /**
   * Returns the value of the '<em><b>Ba23</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba23</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba23</em>' attribute.
   * @see #setBa23(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba23()
   * @model
   * @generated
   */
  String getBa23();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa23 <em>Ba23</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba23</em>' attribute.
   * @see #getBa23()
   * @generated
   */
  void setBa23(String value);

  /**
   * Returns the value of the '<em><b>Ba23bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba23bis</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba23bis</em>' attribute.
   * @see #setBa23bis(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba23bis()
   * @model
   * @generated
   */
  String getBa23bis();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa23bis <em>Ba23bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba23bis</em>' attribute.
   * @see #getBa23bis()
   * @generated
   */
  void setBa23bis(String value);

  /**
   * Returns the value of the '<em><b>Ba24</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba24</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba24</em>' attribute.
   * @see #setBa24(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba24()
   * @model
   * @generated
   */
  String getBa24();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa24 <em>Ba24</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba24</em>' attribute.
   * @see #getBa24()
   * @generated
   */
  void setBa24(String value);

  /**
   * Returns the value of the '<em><b>Ba25</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba25</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba25</em>' attribute.
   * @see #setBa25(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba25()
   * @model
   * @generated
   */
  String getBa25();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa25 <em>Ba25</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba25</em>' attribute.
   * @see #getBa25()
   * @generated
   */
  void setBa25(String value);

  /**
   * Returns the value of the '<em><b>Ba26</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba26</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba26</em>' attribute.
   * @see #setBa26(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba26()
   * @model
   * @generated
   */
  String getBa26();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa26 <em>Ba26</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba26</em>' attribute.
   * @see #getBa26()
   * @generated
   */
  void setBa26(String value);

  /**
   * Returns the value of the '<em><b>Ba27</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba27</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba27</em>' attribute.
   * @see #setBa27(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba27()
   * @model
   * @generated
   */
  String getBa27();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa27 <em>Ba27</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba27</em>' attribute.
   * @see #getBa27()
   * @generated
   */
  void setBa27(String value);

  /**
   * Returns the value of the '<em><b>Ba29</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba29</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba29</em>' containment reference.
   * @see #setBa29(element_values)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba29()
   * @model containment="true"
   * @generated
   */
  element_values getBa29();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa29 <em>Ba29</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba29</em>' containment reference.
   * @see #getBa29()
   * @generated
   */
  void setBa29(element_values value);

  /**
   * Returns the value of the '<em><b>Ba30</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba30</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba30</em>' attribute.
   * @see #setBa30(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba30()
   * @model
   * @generated
   */
  String getBa30();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa30 <em>Ba30</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba30</em>' attribute.
   * @see #getBa30()
   * @generated
   */
  void setBa30(String value);

  /**
   * Returns the value of the '<em><b>Ba30bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba30bis</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba30bis</em>' attribute.
   * @see #setBa30bis(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba30bis()
   * @model
   * @generated
   */
  String getBa30bis();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa30bis <em>Ba30bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba30bis</em>' attribute.
   * @see #getBa30bis()
   * @generated
   */
  void setBa30bis(String value);

  /**
   * Returns the value of the '<em><b>Ba31</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba31</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba31</em>' containment reference.
   * @see #setBa31(behavior_actions)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba31()
   * @model containment="true"
   * @generated
   */
  behavior_actions getBa31();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa31 <em>Ba31</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba31</em>' containment reference.
   * @see #getBa31()
   * @generated
   */
  void setBa31(behavior_actions value);

  /**
   * Returns the value of the '<em><b>Ba32</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba32</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba32</em>' attribute.
   * @see #setBa32(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba32()
   * @model
   * @generated
   */
  String getBa32();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa32 <em>Ba32</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba32</em>' attribute.
   * @see #getBa32()
   * @generated
   */
  void setBa32(String value);

  /**
   * Returns the value of the '<em><b>Ba33</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba33</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba33</em>' attribute.
   * @see #setBa33(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba33()
   * @model
   * @generated
   */
  String getBa33();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa33 <em>Ba33</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba33</em>' attribute.
   * @see #getBa33()
   * @generated
   */
  void setBa33(String value);

  /**
   * Returns the value of the '<em><b>Ba33bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba33bis</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba33bis</em>' attribute.
   * @see #setBa33bis(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba33bis()
   * @model
   * @generated
   */
  String getBa33bis();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa33bis <em>Ba33bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba33bis</em>' attribute.
   * @see #getBa33bis()
   * @generated
   */
  void setBa33bis(String value);

  /**
   * Returns the value of the '<em><b>Ba34</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba34</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba34</em>' containment reference.
   * @see #setBa34(value_expression)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba34()
   * @model containment="true"
   * @generated
   */
  value_expression getBa34();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa34 <em>Ba34</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba34</em>' containment reference.
   * @see #getBa34()
   * @generated
   */
  void setBa34(value_expression value);

  /**
   * Returns the value of the '<em><b>Ba35</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba35</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba35</em>' attribute.
   * @see #setBa35(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba35()
   * @model
   * @generated
   */
  String getBa35();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa35 <em>Ba35</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba35</em>' attribute.
   * @see #getBa35()
   * @generated
   */
  void setBa35(String value);

  /**
   * Returns the value of the '<em><b>Ba35bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba35bis</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba35bis</em>' attribute.
   * @see #setBa35bis(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba35bis()
   * @model
   * @generated
   */
  String getBa35bis();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa35bis <em>Ba35bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba35bis</em>' attribute.
   * @see #getBa35bis()
   * @generated
   */
  void setBa35bis(String value);

  /**
   * Returns the value of the '<em><b>Ba36</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba36</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba36</em>' containment reference.
   * @see #setBa36(behavior_actions)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba36()
   * @model containment="true"
   * @generated
   */
  behavior_actions getBa36();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa36 <em>Ba36</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba36</em>' containment reference.
   * @see #getBa36()
   * @generated
   */
  void setBa36(behavior_actions value);

  /**
   * Returns the value of the '<em><b>Ba37</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba37</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba37</em>' attribute.
   * @see #setBa37(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba37()
   * @model
   * @generated
   */
  String getBa37();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa37 <em>Ba37</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba37</em>' attribute.
   * @see #getBa37()
   * @generated
   */
  void setBa37(String value);

  /**
   * Returns the value of the '<em><b>Ba38</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba38</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba38</em>' attribute.
   * @see #setBa38(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba38()
   * @model
   * @generated
   */
  String getBa38();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa38 <em>Ba38</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba38</em>' attribute.
   * @see #getBa38()
   * @generated
   */
  void setBa38(String value);

  /**
   * Returns the value of the '<em><b>Ba39</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba39</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba39</em>' containment reference.
   * @see #setBa39(behavior_actions)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba39()
   * @model containment="true"
   * @generated
   */
  behavior_actions getBa39();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa39 <em>Ba39</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba39</em>' containment reference.
   * @see #getBa39()
   * @generated
   */
  void setBa39(behavior_actions value);

  /**
   * Returns the value of the '<em><b>Ba40</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba40</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba40</em>' attribute.
   * @see #setBa40(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba40()
   * @model
   * @generated
   */
  String getBa40();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa40 <em>Ba40</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba40</em>' attribute.
   * @see #getBa40()
   * @generated
   */
  void setBa40(String value);

  /**
   * Returns the value of the '<em><b>Ba40bis</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba40bis</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba40bis</em>' attribute.
   * @see #setBa40bis(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba40bis()
   * @model
   * @generated
   */
  String getBa40bis();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa40bis <em>Ba40bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba40bis</em>' attribute.
   * @see #getBa40bis()
   * @generated
   */
  void setBa40bis(String value);

  /**
   * Returns the value of the '<em><b>Ba41</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba41</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba41</em>' containment reference.
   * @see #setBa41(value_expression)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba41()
   * @model containment="true"
   * @generated
   */
  value_expression getBa41();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa41 <em>Ba41</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba41</em>' containment reference.
   * @see #getBa41()
   * @generated
   */
  void setBa41(value_expression value);

  /**
   * Returns the value of the '<em><b>Ba42</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ba42</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ba42</em>' attribute.
   * @see #setBa42(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_Ba42()
   * @model
   * @generated
   */
  String getBa42();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action#getBa42 <em>Ba42</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ba42</em>' attribute.
   * @see #getBa42()
   * @generated
   */
  void setBa42(String value);

} // behavior_action
