/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.array_index;
import org.topcased.adele.xtext.baaction.baAction.name;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>name</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.nameImpl#getN1 <em>N1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.nameImpl#getN2 <em>N2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class nameImpl extends MinimalEObjectImpl.Container implements name
{
  /**
   * The default value of the '{@link #getN1() <em>N1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getN1()
   * @generated
   * @ordered
   */
  protected static final String N1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getN1() <em>N1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getN1()
   * @generated
   * @ordered
   */
  protected String n1 = N1_EDEFAULT;

  /**
   * The cached value of the '{@link #getN2() <em>N2</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getN2()
   * @generated
   * @ordered
   */
  protected EList<array_index> n2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected nameImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.NAME;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getN1()
  {
    return n1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setN1(String newN1)
  {
    String oldN1 = n1;
    n1 = newN1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.NAME__N1, oldN1, n1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<array_index> getN2()
  {
    if (n2 == null)
    {
      n2 = new EObjectContainmentEList<array_index>(array_index.class, this, BaActionPackage.NAME__N2);
    }
    return n2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.NAME__N2:
        return ((InternalEList<?>)getN2()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.NAME__N1:
        return getN1();
      case BaActionPackage.NAME__N2:
        return getN2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.NAME__N1:
        setN1((String)newValue);
        return;
      case BaActionPackage.NAME__N2:
        getN2().clear();
        getN2().addAll((Collection<? extends array_index>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.NAME__N1:
        setN1(N1_EDEFAULT);
        return;
      case BaActionPackage.NAME__N2:
        getN2().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.NAME__N1:
        return N1_EDEFAULT == null ? n1 != null : !N1_EDEFAULT.equals(n1);
      case BaActionPackage.NAME__N2:
        return n2 != null && !n2.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (n1: ");
    result.append(n1);
    result.append(')');
    return result.toString();
  }

} //nameImpl
