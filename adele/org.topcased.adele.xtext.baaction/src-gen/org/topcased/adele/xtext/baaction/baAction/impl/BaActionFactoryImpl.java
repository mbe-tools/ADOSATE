/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.topcased.adele.xtext.baaction.baAction.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BaActionFactoryImpl extends EFactoryImpl implements BaActionFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static BaActionFactory init()
  {
    try
    {
      BaActionFactory theBaActionFactory = (BaActionFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.topcased.org/adele/xtext/baaction/BaAction"); 
      if (theBaActionFactory != null)
      {
        return theBaActionFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new BaActionFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaActionFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK: return createbehavior_action_block();
      case BaActionPackage.BEHAVIOR_ACTION: return createbehavior_action();
      case BaActionPackage.BEHAVIOR_ACTIONS: return createbehavior_actions();
      case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE: return createbehavior_action_sequence();
      case BaActionPackage.BEHAVIOR_ACTION_SET: return createbehavior_action_set();
      case BaActionPackage.ELEMENT_VALUES: return createelement_values();
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE: return createarray_data_component_reference();
      case BaActionPackage.BASIC_ACTION: return createbasic_action();
      case BaActionPackage.ASSIGNMENT_ACTION: return createassignment_action();
      case BaActionPackage.COMMUNICATION_ACTION: return createcommunication_action();
      case BaActionPackage.TIMED_ACTION: return createtimed_action();
      case BaActionPackage.SUBPROGRAM_PARAMETER_LIST: return createsubprogram_parameter_list();
      case BaActionPackage.PARAMETER_LABEL: return createparameter_label();
      case BaActionPackage.DATA_COMPONENT_REFERENCE: return createdata_component_reference();
      case BaActionPackage.NAME: return createname();
      case BaActionPackage.ARRAY_INDEX: return createarray_index();
      case BaActionPackage.INTEGER_VALUE: return createinteger_value();
      case BaActionPackage.NUMERIC_LITERAL: return createnumeric_literal();
      case BaActionPackage.INTEGER_LITERAL: return createinteger_literal();
      case BaActionPackage.REAL_LITERAL: return createreal_literal();
      case BaActionPackage.DECIMAL_INTEGER_LITERAL: return createdecimal_integer_literal();
      case BaActionPackage.DECIMAL_REAL_LITERAL: return createdecimal_real_literal();
      case BaActionPackage.NUMERAL: return createnumeral();
      case BaActionPackage.EXPONENT: return createexponent();
      case BaActionPackage.POSITIVE_EXPONENT: return createpositive_exponent();
      case BaActionPackage.BASED_INTEGER_LITERAL: return createbased_integer_literal();
      case BaActionPackage.BASE: return createbase();
      case BaActionPackage.BASED_NUMERAL: return createbased_numeral();
      case BaActionPackage.BEHAVIOR_TIME: return createbehavior_time();
      case BaActionPackage.UNIT_IDENTIFIER: return createunit_identifier();
      case BaActionPackage.INTEGER_RANGE: return createinteger_range();
      case BaActionPackage.INTEGER: return createinteger();
      case BaActionPackage.VALUE_EXPRESSION: return createvalue_expression();
      case BaActionPackage.RELATION: return createrelation();
      case BaActionPackage.SIMPLE_EXPRESSION: return createsimple_expression();
      case BaActionPackage.TERM: return createterm();
      case BaActionPackage.FACTOR: return createfactor();
      case BaActionPackage.VALUE: return createvalue();
      case BaActionPackage.VALUE_VARIABLE: return createvalue_variable();
      case BaActionPackage.VALUE_CONSTANT: return createvalue_constant();
      case BaActionPackage.LOGICAL_OPERATOR: return createlogical_operator();
      case BaActionPackage.RELATIONAL_OPERATOR: return createrelational_operator();
      case BaActionPackage.BINARY_ADDING_OPERATOR: return createbinary_adding_operator();
      case BaActionPackage.UNARY_ADDING_OPERATOR: return createunary_adding_operator();
      case BaActionPackage.MULTIPLYING_OPERATOR: return createmultiplying_operator();
      case BaActionPackage.BINARY_NUMERIC_OPERATOR: return createbinary_numeric_operator();
      case BaActionPackage.UNARY_NUMERIC_OPERATOR: return createunary_numeric_operator();
      case BaActionPackage.UNARY_BOOLEAN_OPERATOR: return createunary_boolean_operator();
      case BaActionPackage.BOOLEAN_LITERAL: return createboolean_literal();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_action_block createbehavior_action_block()
  {
    behavior_action_blockImpl behavior_action_block = new behavior_action_blockImpl();
    return behavior_action_block;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_action createbehavior_action()
  {
    behavior_actionImpl behavior_action = new behavior_actionImpl();
    return behavior_action;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_actions createbehavior_actions()
  {
    behavior_actionsImpl behavior_actions = new behavior_actionsImpl();
    return behavior_actions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_action_sequence createbehavior_action_sequence()
  {
    behavior_action_sequenceImpl behavior_action_sequence = new behavior_action_sequenceImpl();
    return behavior_action_sequence;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_action_set createbehavior_action_set()
  {
    behavior_action_setImpl behavior_action_set = new behavior_action_setImpl();
    return behavior_action_set;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public element_values createelement_values()
  {
    element_valuesImpl element_values = new element_valuesImpl();
    return element_values;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public array_data_component_reference createarray_data_component_reference()
  {
    array_data_component_referenceImpl array_data_component_reference = new array_data_component_referenceImpl();
    return array_data_component_reference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public basic_action createbasic_action()
  {
    basic_actionImpl basic_action = new basic_actionImpl();
    return basic_action;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public assignment_action createassignment_action()
  {
    assignment_actionImpl assignment_action = new assignment_actionImpl();
    return assignment_action;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public communication_action createcommunication_action()
  {
    communication_actionImpl communication_action = new communication_actionImpl();
    return communication_action;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public timed_action createtimed_action()
  {
    timed_actionImpl timed_action = new timed_actionImpl();
    return timed_action;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public subprogram_parameter_list createsubprogram_parameter_list()
  {
    subprogram_parameter_listImpl subprogram_parameter_list = new subprogram_parameter_listImpl();
    return subprogram_parameter_list;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public parameter_label createparameter_label()
  {
    parameter_labelImpl parameter_label = new parameter_labelImpl();
    return parameter_label;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public data_component_reference createdata_component_reference()
  {
    data_component_referenceImpl data_component_reference = new data_component_referenceImpl();
    return data_component_reference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public name createname()
  {
    nameImpl name = new nameImpl();
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public array_index createarray_index()
  {
    array_indexImpl array_index = new array_indexImpl();
    return array_index;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public integer_value createinteger_value()
  {
    integer_valueImpl integer_value = new integer_valueImpl();
    return integer_value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public numeric_literal createnumeric_literal()
  {
    numeric_literalImpl numeric_literal = new numeric_literalImpl();
    return numeric_literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public integer_literal createinteger_literal()
  {
    integer_literalImpl integer_literal = new integer_literalImpl();
    return integer_literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public real_literal createreal_literal()
  {
    real_literalImpl real_literal = new real_literalImpl();
    return real_literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public decimal_integer_literal createdecimal_integer_literal()
  {
    decimal_integer_literalImpl decimal_integer_literal = new decimal_integer_literalImpl();
    return decimal_integer_literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public decimal_real_literal createdecimal_real_literal()
  {
    decimal_real_literalImpl decimal_real_literal = new decimal_real_literalImpl();
    return decimal_real_literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public numeral createnumeral()
  {
    numeralImpl numeral = new numeralImpl();
    return numeral;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public exponent createexponent()
  {
    exponentImpl exponent = new exponentImpl();
    return exponent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public positive_exponent createpositive_exponent()
  {
    positive_exponentImpl positive_exponent = new positive_exponentImpl();
    return positive_exponent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public based_integer_literal createbased_integer_literal()
  {
    based_integer_literalImpl based_integer_literal = new based_integer_literalImpl();
    return based_integer_literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public base createbase()
  {
    baseImpl base = new baseImpl();
    return base;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public based_numeral createbased_numeral()
  {
    based_numeralImpl based_numeral = new based_numeralImpl();
    return based_numeral;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_time createbehavior_time()
  {
    behavior_timeImpl behavior_time = new behavior_timeImpl();
    return behavior_time;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public unit_identifier createunit_identifier()
  {
    unit_identifierImpl unit_identifier = new unit_identifierImpl();
    return unit_identifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public integer_range createinteger_range()
  {
    integer_rangeImpl integer_range = new integer_rangeImpl();
    return integer_range;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public integer createinteger()
  {
    integerImpl integer = new integerImpl();
    return integer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value_expression createvalue_expression()
  {
    value_expressionImpl value_expression = new value_expressionImpl();
    return value_expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public relation createrelation()
  {
    relationImpl relation = new relationImpl();
    return relation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public simple_expression createsimple_expression()
  {
    simple_expressionImpl simple_expression = new simple_expressionImpl();
    return simple_expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public term createterm()
  {
    termImpl term = new termImpl();
    return term;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public factor createfactor()
  {
    factorImpl factor = new factorImpl();
    return factor;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value createvalue()
  {
    valueImpl value = new valueImpl();
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value_variable createvalue_variable()
  {
    value_variableImpl value_variable = new value_variableImpl();
    return value_variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value_constant createvalue_constant()
  {
    value_constantImpl value_constant = new value_constantImpl();
    return value_constant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public logical_operator createlogical_operator()
  {
    logical_operatorImpl logical_operator = new logical_operatorImpl();
    return logical_operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public relational_operator createrelational_operator()
  {
    relational_operatorImpl relational_operator = new relational_operatorImpl();
    return relational_operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public binary_adding_operator createbinary_adding_operator()
  {
    binary_adding_operatorImpl binary_adding_operator = new binary_adding_operatorImpl();
    return binary_adding_operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public unary_adding_operator createunary_adding_operator()
  {
    unary_adding_operatorImpl unary_adding_operator = new unary_adding_operatorImpl();
    return unary_adding_operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public multiplying_operator createmultiplying_operator()
  {
    multiplying_operatorImpl multiplying_operator = new multiplying_operatorImpl();
    return multiplying_operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public binary_numeric_operator createbinary_numeric_operator()
  {
    binary_numeric_operatorImpl binary_numeric_operator = new binary_numeric_operatorImpl();
    return binary_numeric_operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public unary_numeric_operator createunary_numeric_operator()
  {
    unary_numeric_operatorImpl unary_numeric_operator = new unary_numeric_operatorImpl();
    return unary_numeric_operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public unary_boolean_operator createunary_boolean_operator()
  {
    unary_boolean_operatorImpl unary_boolean_operator = new unary_boolean_operatorImpl();
    return unary_boolean_operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean_literal createboolean_literal()
  {
    boolean_literalImpl boolean_literal = new boolean_literalImpl();
    return boolean_literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaActionPackage getBaActionPackage()
  {
    return (BaActionPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static BaActionPackage getPackage()
  {
    return BaActionPackage.eINSTANCE;
  }

} //BaActionFactoryImpl
