/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>subprogram parameter list</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list#getSpl1 <em>Spl1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list#getSpl2 <em>Spl2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list#getSpl3 <em>Spl3</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getsubprogram_parameter_list()
 * @model
 * @generated
 */
public interface subprogram_parameter_list extends EObject
{
  /**
   * Returns the value of the '<em><b>Spl1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Spl1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Spl1</em>' containment reference.
   * @see #setSpl1(parameter_label)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getsubprogram_parameter_list_Spl1()
   * @model containment="true"
   * @generated
   */
  parameter_label getSpl1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list#getSpl1 <em>Spl1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Spl1</em>' containment reference.
   * @see #getSpl1()
   * @generated
   */
  void setSpl1(parameter_label value);

  /**
   * Returns the value of the '<em><b>Spl2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Spl2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Spl2</em>' attribute list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getsubprogram_parameter_list_Spl2()
   * @model unique="false"
   * @generated
   */
  EList<String> getSpl2();

  /**
   * Returns the value of the '<em><b>Spl3</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baaction.baAction.parameter_label}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Spl3</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Spl3</em>' containment reference list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getsubprogram_parameter_list_Spl3()
   * @model containment="true"
   * @generated
   */
  EList<parameter_label> getSpl3();

} // subprogram_parameter_list
