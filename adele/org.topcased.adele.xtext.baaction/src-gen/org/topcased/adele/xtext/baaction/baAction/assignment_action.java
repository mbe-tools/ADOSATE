/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>assignment action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.assignment_action#getAa1 <em>Aa1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.assignment_action#getAa2 <em>Aa2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.assignment_action#getAa3 <em>Aa3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.assignment_action#getAa4 <em>Aa4</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getassignment_action()
 * @model
 * @generated
 */
public interface assignment_action extends EObject
{
  /**
   * Returns the value of the '<em><b>Aa1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Aa1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Aa1</em>' attribute.
   * @see #setAa1(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getassignment_action_Aa1()
   * @model
   * @generated
   */
  String getAa1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.assignment_action#getAa1 <em>Aa1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Aa1</em>' attribute.
   * @see #getAa1()
   * @generated
   */
  void setAa1(String value);

  /**
   * Returns the value of the '<em><b>Aa2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Aa2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Aa2</em>' attribute.
   * @see #setAa2(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getassignment_action_Aa2()
   * @model
   * @generated
   */
  String getAa2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.assignment_action#getAa2 <em>Aa2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Aa2</em>' attribute.
   * @see #getAa2()
   * @generated
   */
  void setAa2(String value);

  /**
   * Returns the value of the '<em><b>Aa3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Aa3</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Aa3</em>' containment reference.
   * @see #setAa3(value_expression)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getassignment_action_Aa3()
   * @model containment="true"
   * @generated
   */
  value_expression getAa3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.assignment_action#getAa3 <em>Aa3</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Aa3</em>' containment reference.
   * @see #getAa3()
   * @generated
   */
  void setAa3(value_expression value);

  /**
   * Returns the value of the '<em><b>Aa4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Aa4</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Aa4</em>' attribute.
   * @see #setAa4(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getassignment_action_Aa4()
   * @model
   * @generated
   */
  String getAa4();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.assignment_action#getAa4 <em>Aa4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Aa4</em>' attribute.
   * @see #getAa4()
   * @generated
   */
  void setAa4(String value);

} // assignment_action
