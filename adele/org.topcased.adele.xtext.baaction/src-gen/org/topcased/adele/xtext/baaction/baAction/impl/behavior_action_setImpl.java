/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.behavior_action;
import org.topcased.adele.xtext.baaction.baAction.behavior_action_set;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>behavior action set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_setImpl#getBaset1 <em>Baset1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_setImpl#getBaset2 <em>Baset2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_action_setImpl#getBaset3 <em>Baset3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class behavior_action_setImpl extends MinimalEObjectImpl.Container implements behavior_action_set
{
  /**
   * The cached value of the '{@link #getBaset1() <em>Baset1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaset1()
   * @generated
   * @ordered
   */
  protected behavior_action baset1;

  /**
   * The cached value of the '{@link #getBaset2() <em>Baset2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaset2()
   * @generated
   * @ordered
   */
  protected EList<String> baset2;

  /**
   * The cached value of the '{@link #getBaset3() <em>Baset3</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaset3()
   * @generated
   * @ordered
   */
  protected EList<behavior_action> baset3;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected behavior_action_setImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.BEHAVIOR_ACTION_SET;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_action getBaset1()
  {
    return baset1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBaset1(behavior_action newBaset1, NotificationChain msgs)
  {
    behavior_action oldBaset1 = baset1;
    baset1 = newBaset1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION_SET__BASET1, oldBaset1, newBaset1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBaset1(behavior_action newBaset1)
  {
    if (newBaset1 != baset1)
    {
      NotificationChain msgs = null;
      if (baset1 != null)
        msgs = ((InternalEObject)baset1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION_SET__BASET1, null, msgs);
      if (newBaset1 != null)
        msgs = ((InternalEObject)newBaset1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION_SET__BASET1, null, msgs);
      msgs = basicSetBaset1(newBaset1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION_SET__BASET1, newBaset1, newBaset1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getBaset2()
  {
    if (baset2 == null)
    {
      baset2 = new EDataTypeEList<String>(String.class, this, BaActionPackage.BEHAVIOR_ACTION_SET__BASET2);
    }
    return baset2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<behavior_action> getBaset3()
  {
    if (baset3 == null)
    {
      baset3 = new EObjectContainmentEList<behavior_action>(behavior_action.class, this, BaActionPackage.BEHAVIOR_ACTION_SET__BASET3);
    }
    return baset3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION_SET__BASET1:
        return basicSetBaset1(null, msgs);
      case BaActionPackage.BEHAVIOR_ACTION_SET__BASET3:
        return ((InternalEList<?>)getBaset3()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION_SET__BASET1:
        return getBaset1();
      case BaActionPackage.BEHAVIOR_ACTION_SET__BASET2:
        return getBaset2();
      case BaActionPackage.BEHAVIOR_ACTION_SET__BASET3:
        return getBaset3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION_SET__BASET1:
        setBaset1((behavior_action)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION_SET__BASET2:
        getBaset2().clear();
        getBaset2().addAll((Collection<? extends String>)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION_SET__BASET3:
        getBaset3().clear();
        getBaset3().addAll((Collection<? extends behavior_action>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION_SET__BASET1:
        setBaset1((behavior_action)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTION_SET__BASET2:
        getBaset2().clear();
        return;
      case BaActionPackage.BEHAVIOR_ACTION_SET__BASET3:
        getBaset3().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION_SET__BASET1:
        return baset1 != null;
      case BaActionPackage.BEHAVIOR_ACTION_SET__BASET2:
        return baset2 != null && !baset2.isEmpty();
      case BaActionPackage.BEHAVIOR_ACTION_SET__BASET3:
        return baset3 != null && !baset3.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (baset2: ");
    result.append(baset2);
    result.append(')');
    return result.toString();
  }

} //behavior_action_setImpl
