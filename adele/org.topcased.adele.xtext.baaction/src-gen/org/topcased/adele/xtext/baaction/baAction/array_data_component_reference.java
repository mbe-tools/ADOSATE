/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>array data component reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr1 <em>Adcr1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr2 <em>Adcr2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr3 <em>Adcr3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr4 <em>Adcr4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr5 <em>Adcr5</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getarray_data_component_reference()
 * @model
 * @generated
 */
public interface array_data_component_reference extends EObject
{
  /**
   * Returns the value of the '<em><b>Adcr1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Adcr1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Adcr1</em>' attribute.
   * @see #setAdcr1(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getarray_data_component_reference_Adcr1()
   * @model
   * @generated
   */
  String getAdcr1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr1 <em>Adcr1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Adcr1</em>' attribute.
   * @see #getAdcr1()
   * @generated
   */
  void setAdcr1(String value);

  /**
   * Returns the value of the '<em><b>Adcr2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Adcr2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Adcr2</em>' containment reference.
   * @see #setAdcr2(data_component_reference)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getarray_data_component_reference_Adcr2()
   * @model containment="true"
   * @generated
   */
  data_component_reference getAdcr2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr2 <em>Adcr2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Adcr2</em>' containment reference.
   * @see #getAdcr2()
   * @generated
   */
  void setAdcr2(data_component_reference value);

  /**
   * Returns the value of the '<em><b>Adcr3</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Adcr3</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Adcr3</em>' attribute list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getarray_data_component_reference_Adcr3()
   * @model unique="false"
   * @generated
   */
  EList<String> getAdcr3();

  /**
   * Returns the value of the '<em><b>Adcr4</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baaction.baAction.data_component_reference}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Adcr4</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Adcr4</em>' containment reference list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getarray_data_component_reference_Adcr4()
   * @model containment="true"
   * @generated
   */
  EList<data_component_reference> getAdcr4();

  /**
   * Returns the value of the '<em><b>Adcr5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Adcr5</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Adcr5</em>' attribute.
   * @see #setAdcr5(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getarray_data_component_reference_Adcr5()
   * @model
   * @generated
   */
  String getAdcr5();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.array_data_component_reference#getAdcr5 <em>Adcr5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Adcr5</em>' attribute.
   * @see #getAdcr5()
   * @generated
   */
  void setAdcr5(String value);

} // array_data_component_reference
