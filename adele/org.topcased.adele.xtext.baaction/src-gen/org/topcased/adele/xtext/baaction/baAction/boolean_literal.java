/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>boolean literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.boolean_literal#getBlname1 <em>Blname1</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getboolean_literal()
 * @model
 * @generated
 */
public interface boolean_literal extends EObject
{
  /**
   * Returns the value of the '<em><b>Blname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Blname1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Blname1</em>' attribute.
   * @see #setBlname1(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getboolean_literal_Blname1()
   * @model
   * @generated
   */
  String getBlname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.boolean_literal#getBlname1 <em>Blname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Blname1</em>' attribute.
   * @see #getBlname1()
   * @generated
   */
  void setBlname1(String value);

} // boolean_literal
