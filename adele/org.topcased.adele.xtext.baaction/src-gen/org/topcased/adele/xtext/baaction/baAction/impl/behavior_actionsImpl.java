/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.behavior_action;
import org.topcased.adele.xtext.baaction.baAction.behavior_actions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>behavior actions</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionsImpl#getBas1 <em>Bas1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionsImpl#getBaseq2 <em>Baseq2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionsImpl#getBaseq4 <em>Baseq4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionsImpl#getBaseq3 <em>Baseq3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class behavior_actionsImpl extends MinimalEObjectImpl.Container implements behavior_actions
{
  /**
   * The cached value of the '{@link #getBas1() <em>Bas1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBas1()
   * @generated
   * @ordered
   */
  protected behavior_action bas1;

  /**
   * The cached value of the '{@link #getBaseq2() <em>Baseq2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaseq2()
   * @generated
   * @ordered
   */
  protected EList<String> baseq2;

  /**
   * The cached value of the '{@link #getBaseq4() <em>Baseq4</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaseq4()
   * @generated
   * @ordered
   */
  protected EList<String> baseq4;

  /**
   * The cached value of the '{@link #getBaseq3() <em>Baseq3</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBaseq3()
   * @generated
   * @ordered
   */
  protected EList<behavior_action> baseq3;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected behavior_actionsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.BEHAVIOR_ACTIONS;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_action getBas1()
  {
    return bas1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBas1(behavior_action newBas1, NotificationChain msgs)
  {
    behavior_action oldBas1 = bas1;
    bas1 = newBas1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTIONS__BAS1, oldBas1, newBas1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBas1(behavior_action newBas1)
  {
    if (newBas1 != bas1)
    {
      NotificationChain msgs = null;
      if (bas1 != null)
        msgs = ((InternalEObject)bas1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTIONS__BAS1, null, msgs);
      if (newBas1 != null)
        msgs = ((InternalEObject)newBas1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTIONS__BAS1, null, msgs);
      msgs = basicSetBas1(newBas1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTIONS__BAS1, newBas1, newBas1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getBaseq2()
  {
    if (baseq2 == null)
    {
      baseq2 = new EDataTypeEList<String>(String.class, this, BaActionPackage.BEHAVIOR_ACTIONS__BASEQ2);
    }
    return baseq2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getBaseq4()
  {
    if (baseq4 == null)
    {
      baseq4 = new EDataTypeEList<String>(String.class, this, BaActionPackage.BEHAVIOR_ACTIONS__BASEQ4);
    }
    return baseq4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<behavior_action> getBaseq3()
  {
    if (baseq3 == null)
    {
      baseq3 = new EObjectContainmentEList<behavior_action>(behavior_action.class, this, BaActionPackage.BEHAVIOR_ACTIONS__BASEQ3);
    }
    return baseq3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTIONS__BAS1:
        return basicSetBas1(null, msgs);
      case BaActionPackage.BEHAVIOR_ACTIONS__BASEQ3:
        return ((InternalEList<?>)getBaseq3()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTIONS__BAS1:
        return getBas1();
      case BaActionPackage.BEHAVIOR_ACTIONS__BASEQ2:
        return getBaseq2();
      case BaActionPackage.BEHAVIOR_ACTIONS__BASEQ4:
        return getBaseq4();
      case BaActionPackage.BEHAVIOR_ACTIONS__BASEQ3:
        return getBaseq3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTIONS__BAS1:
        setBas1((behavior_action)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTIONS__BASEQ2:
        getBaseq2().clear();
        getBaseq2().addAll((Collection<? extends String>)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTIONS__BASEQ4:
        getBaseq4().clear();
        getBaseq4().addAll((Collection<? extends String>)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTIONS__BASEQ3:
        getBaseq3().clear();
        getBaseq3().addAll((Collection<? extends behavior_action>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTIONS__BAS1:
        setBas1((behavior_action)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTIONS__BASEQ2:
        getBaseq2().clear();
        return;
      case BaActionPackage.BEHAVIOR_ACTIONS__BASEQ4:
        getBaseq4().clear();
        return;
      case BaActionPackage.BEHAVIOR_ACTIONS__BASEQ3:
        getBaseq3().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTIONS__BAS1:
        return bas1 != null;
      case BaActionPackage.BEHAVIOR_ACTIONS__BASEQ2:
        return baseq2 != null && !baseq2.isEmpty();
      case BaActionPackage.BEHAVIOR_ACTIONS__BASEQ4:
        return baseq4 != null && !baseq4.isEmpty();
      case BaActionPackage.BEHAVIOR_ACTIONS__BASEQ3:
        return baseq3 != null && !baseq3.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (baseq2: ");
    result.append(baseq2);
    result.append(", baseq4: ");
    result.append(baseq4);
    result.append(')');
    return result.toString();
  }

} //behavior_actionsImpl
