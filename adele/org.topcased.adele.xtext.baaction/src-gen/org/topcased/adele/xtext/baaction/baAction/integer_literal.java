/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>integer literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.integer_literal#getIlname1 <em>Ilname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.integer_literal#getIlname2 <em>Ilname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getinteger_literal()
 * @model
 * @generated
 */
public interface integer_literal extends EObject
{
  /**
   * Returns the value of the '<em><b>Ilname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ilname1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ilname1</em>' containment reference.
   * @see #setIlname1(decimal_integer_literal)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getinteger_literal_Ilname1()
   * @model containment="true"
   * @generated
   */
  decimal_integer_literal getIlname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.integer_literal#getIlname1 <em>Ilname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ilname1</em>' containment reference.
   * @see #getIlname1()
   * @generated
   */
  void setIlname1(decimal_integer_literal value);

  /**
   * Returns the value of the '<em><b>Ilname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ilname2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ilname2</em>' containment reference.
   * @see #setIlname2(based_integer_literal)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getinteger_literal_Ilname2()
   * @model containment="true"
   * @generated
   */
  based_integer_literal getIlname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.integer_literal#getIlname2 <em>Ilname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ilname2</em>' containment reference.
   * @see #getIlname2()
   * @generated
   */
  void setIlname2(based_integer_literal value);

} // integer_literal
