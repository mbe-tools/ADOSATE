/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>element values</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.element_values#getEv1 <em>Ev1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.element_values#getEv2 <em>Ev2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.element_values#getEv3 <em>Ev3</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getelement_values()
 * @model
 * @generated
 */
public interface element_values extends EObject
{
  /**
   * Returns the value of the '<em><b>Ev1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ev1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ev1</em>' containment reference.
   * @see #setEv1(integer_range)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getelement_values_Ev1()
   * @model containment="true"
   * @generated
   */
  integer_range getEv1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.element_values#getEv1 <em>Ev1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ev1</em>' containment reference.
   * @see #getEv1()
   * @generated
   */
  void setEv1(integer_range value);

  /**
   * Returns the value of the '<em><b>Ev2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ev2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ev2</em>' attribute.
   * @see #setEv2(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getelement_values_Ev2()
   * @model
   * @generated
   */
  String getEv2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.element_values#getEv2 <em>Ev2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ev2</em>' attribute.
   * @see #getEv2()
   * @generated
   */
  void setEv2(String value);

  /**
   * Returns the value of the '<em><b>Ev3</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ev3</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ev3</em>' containment reference.
   * @see #setEv3(array_data_component_reference)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getelement_values_Ev3()
   * @model containment="true"
   * @generated
   */
  array_data_component_reference getEv3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.element_values#getEv3 <em>Ev3</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ev3</em>' containment reference.
   * @see #getEv3()
   * @generated
   */
  void setEv3(array_data_component_reference value);

} // element_values
