/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.basic_action;
import org.topcased.adele.xtext.baaction.baAction.behavior_action;
import org.topcased.adele.xtext.baaction.baAction.behavior_action_block;
import org.topcased.adele.xtext.baaction.baAction.behavior_actions;
import org.topcased.adele.xtext.baaction.baAction.element_values;
import org.topcased.adele.xtext.baaction.baAction.value_expression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>behavior action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa1 <em>Ba1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa2 <em>Ba2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa3 <em>Ba3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa3bis <em>Ba3bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa4 <em>Ba4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa5 <em>Ba5</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa6 <em>Ba6</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa7 <em>Ba7</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa7bis <em>Ba7bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa8 <em>Ba8</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa9 <em>Ba9</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa10 <em>Ba10</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa11 <em>Ba11</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa12 <em>Ba12</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa13 <em>Ba13</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa13bis <em>Ba13bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa14 <em>Ba14</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa14bis <em>Ba14bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa15 <em>Ba15</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa16 <em>Ba16</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa17 <em>Ba17</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa18 <em>Ba18</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa19 <em>Ba19</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa20 <em>Ba20</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa20bis <em>Ba20bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa21 <em>Ba21</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa22 <em>Ba22</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa23 <em>Ba23</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa23bis <em>Ba23bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa24 <em>Ba24</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa25 <em>Ba25</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa26 <em>Ba26</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa27 <em>Ba27</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa29 <em>Ba29</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa30 <em>Ba30</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa30bis <em>Ba30bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa31 <em>Ba31</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa32 <em>Ba32</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa33 <em>Ba33</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa33bis <em>Ba33bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa34 <em>Ba34</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa35 <em>Ba35</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa35bis <em>Ba35bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa36 <em>Ba36</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa37 <em>Ba37</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa38 <em>Ba38</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa39 <em>Ba39</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa40 <em>Ba40</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa40bis <em>Ba40bis</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa41 <em>Ba41</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.behavior_actionImpl#getBa42 <em>Ba42</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class behavior_actionImpl extends MinimalEObjectImpl.Container implements behavior_action
{
  /**
   * The cached value of the '{@link #getBa1() <em>Ba1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa1()
   * @generated
   * @ordered
   */
  protected basic_action ba1;

  /**
   * The cached value of the '{@link #getBa2() <em>Ba2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa2()
   * @generated
   * @ordered
   */
  protected behavior_action_block ba2;

  /**
   * The default value of the '{@link #getBa3() <em>Ba3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa3()
   * @generated
   * @ordered
   */
  protected static final String BA3_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa3() <em>Ba3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa3()
   * @generated
   * @ordered
   */
  protected String ba3 = BA3_EDEFAULT;

  /**
   * The default value of the '{@link #getBa3bis() <em>Ba3bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa3bis()
   * @generated
   * @ordered
   */
  protected static final String BA3BIS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa3bis() <em>Ba3bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa3bis()
   * @generated
   * @ordered
   */
  protected String ba3bis = BA3BIS_EDEFAULT;

  /**
   * The cached value of the '{@link #getBa4() <em>Ba4</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa4()
   * @generated
   * @ordered
   */
  protected value_expression ba4;

  /**
   * The default value of the '{@link #getBa5() <em>Ba5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa5()
   * @generated
   * @ordered
   */
  protected static final String BA5_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa5() <em>Ba5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa5()
   * @generated
   * @ordered
   */
  protected String ba5 = BA5_EDEFAULT;

  /**
   * The cached value of the '{@link #getBa6() <em>Ba6</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa6()
   * @generated
   * @ordered
   */
  protected behavior_actions ba6;

  /**
   * The cached value of the '{@link #getBa7() <em>Ba7</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa7()
   * @generated
   * @ordered
   */
  protected EList<String> ba7;

  /**
   * The cached value of the '{@link #getBa7bis() <em>Ba7bis</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa7bis()
   * @generated
   * @ordered
   */
  protected EList<String> ba7bis;

  /**
   * The cached value of the '{@link #getBa8() <em>Ba8</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa8()
   * @generated
   * @ordered
   */
  protected EList<value_expression> ba8;

  /**
   * The cached value of the '{@link #getBa9() <em>Ba9</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa9()
   * @generated
   * @ordered
   */
  protected EList<String> ba9;

  /**
   * The cached value of the '{@link #getBa10() <em>Ba10</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa10()
   * @generated
   * @ordered
   */
  protected EList<behavior_actions> ba10;

  /**
   * The default value of the '{@link #getBa11() <em>Ba11</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa11()
   * @generated
   * @ordered
   */
  protected static final String BA11_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa11() <em>Ba11</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa11()
   * @generated
   * @ordered
   */
  protected String ba11 = BA11_EDEFAULT;

  /**
   * The cached value of the '{@link #getBa12() <em>Ba12</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa12()
   * @generated
   * @ordered
   */
  protected behavior_actions ba12;

  /**
   * The default value of the '{@link #getBa13() <em>Ba13</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa13()
   * @generated
   * @ordered
   */
  protected static final String BA13_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa13() <em>Ba13</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa13()
   * @generated
   * @ordered
   */
  protected String ba13 = BA13_EDEFAULT;

  /**
   * The default value of the '{@link #getBa13bis() <em>Ba13bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa13bis()
   * @generated
   * @ordered
   */
  protected static final String BA13BIS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa13bis() <em>Ba13bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa13bis()
   * @generated
   * @ordered
   */
  protected String ba13bis = BA13BIS_EDEFAULT;

  /**
   * The default value of the '{@link #getBa14() <em>Ba14</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa14()
   * @generated
   * @ordered
   */
  protected static final String BA14_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa14() <em>Ba14</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa14()
   * @generated
   * @ordered
   */
  protected String ba14 = BA14_EDEFAULT;

  /**
   * The default value of the '{@link #getBa14bis() <em>Ba14bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa14bis()
   * @generated
   * @ordered
   */
  protected static final String BA14BIS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa14bis() <em>Ba14bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa14bis()
   * @generated
   * @ordered
   */
  protected String ba14bis = BA14BIS_EDEFAULT;

  /**
   * The default value of the '{@link #getBa15() <em>Ba15</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa15()
   * @generated
   * @ordered
   */
  protected static final String BA15_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa15() <em>Ba15</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa15()
   * @generated
   * @ordered
   */
  protected String ba15 = BA15_EDEFAULT;

  /**
   * The default value of the '{@link #getBa16() <em>Ba16</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa16()
   * @generated
   * @ordered
   */
  protected static final String BA16_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa16() <em>Ba16</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa16()
   * @generated
   * @ordered
   */
  protected String ba16 = BA16_EDEFAULT;

  /**
   * The default value of the '{@link #getBa17() <em>Ba17</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa17()
   * @generated
   * @ordered
   */
  protected static final String BA17_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa17() <em>Ba17</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa17()
   * @generated
   * @ordered
   */
  protected String ba17 = BA17_EDEFAULT;

  /**
   * The default value of the '{@link #getBa18() <em>Ba18</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa18()
   * @generated
   * @ordered
   */
  protected static final String BA18_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa18() <em>Ba18</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa18()
   * @generated
   * @ordered
   */
  protected String ba18 = BA18_EDEFAULT;

  /**
   * The cached value of the '{@link #getBa19() <em>Ba19</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa19()
   * @generated
   * @ordered
   */
  protected element_values ba19;

  /**
   * The default value of the '{@link #getBa20() <em>Ba20</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa20()
   * @generated
   * @ordered
   */
  protected static final String BA20_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa20() <em>Ba20</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa20()
   * @generated
   * @ordered
   */
  protected String ba20 = BA20_EDEFAULT;

  /**
   * The default value of the '{@link #getBa20bis() <em>Ba20bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa20bis()
   * @generated
   * @ordered
   */
  protected static final String BA20BIS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa20bis() <em>Ba20bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa20bis()
   * @generated
   * @ordered
   */
  protected String ba20bis = BA20BIS_EDEFAULT;

  /**
   * The cached value of the '{@link #getBa21() <em>Ba21</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa21()
   * @generated
   * @ordered
   */
  protected behavior_actions ba21;

  /**
   * The default value of the '{@link #getBa22() <em>Ba22</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa22()
   * @generated
   * @ordered
   */
  protected static final String BA22_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa22() <em>Ba22</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa22()
   * @generated
   * @ordered
   */
  protected String ba22 = BA22_EDEFAULT;

  /**
   * The default value of the '{@link #getBa23() <em>Ba23</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa23()
   * @generated
   * @ordered
   */
  protected static final String BA23_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa23() <em>Ba23</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa23()
   * @generated
   * @ordered
   */
  protected String ba23 = BA23_EDEFAULT;

  /**
   * The default value of the '{@link #getBa23bis() <em>Ba23bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa23bis()
   * @generated
   * @ordered
   */
  protected static final String BA23BIS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa23bis() <em>Ba23bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa23bis()
   * @generated
   * @ordered
   */
  protected String ba23bis = BA23BIS_EDEFAULT;

  /**
   * The default value of the '{@link #getBa24() <em>Ba24</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa24()
   * @generated
   * @ordered
   */
  protected static final String BA24_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa24() <em>Ba24</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa24()
   * @generated
   * @ordered
   */
  protected String ba24 = BA24_EDEFAULT;

  /**
   * The default value of the '{@link #getBa25() <em>Ba25</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa25()
   * @generated
   * @ordered
   */
  protected static final String BA25_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa25() <em>Ba25</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa25()
   * @generated
   * @ordered
   */
  protected String ba25 = BA25_EDEFAULT;

  /**
   * The default value of the '{@link #getBa26() <em>Ba26</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa26()
   * @generated
   * @ordered
   */
  protected static final String BA26_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa26() <em>Ba26</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa26()
   * @generated
   * @ordered
   */
  protected String ba26 = BA26_EDEFAULT;

  /**
   * The default value of the '{@link #getBa27() <em>Ba27</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa27()
   * @generated
   * @ordered
   */
  protected static final String BA27_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa27() <em>Ba27</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa27()
   * @generated
   * @ordered
   */
  protected String ba27 = BA27_EDEFAULT;

  /**
   * The cached value of the '{@link #getBa29() <em>Ba29</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa29()
   * @generated
   * @ordered
   */
  protected element_values ba29;

  /**
   * The default value of the '{@link #getBa30() <em>Ba30</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa30()
   * @generated
   * @ordered
   */
  protected static final String BA30_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa30() <em>Ba30</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa30()
   * @generated
   * @ordered
   */
  protected String ba30 = BA30_EDEFAULT;

  /**
   * The default value of the '{@link #getBa30bis() <em>Ba30bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa30bis()
   * @generated
   * @ordered
   */
  protected static final String BA30BIS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa30bis() <em>Ba30bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa30bis()
   * @generated
   * @ordered
   */
  protected String ba30bis = BA30BIS_EDEFAULT;

  /**
   * The cached value of the '{@link #getBa31() <em>Ba31</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa31()
   * @generated
   * @ordered
   */
  protected behavior_actions ba31;

  /**
   * The default value of the '{@link #getBa32() <em>Ba32</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa32()
   * @generated
   * @ordered
   */
  protected static final String BA32_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa32() <em>Ba32</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa32()
   * @generated
   * @ordered
   */
  protected String ba32 = BA32_EDEFAULT;

  /**
   * The default value of the '{@link #getBa33() <em>Ba33</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa33()
   * @generated
   * @ordered
   */
  protected static final String BA33_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa33() <em>Ba33</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa33()
   * @generated
   * @ordered
   */
  protected String ba33 = BA33_EDEFAULT;

  /**
   * The default value of the '{@link #getBa33bis() <em>Ba33bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa33bis()
   * @generated
   * @ordered
   */
  protected static final String BA33BIS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa33bis() <em>Ba33bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa33bis()
   * @generated
   * @ordered
   */
  protected String ba33bis = BA33BIS_EDEFAULT;

  /**
   * The cached value of the '{@link #getBa34() <em>Ba34</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa34()
   * @generated
   * @ordered
   */
  protected value_expression ba34;

  /**
   * The default value of the '{@link #getBa35() <em>Ba35</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa35()
   * @generated
   * @ordered
   */
  protected static final String BA35_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa35() <em>Ba35</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa35()
   * @generated
   * @ordered
   */
  protected String ba35 = BA35_EDEFAULT;

  /**
   * The default value of the '{@link #getBa35bis() <em>Ba35bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa35bis()
   * @generated
   * @ordered
   */
  protected static final String BA35BIS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa35bis() <em>Ba35bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa35bis()
   * @generated
   * @ordered
   */
  protected String ba35bis = BA35BIS_EDEFAULT;

  /**
   * The cached value of the '{@link #getBa36() <em>Ba36</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa36()
   * @generated
   * @ordered
   */
  protected behavior_actions ba36;

  /**
   * The default value of the '{@link #getBa37() <em>Ba37</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa37()
   * @generated
   * @ordered
   */
  protected static final String BA37_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa37() <em>Ba37</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa37()
   * @generated
   * @ordered
   */
  protected String ba37 = BA37_EDEFAULT;

  /**
   * The default value of the '{@link #getBa38() <em>Ba38</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa38()
   * @generated
   * @ordered
   */
  protected static final String BA38_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa38() <em>Ba38</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa38()
   * @generated
   * @ordered
   */
  protected String ba38 = BA38_EDEFAULT;

  /**
   * The cached value of the '{@link #getBa39() <em>Ba39</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa39()
   * @generated
   * @ordered
   */
  protected behavior_actions ba39;

  /**
   * The default value of the '{@link #getBa40() <em>Ba40</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa40()
   * @generated
   * @ordered
   */
  protected static final String BA40_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa40() <em>Ba40</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa40()
   * @generated
   * @ordered
   */
  protected String ba40 = BA40_EDEFAULT;

  /**
   * The default value of the '{@link #getBa40bis() <em>Ba40bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa40bis()
   * @generated
   * @ordered
   */
  protected static final String BA40BIS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa40bis() <em>Ba40bis</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa40bis()
   * @generated
   * @ordered
   */
  protected String ba40bis = BA40BIS_EDEFAULT;

  /**
   * The cached value of the '{@link #getBa41() <em>Ba41</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa41()
   * @generated
   * @ordered
   */
  protected value_expression ba41;

  /**
   * The default value of the '{@link #getBa42() <em>Ba42</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa42()
   * @generated
   * @ordered
   */
  protected static final String BA42_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBa42() <em>Ba42</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBa42()
   * @generated
   * @ordered
   */
  protected String ba42 = BA42_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected behavior_actionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.BEHAVIOR_ACTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public basic_action getBa1()
  {
    return ba1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBa1(basic_action newBa1, NotificationChain msgs)
  {
    basic_action oldBa1 = ba1;
    ba1 = newBa1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA1, oldBa1, newBa1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa1(basic_action newBa1)
  {
    if (newBa1 != ba1)
    {
      NotificationChain msgs = null;
      if (ba1 != null)
        msgs = ((InternalEObject)ba1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA1, null, msgs);
      if (newBa1 != null)
        msgs = ((InternalEObject)newBa1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA1, null, msgs);
      msgs = basicSetBa1(newBa1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA1, newBa1, newBa1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_action_block getBa2()
  {
    return ba2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBa2(behavior_action_block newBa2, NotificationChain msgs)
  {
    behavior_action_block oldBa2 = ba2;
    ba2 = newBa2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA2, oldBa2, newBa2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa2(behavior_action_block newBa2)
  {
    if (newBa2 != ba2)
    {
      NotificationChain msgs = null;
      if (ba2 != null)
        msgs = ((InternalEObject)ba2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA2, null, msgs);
      if (newBa2 != null)
        msgs = ((InternalEObject)newBa2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA2, null, msgs);
      msgs = basicSetBa2(newBa2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA2, newBa2, newBa2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa3()
  {
    return ba3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa3(String newBa3)
  {
    String oldBa3 = ba3;
    ba3 = newBa3;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA3, oldBa3, ba3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa3bis()
  {
    return ba3bis;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa3bis(String newBa3bis)
  {
    String oldBa3bis = ba3bis;
    ba3bis = newBa3bis;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA3BIS, oldBa3bis, ba3bis));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value_expression getBa4()
  {
    return ba4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBa4(value_expression newBa4, NotificationChain msgs)
  {
    value_expression oldBa4 = ba4;
    ba4 = newBa4;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA4, oldBa4, newBa4);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa4(value_expression newBa4)
  {
    if (newBa4 != ba4)
    {
      NotificationChain msgs = null;
      if (ba4 != null)
        msgs = ((InternalEObject)ba4).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA4, null, msgs);
      if (newBa4 != null)
        msgs = ((InternalEObject)newBa4).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA4, null, msgs);
      msgs = basicSetBa4(newBa4, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA4, newBa4, newBa4));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa5()
  {
    return ba5;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa5(String newBa5)
  {
    String oldBa5 = ba5;
    ba5 = newBa5;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA5, oldBa5, ba5));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_actions getBa6()
  {
    return ba6;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBa6(behavior_actions newBa6, NotificationChain msgs)
  {
    behavior_actions oldBa6 = ba6;
    ba6 = newBa6;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA6, oldBa6, newBa6);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa6(behavior_actions newBa6)
  {
    if (newBa6 != ba6)
    {
      NotificationChain msgs = null;
      if (ba6 != null)
        msgs = ((InternalEObject)ba6).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA6, null, msgs);
      if (newBa6 != null)
        msgs = ((InternalEObject)newBa6).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA6, null, msgs);
      msgs = basicSetBa6(newBa6, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA6, newBa6, newBa6));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getBa7()
  {
    if (ba7 == null)
    {
      ba7 = new EDataTypeEList<String>(String.class, this, BaActionPackage.BEHAVIOR_ACTION__BA7);
    }
    return ba7;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getBa7bis()
  {
    if (ba7bis == null)
    {
      ba7bis = new EDataTypeEList<String>(String.class, this, BaActionPackage.BEHAVIOR_ACTION__BA7BIS);
    }
    return ba7bis;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<value_expression> getBa8()
  {
    if (ba8 == null)
    {
      ba8 = new EObjectContainmentEList<value_expression>(value_expression.class, this, BaActionPackage.BEHAVIOR_ACTION__BA8);
    }
    return ba8;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getBa9()
  {
    if (ba9 == null)
    {
      ba9 = new EDataTypeEList<String>(String.class, this, BaActionPackage.BEHAVIOR_ACTION__BA9);
    }
    return ba9;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<behavior_actions> getBa10()
  {
    if (ba10 == null)
    {
      ba10 = new EObjectContainmentEList<behavior_actions>(behavior_actions.class, this, BaActionPackage.BEHAVIOR_ACTION__BA10);
    }
    return ba10;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa11()
  {
    return ba11;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa11(String newBa11)
  {
    String oldBa11 = ba11;
    ba11 = newBa11;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA11, oldBa11, ba11));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_actions getBa12()
  {
    return ba12;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBa12(behavior_actions newBa12, NotificationChain msgs)
  {
    behavior_actions oldBa12 = ba12;
    ba12 = newBa12;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA12, oldBa12, newBa12);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa12(behavior_actions newBa12)
  {
    if (newBa12 != ba12)
    {
      NotificationChain msgs = null;
      if (ba12 != null)
        msgs = ((InternalEObject)ba12).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA12, null, msgs);
      if (newBa12 != null)
        msgs = ((InternalEObject)newBa12).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA12, null, msgs);
      msgs = basicSetBa12(newBa12, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA12, newBa12, newBa12));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa13()
  {
    return ba13;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa13(String newBa13)
  {
    String oldBa13 = ba13;
    ba13 = newBa13;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA13, oldBa13, ba13));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa13bis()
  {
    return ba13bis;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa13bis(String newBa13bis)
  {
    String oldBa13bis = ba13bis;
    ba13bis = newBa13bis;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA13BIS, oldBa13bis, ba13bis));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa14()
  {
    return ba14;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa14(String newBa14)
  {
    String oldBa14 = ba14;
    ba14 = newBa14;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA14, oldBa14, ba14));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa14bis()
  {
    return ba14bis;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa14bis(String newBa14bis)
  {
    String oldBa14bis = ba14bis;
    ba14bis = newBa14bis;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA14BIS, oldBa14bis, ba14bis));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa15()
  {
    return ba15;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa15(String newBa15)
  {
    String oldBa15 = ba15;
    ba15 = newBa15;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA15, oldBa15, ba15));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa16()
  {
    return ba16;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa16(String newBa16)
  {
    String oldBa16 = ba16;
    ba16 = newBa16;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA16, oldBa16, ba16));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa17()
  {
    return ba17;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa17(String newBa17)
  {
    String oldBa17 = ba17;
    ba17 = newBa17;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA17, oldBa17, ba17));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa18()
  {
    return ba18;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa18(String newBa18)
  {
    String oldBa18 = ba18;
    ba18 = newBa18;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA18, oldBa18, ba18));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public element_values getBa19()
  {
    return ba19;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBa19(element_values newBa19, NotificationChain msgs)
  {
    element_values oldBa19 = ba19;
    ba19 = newBa19;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA19, oldBa19, newBa19);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa19(element_values newBa19)
  {
    if (newBa19 != ba19)
    {
      NotificationChain msgs = null;
      if (ba19 != null)
        msgs = ((InternalEObject)ba19).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA19, null, msgs);
      if (newBa19 != null)
        msgs = ((InternalEObject)newBa19).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA19, null, msgs);
      msgs = basicSetBa19(newBa19, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA19, newBa19, newBa19));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa20()
  {
    return ba20;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa20(String newBa20)
  {
    String oldBa20 = ba20;
    ba20 = newBa20;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA20, oldBa20, ba20));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa20bis()
  {
    return ba20bis;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa20bis(String newBa20bis)
  {
    String oldBa20bis = ba20bis;
    ba20bis = newBa20bis;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA20BIS, oldBa20bis, ba20bis));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_actions getBa21()
  {
    return ba21;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBa21(behavior_actions newBa21, NotificationChain msgs)
  {
    behavior_actions oldBa21 = ba21;
    ba21 = newBa21;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA21, oldBa21, newBa21);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa21(behavior_actions newBa21)
  {
    if (newBa21 != ba21)
    {
      NotificationChain msgs = null;
      if (ba21 != null)
        msgs = ((InternalEObject)ba21).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA21, null, msgs);
      if (newBa21 != null)
        msgs = ((InternalEObject)newBa21).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA21, null, msgs);
      msgs = basicSetBa21(newBa21, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA21, newBa21, newBa21));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa22()
  {
    return ba22;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa22(String newBa22)
  {
    String oldBa22 = ba22;
    ba22 = newBa22;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA22, oldBa22, ba22));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa23()
  {
    return ba23;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa23(String newBa23)
  {
    String oldBa23 = ba23;
    ba23 = newBa23;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA23, oldBa23, ba23));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa23bis()
  {
    return ba23bis;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa23bis(String newBa23bis)
  {
    String oldBa23bis = ba23bis;
    ba23bis = newBa23bis;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA23BIS, oldBa23bis, ba23bis));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa24()
  {
    return ba24;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa24(String newBa24)
  {
    String oldBa24 = ba24;
    ba24 = newBa24;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA24, oldBa24, ba24));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa25()
  {
    return ba25;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa25(String newBa25)
  {
    String oldBa25 = ba25;
    ba25 = newBa25;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA25, oldBa25, ba25));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa26()
  {
    return ba26;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa26(String newBa26)
  {
    String oldBa26 = ba26;
    ba26 = newBa26;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA26, oldBa26, ba26));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa27()
  {
    return ba27;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa27(String newBa27)
  {
    String oldBa27 = ba27;
    ba27 = newBa27;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA27, oldBa27, ba27));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public element_values getBa29()
  {
    return ba29;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBa29(element_values newBa29, NotificationChain msgs)
  {
    element_values oldBa29 = ba29;
    ba29 = newBa29;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA29, oldBa29, newBa29);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa29(element_values newBa29)
  {
    if (newBa29 != ba29)
    {
      NotificationChain msgs = null;
      if (ba29 != null)
        msgs = ((InternalEObject)ba29).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA29, null, msgs);
      if (newBa29 != null)
        msgs = ((InternalEObject)newBa29).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA29, null, msgs);
      msgs = basicSetBa29(newBa29, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA29, newBa29, newBa29));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa30()
  {
    return ba30;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa30(String newBa30)
  {
    String oldBa30 = ba30;
    ba30 = newBa30;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA30, oldBa30, ba30));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa30bis()
  {
    return ba30bis;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa30bis(String newBa30bis)
  {
    String oldBa30bis = ba30bis;
    ba30bis = newBa30bis;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA30BIS, oldBa30bis, ba30bis));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_actions getBa31()
  {
    return ba31;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBa31(behavior_actions newBa31, NotificationChain msgs)
  {
    behavior_actions oldBa31 = ba31;
    ba31 = newBa31;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA31, oldBa31, newBa31);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa31(behavior_actions newBa31)
  {
    if (newBa31 != ba31)
    {
      NotificationChain msgs = null;
      if (ba31 != null)
        msgs = ((InternalEObject)ba31).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA31, null, msgs);
      if (newBa31 != null)
        msgs = ((InternalEObject)newBa31).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA31, null, msgs);
      msgs = basicSetBa31(newBa31, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA31, newBa31, newBa31));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa32()
  {
    return ba32;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa32(String newBa32)
  {
    String oldBa32 = ba32;
    ba32 = newBa32;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA32, oldBa32, ba32));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa33()
  {
    return ba33;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa33(String newBa33)
  {
    String oldBa33 = ba33;
    ba33 = newBa33;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA33, oldBa33, ba33));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa33bis()
  {
    return ba33bis;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa33bis(String newBa33bis)
  {
    String oldBa33bis = ba33bis;
    ba33bis = newBa33bis;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA33BIS, oldBa33bis, ba33bis));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value_expression getBa34()
  {
    return ba34;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBa34(value_expression newBa34, NotificationChain msgs)
  {
    value_expression oldBa34 = ba34;
    ba34 = newBa34;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA34, oldBa34, newBa34);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa34(value_expression newBa34)
  {
    if (newBa34 != ba34)
    {
      NotificationChain msgs = null;
      if (ba34 != null)
        msgs = ((InternalEObject)ba34).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA34, null, msgs);
      if (newBa34 != null)
        msgs = ((InternalEObject)newBa34).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA34, null, msgs);
      msgs = basicSetBa34(newBa34, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA34, newBa34, newBa34));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa35()
  {
    return ba35;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa35(String newBa35)
  {
    String oldBa35 = ba35;
    ba35 = newBa35;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA35, oldBa35, ba35));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa35bis()
  {
    return ba35bis;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa35bis(String newBa35bis)
  {
    String oldBa35bis = ba35bis;
    ba35bis = newBa35bis;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA35BIS, oldBa35bis, ba35bis));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_actions getBa36()
  {
    return ba36;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBa36(behavior_actions newBa36, NotificationChain msgs)
  {
    behavior_actions oldBa36 = ba36;
    ba36 = newBa36;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA36, oldBa36, newBa36);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa36(behavior_actions newBa36)
  {
    if (newBa36 != ba36)
    {
      NotificationChain msgs = null;
      if (ba36 != null)
        msgs = ((InternalEObject)ba36).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA36, null, msgs);
      if (newBa36 != null)
        msgs = ((InternalEObject)newBa36).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA36, null, msgs);
      msgs = basicSetBa36(newBa36, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA36, newBa36, newBa36));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa37()
  {
    return ba37;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa37(String newBa37)
  {
    String oldBa37 = ba37;
    ba37 = newBa37;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA37, oldBa37, ba37));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa38()
  {
    return ba38;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa38(String newBa38)
  {
    String oldBa38 = ba38;
    ba38 = newBa38;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA38, oldBa38, ba38));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public behavior_actions getBa39()
  {
    return ba39;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBa39(behavior_actions newBa39, NotificationChain msgs)
  {
    behavior_actions oldBa39 = ba39;
    ba39 = newBa39;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA39, oldBa39, newBa39);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa39(behavior_actions newBa39)
  {
    if (newBa39 != ba39)
    {
      NotificationChain msgs = null;
      if (ba39 != null)
        msgs = ((InternalEObject)ba39).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA39, null, msgs);
      if (newBa39 != null)
        msgs = ((InternalEObject)newBa39).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA39, null, msgs);
      msgs = basicSetBa39(newBa39, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA39, newBa39, newBa39));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa40()
  {
    return ba40;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa40(String newBa40)
  {
    String oldBa40 = ba40;
    ba40 = newBa40;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA40, oldBa40, ba40));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa40bis()
  {
    return ba40bis;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa40bis(String newBa40bis)
  {
    String oldBa40bis = ba40bis;
    ba40bis = newBa40bis;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA40BIS, oldBa40bis, ba40bis));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value_expression getBa41()
  {
    return ba41;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBa41(value_expression newBa41, NotificationChain msgs)
  {
    value_expression oldBa41 = ba41;
    ba41 = newBa41;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA41, oldBa41, newBa41);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa41(value_expression newBa41)
  {
    if (newBa41 != ba41)
    {
      NotificationChain msgs = null;
      if (ba41 != null)
        msgs = ((InternalEObject)ba41).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA41, null, msgs);
      if (newBa41 != null)
        msgs = ((InternalEObject)newBa41).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BEHAVIOR_ACTION__BA41, null, msgs);
      msgs = basicSetBa41(newBa41, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA41, newBa41, newBa41));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBa42()
  {
    return ba42;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBa42(String newBa42)
  {
    String oldBa42 = ba42;
    ba42 = newBa42;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BEHAVIOR_ACTION__BA42, oldBa42, ba42));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION__BA1:
        return basicSetBa1(null, msgs);
      case BaActionPackage.BEHAVIOR_ACTION__BA2:
        return basicSetBa2(null, msgs);
      case BaActionPackage.BEHAVIOR_ACTION__BA4:
        return basicSetBa4(null, msgs);
      case BaActionPackage.BEHAVIOR_ACTION__BA6:
        return basicSetBa6(null, msgs);
      case BaActionPackage.BEHAVIOR_ACTION__BA8:
        return ((InternalEList<?>)getBa8()).basicRemove(otherEnd, msgs);
      case BaActionPackage.BEHAVIOR_ACTION__BA10:
        return ((InternalEList<?>)getBa10()).basicRemove(otherEnd, msgs);
      case BaActionPackage.BEHAVIOR_ACTION__BA12:
        return basicSetBa12(null, msgs);
      case BaActionPackage.BEHAVIOR_ACTION__BA19:
        return basicSetBa19(null, msgs);
      case BaActionPackage.BEHAVIOR_ACTION__BA21:
        return basicSetBa21(null, msgs);
      case BaActionPackage.BEHAVIOR_ACTION__BA29:
        return basicSetBa29(null, msgs);
      case BaActionPackage.BEHAVIOR_ACTION__BA31:
        return basicSetBa31(null, msgs);
      case BaActionPackage.BEHAVIOR_ACTION__BA34:
        return basicSetBa34(null, msgs);
      case BaActionPackage.BEHAVIOR_ACTION__BA36:
        return basicSetBa36(null, msgs);
      case BaActionPackage.BEHAVIOR_ACTION__BA39:
        return basicSetBa39(null, msgs);
      case BaActionPackage.BEHAVIOR_ACTION__BA41:
        return basicSetBa41(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION__BA1:
        return getBa1();
      case BaActionPackage.BEHAVIOR_ACTION__BA2:
        return getBa2();
      case BaActionPackage.BEHAVIOR_ACTION__BA3:
        return getBa3();
      case BaActionPackage.BEHAVIOR_ACTION__BA3BIS:
        return getBa3bis();
      case BaActionPackage.BEHAVIOR_ACTION__BA4:
        return getBa4();
      case BaActionPackage.BEHAVIOR_ACTION__BA5:
        return getBa5();
      case BaActionPackage.BEHAVIOR_ACTION__BA6:
        return getBa6();
      case BaActionPackage.BEHAVIOR_ACTION__BA7:
        return getBa7();
      case BaActionPackage.BEHAVIOR_ACTION__BA7BIS:
        return getBa7bis();
      case BaActionPackage.BEHAVIOR_ACTION__BA8:
        return getBa8();
      case BaActionPackage.BEHAVIOR_ACTION__BA9:
        return getBa9();
      case BaActionPackage.BEHAVIOR_ACTION__BA10:
        return getBa10();
      case BaActionPackage.BEHAVIOR_ACTION__BA11:
        return getBa11();
      case BaActionPackage.BEHAVIOR_ACTION__BA12:
        return getBa12();
      case BaActionPackage.BEHAVIOR_ACTION__BA13:
        return getBa13();
      case BaActionPackage.BEHAVIOR_ACTION__BA13BIS:
        return getBa13bis();
      case BaActionPackage.BEHAVIOR_ACTION__BA14:
        return getBa14();
      case BaActionPackage.BEHAVIOR_ACTION__BA14BIS:
        return getBa14bis();
      case BaActionPackage.BEHAVIOR_ACTION__BA15:
        return getBa15();
      case BaActionPackage.BEHAVIOR_ACTION__BA16:
        return getBa16();
      case BaActionPackage.BEHAVIOR_ACTION__BA17:
        return getBa17();
      case BaActionPackage.BEHAVIOR_ACTION__BA18:
        return getBa18();
      case BaActionPackage.BEHAVIOR_ACTION__BA19:
        return getBa19();
      case BaActionPackage.BEHAVIOR_ACTION__BA20:
        return getBa20();
      case BaActionPackage.BEHAVIOR_ACTION__BA20BIS:
        return getBa20bis();
      case BaActionPackage.BEHAVIOR_ACTION__BA21:
        return getBa21();
      case BaActionPackage.BEHAVIOR_ACTION__BA22:
        return getBa22();
      case BaActionPackage.BEHAVIOR_ACTION__BA23:
        return getBa23();
      case BaActionPackage.BEHAVIOR_ACTION__BA23BIS:
        return getBa23bis();
      case BaActionPackage.BEHAVIOR_ACTION__BA24:
        return getBa24();
      case BaActionPackage.BEHAVIOR_ACTION__BA25:
        return getBa25();
      case BaActionPackage.BEHAVIOR_ACTION__BA26:
        return getBa26();
      case BaActionPackage.BEHAVIOR_ACTION__BA27:
        return getBa27();
      case BaActionPackage.BEHAVIOR_ACTION__BA29:
        return getBa29();
      case BaActionPackage.BEHAVIOR_ACTION__BA30:
        return getBa30();
      case BaActionPackage.BEHAVIOR_ACTION__BA30BIS:
        return getBa30bis();
      case BaActionPackage.BEHAVIOR_ACTION__BA31:
        return getBa31();
      case BaActionPackage.BEHAVIOR_ACTION__BA32:
        return getBa32();
      case BaActionPackage.BEHAVIOR_ACTION__BA33:
        return getBa33();
      case BaActionPackage.BEHAVIOR_ACTION__BA33BIS:
        return getBa33bis();
      case BaActionPackage.BEHAVIOR_ACTION__BA34:
        return getBa34();
      case BaActionPackage.BEHAVIOR_ACTION__BA35:
        return getBa35();
      case BaActionPackage.BEHAVIOR_ACTION__BA35BIS:
        return getBa35bis();
      case BaActionPackage.BEHAVIOR_ACTION__BA36:
        return getBa36();
      case BaActionPackage.BEHAVIOR_ACTION__BA37:
        return getBa37();
      case BaActionPackage.BEHAVIOR_ACTION__BA38:
        return getBa38();
      case BaActionPackage.BEHAVIOR_ACTION__BA39:
        return getBa39();
      case BaActionPackage.BEHAVIOR_ACTION__BA40:
        return getBa40();
      case BaActionPackage.BEHAVIOR_ACTION__BA40BIS:
        return getBa40bis();
      case BaActionPackage.BEHAVIOR_ACTION__BA41:
        return getBa41();
      case BaActionPackage.BEHAVIOR_ACTION__BA42:
        return getBa42();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION__BA1:
        setBa1((basic_action)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA2:
        setBa2((behavior_action_block)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA3:
        setBa3((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA3BIS:
        setBa3bis((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA4:
        setBa4((value_expression)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA5:
        setBa5((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA6:
        setBa6((behavior_actions)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA7:
        getBa7().clear();
        getBa7().addAll((Collection<? extends String>)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA7BIS:
        getBa7bis().clear();
        getBa7bis().addAll((Collection<? extends String>)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA8:
        getBa8().clear();
        getBa8().addAll((Collection<? extends value_expression>)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA9:
        getBa9().clear();
        getBa9().addAll((Collection<? extends String>)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA10:
        getBa10().clear();
        getBa10().addAll((Collection<? extends behavior_actions>)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA11:
        setBa11((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA12:
        setBa12((behavior_actions)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA13:
        setBa13((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA13BIS:
        setBa13bis((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA14:
        setBa14((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA14BIS:
        setBa14bis((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA15:
        setBa15((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA16:
        setBa16((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA17:
        setBa17((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA18:
        setBa18((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA19:
        setBa19((element_values)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA20:
        setBa20((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA20BIS:
        setBa20bis((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA21:
        setBa21((behavior_actions)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA22:
        setBa22((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA23:
        setBa23((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA23BIS:
        setBa23bis((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA24:
        setBa24((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA25:
        setBa25((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA26:
        setBa26((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA27:
        setBa27((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA29:
        setBa29((element_values)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA30:
        setBa30((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA30BIS:
        setBa30bis((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA31:
        setBa31((behavior_actions)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA32:
        setBa32((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA33:
        setBa33((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA33BIS:
        setBa33bis((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA34:
        setBa34((value_expression)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA35:
        setBa35((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA35BIS:
        setBa35bis((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA36:
        setBa36((behavior_actions)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA37:
        setBa37((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA38:
        setBa38((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA39:
        setBa39((behavior_actions)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA40:
        setBa40((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA40BIS:
        setBa40bis((String)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA41:
        setBa41((value_expression)newValue);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA42:
        setBa42((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION__BA1:
        setBa1((basic_action)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA2:
        setBa2((behavior_action_block)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA3:
        setBa3(BA3_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA3BIS:
        setBa3bis(BA3BIS_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA4:
        setBa4((value_expression)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA5:
        setBa5(BA5_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA6:
        setBa6((behavior_actions)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA7:
        getBa7().clear();
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA7BIS:
        getBa7bis().clear();
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA8:
        getBa8().clear();
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA9:
        getBa9().clear();
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA10:
        getBa10().clear();
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA11:
        setBa11(BA11_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA12:
        setBa12((behavior_actions)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA13:
        setBa13(BA13_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA13BIS:
        setBa13bis(BA13BIS_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA14:
        setBa14(BA14_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA14BIS:
        setBa14bis(BA14BIS_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA15:
        setBa15(BA15_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA16:
        setBa16(BA16_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA17:
        setBa17(BA17_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA18:
        setBa18(BA18_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA19:
        setBa19((element_values)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA20:
        setBa20(BA20_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA20BIS:
        setBa20bis(BA20BIS_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA21:
        setBa21((behavior_actions)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA22:
        setBa22(BA22_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA23:
        setBa23(BA23_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA23BIS:
        setBa23bis(BA23BIS_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA24:
        setBa24(BA24_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA25:
        setBa25(BA25_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA26:
        setBa26(BA26_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA27:
        setBa27(BA27_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA29:
        setBa29((element_values)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA30:
        setBa30(BA30_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA30BIS:
        setBa30bis(BA30BIS_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA31:
        setBa31((behavior_actions)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA32:
        setBa32(BA32_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA33:
        setBa33(BA33_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA33BIS:
        setBa33bis(BA33BIS_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA34:
        setBa34((value_expression)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA35:
        setBa35(BA35_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA35BIS:
        setBa35bis(BA35BIS_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA36:
        setBa36((behavior_actions)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA37:
        setBa37(BA37_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA38:
        setBa38(BA38_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA39:
        setBa39((behavior_actions)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA40:
        setBa40(BA40_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA40BIS:
        setBa40bis(BA40BIS_EDEFAULT);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA41:
        setBa41((value_expression)null);
        return;
      case BaActionPackage.BEHAVIOR_ACTION__BA42:
        setBa42(BA42_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BEHAVIOR_ACTION__BA1:
        return ba1 != null;
      case BaActionPackage.BEHAVIOR_ACTION__BA2:
        return ba2 != null;
      case BaActionPackage.BEHAVIOR_ACTION__BA3:
        return BA3_EDEFAULT == null ? ba3 != null : !BA3_EDEFAULT.equals(ba3);
      case BaActionPackage.BEHAVIOR_ACTION__BA3BIS:
        return BA3BIS_EDEFAULT == null ? ba3bis != null : !BA3BIS_EDEFAULT.equals(ba3bis);
      case BaActionPackage.BEHAVIOR_ACTION__BA4:
        return ba4 != null;
      case BaActionPackage.BEHAVIOR_ACTION__BA5:
        return BA5_EDEFAULT == null ? ba5 != null : !BA5_EDEFAULT.equals(ba5);
      case BaActionPackage.BEHAVIOR_ACTION__BA6:
        return ba6 != null;
      case BaActionPackage.BEHAVIOR_ACTION__BA7:
        return ba7 != null && !ba7.isEmpty();
      case BaActionPackage.BEHAVIOR_ACTION__BA7BIS:
        return ba7bis != null && !ba7bis.isEmpty();
      case BaActionPackage.BEHAVIOR_ACTION__BA8:
        return ba8 != null && !ba8.isEmpty();
      case BaActionPackage.BEHAVIOR_ACTION__BA9:
        return ba9 != null && !ba9.isEmpty();
      case BaActionPackage.BEHAVIOR_ACTION__BA10:
        return ba10 != null && !ba10.isEmpty();
      case BaActionPackage.BEHAVIOR_ACTION__BA11:
        return BA11_EDEFAULT == null ? ba11 != null : !BA11_EDEFAULT.equals(ba11);
      case BaActionPackage.BEHAVIOR_ACTION__BA12:
        return ba12 != null;
      case BaActionPackage.BEHAVIOR_ACTION__BA13:
        return BA13_EDEFAULT == null ? ba13 != null : !BA13_EDEFAULT.equals(ba13);
      case BaActionPackage.BEHAVIOR_ACTION__BA13BIS:
        return BA13BIS_EDEFAULT == null ? ba13bis != null : !BA13BIS_EDEFAULT.equals(ba13bis);
      case BaActionPackage.BEHAVIOR_ACTION__BA14:
        return BA14_EDEFAULT == null ? ba14 != null : !BA14_EDEFAULT.equals(ba14);
      case BaActionPackage.BEHAVIOR_ACTION__BA14BIS:
        return BA14BIS_EDEFAULT == null ? ba14bis != null : !BA14BIS_EDEFAULT.equals(ba14bis);
      case BaActionPackage.BEHAVIOR_ACTION__BA15:
        return BA15_EDEFAULT == null ? ba15 != null : !BA15_EDEFAULT.equals(ba15);
      case BaActionPackage.BEHAVIOR_ACTION__BA16:
        return BA16_EDEFAULT == null ? ba16 != null : !BA16_EDEFAULT.equals(ba16);
      case BaActionPackage.BEHAVIOR_ACTION__BA17:
        return BA17_EDEFAULT == null ? ba17 != null : !BA17_EDEFAULT.equals(ba17);
      case BaActionPackage.BEHAVIOR_ACTION__BA18:
        return BA18_EDEFAULT == null ? ba18 != null : !BA18_EDEFAULT.equals(ba18);
      case BaActionPackage.BEHAVIOR_ACTION__BA19:
        return ba19 != null;
      case BaActionPackage.BEHAVIOR_ACTION__BA20:
        return BA20_EDEFAULT == null ? ba20 != null : !BA20_EDEFAULT.equals(ba20);
      case BaActionPackage.BEHAVIOR_ACTION__BA20BIS:
        return BA20BIS_EDEFAULT == null ? ba20bis != null : !BA20BIS_EDEFAULT.equals(ba20bis);
      case BaActionPackage.BEHAVIOR_ACTION__BA21:
        return ba21 != null;
      case BaActionPackage.BEHAVIOR_ACTION__BA22:
        return BA22_EDEFAULT == null ? ba22 != null : !BA22_EDEFAULT.equals(ba22);
      case BaActionPackage.BEHAVIOR_ACTION__BA23:
        return BA23_EDEFAULT == null ? ba23 != null : !BA23_EDEFAULT.equals(ba23);
      case BaActionPackage.BEHAVIOR_ACTION__BA23BIS:
        return BA23BIS_EDEFAULT == null ? ba23bis != null : !BA23BIS_EDEFAULT.equals(ba23bis);
      case BaActionPackage.BEHAVIOR_ACTION__BA24:
        return BA24_EDEFAULT == null ? ba24 != null : !BA24_EDEFAULT.equals(ba24);
      case BaActionPackage.BEHAVIOR_ACTION__BA25:
        return BA25_EDEFAULT == null ? ba25 != null : !BA25_EDEFAULT.equals(ba25);
      case BaActionPackage.BEHAVIOR_ACTION__BA26:
        return BA26_EDEFAULT == null ? ba26 != null : !BA26_EDEFAULT.equals(ba26);
      case BaActionPackage.BEHAVIOR_ACTION__BA27:
        return BA27_EDEFAULT == null ? ba27 != null : !BA27_EDEFAULT.equals(ba27);
      case BaActionPackage.BEHAVIOR_ACTION__BA29:
        return ba29 != null;
      case BaActionPackage.BEHAVIOR_ACTION__BA30:
        return BA30_EDEFAULT == null ? ba30 != null : !BA30_EDEFAULT.equals(ba30);
      case BaActionPackage.BEHAVIOR_ACTION__BA30BIS:
        return BA30BIS_EDEFAULT == null ? ba30bis != null : !BA30BIS_EDEFAULT.equals(ba30bis);
      case BaActionPackage.BEHAVIOR_ACTION__BA31:
        return ba31 != null;
      case BaActionPackage.BEHAVIOR_ACTION__BA32:
        return BA32_EDEFAULT == null ? ba32 != null : !BA32_EDEFAULT.equals(ba32);
      case BaActionPackage.BEHAVIOR_ACTION__BA33:
        return BA33_EDEFAULT == null ? ba33 != null : !BA33_EDEFAULT.equals(ba33);
      case BaActionPackage.BEHAVIOR_ACTION__BA33BIS:
        return BA33BIS_EDEFAULT == null ? ba33bis != null : !BA33BIS_EDEFAULT.equals(ba33bis);
      case BaActionPackage.BEHAVIOR_ACTION__BA34:
        return ba34 != null;
      case BaActionPackage.BEHAVIOR_ACTION__BA35:
        return BA35_EDEFAULT == null ? ba35 != null : !BA35_EDEFAULT.equals(ba35);
      case BaActionPackage.BEHAVIOR_ACTION__BA35BIS:
        return BA35BIS_EDEFAULT == null ? ba35bis != null : !BA35BIS_EDEFAULT.equals(ba35bis);
      case BaActionPackage.BEHAVIOR_ACTION__BA36:
        return ba36 != null;
      case BaActionPackage.BEHAVIOR_ACTION__BA37:
        return BA37_EDEFAULT == null ? ba37 != null : !BA37_EDEFAULT.equals(ba37);
      case BaActionPackage.BEHAVIOR_ACTION__BA38:
        return BA38_EDEFAULT == null ? ba38 != null : !BA38_EDEFAULT.equals(ba38);
      case BaActionPackage.BEHAVIOR_ACTION__BA39:
        return ba39 != null;
      case BaActionPackage.BEHAVIOR_ACTION__BA40:
        return BA40_EDEFAULT == null ? ba40 != null : !BA40_EDEFAULT.equals(ba40);
      case BaActionPackage.BEHAVIOR_ACTION__BA40BIS:
        return BA40BIS_EDEFAULT == null ? ba40bis != null : !BA40BIS_EDEFAULT.equals(ba40bis);
      case BaActionPackage.BEHAVIOR_ACTION__BA41:
        return ba41 != null;
      case BaActionPackage.BEHAVIOR_ACTION__BA42:
        return BA42_EDEFAULT == null ? ba42 != null : !BA42_EDEFAULT.equals(ba42);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (ba3: ");
    result.append(ba3);
    result.append(", ba3bis: ");
    result.append(ba3bis);
    result.append(", ba5: ");
    result.append(ba5);
    result.append(", ba7: ");
    result.append(ba7);
    result.append(", ba7bis: ");
    result.append(ba7bis);
    result.append(", ba9: ");
    result.append(ba9);
    result.append(", ba11: ");
    result.append(ba11);
    result.append(", ba13: ");
    result.append(ba13);
    result.append(", ba13bis: ");
    result.append(ba13bis);
    result.append(", ba14: ");
    result.append(ba14);
    result.append(", ba14bis: ");
    result.append(ba14bis);
    result.append(", ba15: ");
    result.append(ba15);
    result.append(", ba16: ");
    result.append(ba16);
    result.append(", ba17: ");
    result.append(ba17);
    result.append(", ba18: ");
    result.append(ba18);
    result.append(", ba20: ");
    result.append(ba20);
    result.append(", ba20bis: ");
    result.append(ba20bis);
    result.append(", ba22: ");
    result.append(ba22);
    result.append(", ba23: ");
    result.append(ba23);
    result.append(", ba23bis: ");
    result.append(ba23bis);
    result.append(", ba24: ");
    result.append(ba24);
    result.append(", ba25: ");
    result.append(ba25);
    result.append(", ba26: ");
    result.append(ba26);
    result.append(", ba27: ");
    result.append(ba27);
    result.append(", ba30: ");
    result.append(ba30);
    result.append(", ba30bis: ");
    result.append(ba30bis);
    result.append(", ba32: ");
    result.append(ba32);
    result.append(", ba33: ");
    result.append(ba33);
    result.append(", ba33bis: ");
    result.append(ba33bis);
    result.append(", ba35: ");
    result.append(ba35);
    result.append(", ba35bis: ");
    result.append(ba35bis);
    result.append(", ba37: ");
    result.append(ba37);
    result.append(", ba38: ");
    result.append(ba38);
    result.append(", ba40: ");
    result.append(ba40);
    result.append(", ba40bis: ");
    result.append(ba40bis);
    result.append(", ba42: ");
    result.append(ba42);
    result.append(')');
    return result.toString();
  }

} //behavior_actionImpl
