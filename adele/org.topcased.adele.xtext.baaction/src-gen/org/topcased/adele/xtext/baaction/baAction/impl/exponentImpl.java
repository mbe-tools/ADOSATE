/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.exponent;
import org.topcased.adele.xtext.baaction.baAction.numeral;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>exponent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.exponentImpl#getEname1 <em>Ename1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.exponentImpl#getEname2 <em>Ename2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class exponentImpl extends MinimalEObjectImpl.Container implements exponent
{
  /**
   * The cached value of the '{@link #getEname1() <em>Ename1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEname1()
   * @generated
   * @ordered
   */
  protected numeral ename1;

  /**
   * The cached value of the '{@link #getEname2() <em>Ename2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEname2()
   * @generated
   * @ordered
   */
  protected numeral ename2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected exponentImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.EXPONENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public numeral getEname1()
  {
    return ename1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetEname1(numeral newEname1, NotificationChain msgs)
  {
    numeral oldEname1 = ename1;
    ename1 = newEname1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.EXPONENT__ENAME1, oldEname1, newEname1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEname1(numeral newEname1)
  {
    if (newEname1 != ename1)
    {
      NotificationChain msgs = null;
      if (ename1 != null)
        msgs = ((InternalEObject)ename1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.EXPONENT__ENAME1, null, msgs);
      if (newEname1 != null)
        msgs = ((InternalEObject)newEname1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.EXPONENT__ENAME1, null, msgs);
      msgs = basicSetEname1(newEname1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.EXPONENT__ENAME1, newEname1, newEname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public numeral getEname2()
  {
    return ename2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetEname2(numeral newEname2, NotificationChain msgs)
  {
    numeral oldEname2 = ename2;
    ename2 = newEname2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.EXPONENT__ENAME2, oldEname2, newEname2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEname2(numeral newEname2)
  {
    if (newEname2 != ename2)
    {
      NotificationChain msgs = null;
      if (ename2 != null)
        msgs = ((InternalEObject)ename2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.EXPONENT__ENAME2, null, msgs);
      if (newEname2 != null)
        msgs = ((InternalEObject)newEname2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.EXPONENT__ENAME2, null, msgs);
      msgs = basicSetEname2(newEname2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.EXPONENT__ENAME2, newEname2, newEname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.EXPONENT__ENAME1:
        return basicSetEname1(null, msgs);
      case BaActionPackage.EXPONENT__ENAME2:
        return basicSetEname2(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.EXPONENT__ENAME1:
        return getEname1();
      case BaActionPackage.EXPONENT__ENAME2:
        return getEname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.EXPONENT__ENAME1:
        setEname1((numeral)newValue);
        return;
      case BaActionPackage.EXPONENT__ENAME2:
        setEname2((numeral)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.EXPONENT__ENAME1:
        setEname1((numeral)null);
        return;
      case BaActionPackage.EXPONENT__ENAME2:
        setEname2((numeral)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.EXPONENT__ENAME1:
        return ename1 != null;
      case BaActionPackage.EXPONENT__ENAME2:
        return ename2 != null;
    }
    return super.eIsSet(featureID);
  }

} //exponentImpl
