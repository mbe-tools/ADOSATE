/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionFactory;
import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.array_data_component_reference;
import org.topcased.adele.xtext.baaction.baAction.array_index;
import org.topcased.adele.xtext.baaction.baAction.assignment_action;
import org.topcased.adele.xtext.baaction.baAction.base;
import org.topcased.adele.xtext.baaction.baAction.based_integer_literal;
import org.topcased.adele.xtext.baaction.baAction.based_numeral;
import org.topcased.adele.xtext.baaction.baAction.basic_action;
import org.topcased.adele.xtext.baaction.baAction.behavior_action;
import org.topcased.adele.xtext.baaction.baAction.behavior_action_block;
import org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence;
import org.topcased.adele.xtext.baaction.baAction.behavior_action_set;
import org.topcased.adele.xtext.baaction.baAction.behavior_actions;
import org.topcased.adele.xtext.baaction.baAction.behavior_time;
import org.topcased.adele.xtext.baaction.baAction.binary_adding_operator;
import org.topcased.adele.xtext.baaction.baAction.binary_numeric_operator;
import org.topcased.adele.xtext.baaction.baAction.boolean_literal;
import org.topcased.adele.xtext.baaction.baAction.communication_action;
import org.topcased.adele.xtext.baaction.baAction.data_component_reference;
import org.topcased.adele.xtext.baaction.baAction.decimal_integer_literal;
import org.topcased.adele.xtext.baaction.baAction.decimal_real_literal;
import org.topcased.adele.xtext.baaction.baAction.element_values;
import org.topcased.adele.xtext.baaction.baAction.exponent;
import org.topcased.adele.xtext.baaction.baAction.factor;
import org.topcased.adele.xtext.baaction.baAction.integer;
import org.topcased.adele.xtext.baaction.baAction.integer_literal;
import org.topcased.adele.xtext.baaction.baAction.integer_range;
import org.topcased.adele.xtext.baaction.baAction.integer_value;
import org.topcased.adele.xtext.baaction.baAction.logical_operator;
import org.topcased.adele.xtext.baaction.baAction.multiplying_operator;
import org.topcased.adele.xtext.baaction.baAction.name;
import org.topcased.adele.xtext.baaction.baAction.numeral;
import org.topcased.adele.xtext.baaction.baAction.numeric_literal;
import org.topcased.adele.xtext.baaction.baAction.parameter_label;
import org.topcased.adele.xtext.baaction.baAction.positive_exponent;
import org.topcased.adele.xtext.baaction.baAction.real_literal;
import org.topcased.adele.xtext.baaction.baAction.relation;
import org.topcased.adele.xtext.baaction.baAction.relational_operator;
import org.topcased.adele.xtext.baaction.baAction.simple_expression;
import org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list;
import org.topcased.adele.xtext.baaction.baAction.term;
import org.topcased.adele.xtext.baaction.baAction.timed_action;
import org.topcased.adele.xtext.baaction.baAction.unary_adding_operator;
import org.topcased.adele.xtext.baaction.baAction.unary_boolean_operator;
import org.topcased.adele.xtext.baaction.baAction.unary_numeric_operator;
import org.topcased.adele.xtext.baaction.baAction.unit_identifier;
import org.topcased.adele.xtext.baaction.baAction.value;
import org.topcased.adele.xtext.baaction.baAction.value_constant;
import org.topcased.adele.xtext.baaction.baAction.value_expression;
import org.topcased.adele.xtext.baaction.baAction.value_variable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BaActionPackageImpl extends EPackageImpl implements BaActionPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass behavior_action_blockEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass behavior_actionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass behavior_actionsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass behavior_action_sequenceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass behavior_action_setEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass element_valuesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass array_data_component_referenceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass basic_actionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass assignment_actionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass communication_actionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass timed_actionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass subprogram_parameter_listEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass parameter_labelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass data_component_referenceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass nameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass array_indexEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integer_valueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass numeric_literalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integer_literalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass real_literalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass decimal_integer_literalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass decimal_real_literalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass numeralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass exponentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass positive_exponentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass based_integer_literalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass baseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass based_numeralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass behavior_timeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unit_identifierEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integer_rangeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integerEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass value_expressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass relationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simple_expressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass termEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass factorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass valueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass value_variableEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass value_constantEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass logical_operatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass relational_operatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass binary_adding_operatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unary_adding_operatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass multiplying_operatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass binary_numeric_operatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unary_numeric_operatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unary_boolean_operatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boolean_literalEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private BaActionPackageImpl()
  {
    super(eNS_URI, BaActionFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link BaActionPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static BaActionPackage init()
  {
    if (isInited) return (BaActionPackage)EPackage.Registry.INSTANCE.getEPackage(BaActionPackage.eNS_URI);

    // Obtain or create and register package
    BaActionPackageImpl theBaActionPackage = (BaActionPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof BaActionPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new BaActionPackageImpl());

    isInited = true;

    // Create package meta-data objects
    theBaActionPackage.createPackageContents();

    // Initialize created meta-data
    theBaActionPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theBaActionPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(BaActionPackage.eNS_URI, theBaActionPackage);
    return theBaActionPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbehavior_action_block()
  {
    return behavior_action_blockEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_block_Bac1()
  {
    return (EAttribute)behavior_action_blockEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_block_Bac2()
  {
    return (EReference)behavior_action_blockEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_block_Bac3()
  {
    return (EAttribute)behavior_action_blockEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_block_Bac4()
  {
    return (EAttribute)behavior_action_blockEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_block_Bac5()
  {
    return (EReference)behavior_action_blockEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbehavior_action()
  {
    return behavior_actionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_Ba1()
  {
    return (EReference)behavior_actionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_Ba2()
  {
    return (EReference)behavior_actionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba3()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba3bis()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_Ba4()
  {
    return (EReference)behavior_actionEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba5()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_Ba6()
  {
    return (EReference)behavior_actionEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba7()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba7bis()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_Ba8()
  {
    return (EReference)behavior_actionEClass.getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba9()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(10);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_Ba10()
  {
    return (EReference)behavior_actionEClass.getEStructuralFeatures().get(11);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba11()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(12);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_Ba12()
  {
    return (EReference)behavior_actionEClass.getEStructuralFeatures().get(13);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba13()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(14);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba13bis()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(15);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba14()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(16);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba14bis()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(17);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba15()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(18);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba16()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(19);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba17()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(20);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba18()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(21);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_Ba19()
  {
    return (EReference)behavior_actionEClass.getEStructuralFeatures().get(22);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba20()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(23);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba20bis()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(24);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_Ba21()
  {
    return (EReference)behavior_actionEClass.getEStructuralFeatures().get(25);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba22()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(26);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba23()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(27);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba23bis()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(28);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba24()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(29);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba25()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(30);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba26()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(31);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba27()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(32);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_Ba29()
  {
    return (EReference)behavior_actionEClass.getEStructuralFeatures().get(33);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba30()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(34);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba30bis()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(35);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_Ba31()
  {
    return (EReference)behavior_actionEClass.getEStructuralFeatures().get(36);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba32()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(37);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba33()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(38);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba33bis()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(39);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_Ba34()
  {
    return (EReference)behavior_actionEClass.getEStructuralFeatures().get(40);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba35()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(41);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba35bis()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(42);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_Ba36()
  {
    return (EReference)behavior_actionEClass.getEStructuralFeatures().get(43);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba37()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(44);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba38()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(45);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_Ba39()
  {
    return (EReference)behavior_actionEClass.getEStructuralFeatures().get(46);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba40()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(47);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba40bis()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(48);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_Ba41()
  {
    return (EReference)behavior_actionEClass.getEStructuralFeatures().get(49);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_Ba42()
  {
    return (EAttribute)behavior_actionEClass.getEStructuralFeatures().get(50);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbehavior_actions()
  {
    return behavior_actionsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_actions_Bas1()
  {
    return (EReference)behavior_actionsEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_actions_Baseq2()
  {
    return (EAttribute)behavior_actionsEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_actions_Baseq4()
  {
    return (EAttribute)behavior_actionsEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_actions_Baseq3()
  {
    return (EReference)behavior_actionsEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbehavior_action_sequence()
  {
    return behavior_action_sequenceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_sequence_Baseq1()
  {
    return (EReference)behavior_action_sequenceEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_sequence_Baseq2()
  {
    return (EAttribute)behavior_action_sequenceEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_sequence_Baseq3()
  {
    return (EReference)behavior_action_sequenceEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbehavior_action_set()
  {
    return behavior_action_setEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_set_Baset1()
  {
    return (EReference)behavior_action_setEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbehavior_action_set_Baset2()
  {
    return (EAttribute)behavior_action_setEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_action_set_Baset3()
  {
    return (EReference)behavior_action_setEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getelement_values()
  {
    return element_valuesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getelement_values_Ev1()
  {
    return (EReference)element_valuesEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getelement_values_Ev2()
  {
    return (EAttribute)element_valuesEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getelement_values_Ev3()
  {
    return (EReference)element_valuesEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getarray_data_component_reference()
  {
    return array_data_component_referenceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getarray_data_component_reference_Adcr1()
  {
    return (EAttribute)array_data_component_referenceEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getarray_data_component_reference_Adcr2()
  {
    return (EReference)array_data_component_referenceEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getarray_data_component_reference_Adcr3()
  {
    return (EAttribute)array_data_component_referenceEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getarray_data_component_reference_Adcr4()
  {
    return (EReference)array_data_component_referenceEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getarray_data_component_reference_Adcr5()
  {
    return (EAttribute)array_data_component_referenceEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbasic_action()
  {
    return basic_actionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbasic_action_Bact1()
  {
    return (EReference)basic_actionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbasic_action_Bact2()
  {
    return (EReference)basic_actionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbasic_action_Bact3()
  {
    return (EReference)basic_actionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getassignment_action()
  {
    return assignment_actionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getassignment_action_Aa1()
  {
    return (EAttribute)assignment_actionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getassignment_action_Aa2()
  {
    return (EAttribute)assignment_actionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getassignment_action_Aa3()
  {
    return (EReference)assignment_actionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getassignment_action_Aa4()
  {
    return (EAttribute)assignment_actionEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getcommunication_action()
  {
    return communication_actionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca1()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca2()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca3()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getcommunication_action_Ca4()
  {
    return (EReference)communication_actionEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca5()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca21()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca22()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca23()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca24()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca25()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca26()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(10);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca27()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(11);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca28()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(12);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca29()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(13);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca30()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(14);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca31()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(15);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca32()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(16);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcommunication_action_Ca33()
  {
    return (EAttribute)communication_actionEClass.getEStructuralFeatures().get(17);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass gettimed_action()
  {
    return timed_actionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute gettimed_action_Ta1()
  {
    return (EAttribute)timed_actionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute gettimed_action_Ta1bis()
  {
    return (EAttribute)timed_actionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference gettimed_action_Ta2()
  {
    return (EReference)timed_actionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute gettimed_action_Ta3()
  {
    return (EAttribute)timed_actionEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference gettimed_action_Ta4()
  {
    return (EReference)timed_actionEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute gettimed_action_Ta5()
  {
    return (EAttribute)timed_actionEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getsubprogram_parameter_list()
  {
    return subprogram_parameter_listEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getsubprogram_parameter_list_Spl1()
  {
    return (EReference)subprogram_parameter_listEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getsubprogram_parameter_list_Spl2()
  {
    return (EAttribute)subprogram_parameter_listEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getsubprogram_parameter_list_Spl3()
  {
    return (EReference)subprogram_parameter_listEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getparameter_label()
  {
    return parameter_labelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getparameter_label_Pl1()
  {
    return (EReference)parameter_labelEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getdata_component_reference()
  {
    return data_component_referenceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getdata_component_reference_Dcr1()
  {
    return (EAttribute)data_component_referenceEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getdata_component_reference_Dcr2()
  {
    return (EAttribute)data_component_referenceEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getdata_component_reference_Dcr3()
  {
    return (EAttribute)data_component_referenceEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getname()
  {
    return nameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getname_N1()
  {
    return (EAttribute)nameEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getname_N2()
  {
    return (EReference)nameEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getarray_index()
  {
    return array_indexEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getarray_index_Ai1()
  {
    return (EAttribute)array_indexEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getarray_index_Ai2()
  {
    return (EReference)array_indexEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getarray_index_Ai3()
  {
    return (EAttribute)array_indexEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getinteger_value()
  {
    return integer_valueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getinteger_value_Ivname()
  {
    return (EReference)integer_valueEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getnumeric_literal()
  {
    return numeric_literalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getnumeric_literal_Nlname1()
  {
    return (EReference)numeric_literalEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getnumeric_literal_Nlname2()
  {
    return (EReference)numeric_literalEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getinteger_literal()
  {
    return integer_literalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getinteger_literal_Ilname1()
  {
    return (EReference)integer_literalEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getinteger_literal_Ilname2()
  {
    return (EReference)integer_literalEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getreal_literal()
  {
    return real_literalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getreal_literal_Rlname()
  {
    return (EReference)real_literalEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getdecimal_integer_literal()
  {
    return decimal_integer_literalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdecimal_integer_literal_Dilname1()
  {
    return (EReference)decimal_integer_literalEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdecimal_integer_literal_Dilname2()
  {
    return (EReference)decimal_integer_literalEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getdecimal_real_literal()
  {
    return decimal_real_literalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdecimal_real_literal_Drlname1()
  {
    return (EReference)decimal_real_literalEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdecimal_real_literal_Drlname2()
  {
    return (EReference)decimal_real_literalEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getdecimal_real_literal_Drlname3()
  {
    return (EReference)decimal_real_literalEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getnumeral()
  {
    return numeralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getnumeral_Nname1()
  {
    return (EReference)numeralEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getnumeral_Nname2()
  {
    return (EReference)numeralEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getexponent()
  {
    return exponentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getexponent_Ename1()
  {
    return (EReference)exponentEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getexponent_Ename2()
  {
    return (EReference)exponentEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getpositive_exponent()
  {
    return positive_exponentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getpositive_exponent_Pename()
  {
    return (EReference)positive_exponentEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbased_integer_literal()
  {
    return based_integer_literalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbased_integer_literal_Bilname1()
  {
    return (EReference)based_integer_literalEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbased_integer_literal_Bilname2()
  {
    return (EReference)based_integer_literalEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbased_integer_literal_Bilname3()
  {
    return (EReference)based_integer_literalEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbase()
  {
    return baseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbase_Bname1()
  {
    return (EAttribute)baseEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbase_Bname2()
  {
    return (EAttribute)baseEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbased_numeral()
  {
    return based_numeralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbased_numeral_Bnname1()
  {
    return (EAttribute)based_numeralEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbased_numeral_Bnname2()
  {
    return (EAttribute)based_numeralEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbehavior_time()
  {
    return behavior_timeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_time_Btname1()
  {
    return (EReference)behavior_timeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getbehavior_time_Btname2()
  {
    return (EReference)behavior_timeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getunit_identifier()
  {
    return unit_identifierEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunit_identifier_Uiname1()
  {
    return (EAttribute)unit_identifierEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunit_identifier_Uiname2()
  {
    return (EAttribute)unit_identifierEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunit_identifier_Uiname3()
  {
    return (EAttribute)unit_identifierEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunit_identifier_Uiname4()
  {
    return (EAttribute)unit_identifierEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunit_identifier_Uiname5()
  {
    return (EAttribute)unit_identifierEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunit_identifier_Uiname6()
  {
    return (EAttribute)unit_identifierEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunit_identifier_Uiname7()
  {
    return (EAttribute)unit_identifierEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getinteger_range()
  {
    return integer_rangeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getinteger_range_Min()
  {
    return (EReference)integer_rangeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getinteger_range_Irsep()
  {
    return (EAttribute)integer_rangeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getinteger_range_Max()
  {
    return (EReference)integer_rangeEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getinteger()
  {
    return integerEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getinteger_Value1()
  {
    return (EAttribute)integerEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getinteger_Value2()
  {
    return (EAttribute)integerEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getvalue_expression()
  {
    return value_expressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getvalue_expression_Vename1()
  {
    return (EReference)value_expressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getvalue_expression_Vename3()
  {
    return (EReference)value_expressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getvalue_expression_Vename2()
  {
    return (EReference)value_expressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getrelation()
  {
    return relationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getrelation_Rname1()
  {
    return (EReference)relationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getrelation_Rname3()
  {
    return (EReference)relationEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getrelation_Rname2()
  {
    return (EReference)relationEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getsimple_expression()
  {
    return simple_expressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getsimple_expression_Sename2()
  {
    return (EReference)simple_expressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getsimple_expression_Sename1()
  {
    return (EReference)simple_expressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getsimple_expression_Sename3()
  {
    return (EReference)simple_expressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getsimple_expression_Sename4()
  {
    return (EReference)simple_expressionEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getterm()
  {
    return termEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getterm_Tname1()
  {
    return (EReference)termEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getterm_Tname3()
  {
    return (EReference)termEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getterm_Tname2()
  {
    return (EReference)termEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getfactor()
  {
    return factorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getfactor_Fname1()
  {
    return (EReference)factorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getfactor_Fname3()
  {
    return (EReference)factorEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getfactor_Fname2()
  {
    return (EReference)factorEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getfactor_Fname4()
  {
    return (EReference)factorEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getfactor_Fname5()
  {
    return (EReference)factorEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getfactor_Fname6()
  {
    return (EReference)factorEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getfactor_Fname7()
  {
    return (EReference)factorEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getvalue()
  {
    return valueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getvalue_Vname1()
  {
    return (EReference)valueEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getvalue_Vname2()
  {
    return (EReference)valueEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getvalue_variable()
  {
    return value_variableEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getvalue_variable_Vvname0()
  {
    return (EAttribute)value_variableEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getvalue_variable_Vvname1()
  {
    return (EAttribute)value_variableEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getvalue_variable_Vvname2()
  {
    return (EAttribute)value_variableEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getvalue_variable_Vvname3()
  {
    return (EAttribute)value_variableEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getvalue_constant()
  {
    return value_constantEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getvalue_constant_Vcname1()
  {
    return (EReference)value_constantEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getvalue_constant_Vcname2()
  {
    return (EReference)value_constantEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getvalue_constant_Vcname3()
  {
    return (EAttribute)value_constantEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getvalue_constant_Vcname4()
  {
    return (EAttribute)value_constantEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getlogical_operator()
  {
    return logical_operatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getlogical_operator_Loname1()
  {
    return (EAttribute)logical_operatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getlogical_operator_Loname2()
  {
    return (EAttribute)logical_operatorEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getlogical_operator_Loname3()
  {
    return (EAttribute)logical_operatorEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getrelational_operator()
  {
    return relational_operatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getrelational_operator_Roname1()
  {
    return (EAttribute)relational_operatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getrelational_operator_Roname2()
  {
    return (EAttribute)relational_operatorEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getrelational_operator_Roname3()
  {
    return (EAttribute)relational_operatorEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getrelational_operator_Roname4()
  {
    return (EAttribute)relational_operatorEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getrelational_operator_Roname5()
  {
    return (EAttribute)relational_operatorEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getrelational_operator_Roname6()
  {
    return (EAttribute)relational_operatorEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbinary_adding_operator()
  {
    return binary_adding_operatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbinary_adding_operator_Baoname1()
  {
    return (EAttribute)binary_adding_operatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbinary_adding_operator_Baoname2()
  {
    return (EAttribute)binary_adding_operatorEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getunary_adding_operator()
  {
    return unary_adding_operatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunary_adding_operator_Uaoname1()
  {
    return (EAttribute)unary_adding_operatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunary_adding_operator_Uaoname2()
  {
    return (EAttribute)unary_adding_operatorEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getmultiplying_operator()
  {
    return multiplying_operatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getmultiplying_operator_Moname1()
  {
    return (EAttribute)multiplying_operatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getmultiplying_operator_Moname2()
  {
    return (EAttribute)multiplying_operatorEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getmultiplying_operator_Moname3()
  {
    return (EAttribute)multiplying_operatorEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getmultiplying_operator_Moname4()
  {
    return (EAttribute)multiplying_operatorEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getbinary_numeric_operator()
  {
    return binary_numeric_operatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getbinary_numeric_operator_Bnoname()
  {
    return (EAttribute)binary_numeric_operatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getunary_numeric_operator()
  {
    return unary_numeric_operatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunary_numeric_operator_Unoname()
  {
    return (EAttribute)unary_numeric_operatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getunary_boolean_operator()
  {
    return unary_boolean_operatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getunary_boolean_operator_Uboname()
  {
    return (EAttribute)unary_boolean_operatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getboolean_literal()
  {
    return boolean_literalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getboolean_literal_Blname1()
  {
    return (EAttribute)boolean_literalEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaActionFactory getBaActionFactory()
  {
    return (BaActionFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    behavior_action_blockEClass = createEClass(BEHAVIOR_ACTION_BLOCK);
    createEAttribute(behavior_action_blockEClass, BEHAVIOR_ACTION_BLOCK__BAC1);
    createEReference(behavior_action_blockEClass, BEHAVIOR_ACTION_BLOCK__BAC2);
    createEAttribute(behavior_action_blockEClass, BEHAVIOR_ACTION_BLOCK__BAC3);
    createEAttribute(behavior_action_blockEClass, BEHAVIOR_ACTION_BLOCK__BAC4);
    createEReference(behavior_action_blockEClass, BEHAVIOR_ACTION_BLOCK__BAC5);

    behavior_actionEClass = createEClass(BEHAVIOR_ACTION);
    createEReference(behavior_actionEClass, BEHAVIOR_ACTION__BA1);
    createEReference(behavior_actionEClass, BEHAVIOR_ACTION__BA2);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA3);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA3BIS);
    createEReference(behavior_actionEClass, BEHAVIOR_ACTION__BA4);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA5);
    createEReference(behavior_actionEClass, BEHAVIOR_ACTION__BA6);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA7);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA7BIS);
    createEReference(behavior_actionEClass, BEHAVIOR_ACTION__BA8);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA9);
    createEReference(behavior_actionEClass, BEHAVIOR_ACTION__BA10);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA11);
    createEReference(behavior_actionEClass, BEHAVIOR_ACTION__BA12);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA13);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA13BIS);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA14);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA14BIS);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA15);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA16);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA17);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA18);
    createEReference(behavior_actionEClass, BEHAVIOR_ACTION__BA19);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA20);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA20BIS);
    createEReference(behavior_actionEClass, BEHAVIOR_ACTION__BA21);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA22);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA23);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA23BIS);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA24);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA25);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA26);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA27);
    createEReference(behavior_actionEClass, BEHAVIOR_ACTION__BA29);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA30);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA30BIS);
    createEReference(behavior_actionEClass, BEHAVIOR_ACTION__BA31);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA32);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA33);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA33BIS);
    createEReference(behavior_actionEClass, BEHAVIOR_ACTION__BA34);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA35);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA35BIS);
    createEReference(behavior_actionEClass, BEHAVIOR_ACTION__BA36);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA37);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA38);
    createEReference(behavior_actionEClass, BEHAVIOR_ACTION__BA39);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA40);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA40BIS);
    createEReference(behavior_actionEClass, BEHAVIOR_ACTION__BA41);
    createEAttribute(behavior_actionEClass, BEHAVIOR_ACTION__BA42);

    behavior_actionsEClass = createEClass(BEHAVIOR_ACTIONS);
    createEReference(behavior_actionsEClass, BEHAVIOR_ACTIONS__BAS1);
    createEAttribute(behavior_actionsEClass, BEHAVIOR_ACTIONS__BASEQ2);
    createEAttribute(behavior_actionsEClass, BEHAVIOR_ACTIONS__BASEQ4);
    createEReference(behavior_actionsEClass, BEHAVIOR_ACTIONS__BASEQ3);

    behavior_action_sequenceEClass = createEClass(BEHAVIOR_ACTION_SEQUENCE);
    createEReference(behavior_action_sequenceEClass, BEHAVIOR_ACTION_SEQUENCE__BASEQ1);
    createEAttribute(behavior_action_sequenceEClass, BEHAVIOR_ACTION_SEQUENCE__BASEQ2);
    createEReference(behavior_action_sequenceEClass, BEHAVIOR_ACTION_SEQUENCE__BASEQ3);

    behavior_action_setEClass = createEClass(BEHAVIOR_ACTION_SET);
    createEReference(behavior_action_setEClass, BEHAVIOR_ACTION_SET__BASET1);
    createEAttribute(behavior_action_setEClass, BEHAVIOR_ACTION_SET__BASET2);
    createEReference(behavior_action_setEClass, BEHAVIOR_ACTION_SET__BASET3);

    element_valuesEClass = createEClass(ELEMENT_VALUES);
    createEReference(element_valuesEClass, ELEMENT_VALUES__EV1);
    createEAttribute(element_valuesEClass, ELEMENT_VALUES__EV2);
    createEReference(element_valuesEClass, ELEMENT_VALUES__EV3);

    array_data_component_referenceEClass = createEClass(ARRAY_DATA_COMPONENT_REFERENCE);
    createEAttribute(array_data_component_referenceEClass, ARRAY_DATA_COMPONENT_REFERENCE__ADCR1);
    createEReference(array_data_component_referenceEClass, ARRAY_DATA_COMPONENT_REFERENCE__ADCR2);
    createEAttribute(array_data_component_referenceEClass, ARRAY_DATA_COMPONENT_REFERENCE__ADCR3);
    createEReference(array_data_component_referenceEClass, ARRAY_DATA_COMPONENT_REFERENCE__ADCR4);
    createEAttribute(array_data_component_referenceEClass, ARRAY_DATA_COMPONENT_REFERENCE__ADCR5);

    basic_actionEClass = createEClass(BASIC_ACTION);
    createEReference(basic_actionEClass, BASIC_ACTION__BACT1);
    createEReference(basic_actionEClass, BASIC_ACTION__BACT2);
    createEReference(basic_actionEClass, BASIC_ACTION__BACT3);

    assignment_actionEClass = createEClass(ASSIGNMENT_ACTION);
    createEAttribute(assignment_actionEClass, ASSIGNMENT_ACTION__AA1);
    createEAttribute(assignment_actionEClass, ASSIGNMENT_ACTION__AA2);
    createEReference(assignment_actionEClass, ASSIGNMENT_ACTION__AA3);
    createEAttribute(assignment_actionEClass, ASSIGNMENT_ACTION__AA4);

    communication_actionEClass = createEClass(COMMUNICATION_ACTION);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA1);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA2);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA3);
    createEReference(communication_actionEClass, COMMUNICATION_ACTION__CA4);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA5);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA21);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA22);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA23);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA24);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA25);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA26);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA27);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA28);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA29);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA30);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA31);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA32);
    createEAttribute(communication_actionEClass, COMMUNICATION_ACTION__CA33);

    timed_actionEClass = createEClass(TIMED_ACTION);
    createEAttribute(timed_actionEClass, TIMED_ACTION__TA1);
    createEAttribute(timed_actionEClass, TIMED_ACTION__TA1BIS);
    createEReference(timed_actionEClass, TIMED_ACTION__TA2);
    createEAttribute(timed_actionEClass, TIMED_ACTION__TA3);
    createEReference(timed_actionEClass, TIMED_ACTION__TA4);
    createEAttribute(timed_actionEClass, TIMED_ACTION__TA5);

    subprogram_parameter_listEClass = createEClass(SUBPROGRAM_PARAMETER_LIST);
    createEReference(subprogram_parameter_listEClass, SUBPROGRAM_PARAMETER_LIST__SPL1);
    createEAttribute(subprogram_parameter_listEClass, SUBPROGRAM_PARAMETER_LIST__SPL2);
    createEReference(subprogram_parameter_listEClass, SUBPROGRAM_PARAMETER_LIST__SPL3);

    parameter_labelEClass = createEClass(PARAMETER_LABEL);
    createEReference(parameter_labelEClass, PARAMETER_LABEL__PL1);

    data_component_referenceEClass = createEClass(DATA_COMPONENT_REFERENCE);
    createEAttribute(data_component_referenceEClass, DATA_COMPONENT_REFERENCE__DCR1);
    createEAttribute(data_component_referenceEClass, DATA_COMPONENT_REFERENCE__DCR2);
    createEAttribute(data_component_referenceEClass, DATA_COMPONENT_REFERENCE__DCR3);

    nameEClass = createEClass(NAME);
    createEAttribute(nameEClass, NAME__N1);
    createEReference(nameEClass, NAME__N2);

    array_indexEClass = createEClass(ARRAY_INDEX);
    createEAttribute(array_indexEClass, ARRAY_INDEX__AI1);
    createEReference(array_indexEClass, ARRAY_INDEX__AI2);
    createEAttribute(array_indexEClass, ARRAY_INDEX__AI3);

    integer_valueEClass = createEClass(INTEGER_VALUE);
    createEReference(integer_valueEClass, INTEGER_VALUE__IVNAME);

    numeric_literalEClass = createEClass(NUMERIC_LITERAL);
    createEReference(numeric_literalEClass, NUMERIC_LITERAL__NLNAME1);
    createEReference(numeric_literalEClass, NUMERIC_LITERAL__NLNAME2);

    integer_literalEClass = createEClass(INTEGER_LITERAL);
    createEReference(integer_literalEClass, INTEGER_LITERAL__ILNAME1);
    createEReference(integer_literalEClass, INTEGER_LITERAL__ILNAME2);

    real_literalEClass = createEClass(REAL_LITERAL);
    createEReference(real_literalEClass, REAL_LITERAL__RLNAME);

    decimal_integer_literalEClass = createEClass(DECIMAL_INTEGER_LITERAL);
    createEReference(decimal_integer_literalEClass, DECIMAL_INTEGER_LITERAL__DILNAME1);
    createEReference(decimal_integer_literalEClass, DECIMAL_INTEGER_LITERAL__DILNAME2);

    decimal_real_literalEClass = createEClass(DECIMAL_REAL_LITERAL);
    createEReference(decimal_real_literalEClass, DECIMAL_REAL_LITERAL__DRLNAME1);
    createEReference(decimal_real_literalEClass, DECIMAL_REAL_LITERAL__DRLNAME2);
    createEReference(decimal_real_literalEClass, DECIMAL_REAL_LITERAL__DRLNAME3);

    numeralEClass = createEClass(NUMERAL);
    createEReference(numeralEClass, NUMERAL__NNAME1);
    createEReference(numeralEClass, NUMERAL__NNAME2);

    exponentEClass = createEClass(EXPONENT);
    createEReference(exponentEClass, EXPONENT__ENAME1);
    createEReference(exponentEClass, EXPONENT__ENAME2);

    positive_exponentEClass = createEClass(POSITIVE_EXPONENT);
    createEReference(positive_exponentEClass, POSITIVE_EXPONENT__PENAME);

    based_integer_literalEClass = createEClass(BASED_INTEGER_LITERAL);
    createEReference(based_integer_literalEClass, BASED_INTEGER_LITERAL__BILNAME1);
    createEReference(based_integer_literalEClass, BASED_INTEGER_LITERAL__BILNAME2);
    createEReference(based_integer_literalEClass, BASED_INTEGER_LITERAL__BILNAME3);

    baseEClass = createEClass(BASE);
    createEAttribute(baseEClass, BASE__BNAME1);
    createEAttribute(baseEClass, BASE__BNAME2);

    based_numeralEClass = createEClass(BASED_NUMERAL);
    createEAttribute(based_numeralEClass, BASED_NUMERAL__BNNAME1);
    createEAttribute(based_numeralEClass, BASED_NUMERAL__BNNAME2);

    behavior_timeEClass = createEClass(BEHAVIOR_TIME);
    createEReference(behavior_timeEClass, BEHAVIOR_TIME__BTNAME1);
    createEReference(behavior_timeEClass, BEHAVIOR_TIME__BTNAME2);

    unit_identifierEClass = createEClass(UNIT_IDENTIFIER);
    createEAttribute(unit_identifierEClass, UNIT_IDENTIFIER__UINAME1);
    createEAttribute(unit_identifierEClass, UNIT_IDENTIFIER__UINAME2);
    createEAttribute(unit_identifierEClass, UNIT_IDENTIFIER__UINAME3);
    createEAttribute(unit_identifierEClass, UNIT_IDENTIFIER__UINAME4);
    createEAttribute(unit_identifierEClass, UNIT_IDENTIFIER__UINAME5);
    createEAttribute(unit_identifierEClass, UNIT_IDENTIFIER__UINAME6);
    createEAttribute(unit_identifierEClass, UNIT_IDENTIFIER__UINAME7);

    integer_rangeEClass = createEClass(INTEGER_RANGE);
    createEReference(integer_rangeEClass, INTEGER_RANGE__MIN);
    createEAttribute(integer_rangeEClass, INTEGER_RANGE__IRSEP);
    createEReference(integer_rangeEClass, INTEGER_RANGE__MAX);

    integerEClass = createEClass(INTEGER);
    createEAttribute(integerEClass, INTEGER__VALUE1);
    createEAttribute(integerEClass, INTEGER__VALUE2);

    value_expressionEClass = createEClass(VALUE_EXPRESSION);
    createEReference(value_expressionEClass, VALUE_EXPRESSION__VENAME1);
    createEReference(value_expressionEClass, VALUE_EXPRESSION__VENAME3);
    createEReference(value_expressionEClass, VALUE_EXPRESSION__VENAME2);

    relationEClass = createEClass(RELATION);
    createEReference(relationEClass, RELATION__RNAME1);
    createEReference(relationEClass, RELATION__RNAME3);
    createEReference(relationEClass, RELATION__RNAME2);

    simple_expressionEClass = createEClass(SIMPLE_EXPRESSION);
    createEReference(simple_expressionEClass, SIMPLE_EXPRESSION__SENAME2);
    createEReference(simple_expressionEClass, SIMPLE_EXPRESSION__SENAME1);
    createEReference(simple_expressionEClass, SIMPLE_EXPRESSION__SENAME3);
    createEReference(simple_expressionEClass, SIMPLE_EXPRESSION__SENAME4);

    termEClass = createEClass(TERM);
    createEReference(termEClass, TERM__TNAME1);
    createEReference(termEClass, TERM__TNAME3);
    createEReference(termEClass, TERM__TNAME2);

    factorEClass = createEClass(FACTOR);
    createEReference(factorEClass, FACTOR__FNAME1);
    createEReference(factorEClass, FACTOR__FNAME3);
    createEReference(factorEClass, FACTOR__FNAME2);
    createEReference(factorEClass, FACTOR__FNAME4);
    createEReference(factorEClass, FACTOR__FNAME5);
    createEReference(factorEClass, FACTOR__FNAME6);
    createEReference(factorEClass, FACTOR__FNAME7);

    valueEClass = createEClass(VALUE);
    createEReference(valueEClass, VALUE__VNAME1);
    createEReference(valueEClass, VALUE__VNAME2);

    value_variableEClass = createEClass(VALUE_VARIABLE);
    createEAttribute(value_variableEClass, VALUE_VARIABLE__VVNAME0);
    createEAttribute(value_variableEClass, VALUE_VARIABLE__VVNAME1);
    createEAttribute(value_variableEClass, VALUE_VARIABLE__VVNAME2);
    createEAttribute(value_variableEClass, VALUE_VARIABLE__VVNAME3);

    value_constantEClass = createEClass(VALUE_CONSTANT);
    createEReference(value_constantEClass, VALUE_CONSTANT__VCNAME1);
    createEReference(value_constantEClass, VALUE_CONSTANT__VCNAME2);
    createEAttribute(value_constantEClass, VALUE_CONSTANT__VCNAME3);
    createEAttribute(value_constantEClass, VALUE_CONSTANT__VCNAME4);

    logical_operatorEClass = createEClass(LOGICAL_OPERATOR);
    createEAttribute(logical_operatorEClass, LOGICAL_OPERATOR__LONAME1);
    createEAttribute(logical_operatorEClass, LOGICAL_OPERATOR__LONAME2);
    createEAttribute(logical_operatorEClass, LOGICAL_OPERATOR__LONAME3);

    relational_operatorEClass = createEClass(RELATIONAL_OPERATOR);
    createEAttribute(relational_operatorEClass, RELATIONAL_OPERATOR__RONAME1);
    createEAttribute(relational_operatorEClass, RELATIONAL_OPERATOR__RONAME2);
    createEAttribute(relational_operatorEClass, RELATIONAL_OPERATOR__RONAME3);
    createEAttribute(relational_operatorEClass, RELATIONAL_OPERATOR__RONAME4);
    createEAttribute(relational_operatorEClass, RELATIONAL_OPERATOR__RONAME5);
    createEAttribute(relational_operatorEClass, RELATIONAL_OPERATOR__RONAME6);

    binary_adding_operatorEClass = createEClass(BINARY_ADDING_OPERATOR);
    createEAttribute(binary_adding_operatorEClass, BINARY_ADDING_OPERATOR__BAONAME1);
    createEAttribute(binary_adding_operatorEClass, BINARY_ADDING_OPERATOR__BAONAME2);

    unary_adding_operatorEClass = createEClass(UNARY_ADDING_OPERATOR);
    createEAttribute(unary_adding_operatorEClass, UNARY_ADDING_OPERATOR__UAONAME1);
    createEAttribute(unary_adding_operatorEClass, UNARY_ADDING_OPERATOR__UAONAME2);

    multiplying_operatorEClass = createEClass(MULTIPLYING_OPERATOR);
    createEAttribute(multiplying_operatorEClass, MULTIPLYING_OPERATOR__MONAME1);
    createEAttribute(multiplying_operatorEClass, MULTIPLYING_OPERATOR__MONAME2);
    createEAttribute(multiplying_operatorEClass, MULTIPLYING_OPERATOR__MONAME3);
    createEAttribute(multiplying_operatorEClass, MULTIPLYING_OPERATOR__MONAME4);

    binary_numeric_operatorEClass = createEClass(BINARY_NUMERIC_OPERATOR);
    createEAttribute(binary_numeric_operatorEClass, BINARY_NUMERIC_OPERATOR__BNONAME);

    unary_numeric_operatorEClass = createEClass(UNARY_NUMERIC_OPERATOR);
    createEAttribute(unary_numeric_operatorEClass, UNARY_NUMERIC_OPERATOR__UNONAME);

    unary_boolean_operatorEClass = createEClass(UNARY_BOOLEAN_OPERATOR);
    createEAttribute(unary_boolean_operatorEClass, UNARY_BOOLEAN_OPERATOR__UBONAME);

    boolean_literalEClass = createEClass(BOOLEAN_LITERAL);
    createEAttribute(boolean_literalEClass, BOOLEAN_LITERAL__BLNAME1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes

    // Initialize classes and features; add operations and parameters
    initEClass(behavior_action_blockEClass, behavior_action_block.class, "behavior_action_block", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getbehavior_action_block_Bac1(), ecorePackage.getEString(), "bac1", null, 0, 1, behavior_action_block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_block_Bac2(), this.getbehavior_actions(), null, "bac2", null, 0, 1, behavior_action_block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_block_Bac3(), ecorePackage.getEString(), "bac3", null, 0, 1, behavior_action_block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_block_Bac4(), ecorePackage.getEString(), "bac4", null, 0, 1, behavior_action_block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_block_Bac5(), this.getbehavior_time(), null, "bac5", null, 0, 1, behavior_action_block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(behavior_actionEClass, behavior_action.class, "behavior_action", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getbehavior_action_Ba1(), this.getbasic_action(), null, "ba1", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_Ba2(), this.getbehavior_action_block(), null, "ba2", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba3(), ecorePackage.getEString(), "ba3", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba3bis(), ecorePackage.getEString(), "ba3bis", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_Ba4(), this.getvalue_expression(), null, "ba4", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba5(), ecorePackage.getEString(), "ba5", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_Ba6(), this.getbehavior_actions(), null, "ba6", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba7(), ecorePackage.getEString(), "ba7", null, 0, -1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba7bis(), ecorePackage.getEString(), "ba7bis", null, 0, -1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_Ba8(), this.getvalue_expression(), null, "ba8", null, 0, -1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba9(), ecorePackage.getEString(), "ba9", null, 0, -1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_Ba10(), this.getbehavior_actions(), null, "ba10", null, 0, -1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba11(), ecorePackage.getEString(), "ba11", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_Ba12(), this.getbehavior_actions(), null, "ba12", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba13(), ecorePackage.getEString(), "ba13", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba13bis(), ecorePackage.getEString(), "ba13bis", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba14(), ecorePackage.getEString(), "ba14", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba14bis(), ecorePackage.getEString(), "ba14bis", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba15(), ecorePackage.getEString(), "ba15", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba16(), ecorePackage.getEString(), "ba16", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba17(), ecorePackage.getEString(), "ba17", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba18(), ecorePackage.getEString(), "ba18", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_Ba19(), this.getelement_values(), null, "ba19", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba20(), ecorePackage.getEString(), "ba20", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba20bis(), ecorePackage.getEString(), "ba20bis", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_Ba21(), this.getbehavior_actions(), null, "ba21", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba22(), ecorePackage.getEString(), "ba22", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba23(), ecorePackage.getEString(), "ba23", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba23bis(), ecorePackage.getEString(), "ba23bis", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba24(), ecorePackage.getEString(), "ba24", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba25(), ecorePackage.getEString(), "ba25", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba26(), ecorePackage.getEString(), "ba26", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba27(), ecorePackage.getEString(), "ba27", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_Ba29(), this.getelement_values(), null, "ba29", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba30(), ecorePackage.getEString(), "ba30", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba30bis(), ecorePackage.getEString(), "ba30bis", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_Ba31(), this.getbehavior_actions(), null, "ba31", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba32(), ecorePackage.getEString(), "ba32", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba33(), ecorePackage.getEString(), "ba33", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba33bis(), ecorePackage.getEString(), "ba33bis", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_Ba34(), this.getvalue_expression(), null, "ba34", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba35(), ecorePackage.getEString(), "ba35", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba35bis(), ecorePackage.getEString(), "ba35bis", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_Ba36(), this.getbehavior_actions(), null, "ba36", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba37(), ecorePackage.getEString(), "ba37", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba38(), ecorePackage.getEString(), "ba38", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_Ba39(), this.getbehavior_actions(), null, "ba39", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba40(), ecorePackage.getEString(), "ba40", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba40bis(), ecorePackage.getEString(), "ba40bis", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_Ba41(), this.getvalue_expression(), null, "ba41", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_Ba42(), ecorePackage.getEString(), "ba42", null, 0, 1, behavior_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(behavior_actionsEClass, behavior_actions.class, "behavior_actions", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getbehavior_actions_Bas1(), this.getbehavior_action(), null, "bas1", null, 0, 1, behavior_actions.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_actions_Baseq2(), ecorePackage.getEString(), "baseq2", null, 0, -1, behavior_actions.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_actions_Baseq4(), ecorePackage.getEString(), "baseq4", null, 0, -1, behavior_actions.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_actions_Baseq3(), this.getbehavior_action(), null, "baseq3", null, 0, -1, behavior_actions.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(behavior_action_sequenceEClass, behavior_action_sequence.class, "behavior_action_sequence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getbehavior_action_sequence_Baseq1(), this.getbehavior_action(), null, "baseq1", null, 0, 1, behavior_action_sequence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_sequence_Baseq2(), ecorePackage.getEString(), "baseq2", null, 0, -1, behavior_action_sequence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_sequence_Baseq3(), this.getbehavior_action(), null, "baseq3", null, 0, -1, behavior_action_sequence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(behavior_action_setEClass, behavior_action_set.class, "behavior_action_set", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getbehavior_action_set_Baset1(), this.getbehavior_action(), null, "baset1", null, 0, 1, behavior_action_set.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbehavior_action_set_Baset2(), ecorePackage.getEString(), "baset2", null, 0, -1, behavior_action_set.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_action_set_Baset3(), this.getbehavior_action(), null, "baset3", null, 0, -1, behavior_action_set.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(element_valuesEClass, element_values.class, "element_values", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getelement_values_Ev1(), this.getinteger_range(), null, "ev1", null, 0, 1, element_values.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getelement_values_Ev2(), ecorePackage.getEString(), "ev2", null, 0, 1, element_values.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getelement_values_Ev3(), this.getarray_data_component_reference(), null, "ev3", null, 0, 1, element_values.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(array_data_component_referenceEClass, array_data_component_reference.class, "array_data_component_reference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getarray_data_component_reference_Adcr1(), ecorePackage.getEString(), "adcr1", null, 0, 1, array_data_component_reference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getarray_data_component_reference_Adcr2(), this.getdata_component_reference(), null, "adcr2", null, 0, 1, array_data_component_reference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getarray_data_component_reference_Adcr3(), ecorePackage.getEString(), "adcr3", null, 0, -1, array_data_component_reference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getarray_data_component_reference_Adcr4(), this.getdata_component_reference(), null, "adcr4", null, 0, -1, array_data_component_reference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getarray_data_component_reference_Adcr5(), ecorePackage.getEString(), "adcr5", null, 0, 1, array_data_component_reference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(basic_actionEClass, basic_action.class, "basic_action", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getbasic_action_Bact1(), this.getassignment_action(), null, "bact1", null, 0, 1, basic_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbasic_action_Bact2(), this.getcommunication_action(), null, "bact2", null, 0, 1, basic_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbasic_action_Bact3(), this.gettimed_action(), null, "bact3", null, 0, 1, basic_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(assignment_actionEClass, assignment_action.class, "assignment_action", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getassignment_action_Aa1(), ecorePackage.getEString(), "aa1", null, 0, 1, assignment_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getassignment_action_Aa2(), ecorePackage.getEString(), "aa2", null, 0, 1, assignment_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getassignment_action_Aa3(), this.getvalue_expression(), null, "aa3", null, 0, 1, assignment_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getassignment_action_Aa4(), ecorePackage.getEString(), "aa4", null, 0, 1, assignment_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(communication_actionEClass, communication_action.class, "communication_action", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getcommunication_action_Ca1(), ecorePackage.getEString(), "ca1", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getcommunication_action_Ca2(), ecorePackage.getEString(), "ca2", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getcommunication_action_Ca3(), ecorePackage.getEString(), "ca3", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getcommunication_action_Ca4(), this.getsubprogram_parameter_list(), null, "ca4", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getcommunication_action_Ca5(), ecorePackage.getEString(), "ca5", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getcommunication_action_Ca21(), ecorePackage.getEString(), "ca21", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getcommunication_action_Ca22(), ecorePackage.getEString(), "ca22", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getcommunication_action_Ca23(), ecorePackage.getEString(), "ca23", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getcommunication_action_Ca24(), ecorePackage.getEString(), "ca24", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getcommunication_action_Ca25(), ecorePackage.getEString(), "ca25", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getcommunication_action_Ca26(), ecorePackage.getEString(), "ca26", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getcommunication_action_Ca27(), ecorePackage.getEString(), "ca27", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getcommunication_action_Ca28(), ecorePackage.getEString(), "ca28", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getcommunication_action_Ca29(), ecorePackage.getEString(), "ca29", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getcommunication_action_Ca30(), ecorePackage.getEString(), "ca30", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getcommunication_action_Ca31(), ecorePackage.getEString(), "ca31", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getcommunication_action_Ca32(), ecorePackage.getEString(), "ca32", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getcommunication_action_Ca33(), ecorePackage.getEString(), "ca33", null, 0, 1, communication_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(timed_actionEClass, timed_action.class, "timed_action", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(gettimed_action_Ta1(), ecorePackage.getEString(), "ta1", null, 0, 1, timed_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(gettimed_action_Ta1bis(), ecorePackage.getEString(), "ta1bis", null, 0, 1, timed_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(gettimed_action_Ta2(), this.getbehavior_time(), null, "ta2", null, 0, 1, timed_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(gettimed_action_Ta3(), ecorePackage.getEString(), "ta3", null, 0, 1, timed_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(gettimed_action_Ta4(), this.getbehavior_time(), null, "ta4", null, 0, 1, timed_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(gettimed_action_Ta5(), ecorePackage.getEString(), "ta5", null, 0, 1, timed_action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(subprogram_parameter_listEClass, subprogram_parameter_list.class, "subprogram_parameter_list", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getsubprogram_parameter_list_Spl1(), this.getparameter_label(), null, "spl1", null, 0, 1, subprogram_parameter_list.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getsubprogram_parameter_list_Spl2(), ecorePackage.getEString(), "spl2", null, 0, -1, subprogram_parameter_list.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getsubprogram_parameter_list_Spl3(), this.getparameter_label(), null, "spl3", null, 0, -1, subprogram_parameter_list.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(parameter_labelEClass, parameter_label.class, "parameter_label", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getparameter_label_Pl1(), this.getvalue_expression(), null, "pl1", null, 0, 1, parameter_label.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(data_component_referenceEClass, data_component_reference.class, "data_component_reference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getdata_component_reference_Dcr1(), ecorePackage.getEString(), "dcr1", null, 0, 1, data_component_reference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getdata_component_reference_Dcr2(), ecorePackage.getEString(), "dcr2", null, 0, -1, data_component_reference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getdata_component_reference_Dcr3(), ecorePackage.getEString(), "dcr3", null, 0, -1, data_component_reference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(nameEClass, name.class, "name", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getname_N1(), ecorePackage.getEString(), "n1", null, 0, 1, name.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getname_N2(), this.getarray_index(), null, "n2", null, 0, -1, name.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(array_indexEClass, array_index.class, "array_index", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getarray_index_Ai1(), ecorePackage.getEString(), "ai1", null, 0, 1, array_index.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getarray_index_Ai2(), this.getinteger_literal(), null, "ai2", null, 0, 1, array_index.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getarray_index_Ai3(), ecorePackage.getEString(), "ai3", null, 0, 1, array_index.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integer_valueEClass, integer_value.class, "integer_value", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getinteger_value_Ivname(), this.getnumeric_literal(), null, "ivname", null, 0, 1, integer_value.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(numeric_literalEClass, numeric_literal.class, "numeric_literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getnumeric_literal_Nlname1(), this.getinteger_literal(), null, "nlname1", null, 0, 1, numeric_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getnumeric_literal_Nlname2(), this.getreal_literal(), null, "nlname2", null, 0, 1, numeric_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integer_literalEClass, integer_literal.class, "integer_literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getinteger_literal_Ilname1(), this.getdecimal_integer_literal(), null, "ilname1", null, 0, 1, integer_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getinteger_literal_Ilname2(), this.getbased_integer_literal(), null, "ilname2", null, 0, 1, integer_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(real_literalEClass, real_literal.class, "real_literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getreal_literal_Rlname(), this.getdecimal_real_literal(), null, "rlname", null, 0, 1, real_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(decimal_integer_literalEClass, decimal_integer_literal.class, "decimal_integer_literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getdecimal_integer_literal_Dilname1(), this.getnumeral(), null, "dilname1", null, 0, 1, decimal_integer_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getdecimal_integer_literal_Dilname2(), this.getpositive_exponent(), null, "dilname2", null, 0, 1, decimal_integer_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(decimal_real_literalEClass, decimal_real_literal.class, "decimal_real_literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getdecimal_real_literal_Drlname1(), this.getnumeral(), null, "drlname1", null, 0, 1, decimal_real_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getdecimal_real_literal_Drlname2(), this.getnumeral(), null, "drlname2", null, 0, 1, decimal_real_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getdecimal_real_literal_Drlname3(), this.getexponent(), null, "drlname3", null, 0, 1, decimal_real_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(numeralEClass, numeral.class, "numeral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getnumeral_Nname1(), this.getinteger(), null, "nname1", null, 0, 1, numeral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getnumeral_Nname2(), this.getinteger(), null, "nname2", null, 0, -1, numeral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(exponentEClass, exponent.class, "exponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getexponent_Ename1(), this.getnumeral(), null, "ename1", null, 0, 1, exponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getexponent_Ename2(), this.getnumeral(), null, "ename2", null, 0, 1, exponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(positive_exponentEClass, positive_exponent.class, "positive_exponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getpositive_exponent_Pename(), this.getnumeral(), null, "pename", null, 0, 1, positive_exponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(based_integer_literalEClass, based_integer_literal.class, "based_integer_literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getbased_integer_literal_Bilname1(), this.getbase(), null, "bilname1", null, 0, 1, based_integer_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbased_integer_literal_Bilname2(), this.getbased_numeral(), null, "bilname2", null, 0, 1, based_integer_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbased_integer_literal_Bilname3(), this.getpositive_exponent(), null, "bilname3", null, 0, 1, based_integer_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(baseEClass, base.class, "base", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getbase_Bname1(), ecorePackage.getEString(), "bname1", null, 0, 1, base.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbase_Bname2(), ecorePackage.getEString(), "bname2", null, 0, 1, base.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(based_numeralEClass, based_numeral.class, "based_numeral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getbased_numeral_Bnname1(), ecorePackage.getEString(), "bnname1", null, 0, 1, based_numeral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbased_numeral_Bnname2(), ecorePackage.getEString(), "bnname2", null, 0, 1, based_numeral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(behavior_timeEClass, behavior_time.class, "behavior_time", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getbehavior_time_Btname1(), this.getinteger_value(), null, "btname1", null, 0, 1, behavior_time.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getbehavior_time_Btname2(), this.getunit_identifier(), null, "btname2", null, 0, 1, behavior_time.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(unit_identifierEClass, unit_identifier.class, "unit_identifier", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getunit_identifier_Uiname1(), ecorePackage.getEString(), "uiname1", null, 0, 1, unit_identifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getunit_identifier_Uiname2(), ecorePackage.getEString(), "uiname2", null, 0, 1, unit_identifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getunit_identifier_Uiname3(), ecorePackage.getEString(), "uiname3", null, 0, 1, unit_identifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getunit_identifier_Uiname4(), ecorePackage.getEString(), "uiname4", null, 0, 1, unit_identifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getunit_identifier_Uiname5(), ecorePackage.getEString(), "uiname5", null, 0, 1, unit_identifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getunit_identifier_Uiname6(), ecorePackage.getEString(), "uiname6", null, 0, 1, unit_identifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getunit_identifier_Uiname7(), ecorePackage.getEString(), "uiname7", null, 0, 1, unit_identifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integer_rangeEClass, integer_range.class, "integer_range", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getinteger_range_Min(), this.getinteger_value(), null, "min", null, 0, 1, integer_range.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getinteger_range_Irsep(), ecorePackage.getEString(), "irsep", null, 0, 1, integer_range.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getinteger_range_Max(), this.getinteger_value(), null, "max", null, 0, 1, integer_range.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integerEClass, integer.class, "integer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getinteger_Value1(), ecorePackage.getEInt(), "value1", null, 0, 1, integer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getinteger_Value2(), ecorePackage.getEString(), "value2", null, 0, 1, integer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(value_expressionEClass, value_expression.class, "value_expression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getvalue_expression_Vename1(), this.getrelation(), null, "vename1", null, 0, 1, value_expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getvalue_expression_Vename3(), this.getlogical_operator(), null, "vename3", null, 0, -1, value_expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getvalue_expression_Vename2(), this.getrelation(), null, "vename2", null, 0, -1, value_expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(relationEClass, relation.class, "relation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getrelation_Rname1(), this.getsimple_expression(), null, "rname1", null, 0, 1, relation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getrelation_Rname3(), this.getrelational_operator(), null, "rname3", null, 0, -1, relation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getrelation_Rname2(), this.getsimple_expression(), null, "rname2", null, 0, -1, relation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(simple_expressionEClass, simple_expression.class, "simple_expression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getsimple_expression_Sename2(), this.getunary_adding_operator(), null, "sename2", null, 0, 1, simple_expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getsimple_expression_Sename1(), this.getterm(), null, "sename1", null, 0, 1, simple_expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getsimple_expression_Sename3(), this.getbinary_adding_operator(), null, "sename3", null, 0, -1, simple_expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getsimple_expression_Sename4(), this.getterm(), null, "sename4", null, 0, -1, simple_expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(termEClass, term.class, "term", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getterm_Tname1(), this.getfactor(), null, "tname1", null, 0, 1, term.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getterm_Tname3(), this.getmultiplying_operator(), null, "tname3", null, 0, -1, term.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getterm_Tname2(), this.getfactor(), null, "tname2", null, 0, -1, term.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(factorEClass, factor.class, "factor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getfactor_Fname1(), this.getvalue(), null, "fname1", null, 0, 1, factor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getfactor_Fname3(), this.getbinary_numeric_operator(), null, "fname3", null, 0, -1, factor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getfactor_Fname2(), this.getvalue(), null, "fname2", null, 0, 1, factor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getfactor_Fname4(), this.getunary_numeric_operator(), null, "fname4", null, 0, 1, factor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getfactor_Fname5(), this.getvalue(), null, "fname5", null, 0, 1, factor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getfactor_Fname6(), this.getunary_boolean_operator(), null, "fname6", null, 0, 1, factor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getfactor_Fname7(), this.getvalue(), null, "fname7", null, 0, 1, factor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(valueEClass, value.class, "value", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getvalue_Vname1(), this.getvalue_variable(), null, "vname1", null, 0, 1, value.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getvalue_Vname2(), this.getvalue_constant(), null, "vname2", null, 0, 1, value.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(value_variableEClass, value_variable.class, "value_variable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getvalue_variable_Vvname0(), ecorePackage.getEString(), "vvname0", null, 0, 1, value_variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getvalue_variable_Vvname1(), ecorePackage.getEString(), "vvname1", null, 0, 1, value_variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getvalue_variable_Vvname2(), ecorePackage.getEString(), "vvname2", null, 0, 1, value_variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getvalue_variable_Vvname3(), ecorePackage.getEString(), "vvname3", null, 0, 1, value_variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(value_constantEClass, value_constant.class, "value_constant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getvalue_constant_Vcname1(), this.getboolean_literal(), null, "vcname1", null, 0, 1, value_constant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getvalue_constant_Vcname2(), this.getnumeric_literal(), null, "vcname2", null, 0, 1, value_constant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getvalue_constant_Vcname3(), ecorePackage.getEString(), "vcname3", null, 0, 1, value_constant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getvalue_constant_Vcname4(), ecorePackage.getEString(), "vcname4", null, 0, 1, value_constant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(logical_operatorEClass, logical_operator.class, "logical_operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getlogical_operator_Loname1(), ecorePackage.getEString(), "loname1", null, 0, 1, logical_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getlogical_operator_Loname2(), ecorePackage.getEString(), "loname2", null, 0, 1, logical_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getlogical_operator_Loname3(), ecorePackage.getEString(), "loname3", null, 0, 1, logical_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(relational_operatorEClass, relational_operator.class, "relational_operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getrelational_operator_Roname1(), ecorePackage.getEString(), "roname1", null, 0, 1, relational_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getrelational_operator_Roname2(), ecorePackage.getEString(), "roname2", null, 0, 1, relational_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getrelational_operator_Roname3(), ecorePackage.getEString(), "roname3", null, 0, 1, relational_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getrelational_operator_Roname4(), ecorePackage.getEString(), "roname4", null, 0, 1, relational_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getrelational_operator_Roname5(), ecorePackage.getEString(), "roname5", null, 0, 1, relational_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getrelational_operator_Roname6(), ecorePackage.getEString(), "roname6", null, 0, 1, relational_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(binary_adding_operatorEClass, binary_adding_operator.class, "binary_adding_operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getbinary_adding_operator_Baoname1(), ecorePackage.getEString(), "baoname1", null, 0, 1, binary_adding_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getbinary_adding_operator_Baoname2(), ecorePackage.getEString(), "baoname2", null, 0, 1, binary_adding_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(unary_adding_operatorEClass, unary_adding_operator.class, "unary_adding_operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getunary_adding_operator_Uaoname1(), ecorePackage.getEString(), "uaoname1", null, 0, 1, unary_adding_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getunary_adding_operator_Uaoname2(), ecorePackage.getEString(), "uaoname2", null, 0, 1, unary_adding_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(multiplying_operatorEClass, multiplying_operator.class, "multiplying_operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getmultiplying_operator_Moname1(), ecorePackage.getEString(), "moname1", null, 0, 1, multiplying_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getmultiplying_operator_Moname2(), ecorePackage.getEString(), "moname2", null, 0, 1, multiplying_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getmultiplying_operator_Moname3(), ecorePackage.getEString(), "moname3", null, 0, 1, multiplying_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getmultiplying_operator_Moname4(), ecorePackage.getEString(), "moname4", null, 0, 1, multiplying_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(binary_numeric_operatorEClass, binary_numeric_operator.class, "binary_numeric_operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getbinary_numeric_operator_Bnoname(), ecorePackage.getEString(), "bnoname", null, 0, 1, binary_numeric_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(unary_numeric_operatorEClass, unary_numeric_operator.class, "unary_numeric_operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getunary_numeric_operator_Unoname(), ecorePackage.getEString(), "unoname", null, 0, 1, unary_numeric_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(unary_boolean_operatorEClass, unary_boolean_operator.class, "unary_boolean_operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getunary_boolean_operator_Uboname(), ecorePackage.getEString(), "uboname", null, 0, 1, unary_boolean_operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(boolean_literalEClass, boolean_literal.class, "boolean_literal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getboolean_literal_Blname1(), ecorePackage.getEString(), "blname1", null, 0, 1, boolean_literal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);
  }

} //BaActionPackageImpl
