/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>integer range</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.integer_range#getMin <em>Min</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.integer_range#getIrsep <em>Irsep</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.integer_range#getMax <em>Max</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getinteger_range()
 * @model
 * @generated
 */
public interface integer_range extends EObject
{
  /**
   * Returns the value of the '<em><b>Min</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Min</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Min</em>' containment reference.
   * @see #setMin(integer_value)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getinteger_range_Min()
   * @model containment="true"
   * @generated
   */
  integer_value getMin();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.integer_range#getMin <em>Min</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Min</em>' containment reference.
   * @see #getMin()
   * @generated
   */
  void setMin(integer_value value);

  /**
   * Returns the value of the '<em><b>Irsep</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Irsep</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Irsep</em>' attribute.
   * @see #setIrsep(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getinteger_range_Irsep()
   * @model
   * @generated
   */
  String getIrsep();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.integer_range#getIrsep <em>Irsep</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Irsep</em>' attribute.
   * @see #getIrsep()
   * @generated
   */
  void setIrsep(String value);

  /**
   * Returns the value of the '<em><b>Max</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Max</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Max</em>' containment reference.
   * @see #setMax(integer_value)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getinteger_range_Max()
   * @model containment="true"
   * @generated
   */
  integer_value getMax();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.integer_range#getMax <em>Max</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Max</em>' containment reference.
   * @see #getMax()
   * @generated
   */
  void setMax(integer_value value);

} // integer_range
