/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.unit_identifier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>unit identifier</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.unit_identifierImpl#getUiname1 <em>Uiname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.unit_identifierImpl#getUiname2 <em>Uiname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.unit_identifierImpl#getUiname3 <em>Uiname3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.unit_identifierImpl#getUiname4 <em>Uiname4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.unit_identifierImpl#getUiname5 <em>Uiname5</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.unit_identifierImpl#getUiname6 <em>Uiname6</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.unit_identifierImpl#getUiname7 <em>Uiname7</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class unit_identifierImpl extends MinimalEObjectImpl.Container implements unit_identifier
{
  /**
   * The default value of the '{@link #getUiname1() <em>Uiname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUiname1()
   * @generated
   * @ordered
   */
  protected static final String UINAME1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUiname1() <em>Uiname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUiname1()
   * @generated
   * @ordered
   */
  protected String uiname1 = UINAME1_EDEFAULT;

  /**
   * The default value of the '{@link #getUiname2() <em>Uiname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUiname2()
   * @generated
   * @ordered
   */
  protected static final String UINAME2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUiname2() <em>Uiname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUiname2()
   * @generated
   * @ordered
   */
  protected String uiname2 = UINAME2_EDEFAULT;

  /**
   * The default value of the '{@link #getUiname3() <em>Uiname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUiname3()
   * @generated
   * @ordered
   */
  protected static final String UINAME3_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUiname3() <em>Uiname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUiname3()
   * @generated
   * @ordered
   */
  protected String uiname3 = UINAME3_EDEFAULT;

  /**
   * The default value of the '{@link #getUiname4() <em>Uiname4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUiname4()
   * @generated
   * @ordered
   */
  protected static final String UINAME4_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUiname4() <em>Uiname4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUiname4()
   * @generated
   * @ordered
   */
  protected String uiname4 = UINAME4_EDEFAULT;

  /**
   * The default value of the '{@link #getUiname5() <em>Uiname5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUiname5()
   * @generated
   * @ordered
   */
  protected static final String UINAME5_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUiname5() <em>Uiname5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUiname5()
   * @generated
   * @ordered
   */
  protected String uiname5 = UINAME5_EDEFAULT;

  /**
   * The default value of the '{@link #getUiname6() <em>Uiname6</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUiname6()
   * @generated
   * @ordered
   */
  protected static final String UINAME6_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUiname6() <em>Uiname6</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUiname6()
   * @generated
   * @ordered
   */
  protected String uiname6 = UINAME6_EDEFAULT;

  /**
   * The default value of the '{@link #getUiname7() <em>Uiname7</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUiname7()
   * @generated
   * @ordered
   */
  protected static final String UINAME7_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUiname7() <em>Uiname7</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUiname7()
   * @generated
   * @ordered
   */
  protected String uiname7 = UINAME7_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected unit_identifierImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.UNIT_IDENTIFIER;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getUiname1()
  {
    return uiname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUiname1(String newUiname1)
  {
    String oldUiname1 = uiname1;
    uiname1 = newUiname1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.UNIT_IDENTIFIER__UINAME1, oldUiname1, uiname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getUiname2()
  {
    return uiname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUiname2(String newUiname2)
  {
    String oldUiname2 = uiname2;
    uiname2 = newUiname2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.UNIT_IDENTIFIER__UINAME2, oldUiname2, uiname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getUiname3()
  {
    return uiname3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUiname3(String newUiname3)
  {
    String oldUiname3 = uiname3;
    uiname3 = newUiname3;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.UNIT_IDENTIFIER__UINAME3, oldUiname3, uiname3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getUiname4()
  {
    return uiname4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUiname4(String newUiname4)
  {
    String oldUiname4 = uiname4;
    uiname4 = newUiname4;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.UNIT_IDENTIFIER__UINAME4, oldUiname4, uiname4));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getUiname5()
  {
    return uiname5;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUiname5(String newUiname5)
  {
    String oldUiname5 = uiname5;
    uiname5 = newUiname5;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.UNIT_IDENTIFIER__UINAME5, oldUiname5, uiname5));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getUiname6()
  {
    return uiname6;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUiname6(String newUiname6)
  {
    String oldUiname6 = uiname6;
    uiname6 = newUiname6;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.UNIT_IDENTIFIER__UINAME6, oldUiname6, uiname6));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getUiname7()
  {
    return uiname7;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUiname7(String newUiname7)
  {
    String oldUiname7 = uiname7;
    uiname7 = newUiname7;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.UNIT_IDENTIFIER__UINAME7, oldUiname7, uiname7));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.UNIT_IDENTIFIER__UINAME1:
        return getUiname1();
      case BaActionPackage.UNIT_IDENTIFIER__UINAME2:
        return getUiname2();
      case BaActionPackage.UNIT_IDENTIFIER__UINAME3:
        return getUiname3();
      case BaActionPackage.UNIT_IDENTIFIER__UINAME4:
        return getUiname4();
      case BaActionPackage.UNIT_IDENTIFIER__UINAME5:
        return getUiname5();
      case BaActionPackage.UNIT_IDENTIFIER__UINAME6:
        return getUiname6();
      case BaActionPackage.UNIT_IDENTIFIER__UINAME7:
        return getUiname7();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.UNIT_IDENTIFIER__UINAME1:
        setUiname1((String)newValue);
        return;
      case BaActionPackage.UNIT_IDENTIFIER__UINAME2:
        setUiname2((String)newValue);
        return;
      case BaActionPackage.UNIT_IDENTIFIER__UINAME3:
        setUiname3((String)newValue);
        return;
      case BaActionPackage.UNIT_IDENTIFIER__UINAME4:
        setUiname4((String)newValue);
        return;
      case BaActionPackage.UNIT_IDENTIFIER__UINAME5:
        setUiname5((String)newValue);
        return;
      case BaActionPackage.UNIT_IDENTIFIER__UINAME6:
        setUiname6((String)newValue);
        return;
      case BaActionPackage.UNIT_IDENTIFIER__UINAME7:
        setUiname7((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.UNIT_IDENTIFIER__UINAME1:
        setUiname1(UINAME1_EDEFAULT);
        return;
      case BaActionPackage.UNIT_IDENTIFIER__UINAME2:
        setUiname2(UINAME2_EDEFAULT);
        return;
      case BaActionPackage.UNIT_IDENTIFIER__UINAME3:
        setUiname3(UINAME3_EDEFAULT);
        return;
      case BaActionPackage.UNIT_IDENTIFIER__UINAME4:
        setUiname4(UINAME4_EDEFAULT);
        return;
      case BaActionPackage.UNIT_IDENTIFIER__UINAME5:
        setUiname5(UINAME5_EDEFAULT);
        return;
      case BaActionPackage.UNIT_IDENTIFIER__UINAME6:
        setUiname6(UINAME6_EDEFAULT);
        return;
      case BaActionPackage.UNIT_IDENTIFIER__UINAME7:
        setUiname7(UINAME7_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.UNIT_IDENTIFIER__UINAME1:
        return UINAME1_EDEFAULT == null ? uiname1 != null : !UINAME1_EDEFAULT.equals(uiname1);
      case BaActionPackage.UNIT_IDENTIFIER__UINAME2:
        return UINAME2_EDEFAULT == null ? uiname2 != null : !UINAME2_EDEFAULT.equals(uiname2);
      case BaActionPackage.UNIT_IDENTIFIER__UINAME3:
        return UINAME3_EDEFAULT == null ? uiname3 != null : !UINAME3_EDEFAULT.equals(uiname3);
      case BaActionPackage.UNIT_IDENTIFIER__UINAME4:
        return UINAME4_EDEFAULT == null ? uiname4 != null : !UINAME4_EDEFAULT.equals(uiname4);
      case BaActionPackage.UNIT_IDENTIFIER__UINAME5:
        return UINAME5_EDEFAULT == null ? uiname5 != null : !UINAME5_EDEFAULT.equals(uiname5);
      case BaActionPackage.UNIT_IDENTIFIER__UINAME6:
        return UINAME6_EDEFAULT == null ? uiname6 != null : !UINAME6_EDEFAULT.equals(uiname6);
      case BaActionPackage.UNIT_IDENTIFIER__UINAME7:
        return UINAME7_EDEFAULT == null ? uiname7 != null : !UINAME7_EDEFAULT.equals(uiname7);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (uiname1: ");
    result.append(uiname1);
    result.append(", uiname2: ");
    result.append(uiname2);
    result.append(", uiname3: ");
    result.append(uiname3);
    result.append(", uiname4: ");
    result.append(uiname4);
    result.append(", uiname5: ");
    result.append(uiname5);
    result.append(", uiname6: ");
    result.append(uiname6);
    result.append(", uiname7: ");
    result.append(uiname7);
    result.append(')');
    return result.toString();
  }

} //unit_identifierImpl
