package org.topcased.adele.xtext.baaction.serializer;

import com.google.inject.Inject;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AbstractElementAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.TokenAlias;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynNavigable;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynTransition;
import org.eclipse.xtext.serializer.sequencer.AbstractSyntacticSequencer;
import org.topcased.adele.xtext.baaction.services.BaActionGrammarAccess;

@SuppressWarnings("restriction")
public class AbstractBaActionSyntacticSequencer extends AbstractSyntacticSequencer {

	protected BaActionGrammarAccess grammarAccess;
	protected AbstractElementAlias match_based_numeral__Keyword_1_0_q;
	protected AbstractElementAlias match_exponent_PlusSignKeyword_0_1_q;
	protected AbstractElementAlias match_numeral__Keyword_1_0_q;
	protected AbstractElementAlias match_positive_exponent_PlusSignKeyword_1_q;
	
	@Inject
	protected void init(IGrammarAccess access) {
		grammarAccess = (BaActionGrammarAccess) access;
		match_based_numeral__Keyword_1_0_q = new TokenAlias(true, false, grammarAccess.getBased_numeralAccess().get_Keyword_1_0());
		match_exponent_PlusSignKeyword_0_1_q = new TokenAlias(true, false, grammarAccess.getExponentAccess().getPlusSignKeyword_0_1());
		match_numeral__Keyword_1_0_q = new TokenAlias(true, false, grammarAccess.getNumeralAccess().get_Keyword_1_0());
		match_positive_exponent_PlusSignKeyword_1_q = new TokenAlias(true, false, grammarAccess.getPositive_exponentAccess().getPlusSignKeyword_1());
	}
	
	@Override
	protected String getUnassignedRuleCallToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if(ruleCall.getRule() == grammarAccess.getBaWSRule())
			return getbaWSToken(semanticObject, ruleCall, node);
		return "";
	}
	
	protected String getbaWSToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "";
	}
	
	@Override
	protected void emitUnassignedTokens(EObject semanticObject, ISynTransition transition, INode fromNode, INode toNode) {
		if (transition.getAmbiguousSyntaxes().isEmpty()) return;
		List<INode> transitionNodes = collectNodes(fromNode, toNode);
		for (AbstractElementAlias syntax : transition.getAmbiguousSyntaxes()) {
			List<INode> syntaxNodes = getNodesFor(transitionNodes, syntax);
			if(match_based_numeral__Keyword_1_0_q.equals(syntax))
				emit_based_numeral__Keyword_1_0_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_exponent_PlusSignKeyword_0_1_q.equals(syntax))
				emit_exponent_PlusSignKeyword_0_1_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_numeral__Keyword_1_0_q.equals(syntax))
				emit_numeral__Keyword_1_0_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_positive_exponent_PlusSignKeyword_1_q.equals(syntax))
				emit_positive_exponent_PlusSignKeyword_1_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else acceptNodes(getLastNavigableState(), syntaxNodes);
		}
	}

	/**
	 * Syntax:
	 *     '_'?
	 */
	protected void emit_based_numeral__Keyword_1_0_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     '+'?
	 */
	protected void emit_exponent_PlusSignKeyword_0_1_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     '_'?
	 */
	protected void emit_numeral__Keyword_1_0_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     '+'?
	 */
	protected void emit_positive_exponent_PlusSignKeyword_1_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
}
