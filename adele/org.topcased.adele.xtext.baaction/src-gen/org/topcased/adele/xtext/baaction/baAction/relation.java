/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.relation#getRname1 <em>Rname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.relation#getRname3 <em>Rname3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.relation#getRname2 <em>Rname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getrelation()
 * @model
 * @generated
 */
public interface relation extends EObject
{
  /**
   * Returns the value of the '<em><b>Rname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rname1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rname1</em>' containment reference.
   * @see #setRname1(simple_expression)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getrelation_Rname1()
   * @model containment="true"
   * @generated
   */
  simple_expression getRname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.relation#getRname1 <em>Rname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Rname1</em>' containment reference.
   * @see #getRname1()
   * @generated
   */
  void setRname1(simple_expression value);

  /**
   * Returns the value of the '<em><b>Rname3</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baaction.baAction.relational_operator}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rname3</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rname3</em>' containment reference list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getrelation_Rname3()
   * @model containment="true"
   * @generated
   */
  EList<relational_operator> getRname3();

  /**
   * Returns the value of the '<em><b>Rname2</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baaction.baAction.simple_expression}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rname2</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rname2</em>' containment reference list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getrelation_Rname2()
   * @model containment="true"
   * @generated
   */
  EList<simple_expression> getRname2();

} // relation
