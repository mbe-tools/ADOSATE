/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.relational_operator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>relational operator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.relational_operatorImpl#getRoname1 <em>Roname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.relational_operatorImpl#getRoname2 <em>Roname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.relational_operatorImpl#getRoname3 <em>Roname3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.relational_operatorImpl#getRoname4 <em>Roname4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.relational_operatorImpl#getRoname5 <em>Roname5</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.relational_operatorImpl#getRoname6 <em>Roname6</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class relational_operatorImpl extends MinimalEObjectImpl.Container implements relational_operator
{
  /**
   * The default value of the '{@link #getRoname1() <em>Roname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRoname1()
   * @generated
   * @ordered
   */
  protected static final String RONAME1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getRoname1() <em>Roname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRoname1()
   * @generated
   * @ordered
   */
  protected String roname1 = RONAME1_EDEFAULT;

  /**
   * The default value of the '{@link #getRoname2() <em>Roname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRoname2()
   * @generated
   * @ordered
   */
  protected static final String RONAME2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getRoname2() <em>Roname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRoname2()
   * @generated
   * @ordered
   */
  protected String roname2 = RONAME2_EDEFAULT;

  /**
   * The default value of the '{@link #getRoname3() <em>Roname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRoname3()
   * @generated
   * @ordered
   */
  protected static final String RONAME3_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getRoname3() <em>Roname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRoname3()
   * @generated
   * @ordered
   */
  protected String roname3 = RONAME3_EDEFAULT;

  /**
   * The default value of the '{@link #getRoname4() <em>Roname4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRoname4()
   * @generated
   * @ordered
   */
  protected static final String RONAME4_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getRoname4() <em>Roname4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRoname4()
   * @generated
   * @ordered
   */
  protected String roname4 = RONAME4_EDEFAULT;

  /**
   * The default value of the '{@link #getRoname5() <em>Roname5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRoname5()
   * @generated
   * @ordered
   */
  protected static final String RONAME5_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getRoname5() <em>Roname5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRoname5()
   * @generated
   * @ordered
   */
  protected String roname5 = RONAME5_EDEFAULT;

  /**
   * The default value of the '{@link #getRoname6() <em>Roname6</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRoname6()
   * @generated
   * @ordered
   */
  protected static final String RONAME6_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getRoname6() <em>Roname6</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRoname6()
   * @generated
   * @ordered
   */
  protected String roname6 = RONAME6_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected relational_operatorImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.RELATIONAL_OPERATOR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getRoname1()
  {
    return roname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRoname1(String newRoname1)
  {
    String oldRoname1 = roname1;
    roname1 = newRoname1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.RELATIONAL_OPERATOR__RONAME1, oldRoname1, roname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getRoname2()
  {
    return roname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRoname2(String newRoname2)
  {
    String oldRoname2 = roname2;
    roname2 = newRoname2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.RELATIONAL_OPERATOR__RONAME2, oldRoname2, roname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getRoname3()
  {
    return roname3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRoname3(String newRoname3)
  {
    String oldRoname3 = roname3;
    roname3 = newRoname3;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.RELATIONAL_OPERATOR__RONAME3, oldRoname3, roname3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getRoname4()
  {
    return roname4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRoname4(String newRoname4)
  {
    String oldRoname4 = roname4;
    roname4 = newRoname4;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.RELATIONAL_OPERATOR__RONAME4, oldRoname4, roname4));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getRoname5()
  {
    return roname5;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRoname5(String newRoname5)
  {
    String oldRoname5 = roname5;
    roname5 = newRoname5;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.RELATIONAL_OPERATOR__RONAME5, oldRoname5, roname5));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getRoname6()
  {
    return roname6;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRoname6(String newRoname6)
  {
    String oldRoname6 = roname6;
    roname6 = newRoname6;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.RELATIONAL_OPERATOR__RONAME6, oldRoname6, roname6));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME1:
        return getRoname1();
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME2:
        return getRoname2();
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME3:
        return getRoname3();
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME4:
        return getRoname4();
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME5:
        return getRoname5();
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME6:
        return getRoname6();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME1:
        setRoname1((String)newValue);
        return;
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME2:
        setRoname2((String)newValue);
        return;
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME3:
        setRoname3((String)newValue);
        return;
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME4:
        setRoname4((String)newValue);
        return;
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME5:
        setRoname5((String)newValue);
        return;
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME6:
        setRoname6((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME1:
        setRoname1(RONAME1_EDEFAULT);
        return;
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME2:
        setRoname2(RONAME2_EDEFAULT);
        return;
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME3:
        setRoname3(RONAME3_EDEFAULT);
        return;
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME4:
        setRoname4(RONAME4_EDEFAULT);
        return;
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME5:
        setRoname5(RONAME5_EDEFAULT);
        return;
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME6:
        setRoname6(RONAME6_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME1:
        return RONAME1_EDEFAULT == null ? roname1 != null : !RONAME1_EDEFAULT.equals(roname1);
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME2:
        return RONAME2_EDEFAULT == null ? roname2 != null : !RONAME2_EDEFAULT.equals(roname2);
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME3:
        return RONAME3_EDEFAULT == null ? roname3 != null : !RONAME3_EDEFAULT.equals(roname3);
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME4:
        return RONAME4_EDEFAULT == null ? roname4 != null : !RONAME4_EDEFAULT.equals(roname4);
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME5:
        return RONAME5_EDEFAULT == null ? roname5 != null : !RONAME5_EDEFAULT.equals(roname5);
      case BaActionPackage.RELATIONAL_OPERATOR__RONAME6:
        return RONAME6_EDEFAULT == null ? roname6 != null : !RONAME6_EDEFAULT.equals(roname6);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (roname1: ");
    result.append(roname1);
    result.append(", roname2: ");
    result.append(roname2);
    result.append(", roname3: ");
    result.append(roname3);
    result.append(", roname4: ");
    result.append(roname4);
    result.append(", roname5: ");
    result.append(roname5);
    result.append(", roname6: ");
    result.append(roname6);
    result.append(')');
    return result.toString();
  }

} //relational_operatorImpl
