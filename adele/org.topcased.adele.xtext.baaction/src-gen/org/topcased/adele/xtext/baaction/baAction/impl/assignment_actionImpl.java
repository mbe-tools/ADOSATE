/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.assignment_action;
import org.topcased.adele.xtext.baaction.baAction.value_expression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>assignment action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.assignment_actionImpl#getAa1 <em>Aa1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.assignment_actionImpl#getAa2 <em>Aa2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.assignment_actionImpl#getAa3 <em>Aa3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.assignment_actionImpl#getAa4 <em>Aa4</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class assignment_actionImpl extends MinimalEObjectImpl.Container implements assignment_action
{
  /**
   * The default value of the '{@link #getAa1() <em>Aa1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAa1()
   * @generated
   * @ordered
   */
  protected static final String AA1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAa1() <em>Aa1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAa1()
   * @generated
   * @ordered
   */
  protected String aa1 = AA1_EDEFAULT;

  /**
   * The default value of the '{@link #getAa2() <em>Aa2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAa2()
   * @generated
   * @ordered
   */
  protected static final String AA2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAa2() <em>Aa2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAa2()
   * @generated
   * @ordered
   */
  protected String aa2 = AA2_EDEFAULT;

  /**
   * The cached value of the '{@link #getAa3() <em>Aa3</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAa3()
   * @generated
   * @ordered
   */
  protected value_expression aa3;

  /**
   * The default value of the '{@link #getAa4() <em>Aa4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAa4()
   * @generated
   * @ordered
   */
  protected static final String AA4_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAa4() <em>Aa4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAa4()
   * @generated
   * @ordered
   */
  protected String aa4 = AA4_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected assignment_actionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.ASSIGNMENT_ACTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAa1()
  {
    return aa1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAa1(String newAa1)
  {
    String oldAa1 = aa1;
    aa1 = newAa1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.ASSIGNMENT_ACTION__AA1, oldAa1, aa1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAa2()
  {
    return aa2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAa2(String newAa2)
  {
    String oldAa2 = aa2;
    aa2 = newAa2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.ASSIGNMENT_ACTION__AA2, oldAa2, aa2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value_expression getAa3()
  {
    return aa3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAa3(value_expression newAa3, NotificationChain msgs)
  {
    value_expression oldAa3 = aa3;
    aa3 = newAa3;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.ASSIGNMENT_ACTION__AA3, oldAa3, newAa3);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAa3(value_expression newAa3)
  {
    if (newAa3 != aa3)
    {
      NotificationChain msgs = null;
      if (aa3 != null)
        msgs = ((InternalEObject)aa3).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.ASSIGNMENT_ACTION__AA3, null, msgs);
      if (newAa3 != null)
        msgs = ((InternalEObject)newAa3).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.ASSIGNMENT_ACTION__AA3, null, msgs);
      msgs = basicSetAa3(newAa3, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.ASSIGNMENT_ACTION__AA3, newAa3, newAa3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAa4()
  {
    return aa4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAa4(String newAa4)
  {
    String oldAa4 = aa4;
    aa4 = newAa4;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.ASSIGNMENT_ACTION__AA4, oldAa4, aa4));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.ASSIGNMENT_ACTION__AA3:
        return basicSetAa3(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.ASSIGNMENT_ACTION__AA1:
        return getAa1();
      case BaActionPackage.ASSIGNMENT_ACTION__AA2:
        return getAa2();
      case BaActionPackage.ASSIGNMENT_ACTION__AA3:
        return getAa3();
      case BaActionPackage.ASSIGNMENT_ACTION__AA4:
        return getAa4();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.ASSIGNMENT_ACTION__AA1:
        setAa1((String)newValue);
        return;
      case BaActionPackage.ASSIGNMENT_ACTION__AA2:
        setAa2((String)newValue);
        return;
      case BaActionPackage.ASSIGNMENT_ACTION__AA3:
        setAa3((value_expression)newValue);
        return;
      case BaActionPackage.ASSIGNMENT_ACTION__AA4:
        setAa4((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.ASSIGNMENT_ACTION__AA1:
        setAa1(AA1_EDEFAULT);
        return;
      case BaActionPackage.ASSIGNMENT_ACTION__AA2:
        setAa2(AA2_EDEFAULT);
        return;
      case BaActionPackage.ASSIGNMENT_ACTION__AA3:
        setAa3((value_expression)null);
        return;
      case BaActionPackage.ASSIGNMENT_ACTION__AA4:
        setAa4(AA4_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.ASSIGNMENT_ACTION__AA1:
        return AA1_EDEFAULT == null ? aa1 != null : !AA1_EDEFAULT.equals(aa1);
      case BaActionPackage.ASSIGNMENT_ACTION__AA2:
        return AA2_EDEFAULT == null ? aa2 != null : !AA2_EDEFAULT.equals(aa2);
      case BaActionPackage.ASSIGNMENT_ACTION__AA3:
        return aa3 != null;
      case BaActionPackage.ASSIGNMENT_ACTION__AA4:
        return AA4_EDEFAULT == null ? aa4 != null : !AA4_EDEFAULT.equals(aa4);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (aa1: ");
    result.append(aa1);
    result.append(", aa2: ");
    result.append(aa2);
    result.append(", aa4: ");
    result.append(aa4);
    result.append(')');
    return result.toString();
  }

} //assignment_actionImpl
