/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.assignment_action;
import org.topcased.adele.xtext.baaction.baAction.basic_action;
import org.topcased.adele.xtext.baaction.baAction.communication_action;
import org.topcased.adele.xtext.baaction.baAction.timed_action;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>basic action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.basic_actionImpl#getBact1 <em>Bact1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.basic_actionImpl#getBact2 <em>Bact2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.basic_actionImpl#getBact3 <em>Bact3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class basic_actionImpl extends MinimalEObjectImpl.Container implements basic_action
{
  /**
   * The cached value of the '{@link #getBact1() <em>Bact1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBact1()
   * @generated
   * @ordered
   */
  protected assignment_action bact1;

  /**
   * The cached value of the '{@link #getBact2() <em>Bact2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBact2()
   * @generated
   * @ordered
   */
  protected communication_action bact2;

  /**
   * The cached value of the '{@link #getBact3() <em>Bact3</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBact3()
   * @generated
   * @ordered
   */
  protected timed_action bact3;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected basic_actionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.BASIC_ACTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public assignment_action getBact1()
  {
    return bact1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBact1(assignment_action newBact1, NotificationChain msgs)
  {
    assignment_action oldBact1 = bact1;
    bact1 = newBact1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BASIC_ACTION__BACT1, oldBact1, newBact1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBact1(assignment_action newBact1)
  {
    if (newBact1 != bact1)
    {
      NotificationChain msgs = null;
      if (bact1 != null)
        msgs = ((InternalEObject)bact1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BASIC_ACTION__BACT1, null, msgs);
      if (newBact1 != null)
        msgs = ((InternalEObject)newBact1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BASIC_ACTION__BACT1, null, msgs);
      msgs = basicSetBact1(newBact1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BASIC_ACTION__BACT1, newBact1, newBact1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public communication_action getBact2()
  {
    return bact2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBact2(communication_action newBact2, NotificationChain msgs)
  {
    communication_action oldBact2 = bact2;
    bact2 = newBact2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BASIC_ACTION__BACT2, oldBact2, newBact2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBact2(communication_action newBact2)
  {
    if (newBact2 != bact2)
    {
      NotificationChain msgs = null;
      if (bact2 != null)
        msgs = ((InternalEObject)bact2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BASIC_ACTION__BACT2, null, msgs);
      if (newBact2 != null)
        msgs = ((InternalEObject)newBact2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BASIC_ACTION__BACT2, null, msgs);
      msgs = basicSetBact2(newBact2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BASIC_ACTION__BACT2, newBact2, newBact2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public timed_action getBact3()
  {
    return bact3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBact3(timed_action newBact3, NotificationChain msgs)
  {
    timed_action oldBact3 = bact3;
    bact3 = newBact3;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BASIC_ACTION__BACT3, oldBact3, newBact3);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBact3(timed_action newBact3)
  {
    if (newBact3 != bact3)
    {
      NotificationChain msgs = null;
      if (bact3 != null)
        msgs = ((InternalEObject)bact3).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BASIC_ACTION__BACT3, null, msgs);
      if (newBact3 != null)
        msgs = ((InternalEObject)newBact3).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BASIC_ACTION__BACT3, null, msgs);
      msgs = basicSetBact3(newBact3, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BASIC_ACTION__BACT3, newBact3, newBact3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.BASIC_ACTION__BACT1:
        return basicSetBact1(null, msgs);
      case BaActionPackage.BASIC_ACTION__BACT2:
        return basicSetBact2(null, msgs);
      case BaActionPackage.BASIC_ACTION__BACT3:
        return basicSetBact3(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.BASIC_ACTION__BACT1:
        return getBact1();
      case BaActionPackage.BASIC_ACTION__BACT2:
        return getBact2();
      case BaActionPackage.BASIC_ACTION__BACT3:
        return getBact3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.BASIC_ACTION__BACT1:
        setBact1((assignment_action)newValue);
        return;
      case BaActionPackage.BASIC_ACTION__BACT2:
        setBact2((communication_action)newValue);
        return;
      case BaActionPackage.BASIC_ACTION__BACT3:
        setBact3((timed_action)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BASIC_ACTION__BACT1:
        setBact1((assignment_action)null);
        return;
      case BaActionPackage.BASIC_ACTION__BACT2:
        setBact2((communication_action)null);
        return;
      case BaActionPackage.BASIC_ACTION__BACT3:
        setBact3((timed_action)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BASIC_ACTION__BACT1:
        return bact1 != null;
      case BaActionPackage.BASIC_ACTION__BACT2:
        return bact2 != null;
      case BaActionPackage.BASIC_ACTION__BACT3:
        return bact3 != null;
    }
    return super.eIsSet(featureID);
  }

} //basic_actionImpl
