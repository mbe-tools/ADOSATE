/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.value_variable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>value variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.value_variableImpl#getVvname0 <em>Vvname0</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.value_variableImpl#getVvname1 <em>Vvname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.value_variableImpl#getVvname2 <em>Vvname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.value_variableImpl#getVvname3 <em>Vvname3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class value_variableImpl extends MinimalEObjectImpl.Container implements value_variable
{
  /**
   * The default value of the '{@link #getVvname0() <em>Vvname0</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVvname0()
   * @generated
   * @ordered
   */
  protected static final String VVNAME0_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVvname0() <em>Vvname0</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVvname0()
   * @generated
   * @ordered
   */
  protected String vvname0 = VVNAME0_EDEFAULT;

  /**
   * The default value of the '{@link #getVvname1() <em>Vvname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVvname1()
   * @generated
   * @ordered
   */
  protected static final String VVNAME1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVvname1() <em>Vvname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVvname1()
   * @generated
   * @ordered
   */
  protected String vvname1 = VVNAME1_EDEFAULT;

  /**
   * The default value of the '{@link #getVvname2() <em>Vvname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVvname2()
   * @generated
   * @ordered
   */
  protected static final String VVNAME2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVvname2() <em>Vvname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVvname2()
   * @generated
   * @ordered
   */
  protected String vvname2 = VVNAME2_EDEFAULT;

  /**
   * The default value of the '{@link #getVvname3() <em>Vvname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVvname3()
   * @generated
   * @ordered
   */
  protected static final String VVNAME3_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVvname3() <em>Vvname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVvname3()
   * @generated
   * @ordered
   */
  protected String vvname3 = VVNAME3_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected value_variableImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.VALUE_VARIABLE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVvname0()
  {
    return vvname0;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVvname0(String newVvname0)
  {
    String oldVvname0 = vvname0;
    vvname0 = newVvname0;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.VALUE_VARIABLE__VVNAME0, oldVvname0, vvname0));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVvname1()
  {
    return vvname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVvname1(String newVvname1)
  {
    String oldVvname1 = vvname1;
    vvname1 = newVvname1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.VALUE_VARIABLE__VVNAME1, oldVvname1, vvname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVvname2()
  {
    return vvname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVvname2(String newVvname2)
  {
    String oldVvname2 = vvname2;
    vvname2 = newVvname2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.VALUE_VARIABLE__VVNAME2, oldVvname2, vvname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVvname3()
  {
    return vvname3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVvname3(String newVvname3)
  {
    String oldVvname3 = vvname3;
    vvname3 = newVvname3;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.VALUE_VARIABLE__VVNAME3, oldVvname3, vvname3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.VALUE_VARIABLE__VVNAME0:
        return getVvname0();
      case BaActionPackage.VALUE_VARIABLE__VVNAME1:
        return getVvname1();
      case BaActionPackage.VALUE_VARIABLE__VVNAME2:
        return getVvname2();
      case BaActionPackage.VALUE_VARIABLE__VVNAME3:
        return getVvname3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.VALUE_VARIABLE__VVNAME0:
        setVvname0((String)newValue);
        return;
      case BaActionPackage.VALUE_VARIABLE__VVNAME1:
        setVvname1((String)newValue);
        return;
      case BaActionPackage.VALUE_VARIABLE__VVNAME2:
        setVvname2((String)newValue);
        return;
      case BaActionPackage.VALUE_VARIABLE__VVNAME3:
        setVvname3((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.VALUE_VARIABLE__VVNAME0:
        setVvname0(VVNAME0_EDEFAULT);
        return;
      case BaActionPackage.VALUE_VARIABLE__VVNAME1:
        setVvname1(VVNAME1_EDEFAULT);
        return;
      case BaActionPackage.VALUE_VARIABLE__VVNAME2:
        setVvname2(VVNAME2_EDEFAULT);
        return;
      case BaActionPackage.VALUE_VARIABLE__VVNAME3:
        setVvname3(VVNAME3_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.VALUE_VARIABLE__VVNAME0:
        return VVNAME0_EDEFAULT == null ? vvname0 != null : !VVNAME0_EDEFAULT.equals(vvname0);
      case BaActionPackage.VALUE_VARIABLE__VVNAME1:
        return VVNAME1_EDEFAULT == null ? vvname1 != null : !VVNAME1_EDEFAULT.equals(vvname1);
      case BaActionPackage.VALUE_VARIABLE__VVNAME2:
        return VVNAME2_EDEFAULT == null ? vvname2 != null : !VVNAME2_EDEFAULT.equals(vvname2);
      case BaActionPackage.VALUE_VARIABLE__VVNAME3:
        return VVNAME3_EDEFAULT == null ? vvname3 != null : !VVNAME3_EDEFAULT.equals(vvname3);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (vvname0: ");
    result.append(vvname0);
    result.append(", vvname1: ");
    result.append(vvname1);
    result.append(", vvname2: ");
    result.append(vvname2);
    result.append(", vvname3: ");
    result.append(vvname3);
    result.append(')');
    return result.toString();
  }

} //value_variableImpl
