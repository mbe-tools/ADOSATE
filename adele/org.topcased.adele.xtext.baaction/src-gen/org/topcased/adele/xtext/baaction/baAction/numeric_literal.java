/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>numeric literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.numeric_literal#getNlname1 <em>Nlname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.numeric_literal#getNlname2 <em>Nlname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getnumeric_literal()
 * @model
 * @generated
 */
public interface numeric_literal extends EObject
{
  /**
   * Returns the value of the '<em><b>Nlname1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nlname1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nlname1</em>' containment reference.
   * @see #setNlname1(integer_literal)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getnumeric_literal_Nlname1()
   * @model containment="true"
   * @generated
   */
  integer_literal getNlname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.numeric_literal#getNlname1 <em>Nlname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nlname1</em>' containment reference.
   * @see #getNlname1()
   * @generated
   */
  void setNlname1(integer_literal value);

  /**
   * Returns the value of the '<em><b>Nlname2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nlname2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nlname2</em>' containment reference.
   * @see #setNlname2(real_literal)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getnumeric_literal_Nlname2()
   * @model containment="true"
   * @generated
   */
  real_literal getNlname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.numeric_literal#getNlname2 <em>Nlname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nlname2</em>' containment reference.
   * @see #getNlname2()
   * @generated
   */
  void setNlname2(real_literal value);

} // numeric_literal
