/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>integer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.integer#getValue1 <em>Value1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.integer#getValue2 <em>Value2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getinteger()
 * @model
 * @generated
 */
public interface integer extends EObject
{
  /**
   * Returns the value of the '<em><b>Value1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value1</em>' attribute.
   * @see #setValue1(int)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getinteger_Value1()
   * @model
   * @generated
   */
  int getValue1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.integer#getValue1 <em>Value1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value1</em>' attribute.
   * @see #getValue1()
   * @generated
   */
  void setValue1(int value);

  /**
   * Returns the value of the '<em><b>Value2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value2</em>' attribute.
   * @see #setValue2(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getinteger_Value2()
   * @model
   * @generated
   */
  String getValue2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.integer#getValue2 <em>Value2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value2</em>' attribute.
   * @see #getValue2()
   * @generated
   */
  void setValue2(String value);

} // integer
