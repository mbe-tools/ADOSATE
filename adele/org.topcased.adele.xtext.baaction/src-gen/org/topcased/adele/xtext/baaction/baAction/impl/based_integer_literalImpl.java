/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.base;
import org.topcased.adele.xtext.baaction.baAction.based_integer_literal;
import org.topcased.adele.xtext.baaction.baAction.based_numeral;
import org.topcased.adele.xtext.baaction.baAction.positive_exponent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>based integer literal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.based_integer_literalImpl#getBilname1 <em>Bilname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.based_integer_literalImpl#getBilname2 <em>Bilname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.based_integer_literalImpl#getBilname3 <em>Bilname3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class based_integer_literalImpl extends MinimalEObjectImpl.Container implements based_integer_literal
{
  /**
   * The cached value of the '{@link #getBilname1() <em>Bilname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBilname1()
   * @generated
   * @ordered
   */
  protected base bilname1;

  /**
   * The cached value of the '{@link #getBilname2() <em>Bilname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBilname2()
   * @generated
   * @ordered
   */
  protected based_numeral bilname2;

  /**
   * The cached value of the '{@link #getBilname3() <em>Bilname3</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBilname3()
   * @generated
   * @ordered
   */
  protected positive_exponent bilname3;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected based_integer_literalImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.BASED_INTEGER_LITERAL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public base getBilname1()
  {
    return bilname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBilname1(base newBilname1, NotificationChain msgs)
  {
    base oldBilname1 = bilname1;
    bilname1 = newBilname1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BASED_INTEGER_LITERAL__BILNAME1, oldBilname1, newBilname1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBilname1(base newBilname1)
  {
    if (newBilname1 != bilname1)
    {
      NotificationChain msgs = null;
      if (bilname1 != null)
        msgs = ((InternalEObject)bilname1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BASED_INTEGER_LITERAL__BILNAME1, null, msgs);
      if (newBilname1 != null)
        msgs = ((InternalEObject)newBilname1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BASED_INTEGER_LITERAL__BILNAME1, null, msgs);
      msgs = basicSetBilname1(newBilname1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BASED_INTEGER_LITERAL__BILNAME1, newBilname1, newBilname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public based_numeral getBilname2()
  {
    return bilname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBilname2(based_numeral newBilname2, NotificationChain msgs)
  {
    based_numeral oldBilname2 = bilname2;
    bilname2 = newBilname2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BASED_INTEGER_LITERAL__BILNAME2, oldBilname2, newBilname2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBilname2(based_numeral newBilname2)
  {
    if (newBilname2 != bilname2)
    {
      NotificationChain msgs = null;
      if (bilname2 != null)
        msgs = ((InternalEObject)bilname2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BASED_INTEGER_LITERAL__BILNAME2, null, msgs);
      if (newBilname2 != null)
        msgs = ((InternalEObject)newBilname2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BASED_INTEGER_LITERAL__BILNAME2, null, msgs);
      msgs = basicSetBilname2(newBilname2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BASED_INTEGER_LITERAL__BILNAME2, newBilname2, newBilname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public positive_exponent getBilname3()
  {
    return bilname3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBilname3(positive_exponent newBilname3, NotificationChain msgs)
  {
    positive_exponent oldBilname3 = bilname3;
    bilname3 = newBilname3;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.BASED_INTEGER_LITERAL__BILNAME3, oldBilname3, newBilname3);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBilname3(positive_exponent newBilname3)
  {
    if (newBilname3 != bilname3)
    {
      NotificationChain msgs = null;
      if (bilname3 != null)
        msgs = ((InternalEObject)bilname3).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BASED_INTEGER_LITERAL__BILNAME3, null, msgs);
      if (newBilname3 != null)
        msgs = ((InternalEObject)newBilname3).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.BASED_INTEGER_LITERAL__BILNAME3, null, msgs);
      msgs = basicSetBilname3(newBilname3, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BASED_INTEGER_LITERAL__BILNAME3, newBilname3, newBilname3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.BASED_INTEGER_LITERAL__BILNAME1:
        return basicSetBilname1(null, msgs);
      case BaActionPackage.BASED_INTEGER_LITERAL__BILNAME2:
        return basicSetBilname2(null, msgs);
      case BaActionPackage.BASED_INTEGER_LITERAL__BILNAME3:
        return basicSetBilname3(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.BASED_INTEGER_LITERAL__BILNAME1:
        return getBilname1();
      case BaActionPackage.BASED_INTEGER_LITERAL__BILNAME2:
        return getBilname2();
      case BaActionPackage.BASED_INTEGER_LITERAL__BILNAME3:
        return getBilname3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.BASED_INTEGER_LITERAL__BILNAME1:
        setBilname1((base)newValue);
        return;
      case BaActionPackage.BASED_INTEGER_LITERAL__BILNAME2:
        setBilname2((based_numeral)newValue);
        return;
      case BaActionPackage.BASED_INTEGER_LITERAL__BILNAME3:
        setBilname3((positive_exponent)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BASED_INTEGER_LITERAL__BILNAME1:
        setBilname1((base)null);
        return;
      case BaActionPackage.BASED_INTEGER_LITERAL__BILNAME2:
        setBilname2((based_numeral)null);
        return;
      case BaActionPackage.BASED_INTEGER_LITERAL__BILNAME3:
        setBilname3((positive_exponent)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BASED_INTEGER_LITERAL__BILNAME1:
        return bilname1 != null;
      case BaActionPackage.BASED_INTEGER_LITERAL__BILNAME2:
        return bilname2 != null;
      case BaActionPackage.BASED_INTEGER_LITERAL__BILNAME3:
        return bilname3 != null;
    }
    return super.eIsSet(featureID);
  }

} //based_integer_literalImpl
