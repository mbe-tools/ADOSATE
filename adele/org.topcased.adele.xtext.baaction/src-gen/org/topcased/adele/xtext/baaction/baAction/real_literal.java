/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>real literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.real_literal#getRlname <em>Rlname</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getreal_literal()
 * @model
 * @generated
 */
public interface real_literal extends EObject
{
  /**
   * Returns the value of the '<em><b>Rlname</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rlname</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rlname</em>' containment reference.
   * @see #setRlname(decimal_real_literal)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getreal_literal_Rlname()
   * @model containment="true"
   * @generated
   */
  decimal_real_literal getRlname();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.real_literal#getRlname <em>Rlname</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Rlname</em>' containment reference.
   * @see #getRlname()
   * @generated
   */
  void setRlname(decimal_real_literal value);

} // real_literal
