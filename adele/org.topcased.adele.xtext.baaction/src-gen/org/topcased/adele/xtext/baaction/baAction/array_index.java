/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>array index</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.array_index#getAi1 <em>Ai1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.array_index#getAi2 <em>Ai2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.array_index#getAi3 <em>Ai3</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getarray_index()
 * @model
 * @generated
 */
public interface array_index extends EObject
{
  /**
   * Returns the value of the '<em><b>Ai1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ai1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ai1</em>' attribute.
   * @see #setAi1(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getarray_index_Ai1()
   * @model
   * @generated
   */
  String getAi1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.array_index#getAi1 <em>Ai1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ai1</em>' attribute.
   * @see #getAi1()
   * @generated
   */
  void setAi1(String value);

  /**
   * Returns the value of the '<em><b>Ai2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ai2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ai2</em>' containment reference.
   * @see #setAi2(integer_literal)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getarray_index_Ai2()
   * @model containment="true"
   * @generated
   */
  integer_literal getAi2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.array_index#getAi2 <em>Ai2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ai2</em>' containment reference.
   * @see #getAi2()
   * @generated
   */
  void setAi2(integer_literal value);

  /**
   * Returns the value of the '<em><b>Ai3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ai3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ai3</em>' attribute.
   * @see #setAi3(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getarray_index_Ai3()
   * @model
   * @generated
   */
  String getAi3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.array_index#getAi3 <em>Ai3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ai3</em>' attribute.
   * @see #getAi3()
   * @generated
   */
  void setAi3(String value);

} // array_index
