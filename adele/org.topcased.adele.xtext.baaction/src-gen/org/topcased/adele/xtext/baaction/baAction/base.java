/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>base</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.base#getBname1 <em>Bname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.base#getBname2 <em>Bname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbase()
 * @model
 * @generated
 */
public interface base extends EObject
{
  /**
   * Returns the value of the '<em><b>Bname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bname1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bname1</em>' attribute.
   * @see #setBname1(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbase_Bname1()
   * @model
   * @generated
   */
  String getBname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.base#getBname1 <em>Bname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bname1</em>' attribute.
   * @see #getBname1()
   * @generated
   */
  void setBname1(String value);

  /**
   * Returns the value of the '<em><b>Bname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bname2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bname2</em>' attribute.
   * @see #setBname2(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbase_Bname2()
   * @model
   * @generated
   */
  String getBname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.base#getBname2 <em>Bname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bname2</em>' attribute.
   * @see #getBname2()
   * @generated
   */
  void setBname2(String value);

} // base
