/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>binary adding operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.binary_adding_operator#getBaoname1 <em>Baoname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.binary_adding_operator#getBaoname2 <em>Baoname2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbinary_adding_operator()
 * @model
 * @generated
 */
public interface binary_adding_operator extends EObject
{
  /**
   * Returns the value of the '<em><b>Baoname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Baoname1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Baoname1</em>' attribute.
   * @see #setBaoname1(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbinary_adding_operator_Baoname1()
   * @model
   * @generated
   */
  String getBaoname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.binary_adding_operator#getBaoname1 <em>Baoname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Baoname1</em>' attribute.
   * @see #getBaoname1()
   * @generated
   */
  void setBaoname1(String value);

  /**
   * Returns the value of the '<em><b>Baoname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Baoname2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Baoname2</em>' attribute.
   * @see #setBaoname2(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbinary_adding_operator_Baoname2()
   * @model
   * @generated
   */
  String getBaoname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.binary_adding_operator#getBaoname2 <em>Baoname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Baoname2</em>' attribute.
   * @see #getBaoname2()
   * @generated
   */
  void setBaoname2(String value);

} // binary_adding_operator
