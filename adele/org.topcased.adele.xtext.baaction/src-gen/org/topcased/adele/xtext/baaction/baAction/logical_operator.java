/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>logical operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.logical_operator#getLoname1 <em>Loname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.logical_operator#getLoname2 <em>Loname2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.logical_operator#getLoname3 <em>Loname3</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getlogical_operator()
 * @model
 * @generated
 */
public interface logical_operator extends EObject
{
  /**
   * Returns the value of the '<em><b>Loname1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Loname1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Loname1</em>' attribute.
   * @see #setLoname1(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getlogical_operator_Loname1()
   * @model
   * @generated
   */
  String getLoname1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.logical_operator#getLoname1 <em>Loname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Loname1</em>' attribute.
   * @see #getLoname1()
   * @generated
   */
  void setLoname1(String value);

  /**
   * Returns the value of the '<em><b>Loname2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Loname2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Loname2</em>' attribute.
   * @see #setLoname2(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getlogical_operator_Loname2()
   * @model
   * @generated
   */
  String getLoname2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.logical_operator#getLoname2 <em>Loname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Loname2</em>' attribute.
   * @see #getLoname2()
   * @generated
   */
  void setLoname2(String value);

  /**
   * Returns the value of the '<em><b>Loname3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Loname3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Loname3</em>' attribute.
   * @see #setLoname3(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getlogical_operator_Loname3()
   * @model
   * @generated
   */
  String getLoname3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.logical_operator#getLoname3 <em>Loname3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Loname3</em>' attribute.
   * @see #getLoname3()
   * @generated
   */
  void setLoname3(String value);

} // logical_operator
