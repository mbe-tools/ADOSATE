/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.topcased.adele.xtext.baaction.baAction.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage
 * @generated
 */
public class BaActionSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static BaActionPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaActionSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = BaActionPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @parameter ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case BaActionPackage.BEHAVIOR_ACTION_BLOCK:
      {
        behavior_action_block behavior_action_block = (behavior_action_block)theEObject;
        T result = casebehavior_action_block(behavior_action_block);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.BEHAVIOR_ACTION:
      {
        behavior_action behavior_action = (behavior_action)theEObject;
        T result = casebehavior_action(behavior_action);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.BEHAVIOR_ACTIONS:
      {
        behavior_actions behavior_actions = (behavior_actions)theEObject;
        T result = casebehavior_actions(behavior_actions);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE:
      {
        behavior_action_sequence behavior_action_sequence = (behavior_action_sequence)theEObject;
        T result = casebehavior_action_sequence(behavior_action_sequence);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.BEHAVIOR_ACTION_SET:
      {
        behavior_action_set behavior_action_set = (behavior_action_set)theEObject;
        T result = casebehavior_action_set(behavior_action_set);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.ELEMENT_VALUES:
      {
        element_values element_values = (element_values)theEObject;
        T result = caseelement_values(element_values);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE:
      {
        array_data_component_reference array_data_component_reference = (array_data_component_reference)theEObject;
        T result = casearray_data_component_reference(array_data_component_reference);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.BASIC_ACTION:
      {
        basic_action basic_action = (basic_action)theEObject;
        T result = casebasic_action(basic_action);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.ASSIGNMENT_ACTION:
      {
        assignment_action assignment_action = (assignment_action)theEObject;
        T result = caseassignment_action(assignment_action);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.COMMUNICATION_ACTION:
      {
        communication_action communication_action = (communication_action)theEObject;
        T result = casecommunication_action(communication_action);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.TIMED_ACTION:
      {
        timed_action timed_action = (timed_action)theEObject;
        T result = casetimed_action(timed_action);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.SUBPROGRAM_PARAMETER_LIST:
      {
        subprogram_parameter_list subprogram_parameter_list = (subprogram_parameter_list)theEObject;
        T result = casesubprogram_parameter_list(subprogram_parameter_list);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.PARAMETER_LABEL:
      {
        parameter_label parameter_label = (parameter_label)theEObject;
        T result = caseparameter_label(parameter_label);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.DATA_COMPONENT_REFERENCE:
      {
        data_component_reference data_component_reference = (data_component_reference)theEObject;
        T result = casedata_component_reference(data_component_reference);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.NAME:
      {
        name name = (name)theEObject;
        T result = casename(name);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.ARRAY_INDEX:
      {
        array_index array_index = (array_index)theEObject;
        T result = casearray_index(array_index);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.INTEGER_VALUE:
      {
        integer_value integer_value = (integer_value)theEObject;
        T result = caseinteger_value(integer_value);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.NUMERIC_LITERAL:
      {
        numeric_literal numeric_literal = (numeric_literal)theEObject;
        T result = casenumeric_literal(numeric_literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.INTEGER_LITERAL:
      {
        integer_literal integer_literal = (integer_literal)theEObject;
        T result = caseinteger_literal(integer_literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.REAL_LITERAL:
      {
        real_literal real_literal = (real_literal)theEObject;
        T result = casereal_literal(real_literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.DECIMAL_INTEGER_LITERAL:
      {
        decimal_integer_literal decimal_integer_literal = (decimal_integer_literal)theEObject;
        T result = casedecimal_integer_literal(decimal_integer_literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.DECIMAL_REAL_LITERAL:
      {
        decimal_real_literal decimal_real_literal = (decimal_real_literal)theEObject;
        T result = casedecimal_real_literal(decimal_real_literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.NUMERAL:
      {
        numeral numeral = (numeral)theEObject;
        T result = casenumeral(numeral);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.EXPONENT:
      {
        exponent exponent = (exponent)theEObject;
        T result = caseexponent(exponent);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.POSITIVE_EXPONENT:
      {
        positive_exponent positive_exponent = (positive_exponent)theEObject;
        T result = casepositive_exponent(positive_exponent);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.BASED_INTEGER_LITERAL:
      {
        based_integer_literal based_integer_literal = (based_integer_literal)theEObject;
        T result = casebased_integer_literal(based_integer_literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.BASE:
      {
        base base = (base)theEObject;
        T result = casebase(base);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.BASED_NUMERAL:
      {
        based_numeral based_numeral = (based_numeral)theEObject;
        T result = casebased_numeral(based_numeral);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.BEHAVIOR_TIME:
      {
        behavior_time behavior_time = (behavior_time)theEObject;
        T result = casebehavior_time(behavior_time);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.UNIT_IDENTIFIER:
      {
        unit_identifier unit_identifier = (unit_identifier)theEObject;
        T result = caseunit_identifier(unit_identifier);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.INTEGER_RANGE:
      {
        integer_range integer_range = (integer_range)theEObject;
        T result = caseinteger_range(integer_range);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.INTEGER:
      {
        integer integer = (integer)theEObject;
        T result = caseinteger(integer);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.VALUE_EXPRESSION:
      {
        value_expression value_expression = (value_expression)theEObject;
        T result = casevalue_expression(value_expression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.RELATION:
      {
        relation relation = (relation)theEObject;
        T result = caserelation(relation);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.SIMPLE_EXPRESSION:
      {
        simple_expression simple_expression = (simple_expression)theEObject;
        T result = casesimple_expression(simple_expression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.TERM:
      {
        term term = (term)theEObject;
        T result = caseterm(term);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.FACTOR:
      {
        factor factor = (factor)theEObject;
        T result = casefactor(factor);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.VALUE:
      {
        value value = (value)theEObject;
        T result = casevalue(value);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.VALUE_VARIABLE:
      {
        value_variable value_variable = (value_variable)theEObject;
        T result = casevalue_variable(value_variable);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.VALUE_CONSTANT:
      {
        value_constant value_constant = (value_constant)theEObject;
        T result = casevalue_constant(value_constant);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.LOGICAL_OPERATOR:
      {
        logical_operator logical_operator = (logical_operator)theEObject;
        T result = caselogical_operator(logical_operator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.RELATIONAL_OPERATOR:
      {
        relational_operator relational_operator = (relational_operator)theEObject;
        T result = caserelational_operator(relational_operator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.BINARY_ADDING_OPERATOR:
      {
        binary_adding_operator binary_adding_operator = (binary_adding_operator)theEObject;
        T result = casebinary_adding_operator(binary_adding_operator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.UNARY_ADDING_OPERATOR:
      {
        unary_adding_operator unary_adding_operator = (unary_adding_operator)theEObject;
        T result = caseunary_adding_operator(unary_adding_operator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.MULTIPLYING_OPERATOR:
      {
        multiplying_operator multiplying_operator = (multiplying_operator)theEObject;
        T result = casemultiplying_operator(multiplying_operator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.BINARY_NUMERIC_OPERATOR:
      {
        binary_numeric_operator binary_numeric_operator = (binary_numeric_operator)theEObject;
        T result = casebinary_numeric_operator(binary_numeric_operator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.UNARY_NUMERIC_OPERATOR:
      {
        unary_numeric_operator unary_numeric_operator = (unary_numeric_operator)theEObject;
        T result = caseunary_numeric_operator(unary_numeric_operator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.UNARY_BOOLEAN_OPERATOR:
      {
        unary_boolean_operator unary_boolean_operator = (unary_boolean_operator)theEObject;
        T result = caseunary_boolean_operator(unary_boolean_operator);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case BaActionPackage.BOOLEAN_LITERAL:
      {
        boolean_literal boolean_literal = (boolean_literal)theEObject;
        T result = caseboolean_literal(boolean_literal);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>behavior action block</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>behavior action block</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebehavior_action_block(behavior_action_block object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>behavior action</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>behavior action</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebehavior_action(behavior_action object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>behavior actions</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>behavior actions</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebehavior_actions(behavior_actions object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>behavior action sequence</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>behavior action sequence</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebehavior_action_sequence(behavior_action_sequence object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>behavior action set</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>behavior action set</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebehavior_action_set(behavior_action_set object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>element values</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>element values</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseelement_values(element_values object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>array data component reference</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>array data component reference</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casearray_data_component_reference(array_data_component_reference object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>basic action</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>basic action</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebasic_action(basic_action object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>assignment action</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>assignment action</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseassignment_action(assignment_action object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>communication action</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>communication action</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casecommunication_action(communication_action object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>timed action</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>timed action</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casetimed_action(timed_action object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>subprogram parameter list</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>subprogram parameter list</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casesubprogram_parameter_list(subprogram_parameter_list object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>parameter label</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>parameter label</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseparameter_label(parameter_label object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>data component reference</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>data component reference</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casedata_component_reference(data_component_reference object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>name</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>name</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casename(name object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>array index</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>array index</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casearray_index(array_index object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>integer value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>integer value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseinteger_value(integer_value object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>numeric literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>numeric literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casenumeric_literal(numeric_literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>integer literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>integer literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseinteger_literal(integer_literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>real literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>real literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casereal_literal(real_literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>decimal integer literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>decimal integer literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casedecimal_integer_literal(decimal_integer_literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>decimal real literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>decimal real literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casedecimal_real_literal(decimal_real_literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>numeral</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>numeral</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casenumeral(numeral object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>exponent</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>exponent</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseexponent(exponent object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>positive exponent</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>positive exponent</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casepositive_exponent(positive_exponent object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>based integer literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>based integer literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebased_integer_literal(based_integer_literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>base</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>base</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebase(base object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>based numeral</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>based numeral</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebased_numeral(based_numeral object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>behavior time</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>behavior time</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebehavior_time(behavior_time object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>unit identifier</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>unit identifier</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseunit_identifier(unit_identifier object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>integer range</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>integer range</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseinteger_range(integer_range object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>integer</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>integer</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseinteger(integer object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>value expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>value expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casevalue_expression(value_expression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>relation</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>relation</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caserelation(relation object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>simple expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>simple expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casesimple_expression(simple_expression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>term</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>term</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseterm(term object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>factor</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>factor</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casefactor(factor object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casevalue(value object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>value variable</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>value variable</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casevalue_variable(value_variable object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>value constant</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>value constant</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casevalue_constant(value_constant object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>logical operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>logical operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caselogical_operator(logical_operator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>relational operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>relational operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caserelational_operator(relational_operator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>binary adding operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>binary adding operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebinary_adding_operator(binary_adding_operator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>unary adding operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>unary adding operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseunary_adding_operator(unary_adding_operator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>multiplying operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>multiplying operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casemultiplying_operator(multiplying_operator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>binary numeric operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>binary numeric operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casebinary_numeric_operator(binary_numeric_operator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>unary numeric operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>unary numeric operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseunary_numeric_operator(unary_numeric_operator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>unary boolean operator</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>unary boolean operator</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseunary_boolean_operator(unary_boolean_operator object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>boolean literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>boolean literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseboolean_literal(boolean_literal object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //BaActionSwitch
