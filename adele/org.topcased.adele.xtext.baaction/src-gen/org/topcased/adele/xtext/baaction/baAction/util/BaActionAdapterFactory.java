/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.topcased.adele.xtext.baaction.baAction.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage
 * @generated
 */
public class BaActionAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static BaActionPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaActionAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = BaActionPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BaActionSwitch<Adapter> modelSwitch =
    new BaActionSwitch<Adapter>()
    {
      @Override
      public Adapter casebehavior_action_block(behavior_action_block object)
      {
        return createbehavior_action_blockAdapter();
      }
      @Override
      public Adapter casebehavior_action(behavior_action object)
      {
        return createbehavior_actionAdapter();
      }
      @Override
      public Adapter casebehavior_actions(behavior_actions object)
      {
        return createbehavior_actionsAdapter();
      }
      @Override
      public Adapter casebehavior_action_sequence(behavior_action_sequence object)
      {
        return createbehavior_action_sequenceAdapter();
      }
      @Override
      public Adapter casebehavior_action_set(behavior_action_set object)
      {
        return createbehavior_action_setAdapter();
      }
      @Override
      public Adapter caseelement_values(element_values object)
      {
        return createelement_valuesAdapter();
      }
      @Override
      public Adapter casearray_data_component_reference(array_data_component_reference object)
      {
        return createarray_data_component_referenceAdapter();
      }
      @Override
      public Adapter casebasic_action(basic_action object)
      {
        return createbasic_actionAdapter();
      }
      @Override
      public Adapter caseassignment_action(assignment_action object)
      {
        return createassignment_actionAdapter();
      }
      @Override
      public Adapter casecommunication_action(communication_action object)
      {
        return createcommunication_actionAdapter();
      }
      @Override
      public Adapter casetimed_action(timed_action object)
      {
        return createtimed_actionAdapter();
      }
      @Override
      public Adapter casesubprogram_parameter_list(subprogram_parameter_list object)
      {
        return createsubprogram_parameter_listAdapter();
      }
      @Override
      public Adapter caseparameter_label(parameter_label object)
      {
        return createparameter_labelAdapter();
      }
      @Override
      public Adapter casedata_component_reference(data_component_reference object)
      {
        return createdata_component_referenceAdapter();
      }
      @Override
      public Adapter casename(name object)
      {
        return createnameAdapter();
      }
      @Override
      public Adapter casearray_index(array_index object)
      {
        return createarray_indexAdapter();
      }
      @Override
      public Adapter caseinteger_value(integer_value object)
      {
        return createinteger_valueAdapter();
      }
      @Override
      public Adapter casenumeric_literal(numeric_literal object)
      {
        return createnumeric_literalAdapter();
      }
      @Override
      public Adapter caseinteger_literal(integer_literal object)
      {
        return createinteger_literalAdapter();
      }
      @Override
      public Adapter casereal_literal(real_literal object)
      {
        return createreal_literalAdapter();
      }
      @Override
      public Adapter casedecimal_integer_literal(decimal_integer_literal object)
      {
        return createdecimal_integer_literalAdapter();
      }
      @Override
      public Adapter casedecimal_real_literal(decimal_real_literal object)
      {
        return createdecimal_real_literalAdapter();
      }
      @Override
      public Adapter casenumeral(numeral object)
      {
        return createnumeralAdapter();
      }
      @Override
      public Adapter caseexponent(exponent object)
      {
        return createexponentAdapter();
      }
      @Override
      public Adapter casepositive_exponent(positive_exponent object)
      {
        return createpositive_exponentAdapter();
      }
      @Override
      public Adapter casebased_integer_literal(based_integer_literal object)
      {
        return createbased_integer_literalAdapter();
      }
      @Override
      public Adapter casebase(base object)
      {
        return createbaseAdapter();
      }
      @Override
      public Adapter casebased_numeral(based_numeral object)
      {
        return createbased_numeralAdapter();
      }
      @Override
      public Adapter casebehavior_time(behavior_time object)
      {
        return createbehavior_timeAdapter();
      }
      @Override
      public Adapter caseunit_identifier(unit_identifier object)
      {
        return createunit_identifierAdapter();
      }
      @Override
      public Adapter caseinteger_range(integer_range object)
      {
        return createinteger_rangeAdapter();
      }
      @Override
      public Adapter caseinteger(integer object)
      {
        return createintegerAdapter();
      }
      @Override
      public Adapter casevalue_expression(value_expression object)
      {
        return createvalue_expressionAdapter();
      }
      @Override
      public Adapter caserelation(relation object)
      {
        return createrelationAdapter();
      }
      @Override
      public Adapter casesimple_expression(simple_expression object)
      {
        return createsimple_expressionAdapter();
      }
      @Override
      public Adapter caseterm(term object)
      {
        return createtermAdapter();
      }
      @Override
      public Adapter casefactor(factor object)
      {
        return createfactorAdapter();
      }
      @Override
      public Adapter casevalue(value object)
      {
        return createvalueAdapter();
      }
      @Override
      public Adapter casevalue_variable(value_variable object)
      {
        return createvalue_variableAdapter();
      }
      @Override
      public Adapter casevalue_constant(value_constant object)
      {
        return createvalue_constantAdapter();
      }
      @Override
      public Adapter caselogical_operator(logical_operator object)
      {
        return createlogical_operatorAdapter();
      }
      @Override
      public Adapter caserelational_operator(relational_operator object)
      {
        return createrelational_operatorAdapter();
      }
      @Override
      public Adapter casebinary_adding_operator(binary_adding_operator object)
      {
        return createbinary_adding_operatorAdapter();
      }
      @Override
      public Adapter caseunary_adding_operator(unary_adding_operator object)
      {
        return createunary_adding_operatorAdapter();
      }
      @Override
      public Adapter casemultiplying_operator(multiplying_operator object)
      {
        return createmultiplying_operatorAdapter();
      }
      @Override
      public Adapter casebinary_numeric_operator(binary_numeric_operator object)
      {
        return createbinary_numeric_operatorAdapter();
      }
      @Override
      public Adapter caseunary_numeric_operator(unary_numeric_operator object)
      {
        return createunary_numeric_operatorAdapter();
      }
      @Override
      public Adapter caseunary_boolean_operator(unary_boolean_operator object)
      {
        return createunary_boolean_operatorAdapter();
      }
      @Override
      public Adapter caseboolean_literal(boolean_literal object)
      {
        return createboolean_literalAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_block <em>behavior action block</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_block
   * @generated
   */
  public Adapter createbehavior_action_blockAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action <em>behavior action</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action
   * @generated
   */
  public Adapter createbehavior_actionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.behavior_actions <em>behavior actions</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_actions
   * @generated
   */
  public Adapter createbehavior_actionsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence <em>behavior action sequence</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence
   * @generated
   */
  public Adapter createbehavior_action_sequenceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_set <em>behavior action set</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_action_set
   * @generated
   */
  public Adapter createbehavior_action_setAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.element_values <em>element values</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.element_values
   * @generated
   */
  public Adapter createelement_valuesAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.array_data_component_reference <em>array data component reference</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.array_data_component_reference
   * @generated
   */
  public Adapter createarray_data_component_referenceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.basic_action <em>basic action</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.basic_action
   * @generated
   */
  public Adapter createbasic_actionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.assignment_action <em>assignment action</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.assignment_action
   * @generated
   */
  public Adapter createassignment_actionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.communication_action <em>communication action</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.communication_action
   * @generated
   */
  public Adapter createcommunication_actionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.timed_action <em>timed action</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.timed_action
   * @generated
   */
  public Adapter createtimed_actionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list <em>subprogram parameter list</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list
   * @generated
   */
  public Adapter createsubprogram_parameter_listAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.parameter_label <em>parameter label</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.parameter_label
   * @generated
   */
  public Adapter createparameter_labelAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.data_component_reference <em>data component reference</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.data_component_reference
   * @generated
   */
  public Adapter createdata_component_referenceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.name <em>name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.name
   * @generated
   */
  public Adapter createnameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.array_index <em>array index</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.array_index
   * @generated
   */
  public Adapter createarray_indexAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.integer_value <em>integer value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.integer_value
   * @generated
   */
  public Adapter createinteger_valueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.numeric_literal <em>numeric literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.numeric_literal
   * @generated
   */
  public Adapter createnumeric_literalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.integer_literal <em>integer literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.integer_literal
   * @generated
   */
  public Adapter createinteger_literalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.real_literal <em>real literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.real_literal
   * @generated
   */
  public Adapter createreal_literalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.decimal_integer_literal <em>decimal integer literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.decimal_integer_literal
   * @generated
   */
  public Adapter createdecimal_integer_literalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.decimal_real_literal <em>decimal real literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.decimal_real_literal
   * @generated
   */
  public Adapter createdecimal_real_literalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.numeral <em>numeral</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.numeral
   * @generated
   */
  public Adapter createnumeralAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.exponent <em>exponent</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.exponent
   * @generated
   */
  public Adapter createexponentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.positive_exponent <em>positive exponent</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.positive_exponent
   * @generated
   */
  public Adapter createpositive_exponentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.based_integer_literal <em>based integer literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.based_integer_literal
   * @generated
   */
  public Adapter createbased_integer_literalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.base <em>base</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.base
   * @generated
   */
  public Adapter createbaseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.based_numeral <em>based numeral</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.based_numeral
   * @generated
   */
  public Adapter createbased_numeralAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.behavior_time <em>behavior time</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.behavior_time
   * @generated
   */
  public Adapter createbehavior_timeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.unit_identifier <em>unit identifier</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.unit_identifier
   * @generated
   */
  public Adapter createunit_identifierAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.integer_range <em>integer range</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.integer_range
   * @generated
   */
  public Adapter createinteger_rangeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.integer <em>integer</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.integer
   * @generated
   */
  public Adapter createintegerAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.value_expression <em>value expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.value_expression
   * @generated
   */
  public Adapter createvalue_expressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.relation <em>relation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.relation
   * @generated
   */
  public Adapter createrelationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.simple_expression <em>simple expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.simple_expression
   * @generated
   */
  public Adapter createsimple_expressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.term <em>term</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.term
   * @generated
   */
  public Adapter createtermAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.factor <em>factor</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.factor
   * @generated
   */
  public Adapter createfactorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.value <em>value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.value
   * @generated
   */
  public Adapter createvalueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.value_variable <em>value variable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.value_variable
   * @generated
   */
  public Adapter createvalue_variableAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.value_constant <em>value constant</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.value_constant
   * @generated
   */
  public Adapter createvalue_constantAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.logical_operator <em>logical operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.logical_operator
   * @generated
   */
  public Adapter createlogical_operatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.relational_operator <em>relational operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.relational_operator
   * @generated
   */
  public Adapter createrelational_operatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.binary_adding_operator <em>binary adding operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.binary_adding_operator
   * @generated
   */
  public Adapter createbinary_adding_operatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.unary_adding_operator <em>unary adding operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.unary_adding_operator
   * @generated
   */
  public Adapter createunary_adding_operatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.multiplying_operator <em>multiplying operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.multiplying_operator
   * @generated
   */
  public Adapter createmultiplying_operatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.binary_numeric_operator <em>binary numeric operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.binary_numeric_operator
   * @generated
   */
  public Adapter createbinary_numeric_operatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.unary_numeric_operator <em>unary numeric operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.unary_numeric_operator
   * @generated
   */
  public Adapter createunary_numeric_operatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.unary_boolean_operator <em>unary boolean operator</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.unary_boolean_operator
   * @generated
   */
  public Adapter createunary_boolean_operatorAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.topcased.adele.xtext.baaction.baAction.boolean_literal <em>boolean literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.topcased.adele.xtext.baaction.baAction.boolean_literal
   * @generated
   */
  public Adapter createboolean_literalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //BaActionAdapterFactory
