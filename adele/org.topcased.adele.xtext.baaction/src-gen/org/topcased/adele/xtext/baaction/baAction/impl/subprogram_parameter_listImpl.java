/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.parameter_label;
import org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>subprogram parameter list</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.subprogram_parameter_listImpl#getSpl1 <em>Spl1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.subprogram_parameter_listImpl#getSpl2 <em>Spl2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.subprogram_parameter_listImpl#getSpl3 <em>Spl3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class subprogram_parameter_listImpl extends MinimalEObjectImpl.Container implements subprogram_parameter_list
{
  /**
   * The cached value of the '{@link #getSpl1() <em>Spl1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSpl1()
   * @generated
   * @ordered
   */
  protected parameter_label spl1;

  /**
   * The cached value of the '{@link #getSpl2() <em>Spl2</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSpl2()
   * @generated
   * @ordered
   */
  protected EList<String> spl2;

  /**
   * The cached value of the '{@link #getSpl3() <em>Spl3</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSpl3()
   * @generated
   * @ordered
   */
  protected EList<parameter_label> spl3;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected subprogram_parameter_listImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.SUBPROGRAM_PARAMETER_LIST;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public parameter_label getSpl1()
  {
    return spl1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSpl1(parameter_label newSpl1, NotificationChain msgs)
  {
    parameter_label oldSpl1 = spl1;
    spl1 = newSpl1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL1, oldSpl1, newSpl1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSpl1(parameter_label newSpl1)
  {
    if (newSpl1 != spl1)
    {
      NotificationChain msgs = null;
      if (spl1 != null)
        msgs = ((InternalEObject)spl1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL1, null, msgs);
      if (newSpl1 != null)
        msgs = ((InternalEObject)newSpl1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL1, null, msgs);
      msgs = basicSetSpl1(newSpl1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL1, newSpl1, newSpl1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getSpl2()
  {
    if (spl2 == null)
    {
      spl2 = new EDataTypeEList<String>(String.class, this, BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL2);
    }
    return spl2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<parameter_label> getSpl3()
  {
    if (spl3 == null)
    {
      spl3 = new EObjectContainmentEList<parameter_label>(parameter_label.class, this, BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL3);
    }
    return spl3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL1:
        return basicSetSpl1(null, msgs);
      case BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL3:
        return ((InternalEList<?>)getSpl3()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL1:
        return getSpl1();
      case BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL2:
        return getSpl2();
      case BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL3:
        return getSpl3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL1:
        setSpl1((parameter_label)newValue);
        return;
      case BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL2:
        getSpl2().clear();
        getSpl2().addAll((Collection<? extends String>)newValue);
        return;
      case BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL3:
        getSpl3().clear();
        getSpl3().addAll((Collection<? extends parameter_label>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL1:
        setSpl1((parameter_label)null);
        return;
      case BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL2:
        getSpl2().clear();
        return;
      case BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL3:
        getSpl3().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL1:
        return spl1 != null;
      case BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL2:
        return spl2 != null && !spl2.isEmpty();
      case BaActionPackage.SUBPROGRAM_PARAMETER_LIST__SPL3:
        return spl3 != null && !spl3.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (spl2: ");
    result.append(spl2);
    result.append(')');
    return result.toString();
  }

} //subprogram_parameter_listImpl
