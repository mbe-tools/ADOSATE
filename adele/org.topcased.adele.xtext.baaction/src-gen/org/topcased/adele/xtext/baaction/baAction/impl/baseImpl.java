/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.base;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>base</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.baseImpl#getBname1 <em>Bname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.baseImpl#getBname2 <em>Bname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class baseImpl extends MinimalEObjectImpl.Container implements base
{
  /**
   * The default value of the '{@link #getBname1() <em>Bname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBname1()
   * @generated
   * @ordered
   */
  protected static final String BNAME1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBname1() <em>Bname1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBname1()
   * @generated
   * @ordered
   */
  protected String bname1 = BNAME1_EDEFAULT;

  /**
   * The default value of the '{@link #getBname2() <em>Bname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBname2()
   * @generated
   * @ordered
   */
  protected static final String BNAME2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBname2() <em>Bname2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBname2()
   * @generated
   * @ordered
   */
  protected String bname2 = BNAME2_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected baseImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.BASE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBname1()
  {
    return bname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBname1(String newBname1)
  {
    String oldBname1 = bname1;
    bname1 = newBname1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BASE__BNAME1, oldBname1, bname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBname2()
  {
    return bname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBname2(String newBname2)
  {
    String oldBname2 = bname2;
    bname2 = newBname2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.BASE__BNAME2, oldBname2, bname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.BASE__BNAME1:
        return getBname1();
      case BaActionPackage.BASE__BNAME2:
        return getBname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.BASE__BNAME1:
        setBname1((String)newValue);
        return;
      case BaActionPackage.BASE__BNAME2:
        setBname2((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BASE__BNAME1:
        setBname1(BNAME1_EDEFAULT);
        return;
      case BaActionPackage.BASE__BNAME2:
        setBname2(BNAME2_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.BASE__BNAME1:
        return BNAME1_EDEFAULT == null ? bname1 != null : !BNAME1_EDEFAULT.equals(bname1);
      case BaActionPackage.BASE__BNAME2:
        return BNAME2_EDEFAULT == null ? bname2 != null : !BNAME2_EDEFAULT.equals(bname2);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (bname1: ");
    result.append(bname1);
    result.append(", bname2: ");
    result.append(bname2);
    result.append(')');
    return result.toString();
  }

} //baseImpl
