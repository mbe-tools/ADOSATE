/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.decimal_integer_literal;
import org.topcased.adele.xtext.baaction.baAction.numeral;
import org.topcased.adele.xtext.baaction.baAction.positive_exponent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>decimal integer literal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.decimal_integer_literalImpl#getDilname1 <em>Dilname1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.decimal_integer_literalImpl#getDilname2 <em>Dilname2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class decimal_integer_literalImpl extends MinimalEObjectImpl.Container implements decimal_integer_literal
{
  /**
   * The cached value of the '{@link #getDilname1() <em>Dilname1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDilname1()
   * @generated
   * @ordered
   */
  protected numeral dilname1;

  /**
   * The cached value of the '{@link #getDilname2() <em>Dilname2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDilname2()
   * @generated
   * @ordered
   */
  protected positive_exponent dilname2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected decimal_integer_literalImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.DECIMAL_INTEGER_LITERAL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public numeral getDilname1()
  {
    return dilname1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDilname1(numeral newDilname1, NotificationChain msgs)
  {
    numeral oldDilname1 = dilname1;
    dilname1 = newDilname1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME1, oldDilname1, newDilname1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDilname1(numeral newDilname1)
  {
    if (newDilname1 != dilname1)
    {
      NotificationChain msgs = null;
      if (dilname1 != null)
        msgs = ((InternalEObject)dilname1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME1, null, msgs);
      if (newDilname1 != null)
        msgs = ((InternalEObject)newDilname1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME1, null, msgs);
      msgs = basicSetDilname1(newDilname1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME1, newDilname1, newDilname1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public positive_exponent getDilname2()
  {
    return dilname2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDilname2(positive_exponent newDilname2, NotificationChain msgs)
  {
    positive_exponent oldDilname2 = dilname2;
    dilname2 = newDilname2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME2, oldDilname2, newDilname2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDilname2(positive_exponent newDilname2)
  {
    if (newDilname2 != dilname2)
    {
      NotificationChain msgs = null;
      if (dilname2 != null)
        msgs = ((InternalEObject)dilname2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME2, null, msgs);
      if (newDilname2 != null)
        msgs = ((InternalEObject)newDilname2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME2, null, msgs);
      msgs = basicSetDilname2(newDilname2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME2, newDilname2, newDilname2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME1:
        return basicSetDilname1(null, msgs);
      case BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME2:
        return basicSetDilname2(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME1:
        return getDilname1();
      case BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME2:
        return getDilname2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME1:
        setDilname1((numeral)newValue);
        return;
      case BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME2:
        setDilname2((positive_exponent)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME1:
        setDilname1((numeral)null);
        return;
      case BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME2:
        setDilname2((positive_exponent)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME1:
        return dilname1 != null;
      case BaActionPackage.DECIMAL_INTEGER_LITERAL__DILNAME2:
        return dilname2 != null;
    }
    return super.eIsSet(featureID);
  }

} //decimal_integer_literalImpl
