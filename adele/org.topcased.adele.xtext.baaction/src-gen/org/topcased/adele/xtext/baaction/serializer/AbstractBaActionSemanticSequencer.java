package org.topcased.adele.xtext.baaction.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.array_data_component_reference;
import org.topcased.adele.xtext.baaction.baAction.array_index;
import org.topcased.adele.xtext.baaction.baAction.assignment_action;
import org.topcased.adele.xtext.baaction.baAction.base;
import org.topcased.adele.xtext.baaction.baAction.based_integer_literal;
import org.topcased.adele.xtext.baaction.baAction.based_numeral;
import org.topcased.adele.xtext.baaction.baAction.basic_action;
import org.topcased.adele.xtext.baaction.baAction.behavior_action;
import org.topcased.adele.xtext.baaction.baAction.behavior_action_block;
import org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence;
import org.topcased.adele.xtext.baaction.baAction.behavior_action_set;
import org.topcased.adele.xtext.baaction.baAction.behavior_actions;
import org.topcased.adele.xtext.baaction.baAction.behavior_time;
import org.topcased.adele.xtext.baaction.baAction.binary_adding_operator;
import org.topcased.adele.xtext.baaction.baAction.binary_numeric_operator;
import org.topcased.adele.xtext.baaction.baAction.boolean_literal;
import org.topcased.adele.xtext.baaction.baAction.communication_action;
import org.topcased.adele.xtext.baaction.baAction.data_component_reference;
import org.topcased.adele.xtext.baaction.baAction.decimal_integer_literal;
import org.topcased.adele.xtext.baaction.baAction.decimal_real_literal;
import org.topcased.adele.xtext.baaction.baAction.element_values;
import org.topcased.adele.xtext.baaction.baAction.exponent;
import org.topcased.adele.xtext.baaction.baAction.factor;
import org.topcased.adele.xtext.baaction.baAction.integer;
import org.topcased.adele.xtext.baaction.baAction.integer_literal;
import org.topcased.adele.xtext.baaction.baAction.integer_range;
import org.topcased.adele.xtext.baaction.baAction.integer_value;
import org.topcased.adele.xtext.baaction.baAction.logical_operator;
import org.topcased.adele.xtext.baaction.baAction.multiplying_operator;
import org.topcased.adele.xtext.baaction.baAction.name;
import org.topcased.adele.xtext.baaction.baAction.numeral;
import org.topcased.adele.xtext.baaction.baAction.numeric_literal;
import org.topcased.adele.xtext.baaction.baAction.parameter_label;
import org.topcased.adele.xtext.baaction.baAction.positive_exponent;
import org.topcased.adele.xtext.baaction.baAction.real_literal;
import org.topcased.adele.xtext.baaction.baAction.relation;
import org.topcased.adele.xtext.baaction.baAction.relational_operator;
import org.topcased.adele.xtext.baaction.baAction.simple_expression;
import org.topcased.adele.xtext.baaction.baAction.subprogram_parameter_list;
import org.topcased.adele.xtext.baaction.baAction.term;
import org.topcased.adele.xtext.baaction.baAction.timed_action;
import org.topcased.adele.xtext.baaction.baAction.unary_adding_operator;
import org.topcased.adele.xtext.baaction.baAction.unary_boolean_operator;
import org.topcased.adele.xtext.baaction.baAction.unary_numeric_operator;
import org.topcased.adele.xtext.baaction.baAction.unit_identifier;
import org.topcased.adele.xtext.baaction.baAction.value;
import org.topcased.adele.xtext.baaction.baAction.value_constant;
import org.topcased.adele.xtext.baaction.baAction.value_expression;
import org.topcased.adele.xtext.baaction.baAction.value_variable;
import org.topcased.adele.xtext.baaction.services.BaActionGrammarAccess;

@SuppressWarnings("restriction")
public class AbstractBaActionSemanticSequencer extends AbstractSemanticSequencer {

	@Inject
	protected BaActionGrammarAccess grammarAccess;
	
	@Inject
	protected ISemanticSequencerDiagnosticProvider diagnosticProvider;
	
	@Inject
	protected ITransientValueService transientValues;
	
	@Inject
	@GenericSequencer
	protected Provider<ISemanticSequencer> genericSequencerProvider;
	
	protected ISemanticSequencer genericSequencer;
	
	
	@Override
	public void init(ISemanticSequencer sequencer, ISemanticSequenceAcceptor sequenceAcceptor, Acceptor errorAcceptor) {
		super.init(sequencer, sequenceAcceptor, errorAcceptor);
		this.genericSequencer = genericSequencerProvider.get();
		this.genericSequencer.init(sequencer, sequenceAcceptor, errorAcceptor);
	}
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == BaActionPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case BaActionPackage.ARRAY_DATA_COMPONENT_REFERENCE:
				if(context == grammarAccess.getArray_data_component_referenceRule()) {
					sequence_array_data_component_reference(context, (array_data_component_reference) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.ARRAY_INDEX:
				if(context == grammarAccess.getArray_indexRule()) {
					sequence_array_index(context, (array_index) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.ASSIGNMENT_ACTION:
				if(context == grammarAccess.getAssignment_actionRule()) {
					sequence_assignment_action(context, (assignment_action) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.BASE:
				if(context == grammarAccess.getBaseRule()) {
					sequence_base(context, (base) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.BASED_INTEGER_LITERAL:
				if(context == grammarAccess.getBased_integer_literalRule()) {
					sequence_based_integer_literal(context, (based_integer_literal) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.BASED_NUMERAL:
				if(context == grammarAccess.getBased_numeralRule()) {
					sequence_based_numeral(context, (based_numeral) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.BASIC_ACTION:
				if(context == grammarAccess.getBasic_actionRule()) {
					sequence_basic_action(context, (basic_action) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.BEHAVIOR_ACTION:
				if(context == grammarAccess.getBehavior_actionRule()) {
					sequence_behavior_action(context, (behavior_action) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.BEHAVIOR_ACTION_BLOCK:
				if(context == grammarAccess.getBehavior_action_blockRule()) {
					sequence_behavior_action_block(context, (behavior_action_block) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.BEHAVIOR_ACTION_SEQUENCE:
				if(context == grammarAccess.getBehavior_action_sequenceRule()) {
					sequence_behavior_action_sequence(context, (behavior_action_sequence) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.BEHAVIOR_ACTION_SET:
				if(context == grammarAccess.getBehavior_action_setRule()) {
					sequence_behavior_action_set(context, (behavior_action_set) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.BEHAVIOR_ACTIONS:
				if(context == grammarAccess.getBehavior_actionsRule()) {
					sequence_behavior_actions(context, (behavior_actions) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.BEHAVIOR_TIME:
				if(context == grammarAccess.getBehavior_timeRule()) {
					sequence_behavior_time(context, (behavior_time) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.BINARY_ADDING_OPERATOR:
				if(context == grammarAccess.getBinary_adding_operatorRule()) {
					sequence_binary_adding_operator(context, (binary_adding_operator) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.BINARY_NUMERIC_OPERATOR:
				if(context == grammarAccess.getBinary_numeric_operatorRule()) {
					sequence_binary_numeric_operator(context, (binary_numeric_operator) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.BOOLEAN_LITERAL:
				if(context == grammarAccess.getBoolean_literalRule()) {
					sequence_boolean_literal(context, (boolean_literal) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.COMMUNICATION_ACTION:
				if(context == grammarAccess.getCommunication_actionRule()) {
					sequence_communication_action(context, (communication_action) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.DATA_COMPONENT_REFERENCE:
				if(context == grammarAccess.getData_component_referenceRule()) {
					sequence_data_component_reference(context, (data_component_reference) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.DECIMAL_INTEGER_LITERAL:
				if(context == grammarAccess.getDecimal_integer_literalRule()) {
					sequence_decimal_integer_literal(context, (decimal_integer_literal) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.DECIMAL_REAL_LITERAL:
				if(context == grammarAccess.getDecimal_real_literalRule()) {
					sequence_decimal_real_literal(context, (decimal_real_literal) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.ELEMENT_VALUES:
				if(context == grammarAccess.getElement_valuesRule()) {
					sequence_element_values(context, (element_values) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.EXPONENT:
				if(context == grammarAccess.getExponentRule()) {
					sequence_exponent(context, (exponent) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.FACTOR:
				if(context == grammarAccess.getFactorRule()) {
					sequence_factor(context, (factor) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.INTEGER:
				if(context == grammarAccess.getIntegerRule()) {
					sequence_integer(context, (integer) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.INTEGER_LITERAL:
				if(context == grammarAccess.getInteger_literalRule()) {
					sequence_integer_literal(context, (integer_literal) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.INTEGER_RANGE:
				if(context == grammarAccess.getInteger_rangeRule()) {
					sequence_integer_range(context, (integer_range) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.INTEGER_VALUE:
				if(context == grammarAccess.getInteger_valueRule()) {
					sequence_integer_value(context, (integer_value) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.LOGICAL_OPERATOR:
				if(context == grammarAccess.getLogical_operatorRule()) {
					sequence_logical_operator(context, (logical_operator) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.MULTIPLYING_OPERATOR:
				if(context == grammarAccess.getMultiplying_operatorRule()) {
					sequence_multiplying_operator(context, (multiplying_operator) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.NAME:
				if(context == grammarAccess.getNameRule()) {
					sequence_name(context, (name) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.NUMERAL:
				if(context == grammarAccess.getNumeralRule()) {
					sequence_numeral(context, (numeral) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.NUMERIC_LITERAL:
				if(context == grammarAccess.getNumeric_literalRule()) {
					sequence_numeric_literal(context, (numeric_literal) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.PARAMETER_LABEL:
				if(context == grammarAccess.getParameter_labelRule()) {
					sequence_parameter_label(context, (parameter_label) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.POSITIVE_EXPONENT:
				if(context == grammarAccess.getPositive_exponentRule()) {
					sequence_positive_exponent(context, (positive_exponent) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.REAL_LITERAL:
				if(context == grammarAccess.getReal_literalRule()) {
					sequence_real_literal(context, (real_literal) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.RELATION:
				if(context == grammarAccess.getRelationRule()) {
					sequence_relation(context, (relation) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.RELATIONAL_OPERATOR:
				if(context == grammarAccess.getRelational_operatorRule()) {
					sequence_relational_operator(context, (relational_operator) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.SIMPLE_EXPRESSION:
				if(context == grammarAccess.getSimple_expressionRule()) {
					sequence_simple_expression(context, (simple_expression) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.SUBPROGRAM_PARAMETER_LIST:
				if(context == grammarAccess.getSubprogram_parameter_listRule()) {
					sequence_subprogram_parameter_list(context, (subprogram_parameter_list) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.TERM:
				if(context == grammarAccess.getTermRule()) {
					sequence_term(context, (term) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.TIMED_ACTION:
				if(context == grammarAccess.getTimed_actionRule()) {
					sequence_timed_action(context, (timed_action) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.UNARY_ADDING_OPERATOR:
				if(context == grammarAccess.getUnary_adding_operatorRule()) {
					sequence_unary_adding_operator(context, (unary_adding_operator) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.UNARY_BOOLEAN_OPERATOR:
				if(context == grammarAccess.getUnary_boolean_operatorRule()) {
					sequence_unary_boolean_operator(context, (unary_boolean_operator) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.UNARY_NUMERIC_OPERATOR:
				if(context == grammarAccess.getUnary_numeric_operatorRule()) {
					sequence_unary_numeric_operator(context, (unary_numeric_operator) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.UNIT_IDENTIFIER:
				if(context == grammarAccess.getUnit_identifierRule()) {
					sequence_unit_identifier(context, (unit_identifier) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.VALUE:
				if(context == grammarAccess.getValueRule()) {
					sequence_value(context, (value) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.VALUE_CONSTANT:
				if(context == grammarAccess.getValue_constantRule()) {
					sequence_value_constant(context, (value_constant) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.VALUE_EXPRESSION:
				if(context == grammarAccess.getValue_expressionRule()) {
					sequence_value_expression(context, (value_expression) semanticObject); 
					return; 
				}
				else break;
			case BaActionPackage.VALUE_VARIABLE:
				if(context == grammarAccess.getValue_variableRule()) {
					sequence_value_variable(context, (value_variable) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (adcr1='(' adcr2=data_component_reference (adcr3+=',' adcr4+=data_component_reference)* adcr5=')')
	 *
	 * Features:
	 *    adcr1[1, 1]
	 *    adcr2[1, 1]
	 *    adcr3[0, *]
	 *         SAME adcr4
	 *    adcr4[0, *]
	 *         SAME adcr3
	 *    adcr5[1, 1]
	 */
	protected void sequence_array_data_component_reference(EObject context, array_data_component_reference semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (ai1='[' ai2=integer_literal ai3=']')
	 *
	 * Features:
	 *    ai1[1, 1]
	 *    ai2[1, 1]
	 *    ai3[1, 1]
	 */
	protected void sequence_array_index(EObject context, array_index semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaActionPackage.Literals.ARRAY_INDEX__AI1) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaActionPackage.Literals.ARRAY_INDEX__AI1));
			if(transientValues.isValueTransient(semanticObject, BaActionPackage.Literals.ARRAY_INDEX__AI2) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaActionPackage.Literals.ARRAY_INDEX__AI2));
			if(transientValues.isValueTransient(semanticObject, BaActionPackage.Literals.ARRAY_INDEX__AI3) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaActionPackage.Literals.ARRAY_INDEX__AI3));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getArray_indexAccess().getAi1LeftSquareBracketKeyword_0_0(), semanticObject.getAi1());
		feeder.accept(grammarAccess.getArray_indexAccess().getAi2Integer_literalParserRuleCall_1_0(), semanticObject.getAi2());
		feeder.accept(grammarAccess.getArray_indexAccess().getAi3RightSquareBracketKeyword_2_0(), semanticObject.getAi3());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (aa1=string_literal aa2=':=' (aa3=value_expression | aa4='any'))
	 *
	 * Features:
	 *    aa1[1, 1]
	 *    aa2[1, 1]
	 *    aa3[0, 1]
	 *         EXCLUDE_IF_SET aa4
	 *    aa4[0, 1]
	 *         EXCLUDE_IF_SET aa3
	 */
	protected void sequence_assignment_action(EObject context, assignment_action semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (bname1=digit bname2=digit?)
	 *
	 * Features:
	 *    bname1[1, 1]
	 *    bname2[0, 1]
	 */
	protected void sequence_base(EObject context, base semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (bilname1=base bilname2=based_numeral bilname3=positive_exponent?)
	 *
	 * Features:
	 *    bilname1[1, 1]
	 *    bilname2[1, 1]
	 *    bilname3[0, 1]
	 */
	protected void sequence_based_integer_literal(EObject context, based_integer_literal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (bnname1=extended_digit bnname2=extended_digit?)
	 *
	 * Features:
	 *    bnname1[1, 1]
	 *    bnname2[0, 1]
	 */
	protected void sequence_based_numeral(EObject context, based_numeral semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (bact1=assignment_action | bact2=communication_action | bact3=timed_action)
	 *
	 * Features:
	 *    bact1[0, 1]
	 *         EXCLUDE_IF_SET bact2
	 *         EXCLUDE_IF_SET bact3
	 *    bact2[0, 1]
	 *         EXCLUDE_IF_SET bact1
	 *         EXCLUDE_IF_SET bact3
	 *    bact3[0, 1]
	 *         EXCLUDE_IF_SET bact1
	 *         EXCLUDE_IF_SET bact2
	 */
	protected void sequence_basic_action(EObject context, basic_action semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         ba1=basic_action | 
	 *         ba2=behavior_action_block | 
	 *         (
	 *             ba3='if' 
	 *             ba3bis='(' 
	 *             ba4=value_expression 
	 *             ba5=')' 
	 *             ba6=behavior_actions 
	 *             (ba7+='elsif' ba7bis+='(' ba8+=value_expression ba9+=')' ba10+=behavior_actions)* 
	 *             (ba11='else' ba12=behavior_actions)? 
	 *             ba13='end' 
	 *             ba13bis='if'
	 *         ) | 
	 *         (
	 *             ba14='for' 
	 *             ba14bis='(' 
	 *             ba15=string_literal 
	 *             ba16=':' 
	 *             ba17=string_literal 
	 *             ba18='in' 
	 *             ba19=element_values 
	 *             ba20=')' 
	 *             ba20bis='{' 
	 *             ba21=behavior_actions 
	 *             ba22='}'
	 *         ) | 
	 *         (
	 *             ba23='forall' 
	 *             ba23bis='(' 
	 *             ba24=string_literal 
	 *             ba25=':' 
	 *             ba26=string_literal 
	 *             ba27='in' 
	 *             ba29=element_values 
	 *             ba30=')' 
	 *             ba30bis='{' 
	 *             ba31=behavior_actions 
	 *             ba32='}'
	 *         ) | 
	 *         (
	 *             ba33='while' 
	 *             ba33bis='(' 
	 *             ba34=value_expression 
	 *             ba35=')' 
	 *             ba35bis='{' 
	 *             ba36=behavior_actions 
	 *             ba37='}'
	 *         ) | 
	 *         (
	 *             ba38='do' 
	 *             ba39=behavior_actions 
	 *             ba40='until' 
	 *             ba40bis='(' 
	 *             ba41=value_expression 
	 *             ba42=')'
	 *         )
	 *     )
	 *
	 * Features:
	 *    ba1[0, 1]
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba2[0, 1]
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba3[0, 1]
	 *         EXCLUDE_IF_UNSET ba3bis
	 *         MANDATORY_IF_SET ba3bis
	 *         EXCLUDE_IF_UNSET ba4
	 *         MANDATORY_IF_SET ba4
	 *         EXCLUDE_IF_UNSET ba5
	 *         MANDATORY_IF_SET ba5
	 *         EXCLUDE_IF_UNSET ba6
	 *         MANDATORY_IF_SET ba6
	 *         MANDATORY_IF_SET ba7
	 *         MANDATORY_IF_SET ba7bis
	 *         MANDATORY_IF_SET ba8
	 *         MANDATORY_IF_SET ba9
	 *         MANDATORY_IF_SET ba10
	 *         MANDATORY_IF_SET ba11
	 *         MANDATORY_IF_SET ba12
	 *         EXCLUDE_IF_UNSET ba13
	 *         MANDATORY_IF_SET ba13
	 *         EXCLUDE_IF_UNSET ba13bis
	 *         MANDATORY_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba3bis[0, 1]
	 *         EXCLUDE_IF_UNSET ba3
	 *         MANDATORY_IF_SET ba3
	 *         EXCLUDE_IF_UNSET ba4
	 *         MANDATORY_IF_SET ba4
	 *         EXCLUDE_IF_UNSET ba5
	 *         MANDATORY_IF_SET ba5
	 *         EXCLUDE_IF_UNSET ba6
	 *         MANDATORY_IF_SET ba6
	 *         MANDATORY_IF_SET ba7
	 *         MANDATORY_IF_SET ba7bis
	 *         MANDATORY_IF_SET ba8
	 *         MANDATORY_IF_SET ba9
	 *         MANDATORY_IF_SET ba10
	 *         MANDATORY_IF_SET ba11
	 *         MANDATORY_IF_SET ba12
	 *         EXCLUDE_IF_UNSET ba13
	 *         MANDATORY_IF_SET ba13
	 *         EXCLUDE_IF_UNSET ba13bis
	 *         MANDATORY_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba4[0, 1]
	 *         EXCLUDE_IF_UNSET ba3
	 *         MANDATORY_IF_SET ba3
	 *         EXCLUDE_IF_UNSET ba3bis
	 *         MANDATORY_IF_SET ba3bis
	 *         EXCLUDE_IF_UNSET ba5
	 *         MANDATORY_IF_SET ba5
	 *         EXCLUDE_IF_UNSET ba6
	 *         MANDATORY_IF_SET ba6
	 *         MANDATORY_IF_SET ba7
	 *         MANDATORY_IF_SET ba7bis
	 *         MANDATORY_IF_SET ba8
	 *         MANDATORY_IF_SET ba9
	 *         MANDATORY_IF_SET ba10
	 *         MANDATORY_IF_SET ba11
	 *         MANDATORY_IF_SET ba12
	 *         EXCLUDE_IF_UNSET ba13
	 *         MANDATORY_IF_SET ba13
	 *         EXCLUDE_IF_UNSET ba13bis
	 *         MANDATORY_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba5[0, 1]
	 *         EXCLUDE_IF_UNSET ba3
	 *         MANDATORY_IF_SET ba3
	 *         EXCLUDE_IF_UNSET ba3bis
	 *         MANDATORY_IF_SET ba3bis
	 *         EXCLUDE_IF_UNSET ba4
	 *         MANDATORY_IF_SET ba4
	 *         EXCLUDE_IF_UNSET ba6
	 *         MANDATORY_IF_SET ba6
	 *         MANDATORY_IF_SET ba7
	 *         MANDATORY_IF_SET ba7bis
	 *         MANDATORY_IF_SET ba8
	 *         MANDATORY_IF_SET ba9
	 *         MANDATORY_IF_SET ba10
	 *         MANDATORY_IF_SET ba11
	 *         MANDATORY_IF_SET ba12
	 *         EXCLUDE_IF_UNSET ba13
	 *         MANDATORY_IF_SET ba13
	 *         EXCLUDE_IF_UNSET ba13bis
	 *         MANDATORY_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba6[0, 1]
	 *         EXCLUDE_IF_UNSET ba3
	 *         MANDATORY_IF_SET ba3
	 *         EXCLUDE_IF_UNSET ba3bis
	 *         MANDATORY_IF_SET ba3bis
	 *         EXCLUDE_IF_UNSET ba4
	 *         MANDATORY_IF_SET ba4
	 *         EXCLUDE_IF_UNSET ba5
	 *         MANDATORY_IF_SET ba5
	 *         MANDATORY_IF_SET ba7
	 *         MANDATORY_IF_SET ba7bis
	 *         MANDATORY_IF_SET ba8
	 *         MANDATORY_IF_SET ba9
	 *         MANDATORY_IF_SET ba10
	 *         MANDATORY_IF_SET ba11
	 *         MANDATORY_IF_SET ba12
	 *         EXCLUDE_IF_UNSET ba13
	 *         MANDATORY_IF_SET ba13
	 *         EXCLUDE_IF_UNSET ba13bis
	 *         MANDATORY_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba7[0, *]
	 *         SAME ba7bis
	 *         SAME ba8
	 *         SAME ba9
	 *         SAME ba10
	 *         EXCLUDE_IF_UNSET ba3
	 *         EXCLUDE_IF_UNSET ba3bis
	 *         EXCLUDE_IF_UNSET ba4
	 *         EXCLUDE_IF_UNSET ba5
	 *         EXCLUDE_IF_UNSET ba6
	 *         EXCLUDE_IF_UNSET ba13
	 *         EXCLUDE_IF_UNSET ba13bis
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba7bis[0, *]
	 *         SAME ba7
	 *         SAME ba8
	 *         SAME ba9
	 *         SAME ba10
	 *         EXCLUDE_IF_UNSET ba3
	 *         EXCLUDE_IF_UNSET ba3bis
	 *         EXCLUDE_IF_UNSET ba4
	 *         EXCLUDE_IF_UNSET ba5
	 *         EXCLUDE_IF_UNSET ba6
	 *         EXCLUDE_IF_UNSET ba13
	 *         EXCLUDE_IF_UNSET ba13bis
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba8[0, *]
	 *         SAME ba7
	 *         SAME ba7bis
	 *         SAME ba9
	 *         SAME ba10
	 *         EXCLUDE_IF_UNSET ba3
	 *         EXCLUDE_IF_UNSET ba3bis
	 *         EXCLUDE_IF_UNSET ba4
	 *         EXCLUDE_IF_UNSET ba5
	 *         EXCLUDE_IF_UNSET ba6
	 *         EXCLUDE_IF_UNSET ba13
	 *         EXCLUDE_IF_UNSET ba13bis
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba9[0, *]
	 *         SAME ba7
	 *         SAME ba7bis
	 *         SAME ba8
	 *         SAME ba10
	 *         EXCLUDE_IF_UNSET ba3
	 *         EXCLUDE_IF_UNSET ba3bis
	 *         EXCLUDE_IF_UNSET ba4
	 *         EXCLUDE_IF_UNSET ba5
	 *         EXCLUDE_IF_UNSET ba6
	 *         EXCLUDE_IF_UNSET ba13
	 *         EXCLUDE_IF_UNSET ba13bis
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba10[0, *]
	 *         SAME ba7
	 *         SAME ba7bis
	 *         SAME ba8
	 *         SAME ba9
	 *         EXCLUDE_IF_UNSET ba3
	 *         EXCLUDE_IF_UNSET ba3bis
	 *         EXCLUDE_IF_UNSET ba4
	 *         EXCLUDE_IF_UNSET ba5
	 *         EXCLUDE_IF_UNSET ba6
	 *         EXCLUDE_IF_UNSET ba13
	 *         EXCLUDE_IF_UNSET ba13bis
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba11[0, 1]
	 *         EXCLUDE_IF_UNSET ba12
	 *         MANDATORY_IF_SET ba12
	 *         EXCLUDE_IF_UNSET ba3
	 *         EXCLUDE_IF_UNSET ba3bis
	 *         EXCLUDE_IF_UNSET ba4
	 *         EXCLUDE_IF_UNSET ba5
	 *         EXCLUDE_IF_UNSET ba6
	 *         EXCLUDE_IF_UNSET ba13
	 *         EXCLUDE_IF_UNSET ba13bis
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba12[0, 1]
	 *         EXCLUDE_IF_UNSET ba11
	 *         MANDATORY_IF_SET ba11
	 *         EXCLUDE_IF_UNSET ba3
	 *         EXCLUDE_IF_UNSET ba3bis
	 *         EXCLUDE_IF_UNSET ba4
	 *         EXCLUDE_IF_UNSET ba5
	 *         EXCLUDE_IF_UNSET ba6
	 *         EXCLUDE_IF_UNSET ba13
	 *         EXCLUDE_IF_UNSET ba13bis
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba13[0, 1]
	 *         EXCLUDE_IF_UNSET ba3
	 *         MANDATORY_IF_SET ba3
	 *         EXCLUDE_IF_UNSET ba3bis
	 *         MANDATORY_IF_SET ba3bis
	 *         EXCLUDE_IF_UNSET ba4
	 *         MANDATORY_IF_SET ba4
	 *         EXCLUDE_IF_UNSET ba5
	 *         MANDATORY_IF_SET ba5
	 *         EXCLUDE_IF_UNSET ba6
	 *         MANDATORY_IF_SET ba6
	 *         MANDATORY_IF_SET ba7
	 *         MANDATORY_IF_SET ba7bis
	 *         MANDATORY_IF_SET ba8
	 *         MANDATORY_IF_SET ba9
	 *         MANDATORY_IF_SET ba10
	 *         MANDATORY_IF_SET ba11
	 *         MANDATORY_IF_SET ba12
	 *         EXCLUDE_IF_UNSET ba13bis
	 *         MANDATORY_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba13bis[0, 1]
	 *         EXCLUDE_IF_UNSET ba3
	 *         MANDATORY_IF_SET ba3
	 *         EXCLUDE_IF_UNSET ba3bis
	 *         MANDATORY_IF_SET ba3bis
	 *         EXCLUDE_IF_UNSET ba4
	 *         MANDATORY_IF_SET ba4
	 *         EXCLUDE_IF_UNSET ba5
	 *         MANDATORY_IF_SET ba5
	 *         EXCLUDE_IF_UNSET ba6
	 *         MANDATORY_IF_SET ba6
	 *         MANDATORY_IF_SET ba7
	 *         MANDATORY_IF_SET ba7bis
	 *         MANDATORY_IF_SET ba8
	 *         MANDATORY_IF_SET ba9
	 *         MANDATORY_IF_SET ba10
	 *         MANDATORY_IF_SET ba11
	 *         MANDATORY_IF_SET ba12
	 *         EXCLUDE_IF_UNSET ba13
	 *         MANDATORY_IF_SET ba13
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba14[0, 1]
	 *         EXCLUDE_IF_UNSET ba14bis
	 *         MANDATORY_IF_SET ba14bis
	 *         EXCLUDE_IF_UNSET ba15
	 *         MANDATORY_IF_SET ba15
	 *         EXCLUDE_IF_UNSET ba16
	 *         MANDATORY_IF_SET ba16
	 *         EXCLUDE_IF_UNSET ba17
	 *         MANDATORY_IF_SET ba17
	 *         EXCLUDE_IF_UNSET ba18
	 *         MANDATORY_IF_SET ba18
	 *         EXCLUDE_IF_UNSET ba19
	 *         MANDATORY_IF_SET ba19
	 *         EXCLUDE_IF_UNSET ba20
	 *         MANDATORY_IF_SET ba20
	 *         EXCLUDE_IF_UNSET ba20bis
	 *         MANDATORY_IF_SET ba20bis
	 *         EXCLUDE_IF_UNSET ba21
	 *         MANDATORY_IF_SET ba21
	 *         EXCLUDE_IF_UNSET ba22
	 *         MANDATORY_IF_SET ba22
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba14bis[0, 1]
	 *         EXCLUDE_IF_UNSET ba14
	 *         MANDATORY_IF_SET ba14
	 *         EXCLUDE_IF_UNSET ba15
	 *         MANDATORY_IF_SET ba15
	 *         EXCLUDE_IF_UNSET ba16
	 *         MANDATORY_IF_SET ba16
	 *         EXCLUDE_IF_UNSET ba17
	 *         MANDATORY_IF_SET ba17
	 *         EXCLUDE_IF_UNSET ba18
	 *         MANDATORY_IF_SET ba18
	 *         EXCLUDE_IF_UNSET ba19
	 *         MANDATORY_IF_SET ba19
	 *         EXCLUDE_IF_UNSET ba20
	 *         MANDATORY_IF_SET ba20
	 *         EXCLUDE_IF_UNSET ba20bis
	 *         MANDATORY_IF_SET ba20bis
	 *         EXCLUDE_IF_UNSET ba21
	 *         MANDATORY_IF_SET ba21
	 *         EXCLUDE_IF_UNSET ba22
	 *         MANDATORY_IF_SET ba22
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba15[0, 1]
	 *         EXCLUDE_IF_UNSET ba14
	 *         MANDATORY_IF_SET ba14
	 *         EXCLUDE_IF_UNSET ba14bis
	 *         MANDATORY_IF_SET ba14bis
	 *         EXCLUDE_IF_UNSET ba16
	 *         MANDATORY_IF_SET ba16
	 *         EXCLUDE_IF_UNSET ba17
	 *         MANDATORY_IF_SET ba17
	 *         EXCLUDE_IF_UNSET ba18
	 *         MANDATORY_IF_SET ba18
	 *         EXCLUDE_IF_UNSET ba19
	 *         MANDATORY_IF_SET ba19
	 *         EXCLUDE_IF_UNSET ba20
	 *         MANDATORY_IF_SET ba20
	 *         EXCLUDE_IF_UNSET ba20bis
	 *         MANDATORY_IF_SET ba20bis
	 *         EXCLUDE_IF_UNSET ba21
	 *         MANDATORY_IF_SET ba21
	 *         EXCLUDE_IF_UNSET ba22
	 *         MANDATORY_IF_SET ba22
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba16[0, 1]
	 *         EXCLUDE_IF_UNSET ba14
	 *         MANDATORY_IF_SET ba14
	 *         EXCLUDE_IF_UNSET ba14bis
	 *         MANDATORY_IF_SET ba14bis
	 *         EXCLUDE_IF_UNSET ba15
	 *         MANDATORY_IF_SET ba15
	 *         EXCLUDE_IF_UNSET ba17
	 *         MANDATORY_IF_SET ba17
	 *         EXCLUDE_IF_UNSET ba18
	 *         MANDATORY_IF_SET ba18
	 *         EXCLUDE_IF_UNSET ba19
	 *         MANDATORY_IF_SET ba19
	 *         EXCLUDE_IF_UNSET ba20
	 *         MANDATORY_IF_SET ba20
	 *         EXCLUDE_IF_UNSET ba20bis
	 *         MANDATORY_IF_SET ba20bis
	 *         EXCLUDE_IF_UNSET ba21
	 *         MANDATORY_IF_SET ba21
	 *         EXCLUDE_IF_UNSET ba22
	 *         MANDATORY_IF_SET ba22
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba17[0, 1]
	 *         EXCLUDE_IF_UNSET ba14
	 *         MANDATORY_IF_SET ba14
	 *         EXCLUDE_IF_UNSET ba14bis
	 *         MANDATORY_IF_SET ba14bis
	 *         EXCLUDE_IF_UNSET ba15
	 *         MANDATORY_IF_SET ba15
	 *         EXCLUDE_IF_UNSET ba16
	 *         MANDATORY_IF_SET ba16
	 *         EXCLUDE_IF_UNSET ba18
	 *         MANDATORY_IF_SET ba18
	 *         EXCLUDE_IF_UNSET ba19
	 *         MANDATORY_IF_SET ba19
	 *         EXCLUDE_IF_UNSET ba20
	 *         MANDATORY_IF_SET ba20
	 *         EXCLUDE_IF_UNSET ba20bis
	 *         MANDATORY_IF_SET ba20bis
	 *         EXCLUDE_IF_UNSET ba21
	 *         MANDATORY_IF_SET ba21
	 *         EXCLUDE_IF_UNSET ba22
	 *         MANDATORY_IF_SET ba22
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba18[0, 1]
	 *         EXCLUDE_IF_UNSET ba14
	 *         MANDATORY_IF_SET ba14
	 *         EXCLUDE_IF_UNSET ba14bis
	 *         MANDATORY_IF_SET ba14bis
	 *         EXCLUDE_IF_UNSET ba15
	 *         MANDATORY_IF_SET ba15
	 *         EXCLUDE_IF_UNSET ba16
	 *         MANDATORY_IF_SET ba16
	 *         EXCLUDE_IF_UNSET ba17
	 *         MANDATORY_IF_SET ba17
	 *         EXCLUDE_IF_UNSET ba19
	 *         MANDATORY_IF_SET ba19
	 *         EXCLUDE_IF_UNSET ba20
	 *         MANDATORY_IF_SET ba20
	 *         EXCLUDE_IF_UNSET ba20bis
	 *         MANDATORY_IF_SET ba20bis
	 *         EXCLUDE_IF_UNSET ba21
	 *         MANDATORY_IF_SET ba21
	 *         EXCLUDE_IF_UNSET ba22
	 *         MANDATORY_IF_SET ba22
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba19[0, 1]
	 *         EXCLUDE_IF_UNSET ba14
	 *         MANDATORY_IF_SET ba14
	 *         EXCLUDE_IF_UNSET ba14bis
	 *         MANDATORY_IF_SET ba14bis
	 *         EXCLUDE_IF_UNSET ba15
	 *         MANDATORY_IF_SET ba15
	 *         EXCLUDE_IF_UNSET ba16
	 *         MANDATORY_IF_SET ba16
	 *         EXCLUDE_IF_UNSET ba17
	 *         MANDATORY_IF_SET ba17
	 *         EXCLUDE_IF_UNSET ba18
	 *         MANDATORY_IF_SET ba18
	 *         EXCLUDE_IF_UNSET ba20
	 *         MANDATORY_IF_SET ba20
	 *         EXCLUDE_IF_UNSET ba20bis
	 *         MANDATORY_IF_SET ba20bis
	 *         EXCLUDE_IF_UNSET ba21
	 *         MANDATORY_IF_SET ba21
	 *         EXCLUDE_IF_UNSET ba22
	 *         MANDATORY_IF_SET ba22
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba20[0, 1]
	 *         EXCLUDE_IF_UNSET ba14
	 *         MANDATORY_IF_SET ba14
	 *         EXCLUDE_IF_UNSET ba14bis
	 *         MANDATORY_IF_SET ba14bis
	 *         EXCLUDE_IF_UNSET ba15
	 *         MANDATORY_IF_SET ba15
	 *         EXCLUDE_IF_UNSET ba16
	 *         MANDATORY_IF_SET ba16
	 *         EXCLUDE_IF_UNSET ba17
	 *         MANDATORY_IF_SET ba17
	 *         EXCLUDE_IF_UNSET ba18
	 *         MANDATORY_IF_SET ba18
	 *         EXCLUDE_IF_UNSET ba19
	 *         MANDATORY_IF_SET ba19
	 *         EXCLUDE_IF_UNSET ba20bis
	 *         MANDATORY_IF_SET ba20bis
	 *         EXCLUDE_IF_UNSET ba21
	 *         MANDATORY_IF_SET ba21
	 *         EXCLUDE_IF_UNSET ba22
	 *         MANDATORY_IF_SET ba22
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba20bis[0, 1]
	 *         EXCLUDE_IF_UNSET ba14
	 *         MANDATORY_IF_SET ba14
	 *         EXCLUDE_IF_UNSET ba14bis
	 *         MANDATORY_IF_SET ba14bis
	 *         EXCLUDE_IF_UNSET ba15
	 *         MANDATORY_IF_SET ba15
	 *         EXCLUDE_IF_UNSET ba16
	 *         MANDATORY_IF_SET ba16
	 *         EXCLUDE_IF_UNSET ba17
	 *         MANDATORY_IF_SET ba17
	 *         EXCLUDE_IF_UNSET ba18
	 *         MANDATORY_IF_SET ba18
	 *         EXCLUDE_IF_UNSET ba19
	 *         MANDATORY_IF_SET ba19
	 *         EXCLUDE_IF_UNSET ba20
	 *         MANDATORY_IF_SET ba20
	 *         EXCLUDE_IF_UNSET ba21
	 *         MANDATORY_IF_SET ba21
	 *         EXCLUDE_IF_UNSET ba22
	 *         MANDATORY_IF_SET ba22
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba21[0, 1]
	 *         EXCLUDE_IF_UNSET ba14
	 *         MANDATORY_IF_SET ba14
	 *         EXCLUDE_IF_UNSET ba14bis
	 *         MANDATORY_IF_SET ba14bis
	 *         EXCLUDE_IF_UNSET ba15
	 *         MANDATORY_IF_SET ba15
	 *         EXCLUDE_IF_UNSET ba16
	 *         MANDATORY_IF_SET ba16
	 *         EXCLUDE_IF_UNSET ba17
	 *         MANDATORY_IF_SET ba17
	 *         EXCLUDE_IF_UNSET ba18
	 *         MANDATORY_IF_SET ba18
	 *         EXCLUDE_IF_UNSET ba19
	 *         MANDATORY_IF_SET ba19
	 *         EXCLUDE_IF_UNSET ba20
	 *         MANDATORY_IF_SET ba20
	 *         EXCLUDE_IF_UNSET ba20bis
	 *         MANDATORY_IF_SET ba20bis
	 *         EXCLUDE_IF_UNSET ba22
	 *         MANDATORY_IF_SET ba22
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba22[0, 1]
	 *         EXCLUDE_IF_UNSET ba14
	 *         MANDATORY_IF_SET ba14
	 *         EXCLUDE_IF_UNSET ba14bis
	 *         MANDATORY_IF_SET ba14bis
	 *         EXCLUDE_IF_UNSET ba15
	 *         MANDATORY_IF_SET ba15
	 *         EXCLUDE_IF_UNSET ba16
	 *         MANDATORY_IF_SET ba16
	 *         EXCLUDE_IF_UNSET ba17
	 *         MANDATORY_IF_SET ba17
	 *         EXCLUDE_IF_UNSET ba18
	 *         MANDATORY_IF_SET ba18
	 *         EXCLUDE_IF_UNSET ba19
	 *         MANDATORY_IF_SET ba19
	 *         EXCLUDE_IF_UNSET ba20
	 *         MANDATORY_IF_SET ba20
	 *         EXCLUDE_IF_UNSET ba20bis
	 *         MANDATORY_IF_SET ba20bis
	 *         EXCLUDE_IF_UNSET ba21
	 *         MANDATORY_IF_SET ba21
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba23[0, 1]
	 *         EXCLUDE_IF_UNSET ba23bis
	 *         MANDATORY_IF_SET ba23bis
	 *         EXCLUDE_IF_UNSET ba24
	 *         MANDATORY_IF_SET ba24
	 *         EXCLUDE_IF_UNSET ba25
	 *         MANDATORY_IF_SET ba25
	 *         EXCLUDE_IF_UNSET ba26
	 *         MANDATORY_IF_SET ba26
	 *         EXCLUDE_IF_UNSET ba27
	 *         MANDATORY_IF_SET ba27
	 *         EXCLUDE_IF_UNSET ba29
	 *         MANDATORY_IF_SET ba29
	 *         EXCLUDE_IF_UNSET ba30
	 *         MANDATORY_IF_SET ba30
	 *         EXCLUDE_IF_UNSET ba30bis
	 *         MANDATORY_IF_SET ba30bis
	 *         EXCLUDE_IF_UNSET ba31
	 *         MANDATORY_IF_SET ba31
	 *         EXCLUDE_IF_UNSET ba32
	 *         MANDATORY_IF_SET ba32
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba23bis[0, 1]
	 *         EXCLUDE_IF_UNSET ba23
	 *         MANDATORY_IF_SET ba23
	 *         EXCLUDE_IF_UNSET ba24
	 *         MANDATORY_IF_SET ba24
	 *         EXCLUDE_IF_UNSET ba25
	 *         MANDATORY_IF_SET ba25
	 *         EXCLUDE_IF_UNSET ba26
	 *         MANDATORY_IF_SET ba26
	 *         EXCLUDE_IF_UNSET ba27
	 *         MANDATORY_IF_SET ba27
	 *         EXCLUDE_IF_UNSET ba29
	 *         MANDATORY_IF_SET ba29
	 *         EXCLUDE_IF_UNSET ba30
	 *         MANDATORY_IF_SET ba30
	 *         EXCLUDE_IF_UNSET ba30bis
	 *         MANDATORY_IF_SET ba30bis
	 *         EXCLUDE_IF_UNSET ba31
	 *         MANDATORY_IF_SET ba31
	 *         EXCLUDE_IF_UNSET ba32
	 *         MANDATORY_IF_SET ba32
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba24[0, 1]
	 *         EXCLUDE_IF_UNSET ba23
	 *         MANDATORY_IF_SET ba23
	 *         EXCLUDE_IF_UNSET ba23bis
	 *         MANDATORY_IF_SET ba23bis
	 *         EXCLUDE_IF_UNSET ba25
	 *         MANDATORY_IF_SET ba25
	 *         EXCLUDE_IF_UNSET ba26
	 *         MANDATORY_IF_SET ba26
	 *         EXCLUDE_IF_UNSET ba27
	 *         MANDATORY_IF_SET ba27
	 *         EXCLUDE_IF_UNSET ba29
	 *         MANDATORY_IF_SET ba29
	 *         EXCLUDE_IF_UNSET ba30
	 *         MANDATORY_IF_SET ba30
	 *         EXCLUDE_IF_UNSET ba30bis
	 *         MANDATORY_IF_SET ba30bis
	 *         EXCLUDE_IF_UNSET ba31
	 *         MANDATORY_IF_SET ba31
	 *         EXCLUDE_IF_UNSET ba32
	 *         MANDATORY_IF_SET ba32
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba25[0, 1]
	 *         EXCLUDE_IF_UNSET ba23
	 *         MANDATORY_IF_SET ba23
	 *         EXCLUDE_IF_UNSET ba23bis
	 *         MANDATORY_IF_SET ba23bis
	 *         EXCLUDE_IF_UNSET ba24
	 *         MANDATORY_IF_SET ba24
	 *         EXCLUDE_IF_UNSET ba26
	 *         MANDATORY_IF_SET ba26
	 *         EXCLUDE_IF_UNSET ba27
	 *         MANDATORY_IF_SET ba27
	 *         EXCLUDE_IF_UNSET ba29
	 *         MANDATORY_IF_SET ba29
	 *         EXCLUDE_IF_UNSET ba30
	 *         MANDATORY_IF_SET ba30
	 *         EXCLUDE_IF_UNSET ba30bis
	 *         MANDATORY_IF_SET ba30bis
	 *         EXCLUDE_IF_UNSET ba31
	 *         MANDATORY_IF_SET ba31
	 *         EXCLUDE_IF_UNSET ba32
	 *         MANDATORY_IF_SET ba32
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba26[0, 1]
	 *         EXCLUDE_IF_UNSET ba23
	 *         MANDATORY_IF_SET ba23
	 *         EXCLUDE_IF_UNSET ba23bis
	 *         MANDATORY_IF_SET ba23bis
	 *         EXCLUDE_IF_UNSET ba24
	 *         MANDATORY_IF_SET ba24
	 *         EXCLUDE_IF_UNSET ba25
	 *         MANDATORY_IF_SET ba25
	 *         EXCLUDE_IF_UNSET ba27
	 *         MANDATORY_IF_SET ba27
	 *         EXCLUDE_IF_UNSET ba29
	 *         MANDATORY_IF_SET ba29
	 *         EXCLUDE_IF_UNSET ba30
	 *         MANDATORY_IF_SET ba30
	 *         EXCLUDE_IF_UNSET ba30bis
	 *         MANDATORY_IF_SET ba30bis
	 *         EXCLUDE_IF_UNSET ba31
	 *         MANDATORY_IF_SET ba31
	 *         EXCLUDE_IF_UNSET ba32
	 *         MANDATORY_IF_SET ba32
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba27[0, 1]
	 *         EXCLUDE_IF_UNSET ba23
	 *         MANDATORY_IF_SET ba23
	 *         EXCLUDE_IF_UNSET ba23bis
	 *         MANDATORY_IF_SET ba23bis
	 *         EXCLUDE_IF_UNSET ba24
	 *         MANDATORY_IF_SET ba24
	 *         EXCLUDE_IF_UNSET ba25
	 *         MANDATORY_IF_SET ba25
	 *         EXCLUDE_IF_UNSET ba26
	 *         MANDATORY_IF_SET ba26
	 *         EXCLUDE_IF_UNSET ba29
	 *         MANDATORY_IF_SET ba29
	 *         EXCLUDE_IF_UNSET ba30
	 *         MANDATORY_IF_SET ba30
	 *         EXCLUDE_IF_UNSET ba30bis
	 *         MANDATORY_IF_SET ba30bis
	 *         EXCLUDE_IF_UNSET ba31
	 *         MANDATORY_IF_SET ba31
	 *         EXCLUDE_IF_UNSET ba32
	 *         MANDATORY_IF_SET ba32
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba29[0, 1]
	 *         EXCLUDE_IF_UNSET ba23
	 *         MANDATORY_IF_SET ba23
	 *         EXCLUDE_IF_UNSET ba23bis
	 *         MANDATORY_IF_SET ba23bis
	 *         EXCLUDE_IF_UNSET ba24
	 *         MANDATORY_IF_SET ba24
	 *         EXCLUDE_IF_UNSET ba25
	 *         MANDATORY_IF_SET ba25
	 *         EXCLUDE_IF_UNSET ba26
	 *         MANDATORY_IF_SET ba26
	 *         EXCLUDE_IF_UNSET ba27
	 *         MANDATORY_IF_SET ba27
	 *         EXCLUDE_IF_UNSET ba30
	 *         MANDATORY_IF_SET ba30
	 *         EXCLUDE_IF_UNSET ba30bis
	 *         MANDATORY_IF_SET ba30bis
	 *         EXCLUDE_IF_UNSET ba31
	 *         MANDATORY_IF_SET ba31
	 *         EXCLUDE_IF_UNSET ba32
	 *         MANDATORY_IF_SET ba32
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba30[0, 1]
	 *         EXCLUDE_IF_UNSET ba23
	 *         MANDATORY_IF_SET ba23
	 *         EXCLUDE_IF_UNSET ba23bis
	 *         MANDATORY_IF_SET ba23bis
	 *         EXCLUDE_IF_UNSET ba24
	 *         MANDATORY_IF_SET ba24
	 *         EXCLUDE_IF_UNSET ba25
	 *         MANDATORY_IF_SET ba25
	 *         EXCLUDE_IF_UNSET ba26
	 *         MANDATORY_IF_SET ba26
	 *         EXCLUDE_IF_UNSET ba27
	 *         MANDATORY_IF_SET ba27
	 *         EXCLUDE_IF_UNSET ba29
	 *         MANDATORY_IF_SET ba29
	 *         EXCLUDE_IF_UNSET ba30bis
	 *         MANDATORY_IF_SET ba30bis
	 *         EXCLUDE_IF_UNSET ba31
	 *         MANDATORY_IF_SET ba31
	 *         EXCLUDE_IF_UNSET ba32
	 *         MANDATORY_IF_SET ba32
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba30bis[0, 1]
	 *         EXCLUDE_IF_UNSET ba23
	 *         MANDATORY_IF_SET ba23
	 *         EXCLUDE_IF_UNSET ba23bis
	 *         MANDATORY_IF_SET ba23bis
	 *         EXCLUDE_IF_UNSET ba24
	 *         MANDATORY_IF_SET ba24
	 *         EXCLUDE_IF_UNSET ba25
	 *         MANDATORY_IF_SET ba25
	 *         EXCLUDE_IF_UNSET ba26
	 *         MANDATORY_IF_SET ba26
	 *         EXCLUDE_IF_UNSET ba27
	 *         MANDATORY_IF_SET ba27
	 *         EXCLUDE_IF_UNSET ba29
	 *         MANDATORY_IF_SET ba29
	 *         EXCLUDE_IF_UNSET ba30
	 *         MANDATORY_IF_SET ba30
	 *         EXCLUDE_IF_UNSET ba31
	 *         MANDATORY_IF_SET ba31
	 *         EXCLUDE_IF_UNSET ba32
	 *         MANDATORY_IF_SET ba32
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba31[0, 1]
	 *         EXCLUDE_IF_UNSET ba23
	 *         MANDATORY_IF_SET ba23
	 *         EXCLUDE_IF_UNSET ba23bis
	 *         MANDATORY_IF_SET ba23bis
	 *         EXCLUDE_IF_UNSET ba24
	 *         MANDATORY_IF_SET ba24
	 *         EXCLUDE_IF_UNSET ba25
	 *         MANDATORY_IF_SET ba25
	 *         EXCLUDE_IF_UNSET ba26
	 *         MANDATORY_IF_SET ba26
	 *         EXCLUDE_IF_UNSET ba27
	 *         MANDATORY_IF_SET ba27
	 *         EXCLUDE_IF_UNSET ba29
	 *         MANDATORY_IF_SET ba29
	 *         EXCLUDE_IF_UNSET ba30
	 *         MANDATORY_IF_SET ba30
	 *         EXCLUDE_IF_UNSET ba30bis
	 *         MANDATORY_IF_SET ba30bis
	 *         EXCLUDE_IF_UNSET ba32
	 *         MANDATORY_IF_SET ba32
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba32[0, 1]
	 *         EXCLUDE_IF_UNSET ba23
	 *         MANDATORY_IF_SET ba23
	 *         EXCLUDE_IF_UNSET ba23bis
	 *         MANDATORY_IF_SET ba23bis
	 *         EXCLUDE_IF_UNSET ba24
	 *         MANDATORY_IF_SET ba24
	 *         EXCLUDE_IF_UNSET ba25
	 *         MANDATORY_IF_SET ba25
	 *         EXCLUDE_IF_UNSET ba26
	 *         MANDATORY_IF_SET ba26
	 *         EXCLUDE_IF_UNSET ba27
	 *         MANDATORY_IF_SET ba27
	 *         EXCLUDE_IF_UNSET ba29
	 *         MANDATORY_IF_SET ba29
	 *         EXCLUDE_IF_UNSET ba30
	 *         MANDATORY_IF_SET ba30
	 *         EXCLUDE_IF_UNSET ba30bis
	 *         MANDATORY_IF_SET ba30bis
	 *         EXCLUDE_IF_UNSET ba31
	 *         MANDATORY_IF_SET ba31
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba33[0, 1]
	 *         EXCLUDE_IF_UNSET ba33bis
	 *         MANDATORY_IF_SET ba33bis
	 *         EXCLUDE_IF_UNSET ba34
	 *         MANDATORY_IF_SET ba34
	 *         EXCLUDE_IF_UNSET ba35
	 *         MANDATORY_IF_SET ba35
	 *         EXCLUDE_IF_UNSET ba35bis
	 *         MANDATORY_IF_SET ba35bis
	 *         EXCLUDE_IF_UNSET ba36
	 *         MANDATORY_IF_SET ba36
	 *         EXCLUDE_IF_UNSET ba37
	 *         MANDATORY_IF_SET ba37
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba33bis[0, 1]
	 *         EXCLUDE_IF_UNSET ba33
	 *         MANDATORY_IF_SET ba33
	 *         EXCLUDE_IF_UNSET ba34
	 *         MANDATORY_IF_SET ba34
	 *         EXCLUDE_IF_UNSET ba35
	 *         MANDATORY_IF_SET ba35
	 *         EXCLUDE_IF_UNSET ba35bis
	 *         MANDATORY_IF_SET ba35bis
	 *         EXCLUDE_IF_UNSET ba36
	 *         MANDATORY_IF_SET ba36
	 *         EXCLUDE_IF_UNSET ba37
	 *         MANDATORY_IF_SET ba37
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba34[0, 1]
	 *         EXCLUDE_IF_UNSET ba33
	 *         MANDATORY_IF_SET ba33
	 *         EXCLUDE_IF_UNSET ba33bis
	 *         MANDATORY_IF_SET ba33bis
	 *         EXCLUDE_IF_UNSET ba35
	 *         MANDATORY_IF_SET ba35
	 *         EXCLUDE_IF_UNSET ba35bis
	 *         MANDATORY_IF_SET ba35bis
	 *         EXCLUDE_IF_UNSET ba36
	 *         MANDATORY_IF_SET ba36
	 *         EXCLUDE_IF_UNSET ba37
	 *         MANDATORY_IF_SET ba37
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba35[0, 1]
	 *         EXCLUDE_IF_UNSET ba33
	 *         MANDATORY_IF_SET ba33
	 *         EXCLUDE_IF_UNSET ba33bis
	 *         MANDATORY_IF_SET ba33bis
	 *         EXCLUDE_IF_UNSET ba34
	 *         MANDATORY_IF_SET ba34
	 *         EXCLUDE_IF_UNSET ba35bis
	 *         MANDATORY_IF_SET ba35bis
	 *         EXCLUDE_IF_UNSET ba36
	 *         MANDATORY_IF_SET ba36
	 *         EXCLUDE_IF_UNSET ba37
	 *         MANDATORY_IF_SET ba37
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba35bis[0, 1]
	 *         EXCLUDE_IF_UNSET ba33
	 *         MANDATORY_IF_SET ba33
	 *         EXCLUDE_IF_UNSET ba33bis
	 *         MANDATORY_IF_SET ba33bis
	 *         EXCLUDE_IF_UNSET ba34
	 *         MANDATORY_IF_SET ba34
	 *         EXCLUDE_IF_UNSET ba35
	 *         MANDATORY_IF_SET ba35
	 *         EXCLUDE_IF_UNSET ba36
	 *         MANDATORY_IF_SET ba36
	 *         EXCLUDE_IF_UNSET ba37
	 *         MANDATORY_IF_SET ba37
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba36[0, 1]
	 *         EXCLUDE_IF_UNSET ba33
	 *         MANDATORY_IF_SET ba33
	 *         EXCLUDE_IF_UNSET ba33bis
	 *         MANDATORY_IF_SET ba33bis
	 *         EXCLUDE_IF_UNSET ba34
	 *         MANDATORY_IF_SET ba34
	 *         EXCLUDE_IF_UNSET ba35
	 *         MANDATORY_IF_SET ba35
	 *         EXCLUDE_IF_UNSET ba35bis
	 *         MANDATORY_IF_SET ba35bis
	 *         EXCLUDE_IF_UNSET ba37
	 *         MANDATORY_IF_SET ba37
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba37[0, 1]
	 *         EXCLUDE_IF_UNSET ba33
	 *         MANDATORY_IF_SET ba33
	 *         EXCLUDE_IF_UNSET ba33bis
	 *         MANDATORY_IF_SET ba33bis
	 *         EXCLUDE_IF_UNSET ba34
	 *         MANDATORY_IF_SET ba34
	 *         EXCLUDE_IF_UNSET ba35
	 *         MANDATORY_IF_SET ba35
	 *         EXCLUDE_IF_UNSET ba35bis
	 *         MANDATORY_IF_SET ba35bis
	 *         EXCLUDE_IF_UNSET ba36
	 *         MANDATORY_IF_SET ba36
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba38
	 *         EXCLUDE_IF_SET ba39
	 *         EXCLUDE_IF_SET ba40
	 *         EXCLUDE_IF_SET ba40bis
	 *         EXCLUDE_IF_SET ba41
	 *         EXCLUDE_IF_SET ba42
	 *    ba38[0, 1]
	 *         EXCLUDE_IF_UNSET ba39
	 *         MANDATORY_IF_SET ba39
	 *         EXCLUDE_IF_UNSET ba40
	 *         MANDATORY_IF_SET ba40
	 *         EXCLUDE_IF_UNSET ba40bis
	 *         MANDATORY_IF_SET ba40bis
	 *         EXCLUDE_IF_UNSET ba41
	 *         MANDATORY_IF_SET ba41
	 *         EXCLUDE_IF_UNSET ba42
	 *         MANDATORY_IF_SET ba42
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *    ba39[0, 1]
	 *         EXCLUDE_IF_UNSET ba38
	 *         MANDATORY_IF_SET ba38
	 *         EXCLUDE_IF_UNSET ba40
	 *         MANDATORY_IF_SET ba40
	 *         EXCLUDE_IF_UNSET ba40bis
	 *         MANDATORY_IF_SET ba40bis
	 *         EXCLUDE_IF_UNSET ba41
	 *         MANDATORY_IF_SET ba41
	 *         EXCLUDE_IF_UNSET ba42
	 *         MANDATORY_IF_SET ba42
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *    ba40[0, 1]
	 *         EXCLUDE_IF_UNSET ba38
	 *         MANDATORY_IF_SET ba38
	 *         EXCLUDE_IF_UNSET ba39
	 *         MANDATORY_IF_SET ba39
	 *         EXCLUDE_IF_UNSET ba40bis
	 *         MANDATORY_IF_SET ba40bis
	 *         EXCLUDE_IF_UNSET ba41
	 *         MANDATORY_IF_SET ba41
	 *         EXCLUDE_IF_UNSET ba42
	 *         MANDATORY_IF_SET ba42
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *    ba40bis[0, 1]
	 *         EXCLUDE_IF_UNSET ba38
	 *         MANDATORY_IF_SET ba38
	 *         EXCLUDE_IF_UNSET ba39
	 *         MANDATORY_IF_SET ba39
	 *         EXCLUDE_IF_UNSET ba40
	 *         MANDATORY_IF_SET ba40
	 *         EXCLUDE_IF_UNSET ba41
	 *         MANDATORY_IF_SET ba41
	 *         EXCLUDE_IF_UNSET ba42
	 *         MANDATORY_IF_SET ba42
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *    ba41[0, 1]
	 *         EXCLUDE_IF_UNSET ba38
	 *         MANDATORY_IF_SET ba38
	 *         EXCLUDE_IF_UNSET ba39
	 *         MANDATORY_IF_SET ba39
	 *         EXCLUDE_IF_UNSET ba40
	 *         MANDATORY_IF_SET ba40
	 *         EXCLUDE_IF_UNSET ba40bis
	 *         MANDATORY_IF_SET ba40bis
	 *         EXCLUDE_IF_UNSET ba42
	 *         MANDATORY_IF_SET ba42
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 *    ba42[0, 1]
	 *         EXCLUDE_IF_UNSET ba38
	 *         MANDATORY_IF_SET ba38
	 *         EXCLUDE_IF_UNSET ba39
	 *         MANDATORY_IF_SET ba39
	 *         EXCLUDE_IF_UNSET ba40
	 *         MANDATORY_IF_SET ba40
	 *         EXCLUDE_IF_UNSET ba40bis
	 *         MANDATORY_IF_SET ba40bis
	 *         EXCLUDE_IF_UNSET ba41
	 *         MANDATORY_IF_SET ba41
	 *         EXCLUDE_IF_SET ba1
	 *         EXCLUDE_IF_SET ba2
	 *         EXCLUDE_IF_SET ba3
	 *         EXCLUDE_IF_SET ba3bis
	 *         EXCLUDE_IF_SET ba4
	 *         EXCLUDE_IF_SET ba5
	 *         EXCLUDE_IF_SET ba6
	 *         EXCLUDE_IF_SET ba7
	 *         EXCLUDE_IF_SET ba7bis
	 *         EXCLUDE_IF_SET ba8
	 *         EXCLUDE_IF_SET ba9
	 *         EXCLUDE_IF_SET ba10
	 *         EXCLUDE_IF_SET ba11
	 *         EXCLUDE_IF_SET ba12
	 *         EXCLUDE_IF_SET ba13
	 *         EXCLUDE_IF_SET ba13bis
	 *         EXCLUDE_IF_SET ba14
	 *         EXCLUDE_IF_SET ba14bis
	 *         EXCLUDE_IF_SET ba15
	 *         EXCLUDE_IF_SET ba16
	 *         EXCLUDE_IF_SET ba17
	 *         EXCLUDE_IF_SET ba18
	 *         EXCLUDE_IF_SET ba19
	 *         EXCLUDE_IF_SET ba20
	 *         EXCLUDE_IF_SET ba20bis
	 *         EXCLUDE_IF_SET ba21
	 *         EXCLUDE_IF_SET ba22
	 *         EXCLUDE_IF_SET ba23
	 *         EXCLUDE_IF_SET ba23bis
	 *         EXCLUDE_IF_SET ba24
	 *         EXCLUDE_IF_SET ba25
	 *         EXCLUDE_IF_SET ba26
	 *         EXCLUDE_IF_SET ba27
	 *         EXCLUDE_IF_SET ba29
	 *         EXCLUDE_IF_SET ba30
	 *         EXCLUDE_IF_SET ba30bis
	 *         EXCLUDE_IF_SET ba31
	 *         EXCLUDE_IF_SET ba32
	 *         EXCLUDE_IF_SET ba33
	 *         EXCLUDE_IF_SET ba33bis
	 *         EXCLUDE_IF_SET ba34
	 *         EXCLUDE_IF_SET ba35
	 *         EXCLUDE_IF_SET ba35bis
	 *         EXCLUDE_IF_SET ba36
	 *         EXCLUDE_IF_SET ba37
	 */
	protected void sequence_behavior_action(EObject context, behavior_action semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (bac1='{' bac2=behavior_actions bac3='}' (bac4='timeout' bac5=behavior_time)?)
	 *
	 * Features:
	 *    bac1[1, 1]
	 *    bac2[1, 1]
	 *    bac3[1, 1]
	 *    bac4[0, 1]
	 *         EXCLUDE_IF_UNSET bac5
	 *         MANDATORY_IF_SET bac5
	 *    bac5[0, 1]
	 *         EXCLUDE_IF_UNSET bac4
	 *         MANDATORY_IF_SET bac4
	 */
	protected void sequence_behavior_action_block(EObject context, behavior_action_block semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (baseq1=behavior_action (baseq2+=';' baseq3+=behavior_action)+)
	 *
	 * Features:
	 *    baseq1[1, 1]
	 *    baseq2[1, *]
	 *         SAME baseq3
	 *    baseq3[1, *]
	 *         SAME baseq2
	 */
	protected void sequence_behavior_action_sequence(EObject context, behavior_action_sequence semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (baset1=behavior_action (baset2+='&' baset3+=behavior_action)+)
	 *
	 * Features:
	 *    baset1[1, 1]
	 *    baset2[1, *]
	 *         SAME baset3
	 *    baset3[1, *]
	 *         SAME baset2
	 */
	protected void sequence_behavior_action_set(EObject context, behavior_action_set semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (bas1=behavior_action ((baseq2+=';' | baseq4+='&') baseq3+=behavior_action)*)
	 *
	 * Features:
	 *    bas1[1, 1]
	 *    baseq2[0, *]
	 *         SAME_OR_LESS baseq3
	 *    baseq4[0, *]
	 *         SAME_OR_LESS baseq3
	 *    baseq3[0, *]
	 *         SAME_OR_MORE baseq2
	 *         SAME_OR_MORE baseq4
	 */
	protected void sequence_behavior_actions(EObject context, behavior_actions semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (btname1=integer_value btname2=unit_identifier)
	 *
	 * Features:
	 *    btname1[1, 1]
	 *    btname2[1, 1]
	 */
	protected void sequence_behavior_time(EObject context, behavior_time semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaActionPackage.Literals.BEHAVIOR_TIME__BTNAME1) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaActionPackage.Literals.BEHAVIOR_TIME__BTNAME1));
			if(transientValues.isValueTransient(semanticObject, BaActionPackage.Literals.BEHAVIOR_TIME__BTNAME2) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaActionPackage.Literals.BEHAVIOR_TIME__BTNAME2));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getBehavior_timeAccess().getBtname1Integer_valueParserRuleCall_1_0(), semanticObject.getBtname1());
		feeder.accept(grammarAccess.getBehavior_timeAccess().getBtname2Unit_identifierParserRuleCall_3_0(), semanticObject.getBtname2());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (baoname1='+' | baoname2='-')
	 *
	 * Features:
	 *    baoname1[0, 1]
	 *         EXCLUDE_IF_SET baoname2
	 *    baoname2[0, 1]
	 *         EXCLUDE_IF_SET baoname1
	 */
	protected void sequence_binary_adding_operator(EObject context, binary_adding_operator semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     bnoname='**'
	 *
	 * Features:
	 *    bnoname[1, 1]
	 */
	protected void sequence_binary_numeric_operator(EObject context, binary_numeric_operator semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaActionPackage.Literals.BINARY_NUMERIC_OPERATOR__BNONAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaActionPackage.Literals.BINARY_NUMERIC_OPERATOR__BNONAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getBinary_numeric_operatorAccess().getBnonameAsteriskAsteriskKeyword_0(), semanticObject.getBnoname());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (blname1='true' | blname1='false')
	 *
	 * Features:
	 *    blname1[0, 2]
	 */
	protected void sequence_boolean_literal(EObject context, boolean_literal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         (ca1=string_literal ca2='!' (ca3='(' ca4=subprogram_parameter_list ca5=')')?) | 
	 *         (ca21=string_literal ca22='>>') | 
	 *         (ca23=string_literal ca24='?' (ca25='(' ca26=string_literal ca27=')')?) | 
	 *         (ca28=string_literal ca29='!<') | 
	 *         (ca30=string_literal ca31='!>') | 
	 *         ca32='*!<' | 
	 *         ca33='*!>'
	 *     )
	 *
	 * Features:
	 *    ca1[0, 1]
	 *         EXCLUDE_IF_UNSET ca2
	 *         MANDATORY_IF_SET ca2
	 *         MANDATORY_IF_SET ca3
	 *         MANDATORY_IF_SET ca4
	 *         MANDATORY_IF_SET ca5
	 *         EXCLUDE_IF_SET ca21
	 *         EXCLUDE_IF_SET ca22
	 *         EXCLUDE_IF_SET ca23
	 *         EXCLUDE_IF_SET ca24
	 *         EXCLUDE_IF_SET ca25
	 *         EXCLUDE_IF_SET ca26
	 *         EXCLUDE_IF_SET ca27
	 *         EXCLUDE_IF_SET ca28
	 *         EXCLUDE_IF_SET ca29
	 *         EXCLUDE_IF_SET ca30
	 *         EXCLUDE_IF_SET ca31
	 *         EXCLUDE_IF_SET ca32
	 *         EXCLUDE_IF_SET ca33
	 *    ca2[0, 1]
	 *         EXCLUDE_IF_UNSET ca1
	 *         MANDATORY_IF_SET ca1
	 *         MANDATORY_IF_SET ca3
	 *         MANDATORY_IF_SET ca4
	 *         MANDATORY_IF_SET ca5
	 *         EXCLUDE_IF_SET ca21
	 *         EXCLUDE_IF_SET ca22
	 *         EXCLUDE_IF_SET ca23
	 *         EXCLUDE_IF_SET ca24
	 *         EXCLUDE_IF_SET ca25
	 *         EXCLUDE_IF_SET ca26
	 *         EXCLUDE_IF_SET ca27
	 *         EXCLUDE_IF_SET ca28
	 *         EXCLUDE_IF_SET ca29
	 *         EXCLUDE_IF_SET ca30
	 *         EXCLUDE_IF_SET ca31
	 *         EXCLUDE_IF_SET ca32
	 *         EXCLUDE_IF_SET ca33
	 *    ca3[0, 1]
	 *         EXCLUDE_IF_UNSET ca4
	 *         MANDATORY_IF_SET ca4
	 *         EXCLUDE_IF_UNSET ca5
	 *         MANDATORY_IF_SET ca5
	 *         EXCLUDE_IF_UNSET ca1
	 *         EXCLUDE_IF_UNSET ca2
	 *         EXCLUDE_IF_SET ca21
	 *         EXCLUDE_IF_SET ca22
	 *         EXCLUDE_IF_SET ca23
	 *         EXCLUDE_IF_SET ca24
	 *         EXCLUDE_IF_SET ca25
	 *         EXCLUDE_IF_SET ca26
	 *         EXCLUDE_IF_SET ca27
	 *         EXCLUDE_IF_SET ca28
	 *         EXCLUDE_IF_SET ca29
	 *         EXCLUDE_IF_SET ca30
	 *         EXCLUDE_IF_SET ca31
	 *         EXCLUDE_IF_SET ca32
	 *         EXCLUDE_IF_SET ca33
	 *    ca4[0, 1]
	 *         EXCLUDE_IF_UNSET ca3
	 *         MANDATORY_IF_SET ca3
	 *         EXCLUDE_IF_UNSET ca5
	 *         MANDATORY_IF_SET ca5
	 *         EXCLUDE_IF_UNSET ca1
	 *         EXCLUDE_IF_UNSET ca2
	 *         EXCLUDE_IF_SET ca21
	 *         EXCLUDE_IF_SET ca22
	 *         EXCLUDE_IF_SET ca23
	 *         EXCLUDE_IF_SET ca24
	 *         EXCLUDE_IF_SET ca25
	 *         EXCLUDE_IF_SET ca26
	 *         EXCLUDE_IF_SET ca27
	 *         EXCLUDE_IF_SET ca28
	 *         EXCLUDE_IF_SET ca29
	 *         EXCLUDE_IF_SET ca30
	 *         EXCLUDE_IF_SET ca31
	 *         EXCLUDE_IF_SET ca32
	 *         EXCLUDE_IF_SET ca33
	 *    ca5[0, 1]
	 *         EXCLUDE_IF_UNSET ca3
	 *         MANDATORY_IF_SET ca3
	 *         EXCLUDE_IF_UNSET ca4
	 *         MANDATORY_IF_SET ca4
	 *         EXCLUDE_IF_UNSET ca1
	 *         EXCLUDE_IF_UNSET ca2
	 *         EXCLUDE_IF_SET ca21
	 *         EXCLUDE_IF_SET ca22
	 *         EXCLUDE_IF_SET ca23
	 *         EXCLUDE_IF_SET ca24
	 *         EXCLUDE_IF_SET ca25
	 *         EXCLUDE_IF_SET ca26
	 *         EXCLUDE_IF_SET ca27
	 *         EXCLUDE_IF_SET ca28
	 *         EXCLUDE_IF_SET ca29
	 *         EXCLUDE_IF_SET ca30
	 *         EXCLUDE_IF_SET ca31
	 *         EXCLUDE_IF_SET ca32
	 *         EXCLUDE_IF_SET ca33
	 *    ca21[0, 1]
	 *         EXCLUDE_IF_UNSET ca22
	 *         MANDATORY_IF_SET ca22
	 *         EXCLUDE_IF_SET ca1
	 *         EXCLUDE_IF_SET ca2
	 *         EXCLUDE_IF_SET ca3
	 *         EXCLUDE_IF_SET ca4
	 *         EXCLUDE_IF_SET ca5
	 *         EXCLUDE_IF_SET ca23
	 *         EXCLUDE_IF_SET ca24
	 *         EXCLUDE_IF_SET ca25
	 *         EXCLUDE_IF_SET ca26
	 *         EXCLUDE_IF_SET ca27
	 *         EXCLUDE_IF_SET ca28
	 *         EXCLUDE_IF_SET ca29
	 *         EXCLUDE_IF_SET ca30
	 *         EXCLUDE_IF_SET ca31
	 *         EXCLUDE_IF_SET ca32
	 *         EXCLUDE_IF_SET ca33
	 *    ca22[0, 1]
	 *         EXCLUDE_IF_UNSET ca21
	 *         MANDATORY_IF_SET ca21
	 *         EXCLUDE_IF_SET ca1
	 *         EXCLUDE_IF_SET ca2
	 *         EXCLUDE_IF_SET ca3
	 *         EXCLUDE_IF_SET ca4
	 *         EXCLUDE_IF_SET ca5
	 *         EXCLUDE_IF_SET ca23
	 *         EXCLUDE_IF_SET ca24
	 *         EXCLUDE_IF_SET ca25
	 *         EXCLUDE_IF_SET ca26
	 *         EXCLUDE_IF_SET ca27
	 *         EXCLUDE_IF_SET ca28
	 *         EXCLUDE_IF_SET ca29
	 *         EXCLUDE_IF_SET ca30
	 *         EXCLUDE_IF_SET ca31
	 *         EXCLUDE_IF_SET ca32
	 *         EXCLUDE_IF_SET ca33
	 *    ca23[0, 1]
	 *         EXCLUDE_IF_UNSET ca24
	 *         MANDATORY_IF_SET ca24
	 *         MANDATORY_IF_SET ca25
	 *         MANDATORY_IF_SET ca26
	 *         MANDATORY_IF_SET ca27
	 *         EXCLUDE_IF_SET ca1
	 *         EXCLUDE_IF_SET ca2
	 *         EXCLUDE_IF_SET ca3
	 *         EXCLUDE_IF_SET ca4
	 *         EXCLUDE_IF_SET ca5
	 *         EXCLUDE_IF_SET ca21
	 *         EXCLUDE_IF_SET ca22
	 *         EXCLUDE_IF_SET ca28
	 *         EXCLUDE_IF_SET ca29
	 *         EXCLUDE_IF_SET ca30
	 *         EXCLUDE_IF_SET ca31
	 *         EXCLUDE_IF_SET ca32
	 *         EXCLUDE_IF_SET ca33
	 *    ca24[0, 1]
	 *         EXCLUDE_IF_UNSET ca23
	 *         MANDATORY_IF_SET ca23
	 *         MANDATORY_IF_SET ca25
	 *         MANDATORY_IF_SET ca26
	 *         MANDATORY_IF_SET ca27
	 *         EXCLUDE_IF_SET ca1
	 *         EXCLUDE_IF_SET ca2
	 *         EXCLUDE_IF_SET ca3
	 *         EXCLUDE_IF_SET ca4
	 *         EXCLUDE_IF_SET ca5
	 *         EXCLUDE_IF_SET ca21
	 *         EXCLUDE_IF_SET ca22
	 *         EXCLUDE_IF_SET ca28
	 *         EXCLUDE_IF_SET ca29
	 *         EXCLUDE_IF_SET ca30
	 *         EXCLUDE_IF_SET ca31
	 *         EXCLUDE_IF_SET ca32
	 *         EXCLUDE_IF_SET ca33
	 *    ca25[0, 1]
	 *         EXCLUDE_IF_UNSET ca26
	 *         MANDATORY_IF_SET ca26
	 *         EXCLUDE_IF_UNSET ca27
	 *         MANDATORY_IF_SET ca27
	 *         EXCLUDE_IF_UNSET ca23
	 *         EXCLUDE_IF_UNSET ca24
	 *         EXCLUDE_IF_SET ca1
	 *         EXCLUDE_IF_SET ca2
	 *         EXCLUDE_IF_SET ca3
	 *         EXCLUDE_IF_SET ca4
	 *         EXCLUDE_IF_SET ca5
	 *         EXCLUDE_IF_SET ca21
	 *         EXCLUDE_IF_SET ca22
	 *         EXCLUDE_IF_SET ca28
	 *         EXCLUDE_IF_SET ca29
	 *         EXCLUDE_IF_SET ca30
	 *         EXCLUDE_IF_SET ca31
	 *         EXCLUDE_IF_SET ca32
	 *         EXCLUDE_IF_SET ca33
	 *    ca26[0, 1]
	 *         EXCLUDE_IF_UNSET ca25
	 *         MANDATORY_IF_SET ca25
	 *         EXCLUDE_IF_UNSET ca27
	 *         MANDATORY_IF_SET ca27
	 *         EXCLUDE_IF_UNSET ca23
	 *         EXCLUDE_IF_UNSET ca24
	 *         EXCLUDE_IF_SET ca1
	 *         EXCLUDE_IF_SET ca2
	 *         EXCLUDE_IF_SET ca3
	 *         EXCLUDE_IF_SET ca4
	 *         EXCLUDE_IF_SET ca5
	 *         EXCLUDE_IF_SET ca21
	 *         EXCLUDE_IF_SET ca22
	 *         EXCLUDE_IF_SET ca28
	 *         EXCLUDE_IF_SET ca29
	 *         EXCLUDE_IF_SET ca30
	 *         EXCLUDE_IF_SET ca31
	 *         EXCLUDE_IF_SET ca32
	 *         EXCLUDE_IF_SET ca33
	 *    ca27[0, 1]
	 *         EXCLUDE_IF_UNSET ca25
	 *         MANDATORY_IF_SET ca25
	 *         EXCLUDE_IF_UNSET ca26
	 *         MANDATORY_IF_SET ca26
	 *         EXCLUDE_IF_UNSET ca23
	 *         EXCLUDE_IF_UNSET ca24
	 *         EXCLUDE_IF_SET ca1
	 *         EXCLUDE_IF_SET ca2
	 *         EXCLUDE_IF_SET ca3
	 *         EXCLUDE_IF_SET ca4
	 *         EXCLUDE_IF_SET ca5
	 *         EXCLUDE_IF_SET ca21
	 *         EXCLUDE_IF_SET ca22
	 *         EXCLUDE_IF_SET ca28
	 *         EXCLUDE_IF_SET ca29
	 *         EXCLUDE_IF_SET ca30
	 *         EXCLUDE_IF_SET ca31
	 *         EXCLUDE_IF_SET ca32
	 *         EXCLUDE_IF_SET ca33
	 *    ca28[0, 1]
	 *         EXCLUDE_IF_UNSET ca29
	 *         MANDATORY_IF_SET ca29
	 *         EXCLUDE_IF_SET ca1
	 *         EXCLUDE_IF_SET ca2
	 *         EXCLUDE_IF_SET ca3
	 *         EXCLUDE_IF_SET ca4
	 *         EXCLUDE_IF_SET ca5
	 *         EXCLUDE_IF_SET ca21
	 *         EXCLUDE_IF_SET ca22
	 *         EXCLUDE_IF_SET ca23
	 *         EXCLUDE_IF_SET ca24
	 *         EXCLUDE_IF_SET ca25
	 *         EXCLUDE_IF_SET ca26
	 *         EXCLUDE_IF_SET ca27
	 *         EXCLUDE_IF_SET ca30
	 *         EXCLUDE_IF_SET ca31
	 *         EXCLUDE_IF_SET ca32
	 *         EXCLUDE_IF_SET ca33
	 *    ca29[0, 1]
	 *         EXCLUDE_IF_UNSET ca28
	 *         MANDATORY_IF_SET ca28
	 *         EXCLUDE_IF_SET ca1
	 *         EXCLUDE_IF_SET ca2
	 *         EXCLUDE_IF_SET ca3
	 *         EXCLUDE_IF_SET ca4
	 *         EXCLUDE_IF_SET ca5
	 *         EXCLUDE_IF_SET ca21
	 *         EXCLUDE_IF_SET ca22
	 *         EXCLUDE_IF_SET ca23
	 *         EXCLUDE_IF_SET ca24
	 *         EXCLUDE_IF_SET ca25
	 *         EXCLUDE_IF_SET ca26
	 *         EXCLUDE_IF_SET ca27
	 *         EXCLUDE_IF_SET ca30
	 *         EXCLUDE_IF_SET ca31
	 *         EXCLUDE_IF_SET ca32
	 *         EXCLUDE_IF_SET ca33
	 *    ca30[0, 1]
	 *         EXCLUDE_IF_UNSET ca31
	 *         MANDATORY_IF_SET ca31
	 *         EXCLUDE_IF_SET ca1
	 *         EXCLUDE_IF_SET ca2
	 *         EXCLUDE_IF_SET ca3
	 *         EXCLUDE_IF_SET ca4
	 *         EXCLUDE_IF_SET ca5
	 *         EXCLUDE_IF_SET ca21
	 *         EXCLUDE_IF_SET ca22
	 *         EXCLUDE_IF_SET ca23
	 *         EXCLUDE_IF_SET ca24
	 *         EXCLUDE_IF_SET ca25
	 *         EXCLUDE_IF_SET ca26
	 *         EXCLUDE_IF_SET ca27
	 *         EXCLUDE_IF_SET ca28
	 *         EXCLUDE_IF_SET ca29
	 *         EXCLUDE_IF_SET ca32
	 *         EXCLUDE_IF_SET ca33
	 *    ca31[0, 1]
	 *         EXCLUDE_IF_UNSET ca30
	 *         MANDATORY_IF_SET ca30
	 *         EXCLUDE_IF_SET ca1
	 *         EXCLUDE_IF_SET ca2
	 *         EXCLUDE_IF_SET ca3
	 *         EXCLUDE_IF_SET ca4
	 *         EXCLUDE_IF_SET ca5
	 *         EXCLUDE_IF_SET ca21
	 *         EXCLUDE_IF_SET ca22
	 *         EXCLUDE_IF_SET ca23
	 *         EXCLUDE_IF_SET ca24
	 *         EXCLUDE_IF_SET ca25
	 *         EXCLUDE_IF_SET ca26
	 *         EXCLUDE_IF_SET ca27
	 *         EXCLUDE_IF_SET ca28
	 *         EXCLUDE_IF_SET ca29
	 *         EXCLUDE_IF_SET ca32
	 *         EXCLUDE_IF_SET ca33
	 *    ca32[0, 1]
	 *         EXCLUDE_IF_SET ca1
	 *         EXCLUDE_IF_SET ca2
	 *         EXCLUDE_IF_SET ca3
	 *         EXCLUDE_IF_SET ca4
	 *         EXCLUDE_IF_SET ca5
	 *         EXCLUDE_IF_SET ca21
	 *         EXCLUDE_IF_SET ca22
	 *         EXCLUDE_IF_SET ca23
	 *         EXCLUDE_IF_SET ca24
	 *         EXCLUDE_IF_SET ca25
	 *         EXCLUDE_IF_SET ca26
	 *         EXCLUDE_IF_SET ca27
	 *         EXCLUDE_IF_SET ca28
	 *         EXCLUDE_IF_SET ca29
	 *         EXCLUDE_IF_SET ca30
	 *         EXCLUDE_IF_SET ca31
	 *         EXCLUDE_IF_SET ca33
	 *    ca33[0, 1]
	 *         EXCLUDE_IF_SET ca1
	 *         EXCLUDE_IF_SET ca2
	 *         EXCLUDE_IF_SET ca3
	 *         EXCLUDE_IF_SET ca4
	 *         EXCLUDE_IF_SET ca5
	 *         EXCLUDE_IF_SET ca21
	 *         EXCLUDE_IF_SET ca22
	 *         EXCLUDE_IF_SET ca23
	 *         EXCLUDE_IF_SET ca24
	 *         EXCLUDE_IF_SET ca25
	 *         EXCLUDE_IF_SET ca26
	 *         EXCLUDE_IF_SET ca27
	 *         EXCLUDE_IF_SET ca28
	 *         EXCLUDE_IF_SET ca29
	 *         EXCLUDE_IF_SET ca30
	 *         EXCLUDE_IF_SET ca31
	 *         EXCLUDE_IF_SET ca32
	 */
	protected void sequence_communication_action(EObject context, communication_action semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (dcr1=string_literal (dcr2+='.' dcr3+=string_literal)*)
	 *
	 * Features:
	 *    dcr1[1, 1]
	 *    dcr2[0, *]
	 *         SAME dcr3
	 *    dcr3[0, *]
	 *         SAME dcr2
	 */
	protected void sequence_data_component_reference(EObject context, data_component_reference semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (dilname1=numeral dilname2=positive_exponent?)
	 *
	 * Features:
	 *    dilname1[1, 1]
	 *    dilname2[0, 1]
	 */
	protected void sequence_decimal_integer_literal(EObject context, decimal_integer_literal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (drlname1=numeral drlname2=numeral drlname3=exponent?)
	 *
	 * Features:
	 *    drlname1[1, 1]
	 *    drlname2[1, 1]
	 *    drlname3[0, 1]
	 */
	protected void sequence_decimal_real_literal(EObject context, decimal_real_literal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (ev1=integer_range | ev2=string_literal | ev3=array_data_component_reference)
	 *
	 * Features:
	 *    ev1[0, 1]
	 *         EXCLUDE_IF_SET ev2
	 *         EXCLUDE_IF_SET ev3
	 *    ev2[0, 1]
	 *         EXCLUDE_IF_SET ev1
	 *         EXCLUDE_IF_SET ev3
	 *    ev3[0, 1]
	 *         EXCLUDE_IF_SET ev1
	 *         EXCLUDE_IF_SET ev2
	 */
	protected void sequence_element_values(EObject context, element_values semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (ename1=numeral | ename2=numeral)
	 *
	 * Features:
	 *    ename1[0, 1]
	 *         EXCLUDE_IF_SET ename2
	 *    ename2[0, 1]
	 *         EXCLUDE_IF_SET ename1
	 */
	protected void sequence_exponent(EObject context, exponent semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         (fname1=value (fname3+=binary_numeric_operator fname2=value)?) | 
	 *         (fname4=unary_numeric_operator fname5=value) | 
	 *         (fname6=unary_boolean_operator fname7=value)
	 *     )
	 *
	 * Features:
	 *    fname1[0, 1]
	 *         MANDATORY_IF_SET fname3
	 *         MANDATORY_IF_SET fname2
	 *         EXCLUDE_IF_SET fname4
	 *         EXCLUDE_IF_SET fname5
	 *         EXCLUDE_IF_SET fname6
	 *         EXCLUDE_IF_SET fname7
	 *    fname3[0, 1]
	 *         EXCLUDE_IF_UNSET fname2
	 *         MANDATORY_IF_SET fname2
	 *         EXCLUDE_IF_UNSET fname1
	 *         EXCLUDE_IF_SET fname4
	 *         EXCLUDE_IF_SET fname5
	 *         EXCLUDE_IF_SET fname6
	 *         EXCLUDE_IF_SET fname7
	 *    fname2[0, 1]
	 *         EXCLUDE_IF_UNSET fname3
	 *         MANDATORY_IF_SET fname3
	 *         EXCLUDE_IF_UNSET fname1
	 *         EXCLUDE_IF_SET fname4
	 *         EXCLUDE_IF_SET fname5
	 *         EXCLUDE_IF_SET fname6
	 *         EXCLUDE_IF_SET fname7
	 *    fname4[0, 1]
	 *         EXCLUDE_IF_UNSET fname5
	 *         MANDATORY_IF_SET fname5
	 *         EXCLUDE_IF_SET fname1
	 *         EXCLUDE_IF_SET fname3
	 *         EXCLUDE_IF_SET fname2
	 *         EXCLUDE_IF_SET fname6
	 *         EXCLUDE_IF_SET fname7
	 *    fname5[0, 1]
	 *         EXCLUDE_IF_UNSET fname4
	 *         MANDATORY_IF_SET fname4
	 *         EXCLUDE_IF_SET fname1
	 *         EXCLUDE_IF_SET fname3
	 *         EXCLUDE_IF_SET fname2
	 *         EXCLUDE_IF_SET fname6
	 *         EXCLUDE_IF_SET fname7
	 *    fname6[0, 1]
	 *         EXCLUDE_IF_UNSET fname7
	 *         MANDATORY_IF_SET fname7
	 *         EXCLUDE_IF_SET fname1
	 *         EXCLUDE_IF_SET fname3
	 *         EXCLUDE_IF_SET fname2
	 *         EXCLUDE_IF_SET fname4
	 *         EXCLUDE_IF_SET fname5
	 *    fname7[0, 1]
	 *         EXCLUDE_IF_UNSET fname6
	 *         MANDATORY_IF_SET fname6
	 *         EXCLUDE_IF_SET fname1
	 *         EXCLUDE_IF_SET fname3
	 *         EXCLUDE_IF_SET fname2
	 *         EXCLUDE_IF_SET fname4
	 *         EXCLUDE_IF_SET fname5
	 */
	protected void sequence_factor(EObject context, factor semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (value1=INT | value2=digit)
	 *
	 * Features:
	 *    value1[0, 1]
	 *         EXCLUDE_IF_SET value2
	 *    value2[0, 1]
	 *         EXCLUDE_IF_SET value1
	 */
	protected void sequence_integer(EObject context, integer semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (ilname1=decimal_integer_literal | ilname2=based_integer_literal)
	 *
	 * Features:
	 *    ilname1[0, 1]
	 *         EXCLUDE_IF_SET ilname2
	 *    ilname2[0, 1]
	 *         EXCLUDE_IF_SET ilname1
	 */
	protected void sequence_integer_literal(EObject context, integer_literal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (min=integer_value irsep='..' max=integer_value)
	 *
	 * Features:
	 *    min[1, 1]
	 *    irsep[1, 1]
	 *    max[1, 1]
	 */
	protected void sequence_integer_range(EObject context, integer_range semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaActionPackage.Literals.INTEGER_RANGE__MIN) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaActionPackage.Literals.INTEGER_RANGE__MIN));
			if(transientValues.isValueTransient(semanticObject, BaActionPackage.Literals.INTEGER_RANGE__IRSEP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaActionPackage.Literals.INTEGER_RANGE__IRSEP));
			if(transientValues.isValueTransient(semanticObject, BaActionPackage.Literals.INTEGER_RANGE__MAX) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaActionPackage.Literals.INTEGER_RANGE__MAX));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getInteger_rangeAccess().getMinInteger_valueParserRuleCall_1_0(), semanticObject.getMin());
		feeder.accept(grammarAccess.getInteger_rangeAccess().getIrsepFullStopFullStopKeyword_3_0(), semanticObject.getIrsep());
		feeder.accept(grammarAccess.getInteger_rangeAccess().getMaxInteger_valueParserRuleCall_5_0(), semanticObject.getMax());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     ivname=numeric_literal
	 *
	 * Features:
	 *    ivname[1, 1]
	 */
	protected void sequence_integer_value(EObject context, integer_value semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaActionPackage.Literals.INTEGER_VALUE__IVNAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaActionPackage.Literals.INTEGER_VALUE__IVNAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getInteger_valueAccess().getIvnameNumeric_literalParserRuleCall_0(), semanticObject.getIvname());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (loname1='and' | loname2='or' | loname3='xor')
	 *
	 * Features:
	 *    loname1[0, 1]
	 *         EXCLUDE_IF_SET loname2
	 *         EXCLUDE_IF_SET loname3
	 *    loname2[0, 1]
	 *         EXCLUDE_IF_SET loname1
	 *         EXCLUDE_IF_SET loname3
	 *    loname3[0, 1]
	 *         EXCLUDE_IF_SET loname1
	 *         EXCLUDE_IF_SET loname2
	 */
	protected void sequence_logical_operator(EObject context, logical_operator semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (moname1='*' | moname2='/' | moname3='mod' | moname4='rem')
	 *
	 * Features:
	 *    moname1[0, 1]
	 *         EXCLUDE_IF_SET moname2
	 *         EXCLUDE_IF_SET moname3
	 *         EXCLUDE_IF_SET moname4
	 *    moname2[0, 1]
	 *         EXCLUDE_IF_SET moname1
	 *         EXCLUDE_IF_SET moname3
	 *         EXCLUDE_IF_SET moname4
	 *    moname3[0, 1]
	 *         EXCLUDE_IF_SET moname1
	 *         EXCLUDE_IF_SET moname2
	 *         EXCLUDE_IF_SET moname4
	 *    moname4[0, 1]
	 *         EXCLUDE_IF_SET moname1
	 *         EXCLUDE_IF_SET moname2
	 *         EXCLUDE_IF_SET moname3
	 */
	protected void sequence_multiplying_operator(EObject context, multiplying_operator semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (n1=string_literal n2+=array_index*)
	 *
	 * Features:
	 *    n1[1, 1]
	 *    n2[0, *]
	 */
	protected void sequence_name(EObject context, name semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (nname1=integer nname2+=integer*)
	 *
	 * Features:
	 *    nname1[1, 1]
	 *    nname2[0, *]
	 */
	protected void sequence_numeral(EObject context, numeral semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (nlname1=integer_literal | nlname2=real_literal)
	 *
	 * Features:
	 *    nlname1[0, 1]
	 *         EXCLUDE_IF_SET nlname2
	 *    nlname2[0, 1]
	 *         EXCLUDE_IF_SET nlname1
	 */
	protected void sequence_numeric_literal(EObject context, numeric_literal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     pl1=value_expression
	 *
	 * Features:
	 *    pl1[1, 1]
	 */
	protected void sequence_parameter_label(EObject context, parameter_label semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaActionPackage.Literals.PARAMETER_LABEL__PL1) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaActionPackage.Literals.PARAMETER_LABEL__PL1));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getParameter_labelAccess().getPl1Value_expressionParserRuleCall_0(), semanticObject.getPl1());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     pename=numeral
	 *
	 * Features:
	 *    pename[1, 1]
	 */
	protected void sequence_positive_exponent(EObject context, positive_exponent semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaActionPackage.Literals.POSITIVE_EXPONENT__PENAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaActionPackage.Literals.POSITIVE_EXPONENT__PENAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getPositive_exponentAccess().getPenameNumeralParserRuleCall_2_0(), semanticObject.getPename());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     rlname=decimal_real_literal
	 *
	 * Features:
	 *    rlname[1, 1]
	 */
	protected void sequence_real_literal(EObject context, real_literal semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaActionPackage.Literals.REAL_LITERAL__RLNAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaActionPackage.Literals.REAL_LITERAL__RLNAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getReal_literalAccess().getRlnameDecimal_real_literalParserRuleCall_0(), semanticObject.getRlname());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (rname1=simple_expression (rname3+=relational_operator rname2+=simple_expression)?)
	 *
	 * Features:
	 *    rname1[1, 1]
	 *    rname3[0, 1]
	 *         EXCLUDE_IF_UNSET rname2
	 *         MANDATORY_IF_SET rname2
	 *    rname2[0, 1]
	 *         EXCLUDE_IF_UNSET rname3
	 *         MANDATORY_IF_SET rname3
	 */
	protected void sequence_relation(EObject context, relation semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         roname1='=' | 
	 *         roname2='!=' | 
	 *         roname3='<' | 
	 *         roname4='<=' | 
	 *         roname5='>' | 
	 *         roname6='>='
	 *     )
	 *
	 * Features:
	 *    roname1[0, 1]
	 *         EXCLUDE_IF_SET roname2
	 *         EXCLUDE_IF_SET roname3
	 *         EXCLUDE_IF_SET roname4
	 *         EXCLUDE_IF_SET roname5
	 *         EXCLUDE_IF_SET roname6
	 *    roname2[0, 1]
	 *         EXCLUDE_IF_SET roname1
	 *         EXCLUDE_IF_SET roname3
	 *         EXCLUDE_IF_SET roname4
	 *         EXCLUDE_IF_SET roname5
	 *         EXCLUDE_IF_SET roname6
	 *    roname3[0, 1]
	 *         EXCLUDE_IF_SET roname1
	 *         EXCLUDE_IF_SET roname2
	 *         EXCLUDE_IF_SET roname4
	 *         EXCLUDE_IF_SET roname5
	 *         EXCLUDE_IF_SET roname6
	 *    roname4[0, 1]
	 *         EXCLUDE_IF_SET roname1
	 *         EXCLUDE_IF_SET roname2
	 *         EXCLUDE_IF_SET roname3
	 *         EXCLUDE_IF_SET roname5
	 *         EXCLUDE_IF_SET roname6
	 *    roname5[0, 1]
	 *         EXCLUDE_IF_SET roname1
	 *         EXCLUDE_IF_SET roname2
	 *         EXCLUDE_IF_SET roname3
	 *         EXCLUDE_IF_SET roname4
	 *         EXCLUDE_IF_SET roname6
	 *    roname6[0, 1]
	 *         EXCLUDE_IF_SET roname1
	 *         EXCLUDE_IF_SET roname2
	 *         EXCLUDE_IF_SET roname3
	 *         EXCLUDE_IF_SET roname4
	 *         EXCLUDE_IF_SET roname5
	 */
	protected void sequence_relational_operator(EObject context, relational_operator semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (sename2=unary_adding_operator? sename1=term (sename3+=binary_adding_operator sename4+=term)*)
	 *
	 * Features:
	 *    sename2[0, 1]
	 *    sename1[1, 1]
	 *    sename3[0, *]
	 *         SAME sename4
	 *    sename4[0, *]
	 *         SAME sename3
	 */
	protected void sequence_simple_expression(EObject context, simple_expression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (spl1=parameter_label (spl2+=',' spl3+=parameter_label)*)
	 *
	 * Features:
	 *    spl1[1, 1]
	 *    spl2[0, *]
	 *         SAME spl3
	 *    spl3[0, *]
	 *         SAME spl2
	 */
	protected void sequence_subprogram_parameter_list(EObject context, subprogram_parameter_list semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (tname1=factor (tname3+=multiplying_operator tname2+=factor)*)
	 *
	 * Features:
	 *    tname1[1, 1]
	 *    tname3[0, *]
	 *         SAME tname2
	 *    tname2[0, *]
	 *         SAME tname3
	 */
	protected void sequence_term(EObject context, term semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (ta1='computation' ta1bis='(' ta2=behavior_time (ta3='..' ta4=behavior_time)? ta5=')')
	 *
	 * Features:
	 *    ta1[1, 1]
	 *    ta1bis[1, 1]
	 *    ta2[1, 1]
	 *    ta3[0, 1]
	 *         EXCLUDE_IF_UNSET ta4
	 *         MANDATORY_IF_SET ta4
	 *    ta4[0, 1]
	 *         EXCLUDE_IF_UNSET ta3
	 *         MANDATORY_IF_SET ta3
	 *    ta5[1, 1]
	 */
	protected void sequence_timed_action(EObject context, timed_action semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (uaoname1='+' | uaoname2='-')
	 *
	 * Features:
	 *    uaoname1[0, 1]
	 *         EXCLUDE_IF_SET uaoname2
	 *    uaoname2[0, 1]
	 *         EXCLUDE_IF_SET uaoname1
	 */
	protected void sequence_unary_adding_operator(EObject context, unary_adding_operator semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     uboname='not'
	 *
	 * Features:
	 *    uboname[1, 1]
	 */
	protected void sequence_unary_boolean_operator(EObject context, unary_boolean_operator semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaActionPackage.Literals.UNARY_BOOLEAN_OPERATOR__UBONAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaActionPackage.Literals.UNARY_BOOLEAN_OPERATOR__UBONAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getUnary_boolean_operatorAccess().getUbonameNotKeyword_0(), semanticObject.getUboname());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     unoname='abs'
	 *
	 * Features:
	 *    unoname[1, 1]
	 */
	protected void sequence_unary_numeric_operator(EObject context, unary_numeric_operator semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, BaActionPackage.Literals.UNARY_NUMERIC_OPERATOR__UNONAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, BaActionPackage.Literals.UNARY_NUMERIC_OPERATOR__UNONAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getUnary_numeric_operatorAccess().getUnonameAbsKeyword_0(), semanticObject.getUnoname());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         uiname1='ps' | 
	 *         uiname2='ns' | 
	 *         uiname3='us' | 
	 *         uiname4='ms' | 
	 *         uiname5='sec' | 
	 *         uiname6='min' | 
	 *         uiname7='hr'
	 *     )
	 *
	 * Features:
	 *    uiname1[0, 1]
	 *         EXCLUDE_IF_SET uiname2
	 *         EXCLUDE_IF_SET uiname3
	 *         EXCLUDE_IF_SET uiname4
	 *         EXCLUDE_IF_SET uiname5
	 *         EXCLUDE_IF_SET uiname6
	 *         EXCLUDE_IF_SET uiname7
	 *    uiname2[0, 1]
	 *         EXCLUDE_IF_SET uiname1
	 *         EXCLUDE_IF_SET uiname3
	 *         EXCLUDE_IF_SET uiname4
	 *         EXCLUDE_IF_SET uiname5
	 *         EXCLUDE_IF_SET uiname6
	 *         EXCLUDE_IF_SET uiname7
	 *    uiname3[0, 1]
	 *         EXCLUDE_IF_SET uiname1
	 *         EXCLUDE_IF_SET uiname2
	 *         EXCLUDE_IF_SET uiname4
	 *         EXCLUDE_IF_SET uiname5
	 *         EXCLUDE_IF_SET uiname6
	 *         EXCLUDE_IF_SET uiname7
	 *    uiname4[0, 1]
	 *         EXCLUDE_IF_SET uiname1
	 *         EXCLUDE_IF_SET uiname2
	 *         EXCLUDE_IF_SET uiname3
	 *         EXCLUDE_IF_SET uiname5
	 *         EXCLUDE_IF_SET uiname6
	 *         EXCLUDE_IF_SET uiname7
	 *    uiname5[0, 1]
	 *         EXCLUDE_IF_SET uiname1
	 *         EXCLUDE_IF_SET uiname2
	 *         EXCLUDE_IF_SET uiname3
	 *         EXCLUDE_IF_SET uiname4
	 *         EXCLUDE_IF_SET uiname6
	 *         EXCLUDE_IF_SET uiname7
	 *    uiname6[0, 1]
	 *         EXCLUDE_IF_SET uiname1
	 *         EXCLUDE_IF_SET uiname2
	 *         EXCLUDE_IF_SET uiname3
	 *         EXCLUDE_IF_SET uiname4
	 *         EXCLUDE_IF_SET uiname5
	 *         EXCLUDE_IF_SET uiname7
	 *    uiname7[0, 1]
	 *         EXCLUDE_IF_SET uiname1
	 *         EXCLUDE_IF_SET uiname2
	 *         EXCLUDE_IF_SET uiname3
	 *         EXCLUDE_IF_SET uiname4
	 *         EXCLUDE_IF_SET uiname5
	 *         EXCLUDE_IF_SET uiname6
	 */
	protected void sequence_unit_identifier(EObject context, unit_identifier semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (vcname1=boolean_literal | vcname2=numeric_literal | (vcname3=string_literal vcname4=string_literal))
	 *
	 * Features:
	 *    vcname1[0, 1]
	 *         EXCLUDE_IF_SET vcname2
	 *         EXCLUDE_IF_SET vcname3
	 *         EXCLUDE_IF_SET vcname4
	 *    vcname2[0, 1]
	 *         EXCLUDE_IF_SET vcname1
	 *         EXCLUDE_IF_SET vcname3
	 *         EXCLUDE_IF_SET vcname4
	 *    vcname3[0, 1]
	 *         EXCLUDE_IF_UNSET vcname4
	 *         MANDATORY_IF_SET vcname4
	 *         EXCLUDE_IF_SET vcname1
	 *         EXCLUDE_IF_SET vcname2
	 *    vcname4[0, 1]
	 *         EXCLUDE_IF_UNSET vcname3
	 *         MANDATORY_IF_SET vcname3
	 *         EXCLUDE_IF_SET vcname1
	 *         EXCLUDE_IF_SET vcname2
	 */
	protected void sequence_value_constant(EObject context, value_constant semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (vename1=relation (vename3+=logical_operator vename2+=relation)*)
	 *
	 * Features:
	 *    vename1[1, 1]
	 *    vename3[0, *]
	 *         SAME vename2
	 *    vename2[0, *]
	 *         SAME vename3
	 */
	protected void sequence_value_expression(EObject context, value_expression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (vname1=value_variable | vname2=value_constant)
	 *
	 * Features:
	 *    vname1[0, 1]
	 *         EXCLUDE_IF_SET vname2
	 *    vname2[0, 1]
	 *         EXCLUDE_IF_SET vname1
	 */
	protected void sequence_value(EObject context, value semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (vvname0=string_literal (vvname1='?' | vvname2='count' | vvname3='fresh')?)
	 *
	 * Features:
	 *    vvname0[1, 1]
	 *    vvname1[0, 1]
	 *         EXCLUDE_IF_SET vvname2
	 *         EXCLUDE_IF_SET vvname3
	 *    vvname2[0, 1]
	 *         EXCLUDE_IF_SET vvname1
	 *         EXCLUDE_IF_SET vvname3
	 *    vvname3[0, 1]
	 *         EXCLUDE_IF_SET vvname1
	 *         EXCLUDE_IF_SET vvname2
	 */
	protected void sequence_value_variable(EObject context, value_variable semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
