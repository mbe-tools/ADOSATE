/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>parameter label</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.parameter_label#getPl1 <em>Pl1</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getparameter_label()
 * @model
 * @generated
 */
public interface parameter_label extends EObject
{
  /**
   * Returns the value of the '<em><b>Pl1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Pl1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pl1</em>' containment reference.
   * @see #setPl1(value_expression)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getparameter_label_Pl1()
   * @model containment="true"
   * @generated
   */
  value_expression getPl1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.parameter_label#getPl1 <em>Pl1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Pl1</em>' containment reference.
   * @see #getPl1()
   * @generated
   */
  void setPl1(value_expression value);

} // parameter_label
