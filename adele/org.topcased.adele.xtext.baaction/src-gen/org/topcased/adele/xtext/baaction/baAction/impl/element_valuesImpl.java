/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.array_data_component_reference;
import org.topcased.adele.xtext.baaction.baAction.element_values;
import org.topcased.adele.xtext.baaction.baAction.integer_range;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>element values</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.element_valuesImpl#getEv1 <em>Ev1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.element_valuesImpl#getEv2 <em>Ev2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.element_valuesImpl#getEv3 <em>Ev3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class element_valuesImpl extends MinimalEObjectImpl.Container implements element_values
{
  /**
   * The cached value of the '{@link #getEv1() <em>Ev1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEv1()
   * @generated
   * @ordered
   */
  protected integer_range ev1;

  /**
   * The default value of the '{@link #getEv2() <em>Ev2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEv2()
   * @generated
   * @ordered
   */
  protected static final String EV2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getEv2() <em>Ev2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEv2()
   * @generated
   * @ordered
   */
  protected String ev2 = EV2_EDEFAULT;

  /**
   * The cached value of the '{@link #getEv3() <em>Ev3</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEv3()
   * @generated
   * @ordered
   */
  protected array_data_component_reference ev3;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected element_valuesImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.ELEMENT_VALUES;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public integer_range getEv1()
  {
    return ev1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetEv1(integer_range newEv1, NotificationChain msgs)
  {
    integer_range oldEv1 = ev1;
    ev1 = newEv1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.ELEMENT_VALUES__EV1, oldEv1, newEv1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEv1(integer_range newEv1)
  {
    if (newEv1 != ev1)
    {
      NotificationChain msgs = null;
      if (ev1 != null)
        msgs = ((InternalEObject)ev1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.ELEMENT_VALUES__EV1, null, msgs);
      if (newEv1 != null)
        msgs = ((InternalEObject)newEv1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.ELEMENT_VALUES__EV1, null, msgs);
      msgs = basicSetEv1(newEv1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.ELEMENT_VALUES__EV1, newEv1, newEv1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getEv2()
  {
    return ev2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEv2(String newEv2)
  {
    String oldEv2 = ev2;
    ev2 = newEv2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.ELEMENT_VALUES__EV2, oldEv2, ev2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public array_data_component_reference getEv3()
  {
    return ev3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetEv3(array_data_component_reference newEv3, NotificationChain msgs)
  {
    array_data_component_reference oldEv3 = ev3;
    ev3 = newEv3;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.ELEMENT_VALUES__EV3, oldEv3, newEv3);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEv3(array_data_component_reference newEv3)
  {
    if (newEv3 != ev3)
    {
      NotificationChain msgs = null;
      if (ev3 != null)
        msgs = ((InternalEObject)ev3).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.ELEMENT_VALUES__EV3, null, msgs);
      if (newEv3 != null)
        msgs = ((InternalEObject)newEv3).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.ELEMENT_VALUES__EV3, null, msgs);
      msgs = basicSetEv3(newEv3, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.ELEMENT_VALUES__EV3, newEv3, newEv3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.ELEMENT_VALUES__EV1:
        return basicSetEv1(null, msgs);
      case BaActionPackage.ELEMENT_VALUES__EV3:
        return basicSetEv3(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.ELEMENT_VALUES__EV1:
        return getEv1();
      case BaActionPackage.ELEMENT_VALUES__EV2:
        return getEv2();
      case BaActionPackage.ELEMENT_VALUES__EV3:
        return getEv3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.ELEMENT_VALUES__EV1:
        setEv1((integer_range)newValue);
        return;
      case BaActionPackage.ELEMENT_VALUES__EV2:
        setEv2((String)newValue);
        return;
      case BaActionPackage.ELEMENT_VALUES__EV3:
        setEv3((array_data_component_reference)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.ELEMENT_VALUES__EV1:
        setEv1((integer_range)null);
        return;
      case BaActionPackage.ELEMENT_VALUES__EV2:
        setEv2(EV2_EDEFAULT);
        return;
      case BaActionPackage.ELEMENT_VALUES__EV3:
        setEv3((array_data_component_reference)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.ELEMENT_VALUES__EV1:
        return ev1 != null;
      case BaActionPackage.ELEMENT_VALUES__EV2:
        return EV2_EDEFAULT == null ? ev2 != null : !EV2_EDEFAULT.equals(ev2);
      case BaActionPackage.ELEMENT_VALUES__EV3:
        return ev3 != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (ev2: ");
    result.append(ev2);
    result.append(')');
    return result.toString();
  }

} //element_valuesImpl
