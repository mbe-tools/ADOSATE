/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.array_index;
import org.topcased.adele.xtext.baaction.baAction.integer_literal;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>array index</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.array_indexImpl#getAi1 <em>Ai1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.array_indexImpl#getAi2 <em>Ai2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.array_indexImpl#getAi3 <em>Ai3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class array_indexImpl extends MinimalEObjectImpl.Container implements array_index
{
  /**
   * The default value of the '{@link #getAi1() <em>Ai1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAi1()
   * @generated
   * @ordered
   */
  protected static final String AI1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAi1() <em>Ai1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAi1()
   * @generated
   * @ordered
   */
  protected String ai1 = AI1_EDEFAULT;

  /**
   * The cached value of the '{@link #getAi2() <em>Ai2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAi2()
   * @generated
   * @ordered
   */
  protected integer_literal ai2;

  /**
   * The default value of the '{@link #getAi3() <em>Ai3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAi3()
   * @generated
   * @ordered
   */
  protected static final String AI3_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAi3() <em>Ai3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAi3()
   * @generated
   * @ordered
   */
  protected String ai3 = AI3_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected array_indexImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.ARRAY_INDEX;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAi1()
  {
    return ai1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAi1(String newAi1)
  {
    String oldAi1 = ai1;
    ai1 = newAi1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.ARRAY_INDEX__AI1, oldAi1, ai1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public integer_literal getAi2()
  {
    return ai2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAi2(integer_literal newAi2, NotificationChain msgs)
  {
    integer_literal oldAi2 = ai2;
    ai2 = newAi2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.ARRAY_INDEX__AI2, oldAi2, newAi2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAi2(integer_literal newAi2)
  {
    if (newAi2 != ai2)
    {
      NotificationChain msgs = null;
      if (ai2 != null)
        msgs = ((InternalEObject)ai2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.ARRAY_INDEX__AI2, null, msgs);
      if (newAi2 != null)
        msgs = ((InternalEObject)newAi2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.ARRAY_INDEX__AI2, null, msgs);
      msgs = basicSetAi2(newAi2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.ARRAY_INDEX__AI2, newAi2, newAi2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAi3()
  {
    return ai3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAi3(String newAi3)
  {
    String oldAi3 = ai3;
    ai3 = newAi3;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.ARRAY_INDEX__AI3, oldAi3, ai3));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.ARRAY_INDEX__AI2:
        return basicSetAi2(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.ARRAY_INDEX__AI1:
        return getAi1();
      case BaActionPackage.ARRAY_INDEX__AI2:
        return getAi2();
      case BaActionPackage.ARRAY_INDEX__AI3:
        return getAi3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.ARRAY_INDEX__AI1:
        setAi1((String)newValue);
        return;
      case BaActionPackage.ARRAY_INDEX__AI2:
        setAi2((integer_literal)newValue);
        return;
      case BaActionPackage.ARRAY_INDEX__AI3:
        setAi3((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.ARRAY_INDEX__AI1:
        setAi1(AI1_EDEFAULT);
        return;
      case BaActionPackage.ARRAY_INDEX__AI2:
        setAi2((integer_literal)null);
        return;
      case BaActionPackage.ARRAY_INDEX__AI3:
        setAi3(AI3_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.ARRAY_INDEX__AI1:
        return AI1_EDEFAULT == null ? ai1 != null : !AI1_EDEFAULT.equals(ai1);
      case BaActionPackage.ARRAY_INDEX__AI2:
        return ai2 != null;
      case BaActionPackage.ARRAY_INDEX__AI3:
        return AI3_EDEFAULT == null ? ai3 != null : !AI3_EDEFAULT.equals(ai3);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (ai1: ");
    result.append(ai1);
    result.append(", ai3: ");
    result.append(ai3);
    result.append(')');
    return result.toString();
  }

} //array_indexImpl
