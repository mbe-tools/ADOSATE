/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.topcased.adele.xtext.baaction.baAction.BaActionPackage;
import org.topcased.adele.xtext.baaction.baAction.parameter_label;
import org.topcased.adele.xtext.baaction.baAction.value_expression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>parameter label</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.impl.parameter_labelImpl#getPl1 <em>Pl1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class parameter_labelImpl extends MinimalEObjectImpl.Container implements parameter_label
{
  /**
   * The cached value of the '{@link #getPl1() <em>Pl1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPl1()
   * @generated
   * @ordered
   */
  protected value_expression pl1;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected parameter_labelImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return BaActionPackage.Literals.PARAMETER_LABEL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public value_expression getPl1()
  {
    return pl1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPl1(value_expression newPl1, NotificationChain msgs)
  {
    value_expression oldPl1 = pl1;
    pl1 = newPl1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BaActionPackage.PARAMETER_LABEL__PL1, oldPl1, newPl1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPl1(value_expression newPl1)
  {
    if (newPl1 != pl1)
    {
      NotificationChain msgs = null;
      if (pl1 != null)
        msgs = ((InternalEObject)pl1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.PARAMETER_LABEL__PL1, null, msgs);
      if (newPl1 != null)
        msgs = ((InternalEObject)newPl1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BaActionPackage.PARAMETER_LABEL__PL1, null, msgs);
      msgs = basicSetPl1(newPl1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, BaActionPackage.PARAMETER_LABEL__PL1, newPl1, newPl1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case BaActionPackage.PARAMETER_LABEL__PL1:
        return basicSetPl1(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case BaActionPackage.PARAMETER_LABEL__PL1:
        return getPl1();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case BaActionPackage.PARAMETER_LABEL__PL1:
        setPl1((value_expression)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.PARAMETER_LABEL__PL1:
        setPl1((value_expression)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case BaActionPackage.PARAMETER_LABEL__PL1:
        return pl1 != null;
    }
    return super.eIsSet(featureID);
  }

} //parameter_labelImpl
