/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>communication action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa1 <em>Ca1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa2 <em>Ca2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa3 <em>Ca3</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa4 <em>Ca4</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa5 <em>Ca5</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa21 <em>Ca21</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa22 <em>Ca22</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa23 <em>Ca23</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa24 <em>Ca24</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa25 <em>Ca25</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa26 <em>Ca26</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa27 <em>Ca27</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa28 <em>Ca28</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa29 <em>Ca29</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa30 <em>Ca30</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa31 <em>Ca31</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa32 <em>Ca32</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa33 <em>Ca33</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action()
 * @model
 * @generated
 */
public interface communication_action extends EObject
{
  /**
   * Returns the value of the '<em><b>Ca1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca1</em>' attribute.
   * @see #setCa1(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca1()
   * @model
   * @generated
   */
  String getCa1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa1 <em>Ca1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca1</em>' attribute.
   * @see #getCa1()
   * @generated
   */
  void setCa1(String value);

  /**
   * Returns the value of the '<em><b>Ca2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca2</em>' attribute.
   * @see #setCa2(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca2()
   * @model
   * @generated
   */
  String getCa2();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa2 <em>Ca2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca2</em>' attribute.
   * @see #getCa2()
   * @generated
   */
  void setCa2(String value);

  /**
   * Returns the value of the '<em><b>Ca3</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca3</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca3</em>' attribute.
   * @see #setCa3(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca3()
   * @model
   * @generated
   */
  String getCa3();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa3 <em>Ca3</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca3</em>' attribute.
   * @see #getCa3()
   * @generated
   */
  void setCa3(String value);

  /**
   * Returns the value of the '<em><b>Ca4</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca4</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca4</em>' containment reference.
   * @see #setCa4(subprogram_parameter_list)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca4()
   * @model containment="true"
   * @generated
   */
  subprogram_parameter_list getCa4();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa4 <em>Ca4</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca4</em>' containment reference.
   * @see #getCa4()
   * @generated
   */
  void setCa4(subprogram_parameter_list value);

  /**
   * Returns the value of the '<em><b>Ca5</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca5</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca5</em>' attribute.
   * @see #setCa5(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca5()
   * @model
   * @generated
   */
  String getCa5();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa5 <em>Ca5</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca5</em>' attribute.
   * @see #getCa5()
   * @generated
   */
  void setCa5(String value);

  /**
   * Returns the value of the '<em><b>Ca21</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca21</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca21</em>' attribute.
   * @see #setCa21(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca21()
   * @model
   * @generated
   */
  String getCa21();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa21 <em>Ca21</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca21</em>' attribute.
   * @see #getCa21()
   * @generated
   */
  void setCa21(String value);

  /**
   * Returns the value of the '<em><b>Ca22</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca22</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca22</em>' attribute.
   * @see #setCa22(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca22()
   * @model
   * @generated
   */
  String getCa22();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa22 <em>Ca22</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca22</em>' attribute.
   * @see #getCa22()
   * @generated
   */
  void setCa22(String value);

  /**
   * Returns the value of the '<em><b>Ca23</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca23</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca23</em>' attribute.
   * @see #setCa23(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca23()
   * @model
   * @generated
   */
  String getCa23();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa23 <em>Ca23</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca23</em>' attribute.
   * @see #getCa23()
   * @generated
   */
  void setCa23(String value);

  /**
   * Returns the value of the '<em><b>Ca24</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca24</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca24</em>' attribute.
   * @see #setCa24(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca24()
   * @model
   * @generated
   */
  String getCa24();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa24 <em>Ca24</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca24</em>' attribute.
   * @see #getCa24()
   * @generated
   */
  void setCa24(String value);

  /**
   * Returns the value of the '<em><b>Ca25</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca25</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca25</em>' attribute.
   * @see #setCa25(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca25()
   * @model
   * @generated
   */
  String getCa25();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa25 <em>Ca25</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca25</em>' attribute.
   * @see #getCa25()
   * @generated
   */
  void setCa25(String value);

  /**
   * Returns the value of the '<em><b>Ca26</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca26</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca26</em>' attribute.
   * @see #setCa26(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca26()
   * @model
   * @generated
   */
  String getCa26();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa26 <em>Ca26</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca26</em>' attribute.
   * @see #getCa26()
   * @generated
   */
  void setCa26(String value);

  /**
   * Returns the value of the '<em><b>Ca27</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca27</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca27</em>' attribute.
   * @see #setCa27(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca27()
   * @model
   * @generated
   */
  String getCa27();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa27 <em>Ca27</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca27</em>' attribute.
   * @see #getCa27()
   * @generated
   */
  void setCa27(String value);

  /**
   * Returns the value of the '<em><b>Ca28</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca28</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca28</em>' attribute.
   * @see #setCa28(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca28()
   * @model
   * @generated
   */
  String getCa28();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa28 <em>Ca28</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca28</em>' attribute.
   * @see #getCa28()
   * @generated
   */
  void setCa28(String value);

  /**
   * Returns the value of the '<em><b>Ca29</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca29</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca29</em>' attribute.
   * @see #setCa29(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca29()
   * @model
   * @generated
   */
  String getCa29();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa29 <em>Ca29</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca29</em>' attribute.
   * @see #getCa29()
   * @generated
   */
  void setCa29(String value);

  /**
   * Returns the value of the '<em><b>Ca30</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca30</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca30</em>' attribute.
   * @see #setCa30(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca30()
   * @model
   * @generated
   */
  String getCa30();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa30 <em>Ca30</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca30</em>' attribute.
   * @see #getCa30()
   * @generated
   */
  void setCa30(String value);

  /**
   * Returns the value of the '<em><b>Ca31</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca31</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca31</em>' attribute.
   * @see #setCa31(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca31()
   * @model
   * @generated
   */
  String getCa31();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa31 <em>Ca31</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca31</em>' attribute.
   * @see #getCa31()
   * @generated
   */
  void setCa31(String value);

  /**
   * Returns the value of the '<em><b>Ca32</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca32</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca32</em>' attribute.
   * @see #setCa32(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca32()
   * @model
   * @generated
   */
  String getCa32();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa32 <em>Ca32</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca32</em>' attribute.
   * @see #getCa32()
   * @generated
   */
  void setCa32(String value);

  /**
   * Returns the value of the '<em><b>Ca33</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ca33</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ca33</em>' attribute.
   * @see #setCa33(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getcommunication_action_Ca33()
   * @model
   * @generated
   */
  String getCa33();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.communication_action#getCa33 <em>Ca33</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ca33</em>' attribute.
   * @see #getCa33()
   * @generated
   */
  void setCa33(String value);

} // communication_action
