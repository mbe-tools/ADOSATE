/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>behavior action sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence#getBaseq1 <em>Baseq1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence#getBaseq2 <em>Baseq2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence#getBaseq3 <em>Baseq3</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_sequence()
 * @model
 * @generated
 */
public interface behavior_action_sequence extends EObject
{
  /**
   * Returns the value of the '<em><b>Baseq1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Baseq1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Baseq1</em>' containment reference.
   * @see #setBaseq1(behavior_action)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_sequence_Baseq1()
   * @model containment="true"
   * @generated
   */
  behavior_action getBaseq1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_sequence#getBaseq1 <em>Baseq1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Baseq1</em>' containment reference.
   * @see #getBaseq1()
   * @generated
   */
  void setBaseq1(behavior_action value);

  /**
   * Returns the value of the '<em><b>Baseq2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Baseq2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Baseq2</em>' attribute list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_sequence_Baseq2()
   * @model unique="false"
   * @generated
   */
  EList<String> getBaseq2();

  /**
   * Returns the value of the '<em><b>Baseq3</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baaction.baAction.behavior_action}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Baseq3</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Baseq3</em>' containment reference list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_sequence_Baseq3()
   * @model containment="true"
   * @generated
   */
  EList<behavior_action> getBaseq3();

} // behavior_action_sequence
