/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>unary boolean operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.unary_boolean_operator#getUboname <em>Uboname</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getunary_boolean_operator()
 * @model
 * @generated
 */
public interface unary_boolean_operator extends EObject
{
  /**
   * Returns the value of the '<em><b>Uboname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Uboname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Uboname</em>' attribute.
   * @see #setUboname(String)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getunary_boolean_operator_Uboname()
   * @model
   * @generated
   */
  String getUboname();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.unary_boolean_operator#getUboname <em>Uboname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Uboname</em>' attribute.
   * @see #getUboname()
   * @generated
   */
  void setUboname(String value);

} // unary_boolean_operator
