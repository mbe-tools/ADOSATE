/**
 * <copyright>
 * </copyright>
 *

 */
package org.topcased.adele.xtext.baaction.baAction;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>behavior action set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_set#getBaset1 <em>Baset1</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_set#getBaset2 <em>Baset2</em>}</li>
 *   <li>{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_set#getBaset3 <em>Baset3</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_set()
 * @model
 * @generated
 */
public interface behavior_action_set extends EObject
{
  /**
   * Returns the value of the '<em><b>Baset1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Baset1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Baset1</em>' containment reference.
   * @see #setBaset1(behavior_action)
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_set_Baset1()
   * @model containment="true"
   * @generated
   */
  behavior_action getBaset1();

  /**
   * Sets the value of the '{@link org.topcased.adele.xtext.baaction.baAction.behavior_action_set#getBaset1 <em>Baset1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Baset1</em>' containment reference.
   * @see #getBaset1()
   * @generated
   */
  void setBaset1(behavior_action value);

  /**
   * Returns the value of the '<em><b>Baset2</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Baset2</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Baset2</em>' attribute list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_set_Baset2()
   * @model unique="false"
   * @generated
   */
  EList<String> getBaset2();

  /**
   * Returns the value of the '<em><b>Baset3</b></em>' containment reference list.
   * The list contents are of type {@link org.topcased.adele.xtext.baaction.baAction.behavior_action}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Baset3</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Baset3</em>' containment reference list.
   * @see org.topcased.adele.xtext.baaction.baAction.BaActionPackage#getbehavior_action_set_Baset3()
   * @model containment="true"
   * @generated
   */
  EList<behavior_action> getBaset3();

} // behavior_action_set
