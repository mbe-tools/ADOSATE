
package org.topcased.adele.xtext.baaction;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class BaActionStandaloneSetup extends BaActionStandaloneSetupGenerated{

	public static void doSetup() {
		new BaActionStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

