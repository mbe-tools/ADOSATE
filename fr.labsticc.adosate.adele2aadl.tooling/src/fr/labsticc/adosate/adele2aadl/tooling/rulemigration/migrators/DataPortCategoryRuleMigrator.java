package fr.labsticc.adosate.adele2aadl.tooling.rulemigration.migrators;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.osate.aadl2.Aadl2Package;
import org.osate.aadl2.PortCategory;
import org.topcased.adele.common.utils.AdeleCommonUtils;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;

import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.tgg.ModelElement;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.TGGRule;
import fr.labsticc.adosate.adele2aadl.tooling.rulemigration.AbstractTGGRuleMigrator;

public class DataPortCategoryRuleMigrator extends AbstractTGGRuleMigrator {

	private static final String PORT_LIT = "Port"; 
	private static final String DATA_LIT = "Data"; 
	private static final String DATA_SUBCOMPONENT_TYPE_LIT = DATA_LIT + SUBCOMPONENT_TYPE_LIT;
	private static final String DATA_FEATURE_CLASS_LIT = "data" + FEATURE_CLASS_LIT;
	private static final String DATA_PORT_LIT = DATA_LIT + PORT_LIT; 

	public DataPortCategoryRuleMigrator() {
		super( 	RULE_LIT + WILDCARD_0_ANY_LIT + DATA_PORT_LIT + TO_LIT + WILDCARD_0_ANY_LIT + DATA_PORT_LIT + WILDCARD_0_ANY_LIT );
	}

	@Override
	public void migrateSourceDomain( final TGGRule p_newRule ) {
		final PortCategory portCat = findPortCategory( p_newRule.getName() );
		
		for ( final ModelElement element : p_newRule.getSourceDomain().getModelElements() ) {
			if ( element instanceof ModelObject ) {
				final ModelObject modelObject = (ModelObject) element;
				
				if ( ( AdeleCommonUtils.ADELE_EXT + PORT_LIT ).equals( element.getName() ) ) {
					final EClass portClass = (EClass) ADELE_FeaturesPackage.eINSTANCE.getEClassifier( capitalize( portCat.getName() ) + PORT_LIT );
					modelObject.setClassifier( portClass );
					
					for ( final AttributeAssignment assignment : modelObject.getAttributeAssignments() ) {
						if ( ADELE_FeaturesPackage.Literals.PORT_DIRECTION.equals( assignment.getEStructuralFeature().getEType() ) ) {
							assignment.setEStructuralFeature( portClass.getEStructuralFeature( assignment.getEStructuralFeature().getName() ) );
							
							break;
						}
					}
					
					break;
				}
//				else if ( ( ADELE_LIT + TYPE_LIT ).equals( element.getName() ) ) {
//					final ComponentCategory compoCat = findComponentCategory( p_newRule.getName(), portCat );
//					final EClass compoClass = (EClass) ADELE_ComponentsPackage.eINSTANCE.getEClassifier( capitalize( compoCat.getName() ) );
//					modelObject.setClassifier( compoClass );
//				}
			}
		}
	}

	@Override
	public void migrateCorrespondenceDomain( final TGGRule p_newRule ) {
	}

	@Override
	public void migrateTargetDomain( final TGGRule p_newRule ) {
		//super.migrateTargetDomain( p_newRule );
		
		final PortCategory portCat = findPortCategory( p_newRule.getName() );
		final EClass portClass = (EClass) Aadl2Package.eINSTANCE.getEClassifier( capitalize( portCat.getName() ) + PORT_LIT );
		
		for ( final ModelElement element : p_newRule.getTargetDomain().getModelElements() ) {
			if ( element instanceof ModelObject ) {
				final ModelObject modelObj = (ModelObject) element;

				if ( ( AADL_LIT + PORT_LIT ).equals( element.getName() ) ) {
					modelObj.setClassifier( portClass );

					for ( final AttributeAssignment attAss : modelObj.getAttributeAssignments() ) {
						if ( attAss.getEStructuralFeature().getName().endsWith( FEATURE_CLASS_LIT ) ) {
							final EStructuralFeature classFeat = portClass.getEStructuralFeature( DATA_FEATURE_CLASS_LIT );
							attAss.setEStructuralFeature( classFeat );
						}
					}
				}
				else if ( ( AADL_LIT + CLASSIFIER_LIT ).equals( element.getName() ) ) {
					final EClassifier portTypeClass = Aadl2Package.eINSTANCE.getEClassifier( DATA_SUBCOMPONENT_TYPE_LIT );
					modelObj.setClassifier( portTypeClass );
				}
			}
			else if ( element instanceof ModelLink ) {
				final ModelLink modelLink = (ModelLink) element;
				
				if ( modelLink.getEReference().getName().endsWith( FEATURE_CLASS_LIT ) ) {
					modelLink.setEReference( (EReference) portClass.getEStructuralFeature( DATA_FEATURE_CLASS_LIT ) );
				}
			}
		}
	}

	@Override
	public Set<String> getRulesToMigrate( final TGGRule p_templateRule ) {
		final Set<String> ruleNames = createRuleNamesSet();

		if ( isMigratorFor( p_templateRule ) ) {
			final String templateName = p_templateRule.getName();
			final PortCategory templatePortCat = findPortCategory( templateName );
			final String portNameToReplace = capitalize( templatePortCat.getName() ) + PORT_LIT;
//			final ComponentCategory templateCompoCat = findComponentCategory( templateName, templatePortCat );
//			final String compoNameToReplace = portSuffix + TO_LIT + capitalize( templateCompoCat.getName() );
//
//			for ( final ComponentCategory compoCat: ComponentCategory.values() ) {
//				final String capitCompoName = capitalize( compoCat.getName() );
//				final EClass typeClass = (EClass) Aadl2Package.eINSTANCE.getEClassifier( capitCompoName + TYPE_LIT );
				
			for ( final PortCategory portCat : getPortCategories() ) {
				final String capitPortName = capitalize( portCat.getName() )+ PORT_LIT;
				
//					if ( typeClass.getEStructuralFeature( OWNED_LIT + capitPortName ) != null ) {
				String ruleName = templateName.replaceAll( portNameToReplace, capitPortName  );
//						ruleName = ruleName.replaceFirst( compoNameToReplace, portSuffix + TO_LIT + capitCompoName );
					
				if ( !templateName.equals( ruleName ) ) {
					ruleNames.add( ruleName );
				}
//					}
			}
//			}
		}
		
		return ruleNames;
	}
	
	private Collection<PortCategory> getPortCategories() {
		return Arrays.asList( new PortCategory[] { PortCategory.DATA, PortCategory.EVENT_DATA } );
	}

	private PortCategory findPortCategory( final String p_ruleName ) {
		int indexMin = Integer.MAX_VALUE;
		PortCategory portCat = null;
		
		for ( final PortCategory category : PortCategory.values() ) {
			final int index = p_ruleName.lastIndexOf( capitalize( category.getName() ) + PORT_LIT );
			
			if ( index >= 0 ) {
				if ( index < indexMin ) {
					indexMin = index;
					portCat = category;
				}
			}
		}
		
		return portCat;
	}
}
