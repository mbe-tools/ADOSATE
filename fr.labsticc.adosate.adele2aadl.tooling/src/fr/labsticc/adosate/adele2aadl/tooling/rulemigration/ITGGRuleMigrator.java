package fr.labsticc.adosate.adele2aadl.tooling.rulemigration;

import java.util.Set;

import de.hpi.sam.tgg.TGGRule;

public interface ITGGRuleMigrator {
	
	void migrateSourceDomain( final TGGRule p_newRule );
	void migrateCorrespondenceDomain( final TGGRule p_newRule );
	void migrateTargetDomain( final TGGRule p_newRule );
	
	boolean isMigratorFor( TGGRule p_rule );
	
	Set<String> getRulesToMigrate( TGGRule p_rule );
	
	boolean isModifying();
}
