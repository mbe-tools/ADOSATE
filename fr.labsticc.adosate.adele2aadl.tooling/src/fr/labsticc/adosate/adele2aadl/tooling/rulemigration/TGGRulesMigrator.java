package fr.labsticc.adosate.adele2aadl.tooling.rulemigration;

import java.util.Collection;
import java.util.HashSet;

import org.eclipse.emf.edit.domain.EditingDomain;

import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.TGGRule;
import fr.labsticc.adosate.adele2aadl.tooling.rulemigration.migrators.CreatedModelObjectsConstraintsRuleMigrator;

public class TGGRulesMigrator extends TGGRulesDuplicator {
	
	final Collection<ITGGRuleMigrator> ruleMigrators;
	
	public TGGRulesMigrator() {
		super();
		
		ruleMigrators = new HashSet<ITGGRuleMigrator>();
		ruleMigrators.add( new CreatedModelObjectsConstraintsRuleMigrator() );
//		ruleMigrators.add( new AccessCategoryRuleMigrator() );
//		ruleMigrators.add( new DataPortCategoryRuleMigrator() );
//		ruleMigrators.add( new CompTypeRuleMigrator() );
//		ruleMigrators.add( new CompImplRuleMigrator() );
//		ruleMigrators.add( new SubcomponentCategoryRuleMigrator() );
	}

	@Override
	protected void duplicateTggRule( 	final EditingDomain p_editingDomain,
										final TGGRule p_rule ) {
		final ITGGRuleMigrator migrator = getMigrator( p_rule );
		
		if ( migrator != null ) {
			for ( final String ruleToMigrate : migrator.getRulesToMigrate( p_rule ) ) {
				final TGGDiagram tggDiagram = (TGGDiagram) p_rule.eContainer();
				final TGGRule existRule = findRule( tggDiagram, ruleToMigrate );
				
				if ( existRule != null && existRule != p_rule ) {
					tggDiagram.getTggRules().remove( existRule );
				}
	
				final TGGRule migratedRule;
				
				if ( migrator.isModifying() ) {
					migratedRule = existRule;
				}
				else {
					migratedRule = copy( p_editingDomain, p_rule );
					migratedRule.setName( ruleToMigrate );
					migratedRule.setDescription( "Migrated from " + p_rule.getName() + "." );
					tggDiagram.getTggRules().add( migratedRule );
				}
				
				migrator.migrateSourceDomain( migratedRule );
				migrator.migrateCorrespondenceDomain( migratedRule );
				migrator.migrateTargetDomain( migratedRule );
			}
		}
	}
	
	private ITGGRuleMigrator getMigrator( final TGGRule p_rule ) {
		for ( final ITGGRuleMigrator migrator : ruleMigrators ) {
			if ( migrator.isMigratorFor( p_rule ) ) {
				return migrator;
			}
		}
		
		return null;
	}
 }
