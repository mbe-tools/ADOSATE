package fr.labsticc.adosate.adele2aadl.tooling.rulemigration;

import java.util.Collection;
import java.util.Collections;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.CopyCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import de.hpi.sam.tgg.TGGDiagram;
import de.hpi.sam.tgg.TGGRule;

public abstract class TGGRulesDuplicator {
	
	protected TGGRulesDuplicator() {
	}
	
	public void duplicateTggRules( 	final EditingDomain p_editingDomain,
									final Collection<TGGRule> p_rules ) {
		for ( final TGGRule rule : p_rules ) {
			duplicateTggRule( p_editingDomain, rule );
		}
	}
	
	protected abstract void duplicateTggRule( 	final EditingDomain p_editingDomain,
												final TGGRule p_rule );
	
	protected <T extends EObject> T copy( 	final EditingDomain p_editingDomain,
											final T p_origElement ) {
		final Command copyCommand = CopyCommand.create( p_editingDomain, Collections.singletonList( p_origElement ) );
		copyCommand.execute();
		@SuppressWarnings("unchecked")
		final T duplicatedElem = (T) copyCommand.getResult().iterator().next();
		
		return duplicatedElem;
	}
	
	protected CopyCommand copyCommand( 	final EditingDomain p_editingDomain,
									final EObject p_origElement ) {
		final CopyCommand copyCommand = (CopyCommand) CopyCommand.create( p_editingDomain, Collections.singletonList( p_origElement ) );
		copyCommand.execute();
		
		return copyCommand;
	}
	
	protected TGGRule findRule( final TGGDiagram p_diagram,
								final String p_name ) {
		for ( final TGGRule tggRule : p_diagram.getTggRules() ) {
			if ( p_name.equals( tggRule.getName() ) ) {
				return tggRule;
			}
		}
		
		return null;
	}
 }
