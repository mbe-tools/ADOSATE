package fr.labsticc.adosate.adele2aadl.tooling.rulemigration.migrators;

import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.osate.aadl2.Aadl2Package;
import org.osate.aadl2.ComponentCategory;
import org.topcased.adele.common.utils.AdeleCommonUtils;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;

import de.hpi.sam.tgg.ModelElement;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.SourceModelDomain;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.TargetModelDomain;

public class CompImplRuleMigrator extends CompTypeRuleMigrator {

	public CompImplRuleMigrator() {
		super( RULE_LIT + WILDCARD_1_ANY_LIT + TO_LIT + WILDCARD_1_ANY_LIT + IMPL_LIT + WILDCARD_0_ANY_LIT );
	}

	@Override
	public void migrateSourceDomain( final TGGRule p_newRule ) {
		super.migrateSourceDomain( p_newRule );
		
		final SourceModelDomain srcDomain = p_newRule.getSourceDomain();
		
		for ( final ModelElement element : srcDomain.getModelElements() ) {
			if ( element instanceof ModelObject ) {
				final ModelObject modelObject = (ModelObject) element;
				
				if ( ( AdeleCommonUtils.ADELE_EXT + IMPL_LIT ).equals( element.getName() ) ) {
					final ComponentCategory compoCat = findComponentCategory( p_newRule.getName() );
					final EClass compoClass = (EClass) ADELE_ComponentsPackage.eINSTANCE.getEClassifier( capitalize( compoCat.getName() ) );
					modelObject.setClassifier( compoClass );
					
					break;
				}
			}
		}
	}

	@Override
	public void migrateTargetDomain( final TGGRule p_newRule ) {
		super.migrateTargetDomain( p_newRule );
		
		final TargetModelDomain targetDomain = p_newRule.getTargetDomain();

		for ( final ModelElement element : targetDomain.getModelElements() ) {
			if ( element instanceof ModelObject ) {
				final ModelObject modelObj = (ModelObject) element;

				if ( ( AADL_LIT + IMPL_LIT ).equals( element.getName() ) ) {
					final ComponentCategory compoCat = findComponentCategory( p_newRule.getName() );
					final EClass compoClass = (EClass) Aadl2Package.eINSTANCE.getEClassifier( capitalize( compoCat.getName() ) + IMPL_LIT );
					modelObj.setClassifier( compoClass );
					
					break;
				}
			}
		}
	}
//
//	@Override
//	public boolean isMigratorFor( final TGGRule p_rule ) {
//		return 	p_rule.getName().endsWith( IMPL_LIT ) ||
//				p_rule.getName().endsWith( IMPL_LIT + WITH_EXTENDS_LIT ) ||
//				p_rule.getName().endsWith( IMPL_LIT + WITH_EXTENDS_LIT + "OtherRes" );
//	}

	@Override
	public Set<String> getRulesToMigrate( final TGGRule p_templateRule ) {
		final Set<String> ruleNames = createRuleNamesSet();

		if ( isMigratorFor( p_templateRule ) ) {
			final String templateName = p_templateRule.getName();
			final ComponentCategory templateCompoCat = findComponentCategory( templateName );
			final String templateCapitCatName = capitalize( templateCompoCat.getName() );
			final String templateNameToReplace = templateCapitCatName + TO_LIT + templateCapitCatName + IMPL_LIT;

			for ( final ComponentCategory compoCat: ComponentCategory.values() ) {
				if ( compoCat != templateCompoCat ) {
					final String capitCatName = capitalize( compoCat.getName() );
					final String replacement = capitCatName + TO_LIT + capitCatName + IMPL_LIT;
					final String newRuleName = templateName.replace( templateNameToReplace, replacement );
					ruleNames.add( newRuleName );
				}
			}
		}
		
		return ruleNames;
	}

	@Override
	protected ComponentCategory findComponentCategory( final String p_ruleName ) {
		for ( final ComponentCategory category : ComponentCategory.values() ) {
			final String capitCatName = capitalize( category.getName() );
			
			if ( p_ruleName.contains( capitCatName + TO_LIT + capitCatName + IMPL_LIT ) ) {
				return category;
			}
		}
		
		return null;
	}
}
