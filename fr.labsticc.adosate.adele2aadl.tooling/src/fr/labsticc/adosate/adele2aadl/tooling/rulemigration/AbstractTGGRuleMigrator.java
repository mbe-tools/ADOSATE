package fr.labsticc.adosate.adele2aadl.tooling.rulemigration;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hpi.sam.tgg.TGGRule;

import fr.labsticc.framework.core.util.ReflectUtil;

public abstract class AbstractTGGRuleMigrator implements ITGGRuleMigrator {
	
	protected static final String RULE_LIT = "Rule";
	protected static final String WILDCARD_0_ANY_LIT = "(.*)";
	protected static final String WILDCARD_1_ANY_LIT = "(.+)";
	protected static final String AADL_LIT = "aadl";
	protected static final String TO_LIT = "2";
	protected static final String TYPE_LIT = "Type";
	protected static final String IMPL_LIT = "Implementation";
	protected static final String SUBCOMPONENT_LIT = "Subcomponent";
	protected static final String SUBCOMPONENT_TYPE_LIT = SUBCOMPONENT_LIT + TYPE_LIT;
	protected static final String CLASSIFIER_LIT = "Classifier";
	protected static final String FEATURE_CLASS_LIT = "FeatureClassifier";
	protected static final String OWNED_LIT = "owned";
	protected static final String WITH_CLASS_LIT = "WithClass";
	protected static final String WITH_EXTENDS_LIT = "WithExtends";
	protected static final String CORR_CLASS_PREFIX_LIT = "Corr";
	protected static final String CORR_NAME_PREFIX_LIT = "corr";
	protected static final String OTHER_RES_LIT = "OtherRes";
	protected static final String REFINED_LIT = "Refined";
	

	private final Pattern ruleNamePattern;
	
	protected AbstractTGGRuleMigrator( final String p_pattern ) {
		ruleNamePattern = Pattern.compile( p_pattern );
	}
	
	public static String capitalize( final String p_name ) {
		final StringBuilder strBuilder = new StringBuilder();
		String currentName = p_name;
		int indexSpace = currentName.indexOf( " " );
		
		while ( indexSpace >= 0 ) {
			strBuilder.append( ReflectUtil.capitalizePropertyName( currentName.substring( 0, indexSpace ) ) );
			currentName = currentName.substring( indexSpace + 1 );
			indexSpace = currentName.indexOf( " " );
		}
		
		strBuilder.append( ReflectUtil.capitalizePropertyName( currentName  ) );
		
		return strBuilder.toString();
	}
	
	protected Set<String> createRuleNamesSet() {
		return new LinkedHashSet<String>();
	}

	@Override
	public boolean isMigratorFor( final TGGRule p_rule ) {
		 final Matcher matcher = ruleNamePattern.matcher( p_rule.getName() );
		 
		 return matcher.matches();		
	}

	@Override
	public boolean isModifying() {
		return false;
	}
}
