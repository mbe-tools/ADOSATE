package fr.labsticc.adosate.adele2aadl.tooling.rulemigration.migrators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.hpi.sam.storyDiagramEcore.callActions.CallActionsFactory;
import de.hpi.sam.storyDiagramEcore.callActions.CompareAction;
import de.hpi.sam.storyDiagramEcore.callActions.GetPropertyValueAction;
import de.hpi.sam.storyDiagramEcore.expressions.CallActionExpression;
import de.hpi.sam.storyDiagramEcore.expressions.Expression;
import de.hpi.sam.storyDiagramEcore.expressions.ExpressionsFactory;
import de.hpi.sam.storyDiagramEcore.expressions.StringExpression;
import de.hpi.sam.tgg.ModelDomain;
import de.hpi.sam.tgg.ModelElement;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.TGGModifierEnumeration;
import de.hpi.sam.tgg.TGGRule;
import fr.labsticc.adosate.adele2aadl.tooling.rulemigration.ITGGRuleMigrator;

public class CreatedModelObjectsConstraintsRuleMigrator implements ITGGRuleMigrator {
	
	public CreatedModelObjectsConstraintsRuleMigrator() {
	}
	
	private void migrateCreateModelObjectConstraints( final ModelDomain p_domain ) {
		for ( final ModelElement element : p_domain.getModelElements() ) {
			if ( element.getModifier() == TGGModifierEnumeration.CREATE && element instanceof ModelObject ) {
				final ModelObject modelObject = (ModelObject) element;
				
				if ( modelObject.getClassifier() instanceof EClass ) {
					final Collection<CallActionExpression> createdExpressions = new ArrayList<CallActionExpression>();
					final EClass eClass = (EClass) modelObject.getClassifier(); 
					final Iterator<Expression> itetator = modelObject.getConstraintExpressions().iterator();
					
					while ( itetator.hasNext() ) {
						final Expression constraint = itetator.next();
						
						if ( constraint instanceof StringExpression ) {
							final String exprText = ( (StringExpression) constraint ).getExpressionString().trim();
							final String delimiter;
							
							if ( exprText.endsWith( ".oclIsUndefined()" ) ) {
								delimiter = "\\.";
							}
							else if ( exprText.contains( "=" ) ) {
								delimiter = "=";
							}
							else if ( exprText.startsWith( "not" ) ) {
								delimiter = " ";
							}
							else {
								delimiter = null;
							}
							
							final String featureName;
							final String valueExpr;

							if ( delimiter != null ) {
								final String[] elements = exprText.split( delimiter );
								
								if ( elements.length == 2 ) {
									if ( "not".equals( elements [ 0 ].trim() ) ) {
										featureName = elements [ 1 ].trim();
										valueExpr = "false";
									}
									else {
										featureName = elements [ 0 ].trim(); 

										final String val = elements [ 1 ].trim();
										valueExpr = "oclIsUndefined()".equals( val ) ? "null" : val;
									}
								}
								else {
									featureName = null;
									valueExpr = null;
								}
							}
							else {
								featureName = exprText;
								valueExpr =  "true";
							}
								
							if ( featureName == null ) {
								System.out.println( "Unable to convert constraint " + exprText + " on model object " + modelObject.getName()
										+ " of rule " + ( (TGGRule) p_domain.eContainer() ).getName() + "." );
							}
							else {
								final EStructuralFeature feature = eClass.getEStructuralFeature( featureName );
								
								if ( feature != null ) {
									final GetPropertyValueAction getPropAct = CallActionsFactory.eINSTANCE.createGetPropertyValueAction();
									getPropAct.setProperty( feature );
									final CallActionExpression callExp = ExpressionsFactory.eINSTANCE.createCallActionExpression();
									callExp.getCallActions().add( getPropAct );
									final CompareAction compAction = CallActionsFactory.eINSTANCE.createCompareAction();
									compAction.setExpression1( callExp );
									
									final StringExpression strExpr = ExpressionsFactory.eINSTANCE.createStringExpression();
									strExpr.setExpressionString( valueExpr );
									compAction.setExpression2( strExpr );
	
									final CallActionExpression constrExp = ExpressionsFactory.eINSTANCE.createCallActionExpression();
									constrExp.setName( exprText );
									constrExp.getCallActions().add( compAction );
									itetator.remove();
									createdExpressions.add( constrExp );
								}
							}
						}
					}
					
					modelObject.getConstraintExpressions().addAll( createdExpressions );
				}
			}
		}
	}
	
	@Override
	public void migrateSourceDomain( final TGGRule p_newRule ) {
		migrateCreateModelObjectConstraints( p_newRule.getSourceDomain() );
	}

	@Override
	public void migrateCorrespondenceDomain( final TGGRule p_newRule ) {
	}

	@Override
	public void migrateTargetDomain( final TGGRule p_newRule ) {
		migrateCreateModelObjectConstraints( p_newRule.getTargetDomain() );
	}

	@Override
	public Set<String> getRulesToMigrate( final TGGRule p_templateRule ) {
		final Set<String> ruleNames = new HashSet<String>();
		ruleNames.add( p_templateRule.getName() );
		
		return ruleNames;
	}

	@Override
	public boolean isMigratorFor(TGGRule p_rule) {
		return true;
	}

	@Override
	public boolean isModifying() {
		return true;
	}
}
