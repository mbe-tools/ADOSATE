package fr.labsticc.adosate.adele2aadl.tooling.rulemigration.migrators;

import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.osate.aadl2.Aadl2Package;
import org.osate.aadl2.AccessCategory;
import org.osate.aadl2.ComponentCategory;
import org.topcased.adele.common.utils.AdeleCommonUtils;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;
import org.topcased.adele.model.ADELE_Features.ADELE_FeaturesPackage;

import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.tgg.ModelElement;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.SourceModelDomain;
import de.hpi.sam.tgg.TGGRule;
import de.hpi.sam.tgg.TargetModelDomain;
import fr.labsticc.adosate.adele2aadl.tooling.rulemigration.AbstractTGGRuleMigrator;
import fr.labsticc.framework.core.util.ReflectUtil;

public class AccessCategoryRuleMigrator extends AbstractTGGRuleMigrator {
	
	private static final String ACCESS_LIT = "Access"; 
	
	public AccessCategoryRuleMigrator() {
		super( RULE_LIT + WILDCARD_1_ANY_LIT + ACCESS_LIT + TO_LIT + WILDCARD_1_ANY_LIT + ACCESS_LIT + WILDCARD_0_ANY_LIT );
	}
	
	@Override
	public void migrateSourceDomain( final TGGRule p_newRule ) {
		final SourceModelDomain srcDomain = p_newRule.getSourceDomain();
		final AccessCategory accCat = findAccessCategory( p_newRule.getName() );
		
		for ( final ModelElement element : srcDomain.getModelElements() ) {
			if ( element instanceof ModelObject ) {
				final ModelObject modelObject = (ModelObject) element;
				
				if ( ( AdeleCommonUtils.ADELE_EXT + ACCESS_LIT ).equals( element.getName() ) ) {
					final EClass accessClass = (EClass) ADELE_FeaturesPackage.eINSTANCE.getEClassifier( capitalize( accCat.getName() ) + ACCESS_LIT );
					modelObject.setClassifier( accessClass );
					
					for ( final AttributeAssignment assignment : modelObject.getAttributeAssignments() ) {
						if ( ADELE_FeaturesPackage.Literals.ACCESS_DIRECTION.equals( assignment.getEStructuralFeature().getEType() ) ) {
							assignment.setEStructuralFeature( accessClass.getEStructuralFeature( assignment.getEStructuralFeature().getName() ) );
							
							break;
						}
					}
				}
				else if ( ( AdeleCommonUtils.ADELE_EXT + TYPE_LIT ).equals( element.getName() ) ) {
					final ComponentCategory compoCat = findComponentCategory( p_newRule.getName(), accCat );
					final EClass compoClass = (EClass) ADELE_ComponentsPackage.eINSTANCE.getEClassifier( capitalize( compoCat.getName() ) );
					modelObject.setClassifier( compoClass );
				}
			}
		}
	}

	@Override
	public void migrateCorrespondenceDomain( final TGGRule p_newRule ) {
//		final AccessCategory accCat = findAccessCategory( p_newRule.getName() );
//		final EClass accessClass = (EClass) Adele2aadlPackage.eINSTANCE.getEClassifier( CORR_CLASS_PREFIX_LIT + capitalize( accCat.getName() ) + ACCESS_LIT );
//		
//		final CorrespondenceDomain corrDomain = p_newRule.getCorrespondenceDomain();
//		
//		for ( final CorrespondenceElement element : corrDomain.getCorrespondenceElements() ) {
//			if ( ( CORR_NAME_PREFIX_LIT + ACCESS_LIT ).equals( element.getName() ) && element instanceof CorrespondenceNode ) {
//				( (CorrespondenceNode) element ).setClassifier( accessClass );
//				
//				break;
//			}
//		}
	}

	@Override
	public void migrateTargetDomain( final TGGRule p_newRule ) {
		final AccessCategory accCat = findAccessCategory( p_newRule.getName() );
		final EClass accessClass = (EClass) Aadl2Package.eINSTANCE.getEClassifier( capitalize( accCat.getName() ) + ACCESS_LIT );
		final ComponentCategory compoCat = findComponentCategory( p_newRule.getName(), accCat );
		final EClass compoClass = (EClass) Aadl2Package.eINSTANCE.getEClassifier( capitalize( compoCat.getName() ) + TYPE_LIT );
		
		final TargetModelDomain targetDomain = p_newRule.getTargetDomain();
		
		for ( final ModelElement element : targetDomain.getModelElements() ) {
			if ( element instanceof ModelObject ) {
				final ModelObject modelObj = (ModelObject) element;

				if ( ( AADL_LIT + ACCESS_LIT ).equals( element.getName() ) ) {
					modelObj.setClassifier( accessClass );
					
					for ( final AttributeAssignment attAss : modelObj.getAttributeAssignments() ) {
						if ( attAss.getEStructuralFeature().getName().endsWith( FEATURE_CLASS_LIT ) ) {
							final String accesTypeName = ReflectUtil.decapitalizePropertyName( accessClass.getName().replace( ACCESS_LIT, "" ) );
							final EStructuralFeature classFeat = accessClass.getEStructuralFeature( accesTypeName + FEATURE_CLASS_LIT );
							attAss.setEStructuralFeature( classFeat );
						}
					}
				}
				else if ( ( AADL_LIT + CLASSIFIER_LIT ).equals( element.getName() ) ) {
					final EClassifier accessTypeClass = Aadl2Package.eINSTANCE.getEClassifier( capitalize( accCat.getName() ) + SUBCOMPONENT_TYPE_LIT );
					modelObj.setClassifier( accessTypeClass );
				}
				else if ( ( AADL_LIT + TYPE_LIT ).equals( element.getName() ) ) {
					modelObj.setClassifier( compoClass );
				}
			}
			else if ( element instanceof ModelLink ) {
				final ModelLink modelLink = (ModelLink) element;
				
				if ( modelLink.getEReference().getName().endsWith( ACCESS_LIT ) ) {
					final String featName = OWNED_LIT + capitalize( accCat.getName() ) + ACCESS_LIT;
					final EReference accessRef = (EReference) compoClass.getEStructuralFeature( featName );
					modelLink.setEReference( accessRef );
				}
				else if ( modelLink.getEReference().getName().endsWith( FEATURE_CLASS_LIT ) ) {
					final String featName = accCat.getName() + FEATURE_CLASS_LIT;
					modelLink.setEReference( (EReference) accessClass.getEStructuralFeature( featName ) );
				}
			}
		}
	}
//
//	@Override
//	public boolean isMigratorFor(TGGRule p_rule) {
//		return p_rule.getName().endsWith( ACCESS_LIT ) || p_rule.getName().endsWith( ACCESS_LIT + WITH_CLASS_LIT );
//	}

	@Override
	public Set<String> getRulesToMigrate( final TGGRule p_templateRule ) {
		final Set<String> ruleNames = createRuleNamesSet();

		if ( isMigratorFor( p_templateRule ) ) {
			final String templateName = p_templateRule.getName();
			final AccessCategory templateAccessCat = findAccessCategory( templateName );
			final String accessNameToReplace = capitalize( templateAccessCat.getName() ) + ACCESS_LIT;
			final ComponentCategory templateCompoCat = findComponentCategory( templateName, templateAccessCat );
			final String compoNameToReplace = ACCESS_LIT + TO_LIT + capitalize( templateCompoCat.getName() );

			for ( final ComponentCategory compoCat: ComponentCategory.values() ) {
				final String capitCompoName = capitalize( compoCat.getName() );
				final EClass typeClass = (EClass) Aadl2Package.eINSTANCE.getEClassifier( capitCompoName + TYPE_LIT );
				
				for ( final AccessCategory accCat : AccessCategory.values() ) {
					final String capitAccessName = capitalize( accCat.getName() )+ ACCESS_LIT;
					
					if ( typeClass.getEStructuralFeature( OWNED_LIT + capitAccessName ) != null ) {
						String ruleName = templateName.replaceAll( accessNameToReplace, capitAccessName  );
						ruleName = ruleName.replaceFirst( compoNameToReplace, ACCESS_LIT + TO_LIT + capitCompoName );
						
						if ( !templateName.equals( ruleName ) ) {
							ruleNames.add( ruleName );
						}
					}
				}
			}
		}
		
		return ruleNames;
	}

	private AccessCategory findAccessCategory( final String p_ruleName ) {
		for ( final AccessCategory category : AccessCategory.values() ) {
			if ( p_ruleName.contains( capitalize( category.getName() ) + ACCESS_LIT ) ) {
				return category;
			}
		}
		
		return null;
	}

	private ComponentCategory findComponentCategory( 	final String p_ruleName,
														final AccessCategory p_accessCat ) {
		for ( final ComponentCategory category : ComponentCategory.values() ) {
			if ( p_ruleName.contains( ACCESS_LIT + TO_LIT + capitalize( category.getName() ) + capitalize( p_accessCat.getName() ) ) ) {
				return category;
			}
		}
		
		return null;
	}
}
