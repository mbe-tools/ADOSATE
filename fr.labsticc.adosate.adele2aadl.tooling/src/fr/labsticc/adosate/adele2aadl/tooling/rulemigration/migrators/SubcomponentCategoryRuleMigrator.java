package fr.labsticc.adosate.adele2aadl.tooling.rulemigration.migrators;

import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.osate.aadl2.Aadl2Package;
import org.osate.aadl2.ComponentCategory;
import org.topcased.adele.common.utils.AdeleCommonUtils;
import org.topcased.adele.model.ADELE_Components.ADELE_ComponentsPackage;

import de.hpi.sam.storyDiagramEcore.sdm.AttributeAssignment;
import de.hpi.sam.tgg.ModelElement;
import de.hpi.sam.tgg.ModelLink;
import de.hpi.sam.tgg.ModelObject;
import de.hpi.sam.tgg.TGGRule;
import fr.labsticc.adosate.adele2aadl.tooling.rulemigration.AbstractTGGRuleMigrator;
import fr.labsticc.framework.core.util.ReflectUtil;

public class SubcomponentCategoryRuleMigrator extends AbstractTGGRuleMigrator {
	
	private static final String IMPL_ABREV_LIT = "Impl";

	public SubcomponentCategoryRuleMigrator() {
		super( RULE_LIT + WILDCARD_1_ANY_LIT + TO_LIT + WILDCARD_1_ANY_LIT + SUBCOMPONENT_LIT + WILDCARD_0_ANY_LIT );
	}

	@Override
	public void migrateSourceDomain( final TGGRule p_newRule ) {
		final ComponentCategory subcompoCat = findSubcomponentCategory( p_newRule.getName() );
		final EClass compoClass = (EClass) ADELE_ComponentsPackage.eINSTANCE.getEClassifier( capitalize( subcompoCat.getName() ) );

		for ( final ModelElement element : p_newRule.getSourceDomain().getModelElements() ) {
			if ( element instanceof ModelObject ) {
				final ModelObject modelObject = (ModelObject) element;
				
				/*if ( ( ADELE_LIT + IMPL_LIT ).equals( element.getName() ) ) {
					final ComponentCategory compImplCat = findComponentImplCategory( p_newRule.getName() );
					final EClass compoImplClass = (EClass) ADELE_ComponentsPackage.eINSTANCE.getEClassifier( capitalize( compImplCat.getName() ) );
					modelObject.setClassifier( compoImplClass );
				}
				else*/ if ( ( AdeleCommonUtils.ADELE_EXT + SUBCOMPONENT_LIT ).equals( element.getName() ) ) {
					modelObject.setClassifier( compoClass );
				}
				else if ( ( AdeleCommonUtils.ADELE_EXT + SUBCOMPONENT_LIT + IMPL_ABREV_LIT ).equals( element.getName() ) ) {
					modelObject.setClassifier( compoClass );
				}
			}
		}
	}

	@Override
	public void migrateCorrespondenceDomain(TGGRule p_newRule) {
	}

	@Override
	public void migrateTargetDomain( final TGGRule p_newRule ) {
		//final ComponentCategory compImplCat = findComponentImplCategory( p_newRule.getName() );
		//final EClass compoImplClass = (EClass) Aadl2Package.eINSTANCE.getEClassifier( capitalize( compImplCat.getName() ) + IMPL_LIT );
		final ComponentCategory subcompoCat = findSubcomponentCategory( p_newRule.getName() );
		final EClass subcompoClass = (EClass) Aadl2Package.eINSTANCE.getEClassifier( capitalize( subcompoCat.getName() ) + SUBCOMPONENT_LIT );

		for ( final ModelElement element : p_newRule.getTargetDomain().getModelElements() ) {
			if ( element instanceof ModelObject ) {
				final ModelObject modelObject = (ModelObject) element;

				/*if ( ( AADL_LIT + IMPL_LIT ).equals( element.getName() ) ) {
					modelObject.setClassifier( compoImplClass );
				}
				else*/ if ( ( AADL_LIT + SUBCOMPONENT_LIT ).equals( element.getName() ) ) {
					modelObject.setClassifier( subcompoClass );
					
					for ( final AttributeAssignment attAss : modelObject.getAttributeAssignments() ) {
						if ( attAss.getEStructuralFeature().getName().endsWith( SUBCOMPONENT_TYPE_LIT ) ) {
							final String featName = ReflectUtil.decapitalizePropertyName( capitalize( subcompoCat.getName() ) ) + SUBCOMPONENT_TYPE_LIT;
							attAss.setEStructuralFeature( subcompoClass.getEStructuralFeature( featName ) );
							
							break;
						}
					}
				}
				else if ( ( AADL_LIT + SUBCOMPONENT_TYPE_LIT ).equals( element.getName() ) ) {
					final String subcompoTypeClassName;
					
					// TODO: Remove when spelling error in AADL meta-model is fixed
					if ( ComponentCategory.VIRTUAL_BUS == subcompoCat ) {
						subcompoTypeClassName = "VitualBus" + SUBCOMPONENT_TYPE_LIT;
					}
					else {
						subcompoTypeClassName = capitalize( subcompoCat.getName() ) + SUBCOMPONENT_TYPE_LIT;
					}
					
					final EClass subcompoTypeClass = (EClass) Aadl2Package.eINSTANCE.getEClassifier( subcompoTypeClassName );
					modelObject.setClassifier( subcompoTypeClass );
				}
//				else if ( ( AADL_LIT + REFINED_LIT + SUBCOMPONENT_LIT ).equals( element.getName() ) ) {
//					modelObject.setClassifier( subcompoClass );
//				}
//				else if ( ( AADL_LIT + REFINED_LIT + SUBCOMPONENT_TYPE_LIT ).equals( element.getName() ) ) {
//					modelObject.setClassifier( subcompoTypeClass );
//				}
			}
			else if ( element instanceof ModelLink ) {
				final ModelLink modelLink = (ModelLink) element;
				
				/*if ( modelLink.getEReference().getName().endsWith( SUBCOMPONENT_LIT ) ) {
					final String featName = OWNED_LIT + capitalize( subcompoCat.getName() ) + SUBCOMPONENT_LIT;
					final EReference subcompoRef = (EReference) compoImplClass.getEStructuralFeature( featName );
					modelLink.setEReference( subcompoRef );
				}
				else*/ if ( modelLink.getEReference().getName().endsWith( SUBCOMPONENT_TYPE_LIT ) ) {
					final String featName = ReflectUtil.decapitalizePropertyName( capitalize( subcompoCat.getName() ) ) + SUBCOMPONENT_TYPE_LIT;
					modelLink.setEReference( (EReference) subcompoClass.getEStructuralFeature( featName ) );
				}
			}
		}
	}

	@Override
	public Set<String> getRulesToMigrate( final TGGRule p_templateRule ) {
		final Set<String> ruleNames = createRuleNamesSet();

		if ( isMigratorFor( p_templateRule ) ) {
			final String templateName = p_templateRule.getName();
			final ComponentCategory templateSubcompoCat = findSubcomponentCategory( templateName );
			final String capitTemplateSubcompoCat = capitalize( templateSubcompoCat.getName() );

			for ( final ComponentCategory subcompoCat : ComponentCategory.values() ) {
				final String capitSubcompoCatName = capitalize( subcompoCat.getName() );

				String ruleName = templateName.replaceFirst( capitTemplateSubcompoCat, capitSubcompoCatName );
				ruleName = ruleName.replaceFirst( capitTemplateSubcompoCat + SUBCOMPONENT_LIT, capitSubcompoCatName + SUBCOMPONENT_LIT );
				
				if ( !templateName.equals( ruleName ) ) {
					ruleNames.add( ruleName );
				}
			}
		}
		
		return ruleNames;
	}

	protected ComponentCategory findComponentImplCategory( final String p_ruleName ) {
		int lengthMax = - 1;
		ComponentCategory foundCat = null;
		
		for ( final ComponentCategory category : ComponentCategory.values() ) {
			final String capitCatName = capitalize( category.getName() );
			final String searchStr = TO_LIT + capitCatName;

			if ( p_ruleName.contains( searchStr ) ) {
				final int length = searchStr.length();
				
				if ( length > lengthMax ) {
					lengthMax = length;
					foundCat = category;
				}
			}
		}
		
		return foundCat;
	}

	protected ComponentCategory findSubcomponentCategory( final String p_ruleName ) {
		int lengthMax = - 1;
		ComponentCategory foundCat = null;

		for ( final ComponentCategory category : ComponentCategory.values() ) {
			final String capitCatName = capitalize( category.getName() );
			final String searchStr = capitCatName + SUBCOMPONENT_LIT;

			if ( p_ruleName.contains( searchStr ) ) {
				final int length = searchStr.length();
				
				if ( length > lengthMax ) {
					lengthMax = length;
					foundCat = category;
				}
			}
		}
		
		return foundCat;
	}
}
