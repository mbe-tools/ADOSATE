package fr.labsticc.adosate.adele2aadl.tooling.rulemigration;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.handlers.HandlerUtil;

import de.hpi.sam.tgg.TGGRule;
import fr.labsticc.framework.emf.core.util.EMFUtil;

/**
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class MigrateTggRulesHandler extends AbstractHandler {
	
	private final TGGRulesMigrator rulesDuplicator;

	/**
	 * The constructor.
	 */
	public MigrateTggRulesHandler() {
		TGGRulesMigrator rulesDuplicatorTmp = null;
		
		try {
			rulesDuplicatorTmp = new TGGRulesMigrator();
		}
		catch( final Throwable p_th ) {
			p_th.printStackTrace();
		}
		
		rulesDuplicator = rulesDuplicatorTmp;
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	@Override
	public Object execute( final ExecutionEvent p_event )
	throws ExecutionException {
		final ISelection currentSelection = HandlerUtil.getCurrentSelection( p_event );
		final Collection<TGGRule> selectedRules = EMFUtil.selectedObjects( currentSelection, TGGRule.class );
		
		if ( !selectedRules.isEmpty() ) {
			final IEditorPart editorPart = HandlerUtil.getActiveEditor( p_event );

			final EditingDomain editingDomain;
			
			if ( editorPart == null ) {
				editingDomain = null;
			}
			else {
				if ( editorPart instanceof IEditingDomainProvider ) {
					editingDomain = ( (IEditingDomainProvider) editorPart ).getEditingDomain();
				}
				else {
					editingDomain = null;
				}
			}
			
			final WorkspaceModifyOperation operation = new WorkspaceModifyOperation() {
				
				@Override
				public void execute(IProgressMonitor monitor) 
				throws InvocationTargetException {
					monitor.beginTask( "Duplicating rules...", selectedRules.size() );
					rulesDuplicator.duplicateTggRules( editingDomain, selectedRules );
					
					try {
						selectedRules.iterator().next().eResource().save( null );
					}
					catch( final IOException p_ex ) {
						throw new InvocationTargetException( p_ex );
					}
				}
			};
			
			final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked( p_event );
			
			try {
				new ProgressMonitorDialog( window.getShell() ).run( true, true, operation );
			}
			catch( final InterruptedException p_ex ) {
			}
			catch( final InvocationTargetException p_ex ) {
				p_ex.printStackTrace();
			}
		}
		
		return null;
	}
}
