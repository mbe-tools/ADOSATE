/**
 */
package fr.labsticc.adosate.gmm.osategmm.provider;


import fr.labsticc.adosate.gmm.osategmm.AdeleGraph2SemConsistencyRelation;
import fr.labsticc.adosate.gmm.osategmm.OsategmmFactory;
import fr.labsticc.adosate.gmm.osategmm.OsategmmPackage;
import fr.labsticc.gmm.model.megamodel.MegamodelPackage;
import fr.labsticc.gmm.model.megamodel.provider.BinaryMetaModelRelatedConsistencyRelationItemProvider;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link fr.labsticc.adosate.gmm.osategmm.AdeleGraph2SemConsistencyRelation} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AdeleGraph2SemConsistencyRelationItemProvider
	extends BinaryMetaModelRelatedConsistencyRelationItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdeleGraph2SemConsistencyRelationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addDiagramMetaModelPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Diagram Meta Model feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDiagramMetaModelPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AdeleGraph2SemConsistencyRelation_diagramMetaModel_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AdeleGraph2SemConsistencyRelation_diagramMetaModel_feature", "_UI_AdeleGraph2SemConsistencyRelation_type"),
				 OsategmmPackage.Literals.ADELE_GRAPH2_SEM_CONSISTENCY_RELATION__DIAGRAM_META_MODEL,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This returns AdeleGraph2SemConsistencyRelation.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/AdeleGraph2SemConsistencyRelation"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((AdeleGraph2SemConsistencyRelation)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_AdeleGraph2SemConsistencyRelation_type") :
			getString("_UI_AdeleGraph2SemConsistencyRelation_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(MegamodelPackage.Literals.RELATION__OWNED_RELATION_POLICY,
				 OsategmmFactory.eINSTANCE.createAdosateUriRelBinRelPolicy()));

		newChildDescriptors.add
			(createChildParameter
				(MegamodelPackage.Literals.OBLIGATION_RELATION__OWNED_COVERAGE_POLICY,
				 OsategmmFactory.eINSTANCE.createAdeleOsateModelElementsCovPolicy()));
	}

}
