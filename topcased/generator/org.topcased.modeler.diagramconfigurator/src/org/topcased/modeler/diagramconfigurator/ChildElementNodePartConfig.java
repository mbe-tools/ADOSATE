/**
 */
package org.topcased.modeler.diagramconfigurator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Child Element Node Part Config</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.topcased.modeler.diagramconfigurator.ChildElementNodePartConfig#getChildElement <em>Child Element</em>}</li>
 *   <li>{@link org.topcased.modeler.diagramconfigurator.ChildElementNodePartConfig#getReference <em>Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.topcased.modeler.diagramconfigurator.DiagramconfiguratorPackage#getChildElementNodePartConfig()
 * @model
 * @generated
 */
public interface ChildElementNodePartConfig extends EObject {
	/**
	 * Returns the value of the '<em><b>Child Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Child Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child Element</em>' reference.
	 * @see #setChildElement(NodePartConfiguration)
	 * @see org.topcased.modeler.diagramconfigurator.DiagramconfiguratorPackage#getChildElementNodePartConfig_ChildElement()
	 * @model required="true"
	 * @generated
	 */
	NodePartConfiguration getChildElement();

	/**
	 * Sets the value of the '{@link org.topcased.modeler.diagramconfigurator.ChildElementNodePartConfig#getChildElement <em>Child Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Child Element</em>' reference.
	 * @see #getChildElement()
	 * @generated
	 */
	void setChildElement(NodePartConfiguration value);

	/**
	 * Returns the value of the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference</em>' reference.
	 * @see #setReference(EReference)
	 * @see org.topcased.modeler.diagramconfigurator.DiagramconfiguratorPackage#getChildElementNodePartConfig_Reference()
	 * @model
	 * @generated
	 */
	EReference getReference();

	/**
	 * Sets the value of the '{@link org.topcased.modeler.diagramconfigurator.ChildElementNodePartConfig#getReference <em>Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference</em>' reference.
	 * @see #getReference()
	 * @generated
	 */
	void setReference(EReference value);

} // ChildElementNodePartConfig
