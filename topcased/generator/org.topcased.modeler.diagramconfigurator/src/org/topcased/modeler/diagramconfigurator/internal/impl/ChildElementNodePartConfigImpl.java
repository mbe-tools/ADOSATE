/**
 */
package org.topcased.modeler.diagramconfigurator.internal.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.topcased.modeler.diagramconfigurator.ChildElementNodePartConfig;
import org.topcased.modeler.diagramconfigurator.DiagramconfiguratorPackage;
import org.topcased.modeler.diagramconfigurator.NodePartConfiguration;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Child Element Node Part Config</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.topcased.modeler.diagramconfigurator.internal.impl.ChildElementNodePartConfigImpl#getChildElement <em>Child Element</em>}</li>
 *   <li>{@link org.topcased.modeler.diagramconfigurator.internal.impl.ChildElementNodePartConfigImpl#getReference <em>Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ChildElementNodePartConfigImpl extends EObjectImpl implements ChildElementNodePartConfig {
	/**
	 * The cached value of the '{@link #getChildElement() <em>Child Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildElement()
	 * @generated
	 * @ordered
	 */
	protected NodePartConfiguration childElement;

	/**
	 * The cached value of the '{@link #getReference() <em>Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReference()
	 * @generated
	 * @ordered
	 */
	protected EReference reference;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChildElementNodePartConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DiagramconfiguratorPackage.Literals.CHILD_ELEMENT_NODE_PART_CONFIG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodePartConfiguration getChildElement() {
		if (childElement != null && childElement.eIsProxy()) {
			InternalEObject oldChildElement = (InternalEObject)childElement;
			childElement = (NodePartConfiguration)eResolveProxy(oldChildElement);
			if (childElement != oldChildElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DiagramconfiguratorPackage.CHILD_ELEMENT_NODE_PART_CONFIG__CHILD_ELEMENT, oldChildElement, childElement));
			}
		}
		return childElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodePartConfiguration basicGetChildElement() {
		return childElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChildElement(NodePartConfiguration newChildElement) {
		NodePartConfiguration oldChildElement = childElement;
		childElement = newChildElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiagramconfiguratorPackage.CHILD_ELEMENT_NODE_PART_CONFIG__CHILD_ELEMENT, oldChildElement, childElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReference() {
		if (reference != null && reference.eIsProxy()) {
			InternalEObject oldReference = (InternalEObject)reference;
			reference = (EReference)eResolveProxy(oldReference);
			if (reference != oldReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DiagramconfiguratorPackage.CHILD_ELEMENT_NODE_PART_CONFIG__REFERENCE, oldReference, reference));
			}
		}
		return reference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference basicGetReference() {
		return reference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReference(EReference newReference) {
		EReference oldReference = reference;
		reference = newReference;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiagramconfiguratorPackage.CHILD_ELEMENT_NODE_PART_CONFIG__REFERENCE, oldReference, reference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DiagramconfiguratorPackage.CHILD_ELEMENT_NODE_PART_CONFIG__CHILD_ELEMENT:
				if (resolve) return getChildElement();
				return basicGetChildElement();
			case DiagramconfiguratorPackage.CHILD_ELEMENT_NODE_PART_CONFIG__REFERENCE:
				if (resolve) return getReference();
				return basicGetReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DiagramconfiguratorPackage.CHILD_ELEMENT_NODE_PART_CONFIG__CHILD_ELEMENT:
				setChildElement((NodePartConfiguration)newValue);
				return;
			case DiagramconfiguratorPackage.CHILD_ELEMENT_NODE_PART_CONFIG__REFERENCE:
				setReference((EReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DiagramconfiguratorPackage.CHILD_ELEMENT_NODE_PART_CONFIG__CHILD_ELEMENT:
				setChildElement((NodePartConfiguration)null);
				return;
			case DiagramconfiguratorPackage.CHILD_ELEMENT_NODE_PART_CONFIG__REFERENCE:
				setReference((EReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DiagramconfiguratorPackage.CHILD_ELEMENT_NODE_PART_CONFIG__CHILD_ELEMENT:
				return childElement != null;
			case DiagramconfiguratorPackage.CHILD_ELEMENT_NODE_PART_CONFIG__REFERENCE:
				return reference != null;
		}
		return super.eIsSet(featureID);
	}

} //ChildElementNodePartConfigImpl
