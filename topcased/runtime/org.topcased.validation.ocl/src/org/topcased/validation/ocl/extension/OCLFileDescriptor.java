/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies), 
 *    Mathieu Garcia (Anyware Technologies), 
 *    Jacques Lescot (Anyware Technologies),
 *    Thomas Friol (Anyware Technologies)
 *    - initial API and implementation
 *******************************************************************************/
package org.topcased.validation.ocl.extension;

import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.Diagnostic;
import org.osgi.framework.Bundle;

/**
 * A descriptor of a link between a metamodel and an OCL file <br>
 * creation : 17 janv. 2006
 * 
 * @author <a href="mailto:david.sciamma@anyware-tech.com">David Sciamma</a>
 */
public class OCLFileDescriptor
{

    public static final String WARNING_TEXT = "WARNING";

    public static final String ERROR_TEXT = "ERROR";

    // Constants
    public static final String TAG_RULES = "rules";

    public static final String ATT_ID = "id";

    public static final String ATT_FILE = "file";

    public static final String ATT_URI = "uri";

    public static final String ATT_LEVEL = "level";

    // Values
    private String id;

    private String metamodelURI;

    private String file;
    
    private String relativePath;

    private int level;

    /**
     * Initialize the descriptor from the XML fragment of the extension
     * 
     * @param element The XML Fragment that describes the Validator
     * @throws CoreException if the xml fragment is not valid
     */
    public OCLFileDescriptor(IConfigurationElement element) throws CoreException
    {
        super();

        load(element);
    }

    /**
     * Extract infos from the XML fragment
     * 
     * @param configElement the XML fragment
     * @throws CoreException if the xml fragment is not valid
     */
    private void load(IConfigurationElement configElement) throws CoreException
    {
        id = configElement.getAttribute(ATT_ID);
        metamodelURI = configElement.getAttribute(ATT_URI);

        // Level default value
        String levelTxt = configElement.getAttribute(ATT_LEVEL);
        if (WARNING_TEXT.equals(levelTxt))
        {
            level = Diagnostic.WARNING;
        }
        else
        {
            level = Diagnostic.ERROR;
        }

        // Search OCL file
        relativePath = configElement.getAttribute(ATT_FILE);
        String bundleID = configElement.getDeclaringExtension().getContributor().getName();
        Bundle bundle = Platform.getBundle(bundleID);
        if (relativePath != null && bundle != null)
        {
            try
            {
                URL relativeURL = FileLocator.find(bundle, new Path(relativePath), null);
                URL fileURL = FileLocator.toFileURL(relativeURL);
                file = fileURL.getFile();
            }
            catch (IOException ioe)
            {
                throw new CoreException(
                        new Status(IStatus.ERROR, configElement.getContributor().getName(), IStatus.OK, "Invalid extension. The path to the OCl file is invalid : " + relativePath, ioe));
            }
        }

        // Sanity check.
        if (id == null || file == null || metamodelURI == null)
        {
            throw new CoreException(new Status(IStatus.ERROR, configElement.getContributor().getName(), IStatus.OK, "Invalid extension (missing id, file or uri): " + id, null));
        }
    }

    /**
     * @return Returns the OCL file path.
     */
    public String getFile()
    {
        return file;
    }

    /**
     * @return Returns the identifier of this association.
     */
    public String getId()
    {
        return id;
    }

    /**
     * @return Returns the level of the produced problems.
     */
    public int getLevel()
    {
        return level;
    }

    /**
     * @return Returns the metamodel URI.
     */
    public String getURI()
    {
        return metamodelURI;
    }
    
    /**
     * @return Returns the relative path from the bundle.
     */
    public String getRelativePath()
    {
        return relativePath;
    }

}
