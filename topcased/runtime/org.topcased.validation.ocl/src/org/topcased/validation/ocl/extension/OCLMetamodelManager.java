/*******************************************************************************
 * Copyright (c) 2005,2009 AIRBUS FRANCE. 
 * All rights reserved. This program and the
 * accompanying materials are made available under the terms of the Eclipse
 * Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: 
 * 		David Sciamma (Anyware Technologies) - initial API and implementation
 *      Sebastien Gabel (CS) - code clean-up, fix warnings 
 *      
 ******************************************************************************/
package org.topcased.validation.ocl.extension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.topcased.facilities.extensions.AbstractExtensionManager;
import org.topcased.validation.ocl.internal.OCLValidationPlugin;

/**
 * Class that stores the validator(s) registered with the <i>validators</i> extension point.
 * 
 * @author <a href="mailto:david.sciamma@anyware-tech.com">David Sciamma</a>
 */
public class OCLMetamodelManager extends AbstractExtensionManager
{

    private static final String METAMODELS_EXTENSION_POINT = "metamodels";

    /** the shared instance */
    private static OCLMetamodelManager manager;

    /**
     * A set that will only ever contain OCLFileDescriptors.
     */
    private SortedSet<OCLFileDescriptor> rules = new TreeSet<OCLFileDescriptor>(new Comparator<OCLFileDescriptor>()
    {
        public int compare(OCLFileDescriptor o1, OCLFileDescriptor o2)
        {
            String id1 = o1.getId();
            String id2 = o2.getId();
            return id1.compareTo(id2);
        }
    });

    /**
     * Basic constructor
     */
    private OCLMetamodelManager()
    {
        super(OCLValidationPlugin.getId() + "." + METAMODELS_EXTENSION_POINT);

        readRegistry();
    }

    /**
     * Get the shared instance.
     * 
     * @return the validators manager
     */
    public static OCLMetamodelManager getInstance()
    {
        if (manager == null)
        {
            manager = new OCLMetamodelManager();
        }

        return manager;
    }

    /**
     * Find a descriptor in the registry.
     * 
     * @param id the searched validator id
     * @return the OCLFile Descriptor or <code>null</code> if not found
     */
    public OCLFileDescriptor find(String id)
    {
        for (Iterator<OCLFileDescriptor> itr = rules.iterator(); itr.hasNext();)
        {
            OCLFileDescriptor desc = itr.next();
            if (id.equals(desc.getId()))
            {
                return desc;
            }
        }
        return null;
    }

    /**
     * @see org.topcased.facilities.extensions.AbstractExtensionManager#addExtension(org.eclipse.core.runtime.IExtension)
     */
    @Override
    protected void addExtension(IExtension extension)
    {

        IConfigurationElement[] elements = extension.getConfigurationElements();
        for (int i = 0; i < elements.length; i++)
        {
            IConfigurationElement confElt = elements[i];
            try
            {
                if (OCLFileDescriptor.TAG_RULES.equals(confElt.getName()))
                {
                    OCLFileDescriptor descriptor = new OCLFileDescriptor(confElt);
                    rules.add(descriptor);
                }
            }
            catch (CoreException ce)
            {
                OCLValidationPlugin.log(ce);
            }
        }
    }

    /**
     * @see org.topcased.facilities.extensions.AbstractExtensionManager#removeExtension(org.eclipse.core.runtime.IExtension)
     */
    @Override
    protected void removeExtension(IExtension extension)
    {
        IConfigurationElement[] elements = extension.getConfigurationElements();
        for (IConfigurationElement confElt : elements)
        {
            if (OCLFileDescriptor.TAG_RULES.equals(confElt.getName()))
            {
                String id = confElt.getAttribute(OCLFileDescriptor.ATT_ID);
                OCLFileDescriptor descriptor = find(id);
                rules.remove(descriptor);
            }
        }
    }

    /**
     * Get an enumeration of OCL File descriptors.
     * 
     * @return The registered OCL files
     */
    public OCLFileDescriptor[] getFiles()
    {
        return rules.toArray(new OCLFileDescriptor[rules.size()]);
    }

    /**
     * Get the enumeration of OCL files registered for the given metamodel URI
     * 
     * @param uri the metamodel URI
     * @return the list of OCL files
     */
    public OCLFileDescriptor[] getFiles(String uri)
    {
        Collection<OCLFileDescriptor> filteredFiles = new ArrayList<OCLFileDescriptor>();
        for (Iterator<OCLFileDescriptor> it = rules.iterator(); it.hasNext();)
        {
            OCLFileDescriptor desc = it.next();
            if (uri.equals(desc.getURI()))
            {
                filteredFiles.add(desc);
            }
        }
        return filteredFiles.toArray(new OCLFileDescriptor[filteredFiles.size()]);
    }
}
