/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies),
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *    Tristan Faure (Atos Origin)
 *******************************************************************************/
package org.topcased.validation.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.topcased.validation.core.internal.ValidationPlugin;

/**
 * This class find the validation markers and return a multiple status containing all the messages
 * 
 * @author <a href="david.sciamma@anyware-tech.com">David Sciamma</a>
 */
public class MarkerUtil
{
    /** Marker ID */
    public static final String MARKER_ID = EValidator.MARKER;

    public static final String CONSTANT_FOR_SYNCHRONIZATION = "synchronization_with_other_process_for_saving";

//    static IMarkerCache cache = new MarkerCache();

//    static
//    {
//        // register this class as a workspace listener
//        // in a static way is necesary as the class shall be loaded to fill the cache
//        ResourcesPlugin.getWorkspace().addResourceChangeListener(new MarkerResourceChangeListener());
//    }

    /**
     * This class analyses modifications in the workspace and manage cache integrity (user deletes markers)
     * 
     * @author tfaure
     * 
     */
//    static class MarkerResourceChangeListener implements IResourceChangeListener
//    {
//        public void resourceChanged(IResourceChangeEvent event)
//        {
//            if (event != null && event.getDelta() != null && event.getDelta().getKind() == IResourceDelta.CHANGED)
//            {
//                IResourceDeltaVisitor visitor = new IResourceDeltaVisitor()
//                {
//                    public boolean visit(IResourceDelta delta)
//                    {
//                        // only interested in changed resources (not added or removed)
//                        // only interested in content changes
//                        if ((delta.getFlags() & IResourceDelta.MARKERS) != 0)
//                        {
//                            IMarkerDelta[] deltas = delta.getMarkerDeltas();
//                            for (IMarkerDelta d : deltas)
//                            {
//                                IMarker m = d.getMarker();
//                                if ((!m.exists() && d.getKind() == IResourceDelta.REMOVED) || d.getKind() == IResourceDelta.CHANGED)
//                                {
//                                    cache.dropMarkerEntry(m);
//                                }
//                                if (d.getKind() == IResourceDelta.ADDED || d.getKind() == IResourceDelta.CHANGED)
//                                {
//                                    try
//                                    {
//                                        Object attribute = m.getAttribute(EValidator.URI_ATTRIBUTE);
//                                        if (attribute != null)
//                                        {
//                                            cache.addEntry(m.getResource().getFullPath(), getURI(attribute.toString()), m,true);
//                                        }
//                                    }
//                                    catch (CoreException e)
//                                    {
//                                    }
//                                }
//                            }
//                        }
//                        return true;
//                    }
//
//                };
//                try
//                {
//                    event.getDelta().accept(visitor);
//                }
//                catch (CoreException e)
//                {
//                }
//            }
//        }
//    }

    /**
     * Get an IStatus from the markers associated with the given EObject
     * 
     * @param object the EObject
     * @param reportChildrenStatus indicate whether the status of children elements should be reported
     * @return a MultiStatus composed by the list of status from markers
     */
    public static IStatus getStatus(EObject object, boolean reportChildrenStatus)
    throws CoreException {
        MultiStatus status = new MultiStatus(ValidationPlugin.getId(), IStatus.OK, "Validation Problems", null);
        
        // DB: Check that the resource exists to avoid exceptions
        final Resource objRes = object.eResource();
        
        if ( objRes != null && new ExtensibleURIConverterImpl().exists( objRes.getURI(), null ) ) {
	        for ( final IMarker marker : getMarkers( object ) ) {
	            final IStatus markerStatus = getStatusForMarker( marker );
	           
	            if ( markerStatus != null ) {
	                status.add(markerStatus);
	            }
	        }
        }

        return status;
    }

    /**
     * Method to init marker cache. This methods must be called before any graphical operation
     * 
     * @param set, the resource set
     */
//    public static void initMakers(ResourceSet set)
//    {
//        for (Resource r : set.getResources())
//        {
//            IMarker[] list;
//            try
//            {
//                IFile res = getFile(r);
//                if (res != null)
//                {
//                    list = res.findMarkers(MARKER_ID, true, IResource.DEPTH_ZERO);
//                    for (IMarker m : list)
//                    {
//                        if (m.exists())
//                        {
//                            EObject e = set.getEObject(URI.createURI((String) m.getAttribute(EValidator.URI_ATTRIBUTE)), false);
//                            if (e != null)
//                            {
//                                // the parent resolution has to be managed
//                                manageMap(getFile(e.eResource()), e, m,true);
//                            }
//                        }
//                    }
//                }
//            }
//            catch (CoreException e)
//            {
//            }
//        }
//    }

    /**
     * Returns the markers associated with the given EObject, attribute reportChildrenStatus is no longer available
     * 
     * @param object the EObject
     * @return the list of markers
     * @deprecated
     */
//    private static IMarker[] getMarkers(EObject object, boolean reportChildrenStatus)
//    throws CoreException {
//        return getMarkers(object);
//    }

    /**
     * Returns the markers associated with the given EObject
     * 
     * @param object
     * @return the list of markers
     */
    private static IMarker[] getMarkers(EObject object) 
    throws CoreException {
//        try {
            //TopcasedSynchronizerUtil.eINSTANCE.blockOrTakeAToken(CONSTANT_FOR_SYNCHRONIZATION);
        	
            // the markers are retrieved only by cache it is a faster way
            //return cache.getMarkers(object);
		final Collection<IMarker> markers = new ArrayList<IMarker>();
        	
    	if ( object.eResource() != null ) {
    		final IResource ideRes = getFile( object.eResource() );
    		
    		if ( ideRes != null ) {
	    		final URI objUri = EcoreUtil.getURI( object );
	
	            for ( final IMarker marker : ideRes.findMarkers(MARKER_ID, true, IResource.DEPTH_ZERO ) ) {
	                if ( marker.exists() && objUri.equals( marker.getAttribute( EValidator.URI_ATTRIBUTE) ) ) {
	                	markers.add( marker );
	                }
	            }
    		}
    	}
    	
    	return markers.toArray( new IMarker[ markers.size() ] );
//        }
//        finally
//        {
//            // in any case we release the token to not block other process
//            TopcasedSynchronizerUtil.eINSTANCE.releaseToken(CONSTANT_FOR_SYNCHRONIZATION);
//        }
    }

    /**
     * Transform a marker into an IStatus
     * 
     * @param marker The EMF marker
     * @return the IStatus or <code>null</code> if no marker is found
     */
    private static IStatus getStatusForMarker(IMarker marker)
    {
        IStatus result = null;
        if (marker.exists())
        {
            try
            {
                Object severityAttribute = marker.getAttribute(IMarker.SEVERITY);
                String message = (String) marker.getAttribute(IMarker.MESSAGE);
                if (severityAttribute != null && severityAttribute instanceof Integer)
                {
                    if (((Integer) severityAttribute).intValue() == IMarker.SEVERITY_ERROR)
                    {
                        result = new Status(IStatus.ERROR, ValidationPlugin.getId(), IStatus.OK, message, null);
                    }
                    else if (((Integer) severityAttribute).intValue() == IMarker.SEVERITY_WARNING)
                    {
                        result = new Status(IStatus.WARNING, ValidationPlugin.getId(), IStatus.OK, message, null);
                    }
                }
            }
            catch (CoreException ce)
            {
                ValidationPlugin.log(ce);
                // Don't display a message box to increase usability
            }
        }
        return result;
    }

    /**
     * Return the IFile associated with the given resource
     * 
     * @param resource the EMF resource
     * @return the containing IFile or <code>null</code> if the resource is not an IFile
     */
    public static IFile getFile(Resource resource)
    {
    	if (resource != null ) {//&& resource.getResourceSet() != null)
    		final IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
    		final URI resUri = resource.getURI();
    		
    		if ( resUri.isPlatform() ) {
    			return workspaceRoot.getFile( new Path( resUri.toPlatformString( true ) ) );
    		}

    		if ( resUri.isFile() ) {
    			return workspaceRoot.getFileForLocation( new Path( resUri.toFileString() ) );
    		}
    		
    		throw new IllegalArgumentException( "Resource scheme not managed: " + resUri.scheme() );
//    	{
//    		URI uri = resource.getURI();
//    		uri = resource.getResourceSet().getURIConverter().normalize(uri);
//    		String scheme = uri.scheme();
//    		if ("platform".equals(scheme) && uri.segmentCount() > 1 && "resource".equals(uri.segment(0)))
//    		{
//    			StringBuffer platformResourcePath = new StringBuffer();
//    			for (int j = 1; j < uri.segmentCount(); ++j)
//    			{
//    				platformResourcePath.append('/');
//    				platformResourcePath.append(uri.segment(j));
//    			}
//    			return ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(platformResourcePath.toString()));
//    		}
    	}

    	return null;
    }

    /**
     * Delete the markers according to a resource Set (the resource set analysed)
     * 
     * @param resourceSet
     * @throws CoreException
     */
    public static void deleteMarkers(ResourceSet resourceSet) throws CoreException
    {
        Set<String> resourcesUri = new HashSet<String>();
        addAll(resourceSet.getResources(), resourcesUri);
        for (String uri : resourcesUri)
        {
            IFile theFile = MarkerUtil.getFile(resourceSet.getResource(URI.createURI(uri), false));
            if (theFile != null)
            {
                theFile.deleteMarkers(EValidator.MARKER, true, IResource.DEPTH_ZERO);
                //cache.remove(theFile.getFullPath());
            }
        }
    }

    /**
     * Delete entries in the cache corresponding to the resource r
     * 
     * @param r
     */
//    public static void clearMarkerCache(Resource r)
//    {
//        IFile aFile = MarkerUtil.getFile(r);
//        if (aFile != null)
//        {
//            cache.remove(aFile.getFullPath());
//        }
//    }

    /**
     * Add all the resource uris of resources to resourcesUri set
     * 
     * @param resources
     * @param resourcesUri
     */
    public static void addAll(List<Resource> resources, Set<String> resourcesUri)
    {
        for (int i = 0; i < resources.size(); i++)
        {
            resourcesUri.add(resources.get(i).getURI().toString());
        }
    }

    /**
     * This is a temporary method copied from the EMF EclipseResourceUtil to keep compatibility from 3.2M6 to 3.2RC4.<br>
     * It should be removed in the next versions
     * 
     * @param file
     * @param diagnostic
     * @deprecated
     */
    public static void createMarkers(IResource file, Diagnostic diagnostic)
    {
        // two markers are created, one for TOPCASED another for emf
        // we keep parameters to prevent regressions
        EObject eObject = null;
        List< ? > data = diagnostic.getData();
        if (!data.isEmpty())
        {
            Object target = data.get(0);
            if (target instanceof EObject)
            {
                eObject = (EObject) target;
            }
        }
        if (eObject != null && eObject.eResource() != null )
        {
            //IMarker marker = null;
            if (diagnostic.getChildren().isEmpty()) {
                try {
                    /*marker = */createMarker(getFile(eObject.eResource()), diagnostic, eObject, EcoreUtil.getURI(eObject).toString(), diagnostic.getMessage());
                }
                catch (CoreException exception)
                {
                    ValidationPlugin.log(exception);
                }
            }
            else
            {
                String parentMessage = diagnostic.getMessage() + ". ";
                for (Diagnostic childDiagnostic : diagnostic.getChildren()) {
                    try {
                       /* marker = */createMarker(getFile(eObject.eResource()), childDiagnostic, eObject, EcoreUtil.getURI(eObject).toString(), parentMessage + childDiagnostic.getMessage());
                    }
                    catch (CoreException e)
                    {
                        ValidationPlugin.log(e);
                    }
                }
            }
        }
    }

//    private static void manageMap(IResource file, EObject eObject, IMarker marker, boolean manageParentResolution)
//    {
//        cache.addEntry(file.getFullPath(), eObject, marker,manageParentResolution);
//    }

    private static IMarker createMarker(IResource file, Diagnostic diagnostic, EObject eObject, String uri, String message) throws CoreException
    {
        IMarker marker = file.createMarker(EValidator.MARKER);
        int severity = diagnostic.getSeverity();
        if (severity < Diagnostic.WARNING)
        {
            marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_INFO);
        }
        else if (severity < Diagnostic.ERROR)
        {
            marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_WARNING);
        }
        else
        {
            marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
        }
        marker.setAttribute(IMarker.MESSAGE, message);
        if (eObject != null)
        {
            marker.setAttribute(EValidator.URI_ATTRIBUTE, uri);
        }
        return marker;

    }

    public static String getURI(EObject eObject)
    {
        URI uriFromResource = EcoreUtil.getURI(eObject);
        return getURI(uriFromResource);
    }

//    private static String getURI(String uriFromResource)
//    {
//        return getURI(URI.createURI(uriFromResource));
//    }

    private static String getURI(URI uriFromResource)
    {
        String uri;
        if (uriFromResource.fragment().startsWith("/"))
        {
            uri = uriFromResource.toString();
        }
        else
        {
            // unique id
            uri = uriFromResource.fragment();
        }
        return uri;
    }
    
}
