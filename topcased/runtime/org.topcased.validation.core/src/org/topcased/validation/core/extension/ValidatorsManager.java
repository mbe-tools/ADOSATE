/*******************************************************************************
 * Copyright (c) 2005,2009 AIRBUS FRANCE. 
 * All rights reserved. This program and the
 * accompanying materials are made available under the terms of the Eclipse
 * Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: 
 * 		David Sciamma (Anyware Technologies) - initial API and implementation
 *      Sebastien Gabel (CS) - code clean-up, fix warnings
 *      
 ******************************************************************************/
package org.topcased.validation.core.extension;

import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.topcased.facilities.extensions.AbstractExtensionManager;
import org.topcased.validation.core.internal.ValidationPlugin;

/**
 * Class that stores the validators registered with the <i>validators</i> extension point.
 * 
 * @author <a href="mailto:david.sciamma@anyware-tech.com">David Sciamma</a>
 */
public class ValidatorsManager extends AbstractExtensionManager
{
    private static final String VALIDATORS_EXTENSION_POINT = "validators";

    /** the shared instance */
    private static ValidatorsManager manager;

    /**
     * A set that will only ever contain ValidatorDescriptors.
     */
    private SortedSet<ValidatorDescriptor> validators = new TreeSet<ValidatorDescriptor>(new Comparator<ValidatorDescriptor>()
    {
        public int compare(ValidatorDescriptor o1, ValidatorDescriptor o2)
        {
            String id1 = o1.getId();
            String id2 = o2.getId();
            return id1.compareTo(id2);
        }
    });

    /**
     * Basic constructor
     */
    private ValidatorsManager()
    {
        super(ValidationPlugin.getId() + "." + VALIDATORS_EXTENSION_POINT);

        readRegistry();
    }

    /**
     * Get the shared instance.
     * 
     * @return the validators manager
     */
    public static ValidatorsManager getInstance()
    {
        if (manager == null)
        {
            manager = new ValidatorsManager();
        }

        return manager;
    }

    /**
     * Find a descriptor in the registry.
     * 
     * @param id the searched validator id
     * @return the validator or <code>null</code> if not found
     */
    public ValidatorDescriptor find(String id)
    {
        Iterator<ValidatorDescriptor> itr = validators.iterator();
        while (itr.hasNext())
        {
            ValidatorDescriptor desc = itr.next();
            if (id.equals(desc.getId()))
            {
                return desc;
            }
        }
        return null;
    }

    /**
     * Get an enumeration of validator descriptors.
     * 
     * @return The registered validators
     */
    public ValidatorDescriptor[] getValidators()
    {
        return validators.toArray(new ValidatorDescriptor[validators.size()]);
    }

    /**
     * @see org.topcased.facilities.extensions.AbstractExtensionManager#addExtension(org.eclipse.core.runtime.IExtension)
     */
    @Override
    protected void addExtension(IExtension extension)
    {
        for (IConfigurationElement confElt : extension.getConfigurationElements())
        {
            try
            {
                if (ValidatorDescriptor.TAG_VALIDATOR.equals(confElt.getName()))
                {
                    ValidatorDescriptor descriptor = new ValidatorDescriptor(confElt);
                    validators.add(descriptor);
                }
            }
            catch (CoreException ce)
            {
                ValidationPlugin.log(ce);
            }
        }
    }

    /**
     * @see org.topcased.facilities.extensions.AbstractExtensionManager#removeExtension(org.eclipse.core.runtime.IExtension)
     */
    @Override
    protected void removeExtension(IExtension extension)
    {
        for (IConfigurationElement confElt : extension.getConfigurationElements())
        {
            if (ValidatorDescriptor.TAG_VALIDATOR.equals(confElt.getName()))
            {
                String id = confElt.getAttribute(ValidatorDescriptor.ATT_ID);
                ValidatorDescriptor descriptor = find(id);
                validators.remove(descriptor);
            }
        }
    }

}
