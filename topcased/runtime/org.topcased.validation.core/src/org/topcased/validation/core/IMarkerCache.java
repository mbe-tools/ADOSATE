/*******************************************************************************
 * Copyright (c) 2005,2010 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Tristan Faure (Atos Origin Integration)
 *******************************************************************************/
package org.topcased.validation.core;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.ecore.EObject;

/**
 * Contract for handling markers in TOPCASED
 * @author tfaure
 *
 */
public interface IMarkerCache
{

    /**
     * Returns true if the cache is empty
     * @return
     */
    boolean isEmpty();

    /**
     * Remove in the cache the entry corresponding to the marker
     * @param m, the marker
     */
    void dropMarkerEntry(IMarker m);

    /**
     * Returns the markers associated to an eobject
     * @param object
     * @return
     */
    IMarker[] getMarkers(EObject object);

    /**
     * Remove all the entries corresponding to a path
     * @param fullPath
     */
    void remove(IPath fullPath);

    /**
     * Add an entry for the marker 
     * @param fullPath
     * @param eObject
     * @param marker
     */
    void addEntry(IPath fullPath, EObject eObject, IMarker marker);
    
    /**
     * Add an entry for the marker 
     * @param fullPath
     * @param eObject
     * @param marker
     * @param manageParentResolution
     */
    void addEntry(IPath fullPath, EObject eObject, IMarker marker, boolean manageParentResolution);

    /**
     * Add an entry for the marker 
     * @param fullPath
     * @param uriEObject, the uri of the eobject
     * @param marker
     */
    void addEntry(IPath path, String uriEObject, IMarker marker);
    
    /**
     * Add an entry for the marker 
     * @param fullPath
     * @param uriEObject, the uri of the eobject
     * @param marker
     * @param manageParentResolution
     */
    void addEntry(IPath path, String uriEObject, IMarker marker, boolean manageParentResolution);
    
}
