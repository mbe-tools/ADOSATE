/*******************************************************************************
 * Copyright (c) 2005,2010 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Tristan Faure (Atos Origin Integration)
 *******************************************************************************/
package org.topcased.validation.core;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

/**
 * Default Implementation of {@link IMarkerCache}
 * 
 * @author tfaure
 * 
 */
public class MarkerCache implements IMarkerCache
{
    Map<IPath, Map<String, Set<IMarker>>> markers = new ConcurrentHashMap<IPath, Map<String, Set<IMarker>>>();

    private Map<IMarker, List<Couple>> triples = new ConcurrentHashMap<IMarker, List<Couple>>();

    /*
     * (non-Javadoc)
     * 
     * @see org.topcased.validation.core.IMarkerCache#dropMarkerEntry(org.eclipse.core.resources.IMarker)
     */
    public void dropMarkerEntry(IMarker m)
    {
        IResource resource = m.getResource();
        if (resource != null)
        {
            IPath fullPath = resource.getFullPath();
            Map<String, Set<IMarker>> map = markers.get(fullPath);
            if (map != null)
            {
                for (String s : map.keySet())
                {
                    Set<IMarker> set = map.get(s);
                    for (Iterator<IMarker> i = set.iterator(); i.hasNext();)
                    {
                        IMarker tmp = i.next();
                        // we can only compare IDs
                        if (m.getId() == tmp.getId())
                        {
                            i.remove();
                        }
                    }
                    if (set.isEmpty())
                    {
                        map.remove(s);
                    }
                }
                if (map.isEmpty())
                {
                    markers.remove(fullPath);
                }
                if (markers.isEmpty())
                {
                    triples.clear();
                }
            }
        }
        triples.remove(m);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.topcased.validation.core.IMarkerCache#isEmpty()
     */
    public boolean isEmpty()
    {
        return markers.isEmpty();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.topcased.validation.core.IMarkerCache#getMarkers(org.eclipse.emf.ecore.EObject)
     */
    public IMarker[] getMarkers(EObject object)
    {
        updateCache(object);
        List<IMarker> markerList = new LinkedList<IMarker>();
        if (object != null)
        {
            IFile res = MarkerUtil.getFile(object.eResource());
            if (res != null)
            {
                Map<String, Set<IMarker>> map = markers.get(res.getFullPath());
                if (map != null)
                {
                    Set<IMarker> list = map.get(MarkerUtil.getURI(object));
                    if (list != null)
                    {
                        for (Iterator<IMarker> i = list.iterator(); i.hasNext();)
                        {
                            IMarker m = i.next();
                            if (m.exists())
                            {
                                markerList.add(m);
                            }
                            else
                            {
                                // clean cache when markers are no longer available
                                i.remove();
                            }
                        }
                    }
                }
            }
        }
        return markerList.toArray(new IMarker[markerList.size()]);
    }

    /**
     * Update the cache to manage tree hierarchy
     * 
     * @param object
     */
    protected void updateCache(EObject object)
    {
        if (object != null && object.eResource() != null && object.eResource().getResourceSet() != null)
        {
            ResourceSet set = object.eResource().getResourceSet();
            for (Iterator<IMarker> im = triples.keySet().iterator(); im.hasNext();)
            {
                IMarker m = im.next();
                if (!m.exists())
                {
                    im.remove();
                }
                else
                {
                    List<Couple> l = triples.get(m);
                    if (l != null)
                    {
                        for (Iterator<Couple> i = l.iterator(); i.hasNext();)
                        {
                            Couple t = i.next();
                            Resource r = set.getResource(URI.createPlatformResourceURI(t.iPath.toString(), true), false);
                            if (r != null)
                            {
                                EObject e = r.getEObject(t.uri);
                                if (e != null)
                                {
                                    EObject parent = e;
                                    while (parent.eContainer() != null)
                                    {
                                        parent = parent.eContainer();
                                        addEntry(MarkerUtil.getFile(parent.eResource()).getFullPath(), MarkerUtil.getURI(parent), m, false);
                                    }
                                    i.remove();
                                }
                            }
                        }
                        if (l.isEmpty())
                        {
                            im.remove();
                        }
                    }
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.topcased.validation.core.IMarkerCache#remove(org.eclipse.core.runtime.IPath)
     */
    public void remove(IPath fullPath)
    {
        Map<String, Set<IMarker>> map = markers.remove(fullPath);
        if (map != null)
        {
            map.clear();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.topcased.validation.core.IMarkerCache#addEntry(org.eclipse.core.runtime.IPath,
     * org.eclipse.emf.ecore.EObject, org.eclipse.core.resources.IMarker)
     */
    public void addEntry(IPath path, EObject eObject, IMarker marker)
    {
        String uri = MarkerUtil.getURI(eObject);
        addEntry(path, uri, marker, false);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.topcased.validation.core.IMarkerCache#addEntry(org.eclipse.core.runtime.IPath, java.lang.String,
     * org.eclipse.core.resources.IMarker)
     */
    public void addEntry(IPath path, String uriEObject, IMarker marker)
    {
        addEntry(path, uriEObject, marker, false);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.topcased.validation.core.IMarkerCache#addEntry(org.eclipse.core.runtime.IPath,
     * org.eclipse.emf.ecore.EObject, org.eclipse.core.resources.IMarker, boolean)
     */
    public void addEntry(IPath fullPath, EObject eObject, IMarker marker, boolean manageParentResolution)
    {
        String uri = MarkerUtil.getURI(eObject);
        addEntry(fullPath, uri, marker, manageParentResolution);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.topcased.validation.core.IMarkerCache#addEntry(org.eclipse.core.runtime.IPath, java.lang.String,
     * org.eclipse.core.resources.IMarker, boolean)
     */
    public void addEntry(IPath path, String uriEObject, IMarker marker, boolean manageParentResolution)
    {
        Map<String, Set<IMarker>> map = markers.get(path);
        if (map == null)
        {
            map = new ConcurrentHashMap<String, Set<IMarker>>();
            markers.put(path, map);
        }
        Set<IMarker> list = map.get(uriEObject);
        if (list == null)
        {
            list = new HashSet<IMarker>();
            map.put(uriEObject, list);
        }
        list.add(marker);
        if (manageParentResolution)
        {
            List<Couple> couples = triples.get(marker);
            if (couples == null)
            {
                couples = new LinkedList<MarkerCache.Couple>();
                triples.put(marker, couples);
            }
            couples.add(new Couple(path, uriEObject));
        }
    }

    private class Couple
    {
        IPath iPath = null;

        String uri = null;

        public Couple(IPath path, String uriEObject)
        {
            iPath = path;
            uri = uriEObject;
        }
    }
}
