/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies)
 *    - initial API and implementation
 *******************************************************************************/
package org.topcased.validation.core.extension;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.topcased.validation.core.IValidator;

/**
 * Class that describes a validator
 * 
 * @author <a href="mailto:david.sciamma@anyware-tech.com">David Sciamma</a>
 */
public class ValidatorDescriptor
{

    // Constants

    public static final String TAG_VALIDATOR = "validator";

    public static final String ATT_ID = "id";

    public static final String ATT_NAME = "name";

    public static final String ATT_CLASS = "class";

    public static final String ATT_URI = "uri";

    public static final String TAG_DESCRIPTION = "description";

    // Values
    private String id;

    private String name;

    private String uri;

    private String description;

    private IConfigurationElement configElement;

    /**
     * Initialize the descriptor from the XML fragment of the extension
     * 
     * @param element The XML Fragment that describes the Validator
     * @throws CoreException if the xml fragment is not valid
     */
    ValidatorDescriptor(IConfigurationElement element) throws CoreException
    {
        super();
        configElement = element;

        load();
    }

    private void load() throws CoreException
    {
        id = configElement.getAttribute(ATT_ID);
        name = configElement.getAttribute(ATT_NAME);
        String clazz = configElement.getAttribute(ATT_CLASS);
        uri = configElement.getAttribute(ATT_URI);

        // Sanity check.
        if (id == null || name == null || clazz == null)
        {
            throw new CoreException(new Status(IStatus.ERROR, configElement.getContributor().getName(), IStatus.OK, "Invalid extension (missing id, label or class name): " + id, null));
        }

        IConfigurationElement[] desc = configElement.getChildren(TAG_DESCRIPTION);
        if (desc.length > 0)
        {
            description = desc[0].getValue();
        }
    }

    /**
     * Get the validator associated with this extension
     * 
     * @return the validator or <code>null</code> if the
     * @throws CoreException
     */
    public IValidator getValidator() throws CoreException
    {
        Object validator = configElement.createExecutableExtension(ATT_CLASS);
        if (!(validator instanceof IValidator))
        {
            throw new CoreException(new Status(IStatus.ERROR, configElement.getContributor().getName(), IStatus.OK, "Invalid class name for extension : " + getId(), null));
        }

        return (IValidator) validator;
    }

    /**
     * 
     * @return String
     */
    public String getId()
    {
        return id;
    }

    /**
     * 
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * 
     * @return String
     */
    public String getName()
    {
        return name;
    }

    /**
     * 
     * @return String
     */
    public String getURI()
    {
        return uri;
    }
}
