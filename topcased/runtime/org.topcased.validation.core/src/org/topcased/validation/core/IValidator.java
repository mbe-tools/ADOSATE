/*******************************************************************************
 * Copyright (c) 2005,2010 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies), 
 *    Mathieu Garcia (Anyware Technologies), 
 *    Jacques Lescot (Anyware Technologies),
 *    Thomas Friol (Anyware Technologies)
 *    - initial API and implementation
 *******************************************************************************/
package org.topcased.validation.core;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EObject;

/**
 * Object that validates an EMF model <br>
 * 
 * Creation : 16 January 2006<br>
 * 
 * @author <a href="mailto:david.sciamma@anyware-tech.com">David Sciamma</a>
 */
public interface IValidator
{

    /**
     * Validates the specified {@link EObject EMF element}, using the specified progress <code>monitor</code> to
     * monitor progress of validation (which is especially useful for recursive validation).
     * 
     * @param eObject the EMF element to validate
     * @param diagnostics The diagnostic chain containing the validation result
     * @param monitor the progress monitor to track validation progress, or <code>null</code> if no progress
     *        monitoring is required
     * @return the validation status
     * @throws CoreException
     */
    boolean validate(EObject eObject, DiagnosticChain diagnostics, IProgressMonitor monitor) throws CoreException;
}
