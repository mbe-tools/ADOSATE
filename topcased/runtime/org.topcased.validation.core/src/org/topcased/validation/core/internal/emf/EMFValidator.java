/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies), 
 *    Mathieu Garcia (Anyware Technologies), 
 *    Jacques Lescot (Anyware Technologies),
 *    Thomas Friol (Anyware Technologies)
 *    - initial API and implementation
 *******************************************************************************/
package org.topcased.validation.core.internal.emf;

import java.util.Iterator;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.topcased.validation.core.IValidator;

/**
 * Validate an EObject with the EMF basic validation <br>
 * creation : 16 janv. 2006
 * 
 * @author <a href="mailto:david.sciamma@anyware-tech.com">David Sciamma</a>
 */
public class EMFValidator implements IValidator
{
    /**
     * @see org.topcased.validation.core.IValidator#validate(org.eclipse.emf.ecore.EObject,
     *      org.eclipse.emf.common.util.DiagnosticChain, org.eclipse.core.runtime.IProgressMonitor)
     */
    public boolean validate(EObject eObject, DiagnosticChain diagnosticChain, final IProgressMonitor monitor)
    {
        int count = 0;
        for (Iterator<EObject> i = eObject.eAllContents(); i.hasNext(); i.next())
        {
            ++count;
        }

        monitor.beginTask("EMF Validation", count);

        Diagnostician diagnostician = new Diagnostician()
        {
            public boolean validate(EClass eClass, EObject object, DiagnosticChain diagnostics, Map<Object, Object> context)
            {
                monitor.worked(1);
                return super.validate(eClass, object, diagnostics, context);
            }
        };

        monitor.setTaskName("Validation of " + EcoreUtil.getURI(eObject));

        Diagnostic result = diagnostician.validate(eObject);
        diagnosticChain.merge(result);

        return true;
    }

}
