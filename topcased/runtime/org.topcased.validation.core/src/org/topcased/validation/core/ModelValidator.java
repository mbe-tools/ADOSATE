/*******************************************************************************
 * Copyright (c) 2005, 2011 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies)
 *    - initial API and implementation
 *    Pierre Gaufillet (Airbus)
 *    - remove unicity filter on results (painful when multiple errors share 
 *      the same name).
 *******************************************************************************/
package org.topcased.validation.core;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.topcased.validation.core.extension.ValidatorDescriptor;
import org.topcased.validation.core.extension.ValidatorsManager;
import org.topcased.validation.core.internal.ValidationPlugin;
import org.topcased.validation.core.internal.emf.EMFValidator;

/**
 * Class that validates an EMF model using registered validators from the <i>org.topcased.validation.core.validators</i>
 * extension point. <br>
 * creation : 13 janv. 2006
 * 
 * @author <a href="mailto:david.sciamma@anyware-tech.com">David Sciamma</a>
 */
public class ModelValidator
{

    /**
     * Validate an EObject with several kind of validation (EMF, OCL...)
     * 
     * @param model the EObject to validate
     * @param progressMonitor the progress bar
     * @return the list of diagnostics
     */
    public Diagnostic validate(EObject model, EObject diagrams, final IProgressMonitor progressMonitor)
    {
        String modelURI = model.eClass().getEPackage().getNsURI();     	
        String diagramsURI = diagrams.eClass().getEPackage().getNsURI();     	
        BasicDiagnostic result = new BasicDiagnostic(EObjectValidator.DIAGNOSTIC_SOURCE, 0, "Root diagnostic", new Object[] {model});
        ValidatorDescriptor[] descriptors = ValidatorsManager.getInstance().getValidators();

        progressMonitor.beginTask("Validation", descriptors.length * 10);

        for (int i = 0; i < descriptors.length; i++)
        {
            String validatorURI = descriptors[i].getURI();

            try
            {
                IValidator validator = descriptors[i].getValidator();

                if (validatorURI == null || validatorURI.equals(modelURI))
                {
                    validator.validate(model, result, new SubProgressMonitor(progressMonitor, 40));
                }

                if (validatorURI == null || validatorURI.equals(diagramsURI))
                {
                    validator.validate(diagrams, result, new SubProgressMonitor(progressMonitor, 10));
                }
            }
            catch (CoreException ce)
            {
                ValidationPlugin.log(ce);
            }         
        }
        
        if (result.getSeverity() == Diagnostic.OK && result.getChildren().isEmpty())
        {
            EMFValidator defaultValidator = new EMFValidator();
            defaultValidator.validate(model, result, new SubProgressMonitor(progressMonitor, 40));
            defaultValidator.validate(diagrams, result, new SubProgressMonitor(progressMonitor, 10));
        }

        progressMonitor.done();
        return result;
    }
}