/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies)
 *    - initial API and implementation
 *******************************************************************************/
package org.topcased.validation.ui.actions;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.ui.dialogs.DiagnosticDialog;
import org.eclipse.emf.common.ui.viewer.IViewerProvider;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.edit.ui.EMFEditUIPlugin;
import org.eclipse.emf.edit.ui.provider.DiagnosticDecorator;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ISetSelectionTarget;

/**
 * Validate a model with the extensible validation process. <br>
 * creation : 11 janv. 2006
 * 
 * @author <a href="mailto:david.sciamma@anyware-tech.com">David Sciamma</a>
 */
public class ValidateAction extends org.eclipse.emf.edit.ui.action.ValidateAction {
	
	private boolean valid;
	
	private final boolean showOKDialog;

	/**
     * Constructor
     * 
     * @param f The validated file
     * @param eObject the validated model
     * @param diag diagrams of the validated model
     */
    public ValidateAction( 	final IWorkbenchPart p_part,
    						final List<EObject> p_objects,
    						final boolean pb_showOkDialog ) {
        super();

        valid = true;
        updateSelection( new StructuredSelection( p_objects ) );
        setActiveWorkbenchPart( p_part );
        showOKDialog = pb_showOkDialog;
    }

    @Override
    protected Diagnostic validate(	final IProgressMonitor p_progressMonitor ) {
    	final Diagnostic diag = super.validate( p_progressMonitor );
    	
    	valid = diag.getSeverity() != Diagnostic.ERROR;
    	
    	return diag;
    }

    @Override
    protected void handleDiagnostic(Diagnostic diagnostic) {
    	int severity = diagnostic.getSeverity();
    	String title = null;
    	String message = null;

    	if (severity == Diagnostic.ERROR || severity == Diagnostic.WARNING)
    	{
    		title = EMFEditUIPlugin.INSTANCE.getString("_UI_ValidationProblems_title");
    		message = EMFEditUIPlugin.INSTANCE.getString("_UI_ValidationProblems_message");
    	}
    	else
    	{
    		title = EMFEditUIPlugin.INSTANCE.getString("_UI_ValidationResults_title");
    		message = EMFEditUIPlugin.INSTANCE.getString(severity == Diagnostic.OK ? "_UI_ValidationOK_message" : "_UI_ValidationResults_message");
    	}

    	int result = 0;
    	if (diagnostic.getSeverity() == Diagnostic.OK) {
    		if ( showOKDialog ) {
    			MessageDialog.openInformation(	PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
    											title,
    											message );
    		}

    		result = Window.CANCEL;
    	}
    	else
    	{
    		result = DiagnosticDialog.open
    				(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), title, message, diagnostic);
    	}

    	ResourceSet resourceSet = domain.getResourceSet();
    	Resource resource = eclipseResourcesUtil != null ? resourceSet.getResources().get(0) : null;
    	if (resource != null)
    	{
    		eclipseResourcesUtil.deleteMarkers(resource);
    	}
    	
    	final URI diagResUri = resource.getURI();
    	final String diagExt = diagResUri.fileExtension();
    	final String modelExtension = diagExt.substring( 0, diagExt.length() - 2 );
    	final Resource modelResource = resourceSet.getResource( diagResUri.trimFileExtension().appendFileExtension( modelExtension), true );

    	if ( modelResource != null ) {
    		eclipseResourcesUtil.deleteMarkers( modelResource );
    	}

    	if (result == Window.OK)
    	{
    		if (!diagnostic.getChildren().isEmpty())
    		{
    			List<?> data = (diagnostic.getChildren().get(0)).getData();
    			if (!data.isEmpty() && data.get(0) instanceof EObject)
    			{
    				Object part = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActivePart();
    				if (part instanceof ISetSelectionTarget)
    				{
    					((ISetSelectionTarget)part).selectReveal(new StructuredSelection(data.get(0)));
    				}
    				else if (part instanceof IViewerProvider)
    				{
    					Viewer viewer = ((IViewerProvider)part).getViewer();
    					if (viewer != null)
    					{
    						viewer.setSelection(new StructuredSelection(data.get(0)), true);
    					}
    				}
    			}
    		}

    		if ( resource != null ) {
    			for ( final Diagnostic childDiagnostic : diagnostic.getChildren() ) {
    				eclipseResourcesUtil.createMarkers( resource, childDiagnostic );
    			}
    		}

    		// DB: We also want the markers to be set on the semantic model. Prevents synchronization from being applied.
    		if ( modelResource != null ) {
    			for ( final Diagnostic childDiagnostic : diagnostic.getChildren() ) {
    				eclipseResourcesUtil.createMarkers( modelResource, childDiagnostic );
    			}
    		}
    	}
    	else
    	{
    		// Trigger direct updating of decorations, if there are adapters.
    		//
    		resource = null;
    	}

    	if (resource == null)
    	{
    		// If no markers are produced the decorator won't be able to respond to marker resource deltas, so inform it directly.
    		//

    		// Create a diagnostic for the resource set as a whole.
    		//
    		BasicDiagnostic resourceSetDiagnostic = new BasicDiagnostic(EObjectValidator.DIAGNOSTIC_SOURCE, 0, null, new Object [] { resourceSet });

    		// Create a diagnostic for each resource.
    		//
    		Map<Resource, BasicDiagnostic> resourceDiagnostics = new LinkedHashMap<Resource, BasicDiagnostic>();
    		for (Resource r : resourceSet.getResources())
    		{
    			BasicDiagnostic resourceDiagnostic = new BasicDiagnostic(EObjectValidator.DIAGNOSTIC_SOURCE, 0, null, new Object [] { r });
    			resourceDiagnostics.put(r, resourceDiagnostic);
    		}

    		// Just clean up decorations if the dialog was cancelled.
    		//
    		if (result == Dialog.OK)
    		{
    			// Partition the children among the resource diagnostics.
    			//
    			for (Diagnostic child : diagnostic.getChildren())
    			{
    				List<?> data = child.getData();
    				if (!data.isEmpty())
    				{
    					Object object = data.get(0);
    					if (object instanceof EObject)
    					{
    						BasicDiagnostic resourceDiagnostic = resourceDiagnostics.get(((EObject)object).eResource());
    						if (resourceDiagnostic != null)
    						{
    							resourceDiagnostic.add(child);
    						}
    					}
    				}
    			}
    		}

    		// Add the resource diagnostics to the resource set diagnostic.
    		//
    		for (Diagnostic resourceDiagnostic : resourceDiagnostics.values())
    		{
    			resourceSetDiagnostic.add(resourceDiagnostic);
    		}

    		// Inform any decorators.
    		//
    		DiagnosticDecorator.DiagnosticAdapter.update(resourceSet, resourceSetDiagnostic);
    	}
    }

    public boolean isValid() {
		return valid;
	}
}
