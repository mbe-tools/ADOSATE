/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies)
 *    - initial API and implementation
 *******************************************************************************/
package org.topcased.validation.ui.internal.actions;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.topcased.validation.core.MarkerUtil;
import org.topcased.validation.core.ModelValidator;
import org.topcased.validation.ui.internal.ValidationUIPlugin;

/**
 * Eclipse job that validates a model using the extensible validation process and creates the markers from the result of
 * the validation. <br>
 * Creation : 16 janv. 2006
 * 
 * @author <a href="mailto:david.sciamma@anyware-tech.com">David Sciamma</a>
 */
public class ValidateJob extends Job
{
    private EObject model;
    private EObject diagrams;

    private IFile file;

    /**
     * Build a job that validate the given model and add the markers on the file
     * 
     * @param f the file (where the markers are added)
     * @param eObject the model to validate
     */
    public ValidateJob(IFile f, EObject eObject, EObject diag)
    {
        super("Model Validation");

        model = eObject;
        diagrams = diag;
        file = f;
    }

    /**
     * Validates the model and creates the validation markers
     * 
     * @see org.eclipse.core.runtime.jobs.Job#run(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    protected IStatus run(IProgressMonitor monitor)
    {
        monitor.beginTask("Model validation", 100);

        Diagnostic diagnostic = validate(new SubProgressMonitor(monitor, 90));

        if (!monitor.isCanceled())
        {
            monitor.setTaskName("Markers creation");
            handleDiagnostic(monitor, diagnostic);
        }

        monitor.done();
        return Status.OK_STATUS;
    }

    /**
     * Launch the validation process
     * 
     * @param monitor the progress monitor
     * @return the validation results
     */
    private Diagnostic validate(IProgressMonitor monitor)
    {
        ModelValidator validator = new ModelValidator();
        return validator.validate(model, diagrams, monitor);
    }

    /**
     * Displays a dialog if the validation is successful or creates the markers from the validation results if there is
     * errors or warnings
     * @param monitor 
     * 
     * @param diagnostic the validation results
     * @param monitor2 
     */
    private void handleDiagnostic(IProgressMonitor monitor, final Diagnostic diagnostic)
    {
        if (file != null)
        {
            try
            {
               
                if (diagnostic.getData() != null &&  diagnostic.getData().size() > 0 && diagnostic.getData().get(0) instanceof EObject)
                {
                    EObject obj = (EObject) diagnostic.getData().get(0);
                    ResourceSet resourceSet = obj.eResource().getResourceSet();
                    MarkerUtil.deleteMarkers(resourceSet);
                }
            }
            catch (CoreException exception)
            {
                ValidationUIPlugin.log(exception);
            }
        }

        if (diagnostic.getSeverity() == Diagnostic.OK)
        {
            Display.getDefault().asyncExec(new Runnable()
            {
                public void run()
                {
                    MessageDialog.openInformation(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Validation successfull", "The model is valid.");
                }
            });
        }
        else
        {
        	if (file != null)
        	{
        		WorkspaceModifyOperation op = new WorkspaceModifyOperation() {
        			@Override
                    public void execute(IProgressMonitor mon) throws CoreException {
        				for (Diagnostic childDiagnostic : diagnostic.getChildren())
        				{
        					createMarkers(file, childDiagnostic);
        				}
        			}
        		};
        		try {
        			op.run(monitor);
        		} catch (InvocationTargetException e) {
        			ValidationUIPlugin.log(e);
        		} catch (InterruptedException e) {
        			// Nothing
        		}
        	}
        }
    }

    /**
     * Creates markers from the given diagnostic on the given file
     * 
     * @param f the file where markers are stored
     * @param diagnostic the validation results
     */
    protected void createMarkers(IFile f, Diagnostic diagnostic)
    {
        MarkerUtil.createMarkers(f, diagnostic);
    }
}
