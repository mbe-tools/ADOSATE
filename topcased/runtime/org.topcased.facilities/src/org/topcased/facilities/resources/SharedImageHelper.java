/***********************************************************************************************************************
 * Copyright (c) 2008 Communication and Systems.
 * 
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse
 * Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: Sebastien GABEL (CS) - initial API and implementation
 * 
 **********************************************************************************************************************/
package org.topcased.facilities.resources;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.topcased.facilities.internal.FacilitiesPlugin;

/**
 * Helper for accessing shared images between the different Topcased plug-ins.<br>
 * These common resources correspond to action icons, wizards and dialogs images.
 * 
 * @author <a href="mailto:sebastien.gabel@c-s.fr">Sebastien GABEL</a>
 */
public final class SharedImageHelper implements ITopcasedImageConstants
{
    /**
     * Constructor
     */
    private SharedImageHelper()
    {
        // can not be instancied
    }

    /**
     * Gets the default image descriptor for a wizard designed for Topcased.
     * 
     * @return the default image for a wizard
     */
    public static ImageDescriptor getTopcasedWizardImageDescriptor()
    {
        return FacilitiesPlugin.getImageDescriptor(TOPCASED_LOGO_PATH);
    }

    /**
     * Gets the default image for a wizard designed for Topcased.
     * 
     * @return the default image for a wizard
     */
    public static Image getTopcasedWizardImage()
    {
        return getTopcasedWizardImageDescriptor().createImage();
    }

    /**
     * Gets the default image descriptor for a dialog designed for Topcased.
     * 
     * @return the default image for a dialog
     */
    public static ImageDescriptor getTopcasedDialogImageDescriptor()
    {
        // to be defined....can change in the future.
        return FacilitiesPlugin.getImageDescriptor(TOPCASED_LOGO_PATH);
    }

    /**
     * Gets the default image for a wizard designed for Topcased plug-in.
     * 
     * @return the default image for a wizard
     */
    public static Image getTopcasedDialogImage()
    {
        return getTopcasedDialogImageDescriptor().createImage();
    }

    /**
     * Gets the default image descriptor for an action icon designed for Topcased plug-in.
     * 
     * @return the default image for a dialog
     */
    public static ImageDescriptor getTopcasedIconImageDescriptor()
    {
        return FacilitiesPlugin.getImageDescriptor(TOPCASED_ICON_16);
    }

    /**
     * Gets the default image for a wizard designed for Topcased.
     * 
     * @return the default image for a wizard
     */
    public static Image getTopcasedIconImage()
    {
        return getTopcasedIconImageDescriptor().createImage();
    }
}
