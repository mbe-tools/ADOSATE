/***********************************************************************************************************************
 * Copyright (c) 2008, 2010 Communication & Systems.
 * 
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse
 * Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: Sebastien GABEL (CS) - initial API and implementation
 * 
 **********************************************************************************************************************/
package org.topcased.facilities.resources;

import org.eclipse.core.runtime.IPath;

/**
 * This interface stores the constants used during the various Topcased wizard.<br>
 * It enables to reach some common icons and images.
 * 
 * @author <a href="mailto:sebastien.gabel@c-s.fr">Sebastien GABEL</a>
 */
public interface ITopcasedImageConstants
{
    /**
     * Folder where are stored the images resources.
     */
    String IMG_FOLDER = "icons"; //$NON-NLS-1$

    /**
     * Main Topcased logo represenyed in most of Topcased wizards and dialogs (often in the top right corner)
     */
    String TOPCASED_LOGO_PATH = IMG_FOLDER + IPath.SEPARATOR + "topcased-logo.gif"; //$NON-NLS-1$

    /**
     * Image representing a File (used in T2P plug-in)
     */
    String FILE_IMG = IMG_FOLDER + IPath.SEPARATOR + "file.gif"; //$NON-NLS-1$

    /**
     * Directory Image (used in T2P plug-in)
     */
    String DIRECTORY_IMG = IMG_FOLDER + IPath.SEPARATOR + "directory.gif"; //$NON-NLS-1$

    /**
     * Common Topcased icon represented in most of actions defined in the contextual menu.
     */
    String TOPCASED_ICON_16 = IMG_FOLDER + IPath.SEPARATOR + "topcased-icon16.gif"; //$NON-NLS-1$
}
