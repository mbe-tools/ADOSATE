/***********************************************************************************************************************
 * Copyright (c) 2010 Communication & Systems.
 * 
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse
 * Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: Sebastien GABEL (CS) - initial API and implementation
 * 
 **********************************************************************************************************************/
package org.topcased.facilities.widgets;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.preference.StringButtonFieldEditor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.dialogs.ResourceSelectionDialog;

/**
 * A string button field editor editor dedicated to select a path of a resource contained by the current workspace.<br>
 * A standard workspace dialog appears when the user presses the change button.<br>
 * Selected resources from the workspace are formatted as valid URIs (e.g platform:/resource/PROJECT/FOLDER/FILE.xxx).
 * Note that selected resources are also encoded and decoded when it is necessary.<br>
 * 
 * Creation : 06 may 2010<br>
 * 
 * @author <a href="mailto:sebastien.gabel@c-s.fr">Sebastien GABEL</a>
 * @since Topcased 3.4.0
 */
public class ResourceFieldEditor extends StringButtonFieldEditor
{

    /**
     * Creates a file field editor.
     * 
     * @param name the name of the preference this field editor works on
     * @param labelText the label text of the field editor
     * @param parent the parent of the field editor's control
     */
    public ResourceFieldEditor(String name, String labelText, Composite parent)
    {
        this(name, labelText, VALIDATE_ON_FOCUS_LOST, parent);
    }

    /**
     * Creates a file field editor.
     * 
     * @param name the name of the preference this field editor works on
     * @param labelText the label text of the field editor
     * @param enforceAbsolute <code>true</code> if the file path must be absolute, and <code>false</code> otherwise
     * @param validationStrategy either {@link StringButtonFieldEditor#VALIDATE_ON_KEY_STROKE} to perform on the fly
     *        checking, or {@link StringButtonFieldEditor#VALIDATE_ON_FOCUS_LOST} (the default) to perform validation
     *        only after the text has been typed in
     * @param parent the parent of the field editor's control.
     * @since 3.4
     * @see StringButtonFieldEditor#VALIDATE_ON_KEY_STROKE
     * @see StringButtonFieldEditor#VALIDATE_ON_FOCUS_LOST
     */
    public ResourceFieldEditor(String name, String labelText, int validationStrategy, Composite parent)
    {
        init(name, labelText);
        setErrorMessage("Value must be an existing file");//$NON-NLS-1$
        setValidateStrategy(validationStrategy);
        createControl(parent);
        setChangeButtonText("...");//$NON-NLS-1$
    }

    /**
     * @see org.eclipse.jface.preference.StringButtonFieldEditor#changePressed()
     */
    protected String changePressed()
    {
        IFile d = getFile();
        if (d == null)
        {
            return null;
        }

        URI uri = URI.createPlatformResourceURI(d.getFullPath().toString(), false);
        return URI.decode(uri.toString());
    }

    /**
     * @see org.eclipse.jface.preference.StringFieldEditor#checkState()
     */
    protected boolean checkState()
    {
        String msg = null;
        String path = getTextControl().getText();
        if (path != null)
        {
            path = path.trim();
        }
        else
        {
            path = "";//$NON-NLS-1$
        }
        if (path.length() == 0)
        {
            if (!isEmptyStringAllowed())
            {
                msg = getErrorMessage();
            }
        }
        else
        {
            URI uri = URI.createURI(getTextControl().getText(), true);
            IResource rsc = ResourcesPlugin.getWorkspace().getRoot().findMember(uri.toPlatformString(true));

            if (!rsc.exists())
            {
                msg = getErrorMessage();
            }
        }

        if (msg != null)
        { // error
            showErrorMessage(msg);
            return false;
        }

        if (doCheckState())
        { // OK!
            clearErrorMessage();
            return true;
        }
        msg = getErrorMessage(); // subclass might have changed it in the #doCheckState()
        if (msg != null)
        {
            showErrorMessage(msg);
        }
        return false;
    }

    /**
     * Helper to open the workspace resource chooser dialog. Note that only one value can be returned at the end.<br>
     * 
     * @return The IFile the user selected or <code>null</code> if they do not.
     */
    private IFile getFile()
    {
        ResourceSelectionDialog dialog = new ResourceSelectionDialog(getShell(), ResourcesPlugin.getWorkspace().getRoot(), ""); //$NON-NLS-1$
        if (dialog.open() == Window.OK)
        {
            Object[] result = dialog.getResult();
            // sanity check
            if (result.length == 1)
            {
                if (result[0] instanceof IFile)
                {
                    return (IFile) result[0];
                }
            }
        }
        return null;
    }
}
