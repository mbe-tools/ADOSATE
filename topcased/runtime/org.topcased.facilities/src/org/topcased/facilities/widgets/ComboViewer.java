/*******************************************************************************
 * Copyright (c) 2004 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.topcased.facilities.widgets;

import org.eclipse.jface.viewers.AbstractListViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * A concrete viewer based either on an SWT <code>Combo</code> control or <code>CCombo</code> control. This class is
 * intended as an alternative to the JFace <code>ListViewer</code>, which displays its content in a combo box rather
 * than a list. Wherever possible, this class attempts to behave like ListViewer.
 * <p>
 * 
 * This class is designed to be instantiated with a pre-existing SWT combo control and configured with a domain-specific
 * content provider, label provider, element filter (optional), and element sorter (optional).
 * </p>
 * 
 * @see org.eclipse.jface.viewers.ListViewer
 * @since 3.0
 */
public final class ComboViewer extends AbstractListViewer
{

    /**
     * This viewer's list control if this viewer is instantiated with a combo control; otherwise <code>null</code>.
     * 
     * @see #ComboViewer(Combo)
     */
    private Combo combo;

    /**
     * This viewer's list control if this viewer is instantiated with a CCombo control; otherwise <code>null</code>.
     * 
     * @see #ComboViewer(CCombo)
     */
    private CCombo ccombo;

    /**
     * Creates a combo viewer on a newly-created combo control under the given parent. The viewer has no input, no
     * content provider, a default label provider, no sorter, and no filters.
     * 
     * @param parent the parent control
     */
    public ComboViewer(Composite parent)
    {
        this(parent, SWT.READ_ONLY | SWT.BORDER);
    }

    /**
     * Creates a combo viewer on a newly-created combo control under the given parent. The combo control is created
     * using the given SWT style bits. The viewer has no input, no content provider, a default label provider, no
     * sorter, and no filters.
     * 
     * @param parent the parent control
     * @param style the SWT style bits
     */
    public ComboViewer(Composite parent, int style)
    {
        this(new Combo(parent, style));
    }

    /**
     * Creates a combo viewer on the given combo control. The viewer has no input, no content provider, a default label
     * provider, no sorter, and no filters.
     * 
     * @param list the combo control
     */
    public ComboViewer(Combo list)
    {
        this.combo = list;
        hookControl(list);
    }

    /**
     * Creates a combo viewer on the given CCombo control. The viewer has no input, no content provider, a default label
     * provider, no sorter, and no filters.
     * 
     * @param list the CCombo control
     */
    public ComboViewer(CCombo list)
    {
        this.ccombo = list;
        hookControl(list);
    }

    /**
     * @see org.eclipse.jface.viewers.AbstractListViewer#listAdd(java.lang.String, int)
     */
    protected void listAdd(String string, int index)
    {
        if (combo == null)
        {
            ccombo.add(string, index);
        }
        else
        {
            combo.add(string, index);
        }
    }

    /**
     * @see org.eclipse.jface.viewers.AbstractListViewer#listSetItem(int, java.lang.String)
     */
    protected void listSetItem(int index, String string)
    {
        if (combo == null)
        {
            ccombo.setItem(index, string);
        }
        else
        {
            combo.setItem(index, string);
        }
    }

    /**
     * @see org.eclipse.jface.viewers.AbstractListViewer#listGetSelectionIndices()
     */
    protected int[] listGetSelectionIndices()
    {
        if (combo == null)
        {
            return new int[] {ccombo.getSelectionIndex()};
        }
        else
        {
            return new int[] {combo.getSelectionIndex()};
        }
    }

    /**
     * @see org.eclipse.jface.viewers.AbstractListViewer#listGetItemCount()
     */
    protected int listGetItemCount()
    {
        if (combo == null)
        {
            return ccombo.getItemCount();
        }
        else
        {
            return combo.getItemCount();
        }
    }

    /**
     * @see org.eclipse.jface.viewers.AbstractListViewer#listSetItems(java.lang.String[])
     */
    protected void listSetItems(String[] labels)
    {
        if (combo == null)
        {
            ccombo.setItems(labels);
        }
        else
        {
            combo.setItems(labels);
        }
    }

    /**
     * @see org.eclipse.jface.viewers.AbstractListViewer#listRemoveAll()
     */
    protected void listRemoveAll()
    {
        if (combo == null)
        {
            ccombo.removeAll();
        }
        else
        {
            combo.removeAll();
        }
    }

    /**
     * @see org.eclipse.jface.viewers.AbstractListViewer#listRemove(int)
     */
    protected void listRemove(int index)
    {
        if (combo == null)
        {
            ccombo.remove(index);
        }
        else
        {
            combo.remove(index);
        }
    }

    /**
     * @see org.eclipse.jface.viewers.AbstractListViewer#getControl()
     */
    public Control getControl()
    {
        if (combo == null)
        {
            return ccombo;
        }
        else
        {
            return combo;
        }
    }

    /**
     * Returns this list viewer's list control.
     * 
     * @return the list control
     * @exception IllegalArgumentException
     *            <ul>
     *            <li>ERROR_INVALID_ARGUMENT - if this viewer has been created on a combo control</li>
     *            </ul>
     */
    public CCombo getCCombo()
    {
        if (ccombo == null)
        {
            SWT.error(SWT.ERROR_INVALID_ARGUMENT);
        }
        return ccombo;
    }

    /**
     * Returns this list viewer's list control.
     * 
     * @return the list control
     * @exception IllegalArgumentException
     *            <ul>
     *            <li>ERROR_INVALID_ARGUMENT - if this viewer has been created on a CCombo control</li>
     *            </ul>
     */
    public Combo getCombo()
    {
        if (combo == null)
        {
            SWT.error(SWT.ERROR_INVALID_ARGUMENT);
        }
        return combo;
    }

    /**
     * Do nothing -- combos only display the selected element, so there is no way we can ensure that the given element
     * is visible without changing the selection. Method defined on StructuredViewer.
     * 
     * @see org.eclipse.jface.viewers.StructuredViewer#reveal(java.lang.Object)
     */
    public void reveal(Object element)
    {
        return;
    }

    /**
     * @see org.eclipse.jface.viewers.AbstractListViewer#listSetSelection(int[])
     */
    protected void listSetSelection(int[] ixs)
    {
        if (combo == null)
        {
            for (int idx = 0; idx < ixs.length; idx++)
            {
                ccombo.select(ixs[idx]);
            }
        }
        else
        {
            for (int idx = 0; idx < ixs.length; idx++)
            {
                combo.select(ixs[idx]);
            }
        }
    }

    /**
     * @see org.eclipse.jface.viewers.AbstractListViewer#listDeselectAll()
     */
    protected void listDeselectAll()
    {
        if (combo == null)
        {
            ccombo.deselectAll();
            ccombo.clearSelection();
        }
        else
        {
            combo.deselectAll();
            combo.clearSelection();
        }
    }

    /**
     * @see org.eclipse.jface.viewers.AbstractListViewer#listShowSelection()
     */
    protected void listShowSelection()
    {
        // Do nothing
    }
}