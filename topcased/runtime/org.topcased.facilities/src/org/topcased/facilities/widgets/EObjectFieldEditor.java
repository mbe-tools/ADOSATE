/***********************************************************************************************************************
 * Copyright (c) 2010 Communication & Systems.
 * 
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse
 * Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: Sebastien GABEL (CS) - initial API and implementation
 * 
 **********************************************************************************************************************/
package org.topcased.facilities.widgets;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.preference.StringButtonFieldEditor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Composite;
import org.topcased.facilities.dialogs.ChooseDialog;

/**
 * A string button field editor dedicated to select a model element from a collection.<br>
 * The standard Topcased choose dialog appears when the user presses the change button.<br>
 * A default and an advanced label providers can be provided to the choose dialog (Cf. setLabelProvider() and
 * setAdvancedLabelProvider() methods). The collection of items to display can be provided using the method
 * setCandidates(Array).
 * 
 * Creation : 06 may 2010<br>
 * 
 * @author <a href="mailto:sebastien.gabel@c-s.fr">Sebastien GABEL</a>
 * @since Topcased 3.4.0
 * @see {@link ChooseDialog}
 */
public class EObjectFieldEditor extends StringButtonFieldEditor
{

    /** An advanced label provider proposing qualified name for instance */
    private ILabelProvider advancedLabelProvider;

    /** A default label provider */
    private ILabelProvider labelProvider;

    /** Array of candidate elements */
    private Object[] candidates;

    /**
     * Creates a file field editor.
     * 
     * @param name the name of the preference this field editor works on
     * @param labelText the label text of the field editor
     * @param parent the parent of the field editor's control
     */
    public EObjectFieldEditor(String name, String labelText, Composite parent)
    {
        this(name, labelText, VALIDATE_ON_FOCUS_LOST, parent);
    }

    /**
     * Creates a file field editor.
     * 
     * @param name the name of the preference this field editor works on
     * @param labelText the label text of the field editor
     * @param enforceAbsolute <code>true</code> if the file path must be absolute, and <code>false</code> otherwise
     * @param validationStrategy either {@link StringButtonFieldEditor#VALIDATE_ON_KEY_STROKE} to perform on the fly
     *        checking, or {@link StringButtonFieldEditor#VALIDATE_ON_FOCUS_LOST} (the default) to perform validation
     *        only after the text has been typed in
     * @param parent the parent of the field editor's control.
     * @since 3.4
     * @see StringButtonFieldEditor#VALIDATE_ON_KEY_STROKE
     * @see StringButtonFieldEditor#VALIDATE_ON_FOCUS_LOST
     */
    public EObjectFieldEditor(String name, String labelText, int validationStrategy, Composite parent)
    {
        init(name, labelText);
        setErrorMessage(JFaceResources.getString("FileFieldEditor.errorMessage"));//$NON-NLS-1$
        setValidateStrategy(validationStrategy);
        createControl(parent);
        setChangeButtonText("...");//$NON-NLS-1$
    }

    /**
     * @see org.eclipse.jface.preference.StringButtonFieldEditor#changePressed()
     */
    protected String changePressed()
    {
        ChooseDialog dialog = new ChooseDialog(getShell(), getCandidates());
        // Sets the providers
        dialog.setLabelProvider(getLabelProvider());
        dialog.setAdvancedLabelProvider(getAdvancedLabelProvider());

        // Open the dialog
        if (dialog.open() == Window.OK)
        {
            Object[] result = dialog.getResult();
            if (result.length == 1)
            {
                if (result[0] instanceof EObject)
                {
                    EObject selected = ((EObject) result[0]);
                    // format the result accordingly to one of the providers.
                    return advancedLabelProvider != null ? advancedLabelProvider.getText(selected) : labelProvider.getText(selected);
                }
            }
        }
        return null;
    }

    /**
     * @see org.eclipse.jface.preference.StringFieldEditor#checkState()
     */
    protected boolean checkState()
    {
        return true;
    }

    /**
     * Sets the default provider for this button field editor.
     */
    public void setLabelProvider(ILabelProvider provider)
    {
        labelProvider = provider;
    }

    /**
     * Gets the default label provider set this button field editor.
     * 
     * @return the advanced label provider
     */
    public ILabelProvider getLabelProvider()
    {
        return labelProvider;
    }

    /**
     * Sets the advanced provider this button field editor.
     */
    public void setAdvancedLabelProvider(ILabelProvider provider)
    {
        advancedLabelProvider = provider;
    }

    /**
     * Gets the advanced label provider set for this widget.
     * 
     * @return the advanced label provider
     */
    public ILabelProvider getAdvancedLabelProvider()
    {
        return advancedLabelProvider;
    }

    /**
     * Sets the items candidate to this dialog.
     * 
     * @param array An array of elements to display in the dialog
     */
    public void setCandidates(Object[] array)
    {
        candidates = array;
    }

    /**
     * Gets the items candidate to this dialog.
     * 
     * @return The array of candidates
     */
    public Object[] getCandidates()
    {
        return candidates;
    }

}
