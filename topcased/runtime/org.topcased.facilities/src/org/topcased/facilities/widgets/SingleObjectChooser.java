/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies),
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/

package org.topcased.facilities.widgets;

import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.topcased.facilities.dialogs.ChooseDialog;

/**
 * This widget displays a combo with the available values and a button that opens a dialog with an easier selection :
 * searchable list with pattern text field, case sensitive filter...
 * 
 * @author <a href="david.sciamma@anyware-tech.com">David Sciamma</a>
 */
public class SingleObjectChooser extends Composite
{
    private ComboViewer combo;

    private Button chooseBt;

    private Object[] objects;

    /**
     * Constructor
     * 
     * @param parent
     * @param style
     */
    public SingleObjectChooser(Composite parent, int style)
    {
        super(parent, style);

        createContents(this);
        hookListeners();
    }

    /**
     * Creates the UI
     * 
     * @param parent this widget
     */
    protected void createContents(Composite parent)
    {
        GridLayout layout = new GridLayout(2, false);
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        parent.setLayout(layout);

        combo = new ComboViewer(parent);
        combo.getControl().setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        combo.setSorter(new ViewerSorter());
        combo.setContentProvider(new ArrayContentProvider());

        chooseBt = new Button(parent, SWT.PUSH);
        chooseBt.setText("..."); //$NON-NLS-1$
    }

    /**
     * Adds the listeners on the widgets
     */
    protected void hookListeners()
    {
        chooseBt.addSelectionListener(new SelectionAdapter()
        {
            /**
             * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
             */
            public void widgetSelected(SelectionEvent e)
            {
                handleChoose();
            }
        });
    }

    /**
     * Add a SelectionListener on both the CCombo and the Button
     * 
     * @param listener
     */
    public void addSelectionListener(SelectionListener listener)
    {
        if (listener == null)
        {
            SWT.error(SWT.ERROR_NULL_ARGUMENT);
        }
        combo.getCombo().addSelectionListener(listener);
    }

    /**
     * Remove the SelectionListener of the CCombo and the Button
     * 
     * @param listener
     */
    public void removeSelectionListener(SelectionListener listener)
    {
        if (listener == null)
        {
            SWT.error(SWT.ERROR_NULL_ARGUMENT);
        }
        combo.getCombo().removeSelectionListener(listener);
    }

    /**
     * Set the objects in which the user can choose.
     * 
     * @param objs the list of objects
     */
    public void setChoices(Object[] objs)
    {
        if (objs != null && objs.length > 0)
        {
            this.objects = objs;
            combo.setInput(this.objects);
            combo.setSelection(new StructuredSelection(this.objects[0]));
        }
    }

    /**
     * Clear the combo
     * 
     */
    public void clearCombo()
    {
        if (combo.getInput() != null && ((Object[]) combo.getInput()).length > 0)
        {
            combo.remove(combo.getInput());
        }
    }

    /**
     * Set the provider that displays the objects
     * 
     * @param labelProvider the LabelProvider
     */
    public void setLabelProvider(IBaseLabelProvider labelProvider)
    {
        combo.setLabelProvider(labelProvider);
    }

    /**
     * Open the dialog to choose in the searchable list
     */
    private void handleChoose()
    {
        ChooseDialog dialog = new ChooseDialog(getShell(), objects);
        dialog.setLabelProvider((ILabelProvider) combo.getLabelProvider());
        List< ? > selectedObject = ((IStructuredSelection) combo.getSelection()).toList();
        dialog.setInitialElementSelections(selectedObject);

        if (dialog.open() == Window.OK)
        {
            Object[] selection = dialog.getResult();

            if (selection != null && selection.length > 0)
            {
                combo.setSelection(new StructuredSelection(selection[0]));
                combo.getCombo().notifyListeners(SWT.Selection, new Event());
            }
        }
    }

    /**
     * Returns the selected object
     * 
     * @return the selection
     */
    public Object getSelection()
    {
        Object ret = ((IStructuredSelection) combo.getSelection()).getFirstElement();
        if (ret instanceof String)
        {
            return null;
        }
        else
        {
            return ret;
        }
    }

    /**
     * Set the selection of the combo
     * 
     * @param selection the selected object
     */
    public void setSelection(Object selection)
    {
        if (selection != null)
        {
            combo.setSelection(new StructuredSelection(selection));
            combo.getCombo().notifyListeners(SWT.Selection, new Event());
        }
    }
}
