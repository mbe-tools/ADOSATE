/*******************************************************************************
* Copyright (c) 2005 AIRBUS FRANCE.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
*
* Contributors:
*    David Sciamma (Anyware Technologies), Mathieu Garcia (Anyware Technologies),
*    Jacques Lescot (Anyware Technologies) - initial API and implementation
*    Thibault Landr� (Atos Origin) - Add the project scope to this preferencePage.
*******************************************************************************/ 
package org.topcased.facilities.preferences;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.topcased.facilities.internal.FacilitiesPlugin;

/**
 * This class represents a preference page that is contributed to the
 * Preferences dialog. This page is used to modify preferences only. They are
 * stored in the preference store that belongs to the main plug-in class. That
 * way, preferences can be accessed directly via the preference store.
 */

public class TopcasedPreferencePage extends AbstractTopcasedPreferencePage implements IWorkbenchPreferencePage
{

    /** The parent composite. */
    private Composite parentComposite;

    /**
     * The Constructor.
     */
    public TopcasedPreferencePage()
    {
        // does nothing
    }

    /**
     * Create the contents of the preference page. The page contains nothing for
     * the moment.
     * 
     * @param parent the parent
     * 
     * @return the control
     * 
     * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    protected Control createContents(Composite parent)
    {
        parentComposite = new Composite(parent, SWT.NULL);
        GridLayout layout = new GridLayout();
        layout.numColumns = 1;
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        parentComposite.setLayout(layout);
        parentComposite.setFont(parent.getFont());

        return parentComposite;
    }

    /**
     * Init the preference page.
     * 
     * @param workbench the workbench
     * 
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench)
    {
        // does nothing
    }
    
    /**
     * @see org.topcased.facilities.preferences.AbstractTopcasedPreferencePage#getBundleId()
     */
    protected String getBundleId() {
    	return FacilitiesPlugin.getDefault().getBundle().getSymbolicName();
    }

}
