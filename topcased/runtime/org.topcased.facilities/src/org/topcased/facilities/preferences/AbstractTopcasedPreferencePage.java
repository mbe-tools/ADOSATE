/*****************************************************************************
 * 
 * AbstractTopcasedPreferencePage.java
 * 
 * Copyright (c) 2008 TOPCASED consortium.
 *
 * Contributors:
 *  Thibault Landr� (Atos Origin) thibault.landre@atosorigin.com - initial API and implementation
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
  *****************************************************************************/
package org.topcased.facilities.preferences;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.IWorkbenchPropertyPage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

/**
 * An abstract implementation of a Preference page for Topcased's projects. 
 * 
 * This preference page allows clients to define preference page in the preference of Eclipse, and in the properties of an element in the workspace. 
 * Clients must implement the <code>getBundleId()</code> method in order to define the preference scope (Project or Instance) of the preference page.
 */
public abstract class AbstractTopcasedPreferencePage extends PreferencePage implements
		IWorkbenchPreferencePage, IWorkbenchPropertyPage {
   
    private IProject project;
    
    /**
     * @see org.eclipse.ui.IWorkbenchPropertyPage#getElement()
     */
	public IAdaptable getElement() 
	{
		return project;
	}

	/**
	 * @see org.eclipse.ui.IWorkbenchPropertyPage#setElement(org.eclipse.core.runtime.IAdaptable)
	 */
	public void setElement(IAdaptable element) 
	{
		project = (IProject) element.getAdapter(IResource.class);		
	}
	
	/**
	 * @see org.eclipse.jface.preference.PreferencePage#doGetPreferenceStore()
	 */
	protected IPreferenceStore doGetPreferenceStore() 
	{
		IPreferenceStore store;
		if (project != null) 
		{
			store = new ScopedPreferenceStore(new ProjectScope(project), getBundleId());
		} 
		else 
		{
			store = new ScopedPreferenceStore(new InstanceScope(), getBundleId());
		}
		return store;
	}

	/**
	 * 
	 * @return String
	 */
	protected abstract String getBundleId();
	
}
