/*****************************************************************************
 * Copyright (c) 2009 ATOS ORIGIN INTEGRATION.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Tristan FAURE (ATOS ORIGIN INTEGRATION) tristan.faure@atosorigin.com - Initial API and implementation
 *
 *****************************************************************************/
package org.topcased.facilities.util;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.topcased.facilities.internal.FacilitiesPlugin;

/**
 * The Class TopcasedSynchronizerUtil permits to a developer to protect code accessed by concurrent threads he had to
 * define a common string key for his threads and can block them with it
 */
public final class TopcasedSynchronizerUtil
{
    public static TopcasedSynchronizerUtil eINSTANCE = new TopcasedSynchronizerUtil();

    private ConcurrentHashMap<String, Semaphore> semaphores = new ConcurrentHashMap<String, Semaphore>();

    private TopcasedSynchronizerUtil()
    {
        // do nothing
    }

    /**
     * A thread has to call a method with a key if a thread already calls this method with the same key the caller will
     * be blocked until the first thread calling this method releases the token
     * 
     * @param key, the constant identifying the wait
     */
    public void blockOrTakeAToken(String key)
    {
        Semaphore result = null;
        result = semaphores.get(key);
        if (result == null)
        {
            Semaphore newResult = new Semaphore(1);
            result = semaphores.putIfAbsent(key, newResult);
            if (result == null)
            {
                result = newResult;
            }
        }
        try
        {
            // 30 seconds is sufficient for acquire if time out occurs event is logged
            if (!result.tryAcquire(30,TimeUnit.SECONDS))
            {
                FacilitiesPlugin.getDefault().getLog().log(new Status(IStatus.ERROR, FacilitiesPlugin.PLUGIN_ID, IStatus.ERROR, "an error occurs during semaphore acquisition " + key,null));
            }
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Release the token already taken to unblock threads waiting
     * 
     * @param key the key
     */
    public void releaseToken(String key)
    {
        Semaphore result = null;
        result = semaphores.get(key);
        if (result != null)
        {
            result.release();
        }
    }
}
