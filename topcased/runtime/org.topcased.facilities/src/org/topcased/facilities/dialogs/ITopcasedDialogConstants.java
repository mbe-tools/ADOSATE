/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies), 
 *    Nicolas Lalevee (Anyware Technologies),
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *******************************************************************************/

package org.topcased.facilities.dialogs;

/**
 * This interface stores the constants used to build the dialogs for modelers
 * @author <a href="david.sciamma@anyware-tech.com">David Sciamma</a>
 */
public interface ITopcasedDialogConstants
{
    /**
     * The default width of a dialog
     */
    int DEFAULT_DIALOG_WIDTH = 400;
    
    /**
     * The default height of a dialog
     */
    int DEFAULT_DIALOG_HEIGHT = 300;
    
    /**
     * The minimum width of a dialog
     */
    int MIN_DIALOG_WIDTH = 300;
    
    /**
     * The minimum height of a dialog
     */
    int MIN_DIALOG_HEIGHT = 300;
}
