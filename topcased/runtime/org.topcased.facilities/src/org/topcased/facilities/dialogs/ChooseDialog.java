/*******************************************************************************
 * Copyright (c) 2005, 2008 AIRBUS FRANCE.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    David Sciamma (Anyware Technologies),
 *    Jacques Lescot (Anyware Technologies) - initial API and implementation
 *    Jacques Lescot (Anyware Technologies) - Fix bug #1434
 *******************************************************************************/

package org.topcased.facilities.dialogs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IOpenListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.OpenEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.SelectionDialog;
import org.topcased.facilities.extensions.IChooseDialogFilter;
import org.topcased.facilities.internal.FacilitiesPlugin;
import org.topcased.facilities.internal.Messages;
import org.topcased.facilities.widgets.SearchableTree;

/**
 * The dialog used to choose between the different objects
 * 
 * @author <a href="david.sciamma@anyware-tech.com">David Sciamma</a>
 * @author <a href="tristan.faure@atosorigin.com">Tristan FAURE</a>
 */
public class ChooseDialog extends SelectionDialog implements ITopcasedDialogConstants
{
    /**
     * The key of the dialog's setting corresponding to the activation's advanced label provider preferred state
     */
    private static final String SHOW_ADVANCED_LABEL_PROVIDER = "showAdvancedLabelProvider"; //$NON-NLS-1$

    private SearchableTree tree;

    private ILabelProvider labelProvider;

    private Object[] objects;

    private Button advancedLabelProviderCheckBox;

    private ILabelProvider advancedLabelProvider;

    private static String extension_point_id = "org.topcased.facilities.chooseDialogFilter"; //$NON-NLS-1$

    private static List<IChooseDialogFilter> filters = getFilters();

    /**
     * Wrapper to adapt the ArrayContentProvider to a TreeViewer
     * 
     * @author <a href="david.sciamma@anyware-tech.com">David Sciamma</a>
     */
    private class TreeArrayContentProvider extends ArrayContentProvider implements ITreeContentProvider
    {
        /**
         * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
         */
        public Object[] getChildren(Object parentElement)
        {
            return new Object[0];
        }

        /**
         * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
         */
        public Object getParent(Object element)
        {
            return null;
        }

        /**
         * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
         */
        public boolean hasChildren(Object element)
        {
            return false;
        }
    }

    /**
     * Constructor
     * 
     * @param parentShell the parent shell
     * @param objects The available objects
     */
    public ChooseDialog(Shell parentShell, Object[] objects)
    {
        this(parentShell, objects, true);
    }

    /**
     * Constructor
     * 
     * @param parentShell the parent shell
     * @param objects The available objects
     * @param filter
     */
    public ChooseDialog(Shell parentShell, Object[] objects, boolean filter)
    {
        super(parentShell);
        this.objects = objects;
        if (filter)
        {
            filter();
        }
        setTitle(Messages.getString("ChooseDialog.title")); //$NON-NLS-1$
        setMessage(Messages.getString("ChooseDialog.label")); //$NON-NLS-1$
        setShellStyle(getShellStyle() | SWT.RESIZE);
    }

    /**
     * Filter the objects attribute and modify directly the property
     */
    protected void filter()
    {
        if (objects != null)
        {
            ArrayList<Object> list = new ArrayList<Object>(objects.length);
            Collections.addAll(list, objects);
            for (Iterator<Object> i = list.iterator(); i.hasNext();)
            {
                Object o = i.next();
                for (IChooseDialogFilter f : filters)
                {
                    if (f.filter(o))
                    {
                        i.remove();
                        break;
                    }
                }
            }
            objects = list.toArray();
        }
    }

    /**
     * TODO : javadoc ?
     */
    private static List<IChooseDialogFilter> getFilters()
    {
        IConfigurationElement[] elements = Platform.getExtensionRegistry().getConfigurationElementsFor(extension_point_id);
        List<IChooseDialogFilter> filters = new ArrayList<IChooseDialogFilter>(elements.length);
        for (IConfigurationElement elt : elements)
        {
            try
            {
                IChooseDialogFilter filter = (IChooseDialogFilter) elt.createExecutableExtension("filter"); //$NON-NLS-1$
                filters.add(filter);
            }
            catch (CoreException e)
            {
                FacilitiesPlugin.log(e);
            }
        }
        return filters;
    }

    /**
     * @see org.eclipse.ui.dialogs.SelectionDialog#configureShell(org.eclipse.swt.widgets.Shell)
     */
    protected void configureShell(Shell shell)
    {
        shell.setMinimumSize(MIN_DIALOG_WIDTH, MIN_DIALOG_HEIGHT);

        super.configureShell(shell);
    }

    /**
     * Create the Dialog area
     * 
     * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
     */
    protected Control createDialogArea(Composite parent)
    {
        // Dialog
        Composite dialogComposite = (Composite) super.createDialogArea(parent);

        GridLayout dialogLayout = new GridLayout();
        dialogLayout.marginWidth = 10;
        dialogLayout.marginHeight = 10;
        GridData dialogLayoutData = new GridData(GridData.FILL_BOTH);
        dialogLayoutData.widthHint = DEFAULT_DIALOG_WIDTH;
        dialogLayoutData.heightHint = DEFAULT_DIALOG_HEIGHT;
        dialogComposite.setLayout(dialogLayout);
        dialogComposite.setLayoutData(dialogLayoutData);

        tree = new SearchableTree(dialogComposite, SWT.SINGLE);
        tree.setLayoutData(new GridData(GridData.FILL_BOTH));
        tree.setContentProvider(new TreeArrayContentProvider());
        tree.setInitialSelection(new StructuredSelection(getInitialElementSelections()));

        // Initialize LabelProvider to be used to render Tree contents
        if (getAdvancedLabelProvider() != null)
        {
            // An advanced LabelProvider is defined, add a checkbox to choose the label provider to use
            advancedLabelProviderCheckBox = new Button(dialogComposite, SWT.CHECK);
            advancedLabelProviderCheckBox.setText(Messages.getString("ChooseDialog.opt")); //$NON-NLS-1$
            boolean showAdvanced = FacilitiesPlugin.getDefault().getDialogSettings().getBoolean(SHOW_ADVANCED_LABEL_PROVIDER);
            advancedLabelProviderCheckBox.setSelection(showAdvanced);
            tree.setLabelProvider(showAdvanced ? getAdvancedLabelProvider() : this.labelProvider);
        }
        else
        {
            tree.setLabelProvider(this.labelProvider);
        }
        tree.setInput(this.objects);
        hookListeners();

        return dialogComposite;
    }

    /**
     * This method had the UI listeners on the SWT widgets
     */
    private void hookListeners()
    {
        tree.getTreeViewer().addOpenListener(new IOpenListener()
        {
            /**
             * @see org.eclipse.jface.viewers.IOpenListener#open(org.eclipse.jface.viewers.OpenEvent)
             */
            public void open(OpenEvent event)
            {
                okPressed();
            }
        });

        // When an advancedLabelProvider is defined, add a listener to the checkbox changes
        if (getAdvancedLabelProvider() != null)
        {
            this.advancedLabelProviderCheckBox.addSelectionListener(new SelectionAdapter()
            {
                public void widgetSelected(SelectionEvent e)
                {
                    // Update the LabelProvider to use to render domain elements
                    tree.setLabelProvider(advancedLabelProviderCheckBox.getSelection() ? getAdvancedLabelProvider() : labelProvider);
                    // Update tree labels by setting again input
                    tree.setInput(objects);
                }
            });
        }
    }

    /**
     * Get the Advanced LabelProvider to use to display the Object
     * 
     * @return ILabelProvider
     */
    protected ILabelProvider getAdvancedLabelProvider()
    {
        return advancedLabelProvider;
    }

    /**
     * Set the Advanced LabelProvider to use to display the Object
     * 
     * @param advancedLabelProvider ILabelProvider
     */
    public void setAdvancedLabelProvider(ILabelProvider advancedLabelProvider)
    {
        this.advancedLabelProvider = advancedLabelProvider;
    }

    /**
     * Set the provider that displays the objects
     * 
     * @param provider the LabelProvider
     */
    public void setLabelProvider(ILabelProvider provider)
    {
        this.labelProvider = provider;
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#okPressed()
     */
    protected void okPressed()
    {
        IStructuredSelection selection = (IStructuredSelection) tree.getTreeViewer().getSelection();
        setResult(selection.toList());

        if (getAdvancedLabelProvider() != null)
        {
            // Store the new default value as a DialogSetting
            FacilitiesPlugin.getDefault().getDialogSettings().put(SHOW_ADVANCED_LABEL_PROVIDER, advancedLabelProviderCheckBox.getSelection());
        }

        super.okPressed();
    }

}