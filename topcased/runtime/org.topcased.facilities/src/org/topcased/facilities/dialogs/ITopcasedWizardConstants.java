/***********************************************************************************************************************
 * Copyright (c) 2008,2010 Communication & Systems.
 * 
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse
 * Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: Sebastien GABEL (CS) - initial API and implementation
 * 
 **********************************************************************************************************************/
package org.topcased.facilities.dialogs;

import org.eclipse.core.runtime.IPath;

/**
 * This interface stores the constants used during the various Topcased wizard.<br>
 * It enables to reach some common icons and images.
 * 
 * @author <a href="mailto:sebastien.gabel@c-s.fr">Sebastien GABEL</a>
 * @deprecated Use the helper {@link SharedImageHelper} instead.
 */
public interface ITopcasedWizardConstants
{
    /**
     * Folder where are stored the images resources.
     */
    String IMG_FOLDER = "icons"; //$NON-NLS-1$

    /**
     * Main Topcased logo find in all Topcased Wizard (right top corner)
     */
    String TOPCASED_LOGO_PATH = IMG_FOLDER + IPath.SEPARATOR + "topcased-logo.gif"; //$NON-NLS-1$

    /**
     * File Image (used in T2P plug-in)
     */
    String FILE_IMG = IMG_FOLDER + IPath.SEPARATOR + "file.gif"; //$NON-NLS-1$

    /**
     * Directory Image (used in T2P plug-in)
     */
    String DIRECTORY_IMG = IMG_FOLDER + IPath.SEPARATOR + "directory.gif"; //$NON-NLS-1$
}
