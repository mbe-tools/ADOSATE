/*******************************************************************************
 * Copyright (c) 2005 AIRBUS FRANCE. All rights reserved. This program and the
 * accompanying materials are made available under the terms of the Eclipse
 * Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: Thomas Friol (Anyware Technologies) - initial API and
 * implementation
 ******************************************************************************/
package org.topcased.modeler.internal.actions;

import org.eclipse.gef.ui.actions.DeleteAction;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IWorkbenchPart;
import org.topcased.modeler.ModelerActionConstants;
import org.topcased.modeler.editor.Modeler;
import org.topcased.modeler.internal.ModelerPlugin;
import org.topcased.modeler.internal.actions.precondition.ActionPreconditionHandler;
import org.topcased.modeler.l10n.Messages;

/**
 * Redefines the GEF delete action to prompt a confirmation dialog.<br>
 * 
 * Creation : 12 dec. 2005
 * 
 * @author <a href="mailto:thomas@anyware-tech.com">Thomas Friol</a>
 */
public class DeleteGraphElementAction extends DeleteAction {

    /**
     * Constructor
     * 
     * @param part
     */
    public DeleteGraphElementAction(IWorkbenchPart part) {
        super(part);
    }

    /**
     * @see org.eclipse.gef.ui.actions.DeleteAction#run()
     */
    @Override
    public void run() {
        final Modeler modeler = (Modeler) getWorkbenchPart();
        boolean conditionsChecked = ActionPreconditionHandler.getInstance().executePreconditions(this, modeler, getStructuredSelection() );
       
        if (conditionsChecked) {
            super.run();
        }
    }
    
    private IStructuredSelection getStructuredSelection() {
        if ( getSelection() instanceof IStructuredSelection) {
            return (IStructuredSelection) getSelection();
        }

        return new StructuredSelection(getSelectedObjects());
    }

    /**
     * @see org.eclipse.gef.ui.actions.DeleteAction#init()
     */
    protected void init() {
        super.init();
        
        setText(Messages.getString("DeleteGraphElementAction.CmdLabel"));
        setImageDescriptor(ModelerPlugin.getImageDescriptor("icons/deleteFromDiagram.gif"));
    	setId( ModelerActionConstants.DELETE_GRAPH_ELMEMENT );
    }
    
    @Override
	protected boolean calculateEnabled() {
    	return 	super.calculateEnabled() &&
    	        ActionPreconditionHandler.getInstance().executeEnablementConditions( this, 
						(Modeler) getWorkbenchPart(), 
						getStructuredSelection() );
    			 /*&&
    			ActionPreconditionHandler.getInstance().executePreconditions( 	this, 
    																			(Modeler) getWorkbenchPart(), 
    																			getStructuredSelection() );*/

    }
}
