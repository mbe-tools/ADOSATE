package org.topcased.modeler.actions.preconditions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.topcased.modeler.ActionConditionChecker;
import org.topcased.modeler.editor.Modeler;

public class DeleteGraphEnablementChecker implements ActionConditionChecker {
	
	private final ActionConditionChecker deleteModelCond = new DeleteModelObjectEnablementChecker();

	public boolean checkCondition(	final Action p_actionToCheck,
									final Modeler p_modeler,
									final IStructuredSelection p_selection ) {
		return !p_selection.isEmpty() && !deleteModelCond.checkCondition( p_actionToCheck, p_modeler, p_selection );
	}
}
