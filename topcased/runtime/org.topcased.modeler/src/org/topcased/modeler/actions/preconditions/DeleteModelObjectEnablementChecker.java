package org.topcased.modeler.actions.preconditions;

import java.util.Iterator;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.topcased.modeler.ActionConditionChecker;
import org.topcased.modeler.edit.EMFGraphEdgeEditPart;
import org.topcased.modeler.edit.EMFGraphNodeEditPart;
import org.topcased.modeler.editor.Modeler;

public class DeleteModelObjectEnablementChecker implements ActionConditionChecker {

	public boolean checkCondition(	final Action p_actionToCheck,
									final Modeler p_modeler,
									final IStructuredSelection p_selection ) {
		if ( p_selection.isEmpty() ) {
			return false;
		}
		
		final Iterator<?> elementsIt = p_selection.iterator();
		
		while ( elementsIt.hasNext()  ) {
			final Object obj = elementsIt.next();
			
			if ( !( obj instanceof EMFGraphNodeEditPart ) && !( obj instanceof EMFGraphEdgeEditPart ) ) {
				return false;
			}
		}
		
		return true;
	}
}
