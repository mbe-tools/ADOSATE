package org.eclipse.eclipsemonkey;

import java.util.Iterator;

import org.eclipse.ui.IStartup;
import org.eclipse.ui.PlatformUI;

public class Startup implements IStartup {

	public void earlyStartup() {
		runStartupScripts();
	}

	/**
	 * runStartupScripts
	 */
	private void runStartupScripts() {
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable()	{
			public void run() {
				for (Iterator<?> iter = EclipseMonkeyPlugin.getDefault().getScriptStore().values().iterator(); iter.hasNext();) 
				{
					StoredScript script = (StoredScript) iter.next();
					String onLoadFunction = script.metadata.getOnLoadFunction();
					if(onLoadFunction != null)
					{
						MenuRunMonkeyScript runner = new MenuRunMonkeyScript(script.scriptPath);
						try {
							runner.run(onLoadFunction, new Object[0]);
						} catch (RunMonkeyException e) {
							// Do nothing
						}
					}
				}
			}
		});
	}
}
